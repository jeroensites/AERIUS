# AERIUS Calculator

The root directory for the CALCULATOR project. Contains various scripts to assist with development. Developers should take care to familiarize themselves with them as not doing so will prolong any development effort.

## Prerequisites

To install, configure, and develop for this project, the following tools are required:

- `mvn` (Maven)
- `git`
- `javac` Java JDK 8 (various services have been tested to work up to Java 11 and in JDK 11, however development for now occurs mainly in java 8 and JDK 8)

Optional:

- `tmux` (terminal multiplexer)
- `entr` (file change tracker)
- `lsof` (file/tcp usage, for checking port availability)
- `ag`   (Like ack, but faster. Install as silversearcher-ag)

### Arch Linux

```
sudo pacman -Syu maven git jdk11-openjdk
```

### Ubuntu / Debian

```
sudo apt install git maven openjdk-8-jdk
```

## Cloning this project

Clone the project using:

```
git clone git@github.com:aerius/AERIUS-II.git
```

## Running development mode

### Quick start

Navigate to the `/source/calculator` directory.

```
cd source/calculator
```

To start all development servers, run:

```
./serve.sh
```

This last script requires the optional dependencies.

If you are new to `tmux`, you could consider executing the `AERIUS-II/scripts/tmux-setup.sh` script to install a convenient default setup (changes the command prefix key to the `` ` `` (backtick) character, and enables mouse and scroll interaction)

An explanation of what this does is described in the following few paragraphs. Otherwise skip to [Configure the Eclipse IDE](#configure-the-eclipse-ide).

Next, to run a GWT development environment we need to run two types of servers:

1. A web server to serve static html/js files, fonts, assets, etc.
2. A GWT codeserver to recompile Java files fetched by the static recompile-requester.

This is a multi-module project reconfigured to follow TBroyer's gwt maven setup. Thanks to this setup it is now much easier to run these two servers using just Maven, and avoid dealing with (most of) the Eclipse or IntelliJ configuration hell.

### Instructions

There's various convenience scripts in the calculator (/source/calculator) script directory which will run the servers.

Currently there's a webservers, a GWT recompile server, 2 microservices, and a livereload server:

- codeserver.sh
- webserver.sh
- importservice.sh
- layerservice.sh
- livereload.sh

If all goes well, all of these servers will be up and running, and the application will be available in the browser on:

```
http://localhost:8080
```

More of these types of servers will be added as the project progresses.

#### Troubleshooting

Sometimes one of these servers might fail to start. Usually this can be resolved by executing one or multiple of the clean-*.sh scripts in this directory - depending on failure context. It is the intention these types of servers run correctly without having to do this, however this has not yet completely been achieved, and will likely remain a perpetual WIP as features, bugs, and the accompanying codebase expands.

#### Informational

The codeserver script consists simply of a maven goal for the `gwt` plugin:

```
mvn gwt:codeserver :aerius-calculator-wui-client -am
```

The webserver script consists of a maven goal for the `jetty` plugin, and an argument to run it in the dev profile:

```
mvn jetty:run -pl :aerius-calculator-wui-server -am -Denv=dev
```

### Optional: Run a livereload checker

Angular and other web frameworks often boast a livereload feature which for GWT has been sorely lacking. Not many tools are available to make this happen, so a light and rudimentary way of doing it has been scrapped together. It uses a tool called `entr` to monitor file changes.

For more information on this tool, see: http://eradman.com/entrproject/ It is available for most package managers.

While in dev mode, an additional script has been added to the index which will poll a specific file on the server, if this file changes, it will issue a web page reload. The file changes when `entr` notices source files have changed.

While this method is very rudimentary and imperfect, it is also robust and requires few extra dependencies, and uses no plugins, extensions or websockets.

To run it, simply execute:

```
./livereload.sh
```

Currently, this tool indirectly depends on an IDE and its annotation processor for changes in HTML and SCSS to be reflected, this might possibly be remidied in the future.

#### Hint

> Unfortunately, this livereload method produces a large number of requests, visible in the dev tools' network tab. To ignore this (for Chrome dev tools), add `-reload` to the network filter.

### Optional: Run all of the above through tmux

Alternatively, you can run all of the above in a tmux session.

`tmux` is a terminal multiplexer, emulating terminals on a server rather than in the shell, which the shell can attach to.

You need to additionally install `tmux` for this to work.

Run:

```
./serve.sh
```

This `tmux` session will contain three panels, one for the `codeserver`, one for the `webserver`, and one for the `livereload checker`.

#### Hint

Detach from `tmux` using `Ctrl + B, D`.

## Configure the Eclipse IDE

Configuring the eclipse IDE is not strictly necessary for the development servers to work. However, some code is generated (via annotation processing), and the easiest way to reprocess this is through an IDE.

The following describes how to do a typical eclipse setup. 

1. Import the project root into eclipse as a maven project.

2. Install the m2e-apt plugin, so Eclipse will process annotations. (required for AutoValue and VueGWT annotations)

See https://marketplace.eclipse.org/content/m2e-apt

When installed, enable annotation processing by setting the

```
Window > Preferences > Maven > Annotation Processing > Annotation Processing Mode
```

setting, to

```
Automatically configure JDT APT
```

For further information, please refer to:

https://vuegwt.github.io/vue-gwt/guide/project-setup.html#annotation-processing

3. Install the gwt-vue plugin (required to make eclipse run the annotation processor when editing .html template files, instead of the associated .java file which is automatically reprocessed)

See https://github.com/VueGWT/vue-gwt-eclipse-plugin

For further information, please refer to:

https://vuegwt.github.io/vue-gwt/guide/project-setup.html#vue-gwt-eclipse-plugin

## Known issues

1. Generated annotation cannot be resolved

```
[INFO]          [ERROR] Line x: The import javax.annotation.processing cannot be resolved
[INFO]          [ERROR] Line y: Generated cannot be resolved to a type
```

This seems to be related to this issue: https://github.com/gwtproject/gwt/issues/9612

*Solved* by super-sourcing this annotation. Future GWT version will also have this fix.
