/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wuiservices.services.storage;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import nl.overheid.aerius.shared.LinkedFileType;

/**
 * Service interface for managing storage of files.
 */
public interface StorageService {

  /**
   * Stores the file.
   *
   * @param filename original name of file
   * @param size size of the data
   * @param in inputstream to the file data
   * @return Returns the file code with which this file can be retrieved
   * @throws IOException
   */
  String addRawFile(final String filename, final long size, boolean publiclyAccessible, final InputStream in) throws IOException;

  /**
   * Adds or replaces a file that's linked to the raw file (f.e. a validation log file).
   *
   * @param fileCode file code of the file the validation is for
   * @param in the content of the linked file to add
   * @param linkedFileType the type of linked file to add (max 1 of each type per filecode)
   * @throws IOException
   */
  void addOrReplaceLinkedFile(final String fileCode, final InputStream in, final LinkedFileType linkedFileType) throws IOException;

  /**
   * Retrieves a file that's linked to the raw file if it exists (f.e. a validation log file).
   *
   * @param fileCode
   * @param linkedFileType
   * @return The contents of the retrieved file (optional)
   */
  Optional<String> retrieveLinkedFileIfExists(final String fileCode, final LinkedFileType linkedFileType) throws IOException;

  /**
   * Returns the raw data of the file with the given file code.
   *
   * @param fileCode Code of the file to retrieve
   * @return Data object with information of file
   * @throws FileNotFoundException thrown when the file is unknown
   */
  StoredFile getRawFile(String fileCode) throws FileNotFoundException;

  /**
   * @param fileCode code to check if it exists in storage
   * @return True if it exists, false otherwise.
   */
  boolean isExistingFileCode(String fileCode);

  /**
   * Check if a file has been marked for public access.
   * @param fileCode the fileCode
   * @return true if it exists and is publicly accessible.
   */
  boolean isPubliclyAccessible(String fileCode) throws FileNotFoundException;

  /**
   * Deletes all files for the given file code.
   *
   * @param fileCode code of the files to remove
   * @throws IOException if not found
   */
  void deleteFilesForFileCode(String fileCode) throws IOException;
}
