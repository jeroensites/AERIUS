/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wuiservices.controllers.v2;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.shared.InternalRequestMappings;
import nl.overheid.aerius.shared.LinkedFileType;
import nl.overheid.aerius.wuiservices.services.storage.StorageService;
import nl.overheid.aerius.wuiservices.services.storage.StoredFile;

/**
 * Controller for handling REST calls related to file up- and download.
 */
@Controller
public class FileController {
  private static final Logger LOG = LoggerFactory.getLogger(FileController.class);

  private final StorageService storageService;

  @Autowired
  public FileController(final StorageService storageService) {
    this.storageService = storageService;
  }

  /**
   * Adds a file.
   *
   * @param file file resources
   * @return the fileCode referring to the file in the storage
   * @throws IOException
   */
  @PostMapping(InternalRequestMappings.FILE_ADD)
  @ResponseBody
  public ResponseEntity<String> addFile(final @RequestParam("file") MultipartFile file, final @RequestParam(name = InternalRequestMappings.FILE_PUBLIC_PARAMETER, required = false, defaultValue = "false") Boolean publiclyAccessible) throws IOException {
    LOG.trace("SubmitFile {}", file);
    final String originalFilename = file.getOriginalFilename();
    return ResponseEntity.ok().body(storageService.addRawFile(originalFilename, file.getSize(), publiclyAccessible, file.getInputStream()));
  }

  /**
   * Adds an empty RAW file
   *
   * @param filename name of the file
   * @return the filecode referring to the file in the storage
   * @throws IOException
   */
  @PostMapping(InternalRequestMappings.EMPTY_FILE_ADD)
  @ResponseBody
  public ResponseEntity<String> addEmptyFile(final @RequestParam("filename") String filename) throws IOException {
    LOG.trace("SubmitEmptyFile {}", filename);
    final InputStream emptyInputStream = new ByteArrayInputStream(new byte[0]);

    return ResponseEntity.ok().body(storageService.addRawFile(filename, 0, false, emptyInputStream));
  }

  /**
   * Adds a linked file.
   *
   * @param fileCode The filecode to add the validation file for
   * @param linkedFileType The type of file that is being added
   * @param data The data of the linked file
   * @return true If adding the validation file was successful
   * @throws IOException
   */
  @PostMapping(InternalRequestMappings.LINKED_FILE)
  @ResponseBody
  public ResponseEntity<Boolean> addOrReplaceLinkedFile(final @PathVariable String fileCode, final @PathVariable LinkedFileType linkedFileType,
      final @RequestParam("file") MultipartFile file) throws IOException {
    LOG.trace("SubmitFile for file {} with data {} and linkedFileType {}", fileCode, file, linkedFileType);
    if (storageService.isExistingFileCode(fileCode)) {
      storageService.addOrReplaceLinkedFile(fileCode, file.getInputStream(), linkedFileType);
      return ResponseEntity.ok().body(Boolean.TRUE);
    } else {
      return ResponseEntity.notFound().build();
    }
  }

  /**
   * Retrieves a linked file.
   *
   * @param fileCode The filecode to get the linked file for
   * @param linkedFileType The type of file that is being added
   * @return JSON string of linked file if available, otherwise an empty Optional
   * @throws IOException
   */
  @GetMapping(InternalRequestMappings.LINKED_FILE)
  public ResponseEntity<String> retrieveLinkedFile(final @PathVariable String fileCode, final @PathVariable LinkedFileType linkedFileType)
      throws IOException {
    LOG.trace("Attempting to retrieve the linkedFileType {} for the file with filecode {}", linkedFileType, fileCode);

    final Optional<String> linkedFileContent = storageService.retrieveLinkedFileIfExists(fileCode, linkedFileType);
    if (linkedFileContent.isEmpty()) {
      return ResponseEntity.notFound().build();
    } else {
      return ResponseEntity.ok().body(linkedFileContent.get());
    }
  }

  /**
   * Retrieve the file belonging to the given fileCode
   *
   * @param fileCode The filecode to retrieve a file for
   * @return A file
   */
  @GetMapping(InternalRequestMappings.FILE_RETRIEVE_RAW)
  @ResponseBody
  public ResponseEntity<Resource> retrieveFile(final @PathVariable String fileCode) throws IOException {
    LOG.trace("retrieveFile {}", fileCode);
    if (storageService.isExistingFileCode(fileCode)) {
      return getResourceResponse(fileCode);
    } else {
      return ResponseEntity.notFound().build();
    }
  }

  /**
   * Retrieve a file for exposure to the public by the given fileCode.
   * @param fileCode The fileCode
   * @return A file
   */
  @GetMapping(InternalRequestMappings.FILE_RETRIEVE_RAW_PUBLIC)
  @ResponseBody
  public ResponseEntity<Resource> retrievePublicFile(final @PathVariable String fileCode) throws IOException {
    LOG.trace("retrievePublicFile {}", fileCode);
    if (storageService.isPubliclyAccessible(fileCode)) {
      return getResourceResponse(fileCode);
    } else {
      return ResponseEntity.notFound().build();
    }
  }

  private ResponseEntity<Resource> getResourceResponse(final String fileCode) throws FileNotFoundException, MalformedURLException {
    final StoredFile file = storageService.getRawFile(fileCode);
    final FileUrlResource resource = new FileUrlResource(file.getFile().getAbsolutePath());

    LOG.debug("Returning file: {}", file);
    return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
        "attachment; filename=\"" + file.getFilename() + "\"").body(resource);
  }

  /**
   * Deletes the file for the given fileCode.
   *
   * @param fileCode file code of file to delete
   * @throws IOException
   */
  @DeleteMapping(InternalRequestMappings.FILE_DELETE)
  public ResponseEntity<Void> deleteFile(final @PathVariable String fileCode) throws IOException {
    LOG.trace("removeImportFile {}", fileCode);
    if (storageService.isExistingFileCode(fileCode)) {
      storageService.deleteFilesForFileCode(fileCode);

      return ResponseEntity.ok().build();
    } else {
      return ResponseEntity.notFound().build();
    }
  }
}
