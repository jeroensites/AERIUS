/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wuiservices.services.storage.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.shared.LinkedFileType;
import nl.overheid.aerius.wuiservices.services.storage.StorageProperties;
import nl.overheid.aerius.wuiservices.services.storage.StorageService;
import nl.overheid.aerius.wuiservices.services.storage.StoredFile;

/**
 * Class to manage files to be stored on a file system.
 */
@Service
@EnableConfigurationProperties(StorageProperties.class)
public class FileStorageService implements StorageService {

  /**
   * Types of data that can be stored.
   */
  public enum FileStorageDataType {
    /**
     * Folder containing the files
     */
    FOLDER(""),
    /**
     * Raw input data, like .gml or .pdf files
     */
    RAW("file"),
    /**
     * ImportParcel file (without exceptions and errors) (JSON)
     */
    IMPORTPARCEL("importparcel.json"),
    /**
     * Validation log file (JSON)
     */
    VALIDATION_LOG("validation.json"),
    /**
     * Metadata file (JSON)
     */
    METADATA("metadata.json");

    private final String filename;

    FileStorageDataType(final String filename) {
      this.filename = filename;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
      return filename;
    }
  }

  private static final Logger LOG = LoggerFactory.getLogger(FileStorageService.class);

  private final File localStorageDirectory;
  private final FileStorageRepository repository;

  @Autowired
  public FileStorageService(final StorageProperties properties, final FileStorageRepository fileStorageRepository) throws IOException {
    this.repository = fileStorageRepository;
    final String location = properties.getLocation();
    localStorageDirectory = new File(location);
    if (!localStorageDirectory.exists()) {
      Files.createDirectory(localStorageDirectory.toPath());
    }
  }

  @Override
  public String addRawFile(final String filename, final long definedSize, final boolean publiclyAccessible, final InputStream in) throws IOException {
    final String fileCode = UUID.randomUUID().toString().replace("-", "");
    final Path path = fileCode2Path(fileCode, FileStorageDataType.FOLDER);
    final Path rawFilePath = fileCode2Path(fileCode, FileStorageDataType.RAW);
    final Path metadataFilePath = fileCode2Path(fileCode, FileStorageDataType.METADATA);

    Files.createDirectory(path);
    Files.copy(in, rawFilePath);
    repository.addMetadataFile(fileCode, metadataFilePath, filename, definedSize, publiclyAccessible);

    LOG.trace("Store file: {} with code: {} and size: {}", filename, fileCode, definedSize);
    return fileCode;
  }

  @Override
  public void addOrReplaceLinkedFile(final String fileCode, final InputStream data, final LinkedFileType linkedFileType) throws IOException {
    final FileStorageDataType dataType = getFileStorageDataType(linkedFileType);
    final Path path = fileCode2Path(fileCode, dataType);

    repository.addOrReplaceLinkedFile(path, data);
  }

  @Override
  public Optional<String> retrieveLinkedFileIfExists(final String fileCode, final LinkedFileType linkedFileType) throws IOException {
    final FileStorageDataType dataType = getFileStorageDataType(linkedFileType);
    final Path path = fileCode2Path(fileCode, dataType);

    return repository.retrieveLinkedFileIfExists(path);
  }

  @Override
  public StoredFile getRawFile(final String fileCode) throws FileNotFoundException {
    LOG.debug("Getting file: {}", fileCode);
    final Path path = fileCode2Path(fileCode, FileStorageDataType.FOLDER);
    final File folder = path.toFile();

    if (!folder.exists()) {
      LOG.debug("File with filecode ({}) does not exist", fileCode);
      throw new FileNotFoundException("File with filecode " + fileCode + " is not available.");
    } else {
      final StoredFile storedFile = repository.getMetadataFile(path);
      final File file = new File(folder.toString(), FileStorageDataType.RAW.getFilename());

      storedFile.setFile(file);
      return storedFile;
    }
  }

  @Override
  public boolean isExistingFileCode(final String fileCode) {
    final Path path = fileCode2Path(fileCode, FileStorageDataType.FOLDER);
    final File folder = path.toFile();
    return folder.exists();
  }

  @Override
  public boolean isPubliclyAccessible(final String fileCode) throws FileNotFoundException {
    final Path path = fileCode2Path(fileCode, FileStorageDataType.FOLDER);
    if (!path.toFile().exists()) {
      return false;
    }

    return repository.getMetadataFile(path).isPubliclyAccessible();
  }

  @Override
  public void deleteFilesForFileCode(final String fileCode) throws IOException {
    deleteFile(fileCode, FileStorageDataType.IMPORTPARCEL);
    deleteFile(fileCode, FileStorageDataType.VALIDATION_LOG);
    deleteFile(fileCode, FileStorageDataType.METADATA);
    deleteFile(fileCode, FileStorageDataType.RAW);
    deleteFile(fileCode, FileStorageDataType.FOLDER);
  }

  private void deleteFile(final String fileCode, final FileStorageDataType type) throws IOException {
    try {
      final Path path = fileCode2Path(fileCode, type);
      Files.deleteIfExists(path);
    } catch (final IOException e) {
      final String error = "Could not delete file with code " + fileCode + " and type " + type;
      LOG.info(error);
      throw new IOException(error, e);
    }
  }

  private Path fileCode2Path(final String fileCode, final FileStorageDataType fileType) {
    return Paths.get(localStorageDirectory.getAbsolutePath(), fileCode, fileType.getFilename());
  }

  private FileStorageDataType getFileStorageDataType(final LinkedFileType linkedFileType) {
    switch (linkedFileType) {
    case VALIDATION:
      return FileStorageDataType.VALIDATION_LOG;
    case IMPORTPARCEL:
      return FileStorageDataType.IMPORTPARCEL;
    default:
      throw new IllegalArgumentException("LinkedFileType unsupported: " + linkedFileType);
    }
  }

}
