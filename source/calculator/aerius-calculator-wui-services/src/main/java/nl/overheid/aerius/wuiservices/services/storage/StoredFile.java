/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wuiservices.services.storage;

import java.io.File;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Data file with meta data for a stored raw data file.
 */
public class StoredFile {
  private String fileCode;
  /**
   * Time file was created in seconds.
   */
  private long created;
  private String filename;
  private long size;
  private boolean publiclyAccessible;
  /**
   * Physical location of the file (not saved, hence transient).
   */
  private transient File file;

  StoredFile() {
    // Default constructor
  }

  public StoredFile(final String fileCode, final long created, final String filename, final long size, final boolean publiclyAccessible) {
    this.fileCode = fileCode;
    this.created = created;
    this.filename = filename;
    this.size = size;
    this.publiclyAccessible = publiclyAccessible;
  }

  public String getFileCode() {
    return fileCode;
  }

  public void setFileCode(final String fileCode) {
    this.fileCode = fileCode;
  }

  public long getCreated() {
    return created;
  }

  public void setCreated(final long created) {
    this.created = created;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(final String filename) {
    this.filename = filename;
  }

  public long getSize() {
    return size;
  }

  public void setSize(final long size) {
    this.size = size;
  }

  public boolean isPubliclyAccessible() {
    return publiclyAccessible;
  }

  public void setPubliclyAccessible(final boolean publiclyAccessible) {
    this.publiclyAccessible = publiclyAccessible;
  }

  @JsonIgnore
  public File getFile() {
    return file;
  }

  public void setFile(final File file) {
    this.file = file;
  }

  @Override
  public String toString() {
    return "StoredFile [fileCode=" + fileCode + ", file=" + file + ", filename=" + filename + ", size=" + size + "]";
  }
}
