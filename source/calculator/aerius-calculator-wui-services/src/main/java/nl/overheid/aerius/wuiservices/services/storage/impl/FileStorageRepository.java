/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wuiservices.services.storage.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.wuiservices.services.storage.StoredFile;
import nl.overheid.aerius.wuiservices.services.storage.impl.FileStorageService.FileStorageDataType;

/**
 * Repository for handling raw data files.
 * It keeps a json meta data file with each file with .json extension.
 */
@Component
class FileStorageRepository {

  private final ObjectMapper objectMapper;

  @Autowired
  public FileStorageRepository(final ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  public void addMetadataFile(final String fileCode, final Path file, final String filename, final long definedSize, final boolean publiclyAccessible) throws IOException {
    final long created = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
    final StoredFile storedFile = new StoredFile(fileCode, created, filename, definedSize, publiclyAccessible);

    Files.write(file, objectMapper.writeValueAsBytes(storedFile));
  }

  public StoredFile getMetadataFile(final Path path) throws FileNotFoundException {
    try {
      final Path metadataPath = Paths.get(path.toString(), FileStorageDataType.METADATA.getFilename());

      if (!metadataPath.toFile().exists()) {
        throw new FileNotFoundException("File for code " + path + " could not be found");
      }
      final byte[] bytes = Files.readAllBytes(metadataPath);

      return objectMapper.readValue(bytes, StoredFile.class);
    } catch (final IOException e) {
      throw new FileNotFoundException(e.getMessage());
    }
  }

  public void removeFile(final Path file) throws IOException {
    Files.deleteIfExists(file);
  }

  public void addOrReplaceLinkedFile(final Path path, final InputStream in) throws IOException {
    if (Files.exists(path)) {
      Files.delete(path);
    }
    Files.copy(in, path);
  }

  public Optional<String> retrieveLinkedFileIfExists(final Path path) throws IOException {
    if (Files.exists(path)) {
      return Optional.of(new String(Files.readAllBytes(path), StandardCharsets.UTF_8));
    } else {
      return Optional.empty();
    }
  }

}
