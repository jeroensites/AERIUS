/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wuiservices.controllers.v2;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import nl.overheid.aerius.wuiservices.services.storage.StorageService;
import nl.overheid.aerius.wuiservices.services.storage.StoredFile;

/**
 * Test class for {@link FileController}.
 */
@AutoConfigureMockMvc
@SpringBootTest
class FileControllerTest {

  private static final String VERSION = "/v2/";

  @Autowired private MockMvc mvc;

  @MockBean private StorageService storageService;

  @Test
  void testAddFile() throws Exception {
    final String fileName = "test.gml";
    final MockMultipartFile multipartFile = new MockMultipartFile("file", fileName, "text/plain", "AERIUS".getBytes());
    final long size = multipartFile.getSize();
    when(storageService.addRawFile(eq(fileName), eq(size), anyBoolean(), any())).thenReturn("1");
    mvc.perform(multipart(VERSION + "files").file(multipartFile))
        .andExpect(status().isOk());

    verify(storageService).addRawFile(eq(fileName), eq(size), anyBoolean(), any());
  }

  @Test
  void testRetrieveFile() throws Exception {
    when(storageService.isExistingFileCode("1")).thenReturn(true);
    final StoredFile storedFile = new StoredFile("1", 1L, "test.txt", 1, false);
    storedFile.setFile(new File(FileControllerTest.class.getResource(getClass().getSimpleName() + ".class").toURI()));
    when(storageService.getRawFile("1")).thenReturn(storedFile);

    mvc.perform(get(VERSION + "files/1")).andExpect(status().isOk())
        .andReturn().getResponse().getHeader(HttpHeaders.CONTENT_DISPOSITION).contains("test.txt");
  }

  @Test
  void testRetrieveFile404Missing() throws Exception {
    when(storageService.isExistingFileCode("2")).thenReturn(false);

    mvc.perform(get(VERSION + "files/2")).andExpect(status().isNotFound());
  }

  @Test
  void testRetrievePublicFile() throws Exception {
    when(storageService.isPubliclyAccessible("1")).thenReturn(true);

    final StoredFile storedFile = new StoredFile("1", 1L, "test.txt", 1, true);
    storedFile.setFile(new File(FileControllerTest.class.getResource(getClass().getSimpleName() + ".class").toURI()));
    when(storageService.getRawFile("1")).thenReturn(storedFile);

    mvc.perform(get(VERSION + "files/public/1")).andExpect(status().isOk())
        .andReturn().getResponse().getHeader(HttpHeaders.CONTENT_DISPOSITION).contains("test.txt");
  }

  @Test
  void testRetrievePublicFileIsNotAcessible() throws Exception {
    when(storageService.isPubliclyAccessible("1")).thenReturn(false);

    mvc.perform(get(VERSION + "files/public/1")).andExpect(status().isNotFound());
  }

  @Test
  void testDeleteFile() throws Exception {
    when(storageService.isExistingFileCode("2")).thenReturn(true);

    mvc.perform(delete(VERSION + "files/2")).andExpect(status().isOk());
  }

  @Test
  void testDeleteFile404Missing() throws Exception {
    when(storageService.isExistingFileCode("2")).thenReturn(false);

    mvc.perform(delete(VERSION + "files/2")).andExpect(status().isNotFound());
  }
}
