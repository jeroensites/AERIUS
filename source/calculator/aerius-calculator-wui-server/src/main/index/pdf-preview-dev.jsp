<%@page import="java.util.Date"
%><%
final String lang = "nl";
final long timestamp = new Date().getTime();
%><!DOCTYPE html>
<html lang="<%=lang%>">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="gwt:property" content="locale=<%=lang%>">
<base href="${pageContext.request.contextPath}/">
<link rel="icon shortcut" type="image/x-icon" href="favicon.ico">
<link rel="stylesheet" href="fonts/noto-sans-tc-v10-latin-regular.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="fonts/icons.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="res/base.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="res/print.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="res/ol.css?v=<%=timestamp%>" type="text/css">
<script src="components/graham_scan.min.js" type="text/javascript" async></script>
<script src="webjars/openlayers/${openlayers.version}/ol.js" type="text/javascript" async></script>
<script src="webjars/proj4/${proj4.version}/dist/proj4.js" type="text/javascript" async></script>
<script src="webjars/vue/${vue.version}/vue.min.js" type="text/javascript" async></script>
<script src="webjars/d3js/${d3.version}/d3.js" type="text/javascript" async></script>
<script src="webjars/github-com-vuelidate-vuelidate/${vuelidate.version}/vuelidate.min.js" type="text/javascript" async></script>
<script src="webjars/github-com-vuelidate-vuelidate/${vuelidate.version}/validators.min.js" type="text/javascript" async></script>
<script src="application/application.nocache.js?v=<%=timestamp%>" type="text/javascript" defer></script>
<script src="scripts/config_dev.js?v=<%=timestamp%>" type="text/javascript"></script>
</head>
<body style="margin:0px;height:100%;">
  <div id="base"></div>
</body>
</html>
