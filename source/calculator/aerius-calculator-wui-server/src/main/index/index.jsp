<%@page import="java.util.Date,java.util.Locale,nl.overheid.aerius.util.LocaleRequester,nl.overheid.aerius.server.i18n.AeriusMessages"
%><%
final Locale locale = LocaleRequester.initLocale(request, response);
request.getSession().setAttribute("messages", new AeriusMessages("${aerius.customer}", locale));
final String lang = locale.getLanguage();
final long timestamp = new Date().getTime();
%><!DOCTYPE html>
<html lang="<%=lang%>">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="gwt:property" content="locale=<%=lang%>">
<title>AERIUS&reg; Calculator</title>
<base href="${pageContext.request.contextPath}/">
<link rel="icon shortcut" type="image/x-icon" href="favicon.ico">
<link rel="stylesheet" href="fonts/noto-sans-tc-v10-latin-regular.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="fonts/icons.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="res/base.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="res/ol.css?v=<%=timestamp%>" type="text/css">
<link rel="stylesheet" href="res/jira-collector.css" type="text/css">
<script src="components/graham_scan.min.js" type="text/javascript" async></script>
<script src="webjars/openlayers/${openlayers.version}/ol.js" type="text/javascript" async></script>
<script src="webjars/proj4/${proj4.version}/dist/proj4.js" type="text/javascript" async></script>
<script src="webjars/vue/${vue.version}/vue.min.js" type="text/javascript" async></script>
<script src="webjars/d3js/${d3.version}/d3.js" type="text/javascript" async></script>
<script src="webjars/github-com-vuelidate-vuelidate/${vuelidate.version}/vuelidate.min.js" type="text/javascript" async></script>
<script src="webjars/github-com-vuelidate-vuelidate/${vuelidate.version}/validators.min.js" type="text/javascript" async></script>
<script src="application/application.nocache.js?v=<%=timestamp%>" type="text/javascript" defer></script>
<script src="scripts/config.js?v=<%=timestamp%>" type="text/javascript"></script>
<%@include file="WEB-INF/jsp/collect_feedback.jsp"%>
<%
  String title = "AERIUS<sup class=\"splash-reg\">&reg;</sup> Calculator";
  String text = "";
  boolean loading = true;%>
<%@ include file="/WEB-INF/jsp/splash.jsp" %>
