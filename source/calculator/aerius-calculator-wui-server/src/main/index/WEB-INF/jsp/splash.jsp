<%@page import="nl.overheid.aerius.server.util.ImageInliner"
%><%@ include file="/WEB-INF/jsp/splash.css" %>
</head>
<body style="margin:0px;height:100%;">
  <div class="splash-base" id="base">
    <div class="splash-container">
      <h1 class="splash-title"><%=title%></h1>
      <img class="splash-graph" src="<%=ImageInliner.inline("images/splash/aerius-splash-illustration.svg")%>"></img>
      <div class="splash-loadingContainer">
        <% if (loading) { %><img id="splash-loader" src="<%=ImageInliner.inline("images/loading-spinner.svg")%>"></img><%
           } else { %><div id="splash-loader"></div><%}%>
        <img class="splash-product-logo" src="<%=ImageInliner.inline("images/splash/splash-calculator.svg")%>" alt="AERIUS Calculator Logo"></img>
        <div class="splash-version" id="text">
          <p>${messages['splashVersionPrefix']} <%=nl.overheid.aerius.AeriusVersion.getFriendlyVersionNumber()%></p>
          <p id="splash-message"><% if (loading) {%>${messages['splashLoadingText']}<%} else {%><%=text%><%}%></p>
        </div>
      </div>
    <p class="splash-trademark">${messages['splashTrademark']}</p>
    </div>
    <div class="splash-footer">
      <%@ include file="/WEB-INF/jsp/splash_footer_${aerius.customer}.jsp" %>
    </div>
  </div>
</body>
</html>
