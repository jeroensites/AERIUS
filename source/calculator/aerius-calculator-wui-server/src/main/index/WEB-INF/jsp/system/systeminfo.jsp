<%@page import="java.util.Date,java.util.Locale,nl.overheid.aerius.util.LocaleRequester,nl.overheid.aerius.util.LocaleUtils,nl.overheid.aerius.server.i18n.AeriusMessages"
%><%
final Locale locale = LocaleRequester.initLocale(request, response);
final AeriusMessages messages = new AeriusMessages("${aerius.customer}", locale);
request.getSession().setAttribute("messages", messages);
final long timestamp = new Date().getTime();
String title = "AERIUS<sup class=\"splash-reg\">&reg;</sup> Calculator";
%><!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${messages['systemInfoEditorPageTitle']}</title>
<link rel="stylesheet" href="fonts/noto-sans-tc-v10-latin-regular.css?v=<%=timestamp%>" type="text/css">
<%@ include file="/WEB-INF/jsp/splash.css" %>
</head>
<body style="margin:0px;height:100%;">
  <div class="splash-base" id="base">
    <h1 class="splash-title"><%=title%></h1>
    <div class="message loginform">
      <div class="text-left">
        <h2>${messages['systemInfoEditorTitle']}</h2>
        <p>${messages['systemInfoEditorBody']}</p>
      </div>
      <div class="text-right">
        <form method="post" action="#">
          <input type="text" id="gwt-debug-message" name="message" placeholder="${messages['systemInfoEditorMessage']}" style="display:block;width:1024px"/>
          <select id="gwt-debug-locale" name="locale" placeholder="${messages['systemInfoEditorLocale']}">
          <% for (final Locale lo : LocaleUtils.KNOWN_LOCALES) {%>
            <option value="<%= lo.getLanguage() %>"><%=lo.getDisplayName() %></option>
          <% } %>
          </select>
          <p><input type="checkbox" id="gwt-debug-delete-all" name="clearmessage" placeholder="info" value="on" /> ${messages['systemInfoEditorClearMessage']}</p>
          <input type="text" maxlength="36" id="gwt-debug-uuid" name="uuid" placeholder="${messages['systemInfoEditorPasskey']}" />
          <input type="submit" id="gwt-debug-submit" value="${messages['systemInfoEditorSubmit']}" />
        </form>
        <% if (request.getAttribute("isSuccess") != null) { %>
          <p>${messages['systemInfoEditorSentText']}</p>
        <% } %>
        <% if (request.getAttribute("isError") != null) { %>
          <p style="color:red">${messages['systemInfoEditorErrorText']}</p>
        <% } %>
      </div>
    </div>
  </div>
</body>
