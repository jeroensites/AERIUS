<%@page import="java.sql.Connection, nl.overheid.aerius.shared.constants.SharedConstantsEnum,
nl.overheid.aerius.db.common.ConstantRepository, nl.overheid.aerius.server.service.ServerPMF"%><%
Connection con = null;
try {
  con = ServerPMF.getInstance().getConnection();
  if (ConstantRepository.getBoolean(con, SharedConstantsEnum.COLLECT_FEEDBACK)) {
    final String collectorId = ConstantRepository.getString(con, SharedConstantsEnum.COLLECT_COLLECTOR_ID);
%><!-- Jira issue collector -->
<script type="text/javascript" src="https://aerius.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-dtzt95/b/6/c95134bc67d3a521bb3f4331beb9b804/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?jsI18nTransformer=migrated&locale=en-GB&collectorId=<%=collectorId%>"></script>
<!-- End Jira issue collector --><%
  }
} catch (final java.sql.SQLException e) {
  throw new IllegalArgumentException(e);
} finally {
  if (con != null) {
    try {
      con.close();
    } catch (final java.sql.SQLException e) { /* Eat error. */ }
  }
}
%>
