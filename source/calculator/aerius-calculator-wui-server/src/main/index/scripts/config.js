function AerConfig() {
  this.connectBaseUrl = "/api";
  this.geoserverBaseUrl = "/geoserver-calculator/wms";
  this.production = true;
  this.applicationVersion = "${project.version}_${buildDateTime}_${buildRevision}";
}
