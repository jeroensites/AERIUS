/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.configuration.AppConfigurationRepository;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.LocaleDBUtils;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Loads all language specific contexts into a static map.
 */
public class ContextLoader {
  private static final Logger LOGGER = LoggerFactory.getLogger(ContextLoader.class);

  private static final Map<Locale, ApplicationConfiguration> APP_CONFIGURARIONS = new HashMap<>();
  private static Locale defaultLocale;

  public static void initApplicationConfiguration(final PMF pmf) {
    defaultLocale = LocaleDBUtils.getDefaultLocale(pmf);
    try (Connection con = pmf.getConnection()) {
      final AeriusCustomer customer = AeriusCustomer.safeValueOf(ConstantRepository.getString(con, ConstantsEnum.CUSTOMER));

      for(final Locale locale: LocaleUtils.KNOWN_LOCALES) {
        final DBMessagesKey messagesKey = new DBMessagesKey(pmf.getProductType(), locale);

        APP_CONFIGURARIONS.put(locale, AppConfigurationRepository.getAppConfiguration(con, messagesKey, customer));
      }
    } catch (SQLException | AeriusException e) {
      LOGGER.error("Failed to initialize application configuration", e);
    }
  }

  public static ApplicationConfiguration getConfiguration(final Locale locale) {
    return APP_CONFIGURARIONS.getOrDefault(locale, APP_CONFIGURARIONS.get(defaultLocale));

  }
}
