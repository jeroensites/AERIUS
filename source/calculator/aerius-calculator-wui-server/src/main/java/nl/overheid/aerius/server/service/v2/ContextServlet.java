/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.server.service.v2;


import java.sql.SQLException;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import nl.overheid.aerius.server.service.AeriusSession;
import nl.overheid.aerius.server.service.ContextLoader;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.service.v2.ContextService;

/**
 * GWT-RPC servlet for application context information.
 */
public class ContextServlet extends RemoteServiceServlet implements ContextService {

  private static final long serialVersionUID = 1L;

  private final AeriusSession session;

  public ContextServlet(final AeriusSession session)  throws SQLException, AeriusException {
    this.session = session;
  }

  @Override
  public ApplicationConfiguration getContext() throws AeriusException {
    return ContextLoader.getConfiguration(session.getLocale());
  }

  @Override
  public void closeSession(final String lastCalculationKey) {
    if (session != null) {
      session.close();
    }
  }
}
