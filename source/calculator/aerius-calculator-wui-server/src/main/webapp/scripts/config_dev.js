function AerConfig() {
  this.connectBaseUrl = "/api/connect";
  this.geoserverBaseUrl = "/geoserver-calculator/wms";
  this.production = false;
  this.applicationVersion = "DEV";
}
