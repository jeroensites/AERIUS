class DepositionGraphElement extends HTMLElement {
  // numbers  (and colors) should match those defined in system.color_ranges in database
  static get COLORS_FOR_MAX_VALUES_DEPOSITION() {
    return [{
      "lowerBound": 0,
      "color": "#fffdb3"
    }, {
      "lowerBound": 1.07,
      "color": "#fde76a"
    }, {
      "lowerBound": 2.86,
      "color": "#feb66e"
    }, {
      "lowerBound": 5,
      "color": "#a5cc46"
    }, {
      "lowerBound": 7.14,
      "color": "#23a870"
    }, {
      "lowerBound": 10,
      "color": "#5a7a32"
    }, {
      "lowerBound": 15,
      "color": "#0093bd"
    }, {
      "lowerBound": 20,
      "color": "#0d75b5"
    }, {
      "lowerBound": 25,
      "color": "#6a70b1"
    }, {
      "lowerBound": 35.71,
      "color": "#304594"
    }, {
      "lowerBound": 71.43,
      "color": "#5e2c8f"
    }, {
      "lowerBound": 107.14,
      "color": "#3f2a84"
    } ,{
      "lowerBound": 142.86,
      "color": "#2a1612"
    }];
  }
  static get COLORS_FOR_MAX_VALUES_DIFFERENCE() {
    return [{
      "lowerBound": -Infinity,
      "color": "#507122"
    }, {
      "lowerBound": -20,
      "color": "#507122"
    }, {
      "lowerBound": -10,
      "color": "#738d4e"
    }, {
      "lowerBound": -5,
      "color": "#96aa7a"
    }, {
      "lowerBound": -2.86,
      "color": "#b9c6a7"
    }, {
      "lowerBound": -1.07,
      "color": "#dce3d3"
    }, {
      "lowerBound": 0,
      "color": "#d8d3e5"
    }, {
      "lowerBound": 1.07,
      "color": "#b1a9cb"
    }, {
      "lowerBound": 2.86,
      "color": "#8b7db0"
    }, {
      "lowerBound": 5,
      "color": "#645296"
    }, {
      "lowerBound": 10,
      "color": "#3d277c"
    }, {
      "lowerBound": 20,
      "color": "#3d277c"
    }];
  }

  constructor() {
    super();

    const MAX_NUMBER_OF_VALUES = 13;
    const WIDTH = 770;
    const HEIGHT = 280;
    const GRAPH_MARGIN_TOP = 50;
    const GRAPH_MARGIN_RIGHT = 50;
    const GRAPH_MARGIN_BOTTOM = 0;
    const GRAPH_MARGIN_LEFT = 50;

    this.maxNumberOfValues = MAX_NUMBER_OF_VALUES;
    this.colorsForMaxValues = DepositionGraphElement.COLORS_FOR_MAX_VALUES_DEPOSITION;

    this.d3Data = [];
    this.selectedLowerBounds = [];

    this.width = WIDTH;
    this.height = HEIGHT;

    this.xLabel = "";
    this.yLabel = "";

    this.selectionBars = [];

    this.root = this.attachShadow({ mode: 'open' });
    this.root.innerHTML = `
      <style>
        .svg {
          height: 320px;
          background: #ffffff;
          padding: 10px;
          padding-top: 30px;
          overflow: visible;
        }
        
        .graph-background {
          fill: #f3f3f3;
        }
        
        text {
          font-size: 13px;
          font-family: "Noto Sans TC";
        }

        .y-axis .tick > line {
          color: #ffffff;
        }

        .column {
          cursor: pointer;
          transition: stroke 0.15s ease-out;
          stroke-width: 2px;
          stroke: #222;
        }

        .column:hover {
          stroke-width: 2px;
        }
      </style>`;

    this.graphMargin = {
      top: GRAPH_MARGIN_TOP,
      right: GRAPH_MARGIN_RIGHT,
      bottom: GRAPH_MARGIN_BOTTOM,
      left: GRAPH_MARGIN_LEFT
    };

    this.svg = d3.create("svg")
      .classed("svg", true);
      
    this.root.host.onresize = e => this.resize();
    this.root.appendChild(this.svg.node());

    this.addGraphElementsToSvg();
    this.addLabelElementsToSvg();
  }

  isModeDeposition(mode) {
    return mode == "SITUATION_RESULT" || mode == "MAX_TEMPORARY_CONTRIBUTION";
  }

  updateColorRanges(mode) {
    this.colorsForMaxValues = this.isModeDeposition(mode)
      ? DepositionGraphElement.COLORS_FOR_MAX_VALUES_DEPOSITION
      : DepositionGraphElement.COLORS_FOR_MAX_VALUES_DIFFERENCE;
    this.drawBarGraph();
  }

  addGraphElementsToSvg() {
    let graph = this.svg.append("g")
      .classed("graph", true);


    // Background
    graph.append('rect')
      .classed("graph-background", true)
      .attr("x", this.graphMargin.left)
      .attr("y", this.graphMargin.top)
      .attr("width", this.width - this.graphMargin.left - this.graphMargin.right)
      .attr("height", this.height - this.graphMargin.top - this.graphMargin.bottom);


    // X-axis
    graph.append("g")
      .classed("x-axis", true)
      .style("font-size", "13px")
      .style("font-family", "Noto Sans TC");
      

    // Y-axis
    graph.append("g")
      .classed("y-axis", true)
      .style("font-size", "13px")
      .style("font-family", "Noto Sans TC");
    

    // Color bars
    graph.append("g")
      .classed("bar-container", true);


    // Select bars
    graph.append("g")
      .classed("select-bar-container", true);
  }

  addLabelElementsToSvg() {
    // X-axis label
    this.xLabel = this.svg.append("g")
      .append("text")
      .attr("x", this.graphMargin.left)
      .attr("y", this.height + 50);
    
    // Y-axis label
    this.yLabel = this.svg.append("g")
      .append("text")
      .attr("x", 0)
      .attr("y", 20);
  }

  xFormat(d) {
    // Set The various kinds of infinity to an empty string
    if (d == Infinity || d == -Infinity
      || d == "Infinity" || d == "-Infinity") {
      return "";
    }

    // Set the various kinds of 0 to an explit more-or-less-than-0
    if (d == "0" || d == 0) {
      if (this.isModeDeposition(this.mode)) {
        return "> 0";
      } else {
        return "< 0 >"
      }
    }

    return d;
  }

  drawBarGraph() {
    let me = this;
    this.svg.selectAll("g.bar-container").selectAll("rect").remove();
    this.svg.selectAll("g.select-bar-container").selectAll("rect").remove();

    let dataLength = this.d3Data.length;
    let barWidth = (this.width - (this.graphMargin.left + this.graphMargin.right)) / dataLength;
  
    let xValues = this.d3Data.map(graphData => graphData.lowerBound);
    xValues.push(Infinity);
    let yValues = this.d3Data.map(graphData => graphData.cartographicSurface / 10000);

    // X values
    let x = d3.scalePoint()
      .domain(xValues)
      .range([this.graphMargin.left, this.width - this.graphMargin.right]);
    let xAxis = d3.axisBottom(x)
      .tickFormat(d => this.xFormat(d));

    // Y values
    let y = d3.scaleLinear()
      .domain([0, d3.max(yValues) * 1.2])
      .range([this.height, (this.graphMargin.top + this.graphMargin.bottom)])
      .nice(); // Rounds the domain max value
    let yAxis = d3.axisLeft(y)
      .tickSize((this.graphMargin.left + this.graphMargin.right) - this.width)
      .tickFormat(this.getNumberFormat());


    // Draw x-axis
    let xAxisElement = this.svg.selectAll("g.x-axis")
      .transition()
      .attr("transform", "translate(0," + this.height + ")")
      .call(xAxis);

    // Remove last tick text (Infinity)
    let ticks = this.svg.selectAll("g.x-axis > .tick");
    ticks.filter(function(d, i) { return i == dataLength }).remove(); 

    // Draw y-axis
    let yAxisElement = this.svg.selectAll("g.y-axis")
      .transition()
      .attr("transform", "translate(50,0)")
      .call(yAxis);
    
    // Hide ticks and domains
    xAxisElement.selectAll(".tick")
      .selectAll("line")
      .attr("display", "none");
    xAxisElement.selectAll(".domain")
      .attr("display", "none");
    yAxisElement.selectAll(".domain")
      .attr("display", "none");


    // Draw bars
    let barContainer = this.svg.selectAll("g.bar-container");

    // Bars
    barContainer.selectAll("g")
      .data(this.d3Data)
      .enter()
      .append("rect")
      .attr("x", d => x(d.lowerBound))
      .attr("y", d => y(d.cartographicSurface / 10000))
      .attr("width", barWidth)
      .attr("height", d => y(this.graphMargin.bottom) - y(d.cartographicSurface / 10000))
      .attr("fill", d => {
        let range = this.colorsForMaxValues.find(colorForMaxValue => colorForMaxValue.lowerBound == d.lowerBound);
        return range == null ? "#fff" : range.color;
      });

    // Select bars
    let selectBarContainer = this.svg.selectAll("g.select-bar-container");

    this.selectionBars = selectBarContainer.selectAll("g")
      .data(this.d3Data)
      .enter()
      .append("rect")
      .attr("x", d => x(d.lowerBound))
      .attr("y", this.graphMargin.top)
      .attr("width", barWidth)
      .attr("height", this.height - this.graphMargin.top - this.graphMargin.bottom)
      .attr("fill-opacity", 0) // Only show outline (stroke)
      .attr("stroke", "#000000")
      .attr("stroke-width", 2)
      .attr("stroke-opacity", 0) // Hide by default
      .on("mouseover", function() {
        d3.select(this)
          .attr("stroke-opacity", 1);
      })
      .on("mouseout", function(d) {
        let lowerBound = parseFloat(d.lowerBound);
        if (!me.selectedLowerBounds.includes(lowerBound)) {
          d3.select(this)
            .attr("stroke-opacity", 0);
        }
      })
      .on("click", function(d) {
        let lowerBound = parseFloat(d.lowerBound);
        if (!me.selectedLowerBounds.includes(lowerBound)) { // Select
          me.selectedLowerBounds.push(lowerBound);

          d3.select(this)
            .attr("stroke-opacity", 1);

          me.root.host.dispatchEvent(new CustomEvent("onBarSelect", {
            detail: {
              lowerBound: lowerBound,
              upperBound: me.getUpperBound(me.d3Data.indexOf(d))
            }
          }));
        } else { // Deselect
          let i = me.selectedLowerBounds.indexOf(lowerBound)
          me.selectedLowerBounds.splice(i, 1);

          d3.select(this)
            .attr("stroke-opacity", 0);

          me.root.host.dispatchEvent(new CustomEvent("onBarDeselect", {
            detail: {
              lowerBound: lowerBound,
              upperBound: me.getUpperBound(me.d3Data.indexOf(d))
            }
          }));
        }
      });
  }

  getUpperBound(index) {
    if (index === this.d3Data.length - 1) {
      return Infinity;
    } else {
      return parseFloat(this.d3Data[index + 1].lowerBound);
    }
  }

  getNumberFormat() {
    let locale = d3.formatLocale({
      decimal: ",",
      thousands: ".",
      grouping: [3]
    });

    return locale.format(",");
  }

  resize() {
    this.drawBarGraph();
  }

  handleCreateGraphRequest(d3Data) {
    this.d3Data = JSON.parse(d3Data.replaceAll('`', '"'));

    this.drawBarGraph();
  }

  refreshSelectedRanges(selectArrayString) {
    this.selectedLowerBounds = [];
    let selectedLowerBoundsStringArray = selectArrayString.substring(1, selectArrayString.length - 1).split(", ");

    if (selectArrayString !== "[]") {
      selectedLowerBoundsStringArray
          .forEach((elem, idx) => this.selectedLowerBounds[idx] = parseFloat(elem));
    }

    let me = this;
    this.selectionBars.filter(function(d) {
      return me.selectedLowerBounds.includes(parseFloat(d.lowerBound));
    }).attr("stroke-opacity", 1);

    this.selectionBars.filter(function(d) {
      return !me.selectedLowerBounds.includes(parseFloat(d.lowerBound));
    }).attr("stroke-opacity", 0);
  }

  attributeChangedCallback(name, oldValue, newValue) {
    switch (name) {
      case 'xaxislabel':
        this.xLabel.html(newValue);
        break;
      case 'yaxislabel':
        this.yLabel.html(newValue);
        break;
      case 'data':
        this.handleCreateGraphRequest(newValue);
        break;
      case 'selectedranges':
        this.refreshSelectedRanges(newValue);
        break;
      case 'mode':
        this.mode = newValue;
        this.updateColorRanges(newValue);
        break;
    }
  }

  static get observedAttributes() {
    return ['xaxislabel', 'yaxislabel', 'data', 'selectedranges', 'mode'];
  }
}

customElements.define('deposition-graph-component', DepositionGraphElement);
