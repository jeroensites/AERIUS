<%@page import="java.util.Locale,nl.overheid.aerius.util.LocaleRequester,nl.overheid.aerius.server.i18n.AeriusMessages"
%><%
final Locale locale = LocaleRequester.initLocale(request, response);
request.getSession().setAttribute("messages", new AeriusMessages("${aerius.customer}", locale));
final String lang = locale.getLanguage();
final String errorPage = "500";
%><!doctype html>
<html lang="<%=lang%>">
<head>
<title><%=errorPage%> | AERIUS&reg;</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="gwt:property" content="locale=<%=lang%>">
<base href="${pageContext.request.contextPath}/">
<link rel="icon shortcut" type="image/x-icon" href="favicon.ico">
<link rel="stylesheet" href="fonts/noto-sans-tc-v10-latin-regular.css" type="text/css">
<%
  String title = errorPage;
  String text = new AeriusExceptionMessages(locale).getString(exception);
  boolean loading = false;%>
<%@ include file="/WEB-INF/jsp/splash.jsp" %>
