<%@page import="nl.overheid.aerius.server.util.ImageInliner"
%><img class="splash-rivm" src="<%=ImageInliner.inline("images/splash/rivm-logo-nederlands.svg")%>" alt="RIVM"></img>
    <p class="splash-attribution">${messages['splashAttribution']}</p>
    <div class="splash-provinces">
      <img src="<%=ImageInliner.inline("images/splash/province/Groningen.svg")%>" alt="Provincie Groningen"></img>
      <img src="<%=ImageInliner.inline("images/splash/province/Friesland.svg")%>" alt="Provincie Friesland"></img>
      <img src="<%=ImageInliner.inline("images/splash/province/Drenthe.svg")%>" alt="Provincie Drenthe"></img>
      <img src="<%=ImageInliner.inline("images/splash/province/Overijssel.svg")%>" alt="Provincie Overijssel"></img>
      <img src="<%=ImageInliner.inline("images/splash/province/Gelderland.svg")%>" alt="Provincie Gelderland"></img>
      <img src="<%=ImageInliner.inline("images/splash/province/Utrecht.svg")%>" alt="Provincie Utrecht"></img>
      <img src="<%=ImageInliner.inline("images/splash/province/Noord-Holland.svg")%>" alt="Provincie Noord-Holland"></img>
      <img src="<%=ImageInliner.inline("images/splash/province/Zuid-Holland.svg")%>" alt="Provincie Zuid-Holland"></img>
      <img src="<%=ImageInliner.inline("images/splash/province/Zeeland.svg")%>" alt="Provincie Zeeland"></img>
      <img src="<%=ImageInliner.inline("images/splash/province/Noord-Brabant.svg")%>" alt="Provincie Noord-Brabant"></img>
      <img src="<%=ImageInliner.inline("images/splash/province/Limburg.svg")%>" alt="Provincie Limburg"></img>
      <img src="<%=ImageInliner.inline("images/splash/province/Flevoland.svg")%>" alt="Provincie Flevoland"></img>
    </div>
