<%@page import="nl.overheid.aerius.server.util.ImageInliner"
%><p class="splash-attribution">${messages['splashAttribution']}</p>
    <div class="splash-uk">
      <img src="<%=ImageInliner.inline("images/splash/uk/logo-1-dept-efra.png")%>" alt="Department for Environment Food &amps; Rural Affairs"></img>
      <img src="<%=ImageInliner.inline("images/splash/uk/logo-2-dept-aera.png")%>" alt="Agriculture, Environment and Rural Affairs"></img>
      <img src="<%=ImageInliner.inline("images/splash/uk/logo-3-env-agency.png")%>" alt="Environment Agency"></img>
      <img src="<%=ImageInliner.inline("images/splash/uk/logo-4-niea.png")%>" alt="Northern Ireland Environment Agency"></img>
      <img src="<%=ImageInliner.inline("images/splash/uk/logo-5-sepa.png")%>" alt="Scottish Environment Protection Agency"></img>
      <img src="<%=ImageInliner.inline("images/splash/uk/logo-6-cncnrw.png")%>" alt="Natural Resources Wales"></img>
      <img src="<%=ImageInliner.inline("images/splash/uk/logo-7-naturescot.png")%>" alt="Scotland's Nature Agency"></img>
      <img src="<%=ImageInliner.inline("images/splash/uk/logo-8-natural-england.png")%>" alt="Natural England"></img>
    </div>
