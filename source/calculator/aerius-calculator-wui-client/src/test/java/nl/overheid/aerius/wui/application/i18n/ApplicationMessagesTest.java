/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Tests if the {@link ApplicationMessages} interface is in sync with the language specific properties files,
 * and if the properties files are in sync.
 */
class ApplicationMessagesTest {

  // Ignore these enum types as not all are relevant, deprecated or not yet supported.
  private static final Set<Class<?>> IGNORED_ENUMS = Set.of(Substance.class, Theme.class, CalculationType.class, EmissionResultKey.class);

  /**
   * Tests if all enums are present as key in the properties file.
   */
  @Test
  void testMissingEnumKeysFromInterface() throws IOException, URISyntaxException {
    final Map<String, Method> methods = getMethods();
    final Map<String, String> props = getLanguageProperties("");
    final List<String> missingKeys = new ArrayList<>();

    for (final Entry<String, Method> entry : methods.entrySet()) {
      final List<Class<?>> enumParams = Stream.of(entry.getValue().getParameterTypes()).filter(Class::isEnum).collect(Collectors.toList());
      if (!ignoreExceptions(enumParams)) {
        for (final String enumKey : combinations(List.of(), enumParams, 0)) {
          final String methodEnum = entry.getKey() + '[' + enumKey + ']';

          if (!props.containsKey(methodEnum)) {
            missingKeys.add(methodEnum);
          }
        }
      }
    }
    print("Missing Keys", missingKeys);
    assertEquals(List.of(), missingKeys, "ApplicationMessages.properties misses enum values.");
  }

  /**
   * Test is there are keys in the default properties file that are not present in the {@link ApplicationMessages} interface.
   */
  @Test
  void testObsoleteProperties() throws IOException, URISyntaxException {
    final Map<String, Method> methods = getMethods();
    final Map<String, String> props = getLanguageProperties("");
    final List<String> obsoleteKeys = new ArrayList<>();

    for (final Entry<String, String> prop : props.entrySet()) {
      final String key = prop.getKey();
      final int indexOf = key.indexOf('[');
      final String actualKey = indexOf > 0 ? key.substring(0, indexOf) : key;

      if (!methods.containsKey(actualKey)) {
        obsoleteKeys.add(actualKey);
      }
    }
    print("Obsolete Keys", obsoleteKeys);
    assertEquals(List.of(), obsoleteKeys, "ApplicationMessages.properties contains obsolete keys that are not used anymore.");
  }

  @ParameterizedTest
  @ValueSource(strings = {"en"})
  void testLanguagesMissingKeys(final String language) throws IOException, URISyntaxException {
    final Map<String, String> defaultProps = getLanguageProperties("");
    final Map<String, String> langProps = getLanguageProperties(language);
    assertLanguageKeys(defaultProps, langProps, language, "language " + language + " misses keys present in the default language file.");
  }

  @ParameterizedTest
  @ValueSource(strings = {"en"})
  void testLanguagesObsoleteKeys(final String language) throws IOException, URISyntaxException {
    final Map<String, String> langProps = getLanguageProperties(language);
    final Map<String, String> defaultProps = getLanguageProperties("");
    assertLanguageKeys(langProps, defaultProps, language, "language " + language + " contains obsolete keys that are not used anymore.");
  }

  @ParameterizedTest
  @ValueSource(strings = {"", "en"})
  void testSorting(final String language) throws IOException, URISyntaxException {
    final List<String> content = readLanguageProperties(language);
    final List<String> sorted = content.stream().sorted().collect(Collectors.toList());
    printUnsortedKeys(language, content, sorted);
    assertEquals(sorted, content, "Language property " + language + " file should be sorted.");
  }

  private void printUnsortedKeys(final String language, final List<String> content, final List<String> sorted) {
    final List<String> misMatches = new ArrayList<>();

    for (int i = 0; i < sorted.size(); i++) {
      if (!sorted.get(i).equals(content.get(i))) {
        misMatches.add("Expected: " + keyMap(sorted.get(i)) + ", found: " + keyMap(content.get(i)));
      }
      if (misMatches.size() > 10) {
        // If we have more than a number of mismatches we can break here, to avoid getting a very large list that is probably not helpfull.
        break;
      }
    }
    if (!misMatches.isEmpty()) {
      print("Sorting mismatch for language '" + language + "'", misMatches);
    }
  }

  private static boolean ignoreExceptions(final List<Class<?>> enumParams) {
    return enumParams.stream().anyMatch(e -> IGNORED_ENUMS.contains(e));
  }

  private static List<String> combinations(final List<String> enums, final List<Class<?>> enumParameters, final int idx) {
    final boolean last = idx == enumParameters.size();
    if (last) {
      return enums;
    }
    final Class<?> enumParam = enumParameters.get(idx);
    final List<String> newEnumKeys = new ArrayList<>();

    for (final Object enumValue : enumParam.getEnumConstants()) {
      final String name = ((Enum<?>) enumValue).name();

      if (enums.isEmpty()) {
        newEnumKeys.add(name);
      } else {
        for (final String enumKey : enums) {
          newEnumKeys.add(enumKey + '|' + name);
        }
      }
    }
    return combinations(newEnumKeys, enumParameters, idx + 1);
  }

  private static void assertLanguageKeys(final Map<String, String> referenceProperties, final Map<String, String> testProperties,
      final String language, final String message) {
    final List<String> missingKeys = new ArrayList<>();

    for (final Entry<String, String> prop : referenceProperties.entrySet()) {
      final String key = prop.getKey();

      if (!testProperties.containsKey(key)) {
        missingKeys.add(key);
      }
    }
    print("Missing Keys for " + language, missingKeys);
    assertEquals(List.of(), missingKeys, "ApplicationMessages_" + language + ".properties " + message);
  }

  private static Map<String, Method> getMethods() {
    return Stream.of(ApplicationMessages.class.getMethods()).collect(Collectors.toMap(m -> m.getName(), Function.identity()));
  }

  private static Map<String, String> getLanguageProperties(final String language) throws IOException, URISyntaxException {
    final List<String> content = readLanguageProperties(language);
    return content.stream().filter(s -> !s.trim().isBlank()).collect(Collectors.toMap(ApplicationMessagesTest::keyMap, Function.identity()));
  }

  private static List<String> readLanguageProperties(final String language) throws IOException, URISyntaxException {
    final String fileName = ApplicationMessages.class.getSimpleName() + (language.isEmpty() ? "" : "_" + language) + ".properties";
    final List<String> content = Files.readAllLines(Path.of(ApplicationMessagesTest.class.getResource(fileName).toURI()));
    return content;
  }

  private static String keyMap(final String s) {
    final int optionsIndex = s.indexOf(']'); // if blocks used in key, it can contain an =, and therefore split on = won't work.
    final int equalsIndex = s.indexOf("=");
    return s.substring(0, optionsIndex > 0 && optionsIndex < equalsIndex ? optionsIndex + 1 : s.indexOf('=')).trim();
  }

  private static void print(final String title, final List<String> keys) {
    if (keys.isEmpty()) {
      return;
    }
    final String sep = "-".repeat(10);
    System.out.println(sep + title + sep);
    keys.stream().sorted().forEach(s -> System.out.println(s));
  }
}
