/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.shipping.maritime.mooring;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;

import elemental2.core.JsArray;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.MooringMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MooringMaritimeShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MooringMaritimeShippingType;
import nl.overheid.aerius.wui.application.domain.source.util.EmissionSourceFeatureUtil;

@Component(name = "aer-mooring-maritime-shipping-detail-editor",
components = {
    MooringMaritimeShippingDetailEditorStandardComponent.class,
    MooringMaritimeShippingDetailEditorCustomComponent.class,
    ValidationBehaviour.class
})
public class MooringMaritimeShippingDetailEditorComponent extends ErrorWarningValidator {
  @Prop MooringMaritimeShippingESFeature source;
  @Prop MooringMaritimeShipping subSource;

  @Data MooringMaritimeShippingType toggleSelect;

  @Watch(value = "subSource", isImmediate = true)
  public void onValueChange(final Integer neww, final Integer old) {
    if (subSource != null) {
      toggleSelect = subSource.getMooringMaritimeShippingType();
    }
  }

  @JsMethod
  protected boolean isSourceAvailable() {
    return subSource != null;
  }

  @JsMethod
  public void setToggle(final MooringMaritimeShippingType type) {
    toggleSelect = type;
    final JsArray<MooringMaritimeShipping> shippings = source.getSubSources();
    shippings.splice(shippings.indexOf(subSource), 1, EmissionSourceFeatureUtil.createMooringMaritimeShippingType(type));
  }
}
