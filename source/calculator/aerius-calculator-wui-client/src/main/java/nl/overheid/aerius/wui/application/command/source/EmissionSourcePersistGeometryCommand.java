/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.command.source;

import ol.geom.Geometry;

import nl.aerius.wui.command.SimpleGenericCommand;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.source.EmissionSourcePersistGeometryEvent;

public class EmissionSourcePersistGeometryCommand extends SimpleGenericCommand<Geometry, EmissionSourcePersistGeometryEvent> {
  private final EmissionSourceFeature target;

  public EmissionSourcePersistGeometryCommand(final Geometry value, final EmissionSourceFeature target) {
    super(value);
    this.target = target;
  }

  public EmissionSourceFeature getTarget() {
    return target;
  }

  @Override
  protected EmissionSourcePersistGeometryEvent createEvent(final Geometry value) {
    return new EmissionSourcePersistGeometryEvent(value);
  }
}
