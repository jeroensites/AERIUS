/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.calculation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.google.gwt.user.client.DOM;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;

public class CalculationJobContext {
  private String inputHash = null;

  private String internalId = null;
  private String calculationCode = null;

  private CalculationInfo calculationInfo = null;
  private ResultSummaryContext resultContext = null;

  /**
   * Need this primarily for the.... name
   */
  private @JsProperty List<SituationHandle> situations = new ArrayList<>();

  public CalculationJobContext() {
    this.internalId = DOM.createUniqueId();
    this.calculationInfo = CalculationInfo.create();
    this.resultContext = new ResultSummaryContext();
  }

  public String getInternalId() {
    return internalId;
  }

  public String getCalculationCode() {
    return calculationCode;
  }

  public void setCalculationCode(final String calculationCode) {
    this.calculationCode = calculationCode;
  }

  private JobState getJobState() {
    return Optional.ofNullable(calculationInfo)
        .map(v -> v.getJobProgress())
        .map(v -> v.getState())
        .orElse(JobState.UNDEFINED);
  }

  @JsMethod
  public boolean isNone() {
    return getJobState() == JobState.UNDEFINED;
  }

  @JsMethod
  public boolean isCalculationComplete() {
    return getJobState() == JobState.COMPLETED;
  }

  @JsMethod
  public boolean isCalculationFinalState() {
    return Optional.of(getJobState())
        .map(v -> v.isFinalState())
        .orElseThrow(() -> new RuntimeException("Could not determine job state finality."));
  }

  @JsMethod
  public boolean isCalculating() {
    return getJobState() == JobState.RUNNING;
  }

  public void setCalculationInfo(final CalculationInfo calculationInfo) {
    this.calculationInfo = calculationInfo;
  }

  public CalculationInfo getCalculationInfo() {
    return calculationInfo;
  }

  public ResultSummaryContext getResultContext() {
    return resultContext;
  }

  public List<SituationHandle> getSituations() {
    return situations;
  }

  public void setSituations(final List<SituationHandle> situations) {
    this.situations = situations;
  }

  public String getInputHash() {
    return inputHash;
  }

  public void setInputHash(final String inputHash) {
    this.inputHash = inputHash;
  }
}
