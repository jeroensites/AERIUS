/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.info;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.wui.application.domain.result.EmissionResults;

/**
 * Very basic bean containing emission results info.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class EmissionResultInfo {

  private @JsProperty EmissionResults backgroundEmissionResults;
  private @JsProperty JsPropertyMap<Object> emissionResultsByCode;

  private int year;

  public final @JsOverlay int getYear() {
    return year;
  }

  public final @JsOverlay EmissionResults getBackgroundEmissionResults() {
    return backgroundEmissionResults;
  }

  public final @JsOverlay EmissionResults getEmissionResults(final String situationId) {
    return emissionResultsByCode.has(situationId)
        ? (EmissionResults) Js.asAny(emissionResultsByCode.get(situationId)) : null;
  }

  public final @JsOverlay boolean hasEmissionResults() {
    return emissionResultsByCode != null;
  }

}
