/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.dev;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.command.Command;
import nl.aerius.wui.command.PlaceChangeCommand;
import nl.aerius.wui.command.SimpleCommand;
import nl.aerius.wui.command.SimpleGenericCommand;
import nl.aerius.wui.dev.DevelopmentObserver;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.PlaceChangeEvent;
import nl.aerius.wui.event.SimpleGenericEvent;
import nl.overheid.aerius.geo.command.LayerAddedCommand;
import nl.overheid.aerius.geo.command.LayerRemovedCommand;
import nl.overheid.aerius.geo.event.LayerAddedEvent;
import nl.overheid.aerius.geo.event.LayerRemovedEvent;
import nl.overheid.aerius.wui.application.command.ChangeThemeCommand;
import nl.overheid.aerius.wui.application.command.RegisterFileCommand;
import nl.overheid.aerius.wui.application.command.misc.ToggleNotificationDisplayCommand;
import nl.overheid.aerius.wui.application.command.misc.ToggleSelectorMenuCommand;
import nl.overheid.aerius.wui.application.command.situation.SwitchSituationCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCancelCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSaveCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureToggleSelectCommand;

@Singleton
public class ApplicationDevelopmentObserver implements DevelopmentObserver {
  private static final DevelopmentObserverEventBinder EVENT_BINDER = GWT.create(DevelopmentObserverEventBinder.class);

  interface DevelopmentObserverEventBinder extends EventBinder<ApplicationDevelopmentObserver> {}

  @Inject
  public ApplicationDevelopmentObserver(final EventBus eventBus) {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @SuppressWarnings("rawtypes")
  @EventHandler(handles = { ToggleNotificationDisplayCommand.class })
  public void onCommand(final Command c) {
    log(c.getClass().getSimpleName());
  }

  @SuppressWarnings("rawtypes")
  @EventHandler(handles = { EmissionSourceEditCancelCommand.class, EmissionSourceEditSaveCommand.class })
  public void onSimpleCommand(final SimpleCommand c) {
    log(c.getClass().getSimpleName());
  }

  @SuppressWarnings("rawtypes")
  @EventHandler(handles = { LayerAddedCommand.class, LayerRemovedCommand.class, SwitchSituationCommand.class, ChangeThemeCommand.class })
  public void onSimpleGenericCommand(final SimpleGenericCommand c) {
    log(c.getClass().getSimpleName(), c.getValue());
  }

  @SuppressWarnings("rawtypes")
  @EventHandler(handles = { LayerAddedEvent.class, LayerRemovedEvent.class, RegisterFileCommand.class,
      EmissionSourceFeatureToggleSelectCommand.class, ToggleSelectorMenuCommand.class })
  public void onSimpleGenericEvent(final SimpleGenericEvent e) {
    log(e.getClass().getSimpleName(), e.getValue());
  }

  @EventHandler
  public void onPlaceChangeCommand(final PlaceChangeCommand e) {
    log("PlaceChangeCommand", e.getValue());
  }

  @EventHandler
  public void onPlaceChangeEvent(final PlaceChangeEvent e) {
    log("PlaceChangeEvent", e.getValue());
  }

  private void log(final String origin) {
    logRaw("[" + origin + "]");
  }

  private void log(final String origin, final Object val) {
    logRaw("[" + origin + "] " + String.valueOf(val));
  }

  private void logRaw(final String string) {
    GWTProd.log(string);
  }
}
