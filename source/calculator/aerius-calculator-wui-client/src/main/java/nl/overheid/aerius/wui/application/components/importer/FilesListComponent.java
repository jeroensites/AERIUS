/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.ui.main.VerticalCollapseGroup;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-files", components = {
    VerticalCollapseGroup.class,
    VerticalCollapse.class,
    FileComponent.class
})
public class FilesListComponent extends BasicVueComponent implements IsVueComponent {
  @Inject @Data ApplicationContext applicationContext;

  @Prop
  @JsProperty List<FileUploadStatus> files;

  @Prop boolean hasActiveSituation;
  @Prop boolean isCalculationPointsImport;
  @Prop SituationContext activeSituation;

  @Data FileUploadStatus selectedFileToImportIntoActiveSituation;

  @Watch(value = "files", isImmediate = true, isDeep = true)
  public void onFilesUpdate() {
    selectedFileToImportIntoActiveSituation = getFirstFile();
    vue().$emit("hasFileListErrors", this.hasFileListErrors());
  }

  @JsMethod
  public FileUploadStatus getFirstFile() {
    return files.isEmpty() ? null : updateAndGetFiles().get(0);
  }

  @JsMethod
  public List<FileUploadStatus> updateAndGetCalculationPointFiles() {
    final List<FileUploadStatus> fileUploadStatuses = updateAndGetFiles();
    return fileUploadStatuses.stream()
        .filter(fs -> fs.isCalculationPointStatus())
        .collect(Collectors.toList());
  }

  @JsMethod
  public List<FileUploadStatus> updateAndGetFiles() {
    if (files.isEmpty()) {
      return Collections.emptyList();
    }

    this.updateFiles();

    return files;
  }

  private void updateFiles() {
    if (hasActiveSituation) {
      this.files.stream().forEach(file -> {
        if (file.getSituationType() == SituationType.UNKNOWN) {
          file.setSituationType(activeSituation.getType());
        }
      });
    } else {
      this.files.stream().forEach(file -> {
        if (file.getSituationType() == SituationType.UNKNOWN) {
          file.setSituationType(SituationType.PROPOSED);
        }
      });
    }
  }

  @Emit
  @JsMethod
  public void displayErrors() {
  }

  @Emit
  @JsMethod
  public void displayWarnings() {
  }

  @JsMethod
  public void selectFileToImportIntoActiveSituation(final FileUploadStatus file) {
    selectedFileToImportIntoActiveSituation = file;
    vue().$emit("selectFileToImportIntoActiveSituation", file);
  }

  @JsMethod
  public void removeFile(final FileUploadStatus file) {
    vue().$emit("removeFile", file);
  }

  @JsMethod
  public boolean hasTooManySituations() {
    if (hasActiveSituation) {
      final Map<String, List<FileUploadStatus>> situationsGroupedByFileName = files.stream()
          .collect(Collectors.groupingBy(file -> file.getFile().name));

      return situationsGroupedByFileName.size() > getMaxNumberOfSituations();
    } else {
      return files.stream().filter(fus -> !fus.isCalculationPointStatus()).count() > getMaxNumberOfSituations();
    }
  }

  @JsMethod
  public int getMaxNumberOfSituations() {
    return applicationContext.getMaxNumberOfSituations(hasActiveSituation);
  }

  @JsMethod
  public boolean hasDifferentSituationType() {
    if (selectedFileToImportIntoActiveSituation == null || activeSituation == null) {
      return false;
    }

    return selectedFileToImportIntoActiveSituation.getSituationType() != activeSituation.getType()
        && selectedFileToImportIntoActiveSituation.getSituationType() != null;
  }

  @JsMethod
  public boolean hasTooManyReferenceSituations() {
    return files.stream().filter(file -> file.getSituationType() == SituationType.REFERENCE)
        .count() > applicationContext.getMaxNumberOfSituationsForType(SituationType.REFERENCE);
  }

  @JsMethod
  public boolean hasTooManyNettingSituations() {
    return files.stream().filter(file -> file.getSituationType() == SituationType.NETTING)
        .count() > applicationContext.getMaxNumberOfSituationsForType(SituationType.NETTING);
  }

  public boolean hasFileListErrors() {
    return this.hasTooManySituations() || this.hasTooManyReferenceSituations() || this.hasTooManyNettingSituations();
  }

}
