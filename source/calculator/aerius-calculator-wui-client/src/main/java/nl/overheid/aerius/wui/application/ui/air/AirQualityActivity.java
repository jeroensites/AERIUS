/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.air;

import com.google.gwt.event.dom.client.DragEnterEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.ResettableEventBus;

import nl.aerius.wui.activity.DelegableActivity;
import nl.aerius.wui.command.PlaceChangeCommand;
import nl.aerius.wui.place.Place;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.vue.activity.AbstractVueActivity;
import nl.overheid.aerius.wui.application.command.UserImportRequestedCommand;
import nl.overheid.aerius.wui.application.components.nav.NavigationItem;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.AirQualityPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.ResultPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.ScenarioPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourcePlace;

public class AirQualityActivity extends AbstractVueActivity<AirQualityPresenter, AirQualityView, AirQualityViewFactory>
    implements AirQualityPresenter, DelegableActivity {

  @Inject PlaceController placeController;

  private final AirQualityActivityManager delegator;

  private HandlerRegistration dragRegister;

  @Inject
  public AirQualityActivity(final AirQualityActivityManager delegator) {
    super(AirQualityViewFactory.get());

    this.delegator = delegator;
  }

  @Override
  public void onStart() {
    dragRegister = RootPanel.get().addDomHandler(event -> eventBus.fireEvent(new UserImportRequestedCommand()), DragEnterEvent.getType());
  }

  @Override
  public void onStop() {
    dragRegister.removeHandler();
  }

  @Override
  public AirQualityPresenter getPresenter() {
    return this;
  }

  @Override
  public boolean delegate(final ResettableEventBus eventBus, final PlaceChangeCommand c) {
    final boolean delegated = delegator.delegate(eventBus, c.getValue(), c::setRedirect);

    return delegated;
  }

  @Override
  public void setView(final Object view) {
  }

  @Override
  public void setView(final AirQualityView view) {
    delegator.setView(view);

    final NavigationContext nav = new NavigationContext();

    nav.addNavigationItem(NavigationItem.createPlaceItem(M.messages().menuScenario(), "icon-menu-settings",
        p -> p instanceof ScenarioPlace, placeController, new ScenarioPlace()));
//    nav.add(NavigationItem.createPlaceItem(M.messages().themeAirQualityScenario(), R.images().iconPreferences(),
//        p -> p instanceof ScenarioPlace, placeController, new ScenarioPlace()));

    nav.addNavigationItem(NavigationItem.createPlaceItem(M.messages().menuSources(), "icon-menu-emission-sources",
        p -> p instanceof SourcePlace, () -> false, placeController, new SourcePlace()));

    // TODO Replace icon-menu-help
    nav.addNavigationItem(NavigationItem.createPlaceItem(M.messages().menuCalculationPoints(), "icon-menu-help",
        p -> p instanceof CalculationPointPlace, () -> false, placeController, new CalculationPointPlace()));

    // TODO Replace icon-menu-help
//    nav.addNavigationItem(NavigationItem.createPlaceItem(M.messages().themeAirQualityMeasures(), "icon-menu-help",
//        p -> p instanceof MeasurePlace, () -> false, placeController, new MeasurePlace()));

//    nav.add(NavigationItem.createPlaceItem(M.messages().themeAirQualityPreferences(), R.images().iconPreferences(),
//        p -> p instanceof PreferencePlace, placeController, new PreferencePlace()));

    nav.addNavigationItem(NavigationItem.createPlaceItem(M.messages().menuResults(), "icon-menu-results",
        p -> p instanceof ResultPlace, () -> false, placeController, new ResultPlace()));

//    nav.addNavigationItem(NavigationItem.createPlaceItem(M.messages().themeAirQualityLayers(), "icon-menu-maplayers",
//        p -> p instanceof LayerPlace, placeController, new LayerPlace()));

    view.setNavigation(nav);
  }

  @Override
  public boolean isDelegable(final Place place) {
    return place instanceof AirQualityPlace;
  }

}
