/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics.adms;

import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsProperty;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.adms.ADMSCharacteristicsEditorValidators.ADMSCharacteristicsEditorValidations;
import nl.overheid.aerius.wui.application.components.source.characteristics.building.BuildingSelectorComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.outflow.OutflowEditorComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

@Component(customizeOptions = {
    ADMSCharacteristicsEditorValidators.class
}, directives = {
    ValidateDirective.class
}, components = {
    CollapsiblePanel.class,
    LabeledInputComponent.class,
    BuildingSelectorComponent.class,
    OutflowEditorComponent.class,
    ValidationBehaviour.class,
    VerticalCollapse.class,
})
public class ADMSCharacteristicsEditorComponent extends ErrorWarningValidator
    implements HasCreated, HasValidators<ADMSCharacteristicsEditorValidations> {
  @Prop EventBus eventBus;
  @Prop EmissionSourceFeature source;

  // Validations
  @JsProperty(name = "$v") ADMSCharacteristicsEditorValidations validation;

  @Data ADMSCharacteristics characteristics;

  @Inject @Data ApplicationContext applicationContext;

  @Override
  @Computed
  public ADMSCharacteristicsEditorValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Watch(value = "characteristics", isImmediate = true)
  public void onCharacteristicsChange(final ADMSCharacteristics neww) {
    if (neww == null) {
      return;
    }

  }

  @Watch(value = "source", isImmediate = true)
  public void watchSource() {
    characteristics = source == null ? null : Js.uncheckedCast(source.getCharacteristics());
  }

  @Computed
  public boolean getBuildingInfluence() {
    return Optional.ofNullable(characteristics)
        .map(v -> v.isBuildingInfluence())
        .orElse(false);
  }
}
