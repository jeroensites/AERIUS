/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.info;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.ImaerConstants;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.command.HabitatTypeHoverActiveCommand;
import nl.overheid.aerius.wui.application.command.HabitatTypeHoverInactiveCommand;
import nl.overheid.aerius.wui.application.command.HabitatTypeSelectToggleCommand;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSelectionContext;
import nl.overheid.aerius.wui.application.domain.info.HabitatInfo;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class,
    BasicInfoRowComponent.class
})
public class HabitatTypeComponent extends BasicVueComponent implements IsVueComponent {
  @Prop EventBus eventBus;
  @Prop @JsProperty HabitatInfo[] habitatTypeInfo;

  @Inject @Data ResultSelectionContext context;

  @JsMethod
  public void clear() {}

  @JsMethod
  public double getCriticalLevel(final HabitatInfo habitatType) {
    return habitatType.getCriticalLevels().getEmissionResultValue(EmissionResultKey.NOXNH3_DEPOSITION);
  }

  @JsMethod
  public boolean isSelected(final HabitatInfo info) {
    return context.getSelectedHabitatTypes().contains(String.valueOf(info.getId()));
  }

  @JsMethod
  public double getOverlap(final HabitatInfo habitatInfo) {
    return habitatInfo.getCartographicSurface() / ImaerConstants.M2_TO_HA;
  }

  @JsMethod
  public void rowToggle(final HabitatInfo row) {
    final String habitatId = String.valueOf(row.getId());
    eventBus.fireEvent(new HabitatTypeSelectToggleCommand(habitatId));
  }

  @JsMethod
  public void showHabitatInfoPanel(final boolean show, final HabitatInfo habitatInfo) {
    if (show) {
      eventBus.fireEvent(new HabitatTypeHoverActiveCommand(String.valueOf(habitatInfo.getId())));
    } else {
      eventBus.fireEvent(new HabitatTypeHoverInactiveCommand(String.valueOf(habitatInfo.getId())));
    }
  }

}
