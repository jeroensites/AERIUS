/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.maritime;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

/**
 * Client side implementation of CustomMooringMaritimeShipping props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomMooringMaritimeShipping extends MooringMaritimeShipping {

  private CustomMaritimeShippingEmissionProperties emissionProperties;

  public static final @JsOverlay CustomMooringMaritimeShipping create() {
    final CustomMooringMaritimeShipping props = new CustomMooringMaritimeShipping();
    MooringMaritimeShipping.init(props, MooringMaritimeShippingType.CUSTOM);
    props.setEmissionProperties(CustomMaritimeShippingEmissionProperties.create());
    return props;
  }

  public final @JsOverlay CustomMaritimeShippingEmissionProperties getEmissionProperties() {
    return emissionProperties;
  }

  public final @JsOverlay void setEmissionProperties(final CustomMaritimeShippingEmissionProperties emissionProperties) {
    this.emissionProperties = emissionProperties;
  }

}
