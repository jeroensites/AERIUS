/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;

import ol.Feature;
import ol.OLFactory;
import ol.geom.LineString;
import ol.style.Fill;
import ol.style.Stroke;
import ol.style.Style;

import nl.overheid.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.geo.domain.LayerOptions;
import nl.overheid.aerius.geo.domain.legend.Legend;
import nl.overheid.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.config.AppThemeConfiguration;
import nl.overheid.aerius.shared.constants.DrivingSide;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.wui.application.context.EmissionSourceListContext;
import nl.overheid.aerius.wui.application.context.RoadNetworkContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadBarrierStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadLayerStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadMaxSpeedStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadStagnationStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadTrafficVolumeStyle;
import nl.overheid.aerius.wui.application.geo.layers.source.road.RoadTypeStyle;
import nl.overheid.aerius.wui.application.i18n.M;

public class RoadSourceGeometryLayer extends SourceGeometryLayer {

  public static final int DEFAULT_ROAD_WIDTH = 4;

  private static final Stroke GRAY_STROKE = OLFactory.createStroke(OLFactory.createColor(128, 128, 128, 1), 2);
  private static final Stroke THEME_STROKE = OLFactory.createStroke(OLFactory.createColor(24, 124, 178, 1), 2);

  private static final Fill WHITE_FILL = OLFactory.createFill(OLFactory.createColor(255, 255, 255, 1));

  private static final Style ROAD_SELECTED_STYLE = OLFactory.createStyle(WHITE_FILL, THEME_STROKE);
  private static final Style ROAD_HIGHLIGHTED_STYLE = OLFactory.createStyle(WHITE_FILL, GRAY_STROKE);
  private static final double ROAD_SELECTION_OFFSET_DISTANCE = 8.0;

  private final Map<RoadLayerStyle, RoadStyle> roadStyles = new HashMap<>();

  private @Inject EmissionSourceListContext selectionContext;
  private @Inject RoadNetworkContext roadNetworkContext;

  private RoadLayerStyle roadLayerType = RoadLayerStyle.ROAD_TYPE;
  private VehicleType vehicleType = VehicleType.LIGHT_TRAFFIC;

  @Inject
  public RoadSourceGeometryLayer(final EventBus eventBus, @Assisted final AppThemeConfiguration appThemeConfiguration,
      @Assisted DrivingSide drivingSide, @Assisted final int zIndex) {
    super(new LayerInfo(), eventBus, zIndex);

    roadStyles.put(RoadLayerStyle.ROAD_TYPE, new RoadTypeStyle());
    roadStyles.put(RoadLayerStyle.MAX_SPEED, new RoadMaxSpeedStyle(appThemeConfiguration));
    roadStyles.put(RoadLayerStyle.STAGNATION, new RoadStagnationStyle());
    roadStyles.put(RoadLayerStyle.TRAFFIC_VOLUME, new RoadTrafficVolumeStyle(drivingSide));
    roadStyles.put(RoadLayerStyle.ROAD_BARRIER, new RoadBarrierStyle());

    info.setName(getClass().getCanonicalName());
    info.setTitle(M.messages().esRoadNetwork());
    info.setOptions(getLayerOptions());
    info.setLegend(getLegend());
  }

  private void setRoadLayerType(final RoadLayerStyle roadLayerType) {
    this.roadLayerType = roadLayerType;
    info.setOptions(getLayerOptions());
    info.setLegend(getLegend());
    asLayer().getSource().changed();
  }

  public void setVehicleType(final VehicleType vehicleType) {
    this.vehicleType = vehicleType;
    asLayer().getSource().changed();
  }

  public Legend getLegend() {
    return roadStyles.get(roadLayerType).getLegend();
  }

  private LayerOptions[] getLayerOptions() {

    final List<LayerOptions> options = new ArrayList<>();
    options.add(LayerOptions.createFromEnum(RoadLayerStyle.values(),
        ls -> M.messages().layerRoad(ls),
        v -> this.setRoadLayerType(v), roadLayerType));
    if (roadLayerType != null && roadLayerType.dependsOnVehicleType()) {
      options.add(LayerOptions.createFromEnum(VehicleType.values(),
          vt -> M.messages().esStandardRoadVehicleType(vt),
          v -> this.setVehicleType(v), vehicleType));
    }

    return options.toArray(new LayerOptions[0]);
  }

  @Override
  protected Style[] getStyle(final Feature feature, final double resolution) {
    final EmissionSourceFeature source = (EmissionSourceFeature) feature;

    if (source.getId().equals(getGeometryEditingFeatureId())) {
      return NO_RENDERING;
    }

    if (!RoadEmissionSourceTypes.isRoadSource(source)) {
      return NO_RENDERING;
    }

    final List<Style> baseRoadStyle = new ArrayList<>(roadStyles.get(roadLayerType).getStyle((RoadESFeature) source, vehicleType, resolution));

    if (selectionContext.isSelected(source)) {
      final Style roadSelectedStyle = ROAD_SELECTED_STYLE.clone();
      roadSelectedStyle
          .setGeometry(OL3GeometryUtil.outlinePolygonOfLineString((LineString) source.getGeometry(), ROAD_SELECTION_OFFSET_DISTANCE * resolution));
      baseRoadStyle.add(roadSelectedStyle);
    } else if (roadNetworkContext.isHighlighted(source)) {
      final Style roadHighlightedStyle = ROAD_HIGHLIGHTED_STYLE.clone();
      roadHighlightedStyle
          .setGeometry(OL3GeometryUtil.outlinePolygonOfLineString((LineString) source.getGeometry(), ROAD_SELECTION_OFFSET_DISTANCE * resolution));
      baseRoadStyle.add(roadHighlightedStyle);
    }

    return baseRoadStyle.toArray(new Style[0]);
  }

}
