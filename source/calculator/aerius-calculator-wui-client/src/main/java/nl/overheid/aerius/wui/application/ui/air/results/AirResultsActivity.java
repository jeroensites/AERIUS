/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.air.results;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.application.components.map.MapComponentFactory;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.ui.air.AirQualitySubActivity;
import nl.overheid.aerius.wui.application.ui.air.AirQualityView;

public class AirResultsActivity extends BasicEventComponent implements AirQualitySubActivity {
  @Inject PlaceController placeController;
  @Inject CalculationJobContext calculationContext;

  @Inject
  public AirResultsActivity(@Assisted final AirQualityView view) {
    view.setComponent(AirResultsViewFactory.get(), this);
    view.setMain(MapComponentFactory.get());
  }
}
