/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.input.suggest;

import jsinterop.annotations.JsType;

@JsType
public class SuggestListBoxData {

  private String code;
  private String name;
  private String description;
  private boolean stackable;

  public SuggestListBoxData(final String code, final String name, final String description, final boolean stackable) {
    this.code = code;
    this.name = name;
    this.description = description;
    this.stackable = stackable;
  }

  public String getCode() {
    return code;
  }

  public void setCode(final String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public boolean isStackable() {
    return stackable;
  }

  public void setStackable(final boolean stackable) {
    this.stackable = stackable;
  }
}
