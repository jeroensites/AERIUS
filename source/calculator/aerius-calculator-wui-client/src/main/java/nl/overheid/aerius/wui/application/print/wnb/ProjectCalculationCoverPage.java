/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.wnb;

import java.util.LinkedHashMap;
import java.util.Map;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component
public class ProjectCalculationCoverPage extends BasicVueComponent implements IsVueComponent {

  @Computed
  public Map<String, String> chapters() {
    final LinkedHashMap<String, String> chapters = new LinkedHashMap<>();
    chapters.put(Overview.ID, M.messages().pdfProjectCalculationCoverPageTocOverview());
    chapters.put(SituationSummaries.ID, M.messages().pdfProjectCalculationCoverPageTocSituationsSummary());
    chapters.put(Results.ID, M.messages().pdfProjectCalculationCoverPageTocResults());
    chapters.put(EmissionSources.ID, M.messages().pdfProjectCalculationCoverPageTocEmissionSourceDetails());
    return chapters;
  }
}
