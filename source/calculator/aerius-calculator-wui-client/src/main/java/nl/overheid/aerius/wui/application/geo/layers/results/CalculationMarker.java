/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.results;

import java.util.HashSet;
import java.util.Set;

import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;

/**
 * CalculationMarkers are related to {@link ResultStatisticType}, but the same ResultStatisticType
 * can refer to a different marker type depending on the {@link ScenarioResultType}.
 *
 * This class decribes the available calculation markers and their relation with ResultStatisticType
 */
public enum CalculationMarker {

  /**
   * Brown marker
   */
  HIGHEST_MAX,

  /**
   * Brown marker (+)
   */
  HIGHEST_MAX_INCREASE,

  /**
   * Blue marker
   */
  MAX_CUMULATION,

  /**
   * Pink marker (-)
   */
  MAX_DECREASE,

  /**
   * Pink marker (+)
   */
  MAX_INCREASE,

  /**
   * Pink marker
   */
  MAX_SITUATION,

  /**
   * Purple marker
   */
  MAX_TOTAL,

  /**
   * Orange marker
   */
  MAX_TOTAL_WITH_EFFECT;

  /**
   * Returns the calculation marker for a statisticType given a resultType
   */
  public static CalculationMarker getCalculationMarker(final ScenarioResultType resultType, final ResultStatisticType statisticType) {
    CalculationMarker marker = null;

    switch (resultType) {
    case SITUATION_RESULT:
      marker = getSituationResultMarker(statisticType);
      break;
    case PROJECT_CALCULATION:
      marker = getProjectCalculationResultMarker(statisticType);
      break;
    case MAX_TEMPORARY_EFFECT:
    case MAX_TEMPORARY_CONTRIBUTION:
      marker = getTemporaryResultMarker(statisticType);
      break;
    }

    return marker;
  }

  /**
   * Returns the calculation markers for a set of statisticTypes given a resultType
   */
  public static Set<CalculationMarker> getCalculationMarkers(final ScenarioResultType resultType,
      final Iterable<ResultStatisticType> statisticTypes) {
    final Set<CalculationMarker> markers = new HashSet<>();

    for (final ResultStatisticType statisticType : statisticTypes) {
      markers.add(getCalculationMarker(resultType, statisticType));
    }
    markers.removeIf(m -> m == null);

    return markers;
  }

  private static CalculationMarker getSituationResultMarker(final ResultStatisticType statisticType) {
    switch (statisticType) {
    case MAX_TOTAL:
      return MAX_TOTAL;
    case MAX_CONTRIBUTION:
      return MAX_SITUATION;
    default:
      return null;
    }
  }

  private static CalculationMarker getProjectCalculationResultMarker(final ResultStatisticType statisticType) {
    switch (statisticType) {
    case MAX_INCREASE:
      return MAX_INCREASE;
    case MAX_DECREASE:
      return MAX_DECREASE;
    case MAX_TOTAL:
      return MAX_TOTAL_WITH_EFFECT;
    default:
      return null;
    }
  }

  private static CalculationMarker getTemporaryResultMarker(final ResultStatisticType statisticType) {
    switch (statisticType) {
    case MAX_TEMP_INCREASE:
      return HIGHEST_MAX_INCREASE;
    case MAX_TEMP_CONTRIBUTION:
      return HIGHEST_MAX;
    default:
      return null;
    }
  }
}
