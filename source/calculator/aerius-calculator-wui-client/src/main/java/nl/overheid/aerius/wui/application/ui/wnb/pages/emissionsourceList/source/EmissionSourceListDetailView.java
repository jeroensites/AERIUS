/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.source;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.HorizontalCollapse;
import nl.overheid.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.OPSCharacteristicsComponent;
import nl.overheid.aerius.wui.application.components.source.farmland.detail.FarmLandDetailComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.detail.FarmLodgingDetailComponent;
import nl.overheid.aerius.wui.application.components.source.generic.GenericDetailComponent;
import nl.overheid.aerius.wui.application.components.source.offroad.detail.OffroadDetailComponent;
import nl.overheid.aerius.wui.application.components.source.road.detail.SRM2RoadDetailComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.detail.InlandShippingDetailComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.detail.MaritimeShippingDetailComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.ui.main.VerticalCollapse;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.building.BuildingDetailView;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    LabeledInputComponent.class,
    OPSCharacteristicsComponent.class,
    GenericDetailComponent.class,
    FarmLodgingDetailComponent.class,
    FarmLandDetailComponent.class,
    SRM2RoadDetailComponent.class,
    InlandShippingDetailComponent.class,
    MaritimeShippingDetailComponent.class,
    OffroadDetailComponent.class,
    HorizontalCollapse.class,
    VerticalCollapse.class,
    BuildingDetailView.class
})
public class EmissionSourceListDetailView extends BasicVueComponent implements IsVueComponent {
  @Prop EmissionSourceFeature source;

  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data ApplicationContext applicationContext;

  @Computed
  public String getLocation() {
    return Optional.ofNullable(OL3GeometryUtil.getMiddlePointOfGeometry(source.getGeometry()))
        .map(v -> MessageFormatter.formatPoint(v))
        .orElse(null);
  }

  @Computed
  String getLocationStatistic() {
    return GeoUtil.getGeometryStatistic(source.getGeometry());
  }

  @Computed
  public boolean hasBuilding() {
    return getBuilding() != null;
  }

  @Computed
  public BuildingFeature getBuilding() {
    if (isCType(source.getCharacteristicsType())) {
      return Optional.ofNullable((OPSCharacteristics) source.getCharacteristics())
          .map(v -> v.getBuildingId())
          .map(v -> scenarioContext.getActiveSituation().findBuilding(v))
          .orElse(null);
    } else {
      return null;
    }
  }

  @Computed
  public SectorGroup getSectorGroup() {
    return applicationContext.getConfiguration().getSectorCategories().getSectors()
        .stream().filter(v -> v.getSectorId() == source.getSectorId())
        .findFirst()
        .map(v -> v.getSectorGroup())
        .orElse(null);
  }

  @Computed("substances")
  public List<Substance> getSubstances() {
    return applicationContext.getActiveThemeConfiguration().getSubstances();
  }

  @Computed("substancesRoad")
  public List<Substance> getSubstancesRoad() {
    return applicationContext.getActiveThemeConfiguration().getEmissionSubstancesRoad();
  }

  @JsMethod
  public boolean isCType(final CharacteristicsType type) {
    return source.getCharacteristicsType() == type;
  }

  @Computed
  public Sector getSector() {
    return applicationContext.getConfiguration().getSectorCategories().determineSectorById(source.getSectorId());
  }

  @JsMethod
  public boolean isESType(final EmissionSourceType type) {
    return source.getEmissionSourceType() == type;
  }

  @JsMethod
  public boolean hasSectors(final SectorGroup sectorGroup) {
    return applicationContext.getConfiguration().getSectorCategories().getSectors().stream()
        .filter(v -> v.getSectorGroup() == sectorGroup)
        .count() > 1;
  }

  @JsMethod
  @Emit
  public void close() {}
}
