/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.air.scenario.importer;

import com.google.gwt.event.dom.client.DragEnterEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.application.command.ExportStartCommand;
import nl.overheid.aerius.wui.application.command.UserImportRequestedCommand;
import nl.overheid.aerius.wui.application.components.map.MapComponentFactory;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceCreatePlace;
import nl.overheid.aerius.wui.application.ui.air.AirQualitySubActivity;
import nl.overheid.aerius.wui.application.ui.air.AirQualityView;

public class ScenarioImportActivity extends BasicEventComponent implements AirQualitySubActivity {
  @Inject PlaceController placeController;

  private HandlerRegistration dragRegister;

  @Inject
  public ScenarioImportActivity(@Assisted final AirQualityView view) {
    view.setComponent(ScenarioImportViewFactory.get(), this);
    view.noTertiary();
    view.setMain(MapComponentFactory.get());
  }

  public void goToCreate() {
    placeController.goTo(new SourceCreatePlace());
  }

  @Override
  public void onStart(final AirQualityView View) {
    dragRegister = RootPanel.get().addDomHandler(event -> eventBus.fireEvent(new UserImportRequestedCommand()), DragEnterEvent.getType());
  }

  public void onStop() {
    dragRegister.removeHandler();
  }

  public void startExport(final String scenarioCode, final ScenarioCalculationOptions options) {
    eventBus.fireEvent(new ExportStartCommand(null));
  }
}
