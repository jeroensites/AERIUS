/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import ol.OLFactory;
import ol.geom.LineString;
import ol.style.Style;

import nl.overheid.aerius.geo.domain.legend.Legend;
import nl.overheid.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.constants.DrivingSide;
import nl.overheid.aerius.shared.domain.result.range.ColorRange;
import nl.overheid.aerius.shared.domain.result.range.ColorRangesLegend;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.source.road.TrafficDirection;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.StandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.VehicleType;
import nl.overheid.aerius.wui.application.geo.layers.source.RoadSourceGeometryLayer;
import nl.overheid.aerius.wui.application.i18n.M;

public class RoadTrafficVolumeStyle implements RoadStyle {

  private static final String ROAD_COLOR = "#095da7";
  private static final int ROAD_CENTERLINE_WIDTH = 1;
  private static final Style ROAD_CENTERLINE_STYLE =
      OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor("white"), ROAD_CENTERLINE_WIDTH));
  private static final int ROAD_WIDTH_INCREASE_FACTOR = 2;

  private static final List<ColorRange> legend = new ArrayList<>();

  private final DrivingSide drivingSide;

  static {
    legend.add(new ColorRange(0, ROAD_COLOR));
    legend.add(new ColorRange(10000, ROAD_COLOR));
    legend.add(new ColorRange(20000, ROAD_COLOR));
    legend.add(new ColorRange(30000, ROAD_COLOR));
    legend.add(new ColorRange(50000, ROAD_COLOR));
  }

  public RoadTrafficVolumeStyle(final DrivingSide drivingSide) {
    this.drivingSide = drivingSide;
  }

  @Override
  public List<Style> getStyle(final RoadESFeature feature, final nl.overheid.aerius.shared.domain.v2.source.road.VehicleType vehicleType,
      final double resolution) {

    final int trafficVolume = (int) feature.getSubSources().asList().stream()
        .filter((v) -> v.getVehicleType() == VehicleType.STANDARD)
        .map(v -> ((StandardVehicles) v))
        .mapToDouble(v -> getSafeVehiclesPer(v.getValuesPerVehicleType(vehicleType), v.getTimeUnit()))
        .sum();

    final ColorRange range = legend.stream()
        .sorted(Comparator.reverseOrder())
        .filter(v -> trafficVolume >= v.getLowerValue())
        .findFirst().orElse(legend.get(0));

    final int lineWidth = getRoadWidth(range);
    double lineOffset = 0;

    // Render the line one side of the line
    if (feature.getTrafficDirection() != null && feature.getTrafficDirection() != TrafficDirection.BOTH) {
      lineOffset = (lineWidth - 2 * ROAD_CENTERLINE_WIDTH) * resolution * 0.5;

      boolean drawRightSide = drivingSide == DrivingSide.RIGHT;
      if (feature.getTrafficDirection() == TrafficDirection.B_TO_A) {
        drawRightSide = !drawRightSide;
      }

      if (!drawRightSide) {
        lineOffset *= -1;
      }
    }

    final Style trafficVolumeStyle = OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(range.getColor()), lineWidth));
    trafficVolumeStyle.setGeometry(OL3GeometryUtil.offsetFromLineString((LineString) feature.getGeometry(), lineOffset));

    return Arrays.asList(trafficVolumeStyle, ROAD_CENTERLINE_STYLE);
  }

  private double getSafeVehiclesPer(final ValuesPerVehicleType valuesPerVehicleType, final TimeUnit timeUnit) {
    return valuesPerVehicleType == null ? 0D : valuesPerVehicleType.getVehiclesPer(timeUnit, TimeUnit.DAY);
  }

  private int getRoadWidth(final ColorRange range) {
    return legend.indexOf(range) * ROAD_WIDTH_INCREASE_FACTOR + RoadSourceGeometryLayer.DEFAULT_ROAD_WIDTH;
  }

  @Override
  public Legend getLegend() {
    final ColorRangesLegend colorRangesLegend = new ColorRangesLegend(legend, LegendType.LINE);
    colorRangesLegend.setUnit(M.messages().layerRoadVolumeUnit());
    colorRangesLegend.setIconSizes(legend.stream().mapToInt(r -> getRoadWidth(r)).boxed().toArray(l -> new Integer[l]));
    return colorRangesLegend;
  }

}
