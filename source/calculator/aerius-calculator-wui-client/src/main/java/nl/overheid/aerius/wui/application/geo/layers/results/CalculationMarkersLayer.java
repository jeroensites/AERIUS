/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.results;

import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.DataResource;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Collection;
import ol.Feature;
import ol.OLFactory;
import ol.format.GeoJson;
import ol.format.GeoJsonOptions;
import ol.layer.Layer;
import ol.layer.Vector;
import ol.source.VectorOptions;
import ol.style.Icon;
import ol.style.IconOptions;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.aerius.wui.dev.GWTProd;
import nl.overheid.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsMarker;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.event.CalculationStartEvent;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.application.i18n.M;

public class CalculationMarkersLayer implements IsLayer<Layer> {
  private final Vector layer;
  private final Collection<Feature> featureCollection;

  private final CalculationMarkersLayer.CalculationMarkersLayerEventBinder EVENT_BINDER = GWT.create(
      CalculationMarkersLayer.CalculationMarkersLayerEventBinder.class);

  interface CalculationMarkersLayerEventBinder extends EventBinder<CalculationMarkersLayer> {}

  private final MapConfigurationContext mapConfigurationContext;
  private final CalculationContext calculationContext;
  private final LayerInfo info;

  @Inject
  public CalculationMarkersLayer(final EventBus eventBus, final MapConfigurationContext mapConfigurationContext,
      final CalculationContext calculationContext) {
    this.mapConfigurationContext = mapConfigurationContext;
    this.featureCollection = new Collection<>();
    this.layer = new Vector();

    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerMarkers());

    this.calculationContext = calculationContext;

    final VectorOptions vectorSourceOptions = OLFactory.createOptions();
    vectorSourceOptions.setFeatures(featureCollection);
    final ol.source.Vector vectorSource = new ol.source.Vector(vectorSourceOptions);

    layer.setSource(vectorSource);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onSituationResultsSelectedEvent(final SituationResultsSelectedEvent e) {
    updateMarkers();
  }

  private void updateMarkers() {
    featureCollection.clear();

    final Optional<ResultSummaryContext> summary = Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(v -> v.getResultContext());
    if (!summary.isPresent()) {
      return;
    }

    final ResultSummaryContext summaryContext = summary.get();
    final SituationResultsKey key = summaryContext.getSituationResultsKey();

    if (!summaryContext.hasActiveResultSummary()) {
      return;
    }

    final SituationResultsSummary resultsSummary = summaryContext.getActiveResultSummary();
    for (final SituationResultsMarker resultsMarker : resultsSummary.getMarkers().asList()) {
      final Set<CalculationMarker> calculationMarkers = CalculationMarker.getCalculationMarkers(key.getResultType(),
          resultsMarker.getStatisticsTypes());

      if (calculationMarkers.isEmpty()) {
        continue;
      }

      final DataResource markerImage = CalculationMarkers.getMarkerImage(calculationMarkers);
      if (markerImage == null) {
        GWTProd.warn("Combination image of markers " + calculationMarkers + " does not exist");
      } else {

        final IconOptions iconOptions = new IconOptions();
        iconOptions.setSrc(markerImage.getSafeUri().asString());
        iconOptions.setAnchor(new double[] { 0.5, 1.0 });
        iconOptions.setScale(mapConfigurationContext.getIconScale());
        final Icon icon = new Icon(iconOptions);

        final StyleOptions styleOptions = new StyleOptions();
        styleOptions.setImage(icon);
        final Style style = new Style(styleOptions);

        final Feature markerFeature = new Feature();
        markerFeature.setGeometry(new GeoJson().readGeometry(resultsMarker.getPoint(), new GeoJsonOptions()));
        markerFeature.setStyle(style);

        featureCollection.push(markerFeature);
      }
    }
  }

  @EventHandler
  public void onCalculationStartEvent(final CalculationStartEvent e) {
    layer.setVisible(false);
    featureCollection.clear();
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
