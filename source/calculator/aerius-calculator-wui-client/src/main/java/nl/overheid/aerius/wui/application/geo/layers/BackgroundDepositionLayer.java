/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import ol.OLFactory;
import ol.layer.Image;
import ol.layer.Layer;
import ol.source.ImageWms;
import ol.source.ImageWmsOptions;
import ol.source.ImageWmsParams;

import nl.overheid.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.AppThemeConfiguration;
import nl.overheid.aerius.shared.domain.result.range.ColorRangeType;
import nl.overheid.aerius.shared.domain.result.range.ColorRangesLegend;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.event.situation.SituationChangedYearEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

/**
 * Provides the background deposition layer
 */
public class BackgroundDepositionLayer implements IsLayer<Layer> {

  private static final BackgroundDepositionLayerEventBinder EVENT_BINDER = GWT.create(BackgroundDepositionLayerEventBinder.class);

  interface BackgroundDepositionLayerEventBinder extends EventBinder<BackgroundDepositionLayer> {
  }

  private static final String WMS_DEPOSITIONS_JURISDICTION_POLICIES_VIEW = "calculator:wms_depositions_jurisdiction_policies_view";

  private final Image layer;
  private final ImageWmsParams imageWmsParams;
  private final ImageWmsOptions imageWmsOptions;
  private ImageWms imageWms;

  private final LayerInfo info;

  @Inject ScenarioContext scenarioContext;

  @Inject
  public BackgroundDepositionLayer(final EventBus eventBus, @Assisted final AppThemeConfiguration themeConfiguration,
      final EnvironmentConfiguration configuration) {
    imageWmsParams = OLFactory.createOptions();

    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerBackgroundDeposition());
    this.info.setLegend(new ColorRangesLegend(
        themeConfiguration.getColorRange(ColorRangeType.DEPOSITION_BACKGROUND),
        v -> MessageFormatter.formatDeposition(v, themeConfiguration.getEmissionResultValueDisplaySettings()),
        LegendType.HEXAGON,
        M.messages().layerBackGroundDepositionUnit(themeConfiguration.getEmissionResultValueDisplaySettings().getDisplayType()))); // Maybe we want to change this in the future

    imageWmsOptions = OLFactory.createOptions();
    imageWmsOptions.setUrl(configuration.getGeoserverBaseUrl());
    imageWmsOptions.setParams(imageWmsParams);

    layer = new Image();
    layer.setVisible(false);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler(handles = {SituationChangedYearEvent.class, SituationSwitchEvent.class})
  public void onUpdateLayer(final GenericEvent e) {
    updateLayer();
  }

  private String buildViewParams() {
    return "year:" + this.scenarioContext.getActiveSituation().getYear();
  }

  private void updateLayer() {
    imageWmsParams.setViewParams(buildViewParams());
    imageWmsParams.setLayers(WMS_DEPOSITIONS_JURISDICTION_POLICIES_VIEW);

    if (imageWms == null) {
      imageWms = new ImageWms(imageWmsOptions);
      layer.setSource(imageWms);
    }

    imageWms.refresh();
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
