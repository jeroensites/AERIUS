/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics.adms;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;

/**
 * Validators for {@link ADMSCharacteristicsEditorComponent}.
 */
public class ADMSCharacteristicsEditorValidators extends ValidationOptions<ADMSCharacteristicsEditorComponent> {
  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class ADMSCharacteristicsEditorValidations extends Validations {
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final ADMSCharacteristicsEditorComponent instance = Js.uncheckedCast(options);

  }

}
