/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.info;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.domain.info.EmissionResultInfo;
import nl.overheid.aerius.wui.application.domain.result.EmissionResults;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class,
    BasicInfoRowComponent.class,
})
public class EmissionResultComponent extends BasicVueComponent implements IsVueComponent {
  @Inject @Data ApplicationContext appContext;
  @Inject @Data ScenarioContext scenario;
  @Inject @Data CalculationContext calculationContext;

  @Prop @JsProperty EmissionResultInfo emissionResult;

  @Computed("hasCalculations")
  public boolean hasCalculations() {
    return calculationContext.getActiveCalculation() != null;
  }

  @Computed
  public String getBackgroundDeposition() {
    final Double emissionResultValue = emissionResult.getBackgroundEmissionResults().hasKey(EmissionResultKey.NOXNH3_DEPOSITION)
        ? emissionResult.getBackgroundEmissionResults().getEmissionResultValue(EmissionResultKey.NOXNH3_DEPOSITION) : null;
    return MessageFormatter.formatDepositionWithUnit(emissionResultValue,
        appContext.getActiveThemeConfiguration().getEmissionResultValueDisplaySettings());
  }

  @Computed
  public List<SituationContext> getScenarios() {
    return scenario.getSituations();
  }

  @JsMethod
  public String getEmissionResult(final String situationId) {
    if (emissionResult.hasEmissionResults()) {
      final EmissionResults result = emissionResult.getEmissionResults(situationId);
      if (result != null) {
        return MessageFormatter.formatDepositionWithUnit(result.getEmissionResultValue(EmissionResultKey.NOXNH3_DEPOSITION),
            appContext.getActiveThemeConfiguration().getEmissionResultValueDisplaySettings());
      } else {
        return "-";
      }
    }
    return "-";
  }
}
