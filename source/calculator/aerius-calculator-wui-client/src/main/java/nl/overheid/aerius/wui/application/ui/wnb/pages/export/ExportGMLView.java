/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.wnb.pages.export;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.ui.wnb.pages.export.metadata.ExportMetaDataView;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(name = "export-gml",
components = {
    CheckBoxComponent.class,
    ExportMetaDataView.class
})
public class ExportGMLView extends BasicVueView {
  @Prop @JsProperty List<SituationContext> situations;

  @Inject @Data ExportContext exportContext;
  @Data boolean includingResults;

  @Ref ExportMetaDataView exportMetaData;

  @JsMethod
  public boolean isIncludingResults(final String id) {
    return exportContext.getIncludingResults().contains(id);
  }

  // ToDo: Restore commented out code and implement calculating results for selected individual situations.
  @JsMethod
  public void updateIncludingResults(final boolean checked, final String id) {
    this.includingResults = checked;
    if (checked) {
      // exportContext.getIncludingResults().add(id);
      exportContext.getIncludingResults().addAll(situations.stream().map(sc -> sc.getId()).collect(Collectors.toList()));
    } else {
      // removeSelectedIncludingResults(id);
      exportContext.getIncludingResults().clear();
    }
  }

  @Computed
  public boolean isIncludeScenarioMetaData() {
    return exportContext.isIncludeScenarioMetaData();
  }

  @JsMethod()
  public void toggleIncludeScenarioMetaData(final boolean enabled) {
    exportContext.setIncludeScenarioMetaData(enabled);
  }

  @JsMethod
  public void touchAll() {
    if (exportContext.isIncludeScenarioMetaData()) {
      exportMetaData.touchAll();
    }
  }

  @JsMethod
  public boolean isError() {
    return exportContext.isIncludeScenarioMetaData() && exportMetaData.isError();
  }

  @Watch("situations")
  void onSituationsChange(final List<SituationContext> neww) {
    for (final String id : exportContext.getIncludingResults()) {
      if (!ifExists(id)) {
        removeSelectedIncludingResults(id);
      }
    }
    if (includingResults) {
      neww.forEach(sc -> exportContext.getIncludingResults().add(sc.getId()));
    }
  }

  private boolean ifExists(final String id) {
    return situations.stream().anyMatch(v -> id.equals(v.getId()));
  }

  private void removeSelectedIncludingResults(final String id) {
    exportContext.getIncludingResults().remove(id);
  }
}
