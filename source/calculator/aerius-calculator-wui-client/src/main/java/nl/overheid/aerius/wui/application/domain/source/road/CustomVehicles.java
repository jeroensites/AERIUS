/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;

/**
 * Client side implementation of CustomVehicles props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomVehicles extends Vehicles {

  private String description;
  private double vehiclesPerTimeUnit;
  private EmissionFactors emissionFactors;

  public static final @JsOverlay CustomVehicles create() {
    final CustomVehicles props = new CustomVehicles();
    Vehicles.init(props, VehicleType.CUSTOM);
    props.setDescription("");
    props.setVehiclesPerTimeUnit(0D);
    props.setEmissionFactors(EmissionFactors.create());
    return props;
  }

  public final @JsOverlay String getDescription() {
    return description;
  }

  public final @JsOverlay void setDescription(final String description) {
    this.description = description;
  }

  public final @JsOverlay double getVehiclesPerTimeUnit() {
    return vehiclesPerTimeUnit;
  }

  public final @JsOverlay void setVehiclesPerTimeUnit(final double vehiclesPerTimeUnit) {
    this.vehiclesPerTimeUnit = vehiclesPerTimeUnit;
  }

  public final @JsOverlay double getVehiclesPer(final TimeUnit targetTimeUnit) {
    return this.getTimeUnit() == targetTimeUnit ? vehiclesPerTimeUnit : getTimeUnit().toUnit(vehiclesPerTimeUnit, targetTimeUnit);
  }

  public final @JsOverlay double getEmissionFactor(final Substance substance) {
    return emissionFactors.getEmissionFactor(substance);
  }

  public final @JsOverlay void setEmissionFactor(final Substance substance, final double emission) {
    emissionFactors.setEmissionFactor(substance, emission);
  }

  public final @JsOverlay void setEmissionFactors(final EmissionFactors emissionFactors) {
    this.emissionFactors = emissionFactors;
  }

  public final @JsOverlay EmissionFactors getEmissionFactors() {
    return emissionFactors;
  }

}
