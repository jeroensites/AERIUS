/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.air;

import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointListPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.LayerPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.ResultPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.ScenarioImportPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceImportPlace;
import nl.overheid.aerius.wui.application.ui.air.layer.LayerActivity;
import nl.overheid.aerius.wui.application.ui.air.receptors.create.ReceptorCreateActivity;
import nl.overheid.aerius.wui.application.ui.air.receptors.list.ReceptorListActivity;
import nl.overheid.aerius.wui.application.ui.air.results.AirResultsActivity;
import nl.overheid.aerius.wui.application.ui.air.scenario.importer.ScenarioImportActivity;
import nl.overheid.aerius.wui.application.ui.air.sources.create.SourceCreateActivity;
import nl.overheid.aerius.wui.application.ui.air.sources.start.SourceStartActivity;

public interface AirQualityActivityFactory {
  ScenarioImportActivity createScenarioImportActivity(AirQualityView view, ScenarioImportPlace place);

  SourceStartActivity createSourceStartActivity(AirQualityView view, SourceImportPlace place);

  SourceCreateActivity createSourceCreateActivity(AirQualityView view, SourceCreatePlace place);

  ReceptorCreateActivity createReceptorCreateActivity(AirQualityView view, CalculationPointCreatePlace place);

  ReceptorListActivity createReceptorListActivity(AirQualityView view, CalculationPointListPlace place);

  AirResultsActivity createResultsActivity(AirQualityView view, ResultPlace place);

  LayerActivity createLayerActivity(AirQualityView view, LayerPlace place);
}
