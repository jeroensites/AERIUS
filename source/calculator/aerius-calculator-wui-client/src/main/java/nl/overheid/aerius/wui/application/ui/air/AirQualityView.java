/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.air;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasActivated;
import com.axellience.vuegwt.core.client.vue.VueComponentFactory;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.activity.Presenter;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.vue.AcceptsOneComponent;
import nl.overheid.aerius.wui.application.components.dummy.SimpleDummyComponent;
import nl.overheid.aerius.wui.application.components.map.MapComponent;
import nl.overheid.aerius.wui.application.components.map.MapComponentFactory;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceImportPlace;
import nl.overheid.aerius.wui.application.ui.air.layer.LayerView;
import nl.overheid.aerius.wui.application.ui.air.receptors.create.ReceptorCreateView;
import nl.overheid.aerius.wui.application.ui.air.receptors.list.ReceptorListView;
import nl.overheid.aerius.wui.application.ui.air.results.AirResultsView;
import nl.overheid.aerius.wui.application.ui.air.scenario.importer.ScenarioImportView;
import nl.overheid.aerius.wui.application.ui.air.sources.create.SourceCreateView;
import nl.overheid.aerius.wui.application.ui.air.sources.start.SourceStartView;
import nl.overheid.aerius.wui.application.ui.main.MainView;

@Component(components = {
MainView.class,
ScenarioImportView.class,
ReceptorCreateView.class,
ReceptorListView.class,
SourceStartView.class,
SourceCreateView.class,
AirResultsView.class,
LayerView.class,
MapComponent.class,

SimpleDummyComponent.class
})
public class AirQualityView implements IsVueComponent, HasActivated, AcceptsOneComponent {
  @Inject PlaceController placeController;

  @Data NavigationContext navigation;

  @Prop EventBus eventBus;
  @Prop AirQualityPresenter presenter;

  @Data Presenter subPresenter;

  @Ref MainView outer;

  @Data String input;
  @Data String tertiary;
  @Data String main;

  @JsMethod
  public void testGoToStart() {
    placeController.goTo(new SourceImportPlace());
  }

  @JsMethod
  public void testGoToCreate() {
    placeController.goTo(new SourceCreatePlace());
  }

  @Override
  public void activated() {
    presenter.setView(this);
  }

  @Override
  public <P extends Presenter> void setComponent(final VueComponentFactory<?> fact, final P presenter) {
    input = fact.getComponentTagName();
    subPresenter = presenter;
  }

  /**
   * Set the main view, automatically detecting whether it is a map-type view.
   */
  public void setMain(final VueComponentFactory<?> fact) {
    setMain(fact, true);
  }

  public void setMain(final VueComponentFactory<?> fact, final boolean autoDetect) {
    if (autoDetect) {
      setMapActive(MapComponentFactory.get().getComponentTagName().equals(fact.getComponentTagName()));
    }

    main = fact.getComponentTagName();
  }

  /**
   * Set whether to activate a map-type view, which puts the main view 'behind' the content view (positioning aboslutely)
   */
  public void setMapActive(final boolean mapActive) {
  }

  public void setNavigation(final NavigationContext nav) {
    this.navigation = nav;
  }

  public void setTertiary(final VueComponentFactory<?> fact) {
    if (fact == null) {
      tertiary = null;
    } else {
      tertiary = fact.getComponentTagName();
    }
  }

  public void noTertiary() {
    setTertiary(null);
  }
}
