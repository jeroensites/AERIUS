/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.results;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.OLFactory;
import ol.layer.Image;
import ol.layer.Layer;
import ol.source.ImageWms;
import ol.source.ImageWmsOptions;
import ol.source.ImageWmsParams;

import nl.overheid.aerius.geo.command.LayerHiddenCommand;
import nl.overheid.aerius.geo.command.LayerVisibleCommand;
import nl.overheid.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.js.calculation.ResultsGraphRange;
import nl.overheid.aerius.shared.config.AppThemeConfiguration;
import nl.overheid.aerius.shared.domain.result.range.ColorRangeType;
import nl.overheid.aerius.shared.domain.result.range.ColorRangesLegend;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.event.CalculationCancelEvent;
import nl.overheid.aerius.wui.application.event.CalculationCompleteEvent;
import nl.overheid.aerius.wui.application.event.CalculationStartEvent;
import nl.overheid.aerius.wui.application.event.CalculationStatusEvent;
import nl.overheid.aerius.wui.application.event.ResultsSelectedEvent;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

/**
 * Provides the deposition result layer
 */
public class CalculationResultLayer implements IsLayer<Layer> {

  private static final String WMS_BASE_LAYER_NAME = "calculator:wms_scenario_";

  private static final String SITUATION_RESULT = "situation_result";
  private static final String PROJECT_CALCULATION = "project_calculation";
  private static final String TEMPORARY_CONTRIBUTION = "temporary_contribution";
  private static final String TEMPORARY_EFFECT = "temporary_effect";

  private static final String CQL_FILTER = "CQL_FILTER";
  private static final String DEPOSITION = "deposition";

  private final LayerInfo info;
  private final Image layer;

  private final ImageWmsParams imageWmsParams;
  private final ImageWmsOptions imageWmsOptions;
  private ImageWms imageWms;

  private final CalculationResultLayer.CalculationResultLayerEventBinder EVENT_BINDER = GWT
      .create(CalculationResultLayer.CalculationResultLayerEventBinder.class);

  interface CalculationResultLayerEventBinder extends EventBinder<CalculationResultLayer> {}

  private String situationId = null;
  private ScenarioResultType resultType = ScenarioResultType.SITUATION_RESULT;
  private SummaryHexagonType hexagonType = SummaryHexagonType.RELEVANT_HEXAGONS;
  private int hexagonCount = 0;
  private List<ResultsGraphRange> selectedBounds = new ArrayList<>();

  private final CalculationContext calculationContext;
  private final AppThemeConfiguration themeConfiguration;
  private final EventBus eventBus;

  @Inject
  public CalculationResultLayer(@Assisted final AppThemeConfiguration themeConfiguration,
      final EventBus eventBus, final EnvironmentConfiguration configuration, final CalculationContext calculationContext) {

    this.calculationContext = calculationContext;
    this.themeConfiguration = themeConfiguration;
    this.eventBus = eventBus;

    this.info = new LayerInfo();
    this.info.setName(this.getClass().getCanonicalName());
    this.info.setTitle(M.messages().layerResults());
    updateLegend();

    imageWmsParams = OLFactory.createOptions();

    imageWmsOptions = OLFactory.createOptions();
    imageWmsOptions.setUrl(configuration.getGeoserverBaseUrl());
    imageWmsOptions.setParams(imageWmsParams);

    layer = new Image();
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onCalculationStartEvent(final CalculationStartEvent e) {
    this.situationId = null;
    this.hexagonCount = 0;
    updateLayer();
  }

  @EventHandler
  public void onCalculationCancelEvent(final CalculationCancelEvent e) {
    this.situationId = null;
    this.hexagonCount = 0;
    updateLayer();
  }

  @EventHandler
  public void onCalculationStatusEvent(final CalculationStatusEvent e) {
    this.hexagonCount = e.getValue().getJobProgress().getHexagonCount();
    updateLayer();
  }

  @EventHandler
  public void onCalculationCompleteEvent(final CalculationCompleteEvent e) {
    this.hexagonCount = calculationContext.getActiveCalculation().getCalculationInfo().getJobProgress().getHexagonCount();
    updateLayer();
  }

  @EventHandler
  public void onSituationResultsSelectedEvent(final SituationResultsSelectedEvent e) {
    situationId = e.getValue().getSituationHandle().getId();
    resultType = e.getValue().getResultType();
    hexagonType = e.getValue().getHexagonType();
    updateLayer();
  }

  @EventHandler
  public void onResultsSelectedEvent(final ResultsSelectedEvent e) {
    selectedBounds = e.getValue().getSelectedBounds();
    updateLayer();
  }

  private String buildLayerName() {
    String url = WMS_BASE_LAYER_NAME;

    switch (resultType) {
    case SITUATION_RESULT:
        url += SITUATION_RESULT;
        break;
    case PROJECT_CALCULATION:
        url += PROJECT_CALCULATION;
        break;
    case MAX_TEMPORARY_CONTRIBUTION:
        url += TEMPORARY_CONTRIBUTION;
        break;
    case MAX_TEMPORARY_EFFECT:
        url += TEMPORARY_EFFECT;
        break;
    }

    return url;
  }

  private String buildViewParams() {
    String viewParams = "";
    viewParams += "job_key:" + calculationContext.getActiveCalculationCode();
    viewParams += ";situation_reference:" + situationId;
    viewParams += ";hexagon_type:" + hexagonType.name().toLowerCase(Locale.ROOT);
    return viewParams;
  }

  private String buildCqlFilter() {
    return selectedBounds.stream().map(bound -> {
      if (Double.isInfinite(bound.getLowerBound())) {
        return DEPOSITION + " < " + bound.getUpperBound();
      } else if (Double.isInfinite(bound.getUpperBound())) {
        return DEPOSITION + " >= " + bound.getLowerBound();
      } else {
        return DEPOSITION + " >= " + bound.getLowerBound() + " AND " + DEPOSITION + " < " + bound.getUpperBound();
      }
    }).map(bound -> "(" + bound + ")").collect(Collectors.joining(" OR "));
  }

  private void updateLegend() {
    final ColorRangeType colorRangeType = resultType == ScenarioResultType.SITUATION_RESULT ||
        resultType == ScenarioResultType.MAX_TEMPORARY_CONTRIBUTION
        ? ColorRangeType.DEPOSITION_CONTRIBUTION
        : ColorRangeType.DEPOSITION_DIFFERENCE;

    this.info.setLegend(new ColorRangesLegend(
        themeConfiguration.getColorRange(colorRangeType),
        v -> MessageFormatter.formatDeposition(v, themeConfiguration.getEmissionResultValueDisplaySettings()),
        LegendType.HEXAGON,
        M.messages().layerResultsUnit(themeConfiguration.getEmissionResultValueDisplaySettings().getDisplayType())));
  }

  private void updateLayer() {
    final boolean layerVisible = situationId != null && hexagonType != SummaryHexagonType.CUSTOM_CALCULATION_POINTS;
    if (layerVisible && !layer.getVisible()) {
      eventBus.fireEvent(new LayerVisibleCommand(this));
    }
    if (!layerVisible && layer.getVisible()) {
      eventBus.fireEvent(new LayerHiddenCommand(this));
    }

    if (layerVisible) {
      imageWmsParams.setViewParams(buildViewParams());
      imageWmsParams.setLayers(buildLayerName());
      imageWmsParams.set("hc", String.valueOf(hexagonCount));

      final String cqlFilter = buildCqlFilter();
      if (cqlFilter.isEmpty()) {
        imageWmsParams.set(CQL_FILTER, null);
      } else {
        imageWmsParams.set(CQL_FILTER, cqlFilter);
      }

      if (imageWms == null) {
        imageWms = new ImageWms(imageWmsOptions);
        layer.setSource(imageWms);
      }

      imageWms.refresh();
    }

    updateLegend();
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public Optional<LayerInfo> getInfoOptional() {
    return Optional.of(info);
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
