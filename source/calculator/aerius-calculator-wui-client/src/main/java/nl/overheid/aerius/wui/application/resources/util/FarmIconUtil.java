/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.resources.util;

import elemental2.core.JsArray;

import nl.overheid.aerius.shared.domain.sector.category.AnimalType;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.wui.application.domain.source.FarmLodgingESFeature;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomFarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingType;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodgingUtil;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandType;


/**
 * Util class for Farm related data.
 */
public final class FarmIconUtil {

  public enum AdditionalIcon {
    WARNING,
    ADJUSTED;

    public boolean is(final AdditionalIcon icon) {
      return this == icon;
    }
  }

  private static final String FARMLAND_PREFIX = "farmland";
  private static final String ANIMAL_PREFIX = "animal";
  private static final String ANIMAL_MIXED = FontIconUtil.fontClassName(ANIMAL_PREFIX, "mixed");

  private FarmIconUtil() {
  }

  public static String getFarmLodgingIcon(final FarmLodging farmLodging) {
    final String code;
    if (farmLodging.getFarmLodgingType() == FarmLodgingType.STANDARD) {
      code = ((StandardFarmLodging) farmLodging).getFarmLodgingCode();
    } else {
      code = ((CustomFarmLodging) farmLodging).getAnimalCode();
    }
    return FontIconUtil.fontClassName(ANIMAL_PREFIX, AnimalType.getByCode(code));
  }

  /**
   * Returns the image resource for an emission source containing {@link FarmLodgingESFeature}. If different animals are present the mixed icon is
   * returned, otherwise the animal specific icon.
   *
   * @param source emission source to get icon for
   * @return image resource representing the animals in the emission source
   */
  public static String getFarmIcon(final FarmLodgingESFeature source) {
    final JsArray<FarmLodging> lodgings = source.getSubSources();
    String farmLodgingsCode = null;

    for (int i = 0; i < lodgings.length; i++) {
      final FarmLodging farmLodging = lodgings.getAt(i);
      final String currentFLC = farmLodging.getFarmLodgingType() == FarmLodgingType.STANDARD
          ? ((StandardFarmLodging) farmLodging).getFarmLodgingCode() : "";

      if (farmLodgingsCode == null) {
        farmLodgingsCode = currentFLC;
      } else if (!farmLodgingsCode.equals(currentFLC)){
        // Found a second different farmLodging type. So it's should show a mixed icon.
        return ANIMAL_MIXED;
      }
    }
    return farmLodgingsCode == null ? ANIMAL_MIXED : FontIconUtil.fontClassName(ANIMAL_PREFIX, AnimalType.getByCode(farmLodgingsCode));
  }

  /**
   * Returns an indicator if the given standard farm lodging type is adjusted or contains incompatible combinations.
   *
   * @param source farmlodging source to check
   * @param farmLodgingCategories farmloding categories
   * @return indicator enum or null if nothing to indicate
   */
  public static AdditionalIcon getFarmLodgingAdditionalIcon(final FarmLodging source, final FarmLodgingCategories farmLodgingCategories) {
    AdditionalIcon additionalIcon = null;

    if (source.getFarmLodgingType() == FarmLodgingType.STANDARD) {
      final StandardFarmLodging standardFarmLodging = (StandardFarmLodging) source;
      final String category = standardFarmLodging.getFarmLodgingCode();

      if (category != null) {
        final FarmLodgingCategory farmLodgingCategory = farmLodgingCategories.determineFarmLodgingCategoryByCode(category);

        if (farmLodgingCategory != null &&
            FarmLodgingUtil.hasIncompatibleCombinations(standardFarmLodging, farmLodgingCategory, farmLodgingCategories)) {
          additionalIcon = AdditionalIcon.WARNING;
        } else if (FarmLodgingUtil.hasStacking(standardFarmLodging)) {
          additionalIcon = AdditionalIcon.ADJUSTED;
        }
      }
    }
    return additionalIcon;
  }

  /**
   *  Return icon for supplied {@link FarmlandType}.
   *
   * @param farmlandType
   * @return string for icon
   */
  public static String getFarmlandIcon(final FarmlandType farmlandType) {
    return farmlandType == null ? FontIconUtil.fontClassName(FARMLAND_PREFIX) :
      FontIconUtil.fontClassName(FARMLAND_PREFIX, farmlandType);
  }
}
