/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import ol.OLFactory;
import ol.style.Style;

import nl.overheid.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.overheid.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.geo.layers.source.RoadSourceGeometryLayer;
import nl.overheid.aerius.wui.application.i18n.M;

public class RoadTypeStyle implements RoadStyle {

  private static final Map<RoadType, String> colorMap = new LinkedHashMap<>();
  private static final Map<RoadType, List<Style>> styleMap;

  static {
    colorMap.put(RoadType.URBAN_ROAD, "#ccff66");
    colorMap.put(RoadType.NON_URBAN_ROAD, "#ffff00");
    colorMap.put(RoadType.FREEWAY, "#ff8002");

    styleMap = colorMap.entrySet().stream().collect(Collectors.toMap(
        Map.Entry::getKey,
        c -> Collections.singletonList(OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(c.getValue()), RoadSourceGeometryLayer.DEFAULT_ROAD_WIDTH)))
    ));
  }

  @Override
  public List<Style> getStyle(final RoadESFeature feature, final VehicleType vehicleType, final double resolution) {
    final RoadType roadType = RoadType.valueFromSectorId(feature.getSectorId());
    if (roadType == null) {
      return Collections.emptyList();
    }

    return styleMap.get(roadType);
  }

  @Override
  public ColorLabelsLegend getLegend() {
    return new ColorLabelsLegend(
            colorMap.keySet().stream().map(rt -> M.messages().sourceRoadType(rt)).toArray(String[]::new),
            colorMap.values().toArray(new String[0]),
            LegendType.LINE
    );
  }

}
