/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsibleButton;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.ui.main.FadeTransition;
import nl.overheid.aerius.wui.application.ui.main.HorizontalCollapse;
import nl.overheid.aerius.wui.application.ui.main.VerticalCollapse;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-file", components = {
    VerticalCollapse.class,
    FadeTransition.class,
    CollapsibleButton.class,
    HorizontalCollapse.class,
    TooltipComponent.class,
    ButtonIcon.class,
    MiniStatsComponent.class,
})
public class FileComponent extends BasicVueComponent implements IsVueComponent, HasCreated {
  @Prop FileUploadStatus file;

  @Prop @JsProperty List<FileUploadStatus> files; // Subfiles from the same file

  @Prop boolean hasActiveSituation;

  @Data boolean contentVisible = false;

  @Data @Inject ApplicationContext applicationContext;

  @PropDefault("files")
  public List<FileUploadStatus> defaultFiles() {
    return Collections.emptyList();
  }

  @Override
  public void created() {
    // If we're dealing with calculation points, set selected file explicitly after component creation.
    if (hasActiveSituation && isContainsCalculationPoints()) {
      setSelectedFile(file.getFileCode());
    }
  }

  @Computed
  public List<SituationType> getAvailableSituationTypes() {
    return applicationContext.getActiveThemeConfiguration().getAvailableSituationTypes();
  }

  @Computed
  protected String getSituationType() {
    return file.getSituationType().name();
  }

  @Computed
  public boolean isContainsCalculationPoints() {
    return file.getSituationStats() != null && file.getSituationStats().getCalculationPoints() > 0;
  }

  @Computed
  protected void setSituationType(final String situationType) {
    file.setSituationType(SituationType.safeValueOf(situationType));
  }

  @Computed
  protected String getSelectedFile() {
    return file.getFileCode();
  }

  @Computed
  protected void setSelectedFile(final String fileCode) {
    final Optional<FileUploadStatus> optionalFile = this.files.stream().filter(f -> f.getFileCode().equals(fileCode)).findFirst();
    final FileUploadStatus selectedFile = optionalFile.isPresent() ? optionalFile.get() : file;

    vue().$emit("selectFileToImportIntoActiveSituation", selectedFile);
  }

  @JsMethod
  public String completePercentage() {
    return Math.round(file.getPercentage() * 100) + "%";
  }

  @JsMethod
  public String getIconForFile(final FileUploadStatus file) {
    final String name = file.getFile().name;
    final String ext = name.substring(name.lastIndexOf("."), name.length());

    switch (ext) {
    case ".gml":
      return "icon-doc-gml";
    case ".pdf":
      return "icon-doc-pdf";
    case ".zip":
      return "icon-doc-zip";
    case ".rcp":
      return "icon-doc-rcp";
    case ".brn":
      return "icon-doc-brn";
    case ".csv":
      return "icon-doc-csv";
    default:
      return "icon-doc-text";
    }
  }

  @Emit
  @JsMethod
  public void displayErrors() {}

  @Emit
  @JsMethod
  public void displayWarnings() {}

  @JsMethod
  public void removeFile() {
    vue().$emit("removeFile", file);
  }

  @JsMethod
  public String percentageWidth() {
    return "width: " + completePercentage();
  }

  @JsMethod
  public boolean areFilesComplete() {
    return files.stream()
        .noneMatch(file -> !file.isComplete()
            || file.isFailed()
            || file.isPending());
  }

}
