/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.results.progress;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.ServerUsage;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component
public class ProgressBarView extends BasicVueView {
  @Prop CalculationState calculationState;
  @Prop ServerUsage serverUsage;

  @Computed
  public List<Integer> getSteps() {
    return IntStream.range(1, CalculationState.STEPS + 1)
        .boxed()
        .collect(Collectors.toList());
  }

  @JsMethod
  public String getCssClassForColumn(final int step) {
    if (calculationState.isAfter(step)) {
      return "finished-step";
    } else if (calculationState.isBefore(step)) {
      return "empty-step";
    } else { // Current step
      if (!calculationState.isSuccess()) {
        return "current-step-without-success";
      } else if (serverUsage == ServerUsage.ABOVE_AVERAGE) {
        return "warning-usage-high";
      } else if (serverUsage == ServerUsage.VERY_HIGH) {
        return "warning-usage-very-high";
      } else {
        return "current-step"; // No warning
      }
    }
  }

}
