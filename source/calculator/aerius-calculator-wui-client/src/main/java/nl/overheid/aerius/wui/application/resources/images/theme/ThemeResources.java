/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.resources.images.theme;

import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.DataResource.MimeType;

public interface ThemeResources {
  @Source("theme-external-safety.svg")
  @MimeType("image/svg+xml")
  DataResource themExternalSafety();

  @Source("theme-ground.svg")
  @MimeType("image/svg+xml")
  DataResource themeGround();

  @Source("theme-health.svg")
  @MimeType("image/svg+xml")
  DataResource themeHealth();

  @Source("theme-nitrogen-deposition.svg")
  @MimeType("image/svg+xml")
  DataResource themeNitrogenDeposition();

  @Source("theme-odor.svg")
  @MimeType("image/svg+xml")
  DataResource themeOdor();

  @Source("theme-particulate-matter.svg")
  @MimeType("image/svg+xml")
  DataResource themeAirQuality();

  @Source("theme-sound.svg")
  @MimeType("image/svg+xml")
  DataResource themeSound();

  @Source("theme-water.svg")
  @MimeType("image/svg+xml")
  DataResource themeWater();
}
