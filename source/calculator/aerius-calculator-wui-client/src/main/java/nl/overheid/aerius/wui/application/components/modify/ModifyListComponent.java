/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.modify;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-modify-list", components = {
    ButtonIcon.class,
    TooltipComponent.class
})
public class ModifyListComponent extends BasicVueComponent {
  @Prop EventBus eventBus;

  // TODO Refactor out and remove, use emits instead
  @Deprecated @Prop ModifyListActions presenter;

  @Prop boolean selected;
  @Prop boolean singleSelected;
  @Prop boolean multiSelected;

  @Prop boolean enableImport;
  @Prop boolean enableNameLabels;
  @Prop boolean enableEdit;

  @Prop boolean enableDelete;
  @Prop boolean enableCopy;

  @Data boolean nameLabelsEnabled = false;

  @PropDefault("enableDelete")
  boolean enableDeleteDefault() {
    return false;
  }

  @PropDefault("singleSelected")
  boolean singleSelectedDefault() {
    return false;
  }

  @PropDefault("multiSelected")
  boolean multiSelectedDefault() {
    return false;
  }

  @PropDefault("enableCopy")
  boolean enableCopyDefault() {
    return true;
  }

  @JsMethod
  public void toggleNameLabels() {
    this.nameLabelsEnabled = !this.nameLabelsEnabled;

    vue().$emit("show-name-labels", this.nameLabelsEnabled);
    if (presenter != null) {
      presenter.showNameLabels(this.nameLabelsEnabled);
    }
  }

  @JsMethod
  @Emit
  public void newSource() {
    if (presenter != null) {
      presenter.newSource();
    }
  }

  @JsMethod
  @Emit
  public void importFile() {
    if (presenter != null) {
      presenter.importFile();
    }
  }

  @JsMethod
  @Emit
  public void duplicateSource() {
    if (presenter != null) {
      presenter.copy();
    }
  }

  @JsMethod
  @Emit
  public void deleteSource() {
    if (presenter != null) {
      presenter.delete();
    }
  }

  @JsMethod
  @Emit
  public void editSource() {
    if (presenter != null) {
      presenter.edit();
    }
  }
}
