/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.input.suggest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;

import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(name = "suggest-listbox", components = {
    SuggestDropdownModal.class
})
public class SuggestListBoxComponent extends BasicVueView implements IsVueComponent, HasMounted {
  @Prop boolean enabled;
  @Prop String placeholder;
  @Prop @JsProperty List<SuggestListBoxData> options;

  @Prop String selectedValue;
  @Prop SuggestListBoxData selected;

  @Data SuggestListBoxData selectedData;
  @Data String valueData;

  @Prop int modalOffset;
  @Prop int modalWidth;
  @Data boolean showing = false;

  @Ref HTMLElement elem;
  @Data HTMLElement ref;

  @PropDefault("enabled")
  public boolean enabledDefault() {
    return true;
  }

  @Watch(value = "selectedValue", isImmediate = true)
  public void onSelectedValueChange(final String neww) {
    if (neww == null) {
      return;
    }

    options.stream().filter(v -> v.getCode().equals(neww))
        .findFirst().ifPresent(v -> {
          selectedData = v;
        });
  }

  @Watch(value = "selected", isImmediate = true)
  public void onSelectedChange(final SuggestListBoxData neww, final SuggestListBoxData old) {
    if (neww == null) {
      return;
    }

    selectedData = neww;
    if (neww != null) {
      valueData = neww.getName();
    }
  }

  @Watch(value = "selectedData", isImmediate = true)
  public void onSelectedDataChange(final SuggestListBoxData neww, final SuggestListBoxData old) {
    if (neww != null) {
      valueData = neww.getName();
    }
  }

  /**
   * Cleverly match filter options:
   *
   * <pre>
   * Secondarily match for each part of the string (split on nbsp)
   * Then primarily match on nbsp-less trim
   * </pre>
   *
   * Result will be a prioritized list of comprehensive matches.
   */
  @Computed
  public List<SuggestListBoxData> getFilterOptions() {
    if (valueData == null) {
      return options;
    }

    final String query = valueData.toLowerCase();

    final List<String> phrases = Stream.of(query.split(" "))
        .collect(Collectors.toCollection(() -> new ArrayList<>()));

    final Set<SuggestListBoxData> secondaries = options.stream()
        .filter(v -> phrases.stream().allMatch(ph -> v.getDescription().toLowerCase().contains(ph)))
        .collect(Collectors.toCollection(() -> new LinkedHashSet<>()));

    final String trim = Optional.of(query)
        .map(v -> v.replace(" ", ""))
        .orElse("");

    final HashSet<SuggestListBoxData> primaries = options.stream()
        .filter(v -> v.getDescription().replace(" ", "").toLowerCase().contains(trim))
        .collect(Collectors.toCollection(() -> new LinkedHashSet<>()));

    primaries.addAll(secondaries);

    return new ArrayList<>(primaries);
  }

  @Computed
  public void setValue(final String input) {
    this.valueData = input;
    if (!showing) {
      showing = true;
    }
  }

  @Computed
  public String getValue() {
    return this.valueData;
  }

  @JsMethod
  public void toggle() {
    showing = !showing;
  }

  @JsMethod
  public void emptySelect() {
    vue().$emit("empty-select");
  }

  @JsMethod
  public void select(final SuggestListBoxData selection) {
    valueData = selection.getCode();
    vue().$emit("select", selection);
    vue().$emit("select-value", selection.getCode());
    showing = false;
  }

  @JsMethod
  public boolean isStackable() {
    return selectedData == null ? true : selectedData.isStackable();
  }

  @Override
  public void mounted() {
    ref = elem;
  }
}
