/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.category.AnimalType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSideBarrierType;
import nl.overheid.aerius.shared.domain.v2.source.road.TrafficDirection;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.wui.application.components.source.farmlodging.systems.LodgingStackType;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.inland.InlandMovementComponent.InlandMovementType;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandType;
import nl.overheid.aerius.wui.application.domain.source.offroad.OffRoadType;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MooringMaritimeShippingType;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.ScenarioInputListContext.ViewMode;

/**
 * Text specific for emission source pages.
 */
public interface EmissionSourceMessages {
  String sectorGroup(@Select SectorGroup sectorGroup);

  String inputViewMode(@Select ViewMode viewMode);
  String inputViewModeLabel();
  String inputViewModePlaceholder();

  String esEmissionSources();
  String esEmissionLabel(String substance);
  String esNoSources();

  String esButtonCalculate();
  String esButtonCancel();
  String esButtonConfirm();
  String esButtonAbortCalculate();
  String esButtonCopy();
  String esButtonDelete();
  String esButtonDeleteAllSources();
  String esButtonEdit();
  String esButtonEmissionsBySource();
  String esButtonLabels();
  String esButtonLabelsOff();
  String esButtonNewSource();
  String esButtonSave();
  String esButtonImport();

  String esSubSourceEmptyError();

  String esButtonSaveEncumberedText();
  String esButtonSavePreventedText();

  String esEmissionSourceEmptyInfo();

  String esLabelDefaultPrefix(String sourceId);
  String esLabelName();
  String esLabelSectorGroup();
  String esLabelSector();

  String esTitle();
  String esTotalTitle();
  String esLabel(@Select Substance substance);
  String esLabelEmission(@Select Substance substance);
  String esSubstance();

  String esMarkerClusterLabel(int numberOfSources);

  String esLocation();
  String esLocationDrawExplain();
  String esAddPointSource();
  String esAddLineSource();
  String esAddPolygonSource();
  String esLocationDirectExplain();
  String esLocationDirectPlaceholder(@Select String geometryType);
  String esLocationGeometryError();
  String esLocationLineStringLength(String str);
  String esLocationPolygonArea(String str);

  String esButtonClose();
  String esLabelSectorGroupName();
  String esLabelSectorName();
  String esLabelSourceLocation();
  String esDefaultValues();

  String esSectorType(@Select EmissionSourceType sourceType);
  String esCollapsiblePanelTitle(@Select EmissionSourceType sourceType);
  String esMooringMaritimeShippingDescription();
  String esMooringMaritimeShippingSelect(@Select MooringMaritimeShippingType type);
  String esMooringMaritimeShippingAverageResidenceTime();
  String esMooringMaritimeShippingShorePowerFactor();
  String esDetailTypeTitle(@Select EmissionSourceType sourceType);
  String esGeometryType(@Select String geometryType);
  String esGeometryTypeIllegal();

  //Lodging
  String esLodgingDetailTitle();
  String esLodgingDetailFarmSystem();
  String esLodgingDetailAmount();
  String esLodgingDetailFactor();
  String esLodgingDetailReductionFactor();
  String esLodgingDetailReductionReduction();
  String esLodgingDetailEmission();
  String esFarmLodgingConstrainedFootNote();
  String esFarmLodgingEmissionFactor();
  String esFarmLodgingSelectStandard();
  String esFarmLodgingSelectCustom();
  String esFarmLodgingStandardWarningRAVStacking();

  //Lodging Custom
  String esFarmLodgingCustomDescription();
  String esFarmLodgingCustomDescriptionPlaceholder();
  String esFarmLodgingCustomFactor();
  String esFarmLodgingCustomNumberOfAnimals();
  String esFarmLodgingCustomAnimalTypeLabel();
  String esFarmLodgingCustomAnimalType(@Select AnimalType animalType);

  // Lodging Standard
  String esFarmLodgingStandardLodgingCode();
  String esFarmLodgingStandardNumberOfAnimals();
  String esFarmLodgingStandardSystemDefinitionCode();
  String esFarmLodgingStandardAdditionalType(@Select LodgingStackType additionalType);
  String esFarmLodgingStandardFodderMeasureDelete();
  String esFarmLodgingStandardFodderMeasurePlaceholder();
  String esFarmLodgingStandardReductiveSystemPlaceholder();
  String esFarmLodgingStandardReductiveSystemDelete();
  String esFarmLodgingStandardAdditionalSystemPlaceholder();
  String esFarmLodgingStandardAdditionalSystemDelete();

  // Farmland
  String esFarmlandDetailTitle();
  String esFarmlandTypeLabel();
  String esFarmlandDetailEmission(String farmland);
  String esFarmlandType(@Select FarmlandType farmlandType);

  // Road
  String esRoadNetworkShowMoreSources(int increment);
  String esRoadNetwork();
  String esRoadDetailTitle();
  String esRoadDetailRoadType();
  String esRoadDetailTunnelFactor();
  String esRoadDetailElevation();
  String esRoadDetailElevationHeight();
  String esRoadDetailBarrier();
  String esRoadDetailBarrierType();
  String esRoadDetailBarrierHeight();
  String esRoadDetailBarrierDistanceToRoad();
  String esRoadDetailDrivingDirection();
  String esRoadDetailStandardTitle();
  String esRoadDetailStandardVehiclesTitle();
  String esRoadDetailStandardStagnationTitle();
  String esRoadDetailCustomTitle();
  String esRoadDetailCustomLabel();
  String esRoadDetailCustomEmissionPerVehicle();
  String esRoadDetailEuroClass();
  String esRoadDetailSpecificTitle();
  String esRoadDetailNumberOfVehicles();
  String esRoadDetailEmission();
  String esRoadDetailSubstance();
  String esRoadDetailTotal();
  String esStandardRoadVehicleTypeLightTitle();
  String esStandardRoadVehicleTypeNormalTitle();
  String esStandardRoadVehicleTypeHeavyTitle();
  String esStandardRoadVehicleTypeBusTitle();
  String esStandardRoadVehicleType(@Select VehicleType vehicleType);
  String esRoadDrivingDirection(@Select TrafficDirection direction);
  String esRoadSelectStandard();
  String esRoadSelectCustom();
  String esRoadCustomDescription();
  String esRoadCustomNumberOfVehicles();
  String esRoadCustomEmissionFactorTitle();
  String esRoadStandardDrivingDirectionWarning();
  String esRoadStandardRoadSpeed();
  String esRoadStandardVehicles();
  String esRoadStandardStagnation();
  String esRoadCategory(int speed);
  String esRoadCategoryStrict(int speed);
  String esRoadCategoryUnknown();
  String esRoadEuroClass();
  String esRoadEuroClassSubSourceRow(String euroclass);
  String esRoadMobileSourceCategoriesPlaceHolder();
  String esRoadBarrierTypeNone();
  String esRoadBarrierType(@Select RoadSideBarrierType barrierType);
  String esShippingDescription();
  String esShippingStandardShipCode();
  String esShippingMovements();
  String esShippingAverageResidenceTime();
  String esShippingShorePowerFactor();
  String esShippingPercentageLaden();
  String esShippingVisits();

  String esMaritimeStandardShipCode();

  // Offroad Mobile
  String esOffRoadMobileSelectStandard();
  String esOffRoadMobileSelectCustom();
  String esOffRoadMobileStandardDescription();
  String esOffRoadMobileCategoryLabel();
  String esOffRoadMobileCategory(@Select OffRoadType type);
  String esOffRoadMobileStandardLiterFuel();
  String esOffRoadMobileStandardOperatingHours();
  String esOffRoadMobileStandardLiterAdBlue();
  String esOffRoadStandardFuelAdblueRatioWarning(double maxRatio);

  // Shipping
  String esInlandShippingDetailTitle();
  String esMaritimeShippingDetailTitle();
  String esWaterwayCategory();
  String esWaterwayDirection();
  String esWaterwayDirectionDetail();
  String esWaterwayDirectionType(@Select WaterwayDirection waterwayDirection);
  String esMovement(@Select InlandMovementType movement);
  String esMovementAmountOfShips();
  String esMovementLoaded();
  String esShipWaterwayNotAllowedCombination(String shipCategory, String waterwayCategory);
  String esShipDescription();
  String esShipsVisitsPerTimeUnit();
  String esShipsMovementsPerTimeUnit();
  String esShipTimeunit();
  String esShipPercentageLoad();
  String esShipEmission();
  String esShipTotalEmission();
  String esShipAverageResidenceTime();
  String esShipShorePowerFactor();
  String esShipRoutingDeleteConfirm(String route);

}
