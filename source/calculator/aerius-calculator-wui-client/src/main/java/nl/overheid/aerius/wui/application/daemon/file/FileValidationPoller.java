/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.file;

import java.util.Arrays;
import java.util.Optional;

import javax.inject.Inject;

import com.google.gwt.user.client.Timer;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.File;

import jsinterop.base.Js;

import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.js.file.FileValidationStatus;
import nl.overheid.aerius.js.file.ValidationStatus;
import nl.overheid.aerius.wui.application.context.FileContext;
import nl.overheid.aerius.wui.application.event.RegisterFileEvent;
import nl.overheid.aerius.wui.application.service.FileServiceAsync;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

public class FileValidationPoller {
  private static final int INTERVAL = 1000;

  @Inject private FileContext context;
  @Inject private FileServiceAsync service;
  @Inject private EventBus eventBus;

  private boolean running;

  private final Timer timer = new Timer() {
    @Override
    public void run() {
      doFetch();
    }
  };

  public void start() {
    if (running) {
      return;
    }

    running = true;
    SchedulerUtil.delay(() -> doFetch());
  }

  public boolean isRunning() {
    return running;
  }

  private void doFetch() {
    final Optional<FileUploadStatus> fileOptionalJson = getPendingFile();
    if (!fileOptionalJson.isPresent()) { // isEmpty() is undefined in GWT
      running = false;
      return;
    }
    final FileUploadStatus file = fileOptionalJson.get();

    service.fetchFileValidation(file.getFileCode(), AppAsyncCallback.create(
        fileValidation -> {
          if (fileValidation.getValidationStatus() != ValidationStatus.PENDING) {
            for (final String childFileCode : Arrays.asList(fileValidation.getChildFileCodes())) {
              addNewChildFile(childFileCode, file.getFile());
            }
            process(fileValidation);
          }
        }, e -> processAsError(file.getFileCode())));
    finish();
  }

  private void addNewChildFile(final String fileCode, final File file) {
    final FileUploadStatus newFile = new FileUploadStatus();
    newFile.setFileCode(fileCode);
    newFile.setFile(file);
    newFile.setComplete(true);
    newFile.setCompleteRatio(1D);
    newFile.setPending(true);
    context.add(newFile);
  }

  private void processAsError(final String fileCode) {
    context.findFile(fileCode).ifPresent(file -> {
      file.clear();
      file.setValidated(true);
      file.setFailed(true);
      file.setStatus(ValidationStatus.FAILED);
      file.setPending(false);
    });
  }

  private void process(final FileValidationStatus fileValidation) {
    context.findFile(fileValidation.getFileCode()).ifPresent(file -> {
      file.clear();
      file.setSituationName(fileValidation.getSituationName());
      file.setSituationType(fileValidation.getSituationType());
      file.setStatus(fileValidation.getValidationStatus());
      file.setSituationStats(fileValidation.getSituationStats());
      setWarningsAndErrors(fileValidation, file);
      file.setValidated(true);
      file.setPending(false);
      eventBus.fireEvent(new RegisterFileEvent(file));
    });
  }

  private void setWarningsAndErrors(final FileValidationStatus fileValidation, final FileUploadStatus file) {
    file.getErrors().addAll(Js.cast(Arrays.asList(fileValidation.getErrors())));
    file.getWarnings().addAll(Js.cast(Arrays.asList(fileValidation.getWarnings())));
  }

  private void finish() {
    if (hasPendingFiles()) {
      timer.schedule(INTERVAL);
    } else {
      running = false;
    }
  }

  private Optional<FileUploadStatus> getPendingFile() {
    return context.getFiles().stream()
        .filter(v -> v.getFileCode() != null && v.isPending())
        .findFirst();
  }

  private boolean hasPendingFiles() {
    return context.getFiles().stream()
        .anyMatch(v -> !v.isValidated());
  }
}
