/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.standard;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.TreeSet;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.TrafficDirection;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.source.road.standard.RoadStandardValidators.RoadStandardValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.road.RoadSourceUtil;
import nl.overheid.aerius.wui.application.domain.source.road.StandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerVehicleTypes;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(name = "aer-road-standard",
customizeOptions = {
    RoadStandardValidators.class
}, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    MinimalInputComponent.class,
    RoadStandardVehicleTypeRowComponent.class,
    ValidationBehaviour.class
})
public class RoadStandardEmissionComponent extends ErrorWarningValidator implements HasCreated,
  HasValidators<RoadStandardValidations> {
  @Prop @JsProperty StandardVehicles standardVehicles;
  @Prop @JsProperty RoadType roadType;
  @Prop TrafficDirection trafficDirection;
  @Data String roadSpeedV;

  @Data @Inject ApplicationContext applicationContext;

  @JsProperty(name = "$v") RoadStandardValidations validations;

  /**
   * @reviewers please comment what this is so I can document it
   */
  /**
   * When maximum speed is strict enforcement add this marker.
   */
  private static final String STRICT = "s";

  @Watch(value = "standardVehicles", isImmediate = true)
  public void onStandardVehiclesChange() {
    roadSpeedV = String.valueOf(standardVehicles.getMaximumSpeed());
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Override
  @Computed
  public RoadStandardValidations getV() {
    return validations;
  }

  @Computed
  protected String getTimeUnit() {
    return standardVehicles.getTimeUnit().name();
  }

  @Computed
  protected void setTimeUnit(final String timeUnit) {
    standardVehicles.setTimeUnit(TimeUnit.valueOf(timeUnit));
  }

  @Computed
  protected String getMaximumSpeed() {
    String maximumSpeed = String.valueOf(standardVehicles.getMaximumSpeed());
    maximumSpeed = standardVehicles.isStrictEnforcement() ? STRICT + maximumSpeed : maximumSpeed;
    return maximumSpeed;
  }

  @Computed
  protected void setMaximumSpeed(final String maximumSpeed) {
    standardVehicles.setMaximumSpeed(Integer.valueOf(maximumSpeed.replace(STRICT, "")));
    standardVehicles.setStrictEnforcement(maximumSpeed.contains(STRICT));
    roadSpeedV = maximumSpeed.replace(STRICT, "");
  }

  @Computed
  protected HashSet<String> getMaximumSpeedCategories() {
    final HashSet<String> list = new HashSet<String>();
    for (final Integer speed : getRoadCategories().getMaximumSpeedCategories(roadType, RoadSpeedType.NATIONAL_ROAD)) {
      list.add(String.valueOf(speed));
      if (RoadSourceUtil.determineStrictForMaxSpeed(getRoadCategories(), roadType, speed)) {
        list.add(STRICT + String.valueOf(speed));
      }
    }
    return list;
  }

  private RoadEmissionCategories getRoadCategories() {
    return applicationContext.getConfiguration().getSectorCategories().getRoadEmissionCategories();
  }

  @Computed("valuesPerVehicleType")
  protected ValuesPerVehicleTypes getValuesPerVehicleType() {
    return standardVehicles.getValuesPerVehicleTypes();
  }

  @Computed("vehicleTypes")
  protected EnumSet<VehicleType> getVehicleTypes() {
    return EnumSet.allOf(VehicleType.class);
  }

  @Computed("isInvalid")
  protected boolean isInvalid() {
    return isSpeedCategoriesAvailable() && validations.roadSpeedV.invalid;
  }

  @JsMethod
  protected String getMaximumSpeedMessage(final String maximumSpeed) {
    final int max = Integer.valueOf(maximumSpeed.replace(STRICT, ""));
    return StandardRoadCategoryUtil.roadCategoryDisplayText(i18n, max, maximumSpeed.contains(STRICT));
  }

  @JsMethod
  protected String roadSpeedRequiredError() {
    return i18n.errorRoadSpeedRequired();
  }

  @JsMethod
  protected boolean isSpeedCategoriesAvailable() {
    final TreeSet<Integer> speedCategories = getRoadCategories().getMaximumSpeedCategories(roadType, RoadSpeedType.NATIONAL_ROAD);
    return speedCategories.size() > 1;
  }
}
