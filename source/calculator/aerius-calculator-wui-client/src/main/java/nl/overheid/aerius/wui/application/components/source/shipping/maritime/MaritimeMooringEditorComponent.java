/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.shipping.maritime;

import java.util.List;
import java.util.stream.Collectors;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.MooringType;
import nl.overheid.aerius.wui.application.domain.source.MaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MooringMaritimeShippingESFeature;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-maritime-mooring-emission-editor",
components = {
    LabeledInputComponent.class
})
public class MaritimeMooringEditorComponent extends BasicVueComponent {
  @Prop EventBus eventBus;
  @Prop MaritimeShippingESFeature source;
  @Prop @JsProperty List<MooringMaritimeShippingESFeature> mooringMaritime;

  @JsMethod
  public boolean isESType(final EmissionSourceType type) {
    return source.getEmissionSourceType() == type;
  }

  @Computed
  protected String getMooringAId() {
    return source.getMooringAId() == null ? "" : source.getMooringAId();
  }

  @Computed
  protected void setMooringAId(final String id) {
    source.setMooringAId("".equals(id) ? null : id);
  }

  @Computed
  protected String getMooringBId() {
    return source.getMooringBId() == null ? "" : source.getMooringBId();
  }

  @Computed
  protected void setMooringBId(final String id) {
    source.setMooringBId("".equals(id) ? null : id);
  }

  @JsMethod
  protected List<MooringMaritimeShippingESFeature> getAllowedMoorings(final MooringType mooringType) {
    return mooringMaritime.stream()
        .filter(m -> includeMooringType(m, mooringType))
        .collect(Collectors.toList());
  }

  @JsMethod
  protected String getLabel(final MooringMaritimeShippingESFeature feature) {
    return feature.getLabel().isEmpty() ? feature.getId() : feature.getLabel();
  }

  /**
   * Exclude the mooring Feature that is selected from the opposite direction of A and B
   *
   * @param feature
   * @param mooringType
   * @return
   */
  private boolean includeMooringType(final MooringMaritimeShippingESFeature feature, final MooringType mooringType) {
    return MooringType.A == mooringType ? !feature.getId().equals(getMooringBId()) : !feature.getId().equals(getMooringAId());
  }
}
