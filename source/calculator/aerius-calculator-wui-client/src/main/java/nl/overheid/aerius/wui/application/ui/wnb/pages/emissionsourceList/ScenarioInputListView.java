/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.place.BuildingPlace;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.ScenarioInputListContext.ViewMode;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.building.BuildingListComponent;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.situation.SituationEditorView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.source.EmissionSourceListComponent;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    SimplifiedListBoxComponent.class,
    SituationEditorView.class,
    EmissionSourceListComponent.class,
    BuildingListComponent.class,
    LabeledInputComponent.class,
    VerticalCollapse.class,
})
public class ScenarioInputListView extends BasicVueView implements HasCreated {
  @Prop EventBus eventBus;

  @Inject @Data ScenarioContext context;
  @Inject @Data ScenarioInputListContext inputListContext;

  @Inject PlaceController placeController;

  @Override
  public void created() {
    // TODO Like in EmissionSourceView, move away from this type of logic and use
    // place-based state instead

    final ViewMode viewMode = placeController.getPreviousPlace() instanceof BuildingPlace ? ViewMode.BUILDING : ViewMode.EMISSION_SOURCES;
    inputListContext.setViewMode(viewMode);
  }

  @JsMethod
  public void setViewMode(final ViewMode viewMode) {
    inputListContext.setViewMode(viewMode);
  }

  @Computed("isBuildings")
  public boolean isBuildings() {
    return inputListContext.getViewMode() == ViewMode.BUILDING;
  }

  @Computed("isEmissionSources")
  public boolean isEmissionSOurces() {
    return inputListContext.getViewMode() == ViewMode.EMISSION_SOURCES;
  }

  @Computed
  public SituationContext getActiveSituation() {
    return context.getActiveSituation();
  }

  @Computed("hasActiveSituation")
  public boolean hasActiveSituation() {
    return context.hasActiveSituation();
  }

}
