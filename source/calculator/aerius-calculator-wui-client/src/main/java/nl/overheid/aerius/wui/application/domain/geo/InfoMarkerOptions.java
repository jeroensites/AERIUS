/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.geo;

import java.util.List;

import jsinterop.annotations.JsType;

import nl.overheid.aerius.wui.application.util.json.JsonSerializable;

@JsType
public class InfoMarkerOptions implements JsonSerializable {

  private int receptorId;
  private String calculationYear;
  private String calculationCode;
  private List<String> situations;


  public int getReceptorId() {
    return receptorId;
  }

  public void setReceptorId(final int receptorId) {
    this.receptorId = receptorId;
  }

  public String getCalculationYear() {
    return calculationYear;
  }

  public void setCalculationYear(final String calculationYear) {
    this.calculationYear = calculationYear;
  }

  public String getCalculationCode() {
    return calculationCode;
  }

  public void setCalculationCode(final String calculationCode) {
    this.calculationCode = calculationCode;
  }

  public List<String> getSituations() {
    return situations;
  }

  public void setSituationIds(final List<String> situations) {
    this.situations = situations;
  }

}
