/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import java.util.List;

import com.google.gwt.core.client.GWT;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;

/*
 * Util class for road source
 */
public class RoadSourceUtil {

  /**
   * Determine if for a supplied max speed and @RoadType a strict enforcement exists.
   *
   * @param roadEmissionCategories @RoadEmissionCategories road categories.
   * @param roadType @RoadType supplied roadtype.
   * @param MaxSpeed speed to test if strict enforcement exists.
   * @return boolean
   */
  public static boolean determineStrictForMaxSpeed(final RoadEmissionCategories roadEmissionCategories,
      RoadType roadType, Integer maxSpeed) {
    return roadEmissionCategories.getRoadEmissionCategory(
        new RoadEmissionCategory(roadType, VehicleType.LIGHT_TRAFFIC, true, maxSpeed, null)) != null;
  }

  /**
   * Update the emissionFactors when year of onRoadMobileSourceCategory changes
   *
   * @param roadMobilesourceCategories
   * @param onRoadMobileSourceCategory
   * @param substances
   * @param roadType
   * @param year
   */
  public static EmissionFactors updateEmissionFactors(final List<OnRoadMobileSourceCategory> roadMobilesourceCategories,
      final String onRoadMobileSourceCategory, final List<Substance> substances, RoadType roadType, final String year) {
    EmissionFactors emissionFactors = EmissionFactors.create();
    for (final OnRoadMobileSourceCategory category : roadMobilesourceCategories) {
      if (category.getCode().equals(onRoadMobileSourceCategory)) {
        for (final Substance substance : substances) {
          try {
            final int yearInt = Integer.parseInt(year);
            emissionFactors.setEmissionFactor(substance, category.getEmissionFactor(substance, roadType, yearInt));
          } catch (final NumberFormatException e) {
            throw new RuntimeException("Could not parse year into number: " + year);
          }
        }
        break;
      }
    }
    return emissionFactors;
  }

}
