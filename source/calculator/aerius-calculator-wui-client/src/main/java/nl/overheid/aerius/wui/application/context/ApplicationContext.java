/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.Map;

import com.google.inject.Singleton;

import nl.overheid.aerius.shared.config.AppThemeConfiguration;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.scenario.SituationType;

@Singleton
public class ApplicationContext {
  private Theme activeTheme = null;
  private ApplicationConfiguration configuration = null;
  private Theme oneTheme = null;

  public Theme getActiveTheme() {
    return activeTheme;
  }

  public AppThemeConfiguration getActiveThemeConfiguration() {
    return configuration.getAppThemeConfiguration(activeTheme);
  }

  /**
   * If there is only 1 theme this method returns the theme, otherwise it returns null;
   * @return null or the only available theme
   */
  public Theme getOneTheme() {
    return oneTheme;
  }

  public void setActiveTheme(final Theme activeTheme) {
    this.activeTheme = activeTheme;
  }

  public ApplicationConfiguration getConfiguration() {
    return configuration;
  }

  public void setConfiguration(final ApplicationConfiguration configuration) {
    this.configuration = configuration;
    initOneTheme();
  }

  private void initOneTheme() {
    if (configuration.getContexts().size() == 1) {
      configuration.getContexts().entrySet().stream().findAny().ifPresent(context -> oneTheme = context.getKey());
    }
  }

  public int getMaxNumberOfSituationsForType(final SituationType type) {
    if (getActiveThemeConfiguration() == null) {
      return 0;
    }
    final Map<SituationType, Integer> map = getActiveThemeConfiguration().getMaxNumberOfSituationsPerType();
    if (map.containsKey(type)) {
      return map.get(type);
    } else {
      return getActiveThemeConfiguration().getMaxNumberOfSituations();
    }
  }

  public int getMaxNumberOfSituations(final boolean isImport) {
    return isImport ? getActiveThemeConfiguration().getMaxNumberOfFilesSituationImport()
      : getMaxNumberOfSituationsForType(SituationType.UNKNOWN);
  }
}
