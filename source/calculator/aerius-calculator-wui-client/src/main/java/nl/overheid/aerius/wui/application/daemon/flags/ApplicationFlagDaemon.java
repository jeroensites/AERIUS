/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.flags;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.wui.application.event.ApplicationFinishedLoadingEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.config.ApplicationFlags;

@Singleton
public class ApplicationFlagDaemon extends BasicEventComponent implements Daemon {
  private static final ApplicationFlagDaemonEventBinder EVENT_BINDER = GWT.create(ApplicationFlagDaemonEventBinder.class);

  interface ApplicationFlagDaemonEventBinder extends EventBinder<ApplicationFlagDaemon> {}

  private final ApplicationFlags flags;

  @Inject
  public ApplicationFlagDaemon(final ApplicationFlags flags) {
    this.flags = flags;
    init();
  }

  @Override
  public void init() {
    final boolean extendedResults = Window.Location.getParameterMap().keySet().stream().anyMatch(v -> "extendedResults".equalsIgnoreCase(v));
    final boolean internal = Window.Location.getParameterMap().keySet().stream().anyMatch(v -> "internal".equalsIgnoreCase(v));

    flags.setExtendedResults(extendedResults);
    flags.setInternal(internal);
  }

  @EventHandler
  public void onApplicationFinishedLoadingEvent(final ApplicationFinishedLoadingEvent e) {
    if (flags.isExtendedResults()) {
      NotificationUtil.broadcastWarning(eventBus, M.messages().flagsExtendedResults());
    }

  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
