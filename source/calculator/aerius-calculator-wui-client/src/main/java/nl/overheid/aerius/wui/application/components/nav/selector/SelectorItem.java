/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.nav.selector;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public final class SelectorItem {
  private final String name;
  private final String component;

  private final Function<List<SelectorItemOption>, Boolean> isVisible;
  private final Supplier<List<SelectorItemOption>> options;
  private final Function<List<SelectorItemOption>, SelectorItemOption> selectedOption;
  private final BiFunction<SelectorItemOption, SelectorItemOption, Boolean> isSelected;

  public SelectorItem(final String name, final String component, final Supplier<List<SelectorItemOption>> options,
      final Function<List<SelectorItemOption>, SelectorItemOption> selectedOption,
      final Function<List<SelectorItemOption>, Boolean> isVisible) {
    this(name, component, options, selectedOption, (a, b) -> a.getValue().equals(b.getValue()), isVisible);
  }

  public SelectorItem(final String name, final String component, final Supplier<List<SelectorItemOption>> options,
      final Function<List<SelectorItemOption>, SelectorItemOption> selectedOption) {
    this(name, component, options, selectedOption, (a, b) -> a.getValue().equals(b.getValue()));
  }

  public SelectorItem(final String name, final String component, final Supplier<List<SelectorItemOption>> options,
      final Function<List<SelectorItemOption>, SelectorItemOption> selectedOption,
      final BiFunction<SelectorItemOption, SelectorItemOption, Boolean> isSelected) {
    this(name, component, options, selectedOption, isSelected, opts -> opts.size() >= 1);
  }

  public SelectorItem(final String name, final String component, final Supplier<List<SelectorItemOption>> options,
      final Function<List<SelectorItemOption>, SelectorItemOption> selectedOption,
      final BiFunction<SelectorItemOption, SelectorItemOption, Boolean> isSelected,
      final Function<List<SelectorItemOption>, Boolean> isVisible) {
    this.name = name;
    this.component = component;
    this.options = options;
    this.selectedOption = selectedOption;
    this.isSelected = isSelected;
    this.isVisible = isVisible;
  }

  public SelectorItemOption getSelectedOption() {
    return selectedOption.apply(getOptions());
  }

  public List<SelectorItemOption> getOptions() {
    return options.get();
  }

  public String getComponent() {
    return component;
  }

  public String getName() {
    return name;
  }

  public boolean isVisible() {
    return isVisible.apply(options.get());
  }

  public boolean isSelected(final SelectorItemOption option) {
    return isSelected.apply(getSelectedOption(), option);
  }
}
