/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;

@Singleton
public class BuildingListContext {
  private final @JsProperty List<BuildingFeature> selections = new ArrayList<>();
  private BuildingFeature peekBuilding = null;

  public BuildingFeature getLooseSelection() {
    return Optional.ofNullable(getSingleSelection())
        .orElse(peekBuilding);
  }

  public boolean hasSingleSelection() {
    return selections.size() == 1;
  }

  public boolean hasMultiSelection() {
    return selections.size() > 1;
  }

  public boolean hasSelection() {
    return !selections.isEmpty();
  }

  public BuildingFeature getSingleSelection() {
    return selections.isEmpty() ? null : selections.get(0);
  }

  public List<BuildingFeature> getSelections() {
    return selections;
  }

  public void addSelection(final BuildingFeature selection) {
    selections.add(selection);
  }

  public void removeSelection(final BuildingFeature selection) {
    selections.remove(selection);
  }

  public void setSingleSelection(final BuildingFeature selection) {
    this.selections.clear();
    addSelection(selection);
  }

  public boolean hasLooseSelection() {
    return getLooseSelection() != null;
  }

  public void reset() {
    // Note: If expanding with more resets, make sure assumptions are still met
    selections.clear();
    peekBuilding = null;
  }

  public boolean isSelected(final BuildingFeature building) {
    if (building == null || selections == null) {
      return false;
    }

    return selections.stream()
        .anyMatch(v -> v.getId().equals(building.getId()));
  }

  public void setPeekBuilding(final BuildingFeature peekBuilding) {
    this.peekBuilding = peekBuilding;
  }

  public void clearPeekBuilding() {
    this.peekBuilding = null;
  }
}
