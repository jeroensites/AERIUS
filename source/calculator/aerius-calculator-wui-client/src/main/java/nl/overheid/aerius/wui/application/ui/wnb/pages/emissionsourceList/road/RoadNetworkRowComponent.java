/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.road;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDeselectFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourcePeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceUnpeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.FeatureZoomCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsibleButton;
import nl.overheid.aerius.wui.application.components.source.EmissionSourceRowComponent;
import nl.overheid.aerius.wui.application.context.EmissionSourceListContext;
import nl.overheid.aerius.wui.application.context.RoadNetworkContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.util.DoubleClickUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-road-network-row", components = {
    EmissionSourceRowComponent.class,
    VerticalCollapseGroup.class,
    CollapsibleButton.class
})
public class RoadNetworkRowComponent extends BasicVueComponent implements IsVueComponent {
  private static final int DEFAULT_LIMIT = 100;
  @Data int limit = DEFAULT_LIMIT;
  @Data int limitIncrement = 100;

  @Inject @Data ScenarioContext context;
  @Inject @Data EmissionSourceListContext listContext;
  @Inject @Data RoadNetworkContext roadNetworkContext;
  @Inject EventBus eventBus;

  @Data boolean selected = false;

  private final DoubleClickUtil<EmissionSourceFeature> doubleClickUtil = new DoubleClickUtil<>();

  @Computed
  public SituationContext getActiveSituation() {
    return context.getActiveSituation();
  }

  public Stream<EmissionSourceFeature> getAllRoadSources() {
    return Optional.ofNullable(getActiveSituation())
        .map(v -> v.getSources())
        .orElse(new ArrayList<>())
        .stream()
        .filter(v -> RoadEmissionSourceTypes.isRoadSource(v))
        .filter(v -> selected || roadNetworkContext.isHighlighted(v));
  }

  @Computed("areSourcesLimited")
  public boolean areSourcesLimited() {
    return getAllRoadSources().count() > limit;
  }

  @Computed
  public List<EmissionSourceFeature> getEmissionSources() {
    return getAllRoadSources()
        .sorted((o1, o2) -> -Boolean.compare(roadNetworkContext.isHighlighted(o1), roadNetworkContext.isHighlighted(o2)))
        .limit(calculateLimit())
        .collect(Collectors.toList());
  }

  private long calculateLimit() {
    return Math.max(roadNetworkContext.highlightedSize(), limit);
  }

  @Computed("hasSelectedRoadEmissionSources")
  public boolean hasSelectedRoadEmissionSources() {
    return !getEmissionSources().isEmpty();
  }

  @JsMethod
  public void increaseLimit() {
    limit = Math.max(limit, roadNetworkContext.highlightedSize()) + limitIncrement;
  }

  @Computed("isRoadNetworkRowSelected")
  public boolean isRoadNetworkRowSelected() {
    return selected && !Optional.ofNullable(listContext.getSelection())
        .map(v -> RoadEmissionSourceTypes.isRoadSource(v))
        .orElse(false);
  }

  @Watch("isRoadNetworkRowSelected()")
  public void onIsRoadNetworkRowSelectedChange(final boolean neww) {
    vue().$emit("select", isRoadNetworkRowSelected());
  }

  @JsMethod
  public void toggleNetwork() {
    selected = !selected;
    limit = Math.max(DEFAULT_LIMIT, roadNetworkContext.highlightedSize() + limitIncrement);

    // If closing the network, deselect feature if it is a road source
    if (!selected) {
      Optional.ofNullable(listContext.getSelection())
          .map(v -> RoadEmissionSourceTypes.isRoadSource(v))
          .ifPresent(selected -> {
            eventBus.fireEvent(new EmissionSourceDeselectFeatureCommand());
          });
    }
  }

  @JsMethod
  public void peekSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new EmissionSourcePeekFeatureCommand(source));
  }

  @JsMethod
  public void unpeekSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new EmissionSourceUnpeekFeatureCommand(source));
  }

  @JsMethod
  public void selectSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new EmissionSourceFeatureToggleSelectCommand(source));
  }

  @JsMethod
  public void focusSource(final EmissionSourceFeature source) {
    eventBus.fireEvent(new FeatureZoomCommand(source));
  }

  @JsMethod
  public void clickSource(final EmissionSourceFeature source) {
    doubleClickUtil.click(source, v -> selectSource(v), v -> focusSource(v));
  }

}
