/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.wnb.pages.export.metadata;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasActivated;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.wui.application.components.input.InputErrorComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "export-meta-data", customizeOptions = {
    ExportMetaDataViewValidators.class
}, directives = {
    ValidateDirective.class
}, components = {
    LabeledInputComponent.class,
    InputErrorComponent.class
})
public class ExportMetaDataView extends BasicVueComponent implements HasCreated, HasActivated {
  @Inject ScenarioContext scenarioContext;
  @Prop boolean validationRequired;

  @Data String corporationV;
  @Data String projectNameV;
  @Data String descriptionV;
  @Data String streetAddressV;
  @Data String postcodeV;
  @Data String cityV;

  @JsProperty(name = "$v") ExportMetaDataViewValidators.ExportMetaDataValidations validation;

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
    activated();
  }

  @Override
  public void activated() {
    corporationV = scenarioContext.getScenarioMetaData().getCorporation();
    projectNameV = scenarioContext.getScenarioMetaData().getProjectName();
    descriptionV = scenarioContext.getScenarioMetaData().getDescription();
    streetAddressV = scenarioContext.getScenarioMetaData().getStreetAddress();
    postcodeV = scenarioContext.getScenarioMetaData().getPostcode();
    cityV = scenarioContext.getScenarioMetaData().getCity();
  }

  @Computed
  public ExportMetaDataViewValidators.ExportMetaDataValidations getV() {
    return validation;
  }

  @Computed
  public String getCorporation() {
    return corporationV;
  }

  @Computed
  public void setCorporation(final String corporation) {
    this.corporationV = corporation;
    scenarioContext.getScenarioMetaData().setCorporation(corporation);
  }

  @Computed
  public String getProjectName() {
    return projectNameV;
  }

  @Computed
  public void setProjectName(final String projectName) {
    this.projectNameV = projectName;
    scenarioContext.getScenarioMetaData().setProjectName(projectName);
  }

  @Computed
  public String getDescription() {
    return descriptionV;
  }

  @Computed
  public void setDescription(final String description) {
    descriptionV = description;
    scenarioContext.getScenarioMetaData().setDescription(description);
  }

  @Computed
  public String getStreetAddress() {
    return streetAddressV;
  }

  @Computed
  public void setStreetAddress(final String streetAddress) {
    streetAddressV = streetAddress;
    scenarioContext.getScenarioMetaData().setStreetAddress(streetAddress);
  }

  @Computed
  public String getPostcode() {
    return postcodeV;
  }

  @Computed
  public void setPostcode(final String postcode) {
    postcodeV = postcode;
    scenarioContext.getScenarioMetaData().setPostcode(postcode);
  }

  @Computed
  public String getCity() {
    return cityV;
  }

  @Computed
  public void setCity(final String city) {
    cityV = city;
    scenarioContext.getScenarioMetaData().setCity(city);
  }

  @JsMethod
  public Validations ifValidationRequired(Validations validation) {
    return validationRequired ? validation : null;
  }

  @JsMethod
  public void touchAll() {
    validation.corporationV.$touch();
    validation.projectNameV.$touch();
    validation.descriptionV.$touch();
    validation.streetAddressV.$touch();
    validation.postcodeV.$touch();
    validation.cityV.$touch();
  }

  @JsMethod
  public boolean isError() {
    return validation.corporationV.error ||
        validation.projectNameV.error ||
        validation.descriptionV.error ||
        validation.streetAddressV.error ||
        validation.postcodeV.error ||
        validation.cityV.error;
  }
}
