/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.context.dummy;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.Random;

import ol.Coordinate;
import ol.geom.Point;

import elemental2.core.JsArray;

import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.ops.HeatContentType;
import nl.overheid.aerius.shared.domain.sector.category.AnimalType;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadManager;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSideBarrierType;
import nl.overheid.aerius.shared.domain.v2.source.road.TrafficDirection;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.FarmLodgingESFeature;
import nl.overheid.aerius.wui.application.domain.source.FarmlandESFeature;
import nl.overheid.aerius.wui.application.domain.source.GenericESFeature;
import nl.overheid.aerius.wui.application.domain.source.InlandMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.InlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MaritimeMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MooringInlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MooringMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.SRM2RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.farm.AdditionalLodgingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomFarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farm.FarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farm.LodgingFodderMeasure;
import nl.overheid.aerius.wui.application.domain.source.farm.ReductiveLodgingSystem;
import nl.overheid.aerius.wui.application.domain.source.farm.StandardFarmLodging;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandActivity;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandType;
import nl.overheid.aerius.wui.application.domain.source.road.CustomVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.RoadSideBarrier;
import nl.overheid.aerius.wui.application.domain.source.road.SpecificVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.StandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.ValuesPerVehicleTypes;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.InlandShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.MooringInlandShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.StandardInlandShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.inland.StandardMooringInlandShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MaritimeShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.MooringMaritimeShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.StandardMaritimeShipping;
import nl.overheid.aerius.wui.application.domain.source.shipping.maritime.StandardMooringMaritimeShipping;

/**
 * Dummy class until actual data is available.
 */
public class DummyEmissionSources {

  private static final String BUILDING_1 = "Gebouw.1";
  private static final String BUILDING_2 = "Gebouw.2";

  public static List<BuildingFeature> buildings() {
    final List<BuildingFeature> buildings = new ArrayList<>();

    final BuildingFeature building1 = BuildingFeature.create();
    building1.setId("1");
    building1.setGmlId(BUILDING_1);
    building1.setLabel("Gebouw 1.");
    building1.setHeight(17);
    building1.setGeometry(RandomGeometry.randomGeometry(GeometryType.POLYGON));
    final BuildingFeature building2 = BuildingFeature.create();
    building2.setId("2");
    building2.setGmlId(BUILDING_2);
    building2.setLabel("Gebouw 2.");
    building2.setHeight(31);
    building2.setGeometry(RandomGeometry.randomGeometry(GeometryType.POLYGON));

    buildings.add(building1);
    buildings.add(building2);

    return buildings;
  }

  public static List<EmissionSourceFeature> sources(final int count, final boolean linkBuildings) {
    final List<EmissionSourceFeature> features = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      if (i == 0) {
        final FarmLodgingESFeature farmFeature = FarmLodgingESFeature.create(CharacteristicsType.OPS);
        addGlobalSettings(farmFeature, "Landbouw ", i, 4110);
        if (linkBuildings) {
          final OPSCharacteristics characteristics = Js.uncheckedCast(farmFeature.getCharacteristics());
          characteristics.setBuildingId(BUILDING_1);
        }
        farmFeature.setSubSources(createFarmLodgings());
        farmFeature.setGeometry(new Point(new Coordinate(150000, 450000)));
        FeatureUtil.addAndSetId(features, farmFeature);
      } else if (i == 1) {
        final FarmlandESFeature farmLand = FarmlandESFeature.create(CharacteristicsType.OPS);
        addGlobalSettings(farmLand, "Landbouw grond ", i, 4150);
        if (linkBuildings) {
          final OPSCharacteristics characteristics = Js.uncheckedCast(farmLand.getCharacteristics());
          characteristics.setBuildingId(BUILDING_2);
        }
        farmLand.setSubSources(createFarmLandActivites());
        FeatureUtil.addAndSetId(features, farmLand);
      } else if (i == 2) {
        final GenericESFeature generic = GenericESFeature.create(CharacteristicsType.OPS);
        addGlobalSettings(generic, "Mestopslag ", i, 4120);
        generic.setEmission(Substance.NOX, 1.1234567D);
        generic.setEmission(Substance.NH3, 2.1D);
        FeatureUtil.addAndSetId(features, generic);
      } else if (i <= 3) {
        final SRM2RoadESFeature srm2RoadFeature = SRM2RoadESFeature.create(3111);
        addGlobalSettingsRoad(srm2RoadFeature, "Wegverkeer snelweg alle snelheden", i, 3111);
        srm2RoadFeature.setFreeway(true);
        srm2RoadFeature.setTunnelFactor(0.5d);
        srm2RoadFeature.setElevation(RoadElevation.NORMAL);
        srm2RoadFeature.setElevationHeight(12);
        srm2RoadFeature.setBarrierLeft(RoadSideBarrier.create());
        srm2RoadFeature.getBarrierLeft().setBarrierType(RoadSideBarrierType.SCREEN);
        srm2RoadFeature.getBarrierLeft().setHeight(9d);
        srm2RoadFeature.getBarrierLeft().setDistance(6d);
        srm2RoadFeature.setBarrierRight(RoadSideBarrier.create());
        srm2RoadFeature.getBarrierRight().setBarrierType(RoadSideBarrierType.WALL);
        srm2RoadFeature.getBarrierRight().setHeight(9d);
        srm2RoadFeature.getBarrierRight().setDistance(6d);
        srm2RoadFeature.setTrafficDirection(TrafficDirection.A_TO_B);
        srm2RoadFeature.setSubSources(createStandardVehicles());
        FeatureUtil.addAndSetId(features, srm2RoadFeature);
      } else if (i <= 4) {
        final SRM2RoadESFeature srm2RoadFeature = SRM2RoadESFeature.create(3112);
        addGlobalSettingsRoad(srm2RoadFeature, "Wegverkeer buitenweg alle snelheden", i, 3112);
        srm2RoadFeature.setTrafficDirection(TrafficDirection.BOTH);
        srm2RoadFeature.setSubSources(createStandardVehicles());
        FeatureUtil.addAndSetId(features, srm2RoadFeature);
      } else if (i <= 5) {
        final SRM2RoadESFeature srm2RoadFeature = SRM2RoadESFeature.create(3113);
        addGlobalSettingsRoad(srm2RoadFeature, "Wegverkeer binnen de bebouwde kom alle snelheden", i, 3113);
        srm2RoadFeature.setTrafficDirection(TrafficDirection.B_TO_A);
        srm2RoadFeature.setSubSources(createStandardVehicles());
        FeatureUtil.addAndSetId(features, srm2RoadFeature);
      } else if (i <= 6) {
        final SRM2RoadESFeature srm2RoadFeature = SRM2RoadESFeature.create(3111);
        addGlobalSettingsRoad(srm2RoadFeature, "Wegverkeer eigen specificatie ", i, 3111);
        srm2RoadFeature.setTrafficDirection(TrafficDirection.BOTH);
        srm2RoadFeature.setSubSources(createVehicles());
        FeatureUtil.addAndSetId(features, srm2RoadFeature);
      } else if (i <= 7) {
        final MaritimeMaritimeShippingESFeature maritimeMaritimeShippingFeature = MaritimeMaritimeShippingESFeature.create();
        addGlobalSettingsShipping(maritimeMaritimeShippingFeature, "Zeeroute Zeescheepvaart", i, 7530);
        maritimeMaritimeShippingFeature.setSubSources(createMShipping());
        FeatureUtil.addAndSetId(features, maritimeMaritimeShippingFeature);
      } else if (i <= 8) {
        final InlandMaritimeShippingESFeature inlandMaritimeShippingFeature = InlandMaritimeShippingESFeature.create();
        addGlobalSettingsShipping(inlandMaritimeShippingFeature, "Binnengaats route Zeescheepvaart", i, 7520);
        inlandMaritimeShippingFeature.setSubSources(createMShipping());
        FeatureUtil.addAndSetId(features, inlandMaritimeShippingFeature);
      } else if (i <= 9) {
        final MooringMaritimeShippingESFeature mooringMaritimeShippingFeature = MooringMaritimeShippingESFeature.create();
        addGlobalSettingsMooringShipping(mooringMaritimeShippingFeature, "Aanlegplaats route Zeescheepvaart", i, 7510);
        mooringMaritimeShippingFeature.setSubSources(createMMShipping());
        FeatureUtil.addAndSetId(features, mooringMaritimeShippingFeature);
      } else if (i <= 10) {
        final InlandShippingESFeature inlandShippingFeature = InlandShippingESFeature.create();
        addGlobalSettingsInlandShipping(inlandShippingFeature, "Vaarroute Binnenvaart", i, 7620);
        final InlandWaterway ww = InlandWaterway.create();
        ww.setWaterwayCode("CEMT_IV");
        inlandShippingFeature.setWaterway(ww);
        inlandShippingFeature.setSubSources(createIshipping());
        FeatureUtil.addAndSetId(features, inlandShippingFeature);
      } else if (i <= 11) {
        final MooringInlandShippingESFeature mooringInlandShippingFeature = MooringInlandShippingESFeature.create();
        addGlobalSettingsMooringInlandShipping(mooringInlandShippingFeature, "Aanlegplaats Binnenvaart", i, 7610);
        mooringInlandShippingFeature.setSubSources(createIMShipping());
        FeatureUtil.addAndSetId(features, mooringInlandShippingFeature);
      } else {
        final GenericESFeature feature = GenericESFeature.create(CharacteristicsType.OPS);
        addGlobalSettings(feature, "Industrie ", i, 1800);
        FeatureUtil.addAndSetId(features, feature);
      }
    }
    return features;
  }

  private static void addGlobalSettingsShipping(final MaritimeShippingESFeature feature, final String label, final int id,
      final int sectorId) {
    addGlobalSettings(feature, label, id, sectorId);
    feature.setLabel(label);
    feature.setGeometry(RandomGeometry.randomGeometry(GeometryType.LINESTRING));
  }

  private static void addGlobalSettingsMooringShipping(final MooringMaritimeShippingESFeature feature, final String label, final int id,
      final int sectorId) {
    addGlobalSettings(feature, label, id, sectorId);
    feature.setLabel(label);
    feature.setGeometry(RandomGeometry.randomGeometry(GeometryType.LINESTRING));
  }

  private static void addGlobalSettingsInlandShipping(final InlandShippingESFeature feature, final String label, final int id,
      final int sectorId) {
    addGlobalSettings(feature, label, id, sectorId);
    feature.setLabel(label);
    feature.setGeometry(RandomGeometry.randomGeometry(GeometryType.LINESTRING));
  }

  private static void addGlobalSettingsMooringInlandShipping(final MooringInlandShippingESFeature feature, final String label, final int id,
      final int sectorId) {
    addGlobalSettings(feature, label, id, sectorId);
    feature.setLabel(label);
    feature.setGeometry(RandomGeometry.randomGeometry(GeometryType.LINESTRING));
  }

  public static void addGlobalSettings(final EmissionSourceFeature feature, final String label, final int index, final int sectorId) {
    if (feature.getCharacteristicsType() != null && feature.getCharacteristicsType() == CharacteristicsType.OPS) {
      feature.setCharacteristics(createOPSCharacteristics());
    }
    feature.setGeometry(RandomGeometry.randomGeometry());
    feature.setLabel(label + index);
    feature.setEmission(Substance.NOX, Random.nextInt(10000));
    feature.setEmission(Substance.NH3, Random.nextInt(10000));
    feature.setSectorId(sectorId);
  }

  private static void addGlobalSettingsRoad(final RoadESFeature feature, final String label, final int id, final int sectorId) {
    addGlobalSettings(feature, label, id, sectorId);
    feature.setLabel(label);
    feature.setJurisdictionId(235);
    feature.setRoadManager(RoadManager.PRIVATE);
    feature.setGeometry(RandomGeometry.randomGeometry(GeometryType.LINESTRING));
  }

  private static JsArray<FarmlandActivity> createFarmLandActivites() {
    final JsArray<FarmlandActivity> subSources = new JsArray<FarmlandActivity>();
    final FarmlandActivity row1 = FarmlandActivity.create(FarmlandType.PASTURE);
    row1.setEmission(Substance.NOX, 1.0D);
    row1.setEmission(Substance.NH3, 2.1D);
    subSources.push(row1);
    final FarmlandActivity row2 = FarmlandActivity.create(FarmlandType.MANURE);
    row2.setEmission(Substance.NOX, 3.0D);
    row2.setEmission(Substance.NH3, 4.1D);
    subSources.push(row2);
    final FarmlandActivity row3 = FarmlandActivity.create(FarmlandType.FERTILIZER);
    row3.setEmission(Substance.NOX, 4.0D);
    row3.setEmission(Substance.NH3, 5.1D);
    subSources.push(row2);
    final FarmlandActivity row4 = FarmlandActivity.create(FarmlandType.ORGANIC_PROCESSES);
    row4.setEmission(Substance.NOX, 6.0D);
    row4.setEmission(Substance.NH3, 7.1D);
    subSources.push(row4);
    return subSources;
  }

  private static JsArray<MaritimeShipping> createMShipping() {
    final JsArray<MaritimeShipping> shipping = new JsArray<MaritimeShipping>();
    final StandardMaritimeShipping row1 = StandardMaritimeShipping.create();
    row1.setShipCode("OO100");
    row1.setDescription("Ship type standard OO100");
    row1.setMovementsPerTimeUnit(12);
    row1.setTimeUnit(TimeUnit.YEAR);
    shipping.push(row1);
    final StandardMaritimeShipping row2 = StandardMaritimeShipping.create();
    row2.setShipCode("OO100");
    row2.setDescription("Ship type standard OO100");
    row2.setMovementsPerTimeUnit(12);
    row2.setTimeUnit(TimeUnit.YEAR);
    shipping.push(row2);
    return shipping;
  }

  private static JsArray<MooringMaritimeShipping> createMMShipping() {
    final JsArray<MooringMaritimeShipping> shipping = new JsArray<MooringMaritimeShipping>();
    final StandardMooringMaritimeShipping row1 = StandardMooringMaritimeShipping.create();
    row1.setShipCode("OO100000");
    row1.setDescription("Ship own description");
    row1.setAverageResidenceTime(4);
    row1.setShipsPerTimeUnit(100);
    row1.setShorePowerFactor(12d);
    row1.setTimeUnit(TimeUnit.YEAR);
    shipping.push(row1);
    final StandardMooringMaritimeShipping row2 = StandardMooringMaritimeShipping.create();
    row2.setShipCode("OO100000");
    row2.setDescription("Ship own description");
    row2.setAverageResidenceTime(4);
    row2.setShipsPerTimeUnit(100);
    row2.setShorePowerFactor(12d);
    row2.setTimeUnit(TimeUnit.YEAR);
    shipping.push(row2);
    return shipping;
  }

  private static JsArray<InlandShipping> createIshipping() {
    final JsArray<InlandShipping> shipping = new JsArray<InlandShipping>();
    final StandardInlandShipping row1 = StandardInlandShipping.create();
    row1.setShipCode("M0");
    row1.setDescription("Ship inland");
    row1.setMovementsAtoBPerTimeUnit(5);
    row1.setMovementsBtoAPerTimeUnit(5);
    row1.setPercentageLadenAtoB(20);
    row1.setPercentageLadenBtoA(80);
    shipping.push(row1);
    final StandardInlandShipping row2 = StandardInlandShipping.create();
    row2.setShipCode("M9");
    row2.setDescription("zandschip");
    row2.setMovementsAtoBPerTimeUnit(1);
    row2.setMovementsBtoAPerTimeUnit(1);
    row2.setPercentageLadenAtoB(20);
    row2.setPercentageLadenBtoA(80);
    shipping.push(row2);
    return shipping;
  }

  private static JsArray<MooringInlandShipping> createIMShipping() {
    final JsArray<MooringInlandShipping> shipping = new JsArray<MooringInlandShipping>();
    final StandardMooringInlandShipping row1 = StandardMooringInlandShipping.create();
    row1.setShipCode("BII-2B");
    row1.setDescription("Ship inland");
    row1.setAverageResidenceTime(12);
    row1.setShipsPerTimeUnit(100);
    row1.setShorePowerFactor(0.12d);
    row1.setTimeUnit(TimeUnit.YEAR);
    row1.setPercentageLaden(50);
    shipping.push(row1);
    final StandardMooringInlandShipping row2 = StandardMooringInlandShipping.create();
    row2.setShipCode("BII-2B");
    row2.setDescription("Ship inland");
    row2.setAverageResidenceTime(12);
    row2.setShipsPerTimeUnit(1200);
    row2.setShorePowerFactor(0.25d);
    row2.setTimeUnit(TimeUnit.DAY);
    row2.setPercentageLaden(25);
    shipping.push(row2);
    return shipping;
  }

  private static JsArray<Vehicles> createStandardVehicles() {
    final ValuesPerVehicleTypes valuesPerVehicleType = Js.uncheckedCast(JsPropertyMap.of());
    final ValuesPerVehicleType row1 = ValuesPerVehicleType.create();
    row1.setStagnationFraction(0.10);
    row1.setVehiclesPerTimeUnit(1000);
    valuesPerVehicleType.setValuePerVehicleType(VehicleType.LIGHT_TRAFFIC, row1);

    final ValuesPerVehicleType row2 = ValuesPerVehicleType.create();
    row2.setStagnationFraction(0.15);
    row2.setVehiclesPerTimeUnit(500);
    valuesPerVehicleType.setValuePerVehicleType(VehicleType.NORMAL_FREIGHT, row2);

    final ValuesPerVehicleType row3 = ValuesPerVehicleType.create();
    row3.setStagnationFraction(0.20);
    row3.setVehiclesPerTimeUnit(100);
    valuesPerVehicleType.setValuePerVehicleType(VehicleType.HEAVY_FREIGHT, row3);

    final ValuesPerVehicleType row4 = ValuesPerVehicleType.create();
    row4.setStagnationFraction(-0.5);
    row4.setVehiclesPerTimeUnit(-10);
    valuesPerVehicleType.setValuePerVehicleType(VehicleType.AUTO_BUS, row4);

    final JsArray<Vehicles> standardVehicles = new JsArray<Vehicles>();
    final StandardVehicles standard1 = StandardVehicles.create();
    standard1.setMaximumSpeed(80);
    standard1.setStrictEnforcement(true);
    standard1.setTimeUnit(TimeUnit.DAY);
    standard1.setValuesPerVehicleTypes(valuesPerVehicleType);
    standardVehicles.push(standard1);
    final StandardVehicles standard2 = StandardVehicles.create();
    standard2.setMaximumSpeed(100);
    standard2.setStrictEnforcement(true);
    standard2.setTimeUnit(TimeUnit.MONTH);
    standard2.setValuesPerVehicleTypes(valuesPerVehicleType);
    standardVehicles.push(standard2);
    final StandardVehicles standard3 = StandardVehicles.create();
    standard3.setMaximumSpeed(120);
    standard3.setTimeUnit(TimeUnit.YEAR);
    standard3.setValuesPerVehicleTypes(valuesPerVehicleType);
    standardVehicles.push(standard3);

    return standardVehicles;
  }

  private static JsArray<Vehicles> createVehicles() {
    final JsArray<Vehicles> vehicles = new JsArray<Vehicles>();
    final CustomVehicles row1 = CustomVehicles.create();
    row1.setDescription("Eigen voertuig specificatie I");
    row1.setTimeUnit(TimeUnit.DAY);
    row1.setVehiclesPerTimeUnit(1000);
    row1.setEmissionFactor(Substance.NOX, 1.0D);
    row1.setEmissionFactor(Substance.NO2, 1.1D);
    row1.setEmissionFactor(Substance.NH3, 1.123D);
    vehicles.push(row1);

    final CustomVehicles row2 = CustomVehicles.create();
    row2.setDescription("Eigen voertuigen II");
    row2.setTimeUnit(TimeUnit.YEAR);
    row2.setVehiclesPerTimeUnit(2000);
    row2.setEmissionFactor(Substance.NOX, 2.0D);
    row2.setEmissionFactor(Substance.NO2, 2.1D);
    row2.setEmissionFactor(Substance.NH3, 2.123D);
    vehicles.push(row2);

    final SpecificVehicles row3 = SpecificVehicles.create();
    row3.setVehicleCode("MVADEUR4ZWA");
    row3.setTimeUnit(TimeUnit.YEAR);
    row3.setVehiclesPerTimeUnit(2000);
    vehicles.push(row3);

    return vehicles;
  }

  private static JsArray<FarmLodging> createFarmLodgings() {
    final JsArray<FarmLodging> farmLodgings = new JsArray<FarmLodging>();
    final CustomFarmLodging row1 = CustomFarmLodging.create();
    row1.setDescription("Schapen");
    row1.setAnimalCode(AnimalType.SHEEP.getAnimalCode());
    row1.setNumberOfAnimals(10);
    row1.setEmissionFactor(Substance.NH3, 1.123456D);
    farmLodgings.push(row1);

    final CustomFarmLodging row2 = CustomFarmLodging.create();
    row2.setDescription("Konijnen");
    row2.setAnimalCode(AnimalType.RABBIT.getAnimalCode());
    row2.setNumberOfAnimals(11);
    row2.setEmissionFactor(Substance.NH3, 1.123456D);
    farmLodgings.push(row2);

    final StandardFarmLodging row3 = StandardFarmLodging.create();
    row3.setFarmLodgingCode("A1.1");
    row3.setSystemDefinitionCode("BB93.03.001");
    row3.setNumberOfAnimals(100);

    final JsArray<AdditionalLodgingSystem> additionalLodgingSystems = new JsArray<AdditionalLodgingSystem>();
    final AdditionalLodgingSystem als = AdditionalLodgingSystem.create();
    als.setLodgingSystemCode("E6.10.a"); // RAV
    als.setSystemDefinitionCode("BWL2017.05"); // BWL
    als.setNumberOfAnimals(100);
    additionalLodgingSystems.push(als);

    final AdditionalLodgingSystem als2 = AdditionalLodgingSystem.create();
    als2.setLodgingSystemCode("E6.10.b");
    als2.setSystemDefinitionCode("BWL2017.05");
    als2.setNumberOfAnimals(100);
    additionalLodgingSystems.push(als2);

    final AdditionalLodgingSystem als3 = AdditionalLodgingSystem.create();
    als3.setLodgingSystemCode("E6.1.a");
    als3.setSystemDefinitionCode("BWL2001.36");
    als3.setNumberOfAnimals(100);
    additionalLodgingSystems.push(als3);

    final JsArray<ReductiveLodgingSystem> reductiveLodgingSystems = new JsArray<ReductiveLodgingSystem>();
    final ReductiveLodgingSystem rls = ReductiveLodgingSystem.create();
    rls.setLodgingSystemCode("D1.1.15.4");
    rls.setSystemDefinitionCode("BWL2010.02");
    reductiveLodgingSystems.push(rls);

    final JsArray<LodgingFodderMeasure> fodderMeasures = new JsArray<LodgingFodderMeasure>();
    final LodgingFodderMeasure fmls = LodgingFodderMeasure.create();
    fmls.setFodderMeasureCode("PAS2015.09-01");
    fodderMeasures.push(fmls);

    row3.setAdditionalLodgingSystems(additionalLodgingSystems);
    row3.setReductiveLodgingSystems(reductiveLodgingSystems);
    row3.setFodderMeasures(fodderMeasures);
    farmLodgings.push(row3);

    return farmLodgings;
  }

  private static OPSCharacteristics createOPSCharacteristics() {
    final OPSCharacteristics dummie = OPSCharacteristics.create();
    dummie.setHeatContent(0);
    dummie.setEmissionHeight(Random.nextDouble() * 100);
    dummie.setOutflowVelocity(12);
    dummie.setSpread(20);
    dummie.setDiurnalVariation(DiurnalVariation.ANIMAL_HOUSING);
    dummie.setHeatContentType(HeatContentType.FORCED);
    return dummie;
  }
}
