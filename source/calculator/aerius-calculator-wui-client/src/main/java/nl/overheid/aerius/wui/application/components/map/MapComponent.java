/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.map;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.hooks.HasBeforeDestroy;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;
import jsinterop.base.Js;

import nl.aerius.search.wui.component.PopoutSearchComponent;
import nl.aerius.search.wui.context.SearchContext;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.easter.ActivateEasterEggCommand;
import nl.aerius.wui.easter.DeactivateEasterEggCommand;
import nl.aerius.wui.easter.TetrisGame;
import nl.aerius.wui.easter.game.TetrisGameOverEvent;
import nl.overheid.aerius.geo.wui.OL3MapComponent;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.wui.application.command.InfoPopupResetCommand;
import nl.overheid.aerius.wui.application.command.InfoPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.LayerPopupResetCommand;
import nl.overheid.aerius.wui.application.command.LayerPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.MeasureToggleCommand;
import nl.overheid.aerius.wui.application.command.SearchPopupToggleCommand;
import nl.overheid.aerius.wui.application.components.button.IconComponent;
import nl.overheid.aerius.wui.application.components.toggle.SvgToggleButton;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.InfoPanelContext;
import nl.overheid.aerius.wui.application.context.LayerPanelContext;
import nl.overheid.aerius.wui.application.context.MeasureContext;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(name = "aer-map", components = {
    OL3MapComponent.class,
    SvgToggleButton.class,
    IconComponent.class,
    StatefulIconButton.class,
    PopoutSearchComponent.class,
})
public class MapComponent extends BasicVueEventComponent implements HasMounted, HasBeforeDestroy {
  private static final MapComponentEventBinder EVENT_BINDER = GWT.create(MapComponentEventBinder.class);

  interface MapComponentEventBinder extends EventBinder<MapComponent> {}

  @Prop EventBus eventBus;
  @Ref OL3MapComponent olMap;

  @Inject @Data SearchContext searchContext;

  @Data @Inject MeasureContext measureContext;
  @Data @Inject LayerPanelContext layerContext;
  @Data @Inject InfoPanelContext infoContext;

  @Ref StatefulIconButton infoButton;
  @Ref StatefulIconButton layerButton;
  @Ref StatefulIconButton searchButton;
  private HandlerRegistration resizeHandler;
  private HandlerRegistration eventHandlers;

  @Data boolean enableEaster;

  @Inject @Data ApplicationContext applicationContext;

  @Override
  public void mounted() {
    eventBus.fireEvent(new InfoPanelDockChangeCommand(infoButton.vue().$el()));
    eventBus.fireEvent(new LayerPanelDockChangeCommand(layerButton.vue().$el()));

    resizeHandler = Window.addResizeHandler(v -> {
      eventBus.fireEvent(new InfoPanelDockChangeCommand(infoButton.vue().$el()));
      eventBus.fireEvent(new LayerPanelDockChangeCommand(layerButton.vue().$el()));
    });
    eventHandlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void beforeDestroy() {
    eventBus.fireEvent(new InfoPanelDockRemoveCommand(infoButton.vue().$el()));
    eventBus.fireEvent(new LayerPanelDockRemoveCommand(layerButton.vue().$el()));
    resizeHandler.removeHandler();
    eventHandlers.removeHandler();
  }

  @EventHandler
  public void onActivateEasterEggCommand(final ActivateEasterEggCommand c) {
    if (enableEaster) {
      return;
    }

    final String originStr = applicationContext.getConfiguration().getSettings().getSetting(SharedConstantsEnum.TETRIS_ORIGIN_POINT, true);
    if (originStr == null) {
      // Report in console and ignore
      GWTProd.warn("Easter not configured.");
      return;
    }

    try {
      final String[] originParts = originStr.split(":");
      final Point origin = new Point(Integer.parseInt(originParts[0]), Integer.parseInt(originParts[1]));
      final ReceptorGridSettings grid = applicationContext.getConfiguration().getReceptorGridSettings();
      final ol.Map olMapElement = Js.uncheckedCast(olMap.getInnerMap());
      TetrisGame.go(olMapElement, grid, eventBus, origin);

      enableEaster = true;
    } catch (final Exception e) {
      // Eat error with a sparse description in console
      GWTProd.warn("Could not start easter. " + e.getMessage());
    }
  }

  @EventHandler
  public void onDeactivateEasterEggCommand(final DeactivateEasterEggCommand c) {
    enableEaster = false;
    TetrisGame.destroy();
  }

  @EventHandler
  public void onTetrisGameOverEvent(final TetrisGameOverEvent e) {
    enableEaster = false;
  }

  public void attach() {
    olMap.attach();
  }

  @JsMethod
  public void onMeasureClick() {
    eventBus.fireEvent(new MeasureToggleCommand());
  }

  @JsMethod
  public void onSearchPanelClick() {
    eventBus.fireEvent(new SearchPopupToggleCommand());
  }

  @JsMethod
  public boolean isSearchShowing() {
    return searchContext.isSearchShowing();
  }

  @JsMethod
  public void onInfoPanelClick() {
    eventBus.fireEvent(new InfoPopupToggleCommand());
  }

  @JsMethod
  public void onInfoPanelReset() {
    eventBus.fireEvent(new InfoPopupResetCommand());
  }

  @JsMethod
  public void onLayerPanelClick() {
    eventBus.fireEvent(new LayerPopupToggleCommand());
  }

  @JsMethod
  public void onLayerPanelReset() {
    eventBus.fireEvent(new LayerPopupResetCommand());
  }
}
