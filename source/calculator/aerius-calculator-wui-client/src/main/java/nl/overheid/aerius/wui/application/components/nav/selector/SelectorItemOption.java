/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.nav.selector;

/**
 * Selectable option in a selector menu
 *
 * @param <V> Type of selectable value
 */
public class SelectorItemOption {
  private final String shortLabel;
  private final String label;
  private final String icon;
  private final Object value;

  public SelectorItemOption(final String shortLabel, final String label, final String icon, final Object value) {
    this.shortLabel = shortLabel;
    this.label = label;
    this.icon = icon;
    this.value = value;
  }

  public SelectorItemOption(final String label, final String icon, final Object value) {
    this(label, label, icon, value);
  }

  public SelectorItemOption(final String label, final Object value) {
    this(label, null, value);
  }

  public String getShortLabel() {
    return shortLabel;
  }

  public String getLabel() {
    return label;
  }

  public String getIcon() {
    return icon;
  }

  public Object getValue() {
    return value;
  }
}
