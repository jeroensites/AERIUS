/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.ResultsView.MainTab;

/**
 * Text specific for results pages.
 */
public interface ResultsMessages {
  String resultsScenarioLabel();
  String resultsResultLabel();
  String resultsSubstanceLabel();
  String resultsHexagonTypeLabel();

  String resultsDiscrepancyWarningText();
  String resultsDropdown(@Select ScenarioResultType type);
  String resultsHexagonType(@Select SummaryHexagonType hexagonType);
  String resultsBeingLoaded();
  String resultsErrorOccurred(String errorMsg);
  String resultsNoResultsCalculated();
  String resultsViewMainTabLabel(@Select MainTab tab);
  String resultsViewTabLabel(@Select ResultView tab);
  String resultsChartTitle();
  String resultsNatureAreaTitle();
  String resultsHabitatsTitle();
  String resultsStatistic(@Select ResultStatisticType type, @Select DepositionValueDisplayType displayType);
  String resultsScenarioResultType(@Select ScenarioResultType type, @Select DepositionValueDisplayType displayType);

  String resultsGraphXAxisLabel();
  String resultsGraphYAxisLabel();
  String resultsZoomToAreaTitle();

  String resultsViewSummaryConstructionNotice();
}
