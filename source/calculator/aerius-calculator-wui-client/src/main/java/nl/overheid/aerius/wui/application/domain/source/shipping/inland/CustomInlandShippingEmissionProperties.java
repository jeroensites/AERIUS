/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.inland;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;

/**
 * Client side implementation of CustomInlandShippingEmission props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomInlandShippingEmissionProperties  {

  private double heatContentEmpty;
  private double heatContentLaden;
  private double emissionHeightEmpty;
  private double emissionHeightLaden;
  private EmissionFactors emissionFactorsEmpty;
  private EmissionFactors emissionFactorsLaden;

  public static final @JsOverlay CustomInlandShippingEmissionProperties create() {
    final CustomInlandShippingEmissionProperties props = new CustomInlandShippingEmissionProperties();
    props.setEmissionFactorsEmpty(EmissionFactors.create());
    props.setEmissionFactorsLaden(EmissionFactors.create());
    return props;
  }

  public final @JsOverlay double getHeatContentEmpty() {
    return heatContentEmpty;
  }

  public final @JsOverlay void setHeatContentEmpty(final double heatContentEmpty) {
    this.heatContentEmpty = heatContentEmpty;
  }

  public final @JsOverlay double getHeatContentLaden() {
    return heatContentLaden;
  }

  public final @JsOverlay void setHeatContentLaden(final double heatContentLaden) {
    this.heatContentLaden = heatContentLaden;
  }

  public final @JsOverlay double getEmissionHeightEmpty() {
    return emissionHeightEmpty;
  }

  public final @JsOverlay void setEmissionHeightEmpty(final double emissionHeightEmpty) {
    this.emissionHeightEmpty = emissionHeightEmpty;
  }

  public final @JsOverlay double getEmissionHeightLaden() {
    return emissionHeightLaden;
  }

  public final @JsOverlay void setEmissionHeightLaden(final double emissionHeightLaden) {
    this.emissionHeightLaden = emissionHeightLaden;
  }

  public final @JsOverlay double getEmissionFactorEmpty(final Substance substance) {
    return emissionFactorsEmpty.getEmissionFactor(substance);
  }

  public final @JsOverlay void setEmissionFactorEmpty(final Substance substance, final double emission) {
    emissionFactorsEmpty.setEmissionFactor(substance, emission);
  }

  public final @JsOverlay void setEmissionFactorsEmpty(final EmissionFactors emissionFactors) {
    this.emissionFactorsEmpty = emissionFactors;
  }

  public final @JsOverlay EmissionFactors getEmissionFactorsEmpty() {
    return emissionFactorsEmpty;
  }

  public final @JsOverlay double getEmissionFactorLaden(final Substance substance) {
    return emissionFactorsLaden.getEmissionFactor(substance);
  }

  public final @JsOverlay void setEmissionFactorLaden(final Substance substance, final double emission) {
    emissionFactorsLaden.setEmissionFactor(substance, emission);
  }

  public final @JsOverlay void setEmissionFactorsLaden(final EmissionFactors emissionFactors) {
    this.emissionFactorsLaden = emissionFactors;
  }

  public final @JsOverlay EmissionFactors getEmissionFactorsLaden() {
    return emissionFactorsLaden;
  }

}
