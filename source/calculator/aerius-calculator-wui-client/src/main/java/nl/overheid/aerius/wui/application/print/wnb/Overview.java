/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.wnb;

import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.PrintWnbContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.domain.importer.ScenarioMetaData;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.event.RestoreScenarioContextCompletedEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.service.CalculationServiceAsync;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.stats.ResultsStatisticsFormatter;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component
public class Overview extends BasicVueEventComponent implements HasMounted {
  public static final OverviewEventBinder EVENT_BINDER = GWT.create(OverviewEventBinder.class);
  public static final String ID = "overview";
  private static final DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd MMMM yyyy, HH:mm");

  interface OverviewEventBinder extends EventBinder<Overview> {
  }

  @Inject ApplicationContext appContext;
  @Inject CalculationServiceAsync calculationService;
  @Inject PrintWnbContext context;
  @Inject ResultsStatisticsFormatter resultsStatisticsFormatter;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data CalculationContext calculationContext;
  @Data @JsProperty Map<String, SituationResultsSummary> projectResultSummaries = new HashMap<>();
  @Data @JsProperty Map<String, SituationResultsSummary> situationResultsSummaries = new HashMap<>();
  @Prop EventBus eventBus;

  @Override
  public void mounted() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Computed
  public String getCalculationConfiguration() {
    if (calculationContext.getCalculations().stream().anyMatch(calculation -> calculation.getResultContext().containsCalculationPointResults())) {
      return i18n.pdfOverviewCalculationConfigurationWnbPlus();
    }
    return i18n.pdfOverviewCalculationConfigurationWnb();
  }

  @Computed
  public ScenarioMetaData getScenarioMetaData() {
    return scenarioContext.getScenarioMetaData();
  }

  @Computed
  public String getTimestamp() {
    return dateTimeFormat.format(new Date());
  }

  @Computed
  public String getAddress() {
    return getScenarioMetaData().getStreetAddress() + ",<br/>" + scenarioContext.getScenarioMetaData().getPostcode() + " " + scenarioContext
        .getScenarioMetaData().getCity();
  }

  @Computed
  public String getReference() {
    return context.getReference();
  }

  @JsMethod
  public String getLongSituationName(final SituationContext situationContext) {
    return situationContext.getName() + " - " + M.messages().situationType(situationContext.getType());
  }

  @JsMethod
  public String formatEmission(final double emission) {
    return emission > 0 ? MessageFormatter.formatEmissionWithUnitSmart(emission) : "-";
  }

  @JsMethod
  public String formatDepositionValue(final SituationResultsSummary resultsSummary, final ResultStatisticType statisticsType) {
    return Optional.ofNullable(resultsSummary)
        .map(summary -> summary.getStatistics())
        .map(statistics -> resultsStatisticsFormatter.formatStatisticType(statistics, statisticsType, true))
        .orElse("");
  }

  @Computed
  public String getFormattedNettingFactor() {
    return getSituations().stream()
        .filter(situationContext -> situationContext.getType() == SituationType.NETTING)
        .map(situationContext -> situationContext.getNettingFactor())
        .map(nettingFactor -> MessageFormatter.toFixed(nettingFactor, 2))
        .findFirst().orElse(null);
  }

  @Computed
  public List<SituationContext> getSituations() {
    return scenarioContext.getSituations().stream()
        .sorted(Comparator.comparingInt(o -> PrintWnbContext.PROJECT_CALCULATION_SITUATION_TYPES.indexOf(o.getType())))
        .collect(Collectors.toList());
  }

  @JsMethod
  public String getHexagonId(final SituationResultsSummary summary, final ResultStatisticType type) {
    return Optional.ofNullable(summary)
        .map(resultsSummary -> resultsSummary.getReceptors())
        .map(receptors -> receptors.getValue(type))
        .map(receptor -> String.valueOf(receptor.getReceptorId()))
        .orElse(null);
  }

  @JsMethod
  public String getAreaName(final SituationResultsSummary summary, final ResultStatisticType type) {
    return Optional.ofNullable(summary)
        .map(resultsSummary -> resultsSummary.getReceptors())
        .map(receptors -> receptors.getValue(type))
        .map(receptor -> String.valueOf(receptor.getAssessmentAreaName()))
        .orElse(null);
  }

  @JsMethod
  public String getFormattedProjectResultStatistic(final ResultStatisticType resultStatisticType) {
    return Optional.ofNullable(scenarioContext.getActiveSituation())
        .map(SituationContext::getSituationCode)
        .map(projectResultSummaries::get)
        .map(SituationResultsSummary::getStatistics)
        .map(statistics -> resultsStatisticsFormatter.formatStatisticType(statistics, resultStatisticType, true))
        .orElse("");
  }

  @EventHandler
  public void onScenarioContextRestored(final RestoreScenarioContextCompletedEvent e) {
    for (final SituationContext situation : scenarioContext.getSituations()) {
      if (situation.getType() == SituationType.PROPOSED) {
        calculationService.getSituationResultsSummary(context.getJobId(),
            new SituationResultsKey(SituationContext.handleFromSituation(situation), ScenarioResultType.PROJECT_CALCULATION,
                appContext.getActiveThemeConfiguration().getEmissionResultKeys().get(0), context.getSummaryHexagonType()),
            resultsSummary -> projectResultSummaries.put(situation.getSituationCode(), resultsSummary));
      }
      calculationService.getSituationResultsSummary(context.getJobId(),
          new SituationResultsKey(SituationContext.handleFromSituation(situation), ScenarioResultType.SITUATION_RESULT,
              appContext.getActiveThemeConfiguration().getEmissionResultKeys().get(0), context.getSummaryHexagonType()),
          resultsSummary -> situationResultsSummaries.put(situation.getSituationCode(), resultsSummary));
    }
  }

}
