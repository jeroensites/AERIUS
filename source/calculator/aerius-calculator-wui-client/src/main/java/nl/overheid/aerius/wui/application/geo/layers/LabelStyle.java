/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

/**
 * Contains the styles for the different labels on the map (emission sources and buildings)
 */
public final class LabelStyle {

  private static final String BUILDING_LABEL_COLOR = "#ffffff";
  private static final String CLUSTERED_LABEL_COLOR = "#333333";
  private static final String NO_SECTOR_SELECTED_COLOR = "#699DCD";
  private static final String CALCULATION_MARKER_COLOR = "#FFFE7E";

  private LabelStyle() {
  }

  public static String getLabelColor(final CalculationPointFeature calculationPoint) {
    return CALCULATION_MARKER_COLOR;
  }

  public static String getLabelColor(final EmissionSourceFeature source) {
    if (source.getSectorId() == 0) {
      return NO_SECTOR_SELECTED_COLOR;
    }
    return ColorUtil.webColorSector(source.getSectorId());
  }

  public static String getLabelColor(final BuildingFeature building) {
    return BUILDING_LABEL_COLOR;
  }

  public static String getLabelColor(final LabelFeature labelFeature) {
    return labelFeature.getColor();
  }

  public static String getLabelColor(final LabelFeature[] sources) {
    final String clusteredColor = getLabelColor(sources[0]);

    for (int i = 1; i < sources.length; i++) {
      if (!clusteredColor.equals(getLabelColor(sources[i]))) {
        return CLUSTERED_LABEL_COLOR;
      }
    }

    return clusteredColor;
  }

}
