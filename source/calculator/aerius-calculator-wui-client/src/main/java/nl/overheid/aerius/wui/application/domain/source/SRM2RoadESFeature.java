/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import elemental2.core.JsArray;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadManager;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.TrafficDirection;
import nl.overheid.aerius.wui.application.domain.source.road.RoadSideBarrier;
import nl.overheid.aerius.wui.application.domain.source.road.SRM2LinearReference;

/**
 * Feature object for SRM2 Road Emission Sources.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class SRM2RoadESFeature extends RoadESFeature {

  /**
   * @param sectorId
   * @return Returns Feature {@link SRM2RoadESFeature} object.
   */
  public static @JsOverlay SRM2RoadESFeature create(final int sectorId) {
    final SRM2RoadESFeature feature = new SRM2RoadESFeature();
    init(feature, EmissionSourceType.SRM2_ROAD);
    feature.setTunnelFactor(1.0);
    feature.setRoadManager(RoadManager.STATE);
    feature.setElevation(RoadElevation.NORMAL);
    feature.setElevationHeight(0);
    feature.setPorosity(0D);
    final RoadType roadType = RoadType.valueFromSectorId(sectorId);
    feature.setTrafficDirection(TrafficDirection.BOTH);

    feature.setFreeway(roadType == RoadType.FREEWAY);
    feature.setBarrierLeft(null);
    feature.setBarrierRight(null);
    return feature;
  }

  public final @JsOverlay void setPorosity(final Double porosity) {
    set("porosity", porosity);
  }

  public final @JsOverlay Double getPorosity() {
    return get("porosity") == null ? 0 : Js.coerceToDouble(get("porosity"));
  }

  public final @JsOverlay void setElevation(final RoadElevation elevation) {
    set("elevation", elevation.name());
  }

  public final @JsOverlay RoadElevation getElevation() {
    return RoadElevation.valueOf(get("elevation"));
  }

  public final @JsOverlay int getElevationHeight() {
    return get("elevationHeight") == null ? 0 : Js.coerceToInt(get("elevationHeight"));
  }

  public final @JsOverlay void setElevationHeight(final int elevationHeight) {
    set("elevationHeight", Double.valueOf(elevationHeight));
  }

  public final @JsOverlay boolean isFreeway() {
    return Js.asBoolean(get("freeway"));
  }

  public final @JsOverlay void setFreeway(final boolean freeway) {
    set("freeway", freeway);
  }

  public final @JsOverlay RoadSideBarrier getBarrierLeft() {
    return Js.uncheckedCast(get("barrierLeft"));
  }

  public final @JsOverlay void setBarrierLeft(final RoadSideBarrier barrierLeft) {
    set("barrierLeft", barrierLeft);
  }

  public final @JsOverlay RoadSideBarrier getBarrierRight() {
    return Js.uncheckedCast(get("barrierRight"));
  }

  public final @JsOverlay void setBarrierRight(final RoadSideBarrier barrierRight) {
    set("barrierRight", barrierRight);
  }

  public final @JsOverlay JsArray<SRM2LinearReference> getPartialChanges() {
    final SRM2LinearReference[] items = (SRM2LinearReference[]) Js.asArray(get("partialChanges"));
    final JsArray<SRM2LinearReference> partialChanges = new JsArray<SRM2LinearReference>();
    for (final SRM2LinearReference row : items) {
      partialChanges.push(row);
    }
    return partialChanges;
  }

  public final @JsOverlay void setPartialChanges(final JsArray<SRM2LinearReference> partialChanges) {
    set("partialChanges", partialChanges);
  }

}
