/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ValuesPerVehicleTypes implements JsPropertyMap<ValuesPerVehicleType> {

  public static final @JsOverlay ValuesPerVehicleTypes create() {
    final ValuesPerVehicleTypes standardRoadVehicleType =  Js.uncheckedCast(JsPropertyMap.of());
    for( final nl.overheid.aerius.shared.domain.v2.source.road.VehicleType vehicleType :
        nl.overheid.aerius.shared.domain.v2.source.road.VehicleType.values()) {
      standardRoadVehicleType.setValuePerVehicleType(vehicleType, ValuesPerVehicleType.create());
    }
    return standardRoadVehicleType;
  }

  public final @JsOverlay boolean hasValuePerVehicleType(final nl.overheid.aerius.shared.domain.v2.source.road.VehicleType vehicleType) {
    return has(vehicleType.name());
  }

  public final @JsOverlay ValuesPerVehicleType getValuePerVehicleType(final nl.overheid.aerius.shared.domain.v2.source.road.VehicleType vehicleType) {
    return hasValuePerVehicleType(vehicleType) ? get(vehicleType.name()) : null;
  }

  public final @JsOverlay void setValuePerVehicleType(final nl.overheid.aerius.shared.domain.v2.source.road.VehicleType vehicleType,
      final ValuesPerVehicleType valuePerVehicleType) {
    set(vehicleType.name(), valuePerVehicleType);
  }

}
