/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import javax.inject.Singleton;

@Singleton
public class EmissionSourceValidationContext {
  private boolean displayFormProblems = false;

  private boolean detailEditorError = false;
  private boolean locationError = false;
  private boolean characteristicsError = false;
  private boolean emissionEditorError = false;

  private boolean detailWarning = false;
  private boolean locationWarning = false;
  private boolean characteristicsWarning = false;
  private boolean emissionEditorWarning = false;

  public boolean getDetailEditorError() {
    return detailEditorError;
  }

  public void setDetailEditorError(final boolean detailEditorError) {
    this.detailEditorError = detailEditorError;
  }

  public boolean getDetailEditorWarning() {
    return detailWarning;
  }

  public void setDetailEditorWarning(final boolean detailWarning) {
    this.detailWarning = detailWarning;
  }

  public boolean getLocationWarning() {
    return locationWarning;
  }

  public void setLocationWarning(final boolean locationWarning) {
    this.locationWarning = locationWarning;
  }

  public boolean getLocationError() {
    return locationError;
  }

  public void setLocationError(final boolean locationError) {
    this.locationError = locationError;
  }

  public boolean getCharacteristicsWarning() {
    return characteristicsWarning;
  }

  public void setCharacteristicsWarning(final boolean characteristicsWarning) {
    this.characteristicsWarning = characteristicsWarning;
  }

  public boolean getCharacteristicsError() {
    return characteristicsError;
  }

  public void setCharacteristicsError(final boolean characteristicsError) {
    this.characteristicsError = characteristicsError;
  }

  public boolean getEmissionEditorWarning() {
    return emissionEditorWarning;
  }

  public void setEmissionEditorWarning(final boolean emissionEditorWarning) {
    this.emissionEditorWarning = emissionEditorWarning;
  }

  public boolean getEmissionEditorError() {
    return emissionEditorError;
  }

  public void setEmissionEditorError(final boolean emissionEditorError) {
    this.emissionEditorError = emissionEditorError;
  }

  public boolean hasErrors() {
    // detail errors are not considered because they _should_ be captured within the
    // emissionEditorError
    return locationError
        || characteristicsError
        || emissionEditorError;
  }

  public boolean hasWarnings() {
    // detail warnings are not considered because they _should_ be captured within
    // the emissionEditorError
    return locationWarning
        || characteristicsWarning
        || emissionEditorWarning;
  }

  public boolean isDisplayFormProblems() {
    return displayFormProblems;
  }

  public void setDisplayFormProblems(final boolean displayFormProblems) {
    this.displayFormProblems = displayFormProblems;
  }

  public void reset() {
    displayFormProblems = false;

    detailEditorError = false;
    locationError = false;
    characteristicsError = false;
    emissionEditorError = false;

    detailWarning = false;
    locationWarning = false;
    characteristicsWarning = false;
    emissionEditorWarning = false;
  }
}
