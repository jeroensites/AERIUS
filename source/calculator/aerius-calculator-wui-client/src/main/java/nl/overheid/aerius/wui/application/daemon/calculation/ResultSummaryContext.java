/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.calculation;

import java.util.HashMap;
import java.util.Map;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.context.calculation.SituationHandle;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;

public class ResultSummaryContext {
  private final @JsProperty Map<SituationResultsKey, SituationResultsSummary> availableResults = new HashMap<>();

  private boolean isLoading = false;

  private SituationHandle situationHandle = null;
  private ScenarioResultType resultType = ScenarioResultType.SITUATION_RESULT;
  private EmissionResultKey emissionResultKey = EmissionResultKey.NOXNH3_DEPOSITION;
  private SummaryHexagonType hexagonType = SummaryHexagonType.EXCEEDING_HEXAGONS;

  public void addResultSummary(final SituationResultsKey key, final SituationResultsSummary resultsSummary) {
    availableResults.put(key, resultsSummary);
  }

  public SituationResultsSummary getResultSummary(final SituationResultsKey key) {
    return availableResults.get(key);
  }

  public boolean hasActiveResultSummary() {
    return availableResults.containsKey(getSituationResultsKey());
  }

  public SituationResultsKey getSituationResultsKey() {
    return new SituationResultsKey(situationHandle, resultType, emissionResultKey, hexagonType);
  }

  public SituationResultsSummary getActiveResultSummary() {
    return getResultSummary(getSituationResultsKey());
  }

  public SituationHandle getSituationHandle() {
    return situationHandle;
  }

  public void setSituationHandle(final SituationHandle situationHandle) {
    this.situationHandle = situationHandle;
  }

  public ScenarioResultType getResultType() {
    return resultType;
  }

  public void setResultType(final ScenarioResultType resultType) {
    this.resultType = resultType;
  }

  public EmissionResultKey getEmissionResultKey() {
    return emissionResultKey;
  }

  public void setEmissionResultKey(final EmissionResultKey emissionResultKey) {
    this.emissionResultKey = emissionResultKey;
  }

  public SummaryHexagonType getHexagonType() {
    return hexagonType;
  }

  public void setHexagonType(final SummaryHexagonType hexagonType) {
    this.hexagonType = hexagonType;
  }

  public boolean isLoading() {
    return isLoading;
  }

  public void setLoading(final boolean isLoading) {
    this.isLoading = isLoading;
  }

  public boolean containsCalculationPointResults() {
    return this.availableResults.values().stream().anyMatch(result -> result.getCustomPointResults().length > 0);
  }
}
