/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.modal.notification;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.util.Notification;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.wui.application.command.NotificationRemoveCommand;
import nl.overheid.aerius.wui.application.command.NotificationsClearCommand;
import nl.overheid.aerius.wui.application.command.misc.CloseNotificationDisplayCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.context.NotificationContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.main.VerticalCollapseGroup;
import nl.overheid.aerius.wui.vue.BasicVueComponent;
import nl.overheid.aerius.wui.vue.VectorDirective;

@Component(components = {
    VerticalCollapse.class,
    VerticalCollapseGroup.class,
    ButtonIcon.class
}, directives = {
    VectorDirective.class
})
public class NotificationComponent extends BasicVueComponent implements IsVueComponent {
  @Prop EventBus eventBus;
  @Prop boolean active;

  @Inject @Data NotificationContext context;

  @PropDefault("active")
  boolean activeDefault() {
    return false;
  }

  @Computed
  public List<Notification> getNotifications() {
    return context.getNotifications();
  }

  @JsMethod
  public void removeAll() {
    eventBus.fireEvent(new NotificationsClearCommand());
  }

  @JsMethod
  public void remove(final Notification notification) {
    eventBus.fireEvent(new NotificationRemoveCommand(notification));
  }

  @JsMethod
  public String getInactiveMessageText() {
    return context.getNotificationCount() > 0
        ? M.messages().notificationNumberOfMessages(context.getNotificationCount())
        : M.messages().notificationNoMessages();
  }

  @JsMethod
  public void close() {
    eventBus.fireEvent(new CloseNotificationDisplayCommand());
  }
}
