/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.result;

import static jsinterop.annotations.JsPackage.GLOBAL;

import java.util.List;
import java.util.stream.Collectors;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.JsArrayLike;

import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;

/**
 * Represents one or more markers at a particular location.
 * The marker types are specified in `statisticsTypes`
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class SituationResultsMarker {

  private int receptorId;
  private JsArrayLike<String> statisticsTypes;
  private Object point;

  public final @JsOverlay int getReceptorId() {
    return receptorId;
  }

  public final @JsOverlay List<ResultStatisticType> getStatisticsTypes() {
    return statisticsTypes.asList().stream().map(
        key -> ResultStatisticType.getValueByJsonKey(key)
    ).collect(Collectors.toList());
  }

  public final @JsOverlay Object getPoint() {
    return point;
  }
}

