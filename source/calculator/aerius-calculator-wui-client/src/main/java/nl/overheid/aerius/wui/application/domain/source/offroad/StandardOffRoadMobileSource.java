/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.offroad;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

/**
 * Client side implementation of StandardOffRoadMobileSource props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class StandardOffRoadMobileSource extends OffRoadMobileSource {

  private String offRoadMobileSourceCode;
  private int literFuelPerYear;
  private int operatingHoursPerYear;
  private int literAdBluePerYear;

  public static final @JsOverlay StandardOffRoadMobileSource create() {
    final StandardOffRoadMobileSource props = new StandardOffRoadMobileSource();
    OffRoadMobileSource.init(props, OffRoadType.STANDARD);
    props.setOffRoadMobileSourceCode(null);
    props.setLiterFuelPerYear(0);
    props.setOperatingHoursPerYear(0);
    props.setLiterAdBluePerYear(0);
    return props;
  }

  public final @JsOverlay String getOffRoadMobileSourceCode() {
    return offRoadMobileSourceCode;
  }

  public final @JsOverlay void setOffRoadMobileSourceCode(final String offRoadMobileSourceCode) {
    this.offRoadMobileSourceCode = offRoadMobileSourceCode;
  }

  public final @JsOverlay int getLiterFuelPerYear() {
    return literFuelPerYear;
  }

  public final @JsOverlay void setLiterFuelPerYear(final int literFuelPerYear) {
    this.literFuelPerYear = literFuelPerYear;
  }

  public final @JsOverlay int getOperatingHoursPerYear() {
    return operatingHoursPerYear;
  }

  public final @JsOverlay void setOperatingHoursPerYear(final int operatingHoursPerYear) {
    this.operatingHoursPerYear = operatingHoursPerYear;
  }

  public final @JsOverlay int getLiterAdBluePerYear() {
    return literAdBluePerYear;
  }

  public final @JsOverlay void setLiterAdBluePerYear(final int literAdBluePerYear) {
    this.literAdBluePerYear = literAdBluePerYear;
  }

}
