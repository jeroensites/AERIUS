/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.wnb.pages.calculationpoints;

import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.wui.application.command.DeleteAllCalculationPointsCommand;
import nl.overheid.aerius.wui.application.command.UserImportCalculationPointsRequestedCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditNewCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.RemoveCalculationPointCommand;
import nl.overheid.aerius.wui.application.command.source.FeatureZoomCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.ui.main.VerticalCollapseGroup;
import nl.overheid.aerius.wui.application.util.DoubleClickUtil;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    CalculationPointRowComponent.class,
    ModifyListComponent.class,
    VerticalCollapse.class,
    VerticalCollapseGroup.class,
    ButtonIcon.class,
})
public class CalculationPointsListComponent extends BasicVueEventComponent implements HasCreated, HasDestroyed {
  private static final CalculationPointsListComponentEventBinder EVENT_BINDER = GWT.create(CalculationPointsListComponentEventBinder.class);

  interface CalculationPointsListComponentEventBinder extends EventBinder<CalculationPointsListComponent> {}

  private final DoubleClickUtil<CalculationPointFeature> doubleClickUtil = new DoubleClickUtil<>();

  @Prop EventBus eventBus;
  @Prop @JsProperty List<CalculationPointFeature> calculationPoints;

  @Data CalculationPointFeature selected;

  private HandlerRegistration handlers;

  /**
   * TODO Move this kind of logic and selected-tracking to a Daemon
   */
  @EventHandler
  public void onCalculationPointToggleSelectFeatureCommand(final CalculationPointToggleSelectFeatureCommand c) {
    selectPoint(c.getValue());
  }
  /**
   * TODO Hack to fetch the selected calculation point, remove this as part of above
   */
  @EventHandler
  public void onCalculationPointToggleSelectFeatureCommand(final FetchCalculationPointSelectionEvent c) {
    c.setValue(selected);
  }

  @Override
  public void created() {
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    handlers.removeHandler();
  }

  @Computed
  public boolean isHasCalculationPoints() {
    return !calculationPoints.isEmpty();
  }

  @JsMethod
  public void deleteAllCalculationPoints() {
    if (Window.confirm(i18n.calculationPointsDeleteWarning())) {
      eventBus.fireEvent(new DeleteAllCalculationPointsCommand());
    }
  }

  @JsMethod
  public void importFile() {
    eventBus.fireEvent(new UserImportCalculationPointsRequestedCommand());
  }

  @JsMethod
  public void selectPoint(final CalculationPointFeature point) {
    if (selected == point) {
      selected = null;
    } else {
      selected = point;
    }
  }

  @JsMethod
  public void focusPoint(final CalculationPointFeature point) {
    eventBus.fireEvent(new FeatureZoomCommand(point));
  }

  @JsMethod
  public void clickPoint(final CalculationPointFeature point) {
    doubleClickUtil.click(point, this::selectPoint, this::focusPoint);
  }

  @JsMethod
  public void newCalculationPoint() {
    eventBus.fireEvent(new CalculationPointEditNewCommand());
  }

  @JsMethod
  public void editCalculationPoint() {
    eventBus.fireEvent(new CalculationPointEditCommand(selected));
  }

  @JsMethod
  public void deleteCalculationPoint() {
    eventBus.fireEvent(new RemoveCalculationPointCommand(selected));
  }
}
