/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import elemental2.core.JsArray;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Any;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadManager;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.TrafficDirection;
import nl.overheid.aerius.wui.application.domain.source.road.SRM1LinearReference;
/**
 * feature object for SRM1 Road Emission sources.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class SRM1RoadESFeature extends RoadESFeature {

  /**
   * @return Returns a new {@link SRM1RoadESFeature} object.
   */
  public static @JsOverlay SRM1RoadESFeature create() {
    final SRM1RoadESFeature feature = new SRM1RoadESFeature();
    init(feature, EmissionSourceType.SRM1_ROAD);
    feature.setTunnelFactor(1.0);
    feature.setRoadManager(RoadManager.PROVINCE);
    feature.setRoadSpeedType(RoadSpeedType.NATIONAL_ROAD);
    feature.setTrafficDirection(TrafficDirection.BOTH);
    feature.setPartialChanges(new JsArray<SRM1LinearReference>());
    return feature;
  }

  public final @JsOverlay RoadSpeedType getRoadSpeedType() {
    return RoadSpeedType.safeLegacyValueOf(get("snelheid"));
  }

  public final @JsOverlay void setRoadSpeedType(final RoadSpeedType roadSpeedType) {
    set("roadSpeedType", roadSpeedType.name());
  }

  public final @JsOverlay JsArray<SRM1LinearReference> getPartialChanges() {
    final Any[] items = Js.asArray(get("partialChanges"));
    return new JsArray<SRM1LinearReference>(items);
  }

  public final @JsOverlay void setPartialChanges(final JsArray<SRM1LinearReference> partialChanges) {
    set("partialChanges", partialChanges);
  }

}
