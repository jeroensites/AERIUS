/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.modal;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.gwt.core.client.GWT;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.ui.main.VerticalCollapse;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    VerticalCollapse.class
})
public class DialogModalComponent extends BasicVueComponent implements IsVueComponent, HasCreated {
  @Prop boolean showing;

  @Prop boolean submitEnabled;

  @Data boolean isClosing;

  @Emit
  @JsMethod
  public void submit() {
    isClosing = true;
  }

  @Watch("showing")
  public void onShowingChanged(final boolean old, final boolean neww) {
    if (!showing) {
      isClosing = false;
    }
  }

  public void cancelClose() {
    isClosing = false;
  }

  /**
   * Emits when the component wants to close through means other than user interaction with the close button
   */
  @Emit
  @JsMethod
  public void autoClose() {}

  /**
   * Emits when the component wants to close as a result of cancelling
   */
  @Emit
  @JsMethod
  public void cancel() {}

  @Watch("submitEnabled")
  public void onSubmitEnabledChanged(final boolean old, final boolean neww) {
    GWT.log("Submit Enabled: " + submitEnabled);
  }

  @Override
  public void created() {
    GWT.log("Submit Enabled: " + submitEnabled);
  }
}
