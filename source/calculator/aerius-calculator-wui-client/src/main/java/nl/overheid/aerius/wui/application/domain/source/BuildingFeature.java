/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import ol.Feature;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

/**
 * Base class Building Feature. This class extends openlayers {@link Feature}
 * class.
 *
 * NOTE: This class can't be implemented with java fields as the variables
 * should be in the Feature properties
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class BuildingFeature extends Feature {
  public final static @JsOverlay BuildingFeature create() {
    final BuildingFeature feature = new BuildingFeature();
    feature.setGeometry(null);
    feature.setGmlId("");
    feature.setLabel("");
    feature.setHeight(0);
    feature.setRadius(null);
    return feature;
  }

  public final @JsOverlay String getLabel() {
    return get("label") == null ? "" : Js.asString(get("label"));
  }

  public final @JsOverlay void setLabel(final String label) {
    set("label", label);
  }

  public final @JsOverlay String getGmlId() {
    return get("gmlId") == null ? "" : Js.asString(get("gmlId"));
  }

  public final @JsOverlay void setGmlId(final String gmlId) {
    set("gmlId", gmlId);
  }

  public final @JsOverlay Double getHeight() {
    return Js.coerceToDouble(get("height"));
  }

  public final @JsOverlay void setHeight(final double height) {
    set("height", Double.valueOf(height));
  }

  public final @JsOverlay Double getRadius() {
    return get("radius") == null ? null : Js.coerceToDouble(get("radius"));
  }

  public final @JsOverlay void setRadius(final Double radius) {
    set("radius", radius == null ? null : Double.valueOf(radius));
  }
}
