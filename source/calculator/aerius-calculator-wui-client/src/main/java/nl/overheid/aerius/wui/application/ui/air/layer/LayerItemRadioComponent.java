/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.air.layer;

import java.util.Optional;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.gwt.dom.client.SelectElement;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.Event;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.geo.command.LayerHiddenCommand;
import nl.overheid.aerius.geo.command.LayerOpacityCommand;
import nl.overheid.aerius.geo.command.LayerVisibleCommand;
import nl.overheid.aerius.geo.domain.BundledLayerItem;
import nl.overheid.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    LayerItemTemplate.class,
})
public class LayerItemRadioComponent extends BasicVueComponent implements IsVueComponent {
  @Prop BundledLayerItem item;

  @Prop EventBus eventBus;

  @JsMethod
  public void onChange(final Event event) {
    final String value = ((SelectElement) event.target).getValue();

    final Optional<IsLayer<?>> layer = item.getLayers().stream()
        .filter(v -> v.info().getName().equals(value))
        .findFirst();
    layer.ifPresent(v -> {
      eventBus.fireEvent(new LayerHiddenCommand(item.getLayer()));
      eventBus.fireEvent(new LayerVisibleCommand(v));
      eventBus.fireEvent(new LayerOpacityCommand(v, item.getOpacity()));
    });
  }
}
