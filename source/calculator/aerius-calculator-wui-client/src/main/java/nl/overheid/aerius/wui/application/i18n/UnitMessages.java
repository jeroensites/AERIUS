/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;

public interface UnitMessages {

  String unitC(String str);
  String unitKgPerY(String amount);
  String unitTonnesPerY(String amount);
  String unitM(String str);
  String unitMPerS(String str);
  String unitMW(String str);
  String unitOrientation(String str);
  String unitPercentage(String str);
  String unitHa(String str);
  String unitSingularCelsius();
  String unitSingularDegrees();
  String unitSingularSquareMetre();
  String unitSingularMW();
  String unitSingularOrientation();
  String unitSingularMeter();
  String unitSingularMeterPerSecond();
  String unitSingularKgPerY();
  String unitSingularKgHousingPerY();
  String unitSingularPercentage();
  String unitSingularGramPerKilometers();
  String unitSingularLiterPerYear();
  String unitSingularHourPerYear();
  String unitSingularHour();
  String unitSingularHectares();
  String unitSingularTonPerY();

  String unitDeposition(@Select DepositionValueDisplayType displayType, String str);
  String unitDepositionSingular(@Select DepositionValueDisplayType displayType);
  String unitDepositionSingularPerHa(@Select DepositionValueDisplayType displayType);
  String unitDepositionSingularPerY(@Select DepositionValueDisplayType displayType);
  String unitDepositionSingularPerHaPerY(@Select DepositionValueDisplayType displayType);

  String decimalNumberCapped(double value, @Select int digits);
  String decimalNumberFixed(double value, @Select int digits);

  String point(double x, double y);

  String unitTime(@Select TimeUnit timeunit);

}
