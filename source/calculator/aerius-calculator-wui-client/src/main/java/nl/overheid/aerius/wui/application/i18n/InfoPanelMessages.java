/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.info.HabitatGoal;

public interface InfoPanelMessages {
  String infoPanelTitle();
  String infoPanelNoSelectionInactiveText();
  String infoPanelNoSelectionText();
  String infoPanelLoadingText();
  String infoPanelReceptorIdLabel();
  String infoPanelCoordinateLabel();
  String infoPanelNoResults();

  String infoPanelDespositionTitle();
  String infoPanelDepositionBackground();
  String infoPanelDepositionSource();
  String infoPanelHabitatTypeTitle();
  String infoPanelHabitatTypeCode();
  String infoPanelHabitatKdw();
  String infoPanelHabitatOverlap();

  String infoPanelAreaInfoAreaId();
  String infoPanelAreaInfoContractor();
  String infoPanelAreaInfoHabitat();
  String infoPanelAreaInfoProtection();
  String infoPanelAreaInfoStatus();
  String infoPanelAreaInfoSurface();
  String infoPanelAreaInfoTitle();

  String habitatInfoPanelAreaInfoKDW();
  String habitatInfoPanelAreaInfoQualityGoal();
  String habitatInfoPanelSurfaceExtentGoal();
  String habitatInfoPanelSurfaceRelevantCartographic();
  String habitatInfoPanelSurfaceRelevantMapped();

  String habitatsTableHabitatGoal(@Select HabitatGoal select);

}
