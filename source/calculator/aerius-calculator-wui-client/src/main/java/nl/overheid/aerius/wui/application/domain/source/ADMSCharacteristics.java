/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;

/**
 * Client side implementation of AMDS props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ADMSCharacteristics extends SourceCharacteristics {

  private String buildingId;

  public static final @JsOverlay ADMSCharacteristics create() {
    final ADMSCharacteristics props = Js.uncheckedCast(JsPropertyMap.of());
    props.setCharacteristicsType(CharacteristicsType.ADMS);
    props.setBuildingId(null);
    return props;
  }

  public final @JsOverlay boolean isBuildingInfluence() {
    return buildingId != null;
  }

  public final @JsOverlay void setBuildingId(final String buildingId) {
    this.buildingId = buildingId;
  }

  public final @JsOverlay String getBuildingId() {
    return buildingId;
  }
}
