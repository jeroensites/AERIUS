/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Singleton;

import nl.aerius.wui.util.RequestUtil;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculateContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationOptions;
import nl.overheid.aerius.wui.application.domain.export.ExportOptions;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

@Singleton
public class CalculationServiceAsyncImpl implements CalculationServiceAsync {

  @Inject EnvironmentConfiguration cfg;
  @Inject ExportContext exportContext;
  @Inject CalculateContext calculateContext;
  @Inject HeaderHelper hdr;

  @Override
  public void startCalculation(final ExportOptions exportOptions, final ScenarioContext scenarioContext, final Theme theme,
      final AsyncCallback<String> callback) {
    final ExportOptions options = Optional.ofNullable(exportOptions)
        .orElse(ExportOptions.builder().build());
    final CalculationOptions calculationOptions = getValidCalculationOptionsFromContext(options);

    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_CALCULATE);
    final String calculationRequest = "{"
        + "\"theme\": \"" + theme.getKey().toUpperCase() + "\","
        + "\"scenario\": " + scenarioContext.toJSONString(exportContext.isIncludeScenarioMetaData(), calculationOptions) + ","
        + "\"exportOptions\": " + options.toJSONString()
        + "}";

    RequestUtil.doPost(requestUrl, calculationRequest, hdr.defaultHeaders(), AeriusRequestCallback.createRawAsync(callback, s -> s));
  }

  @Override
  public void getCalculationStatus(final String calculationCode, final AsyncCallback<CalculationInfo> callback) {
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_CALCULATE_INFO,
        RequestMappings.CALCULATION_CODE, calculationCode);
    RequestUtil.doGetWithHeaders(requestUrl, hdr.defaultHeaders(), AeriusRequestCallback.createAsync(callback));
  }

  @Override
  public void cancelCalculation(final String calculationCode, final Theme theme, final AsyncCallback<String> callback) {
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_CALCULATE_CANCEL,
        RequestMappings.CALCULATION_CODE, calculationCode);
    RequestUtil.doPost(requestUrl, "", hdr.defaultHeaders(), AeriusRequestCallback.createRawAsync(callback, s -> s));
  }

  @Override
  public void deleteCalculation(final String calculationCode, final AsyncCallback<String> callback) {
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_CALCULATE_DELETE,
        RequestMappings.CALCULATION_CODE, calculationCode);
    RequestUtil.doDelete(requestUrl, hdr.defaultHeaders(), AeriusRequestCallback.createRawAsync(callback, s -> s));
  }

  @Override
  public void getSituationResultsSummary(final String calculationCode, final SituationResultsKey situationResultsKey,
      final AsyncCallback<SituationResultsSummary> callback) {
    final Map<String, String> queryParams = new HashMap<>();
    if (situationResultsKey.getHexagonType() != null) {
      queryParams.put(RequestMappings.CONNECT_HEXAGON_TYPE_PARAM, situationResultsKey.getHexagonType().name());
    }
    final String requestUrl = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_CALCULATION_SUMMARY,
        RequestMappings.CALCULATION_CODE, calculationCode,
        RequestMappings.SITUATION_CODE, situationResultsKey.getSituationHandle().getId(),
        RequestMappings.RESULT_TYPE, situationResultsKey.getResultType().name());
    RequestUtil.doGet(requestUrl, queryParams, hdr.defaultHeaders(), AeriusRequestCallback.createAsync(callback));
  }

  /**
   * Ensure the combination of export- and calculation options are valid. If not, create new options without altering the context.
   */
  private CalculationOptions getValidCalculationOptionsFromContext(final ExportOptions exportOptions) {
    final CalculationOptions calculationOptions = calculateContext.getCalculationOptions();

    if (exportOptions.getExportType() == ExportType.PAA && calculationOptions.getCalculationType() != CalculationType.PERMIT) {
      final CalculationOptions newCalculationOptions = new CalculationOptions();
      newCalculationOptions.setCalculationType(CalculationType.PERMIT);
      return newCalculationOptions;
    }
    return calculationOptions;
  }
}
