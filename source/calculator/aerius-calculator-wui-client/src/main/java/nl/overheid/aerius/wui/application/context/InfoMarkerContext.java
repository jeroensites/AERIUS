/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.domain.info.ReceptorInfo;

@Singleton
public class InfoMarkerContext {
  private boolean loading = false;
  private Integer receptor = null;
  private String year = "";
  private @JsProperty List<String> situations = new ArrayList<String>();
  private ReceptorInfo receptorInfo = null;

  public boolean isLoading() {
    return loading;
  }

  public void setLoading(final boolean loading) {
    this.loading = loading;
  }

  public boolean hasSelection() {
    return receptor != null;
  }

  public int getReceptor() {
    return receptor;
  }

  public void setReceptor(final Integer id) {
    this.receptor = id;
  }

  public boolean isSelected(final Integer value) {
    return receptor != null && receptor.equals(value);
  }

  public void setReceptorInfo(final ReceptorInfo receptorInfo) {
    this.receptorInfo = receptorInfo;
  }

  public boolean hasReceptorInfo() {
    return receptorInfo != null;
  }

  public ReceptorInfo getReceptorInfo() {
    return receptorInfo;
  }

  public String getYear() {
    return year;
  }

  public void setYear(final String year) {
    this.year = year;
  }

  public List<String> getSituations() {
    return situations;
  }

  public void setSituations(final List<String> situations) {
    this.situations = situations;
  }

}
