/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.ArrayList;
import java.util.List;

import ol.OLFactory;
import ol.geom.LineString;
import ol.style.Style;

import nl.overheid.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.overheid.aerius.geo.domain.legend.Legend;
import nl.overheid.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.SRM2RoadESFeature;
import nl.overheid.aerius.wui.application.geo.layers.source.RoadSourceGeometryLayer;
import nl.overheid.aerius.wui.application.i18n.M;

public class RoadBarrierStyle implements RoadStyle {

  private static final String ROAD_STYLE_COLOR = "#f18617";
  private static final Style ROAD_STYLE = OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(ROAD_STYLE_COLOR), RoadSourceGeometryLayer.DEFAULT_ROAD_WIDTH));

  private static final String ROAD_BARRIER_COLOR = "#095da7";
  private static final int ROAD_BARRIER_WIDTH = 4;
  private static final Style ROAD_BARRIER_STYLE = OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(ROAD_BARRIER_COLOR), ROAD_BARRIER_WIDTH));
  private static final double ROAD_BARRIER_STYLE_DISTANCE = 20d;

  @Override
  public List<Style> getStyle(final RoadESFeature source, final VehicleType vehicleType, final double resolution) {

    final List<Style> styleList = new ArrayList<>();
    styleList.add(ROAD_STYLE);

    if (source.getEmissionSourceType().equals(EmissionSourceType.SRM2_ROAD)) {

      final SRM2RoadESFeature srm2source = (SRM2RoadESFeature) source;
      final LineString geometry = (LineString) source.getGeometry();

      if (srm2source.getBarrierLeft() != null && srm2source.getBarrierLeft().getHeight() > 0) {
        styleList.add(getRoadWallStyle(geometry, -ROAD_BARRIER_STYLE_DISTANCE * resolution));
      }
      if (srm2source.getBarrierRight() != null && srm2source.getBarrierRight().getHeight() > 0) {
        styleList.add(getRoadWallStyle(geometry, ROAD_BARRIER_STYLE_DISTANCE * resolution));
      }

    }

    return styleList;
  }

  private Style getRoadWallStyle(final LineString geometry, final double distance) {
    final Style roadWallStyle = ROAD_BARRIER_STYLE.clone();
    roadWallStyle.setGeometry(OL3GeometryUtil.offsetFromLineString(geometry, distance));
    return roadWallStyle;
  }

  @Override
  public Legend getLegend() {
    return new ColorLabelsLegend(new String[] {M.messages().sourceRoadBarrierTitle()}, new String[] {ROAD_BARRIER_COLOR}, LegendType.LINE);
  }
}
