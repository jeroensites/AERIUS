/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.calculation;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.domain.calculation.CalculationOptions;

@Singleton
public class CalculateContext {
  private String referenceSituationId = "";
  private String nettingSituationId = "";
  private CalculationOptions calculationOptions = new CalculationOptions();
  private String meteoYear = null;
  @JsProperty private final List<String> selectedProposedAndTemporarySituations = new ArrayList<>();

  public String getReferenceSituationId() {
    return referenceSituationId;
  }

  public void setReferenceSituationId(final String referenceSituationId) {
    this.referenceSituationId = referenceSituationId;
  }

  public String getNettingSituationId() {
    return nettingSituationId;
  }

  public void setNettingSituationId(final String nettingSituationId) {
    this.nettingSituationId = nettingSituationId;
  }

  public CalculationOptions getCalculationOptions() {
    return calculationOptions;
  }

  public void setCalculationOptions(CalculationOptions calculationOptions) {
    this.calculationOptions = calculationOptions;
  }

  public List<String> getSelectedProposedAndTemporarySituations() {
    return new ArrayList<>(selectedProposedAndTemporarySituations);
  }

  public String getMeteoYear() {
    return meteoYear;
  }

  public void setMeteoYear(final String meteoYear) {
    this.meteoYear = meteoYear;
  }

  public void addProposedAndTemporarySituation(final String id) {
    selectedProposedAndTemporarySituations.add(id);
  }

  public void removeProposedAndTemporarySituation(final String id) {
    selectedProposedAndTemporarySituations.remove(id);
  }
}
