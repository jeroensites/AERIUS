/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmland;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.source.farmland.FarmLandDetailValidators.FarmLandDetailValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandActivity;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandType;

@Component(customizeOptions = FarmLandDetailValidators.class, components = {
    MinimalInputComponent.class,
    FarmLandDetailEmissionRowComponent.class,
    SimplifiedListBoxComponent.class,
    ValidationBehaviour.class,
})
public class FarmLandDetailEditorComponent extends ErrorWarningValidator implements HasValidators<FarmLandDetailValidations>, HasCreated {
  @Prop(required = true) FarmlandActivity subSource;

  @Prop EventBus eventBus;

  @Data @Inject ApplicationContext applicationContext;

  @JsProperty(name = "$v") FarmLandDetailValidations validations;

  @Data FarmlandType selectedFarmLandType;

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Watch(value = "subSource", isImmediate = true)
  public void onSubSourceChange(final FarmlandActivity subSource) {
    selectedFarmLandType = FarmlandType.safeValueOf(subSource.getActivityCode());
  }

  @Override
  @Computed
  public FarmLandDetailValidations getV() {
    return validations;
  }

  @Computed("substances")
  public List<Substance> getSubstances() {
    return applicationContext.getActiveThemeConfiguration().getSubstances();
  }

  @JsMethod
  public void selectFarmland(final FarmlandType option) {
    selectedFarmLandType = option;
    subSource.setActivityCode(option.name());
  }

  @JsMethod
  public boolean hasActivity() {
    return subSource != null;
  }
}
