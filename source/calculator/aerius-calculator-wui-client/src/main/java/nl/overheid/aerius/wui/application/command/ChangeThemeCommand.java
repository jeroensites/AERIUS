/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.command;

import nl.aerius.wui.command.SimpleGenericCommand;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.event.ChangeThemeEvent;

/**
 * Command event to change the theme.
 */
public class ChangeThemeCommand extends SimpleGenericCommand<Theme, ChangeThemeEvent> {
  public ChangeThemeCommand(final Theme theme) {
    super(theme);
  }

  @Override
  protected ChangeThemeEvent createEvent(final Theme theme) {
    return new ChangeThemeEvent(theme);
  }
}
