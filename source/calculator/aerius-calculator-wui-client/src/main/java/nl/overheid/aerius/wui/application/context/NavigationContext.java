/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.wui.application.components.nav.ButtonItem;
import nl.overheid.aerius.wui.application.components.nav.NavigationItem;
import nl.overheid.aerius.wui.application.components.nav.selector.SelectorItem;

@Singleton
public class NavigationContext {
  private final @JsProperty List<SelectorItem> selectorItems = new ArrayList<>();
  private final @JsProperty List<NavigationItem> navigationItems = new ArrayList<>();
  private final @JsProperty List<ButtonItem> buttonItems = new ArrayList<>();

  private SelectorItem activeSelector = null;

  public void addSelectorItem(final SelectorItem selectorItem) {
    selectorItems.add(selectorItem);
  }

  public List<SelectorItem> getSelectorItems() {
    return selectorItems;
  }

  public void addNavigationItem(final NavigationItem navigationItem) {
    navigationItems.add(navigationItem);
  }

  public List<NavigationItem> getNavigationItems() {
    return navigationItems;
  }

  public void addButtonItem(final ButtonItem buttonItem) {
    buttonItems.add(buttonItem);
  }

  public List<ButtonItem> getButtonItems() {
    return buttonItems;
  }

  public boolean isSelectorMenuActive() {
    return activeSelector != null;
  }

  public boolean isActiveSelector(final SelectorItem item) {
    return activeSelector == item;
  }

  public SelectorItem getActiveSelector() {
    return activeSelector;
  }

  public void activateSelectorMenu(final SelectorItem selection) {
    this.activeSelector = selection;
  }

  public void deactivateSelectorMenu() {
    this.activeSelector = null;
  }

  public void reset() {
    activeSelector = null;
    selectorItems.clear();
    navigationItems.clear();
    buttonItems.clear();
  }

  public void toggleSelectorMenu(final SelectorItem selection) {
    if (activeSelector == null || activeSelector != selection) {
      activateSelectorMenu(selection);
    } else {
      deactivateSelectorMenu();
    }
  }
}
