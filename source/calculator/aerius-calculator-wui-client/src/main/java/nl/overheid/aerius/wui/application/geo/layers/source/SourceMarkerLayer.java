/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Collection;
import ol.Feature;
import ol.OLFactory;
import ol.geom.Point;
import ol.layer.Layer;
import ol.layer.VectorLayerOptions;
import ol.source.Cluster;
import ol.source.ClusterOptions;
import ol.source.Vector;
import ol.source.VectorOptions;
import ol.style.Icon;
import ol.style.IconOptions;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.overheid.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.geo.icon.DynamicSvgIcon;
import nl.overheid.aerius.wui.application.ApplicationLimits;
import nl.overheid.aerius.wui.application.command.NameLabelVisibilityCommand;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceAddedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceDeletedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceListChangeEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceModifyGeometryCommand;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceUpdatedEvent;
import nl.overheid.aerius.wui.application.geo.icons.LabelSvgIconBuilder;
import nl.overheid.aerius.wui.application.geo.layers.LabelFeature;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.OptimizedCollection;
import nl.overheid.aerius.wui.application.i18n.M;

public class SourceMarkerLayer implements IsLayer<Layer> {

  // Minimum number of pixels between source labels
  private static final int CLUSTER_MINIMUM_PIXEL_DISTANCE = 60;

  // Resolution below which to switch to labeling individual sources
  private static final double CLUSTER_MINIMUM_RESOLUTION = 1.5;

  private static String selectedFeatureId = null;

  private static final LabelSvgIconBuilder LABEL_SVG_ICON_BUILDER = new LabelSvgIconBuilder();

  private final LayerInfo info;
  private final ol.layer.Vector layer;

  private final Map<String, Collection<Feature>> featuresMap = new HashMap<>();
  private final Map<String, Map<String, LabelFeature>> labelFeaturesById = new HashMap<>();
  private final Map<String, Cluster> sources = new HashMap<>();

  private final SourceMarkerLayerEventBinder EVENT_BINDER = GWT.create(SourceMarkerLayerEventBinder.class);

  private boolean showNameLabels = false;

  interface SourceMarkerLayerEventBinder extends EventBinder<SourceMarkerLayer> {}

  private final MapConfigurationContext mapConfigurationContext;

  @Inject
  public SourceMarkerLayer(@Assisted final LayerInfo info, final EventBus eventBus, @Assisted final int zIndex,
      final MapConfigurationContext mapConfigurationContext) {
    this.info = info;
    this.mapConfigurationContext = mapConfigurationContext;

    final VectorLayerOptions vectorLayerOptions = OLFactory.createOptions();
    vectorLayerOptions.setStyle((feature, v) -> getStyleFunction(feature, v));

    layer = new ol.layer.Vector(vectorLayerOptions);
    layer.setZIndex(zIndex);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  private Collection<Feature> getFeatureCollection(final String situationId) {
    return featuresMap.get(situationId);
  }

  @EventHandler
  public void onEmissionSourceListChangeEvent(final EmissionSourceListChangeEvent e) {
    init(e.getSituationId(), e.getValue());
  }

  @EventHandler
  public void onEmissionSourceAddedEvent(final EmissionSourceAddedEvent e) {
    addLabelFeature(e.getSituationId(), e.getValue());
  }

  @EventHandler
  public void onEmissionSourceDeletedEvent(final EmissionSourceDeletedEvent e) {
    removeLabelFeature(e.getSituationId(), e.getValue());
  }

  @EventHandler
  public void onEmissionSourceUpdatedEvent(final EmissionSourceUpdatedEvent e) {
    if (getFeatureCollection(e.getSituationId()) != null) {
      removeLabelFeature(e.getSituationId(), e.getValue());
      addLabelFeature(e.getSituationId(), e.getValue());
    }
  }

  private void addLabelFeature(final String situationId, final EmissionSourceFeature source) {
    final LabelFeature labelFeature = getOrCreateLabelFeature(situationId, source);
    if (labelFeature != null) {
      getFeatureCollection(situationId).push(labelFeature);
    }
  }

  private void removeLabelFeature(final String situationId, final EmissionSourceFeature source) {
    final LabelFeature labelFeature = labelFeaturesById.get(situationId).remove(source.getId());
    if (labelFeature != null) {
      getFeatureCollection(situationId).remove(labelFeature);
    }
  }

  private void init(final String situationId, final List<EmissionSourceFeature> features) {
    // Clean the old source if any
    final Vector oldVectorSource = layer.getSource();
    if (oldVectorSource != null) {
      oldVectorSource.clear(true);
    }

    // Construct labelFeature storage
    labelFeaturesById.put(situationId, new HashMap<>());

    // Clustering is quite efficient, so if sources are clustered there is no real
    // reason to apply a limit, however when sources are unclustered there's a
    // noticeable slowdown starting from a couple thousand features.
    final LabelFeature[] array = features.stream()
        .map(source -> getOrCreateLabelFeature(situationId, source))
        .filter(source -> source != null)
        .limit(ApplicationLimits.SOURCE_LAYER_DRAW_LIMIT)
        .toArray(len -> new LabelFeature[len]);

    final Collection<Feature> olFeatures = new OptimizedCollection<Feature>(array);
    featuresMap.put(situationId, olFeatures);

    final VectorOptions vectorSourceOptions = OLFactory.createOptions();
    vectorSourceOptions.setFeatures(olFeatures);
    final Vector vectorSource = new Vector(vectorSourceOptions);

    final ClusterOptions clusterOptions = new ClusterOptions();
    clusterOptions.setDistance(CLUSTER_MINIMUM_PIXEL_DISTANCE);
    clusterOptions.setSource(vectorSource);
    clusterOptions.setGeometryFunction(feature -> {
      if (feature.getId().equals(selectedFeatureId)) {
        return null;
      }
      return (Point) feature.getGeometry();
    });

    final Cluster cluster = new Cluster(clusterOptions);
    sources.put(situationId, cluster);
    layer.setSource(cluster);
  }

  @EventHandler
  public void onEmissionSourceModifyGeometry(final EmissionSourceModifyGeometryCommand e) {
    if (e.getValue() == null) {
      selectedFeatureId = null;
    } else {
      selectedFeatureId = e.getValue().getId();
    }
    layer.getSource().changed();
    ((Cluster) layer.getSource()).getSource().changed();
  }

  @EventHandler
  public void onSituationSwitch(final SituationSwitchEvent e) {
    getFeatureCollection(e.getValue());
    layer.setSource(sources.get(e.getValue()));
  }

  @EventHandler
  public void onNameLabelVisibilityCommand(final NameLabelVisibilityCommand e) {
    this.showNameLabels = e.getValue();

    layer.getSource().changed();
    ((Cluster) layer.getSource()).getSource().changed();
  }

  private Style[] getStyleFunction(final Feature feature, final double resolution) {
    final LabelFeature[] features = feature.get("features");
    if (features.length == 0) {
      // This method can be called without any sources. So we just return null as
      // there is nothing to show.
      return null;
    }
    final boolean isClustered = features.length > 1 && resolution > CLUSTER_MINIMUM_RESOLUTION;

    final List<LabelSvgIconBuilder.Label> markers = new ArrayList<>();
    if (isClustered) {

      final LabelSvgIconBuilder.Label marker = new LabelSvgIconBuilder.Label(
          M.messages().esMarkerClusterLabel(features.length),
          LabelStyle.getLabelColor(features));
      markers.add(marker);

    } else {
      for (final LabelFeature f : features) {
        final String label = f.getId().equals(selectedFeatureId) ? f.getId() : showNameLabels ? f.getLabel() : f.getId();
        final String correctedLabel = label.startsWith("-") ? label.substring(1) : label;

        final LabelSvgIconBuilder.Label marker = new LabelSvgIconBuilder.Label(
            correctedLabel, LabelStyle.getLabelColor(f));
        markers.add(marker);
      }
    }

    final DynamicSvgIcon svg = LABEL_SVG_ICON_BUILDER.getSvgIcon(markers, false, LabelSvgIconBuilder.LabelStyle.EMISSION_SOURCE);

    final IconOptions iconOptions = new IconOptions();
    iconOptions.setSrc(svg.getSrc());
    iconOptions.setAnchor(svg.getAnchorInPx());
    iconOptions.setAnchorXUnits("pixels");
    iconOptions.setAnchorYUnits("pixels");
    iconOptions.setScale(mapConfigurationContext.getIconScale());
    final Icon icon = new Icon(iconOptions);

    final StyleOptions styleOptions = new StyleOptions();
    styleOptions.setImage(icon);
    final Style style = new Style(styleOptions);

    return new Style[] { style };
  }

  private LabelFeature getOrCreateLabelFeature(final String situationId, final EmissionSourceFeature source) {
    if (labelFeaturesById.get(situationId).get(source.getId()) == null) {
      labelFeaturesById.get(situationId).put(source.getId(), createLabelFeature(source));
    }
    return labelFeaturesById.get(situationId).get(source.getId());
  }

  private LabelFeature createLabelFeature(final EmissionSourceFeature source) {
    if (RoadEmissionSourceTypes.isRoadSource(source)) {
      return null;
    }

    return LabelFeature.create(source.getId(), source.getGeometry(), source.getLabel(), LabelStyle.getLabelColor(source), source);
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public Optional<LayerInfo> getInfoOptional() {
    return Optional.of(info);
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
