/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.validation;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.Validations;
import nl.overheid.aerius.wui.application.context.EmissionSourceValidationContext;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

/**
 * A simple component containing syntactic sugar easily usable in templates
 */
@Component
public abstract class ErrorWarningValidator extends BasicVueComponent {
  @Data @JsProperty Map<String, Boolean> childErrors = new HashMap<>();
  @Data @JsProperty Map<String, Boolean> childWarnings = new HashMap<>();

  @Data @Inject EmissionSourceValidationContext validationContext;

  @Data public boolean displayProblems;

  @JsMethod
  public void touch() {
    if (this instanceof HasValidators) {
      final Validations validations = ((HasValidators<?>) this).getV();
      if (validations != null) {
        validations.$touch();
      }
    }
  }

  /**
   * A slightly more cleverer touch, which either touches or resets the
   * validations. To be used as
   *
   * <pre>
   *   &#64;touch="nudge((boolean) $event)"
   * </pre>
   *
   * Called nudge rather than touch because there can't be JsMethod overloads
   */
  @JsMethod
  public void nudge(final boolean touch) {
    if (this instanceof HasValidators) {
      final Validations validations = ((HasValidators<?>) this).getV();
      if (validations != null) {
        if (touch) {
          validations.$touch();
        } else {
          validations.$reset();
        }
      }
    }
  }

  /**
   * A more archaic dirty mechanic which doesn't depend on a Validations construct
   */
  @JsMethod
  public void displayProblems(final boolean displayProblems) {
    this.displayProblems = displayProblems;
  }

  @JsMethod
  public void emitWarnings(final boolean neww) {
    vue().$emit("warnings", neww);
  }

  @JsMethod
  public void emitErrors(final boolean neww) {
    vue().$emit("errors", neww);
  }

  @JsMethod
  public void trackChildError(final String key, final boolean flag) {
    childErrors.put(key, flag);
  }

  @JsMethod
  public void trackChildWarning(final String key, final boolean flag) {
    childWarnings.put(key, flag);
  }

  /**
   * Return true if something was removed
   */
  public boolean removeChildError(final String key) {
    return childErrors.remove(key) != null;
  }

  /**
   * Return true if something was removed
   */
  public boolean removeChildWarning(final String key) {
    return childWarnings.remove(key) != null;
  }

  @Computed("hasChildErrors")
  public boolean hasChildErrors() {
    return childErrors.values().stream().anyMatch(v -> v);
  }

  @Computed("hasChildWarnings")
  public boolean hasChildWarnings() {
    return childWarnings.values().stream().anyMatch(v -> v);
  }
}
