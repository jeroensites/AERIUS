/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.air.layer;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.application.components.map.MapComponentFactory;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointCreatePlace;
import nl.overheid.aerius.wui.application.ui.air.AirQualitySubActivity;
import nl.overheid.aerius.wui.application.ui.air.AirQualityView;

public class LayerActivity implements AirQualitySubActivity {
  @Inject PlaceController placeController;

  @Inject
  public LayerActivity(@Assisted final AirQualityView view) {
    view.setComponent(LayerViewFactory.get(), this);
    view.noTertiary();
    view.setMain(MapComponentFactory.get());
  }

  public void goToImport() {
    placeController.goTo(new CalculationPointCreatePlace());
  }
}
