/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.command;

import java.util.List;

import nl.aerius.wui.event.SimpleGenericEvent;
import nl.overheid.aerius.wui.application.components.importer.ApplicationImportModalComponent.ImportFilter;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

/**
 * Command to trigger retrieving data of files send to server via import dialog.
 */
public class ImportFilesCommand extends SimpleGenericEvent<List<FileUploadStatus>> {

  private final boolean intoActiveSituation;
  private final ImportFilter importFilter;

  public ImportFilesCommand(final List<FileUploadStatus> files, final boolean intoActiveSituation, final ImportFilter importFilter) {
    super(files);
    this.intoActiveSituation = intoActiveSituation;
    this.importFilter = importFilter;
  }

  public boolean isIntoActiveSituation() {
    return intoActiveSituation;
  }

  public ImportFilter getImportFilter() {
    return importFilter;
  }
}
