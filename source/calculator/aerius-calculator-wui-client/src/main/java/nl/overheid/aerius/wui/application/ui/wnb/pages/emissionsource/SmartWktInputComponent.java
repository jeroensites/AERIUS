/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsource;

import java.util.Optional;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import ol.EventsKey;
import ol.Observable;
import ol.geom.Geometry;
import ol.geom.GeometryType;

import elemental2.dom.HTMLInputElement;
import elemental2.dom.InputEvent;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.util.WKTUtil;
import nl.overheid.aerius.wui.application.util.WKTUtil.GeometryResult;

@Component(components = {
    MinimalInputComponent.class,
    ValidationBehaviour.class,
})
public class SmartWktInputComponent extends ErrorWarningValidator implements IsVueComponent {
  @Prop Geometry geometry;
  @Prop GeometryType geometryType;

  @Prop boolean showErrors;

  @Data String locationString;

  private EventsKey on;

  @Watch("showErrors")
  public void onShowErrorsChange(final boolean neww) {
    displayProblems(neww);
  }

  @Watch(value = "geometry", isImmediate = true)
  public void onGeometryChange(final Geometry geom) {
    updateGeometry(geom);
    if (on != null) {
      Observable.unByKey(on);
      on = null;
    }
    if (geom != null) {
      on = geom.on("change", v -> {
        updateGeometry(geom);
      });
    }
  }

  private void updateGeometry(final Geometry geom) {
    locationString = Optional.ofNullable(geom)
        .map(v -> OL3GeometryUtil.toWktString(geom))
        .orElse("");
  }

  @Watch(value = "locationString", isImmediate = true)
  public void onLocationStringChange() {
    vue().$emit("wkt-change", locationString);
  }

  @JsMethod
  public String getPlaceholderText() {
    return i18n.esLocationDirectPlaceholder(Optional.ofNullable(geometryType)
        .orElse(GeometryType.Point).name());
  }

  @JsMethod
  public void updateFromDirectInput(final InputEvent event) {
    final String wktInput = ((HTMLInputElement) event.target).value;

    locationString = wktInput;

    final GeometryResult result = WKTUtil.tryReadGeometry(wktInput);
    if (result.getGeometry() == null) {
      return;
    }

    vue().$emit("geometry-change", result.getGeometry());
  }

  @Computed("isGeometryValid")
  public boolean isGeometryValid() {
    final GeometryResult result = WKTUtil.tryReadGeometry(locationString);
    return result.getGeometry() != null;
  }
}
