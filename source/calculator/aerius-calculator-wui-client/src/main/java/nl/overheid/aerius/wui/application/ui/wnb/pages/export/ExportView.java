/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.wnb.pages.export;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.ui.wnb.pages.export.ExportValidators.ExportValidations;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(customizeOptions = {
    ExportValidators.class
}, directives = {
    ValidateDirective.class
}, components = {
    LabeledInputComponent.class,
    ExportGMLView.class,
    ExportPDFView.class
})
public class ExportView extends BasicVueView implements HasCreated {
  @Prop ExportActivity presenter;

  @Inject @Data ScenarioContext context;
  @Inject @Data ExportContext exportContext;

  @Ref public ExportPDFView exportPDFView;
  @Ref public ExportGMLView exportGMLView;

  @Data String emailV;
  @JsProperty(name = "$v") ExportValidations validation;

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
    emailV = exportContext.getEmail();
  }

  @Computed
  public ExportValidations getV() {
    return validation;
  }

  @Computed
  public List<SituationContext> getSituations() {
    return context.getSituations();
  }

  @Computed
  public void setEmail(final String email) {
    exportContext.setEmail(email);
    emailV = email;
  }

  @Computed
  public String getEmail() {
    return exportContext.getEmail();
  }

  @Computed
  public JobType getToggleSelect() {
    return exportContext.getJobType();
  }

  @JsMethod
  public boolean exportDisabled() {
    return isError();
  }

  @JsMethod
  public void setToggle(final JobType jobType) {
    exportContext.setJobType(jobType);
  }

  @JsMethod
  public String emailError() {
    return i18n.errorEmail(getEmail());
  }

  @Computed
  public boolean hasExportWithoutRequiredEmissions() {
    return getToggleSelect() == JobType.CALCULATION && !ScenarioContext.hasAnyEmissions(context) && !exportContext.getIncludingResults().isEmpty()
        || getToggleSelect() == JobType.REPORT && !ScenarioContext.hasAnyEmissions(context);
  }

  @JsMethod
  public void startExport() {
    touchAll();

    if (!isError()) {
      if (JobType.REPORT.equals(exportContext.getJobType())) {
        exportContext.setExportType(ExportType.PAA);
      } else if (JobType.CALCULATION.equals(exportContext.getJobType())) {
        if (exportContext.getIncludingResults().isEmpty()) {
          exportContext.setExportType(ExportType.GML_SOURCES_ONLY);
        } else {
          exportContext.setExportType(ExportType.GML_WITH_RESULTS);
        }
      }
      presenter.startExport(exportContext.getJobType(), exportContext.getExportType(), exportContext.getEmail());
    }
  }

  private void touchAll() {
    validation.emailV.$touch();
    if (exportContext.getJobType() == JobType.REPORT) {
      exportPDFView.touchAll();
    }
    if (exportContext.getJobType() == JobType.CALCULATION) {
      exportGMLView.touchAll();
    }
  }

  public boolean isError() {
    if (validation.emailV.error || hasExportWithoutRequiredEmissions()) {
      return true;
    }

    if (exportContext.getJobType() == JobType.REPORT) {
      return (exportPDFView != null && exportPDFView.isError());
    } else {
      return (exportGMLView != null && exportGMLView.isError());
    }
  }
}
