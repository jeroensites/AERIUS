/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.importer;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.google.web.bindery.event.shared.EventBus;

import ol.format.GeoJson;

import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.components.importer.ApplicationImportModalComponent.ImportFilter;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.HasMoorings;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.i18n.ApplicationMessages;
import nl.overheid.aerius.wui.application.i18n.M;

public class ImportParcelConverter {
  private static final GeoJson GEO_JSON = new GeoJson();
  private static final ApplicationMessages i18n = M.messages();

  @Inject EventBus eventBus;

  private static final Predicate<EmissionSourceFeature> IS_SHIPPING_FEATURE = v -> v
      .getEmissionSourceType() == EmissionSourceType.SHIPPING_MARITIME_MARITIME
      || v.getEmissionSourceType() == EmissionSourceType.SHIPPING_MARITIME_INLAND
      || v.getEmissionSourceType() == EmissionSourceType.SHIPPING_INLAND;

  private static final Predicate<EmissionSourceFeature> IS_MOORING = v -> v
      .getEmissionSourceType() == EmissionSourceType.SHIPPING_INLAND_DOCKED
      || v.getEmissionSourceType() == EmissionSourceType.SHIPPING_MARITIME_DOCKED;

  public SituationContext createSituationFromParcel(final ImportParcelSituation parcel) {
    final SituationContext situation = new SituationContext(parcel.getId());
    convertParcelIntoSituation(parcel, situation, ImportFilter.ALL);
    return situation;
  }

  public boolean addMetaData(final ImportParcel parcel, final ScenarioContext scenarioContext) {
    if (parcel.getImportedMetaData() != null) {
      scenarioContext.setScenarioMetaData(parcel.getImportedMetaData());
      return true;
    }
    return false;
  }

  public CalculationPointFeature[] getCalculationPoints(final Object unparsedCalculationPoints) {
    return (CalculationPointFeature[]) GEO_JSON.readFeatures(unparsedCalculationPoints);
  }

  public void convertParcelIntoSituation(final ImportParcelSituation parcel, final SituationContext situation, final ImportFilter filter) {
    final String newSituationName = parcel.getName();
    if (newSituationName != null && !newSituationName.isEmpty()) {
      situation.setName(newSituationName);
    }

    situation.setId(parcel.getId());
    situation.setType(parcel.getType());
    if (!"0".equals(parcel.getYear())) {
      situation.setYear(parcel.getYear());
    }
    situation.setNettingFactor(parcel.getNettingFactor());

    addParcelToSituation(parcel, situation, filter);
  }

  public void addParcelToSituation(final ImportParcelSituation parcel, final SituationContext situation, final ImportFilter filter) {
    switch (filter) {
    case BUILDINGS:
      addBuildings(parcel, situation);
      break;
    case SOURCES:
      addEmissionSources(parcel, situation, true);
      break;
    case ALL:
      final Map<String, String> buildingRemappings = ensureUniqueSourceBuildingMapping(parcel, situation);
      final Map<String, String> mooringRemappings = ensureUniqueMooringMapping(parcel, situation);

      addEmissionSources(parcel, situation, false, buildingRemappings, mooringRemappings);
      addBuildings(parcel, situation, buildingRemappings);
      break;
    default:
      throw new RuntimeException("Unknown filter type encountered: " + filter);
    }
  }

  private static Map<String, String> ensureUniqueMooringMapping(final ImportParcelSituation parcel,
      final SituationContext situation) {
    final List<EmissionSourceFeature> parcelMoores = Stream.of((EmissionSourceFeature[]) GEO_JSON.readFeatures(parcel.getEmissionSources()))
        .filter(IS_MOORING)
        .collect(Collectors.toList());

    final Set<String> existingMoores = situation.getSources().stream()
        .filter(IS_MOORING)
        .map(v -> v.getGmlId())
        .collect(Collectors.toSet());

    return ensureUniqueMooringMapping(parcelMoores, existingMoores);
  }

  private static Map<String, String> ensureUniqueMooringMapping(final List<EmissionSourceFeature> parcelSources,
      final Set<String> existingMoores) {
    return parcelSources.stream()
        .map(v -> v.getGmlId())
        .filter(v -> existingMoores.contains(v))
        .collect(Collectors.toMap(v -> v, v -> produceUniqueNewKey(v, existingMoores)));
  }

  /**
   * Return a map specifying the ids that must be remapped to another value.
   */
  private static Map<String, String> ensureUniqueSourceBuildingMapping(final ImportParcelSituation parcel, final SituationContext situation) {
    final List<BuildingFeature> parcelBuildings = Stream.of((BuildingFeature[]) GEO_JSON.readFeatures(parcel.getBuildings()))
        .collect(Collectors.toList());

    final Set<String> existingBuildings = situation.getBuildings()
        .stream().map(v -> v.getGmlId())
        .collect(Collectors.toSet());

    return ensureUniqueSourceBuildingMapping(parcelBuildings, existingBuildings);
  }

  private static Map<String, String> ensureUniqueSourceBuildingMapping(final List<BuildingFeature> parcelBuildings,
      final Set<String> existingBuildings) {
    return parcelBuildings.stream()
        .map(v -> v.getGmlId())
        .filter(v -> existingBuildings.contains(v))
        .collect(Collectors.toMap(v -> v, v -> produceUniqueNewKey(v, existingBuildings)));
  }

  /**
   * Recursively increment the key with a remapping postfix in order to ensure a
   * unique key is produced.
   *
   * @return a unique key
   */
  private static String produceUniqueNewKey(final String key, final Set<String> existingKeys) {
    final String newKey = smartIncrementedRemapping(key);
    if (existingKeys.contains(newKey)) {
      return produceUniqueNewKey(newKey, existingKeys);
    } else {
      return newKey;
    }
  }

  private static String smartIncrementedRemapping(final String key) {
    if (key.matches(".+-remap-[1-9]+$")) {
      final int endBracket = key.lastIndexOf("-");
      final String numPortion = key.substring(endBracket + 1);

      try {
        final int num = Integer.parseInt(numPortion);
        final String labelBare = key.substring(0, key.lastIndexOf("-") + 1);
        return labelBare + (num + 1);
      } catch (final NumberFormatException e) {
        return key + "-remap-1";
      }
    } else {
      return key + "-remap-1";
    }
  }

  private static void addEmissionSources(final ImportParcelSituation parcel, final SituationContext situation, final boolean severBuildings) {
    addEmissionSources(parcel, situation, severBuildings, null, null);
  }

  private static void addEmissionSources(final ImportParcelSituation parcel, final SituationContext situation, final boolean severBuildings,
      final Map<String, String> buildingRemappings, final Map<String, String> mooringRemappings) {
    final EmissionSourceFeature[] sources = (EmissionSourceFeature[]) GEO_JSON.readFeatures(parcel.getEmissionSources());
    final List<EmissionSourceFeature> situationSources = situation.getOriginalSources();

    Stream.of(sources).forEach(source -> {
      if (severBuildings) {
        severBuildingLink(source);
      }

      // Try to remap the building id according to the remappings specified (note:
      // this won't do anything if building links have already been severed)
      tryBuildingRemap(source, buildingRemappings);

      // Try to remap the mooring ids according to the remappings specificied
      tryMooringRemap(source, mooringRemappings);

      // Post-process the sources so known properties aren't undefined
      EmissionSourcePostProcessor.postProcessEmissionSource(source);

      FeatureUtil.addAndSetId(situationSources, source);
    });
  }

  private static void tryMooringRemap(final EmissionSourceFeature source, final Map<String, String> mooringRemappings) {
    if (mooringRemappings == null) {
      return;
    }

    // Remap the moore gml id
    if (IS_MOORING.test(source)) {
      final String sourceGmlId = source.getGmlId();
      if (mooringRemappings.containsKey(sourceGmlId)) {
        source.setGmlId(mooringRemappings.get(sourceGmlId));
      }
    }

    // Remap the shipping feature's moores
    if (IS_SHIPPING_FEATURE.test(source)) {
      final HasMoorings hasMoores = (HasMoorings) source;
      if (mooringRemappings.containsKey(hasMoores.retrieveMooringAId())) {
        hasMoores.persistMooringAId(mooringRemappings.get(hasMoores.retrieveMooringAId()));
      }

      if (mooringRemappings.containsKey(hasMoores.retrieveMooringBId())) {
        hasMoores.persistMooringBId(mooringRemappings.get(hasMoores.retrieveMooringBId()));
      }
    }
  }

  private static void tryBuildingRemap(final EmissionSourceFeature source, final Map<String, String> buildingRemappings) {
    if (buildingRemappings == null) {
      return;
    }

    if (source.getCharacteristicsType() == CharacteristicsType.OPS) {
      final OPSCharacteristics characteristics = (OPSCharacteristics) source.getCharacteristics();

      if (buildingRemappings.containsKey(characteristics.getBuildingId())) {
        characteristics.setBuildingId(buildingRemappings.get(characteristics.getBuildingId()));
      }
    } else if (source.getCharacteristicsType() == CharacteristicsType.ADMS) {
      final ADMSCharacteristics characteristics = (ADMSCharacteristics) source.getCharacteristics();

      if (buildingRemappings.containsKey(characteristics.getBuildingId())) {
        characteristics.setBuildingId(buildingRemappings.get(characteristics.getBuildingId()));
      }
    }
  }

  private static void severBuildingLink(final EmissionSourceFeature source) {
    if (source.getCharacteristicsType() == CharacteristicsType.OPS) {
      ((OPSCharacteristics) source.getCharacteristics()).setBuildingId(null);
    } else if (source.getCharacteristicsType() == CharacteristicsType.ADMS) {
      ((ADMSCharacteristics) source.getCharacteristics()).setBuildingId(null);
    }
  }

  private static void addBuildings(final ImportParcelSituation parcel, final SituationContext situation) {
    addBuildings(parcel, situation, null);
  }

  private static void addBuildings(final ImportParcelSituation parcel, final SituationContext situation, final Map<String, String> remappings) {
    final BuildingFeature[] parcelBuildings = (BuildingFeature[]) GEO_JSON.readFeatures(parcel.getBuildings());
    final List<BuildingFeature> situationBuildings = situation.getOriginalBuildings();
    Stream.of(parcelBuildings).forEach(building -> {
      FeatureUtil.addAndSetId(situationBuildings, building);

      // Remap the building gml id if it is specified it should be remapped
      if (remappings != null && remappings.containsKey(building.getGmlId())) {
        building.setGmlId(remappings.get(building.getGmlId()));
      }
    });
  }
}
