/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.calculation;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsProperty;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.util.GWTAtomicInteger;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.command.ChangeSituationResultKeyCommand;
import nl.overheid.aerius.wui.application.command.FetchSummaryResultsCommand;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationHandle;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.event.CalculationStartEvent;
import nl.overheid.aerius.wui.application.event.CalculationStatusEvent;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.service.CalculationServiceAsync;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.habitat.ResultHabitatView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.habitat.SelectResultHabitatStatisticTypeCommand;

/**
 * This daemon manages retrieval, persistence, and caching of result summaries
 * via resultkeys
 */
@Singleton
public class ResultsSummaryDaemon extends BasicEventComponent {
  private static final ResultsDaemonEventBinder EVENT_BINDER = GWT.create(ResultsDaemonEventBinder.class);

  interface ResultsDaemonEventBinder extends EventBinder<ResultsSummaryDaemon> {}

  private final @JsProperty Map<SituationResultsKey, Integer> resultAges = new HashMap<>();

  @Inject CalculationServiceAsync calculationService;
  @Inject CalculationContext calculationContext;

  private final GWTAtomicInteger calculationAge = new GWTAtomicInteger();
  private final Map<SituationResultsKey, GWTAtomicInteger> serviceCallCounters = new HashMap<>();

  @EventHandler
  public void onChangeSituationResultKeyCommand(final ChangeSituationResultKeyCommand c) {
    getResultContext().ifPresent(context -> {
      final SituationResultsKey key = c.getValue();
      updateSituationHandle(context, key.getSituationHandle());
      updateResultType(context, key.getResultType());
      updateEmissionResultKey(context, key.getEmissionResultKey());
      updateHexagonType(context, key.getHexagonType());

      // Clear result value bounds
      eventBus.fireEvent(new ClearResultValueBoundsCommand());

      renewCalculationResult(c.getValue(), context, v -> eventBus.fireEvent(new SituationResultsSelectedEvent(v)));
    });
  }

  private void updateSituationHandle(final ResultSummaryContext context, final SituationHandle handle) {
    context.setSituationHandle(handle);
  }

  private void updateResultType(final ResultSummaryContext context, final ScenarioResultType type) {
    final ScenarioResultType old = context.getResultType();
    if (old == type) {
      return;
    }

    context.setResultType(type);

    // Update habitat result type statistics to be compatible with the scenario
    final ResultStatisticType defaultStatisticType = Optional
        .ofNullable(ResultHabitatView.getHabitatStatisticsTypes().get(type))
        .map(st -> st.get(0)).orElse(null);
    eventBus.fireEvent(new SelectResultHabitatStatisticTypeCommand(defaultStatisticType));
  }

  private void updateEmissionResultKey(final ResultSummaryContext context, final EmissionResultKey emissionResultKey) {
    context.setEmissionResultKey(emissionResultKey);
  }

  private void updateHexagonType(final ResultSummaryContext context, final SummaryHexagonType type) {
    context.setHexagonType(type);
  }

  @EventHandler
  public void onFetchSummaryResultsCommand(final FetchSummaryResultsCommand c) {
    // Event is fired after request completed.
    c.silence();

    getResultContext()
        .ifPresent(context -> renewCalculationResult(c.getValue(), context, key -> eventBus.fireEvent(c.getEvent())));
  }

  private void renewCalculationResult(final SituationResultsKey resultsKey, final ResultSummaryContext context,
      final Consumer<SituationResultsKey> callback) {
    if (actualizeResultKey(resultsKey)) {
      eventBus.fireEvent(new SituationResultsSelectedEvent(resultsKey));
      return;
    }

    final CalculationJobContext calculation = calculationContext.getActiveCalculation();
    if (calculation == null || calculation.getCalculationCode() == null) {
      // Log a warning to point out a regression has occurred (and handled)
      GWTProd.warn("Not expecting calculation to be empty at this point.");
      return;
    }

    final int count = serviceCallCounters.computeIfAbsent(resultsKey, key -> new GWTAtomicInteger()).incrementAndGet();
    context.setLoading(true);
    calculationService.getSituationResultsSummary(calculation.getCalculationCode(), resultsKey, AppAsyncCallback.create(summary -> {
      // Belated bail if the counter has updated -- will fetch next time
      if (count != serviceCallCounters.get(resultsKey).get()) {
        return;
      }

      context.setLoading(false);
      context.addResultSummary(resultsKey, summary);
      callback.accept(resultsKey);
    }, e -> NotificationUtil.broadcastError(eventBus, M.messages().notificationCalculationStatusError(e.getMessage()))));
  }

  /**
   * Return true if local result key is actual, false if not
   */
  private boolean actualizeResultKey(final SituationResultsKey resultsKey) {
    final int localAge = calculationAge.get();
    // If the result age is equal to the calculation age, bail out (this indicates a
    // result is not potentially newer and can be retrieved out of the cache)
    final int resultAge = getResultAge(resultsKey);
    if (resultAge == localAge) {
      return true;
    }

    actualizeResultAge(resultsKey, localAge);
    return false;
  }

  @EventHandler
  public void calculationStartEvent(final CalculationStartEvent e) {
    calculationAge.set(0);
  }

  @EventHandler
  public void calculationStatusEvent(final CalculationStatusEvent e) {
    calculationAge.incrementAndGet();

    if (e.getValue().getJobProgress().getHexagonCount() > 0) {
      updateActiveSituationResultsSummary();
    }
  }

  private void updateActiveSituationResultsSummary() {
    getResultContext().ifPresent(context -> {
      final SituationResultsKey resultsKey = context.getSituationResultsKey();
      renewCalculationResult(resultsKey, context, key -> eventBus.fireEvent(new SituationResultsSelectedEvent(key)));
    });
  }

  public Integer getResultAge(final SituationResultsKey resultsKey) {
    return resultAges.computeIfAbsent(resultsKey, key -> 0);
  }

  public void actualizeResultAge(final SituationResultsKey resultsKey, final int calculationAge) {
    resultAges.put(resultsKey, calculationAge);
  }

  private Optional<ResultSummaryContext> getResultContext() {
    return Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(v -> v.getResultContext());
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
