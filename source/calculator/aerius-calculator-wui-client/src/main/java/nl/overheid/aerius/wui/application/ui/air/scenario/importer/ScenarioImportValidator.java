/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.air.scenario.importer;

import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.ValidatorBuilder;

public class ScenarioImportValidator extends ValidationOptions<ScenarioImportView> {
  @Override
  protected void constructValidators(final Object parent, final ValidatorInstaller installer) {
    final ScenarioImportView instance = Js.uncheckedCast(parent);

    installer.install("name", () -> instance.name = null, ValidatorBuilder.create().required());
    installer.install("calculationYear", () -> instance.calculationYear = null, ValidatorBuilder.create().required().numeric());
    installer.install("srm2", () -> instance.srm2 = null, ValidatorBuilder.create().required().numeric());
    installer.install("email", () -> instance.email = null, ValidatorBuilder.create().email());
  }
}
