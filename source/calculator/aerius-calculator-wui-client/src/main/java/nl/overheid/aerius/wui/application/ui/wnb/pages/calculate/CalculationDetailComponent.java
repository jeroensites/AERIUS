/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.calculate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculateContext;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    CollapsiblePanel.class,
    LabeledInputComponent.class,
    CheckBoxComponent.class
})
public class CalculationDetailComponent extends BasicVueView {
  @Prop @JsProperty List<SituationContext> situations;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data @JsProperty CalculateContext calculateContext;

  @Computed
  public List<String> getMeteoYears() {
    return applicationContext.getActiveThemeConfiguration().getMeteoYears();
  }

  @Computed("hasMeteoConfiguration")
  public boolean hasMeteoConfiguration() {
    return getMeteoYears().size() > 1;
  }

  /**
   * When is this situation list changed? None of the stuff that happens here
   * should happen in a *view* component but rather in a daemon
   *
   * (to be clear: view components are for *viewing*)
   */
  @Watch(value = "situations", isImmediate = true)
  void onSituationsChange(final List<SituationContext> neww) {
    // TODO Move logic to daemon
    final List<SituationContext> referenceSituations = getReferenceSituations();
    final String referenceSituationId = getReferenceSituationId();
    if (referenceSituations.stream().noneMatch(v -> referenceSituationId.equals(v.getId()))) {
      setReferenceSituationId(referenceSituations.stream().findFirst()
          .map(v -> v.getId())
          .orElse(""));
    }

    // TODO Move logic to daemon
    final List<SituationContext> nettingSituations = getNettingSituations();
    final String nettingSituationId = getNettingSituationId();
    if (nettingSituations.stream().noneMatch(v -> nettingSituationId.equals(v.getId()))) {
      setNettingSituationId(nettingSituations.stream().findFirst()
          .map(v -> v.getId())
          .orElse(""));
    }

    // TODO Move logic to daemon
    for (final String id : calculateContext.getSelectedProposedAndTemporarySituations()) {
      if (referenceSituations.stream()
          .noneMatch(v -> id.equals(v.getId()))) {
        // wtf?
        // TODO Move logic to daemon
        calculateContext.removeProposedAndTemporarySituation(id);
      }
    }

    // TODO Move logic to daemon
    for (final SituationContext row : getOtherSituations()) {
      calculateContext.addProposedAndTemporarySituation(row.getId());
    }
  }

  @Computed
  public List<CalculationType> getCalculationTypes() {
    return applicationContext.getActiveThemeConfiguration().getCalculationTypes();
  }

  @Computed
  public void setReferenceSituationId(final String id) {
    calculateContext.setReferenceSituationId(id);
  }

  @Computed
  public String getReferenceSituationId() {
    return calculateContext.getReferenceSituationId();
  }

  @Computed
  public void setNettingSituationId(final String id) {
    calculateContext.setNettingSituationId(id);
  }

  @Computed
  String getNettingSituationId() {
    return calculateContext.getNettingSituationId();
  }

  @Computed
  public void setCalculationType(final String calculationType) {
    calculateContext.getCalculationOptions().setCalculationType(CalculationType.safeValueOf(calculationType));
  }

  @Computed
  public String getCalculationType() {
    return calculateContext.getCalculationOptions().getCalculationType().name();
  }

  @Computed
  public String getMeteoYear() {
    return calculateContext.getMeteoYear();
  }

  @Computed
  public void setMeteoYear(final String year) {
    calculateContext.setMeteoYear(year);
  }

  @Computed("isReference")
  public boolean isReference() {
    return !getReferenceSituations().isEmpty();
  }

  @Computed("isNetting")
  public boolean isNetting() {
    return !getNettingSituations().isEmpty();
  }

  @Computed("isOther")
  public boolean isOther() {
    return !getOtherSituations().isEmpty();
  }

  @Computed
  public List<SituationContext> getReferenceSituations() {
    return situations.stream()
        .filter(v -> SituationType.REFERENCE.equals(v.getType()))
        .collect(Collectors.toList());
  }

  @Computed
  public List<SituationContext> getNettingSituations() {
    return situations.stream()
        .filter(v -> SituationType.NETTING.equals(v.getType()))
        .collect(Collectors.toList());
  }

  @Computed
  public List<ScenarioResultType> getScenarioResultTypes() {
    return Arrays.asList(ScenarioResultType.values());
  }

  @Computed
  public List<SituationContext> getOtherSituations() {
    return situations.stream()
        .filter(v -> !v.getId().equals(getNettingSituationId())
            && !v.getId().equals(getReferenceSituationId()))
        .collect(Collectors.toList());
  }

  @JsMethod
  public List<EmissionResultKey> getEmissions() {
    return applicationContext.getActiveThemeConfiguration().getEmissionResultKeys();
  }

  @JsMethod
  public boolean isProposedAndTemporarySituation(final String id) {
    return calculateContext.getSelectedProposedAndTemporarySituations().contains(id);
  }

  @JsMethod
  public void updateProposedAndTemporarySituation(final boolean checked, final SituationContext situation) {
    if (checked) {
      // TODO Move this logic to a daemon
      calculateContext.addProposedAndTemporarySituation(situation.getId());
    } else {
      calculateContext.removeProposedAndTemporarySituation(situation.getId());
    }
  }
}
