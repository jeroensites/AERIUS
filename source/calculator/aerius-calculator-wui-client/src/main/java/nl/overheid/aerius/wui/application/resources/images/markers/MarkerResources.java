/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.resources.images.markers;

import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.DataResource.MimeType;

public interface MarkerResources {

  @Source("map-infomarker.svg")
  @MimeType("image/svg+xml")
  DataResource markerInfo();

  @Source("marker-highest-max.svg")
  @MimeType("image/svg+xml")
  DataResource markerHighestMax();

  @Source("marker-highest-max-increase.svg")
  @MimeType("image/svg+xml")
  DataResource markerHighestMaxIncrease();

  @Source("marker-max-cumulation.svg")
  @MimeType("image/svg+xml")
  DataResource markerMaxCumulation();

  @Source("marker-max-decrease-max-total-with-effect.svg")
  @MimeType("image/svg+xml")
  DataResource markerMaxDecreaseMaxTotalWithEffect();

  @Source("marker-max-decrease.svg")
  @MimeType("image/svg+xml")
  DataResource markerMaxDecrease();

  @Source("marker-max-increase-max-total-with-effect.svg")
  @MimeType("image/svg+xml")
  DataResource markerMaxIncreaseMaxTotalWithEffect();

  @Source("marker-max-increase.svg")
  @MimeType("image/svg+xml")
  DataResource markerMaxIncrease();

  @Source("marker-max-situation-max-cumulation.svg")
  @MimeType("image/svg+xml")
  DataResource markerMaxSituationMaxCumulation();

  @Source("marker-max-situation.svg")
  @MimeType("image/svg+xml")
  DataResource markerMaxSituation();

  @Source("marker-max-total-max-cumulation.svg")
  @MimeType("image/svg+xml")
  DataResource markerMaxTotalMaxCumulation();

  @Source("marker-max-total-max-situation-max-cumulation.svg")
  @MimeType("image/svg+xml")
  DataResource markerMaxTotalMaxSituationMaxCumulation();

  @Source("marker-max-total-max-situation.svg")
  @MimeType("image/svg+xml")
  DataResource markerMaxTotalMaxSituation();

  @Source("marker-max-total.svg")
  @MimeType("image/svg+xml")
  DataResource markerMaxTotal();

  @Source("marker-max-total-with-effect.svg")
  @MimeType("image/svg+xml")
  DataResource markerMaxTotalWithEffect();

}
