/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.pages;

import java.util.List;
import java.util.stream.Collectors;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.DragEnterEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.ResettableEventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.activity.DelegableActivity;
import nl.aerius.wui.command.PlaceChangeCommand;
import nl.aerius.wui.place.Place;
import nl.aerius.wui.vue.activity.AbstractVueActivity;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.activity.DelegatedActivityManager;
import nl.overheid.aerius.wui.application.command.UserImportRequestedCommand;
import nl.overheid.aerius.wui.application.components.nav.ButtonItem;
import nl.overheid.aerius.wui.application.components.nav.NavigationItem;
import nl.overheid.aerius.wui.application.components.nav.selector.SelectorItem;
import nl.overheid.aerius.wui.application.components.nav.selector.SelectorItemOption;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.LayerPanelContext;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.event.ChangeThemeEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.ApplicationPlaceController;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourceListPlace;
import nl.overheid.aerius.wui.application.place.ExportPlace;
import nl.overheid.aerius.wui.application.place.LanguagePlace;
import nl.overheid.aerius.wui.application.place.MainThemePlace;
import nl.overheid.aerius.wui.application.place.ResultsPlace;
import nl.overheid.aerius.wui.application.resources.util.ThemeUtil;
import nl.overheid.aerius.wui.application.ui.main.selectors.SituationMenuComponentFactory;
import nl.overheid.aerius.wui.application.ui.main.selectors.SubstanceMenuComponentFactory;
import nl.overheid.aerius.wui.application.ui.main.selectors.ThemeMenuComponentFactory;

/**
 * Base class for Delegable Theme activity classes. Override methods if specific
 * implementations are needed.
 */
public class ThemeActivity<T extends DelegatedActivityManager<ThemeView>> extends AbstractVueActivity<ThemePresenter, ThemeView, ThemeViewFactory>
    implements ThemePresenter, DelegableActivity {

  private final ThemeActivityEventBinder EVENT_BINDER = GWT.create(ThemeActivityEventBinder.class);

  interface ThemeActivityEventBinder extends EventBinder<ThemeActivity> {}

  @Inject ApplicationPlaceController placeController;

  @Inject LayerPanelContext layerPanelContext;
  @Inject ApplicationContext applicationContext;
  @Inject CalculationContext calculationContext;
  @Inject ScenarioContext scenarioContext;
  @Inject NavigationContext navigationContext;

  private final T delegator;

  private HandlerRegistration dragRegister;

  public ThemeActivity(final T delegator) {
    super(ThemeViewFactory.get());

    this.delegator = delegator;
  }

  @Override
  public void onStart() {
    dragRegister = RootPanel.get().addDomHandler(event -> eventBus.fireEvent(new UserImportRequestedCommand()), DragEnterEvent.getType());
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    setEventBus(eventBus, this, EVENT_BINDER);
  }

  @Override
  public void onStop() {
    dragRegister.removeHandler();
    delegator.onStop();
  }

  @Override
  public ThemePresenter getPresenter() {
    return this;
  }

  @Override
  public boolean delegate(final ResettableEventBus eventBus, final PlaceChangeCommand c) {
    final boolean delegated = delegator.delegate(eventBus, c.getValue(), c::setRedirect);

    return delegated;
  }

  @Override
  public void setView(final ThemeView view) {
    delegator.setView(view);
  }

  @EventHandler
  public void onChangeThemeEvent(final ChangeThemeEvent e) {
    navigationContext.reset();

    navigationContext.addSelectorItem(new SelectorItem(M.messages().navigationThemeSelection(),
        ThemeMenuComponentFactory.get().getComponentTagName(), () -> formatThemes(), v -> findSelectedTheme(v),
        opts -> opts.size() > 1));

    navigationContext.addSelectorItem(new SelectorItem(M.messages().navigationSubstanceSelection(),
        SubstanceMenuComponentFactory.get().getComponentTagName(), () -> formatSubstances(), v -> findSelectedSubstance(v),
        opts -> opts.size() > 1));

    navigationContext.addSelectorItem(new SelectorItem(M.messages().navigationSituationSelection(),
        SituationMenuComponentFactory.get().getComponentTagName(), () -> formatSituations(), v -> findSelectedSituation(v)));

    navigationContext.addNavigationItem(NavigationItem.createPlaceItem(M.messages().menuSources(), "icon-menu-input",
        placeController, new EmissionSourceListPlace(e.getValue())));

    navigationContext.addNavigationItem(NavigationItem.createPlaceItem(M.messages().menuCalculationPoints(), "icon-menu-calculation-points",
        placeController, new CalculationPointsPlace(e.getValue())));

    navigationContext.addNavigationItem(NavigationItem.createPlaceItem(M.messages().menuCalculate(), "icon-menu-calculate",
        placeController, new CalculatePlace(e.getValue())));

    navigationContext.addNavigationItem(NavigationItem.createPlaceItem(M.messages().menuResults(), "icon-menu-results",
        placeController, new ResultsPlace(e.getValue())));

    navigationContext.addNavigationItem(NavigationItem.createPlaceItem(M.messages().menuExport(), "icon-menu-export",
        placeController, new ExportPlace(e.getValue())));

    // Disabled settings and help buttons until functionality available.
    // navigationContext.addButtonItem(new ButtonItem("icon-menu-settings", () ->
    // placeController.goTo(new PreferencesPlace(e.getValue()))));
    // navigationContext.addButtonItem(new ButtonItem("icon-menu-help", () -> {}));
    navigationContext.addButtonItem(new ButtonItem("icon-menu-manual", () -> openManual()));
    navigationContext.addButtonItem(new ButtonItem("icon-menu-language", () -> placeController.goTo(new LanguagePlace(e.getValue()))));
  }

  private void openManual() {
    Window.open(applicationContext.getConfiguration().getSettings().getSetting(SharedConstantsEnum.MANUAL_URL), "_blank", "");
  }

  private List<SelectorItemOption> formatThemes() {
    return applicationContext.getConfiguration().getContexts().keySet().stream()
        .map(v -> optionFromTheme(v))
        .collect(Collectors.toList());
  }

  private List<SelectorItemOption> formatSubstances() {
    return applicationContext.getActiveThemeConfiguration()
        .getEmissionResultKeys().stream()
        .map(v -> optionFromSubstance(v.getSubstance()))
        .collect(Collectors.toList());
  }

  private List<SelectorItemOption> formatSituations() {
    return scenarioContext.getSituations().stream()
        .map(v -> optionFromSituation(v))
        .collect(Collectors.toList());
  }

  private SelectorItemOption findSelectedSituation(final List<SelectorItemOption> v) {
    return v.stream()
        .filter(opt -> opt.getValue().equals(scenarioContext.getActiveSituationCode()))
        .findFirst().orElse(null);
  }

  private SelectorItemOption findSelectedTheme(final List<SelectorItemOption> v) {
    return v.stream()
        .filter(opt -> opt.getValue().equals(applicationContext.getActiveTheme()))
        .findFirst().orElse(null);
  }

  private SelectorItemOption findSelectedSubstance(final List<SelectorItemOption> v) {
    return v.stream()
        .findFirst().orElse(null);
  }

  private SelectorItemOption optionFromTheme(final Theme theme) {
    return new SelectorItemOption(M.messages().themeName(theme), ThemeUtil.getIconClass(theme), theme);
  }

  private SelectorItemOption optionFromSubstance(final Substance substance) {
    return new SelectorItemOption(substance.getName(), substance);
  }

  private SelectorItemOption optionFromSituation(final SituationContext situation) {
    if (situation == null) {
      return null;
    }

    return new SelectorItemOption(M.messages().navigationSituationShortLabel(
        situation.getSituationCode()),
        situation.getName() + " - " + M.messages().situationType(situation.getType()),
        null,
        situation.getSituationCode());
  }

  @Override
  public boolean isDelegable(final Place place) {
    return place instanceof MainThemePlace;
  }
}
