/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.place.air;

import nl.aerius.wui.place.PlaceTokenizer;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.AirQualityPlace.Tokenizer;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointListPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.LayerPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.MeasurePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.PreferencePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.ResultPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.ScenarioImportPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.ScenarioPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceImportPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceListPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourcePlace;

public class AirQualityTokenizers {
  private static final String SCENARIOS = "scenario";
  private static final String SOURCES = "sources";
  private static final String CALCULATION_POINTS = "calcpoints";
  private static final String MEASURES = "measures";
  private static final String PREFERENCES = "preferences";
  private static final String RESULTS = "results";
  private static final String LAYERS = "layers";


  public static final PlaceTokenizer<ScenarioPlace> SCENARIO =
      Tokenizer.createAir(() -> new ScenarioPlace(), SCENARIOS);
  public static final PlaceTokenizer<ScenarioImportPlace> SCENARIO_IMPORT =
      Tokenizer.createAir(() -> new ScenarioImportPlace(), SCENARIOS, "start");

  public static final PlaceTokenizer<SourcePlace> SOURCE =
      Tokenizer.createAir(() -> new SourcePlace(), SOURCES);

  public static final PlaceTokenizer<SourceImportPlace> SOURCE_IMPORT =
      Tokenizer.createAir(() -> new SourceImportPlace(), SOURCES, "start");
  public static final PlaceTokenizer<SourceCreatePlace> SOURCE_CREATE =
      Tokenizer.createAir(() -> new SourceCreatePlace(), SOURCES, "create");
  public static final PlaceTokenizer<SourceListPlace> SOURCE_LIST =
      Tokenizer.createAir(() -> new SourceListPlace(), SOURCES, "list");

  public static final PlaceTokenizer<CalculationPointPlace> CALCULATION_POINT =
      Tokenizer.createAir(() -> new CalculationPointPlace(), CALCULATION_POINTS);
  public static final PlaceTokenizer<CalculationPointCreatePlace> RECEPTOR_CREATE =
      Tokenizer.createAir(() -> new CalculationPointCreatePlace(), CALCULATION_POINTS, "create");
  public static final PlaceTokenizer<CalculationPointListPlace> RECEPTOR_LIST =
      Tokenizer.createAir(() -> new CalculationPointListPlace(), CALCULATION_POINTS, "list");

  public static final PlaceTokenizer<MeasurePlace> MEASURE =
      Tokenizer.createAir(() -> new MeasurePlace(), MEASURES);
  public static final PlaceTokenizer<PreferencePlace> PREFERENCE =
      Tokenizer.createAir(() -> new PreferencePlace(), PREFERENCES);
  public static final PlaceTokenizer<ResultPlace> RESULT =
      Tokenizer.createAir(() -> new ResultPlace(), RESULTS);
  public static final PlaceTokenizer<LayerPlace> LAYER =
      Tokenizer.createAir(() -> new LayerPlace(), LAYERS);

  public static final PlaceTokenizer<?>[] TOKENIZERS = new PlaceTokenizer[] {
  SCENARIO, SCENARIO_IMPORT,

  SOURCE, SOURCE_IMPORT, SOURCE_CREATE, SOURCE_LIST,

  CALCULATION_POINT, RECEPTOR_CREATE, RECEPTOR_LIST,

  MEASURE,
  PREFERENCE,
  RESULT,
  LAYER,
  };
}
