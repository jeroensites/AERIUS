/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.base.Js;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.command.building.BuildingAddCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDeleteCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDeselectFeatureCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDuplicateSelectedCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingPeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingSelectFeatureCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingUnpeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationDeleteCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.BuildingListContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.event.building.BuildingListChangeEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.building.BuildingToggleSelectFeatureCommand;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.building.DeleteAllBuildingsCommand;

public class BuildingListDaemon extends BasicEventComponent {
  private static final BuildingEditorDaemonEventBinder EVENT_BINDER = GWT.create(BuildingEditorDaemonEventBinder.class);

  interface BuildingEditorDaemonEventBinder extends EventBinder<BuildingListDaemon> {}

  @Inject PlaceController placeController;

  @Inject BuildingListContext context;
  @Inject ScenarioContext scenarioContext;
  @Inject ApplicationContext applicationContext;

  @EventHandler
  public void onBuildingEditSelectedCommand(final BuildingEditSelectedCommand c) {
    if (!context.hasSingleSelection()) {
      return;
    }

    eventBus.fireEvent(new BuildingEditCommand(context.getSingleSelection()));
  }

  @EventHandler
  public void onSituationSwitchEvent(final SituationSwitchEvent e) {
    context.reset();
  }

  @EventHandler
  public void onBuildingDuplicateSelectedCommand(final BuildingDuplicateSelectedCommand c) {
    final List<BuildingFeature> newSelection = new ArrayList<>();

    context.getSelections().forEach(building -> {
      final BuildingFeature copy = FeatureUtil.cloneWithoutIds(building);
      copy.setLabel(FeatureUtil.smartIncrementLabel(copy.getLabel()));
      eventBus.fireEvent(new BuildingAddCommand(copy));
      newSelection.add(copy);
    });

    // Delay because of what is described in EmissionSourceEditorDaemon:120
    SchedulerUtil.delay(() -> {
      context.reset();
      newSelection.forEach(v -> context.addSelection(v));
    });
  }

  @EventHandler
  public void onBuildingDeleteSelectedCommand(final BuildingDeleteSelectedCommand c) {
    if (!context.hasSelection()) {
      return;
    }

    final String confirm = context.getSelections().stream()
        .map(building -> {
          // Do a comprehensive search of all sources, to see if this building is linked
          final List<EmissionSourceFeature> sourceMatches = ScenarioContext.findBuildingLinkedSources(scenarioContext,
              scenarioContext.getActiveSituationCode(), building.getGmlId());

          if (sourceMatches.isEmpty()) {
            return null;
          } else if (sourceMatches.size() == 1) {
            final EmissionSourceFeature src = sourceMatches.get(0);
            return M.messages().buildingRemoveConfirm(building.getId(), building.getLabel(), src.getId(), src.getLabel());
          } else {
            return M.messages().buildingRemoveMultipleConfirm(building.getId(), building.getLabel(), sourceMatches.size());
          }
        })
        .filter(v -> v != null)
        .collect(Collectors.joining("\n\n"));

    if (confirm.isEmpty() || Window.confirm(confirm)) {
      context.getSelections().forEach(v -> eventBus.fireEvent(new BuildingDeleteCommand(v)));
      context.reset();
    }
  }

  @EventHandler
  public void onBuildingToggleSelectFeatureCommand(final BuildingToggleSelectFeatureCommand c) {
    final BuildingFeature building = c.getValue();

    if (context.isSelected(building)) {
      eventBus.fireEvent(new BuildingDeselectFeatureCommand(building));
    } else {
      eventBus.fireEvent(new BuildingSelectFeatureCommand(building, c.isMulti()));
    }
  }

  @EventHandler
  public void onBuildingDeselectFeatureCommand(final BuildingDeselectFeatureCommand c) {
    context.removeSelection(c.getValue());
    if (context.hasSelection()) {
      context.reset();
    }
  }

  @EventHandler
  public void onBuildingSelectFeatureCommand(final BuildingSelectFeatureCommand c) {
    final BuildingFeature building = c.getValue();

    if (c.isMulti()) {
      context.addSelection(building);
    } else {
      context.setSingleSelection(building);
    }
  }

  @EventHandler
  public void onBuildingPeekFeatureCommand(final BuildingPeekFeatureCommand c) {
    context.setPeekBuilding(c.getValue());
  }

  @EventHandler
  public void onBuildingUnpeekFeatureCommand(final BuildingUnpeekFeatureCommand c) {
    context.clearPeekBuilding();
  }

  @EventHandler
  public void onDeleteAllBuildingsCommand(final DeleteAllBuildingsCommand c) {
    final SituationContext situation = scenarioContext.getActiveSituation();
    final String confirm = situation.getBuildings().stream()
        .map(building -> {
          // Do a comprehensive search of all sources, to see if this building is linked
          final List<EmissionSourceFeature> sourceMatches = ScenarioContext.findBuildingLinkedSources(scenarioContext,
              scenarioContext.getActiveSituationCode(), building.getGmlId());

          if (sourceMatches.isEmpty()) {
            return null;
          } else if (sourceMatches.size() == 1) {
            final EmissionSourceFeature src = sourceMatches.get(0);
            return M.messages().buildingRemoveConfirm(building.getId(), building.getLabel(), src.getId(), src.getLabel());
          } else {
            return M.messages().buildingRemoveMultipleConfirm(building.getId(), building.getLabel(), sourceMatches.size());
          }
        })
        .filter(v -> v != null)
        .collect(Collectors.joining("\n\n"));

    if (confirm.isEmpty() || Window.confirm(confirm)) {
      final List<BuildingFeature> features = situation.getOriginalBuildings();
      final String situationCode = situation.getSituationCode();
      features.forEach(building -> {
        ScenarioContext.findBuildingLinkedSources(scenarioContext, situationCode, building.getGmlId())
            .forEach(v -> {
              ((OPSCharacteristics) Js.uncheckedCast(v.getCharacteristics())).setBuildingId(null);
            });
      });

      features.clear();
      context.reset();
      eventBus.fireEvent(new BuildingListChangeEvent(situation.getBuildings(), situation.getSituationCode()));
    }
  }

  @EventHandler
  public void onSituationDeleteCommand(final SituationDeleteCommand c) {
    if (c.getValue().getBuildings().contains(context.getSingleSelection())) {
      context.reset();
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
