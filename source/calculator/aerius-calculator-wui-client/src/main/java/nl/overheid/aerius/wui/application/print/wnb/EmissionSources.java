/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.wnb;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.print.wnb.source.EmissionSourceOPSDetails;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(
  components = EmissionSourceOPSDetails.class
)
public class EmissionSources extends BasicVueComponent {
  public static final String ID = "emission-sources";

  @Inject @Data ScenarioContext scenarioContext;

  @Computed
  public List<SituationContext> getSituations() {
    return scenarioContext.getSituations();
  }

  @Computed
  public boolean isRender() {
    return getSituations().stream().anyMatch(situationContext -> !getEmissionSources(situationContext).isEmpty());
  }

  @JsMethod
  public boolean render(final SituationContext situationContext) {
    return !getEmissionSources(situationContext).isEmpty();
  }

  @JsMethod
  public List<EmissionSourceFeature> getEmissionSources(final SituationContext situationContext) {
    return situationContext.getSources()
      .stream().filter(emissionSourceFeature -> isNotRoadSource(emissionSourceFeature))
      .collect(Collectors.toList());
  }

  private boolean isNotRoadSource(final EmissionSourceFeature emissionSourceFeature) {
    return emissionSourceFeature.getEmissionSourceType() != EmissionSourceType.SRM2_ROAD
      && emissionSourceFeature.getEmissionSourceType() != EmissionSourceType.SRM1_ROAD;
  }
}
