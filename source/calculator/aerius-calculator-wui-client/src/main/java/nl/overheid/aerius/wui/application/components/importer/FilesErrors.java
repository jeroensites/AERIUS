/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer;

import java.util.List;
import java.util.stream.Collectors;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.js.exception.AeriusJsExceptionMessage;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-file-errors")
public class FilesErrors extends BasicVueComponent implements IsVueComponent {
  @Prop @JsProperty List<FileUploadStatus> files;

  @Prop @JsProperty List<AeriusJsExceptionMessage> cohesionErrors;

  @Computed
  public List<FileUploadStatus> getFilesWithErrors() {
    return files.stream()
        .filter(v -> !v.getErrors().isEmpty())
        .collect(Collectors.toList());
  }

  @Computed("hasCohesionErrors")
  public boolean hasCohesionErrors() {
    return !cohesionErrors.isEmpty();
  }

  @Computed("hasErrorOverflow")
  public boolean hasErrorOverflow() {
    return cohesionErrors.size() + files.stream()
    .flatMap(v -> v.getErrors().stream())
    .count() >= RequestMappings.MAX_LIST_SIZE_SANITY;
  }
}
