/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.place.air;

import java.util.function.Supplier;

import nl.aerius.wui.place.PlaceTokenizer;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.place.MainThemePlace;
import nl.overheid.aerius.wui.application.place.PlaceVisitor;

public final class AirQualityPlaces {
  private AirQualityPlaces() {}

  public static class AirQualityPlace extends MainThemePlace {
    public static class Tokenizer<T extends AirQualityPlace> extends MainThemePlace.TokenizerOld<T> {
      public Tokenizer(final Supplier<T> provider, final String... postfix) {
        super(provider, Theme.RBL, postfix);
      }

      public static <P extends AirQualityPlace> Tokenizer<P> createAir(final Supplier<P> supplier, final String... postfix) {
        return new Tokenizer<P>(supplier, postfix);
      }
    }

    public <P extends AirQualityPlace> AirQualityPlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }

    @Override
    public <T> T visit(final PlaceVisitor<T> visitor) {
      return null;
    }
  }

  public static class ScenarioPlace extends AirQualityPlace {
    public ScenarioPlace() {
      this(AirQualityTokenizers.SCENARIO);
    }

    public <P extends ScenarioPlace> ScenarioPlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }

  public static class ScenarioImportPlace extends ScenarioPlace {
    public ScenarioImportPlace() {
      super(AirQualityTokenizers.SCENARIO_IMPORT);
    }
  }

  public static class SourcePlace extends AirQualityPlace {
    public SourcePlace() {
      this(AirQualityTokenizers.SOURCE);
    }

    public <P extends SourcePlace> SourcePlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }

  public static class SourceCreatePlace extends SourcePlace {
    public SourceCreatePlace() {
      super(AirQualityTokenizers.SOURCE_CREATE);
    }
  }

  public static class SourceImportPlace extends SourcePlace {
    public SourceImportPlace() {
      super(AirQualityTokenizers.SOURCE_IMPORT);
    }
  }

  public static class SourceListPlace extends SourcePlace {
    public SourceListPlace() {
      super(AirQualityTokenizers.SOURCE_LIST);
    }
  }

  public static class CalculationPointPlace extends AirQualityPlace {
    public CalculationPointPlace() {
      this(AirQualityTokenizers.CALCULATION_POINT);
    }

    public <P extends CalculationPointPlace> CalculationPointPlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }

  public static class CalculationPointCreatePlace extends CalculationPointPlace {
    public CalculationPointCreatePlace() {
      super(AirQualityTokenizers.RECEPTOR_CREATE);
    }
  }

  public static class CalculationPointListPlace extends CalculationPointPlace {
    public CalculationPointListPlace() {
      super(AirQualityTokenizers.RECEPTOR_LIST);
    }
  }

  public static class MeasurePlace extends AirQualityPlace {
    public MeasurePlace() {
      this(AirQualityTokenizers.MEASURE);
    }

    public <P extends MeasurePlace> MeasurePlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }

  public static class PreferencePlace extends AirQualityPlace {
    public PreferencePlace() {
      this(AirQualityTokenizers.PREFERENCE);
    }

    public <P extends PreferencePlace> PreferencePlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }

  public static class ResultPlace extends AirQualityPlace {
    public ResultPlace() {
      this(AirQualityTokenizers.RESULT);
    }

    public <P extends ResultPlace> ResultPlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }

  public static class LayerPlace extends AirQualityPlace {
    public LayerPlace() {
      this(AirQualityTokenizers.LAYER);
    }

    public <P extends LayerPlace> LayerPlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }
}
