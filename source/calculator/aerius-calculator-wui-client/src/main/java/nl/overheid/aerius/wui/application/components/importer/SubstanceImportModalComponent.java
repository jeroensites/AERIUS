/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.binder.EventBinder;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.components.importer.ApplicationImportModalComponent.ImportType;
import nl.overheid.aerius.wui.application.components.modal.DialogModalComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.ui.main.VerticalCollapse;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-brn-import-modal", components = {
    DialogModalComponent.class,
    VerticalCollapse.class,
    FilesListComponent.class
})
public class SubstanceImportModalComponent extends BasicVueComponent implements IsVueComponent {

  interface SubstanceImportModalEventBinder extends EventBinder<SubstanceImportModalComponent> {}

  @Inject ApplicationContext appContext;

  @JsProperty @Prop List<FileUploadStatus> brnFiles;

  @Data boolean canSubmit = false;
  @Data ImportType importType;

  @Watch(value = "brnFiles", isDeep = true)
  public void watchBrnFiles(final List<FileUploadStatus> files) {
    canSubmit = !files.isEmpty() && files.stream().allMatch(file -> file.getSubstance() != null);
  }

  @JsMethod
  public List<Substance> getSubstances() {
    return appContext.getActiveThemeConfiguration().getImportSubstances();
  }

  @JsMethod
  public void onSubstanceSet(final FileUploadStatus file, final Substance substance) {
    brnFiles.stream()
            .filter(brnFile -> brnFile.getFile().name.equals(file.getFile().name))
            .findFirst()
            .ifPresent(optBrnFile -> optBrnFile.setSubstance(substance));
  }

  @JsMethod
  public void removeFile(final FileUploadStatus file) {
    brnFiles.remove(file);
  }

  @JsMethod
  public void modalCancel() {
    confirmClose();
  }

  public void confirmClose() {
    if (!brnFiles.isEmpty() || Window.confirm(i18n.importMayCloseConfirm())) {
      brnFiles.clear();
    }
  }

  @JsMethod
  public void modalSubmit() {
    if (canSubmit) {
      vue().$emit("importBrnFiles", this.brnFiles);
      brnFiles.clear();
    }
  }

}
