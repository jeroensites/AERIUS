/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca;

import com.google.inject.Inject;

import nl.overheid.aerius.wui.application.place.BuildingPlace;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourceListPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;
import nl.overheid.aerius.wui.application.place.ExportPlace;
import nl.overheid.aerius.wui.application.place.LanguagePlace;
import nl.overheid.aerius.wui.application.place.PlaceVisitor;
import nl.overheid.aerius.wui.application.place.PreferencesPlace;
import nl.overheid.aerius.wui.application.place.ResultsPlace;
import nl.overheid.aerius.wui.application.place.ThemeLaunchPlace;
import nl.overheid.aerius.wui.application.ui.pages.ThemeDelegatedActivity;

public class NcaThemeActivityHelperFactory implements PlaceVisitor<ThemeDelegatedActivity> {
  private final NcaDelegatedActivityFactory activityFactory;

  @Inject
  public NcaThemeActivityHelperFactory(final NcaDelegatedActivityFactory activityFactory) {
    this.activityFactory = activityFactory;
  }

  @Override
  public ThemeDelegatedActivity apply(final EmissionSourceListPlace place) {
    return activityFactory.create(place);
  }

  @Override
  public ThemeDelegatedActivity apply(final EmissionSourcePlace place) {
    return activityFactory.create(place);
  }

  @Override
  public ThemeDelegatedActivity apply(final PreferencesPlace place) {
    return activityFactory.create(place);
  }

  @Override
  public ThemeDelegatedActivity apply(final ResultsPlace place) {
    return activityFactory.create(place);
  }

  @Override
  public ThemeDelegatedActivity apply(final ThemeLaunchPlace place) {
    return activityFactory.create(place);
  }

  @Override
  public ThemeDelegatedActivity apply(final ExportPlace place) {
    return activityFactory.create(place);
  }

  @Override
  public ThemeDelegatedActivity apply(final BuildingPlace place) {
    return activityFactory.create(place);
  }

  @Override
  public ThemeDelegatedActivity apply(final CalculatePlace place) {
    return activityFactory.create(place);
  }

  @Override
  public ThemeDelegatedActivity apply(final LanguagePlace place) {
    return activityFactory.create(place);
  }

  @Override
  public ThemeDelegatedActivity apply(final CalculationPointsPlace place) {
    return activityFactory.create(place);
  }
}
