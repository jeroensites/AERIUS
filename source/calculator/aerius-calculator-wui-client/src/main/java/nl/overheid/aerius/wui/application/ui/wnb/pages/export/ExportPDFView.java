/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.wnb.pages.export;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Ref;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.ui.wnb.pages.export.metadata.ExportMetaDataView;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "export-pdf", components = {
    SimplifiedListBoxComponent.class,
    LabeledInputComponent.class,
    CheckBoxComponent.class,
    ExportMetaDataView.class
})
public class ExportPDFView extends BasicVueComponent {

  @Inject ScenarioContext scenarioContext;

  @Ref ExportMetaDataView exportMetaData;

  @Computed
  public ScenarioResultType[] getExportOptions() {
    return new ScenarioResultType[] { ScenarioResultType.PROJECT_CALCULATION };
  }

  @Computed
  public List<SituationContext> getProposedSituations() {
    return scenarioContext.getSituations().stream()
        .filter(situationContext -> situationContext.getType().equals(SituationType.PROPOSED))
        .collect(Collectors.toList());
  }

  @JsMethod
  public void touchAll() {
    exportMetaData.touchAll();
  }

  @JsMethod
  public boolean isError() {
    return getProposedSituations().isEmpty() || exportMetaData.isError();
  }
}
