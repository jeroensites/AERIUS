/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

public interface PdfMessages {

  String pdfProjectCalculationCoverPageTocOverview();
  String pdfProjectCalculationCoverPageTocSituationsSummary();
  String pdfProjectCalculationCoverPageTocResults();
  String pdfProjectCalculationCoverPageTocEmissionSourceDetails();
  String pdfProjectCalculationCoverPageTitle();
  String pdfProjectCalculationCoverPageDescription();
  String pdfProjectCalculationCoverPageNote();

  String pdfOverviewContactDetails();
  String pdfOverviewContactDetailsCorporation();
  String pdfOverviewContactDetailsLocation();
  String pdfOverviewActivity();
  String pdfOverviewActivityTitle();
  String pdfOverviewActivityDescription();
  String pdfOverviewCalculation();
  String pdfOverviewCalculationReference();
  String pdfOverviewCalculationDate();
  String pdfOverviewCalculationConfiguration();
  String pdfOverviewCalculationConfigurationWnb();
  String pdfOverviewCalculationConfigurationWnbPlus();
  String pdfOverviewTotalEmission();
  String pdfOverviewTotalEmissionCalculationYear();
  String pdfOverviewTotalEmissionEmissionNH3();
  String pdfOverviewTotalEmissionEmissionNOx();
  String pdfOverviewResults();
  String pdfOverviewResultsHighestDeposition();
  String pdfOverviewResultsHexagon();
  String pdfOverviewResultsArea();
  String pdfOverviewResultsLargestIncrease();
  String pdfOverviewResultsLargestDecrease();
  String pdfOverviewResultsAreaWithIncrease();
  String pdfOverviewResultsAreaWithDecrease();
  String pdfOverviewNetting();
  String pdfOverviewNettingFactor();

  String pdfSituationRekenjaarLabel();
  String pdfSituationOverviewEmissionSources();
  String pdfSituationOverviewEmissionNH3Header();
  String pdfSituationOverviewEmissionNOxHeader();
  String pdfSituationOverviewBuildings();
  String pdfSituationOverviewBuildingsEnvelopeHeader();
  String pdfSituationOverviewBuildingsEnvelope(String length, String width, String height, String orientation);
  String pdfSituationOverviewBuildingsEnvelopeCorrected(String length, String width, String height);

  String pdfResultSummaryMapIntro();
  String pdfResultSummaryMapOutro();

  String pdfResultSummaryTableIntro(String situation);
  String pdfResultSummaryTableTotal();
  String pdfResultSummaryTablePerArea();
  String pdfResultSummaryTablePerCalculationPoint();
  String pdfResultSummaryTableOmittedAreaTitle();

  String pdfEmissionSourceDetailsLodgingTableDescription();
  String pdfEmissionSourceDetailsFarmLandTableDescription();
  String pdfEmissionSourceDetailsShippingTableDescription();
  String pdfEmissionSourceDetailsOffroadTableDescription();

  String pdfDisclaimer();
  String pdfDisclaimerLabel();
  String pdfCalculationBaseLabel();
  String pdfCalculationBase();
  String pdfCalculationBaseAeriusVersion();
  String pdfCalculationBaseDatabaseVersion();
  String pdfCalculationBaseMoreInfo();
}
