/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.building;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import ol.geom.Geometry;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.geo.util.OrientedEnvelope;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.main.VerticalCollapse;

@Component(components = {
    VerticalCollapse.class,
    BuildingPreviewComponent.class,
    ValidationBehaviour.class,
})
public class BuildingEnvelopeComponent extends ErrorWarningValidator implements IsVueComponent {
  private static final double SCOPE_BUILDING_RATIO_MINIMUM = 0.15;

  @Prop Geometry geometry;
  @Prop double height;

  @Prop boolean forgiveHeightWarning;
  @Prop String backgroundColor;

  @Data OrientedEnvelope envelope;

  @PropDefault("forgiveHeightWarning")
  public boolean forgiveHeightWarningDefault() {
    return false;
  }

  @PropDefault("backgroundColor")
  public String backgroundColorDefault() {
    return "var(--back-color)";
  }

  @Computed("hasEnvelope")
  public boolean hasEnvelope() {
    return envelope != null;
  }

  @Watch(value = "geometry", isImmediate = true)
  public void onGeometryChange(final Geometry neww) {
    if (neww == null) {
      envelope = null;
    } else {
      try {
        envelope = GeoUtil.determineOrientedEnvelope(geometry);
      } catch (final Exception e) {
        envelope = null;
      }
    }
  }

  @JsMethod
  public boolean isInvalidGeometry() {
    if (geometry == null) {
      return false;
    }

    try {
      GeoUtil.determineOrientedEnvelope(geometry);
      return false;
    } catch (final Exception e) {
      return true;
    }
  }

  @Computed("isBuildingLengthCorrected")
  public boolean isBuildingLengthCorrected() {
    return envelope.getLength() > OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM;
  }

  @Computed
  public double getUsedBuildingLength() {
    return Math.min(OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM, envelope.getLength());
  }

  @Computed
  public String getCorrectedBuildingLength() {
    if (!isBuildingLengthCorrected()) {
      return "";
    }

    return i18n.unitM(i18n.decimalNumberFixed(OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM, OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION));
  }

  @Computed("isBuildingWidthCorrected")
  public boolean isBuildingWidthCorrected() {
    return envelope.getWidth() > OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM;
  }

  @Computed
  public double getUsedBuildingWidth() {
    return Math.min(OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM, envelope.getWidth());
  }

  @Computed
  public String getCorrectedBuildingWidth() {
    if (!isBuildingWidthCorrected()) {
      return "";
    }

    return i18n.unitM(i18n.decimalNumberFixed(OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM, OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION));
  }

  @Computed
  public String getCorrectedBuildingHeight() {
    if (!isBuildingHeightCorrected()) {
      return "";
    }

    return i18n.unitM(i18n.decimalNumberFixed(OPSLimits.SCOPE_BUILDING_HEIGHT_MAXIMUM, OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION));
  }

  @Computed
  public String getBuildingLength() {
    return i18n.unitM(i18n.decimalNumberFixed(envelope.getLength(), OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION));
  }

  @Computed
  public String getBuildingWidth() {
    return i18n.unitM(i18n.decimalNumberFixed(envelope.getWidth(), OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION));
  }

  @Computed("isBuildingHeightCorrected")
  public boolean isBuildingHeightCorrected() {
    return height > OPSLimits.SCOPE_BUILDING_HEIGHT_MAXIMUM;
  }

  @Computed
  public String getBuildingHeight() {
    return i18n.unitM(i18n.decimalNumberFixed(height, OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION));
  }

  @Computed
  public String getBuildingOrientation() {
    return M.messages().decimalNumberFixed(envelope.getOrientation(), 0) + M.messages().unitSingularDegrees();
  }

  @Computed("buildingRatio")
  public double getBuildingRatio() {
    return getUsedBuildingWidth() / getUsedBuildingLength();
  }

  @Computed("formattedBuildingRatio")
  public String getFormattedBuildingRatio() {
    return i18n.decimalNumberFixed(getBuildingRatio(), 3);
  }

  @Computed("isBuildingRatioViolated")
  public boolean isBuildingRatioViolated() {
    return envelope != null && getBuildingRatio() < SCOPE_BUILDING_RATIO_MINIMUM;
  }

  @Computed("isBuildingDimensionViolated")
  public boolean isBuildingDimensionViolated() {
    return isBuildingLengthCorrected()
        || isBuildingWidthCorrected()
        || !forgiveHeightWarning && isBuildingHeightCorrected();
  }
}
