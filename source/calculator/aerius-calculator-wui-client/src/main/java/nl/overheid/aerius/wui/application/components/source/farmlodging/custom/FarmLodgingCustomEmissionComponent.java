/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.farmlodging.custom;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.AnimalType;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.custom.FarmLodgingCustomValidators.FarmLodgingCustomValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.farm.CustomFarmLodging;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(customizeOptions = FarmLodgingCustomValidators.class, directives = {
    ValidateDirective.class,
    DebugDirective.class
}, components = {
    LabeledInputComponent.class,
    ValidationBehaviour.class,
})
public class FarmLodgingCustomEmissionComponent extends ErrorWarningValidator implements HasCreated, HasValidators<FarmLodgingCustomValidations> {
  @Prop @JsProperty CustomFarmLodging customFarmLodging;

  @Data String descriptionV;
  @Data String emissionFactorV;
  @Data String numberOfAnimalsV;

  @JsProperty(name = "$v") FarmLodgingCustomValidations validation;

  @Override
  @Computed
  public FarmLodgingCustomValidations getV() {
    return validation;
  }

  @Watch(value = "customFarmLodging", isImmediate = true)
  public void onCustomFarmLodgingChange() {
    descriptionV = String.valueOf(customFarmLodging.getDescription());
    emissionFactorV = String.valueOf(customFarmLodging.getEmissionFactor(Substance.NH3));
    numberOfAnimalsV = String.valueOf(customFarmLodging.getNumberOfAnimals());

    validation.$reset();
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @Computed
  protected String getDescription() {
    return descriptionV;
  }

  @Computed
  protected void setDescription(final String description) {
    customFarmLodging.setDescription(description);
    descriptionV = description;
  }

  @Computed
  protected String getAnimalType() {
    return AnimalType.getByCode(customFarmLodging.getAnimalCode()).getAnimalCode();
  }

  @Computed
  protected void setAnimalType(final String animalType) {
    customFarmLodging.setAnimalCode(AnimalType.getByCode(animalType).getAnimalCode());
  }

  @Computed
  protected String getNumberOfAnimals() {
    return numberOfAnimalsV;
  }

  @Computed
  protected void setNumberOfAnimals(final String numberOfAnimals) {
    ValidationUtil.setSafeIntegerValue(v -> customFarmLodging.setNumberOfAnimals(v), numberOfAnimals, 0);
    this.numberOfAnimalsV = numberOfAnimals;
  }

  @Computed
  protected String getEmissionFactor() {
    return emissionFactorV;
  }

  @Computed
  protected void setEmissionFactor(final String emissionFactor) {
    ValidationUtil.setSafeDoubleValue(ef -> customFarmLodging.setEmissionFactor(Substance.NH3, ef), emissionFactor, 0D);
    this.emissionFactorV = emissionFactor;
  }

  @JsMethod
  protected String emissionFactorConversionError() {
    return i18n.errorDecimal(emissionFactorV);
  }

  @JsMethod
  protected String numberOfAnimalsConversionError() {
    return i18n.errorNumeric(numberOfAnimalsV);
  }

  @Computed("isInvalid")
  public boolean isInvalid() {
    return validation.descriptionV.invalid
        || validation.emissionFactorV.invalid
        || validation.numberOfAnimalsV.invalid;
  }
}
