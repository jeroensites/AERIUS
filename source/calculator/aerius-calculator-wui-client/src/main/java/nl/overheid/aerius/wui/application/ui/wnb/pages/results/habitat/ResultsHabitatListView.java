/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.results.habitat;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSelectionContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsHabitatSummary;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component
public class ResultsHabitatListView extends BasicVueView {
  @Inject @Data ApplicationContext applicationContext;

  @Prop @JsProperty SituationResultsHabitatSummary[] data;

  @Prop ResultStatisticType statisticsType = ResultStatisticType.MAX_CONTRIBUTION;

  @Inject @Data ResultSelectionContext context;

  @JsMethod
  public void rowSelectToggle(final SituationResultsHabitatSummary row) {
    vue().$emit("toggle-habitat-selection", String.valueOf(row.getHabitatType().getId()));
  }

  @JsMethod
  public void rowHighlightActive(final SituationResultsHabitatSummary row) {
    vue().$emit("habitat-highlight-active", String.valueOf(row.getHabitatType().getId()));
  }

  @JsMethod
  public void rowHighlightInactive(final SituationResultsHabitatSummary row) {
    vue().$emit("habitat-highlight-inactive", String.valueOf(row.getHabitatType().getId()));
  }

  @JsMethod
  public boolean isSelected(final SituationResultsHabitatSummary summary) {
    return context.isSelectedHabitatTypeCode(String.valueOf(summary.getHabitatType().getId()));
  }

  @JsMethod
  public String formatHectareValue(final SituationResultsHabitatSummary row) {
    final Double resultValue = getStatistic(row, ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE);
    return MessageFormatter.formatSurfaceToHectare(resultValue);
  }

  @JsMethod
  public String formatDepositionValue(final SituationResultsHabitatSummary row) {
    final Number resultValue = getStatistic(row, statisticsType);
    return MessageFormatter.formatDeposition(resultValue, applicationContext.getActiveThemeConfiguration().getEmissionResultValueDisplaySettings());
  }

  private Double getStatistic(final SituationResultsHabitatSummary row, final ResultStatisticType statisticsType) {
    return row == null || row.getStatistics() == null || statisticsType == null
        ? null
        : row.getStatistics().getValue(statisticsType);
  }
}
