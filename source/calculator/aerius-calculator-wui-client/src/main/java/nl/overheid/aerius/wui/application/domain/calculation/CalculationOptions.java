/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.calculation;

import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.wui.application.util.json.JsonSerializable;

/**
 * Calculation options to pass to the server
 * @see nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions
 */
@JsType
public class CalculationOptions implements JsonSerializable {

  private @JsProperty String calculationType = CalculationType.PERMIT.name();

  public CalculationType getCalculationType() {
    return CalculationType.valueOf(calculationType);
  }

  public void setCalculationType(CalculationType calculationType) {
    this.calculationType = calculationType.name();
  }

}
