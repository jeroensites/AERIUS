/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.main.selectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.application.command.misc.DeactivateSelectorMenuCommand;
import nl.overheid.aerius.wui.application.command.situation.CreateEmptySituationCommand;
import nl.overheid.aerius.wui.application.command.situation.DuplicateSituationCommand;
import nl.overheid.aerius.wui.application.command.situation.GoToSituationCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.nav.selector.SelectorItem;
import nl.overheid.aerius.wui.application.components.nav.selector.SelectorItemOption;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    BasicSelectorMenuComponent.class,
    TooltipComponent.class,
    ButtonIcon.class
})
public class SituationMenuComponent extends BasicVueComponent implements IsVueComponent {
  @Prop EventBus eventBus;
  @Prop SelectorItem item;

  @Inject @Data ScenarioContext scenarioContext;

  @Inject PlaceController placeController;

  @JsMethod
  public void onDuplicateClick(final SelectorItemOption option) {
    eventBus.fireEvent(new DuplicateSituationCommand((String) option.getValue()));
  }

  @JsMethod
  public void onNewSituationClick() {
    eventBus.fireEvent(new CreateEmptySituationCommand());
  }

  @JsMethod
  public void selectOption(final SelectorItemOption option) {
    eventBus.fireEvent(new DeactivateSelectorMenuCommand());
    eventBus.fireEvent(new GoToSituationCommand((String) option.getValue()));
  }

  @Computed
  public SelectorItemOption getSelectedOption() {
    return item.getSelectedOption();
  }
}
