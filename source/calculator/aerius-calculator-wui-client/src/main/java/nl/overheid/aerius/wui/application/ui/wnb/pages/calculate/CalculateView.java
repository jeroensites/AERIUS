/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.calculate;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculateContext;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    LabeledInputComponent.class,
    CollapsiblePanel.class
})
public class CalculateView extends BasicVueComponent implements IsVueComponent {
  @Prop EventBus eventBus;

  @Inject @Data ScenarioContext context;
  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data CalculateContext calculateContext;

  @Computed
  public List<SituationContext> getSituations() {
    return context.getSituations();
  }

  @Computed
  public String getGridColumns() {
    // header, type, year, netting, sources, [n substances]
    return "auto 1fr auto auto auto auto " + getSubstances().stream()
        .map(v -> "auto")
        .collect(Collectors.joining(" "));
  }

  @Computed
  protected List<Substance> getSubstances() {
    return applicationContext.getActiveThemeConfiguration().getSubstances();
  }

  @JsMethod
  public boolean isNetting(final SituationContext situation) {
    return SituationType.NETTING.equals(situation.getType());
  }
}
