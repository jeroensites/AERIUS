/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Feature;
import ol.OLFactory;
import ol.geom.Point;
import ol.layer.Layer;
import ol.layer.VectorLayerOptions;
import ol.source.Vector;
import ol.style.Style;

import nl.overheid.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.ApplicationLimits;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDeselectFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceSelectFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.TemporaryEmissionSourceAddCommand;
import nl.overheid.aerius.wui.application.command.source.TemporaryEmissionSourceClearCommand;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceAddedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceDeletedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceHighlightToggledEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceListChangeEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceModifyGeometryCommand;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceUpdatedEvent;

public abstract class SourceGeometryLayer implements IsLayer<Layer> {
  private final SourceGeometryLayerEventBinder EVENT_BINDER = GWT.create(SourceGeometryLayerEventBinder.class);

  interface SourceGeometryLayerEventBinder extends EventBinder<SourceGeometryLayer> {}

  protected static final Style[] NO_RENDERING = new Style[] {};

  private static final double GEOMETRY_MAX_RESOLUTION = 100.0;

  protected final LayerInfo info;
  private final ol.layer.Vector layer;
  private final Map<String, Vector> sources = new HashMap<>();

  @Inject private EmissionSourceEditorContext emissionSourceEditorContext;
  private String geometryEditingFeatureId = null;

  public SourceGeometryLayer(final LayerInfo info, final EventBus eventBus, final int zIndex) {
    this.info = info;

    final VectorLayerOptions vectorLayerOptions = OLFactory.createOptions();
    vectorLayerOptions.setMaxResolution(GEOMETRY_MAX_RESOLUTION);
    vectorLayerOptions.setStyle((feature, v) -> getStyle(feature, v));

    layer = new ol.layer.Vector(vectorLayerOptions);
    layer.setZIndex(zIndex);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  protected abstract Style[] getStyle(Feature feature, double resolution);

  @EventHandler
  public void onTemporaryEmissionSourceAddCommand(final TemporaryEmissionSourceAddCommand c) {
    final Vector vector = getFeaturesVector(c.getSituationId());

    addFeature(vector, c.getValue());
  }

  @EventHandler
  public void onTemporaryEmissionSourceClearCommand(final TemporaryEmissionSourceClearCommand c) {
    final Vector vector = getFeaturesVector(c.getSituationId());
    final Feature feature = vector.getFeatureById(c.getValue().getId());

    if (feature != null) {
      vector.removeFeature(feature);
    }
  }

  @EventHandler
  public void onEmissionSourceAddedEvent(final EmissionSourceAddedEvent e) {
    final Vector vector = getFeaturesVector(e.getSituationId());

    addFeature(vector, e.getValue());
  }

  @EventHandler
  public void onEmissionSourceUpdatedEvent(final EmissionSourceUpdatedEvent e) {
    final Vector vector = getFeaturesVector(e.getSituationId());

    final Feature feat = getFeatureById(e.getValue(), vector);
    if (feat == null) {
      addFeature(vector, e.getValue());
    } else {
      vector.changed();
    }
  }

  @EventHandler
  public void onEmissionSourceDeletedEvent(final EmissionSourceDeletedEvent e) {
    final Vector vector = getFeaturesVector(e.getSituationId());
    final Feature feature = getFeatureById(e.getValue(), vector);

    if (feature != null) {
      vector.removeFeature(feature);
    }
  }

  @EventHandler
  public void onEmissionSourceListChangeEvent(final EmissionSourceListChangeEvent e) {
    final Vector vector = getFeaturesVector(e.getSituationId());

    vector.clear(true);
    addFeatures(vector, e.getValue());
    layer.setSource(vector);
  }

  @EventHandler
  public void onSituationSwitch(final SituationSwitchEvent e) {
    final Vector vector = getFeaturesVector(e.getValue());

    layer.setSource(vector);
  }

  @EventHandler
  public void onEmissionSourceModifyGeometry(final EmissionSourceModifyGeometryCommand e) {
    if (e.getValue() == null) {
      geometryEditingFeatureId = null;
    } else {
      geometryEditingFeatureId = e.getValue().getId();
    }
    layer.getSource().changed();
  }

  @EventHandler(handles = { EmissionSourceSelectFeatureCommand.class, EmissionSourceDeselectFeatureCommand.class,
      EmissionSourceHighlightToggledEvent.class, EmissionSourceFeatureToggleSelectCommand.class, EmissionSourceUpdatedEvent.class })
  public void refreshLayer() {
    layer.getSource().changed();
  }

  /**
   * Add multiple features in an optimized way
   */
  private void addFeatures(final Vector vector, final List<EmissionSourceFeature> features) {
    final List<EmissionSourceFeature> featuresToAdd = features.stream()
        .filter(v -> !(v.getGeometry() instanceof Point))
        .limit(Math.max(0, ApplicationLimits.SOURCE_LAYER_DRAW_LIMIT - vector.getFeatures().length))
        .collect(Collectors.toList());

    vector.addFeatures(featuresToAdd.toArray(new Feature[featuresToAdd.size()]));
  }

  private void addFeature(final Vector vector, final EmissionSourceFeature feature) {
    if (vector.getFeatures().length > ApplicationLimits.SOURCE_LAYER_DRAW_LIMIT) {
      return;
    }

    vector.addFeature(feature);
  }

  private Vector getFeaturesVector(final String situationCode) {
    Vector vector = sources.get(situationCode);

    if (vector == null) {
      vector = new Vector();
      sources.put(situationCode, vector);
    }
    return vector;
  }

  private Feature getFeatureById(final EmissionSourceFeature feature, final Vector vector) {
    return vector.getFeatureById(feature.getId());
  }

  protected String getGeometryEditingFeatureId() {
    return geometryEditingFeatureId;
  }

  protected String getEditingFeatureId() {
    if (emissionSourceEditorContext.getSource() == null) {
      return null;
    } else {
      return emissionSourceEditorContext.getSource().getId();
    }
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public Optional<LayerInfo> getInfoOptional() {
    return Optional.of(info);
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
