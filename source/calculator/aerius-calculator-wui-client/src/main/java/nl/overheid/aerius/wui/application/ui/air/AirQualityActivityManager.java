/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.air;

import com.google.inject.Inject;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.place.Place;
import nl.overheid.aerius.wui.application.activity.DelegatedActivityManager;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointListPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.LayerPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.ResultPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.ScenarioImportPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.ScenarioPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceImportPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourcePlace;

public class AirQualityActivityManager extends DelegatedActivityManager<AirQualityView> {
  private final AirQualityActivityFactory activityFactory;

  @Inject
  public AirQualityActivityManager(final AirQualityActivityFactory activityFactory) {
    this.activityFactory = activityFactory;
  }

  @Override
  public AirQualitySubActivity getActivity(final Place place) {
    final AirQualityView view = null; // FIXME This needs to be initialized.
    if (place instanceof ScenarioImportPlace) {
      return activityFactory.createScenarioImportActivity(view, (ScenarioImportPlace) place);
    } else if (place instanceof SourceImportPlace) {
      return activityFactory.createSourceStartActivity(view, (SourceImportPlace) place);
    } else if (place instanceof SourceCreatePlace) {
      return activityFactory.createSourceCreateActivity(view, (SourceCreatePlace) place);
    } else if (place instanceof CalculationPointCreatePlace) {
      return activityFactory.createReceptorCreateActivity(view, (CalculationPointCreatePlace) place);
    } else if (place instanceof CalculationPointListPlace) {
      return activityFactory.createReceptorListActivity(view, (CalculationPointListPlace) place);
    } else if (place instanceof ResultPlace) {
      return activityFactory.createResultsActivity(view, (ResultPlace) place);
    } else if (place instanceof LayerPlace) {
      return activityFactory.createLayerActivity(view, (LayerPlace) place);
    } else {
      GWTProd.warn("AirQualityActivityManager", "Could not create sub-activity inside AirQualityActivity: no activity for " + place);
      return null;
    }
  }

  @Override
  protected Place getRedirect(final Place place) {
    Place redirect = null;
    if (place.getClass().equals(ScenarioPlace.class)) {
      redirect = new ScenarioImportPlace();
    } else if (place.getClass().equals(SourcePlace.class)) {
      redirect = new SourceImportPlace();
    } else if (place.getClass().equals(CalculationPointPlace.class)) {
      redirect = new CalculationPointCreatePlace();
    }

    return redirect;
  }
}
