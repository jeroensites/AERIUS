/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.results;

import java.util.Arrays;
import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.vue.VueComponentFactory;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ScenarioResultsView.class,
    SummaryResultsView.class,
})
public class ResultsView extends BasicVueComponent implements IsVueComponent {
  @Prop EventBus eventBus;

  public enum MainTab {
    SUMMARY,
    SCENARIO_RESULTS,
  }

  @Data MainTab selectedTab = MainTab.SCENARIO_RESULTS;

  @Computed
  public List<MainTab> getAvailableTabs() {
    return Arrays.asList(MainTab.values());
  }

  @Computed
  public String getSelectedView() {
    VueComponentFactory<?> tabContent;

    switch (selectedTab) {
    case SUMMARY:
      tabContent = SummaryResultsViewFactory.get();
      break;
    case SCENARIO_RESULTS:
      tabContent = ScenarioResultsViewFactory.get();
      break;
    default:
      return null;
    }

    return tabContent.getComponentTagName();
  }

  @JsMethod
  public boolean isSelected(final MainTab tab) {
    return selectedTab == tab;
  }

  @JsMethod
  public void selectMainTab(final MainTab tab) {
    selectedTab = tab;
  }
}
