/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.nca;

import nl.overheid.aerius.wui.application.place.BuildingPlace;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourceListPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;
import nl.overheid.aerius.wui.application.place.ExportPlace;
import nl.overheid.aerius.wui.application.place.LanguagePlace;
import nl.overheid.aerius.wui.application.place.PreferencesPlace;
import nl.overheid.aerius.wui.application.place.ResultsPlace;
import nl.overheid.aerius.wui.application.place.ThemeLaunchPlace;
import nl.overheid.aerius.wui.application.ui.pages.language.LanguageActivity;
import nl.overheid.aerius.wui.application.ui.wnb.pages.building.BuildingActivity;
import nl.overheid.aerius.wui.application.ui.wnb.pages.calculate.CalculateActivity;
import nl.overheid.aerius.wui.application.ui.wnb.pages.calculationpoints.CalculationPointsActivity;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsource.EmissionSourceActivity;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.ScenarioInputListActivity;
import nl.overheid.aerius.wui.application.ui.wnb.pages.export.ExportActivity;
import nl.overheid.aerius.wui.application.ui.wnb.pages.launch.WnbThemeLaunchActivity;
import nl.overheid.aerius.wui.application.ui.wnb.pages.preferences.PreferencesActivity;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.ResultsActivity;

public interface NcaDelegatedActivityFactory {

  BuildingActivity create(BuildingPlace place);

  CalculateActivity create(CalculatePlace place);

  ScenarioInputListActivity create(EmissionSourceListPlace place);

  EmissionSourceActivity create(EmissionSourcePlace place);

  PreferencesActivity create(PreferencesPlace place);

  ResultsActivity create(ResultsPlace place);

  WnbThemeLaunchActivity create(ThemeLaunchPlace place);

  ExportActivity create(ExportPlace place);

  LanguageActivity create(LanguagePlace place);

  CalculationPointsActivity create(CalculationPointsPlace place);
}
