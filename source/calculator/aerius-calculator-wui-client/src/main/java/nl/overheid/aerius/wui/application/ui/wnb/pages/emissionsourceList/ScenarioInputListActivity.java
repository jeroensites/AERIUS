/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.application.command.building.BuildingDeselectFeatureCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingSelectFeatureCommand;
import nl.overheid.aerius.wui.application.command.situation.SwitchSituationCommand;
import nl.overheid.aerius.wui.application.command.source.DeleteAllEmissionSourceCommand;
import nl.overheid.aerius.wui.application.command.source.DeleteAllRoadNetworkSourcesCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceAddCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDeleteCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDeselectFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDuplicateSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditEvent;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourcePeekFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceSelectFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceUnpeekFeatureCommand;
import nl.overheid.aerius.wui.application.components.map.MapComponentFactory;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceListContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceDeletedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceListChangeEvent;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;
import nl.overheid.aerius.wui.application.ui.main.MainView.FlexView;
import nl.overheid.aerius.wui.application.ui.pages.ThemeDelegatedActivity;
import nl.overheid.aerius.wui.application.ui.pages.ThemeView;

public class ScenarioInputListActivity extends BasicEventComponent implements ThemeDelegatedActivity, ScenarioInputsPresenter {

  private static final EmissionSourceListActivityEventBinder EVENT_BINDER = GWT.create(EmissionSourceListActivityEventBinder.class);

  interface EmissionSourceListActivityEventBinder extends EventBinder<ScenarioInputListActivity> {}

  @Inject PlaceController placeController;
  @Inject ApplicationContext applicationContext;
  @Inject ScenarioContext scenarioContext;
  @Inject EmissionSourceListContext listContext;

  private ThemeView wnbThemeView;

  private HandlerRegistration handlers;

  @Override
  public void onStart(final ThemeView view) {
    this.wnbThemeView = view;
    view.setDelegatedPresenter(this);
    view.setLeftView(ScenarioInputListViewFactory.get(), FlexView.RIGHT);
    view.setMiddleView(ScenarioInputListDetailViewFactory.get());
    view.setRightView(MapComponentFactory.get());
    view.attachMap(() -> view.rightComponent);
  }

  @Override
  public void onStop() {
    handlers.removeHandler();
    listContext.reset();
  }

  @EventHandler
  public void onEmissionSourceDuplicateSelectedCommand(final EmissionSourceDuplicateSelectedCommand c) {
    if (!listContext.hasSelection()) {
      return;
    }

    final EmissionSourceFeature copy = FeatureUtil.cloneWithoutIds(listContext.getSelection());
    copy.setLabel(FeatureUtil.smartIncrementLabel(copy.getLabel()));

    eventBus.fireEvent(new EmissionSourceAddCommand(copy, scenarioContext.getActiveSituation().getSituationCode()));
    selectSource(copy);
  }

  @EventHandler
  public void onEmissionSourceDeleteSelectedCommand(final EmissionSourceDeleteSelectedCommand c) {
    if (!listContext.hasSelection()) {
      return;
    }

    eventBus.fireEvent(new EmissionSourceDeleteCommand(listContext.getSelection(), scenarioContext.getActiveSituation().getSituationCode()));
    listContext.reset();
  }

  @EventHandler
  public void onEmissionSourceSelectFeatureCommand(final EmissionSourceSelectFeatureCommand c) {
    selectSource(c.getValue());
  }

  @EventHandler
  public void onEmissionSourceDeselectFeatureCommand(final EmissionSourceDeselectFeatureCommand c) {
    deselectSource();
  }

  @EventHandler
  public void onEmissionSourceFeatureToggleSelectCommand(final EmissionSourceFeatureToggleSelectCommand c) {
    final EmissionSourceFeature source = c.getValue();

    if (listContext.isSelected(source)) {
      deselectSource();
    } else {
      selectSource(source);
    }
  }

  @EventHandler
  public void onSwitchSituationCommand(final SwitchSituationCommand c) {
    deselectSource();
  }

  @EventHandler
  public void onEmissionSourceDeletedEvent(final EmissionSourceDeletedEvent e) {
    deselectSource();
  }

  @EventHandler
  public void onEmissionSourcePeekFeatureCommand(final EmissionSourcePeekFeatureCommand c) {
    listContext.setPeekSource(c.getValue());
  }

  @EventHandler
  public void onEmissionSourceUnpeekFeatureCommand(final EmissionSourceUnpeekFeatureCommand c) {
    listContext.clearPeekSource();
  }

  @EventHandler
  public void onEmissionSourceEditSelectedCommand(final EmissionSourceEditSelectedCommand c) {
    if (!listContext.hasSelection()) {
      return;
    }

    eventBus.fireEvent(new EmissionSourceEditCommand(listContext.getSelection()));
  }

  @EventHandler
  public void onEmissionSourceEditEvent(final EmissionSourceEditEvent c) {
    final EmissionSourcePlace place = new EmissionSourcePlace(applicationContext.getActiveTheme());
    place.setEmissionSourceId(c.getValue().getId());

    listContext.reset();
    placeController.goTo(place);
  }

  @EventHandler
  public void onDeleteAllRoadNetworkSourcesCommand(final DeleteAllRoadNetworkSourcesCommand c) {
    Optional.ofNullable(scenarioContext.getActiveSituation())
        .map(v -> v.getSources())
        .orElse(new ArrayList<>())
        .stream()
        .filter(v -> RoadEmissionSourceTypes.isRoadSource(v))
        .forEach(source -> {
          eventBus.fireEvent(new EmissionSourceDeleteCommand(source, scenarioContext.getActiveSituation().getSituationCode()));
        });
  }

  @EventHandler
  public void onDeleteAllEmissionSourceCommand(final DeleteAllEmissionSourceCommand c) {
    final SituationContext situation = scenarioContext.getActiveSituation();
    final List<EmissionSourceFeature> features = situation.getOriginalSources();
    features.clear();
    deselectSource();
    eventBus.fireEvent(new EmissionSourceListChangeEvent(situation.getSources(), situation.getSituationCode()));
  }

  @EventHandler
  public void onBuildingSelectedCommand(final BuildingSelectFeatureCommand c) {
    wnbThemeView.setSoftMiddle(false);
  }

  @EventHandler
  public void onBuildingDeselectedCommand(final BuildingDeselectFeatureCommand c) {
    wnbThemeView.setSoftMiddle(true);
  }

  public void selectSource(final EmissionSourceFeature source) {
    wnbThemeView.setSoftMiddle(false);
    listContext.setSelection(source);
  }

  public void deselectSource() {
    wnbThemeView.setSoftMiddle(true);
    listContext.reset();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    handlers = super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
