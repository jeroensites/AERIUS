/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.place;

import java.util.HashMap;
import java.util.Map;

import nl.aerius.wui.place.ApplicationPlace;

public class PrintWnbPlace extends ApplicationPlace {
  private final Map<String, String> tokens = new HashMap<>();

  public PrintWnbPlace() {
    super(ApplicationTokenizers.PRINT_WNB);
  }

  public String getScenarioFileCodes() {
    return tokens.get("scenarioFileCodes");
  }

  public String getJobId() {
    return tokens.get("jobId");
  }

  public String getProposedSituationId() {
    return tokens.get("proposedSituationId");
  }

  public String getReference() {
    return tokens.get("reference");
  }

  public Map<String, String> getTokens() {
    return tokens;
  }
}
