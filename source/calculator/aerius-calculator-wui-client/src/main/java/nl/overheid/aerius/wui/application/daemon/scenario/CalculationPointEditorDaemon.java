/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.command.calculationpoint.AddCalculationPointCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditCancelCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditNewCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditSaveCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.RemoveCalculationPointCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.CalculationPointEditorContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;

@Singleton
public class CalculationPointEditorDaemon extends BasicEventComponent implements Daemon {
  private static final CalculationPointEditorDaemonEventBinder EVENT_BINDER = GWT.create(CalculationPointEditorDaemonEventBinder.class);

  interface CalculationPointEditorDaemonEventBinder extends EventBinder<CalculationPointEditorDaemon> {}

  @Inject PlaceController placeController;

  @Inject CalculationPointEditorContext editorContext;
  @Inject ScenarioContext scenarioContext;
  @Inject ApplicationContext appContext;

  @EventHandler
  public void onCreateCalculationPointCommand(final CalculationPointEditNewCommand c) {
    final CalculationPointFeature calculationPointFeature = new CalculationPointFeature();
    CalculationPointFeature.init(calculationPointFeature);

    final List<CalculationPointFeature> calculationPoints = scenarioContext.getCalculationPoints();
    final String nextId = FeatureUtil.getNextLabel(calculationPoints);
    calculationPointFeature.setLabel(M.messages().calculationPointsDefaultLabel(nextId));

    final ConsecutiveIdUtil.IndexIdPair newId = FeatureUtil.getConsecutiveIdUtil(calculationPoints).generate();
    calculationPointFeature.setId(newId.getId());

    editorContext.setCalculationPoint(calculationPointFeature);
    editorContext.setOriginal(null);
    eventBus.fireEvent(new AddCalculationPointCommand(calculationPointFeature));

    // TODO Separate CalculationPointsActivity
    placeController.goTo(new CalculationPointsPlace(appContext.getActiveTheme()));
  }

  @EventHandler
  public void onEditCalculationPointCommand(final CalculationPointEditCommand c) {
    editorContext.setOriginal(c.getValue());

    final CalculationPointFeature clone = FeatureUtil.clone(c.getValue());
    clone.setGeometry(c.getValue().getGeometry());
    editorContext.setCalculationPoint(clone);

    eventBus.fireEvent(new RemoveCalculationPointCommand(editorContext.getOriginal()));
    eventBus.fireEvent(new AddCalculationPointCommand(editorContext.getCalculationPoint()));
  }

  @EventHandler
  public void onRemoveCalculationPointCommand(final RemoveCalculationPointCommand c) {
    scenarioContext.getCalculationPoints().remove(c.getValue());
  }

  @EventHandler
  public void onCancelEditCalculationPointCommand(final CalculationPointEditCancelCommand c) {
    eventBus.fireEvent(new RemoveCalculationPointCommand(editorContext.getCalculationPoint()));
    if (editorContext.getOriginal() != null) {
      eventBus.fireEvent(new AddCalculationPointCommand(editorContext.getOriginal()));
    }

    editorContext.setOriginal(null);
    editorContext.setCalculationPoint(null);
  }

  @EventHandler
  public void onSaveCalculationPointCommand(final CalculationPointEditSaveCommand c) {
    if (editorContext.getOriginal() != null) {
      editorContext.getCalculationPoint().setId(editorContext.getOriginal().getId());
    }

    editorContext.setCalculationPoint(null);
    editorContext.setOriginal(null);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
