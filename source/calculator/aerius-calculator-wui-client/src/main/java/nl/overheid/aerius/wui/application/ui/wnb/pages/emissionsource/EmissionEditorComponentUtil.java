/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsource;

import com.axellience.vuegwt.core.client.vue.VueComponentFactory;

import nl.aerius.wui.dev.GWTProd;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.components.source.characteristics.OPSCharacteristicsEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.characteristics.adms.ADMSCharacteristicsEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.farmland.FarmLandDetailEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.farmland.FarmLandEmissionEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.farmlodging.FarmLodgingDetailEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.farmlodging.FarmLodgingEmissionEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.generic.GenericDetailEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.offroad.OffRoadMobileDetailEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.offroad.OffRoadMobileEmissionEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.road.RoadDetailEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.road.RoadEmissionEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.road.characteristics.SRM2CharacteristicsEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.InlandMooringShippingEmissionEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.InlandShippingEmissionEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.inland.InlandShippingDetailEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.mooring.InlandMooringShippingDetailEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.MaritimeShippingDetailEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.MaritimeShippingEmissionEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.mooring.MooringMaritimeShippingDetailEditorComponentFactory;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.mooring.MooringMaritimeShippingEmissionEditorComponentFactory;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

public final class EmissionEditorComponentUtil {
  private EmissionEditorComponentUtil() {}

  public static String getSourceCharacteristicsComponent(final EmissionSourceFeature source) {
    String type;

    if (isESType(source, EmissionSourceType.SRM2_ROAD)) {
      type = SRM2CharacteristicsEditorComponentFactory.get().getComponentTagName();
    } else if (isCType(source, CharacteristicsType.OPS)) {
      type = OPSCharacteristicsEditorComponentFactory.get().getComponentTagName();
    } else if (isCType(source, CharacteristicsType.ADMS)) {
      type = ADMSCharacteristicsEditorComponentFactory.get().getComponentTagName();
    } else {
      type = null;
    }

    return type;
  }

  public static String getEmissionEditorDetailComponent(final EmissionSourceFeature source) {
    final VueComponentFactory<?> factory;

    final EmissionSourceType type = source.getEmissionSourceType();
    switch (type) {
    case FARM_LODGE:
      factory = FarmLodgingDetailEditorComponentFactory.get();
      break;
    case FARMLAND:
      factory = FarmLandDetailEditorComponentFactory.get();
      break;
    case GENERIC:
      factory = GenericDetailEditorComponentFactory.get();
      break;
    case SRM2_ROAD:
      factory = RoadDetailEditorComponentFactory.get();
      break;
    case SHIPPING_MARITIME_MARITIME:
    case SHIPPING_MARITIME_INLAND:
      factory = MaritimeShippingDetailEditorComponentFactory.get();
      break;
    case SHIPPING_INLAND:
      factory = InlandShippingDetailEditorComponentFactory.get();
      break;
    case SHIPPING_INLAND_DOCKED:
      factory = InlandMooringShippingDetailEditorComponentFactory.get();
      break;
    case OFFROAD_MOBILE:
      factory = OffRoadMobileDetailEditorComponentFactory.get();
      break;
    case SHIPPING_MARITIME_DOCKED:
      factory = MooringMaritimeShippingDetailEditorComponentFactory.get();
      break;
    default:
      GWTProd.warn("Unknown emission editor for source: ", source);
      return null;
    }

    return factory.getComponentTagName();
  }

  public static String getEmissionEditorComponenbt(final EmissionSourceFeature source) {
    final VueComponentFactory<?> factory;

    final EmissionSourceType type = source.getEmissionSourceType();
    switch (type) {
    case FARM_LODGE:
      factory = FarmLodgingEmissionEditorComponentFactory.get();
      break;
    case FARMLAND:
      factory = FarmLandEmissionEditorComponentFactory.get();
      break;
    case GENERIC:
      factory = GenericDetailEditorComponentFactory.get();
      break;
    case SRM2_ROAD:
      factory = RoadEmissionEditorComponentFactory.get();
      break;
    case SHIPPING_MARITIME_MARITIME:
    case SHIPPING_MARITIME_INLAND:
      factory = MaritimeShippingEmissionEditorComponentFactory.get();
      break;
    case SHIPPING_INLAND:
      factory = InlandShippingEmissionEditorComponentFactory.get();
      break;
    case SHIPPING_INLAND_DOCKED:
      factory = InlandMooringShippingEmissionEditorComponentFactory.get();
      break;
    case OFFROAD_MOBILE:
      factory = OffRoadMobileEmissionEditorComponentFactory.get();
      break;
    case SHIPPING_MARITIME_DOCKED:
      factory = MooringMaritimeShippingEmissionEditorComponentFactory.get();
      break;
    default:
      GWTProd.warn("Unknown emission editor for source: ", source);
      return null;
    }

    return factory.getComponentTagName();
  }

  private static boolean isCType(final EmissionSourceFeature source, final CharacteristicsType type) {
    return source.getCharacteristicsType() == type;
  }

  private static boolean isESType(final EmissionSourceFeature source, final EmissionSourceType type) {
    return source.getEmissionSourceType() == type;
  }

}
