/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.event.PlaceChangeEvent;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.command.ChangeThemeCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.place.MainThemePlace;

/**
 * Daemon to handle events related to the {@link ApplicationContext}.
 */
public class ApplicationContextDaemon extends BasicEventComponent implements Daemon {
  private static final ApplicationContextDaemonEventBinder EVENT_BINDER = GWT.create(ApplicationContextDaemonEventBinder.class);

  interface ApplicationContextDaemonEventBinder extends EventBinder<ApplicationContextDaemon> {}

  private final ApplicationContext applicationContext;

  @Inject
  public ApplicationContextDaemon(final ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  @EventHandler
  public void onPlaceChangeEvent(final PlaceChangeEvent event) {
    if (event.getValue() instanceof MainThemePlace) {
      final MainThemePlace place = (MainThemePlace) event.getValue();

      if (place.getTheme() != applicationContext.getActiveTheme()) {
        // TODO Don't do this here
        SchedulerUtil.delay(() -> eventBus.fireEvent(new ChangeThemeCommand(place.getTheme())));
      }
    }
  }

  @EventHandler
  public void onChangeThemeCommand(final ChangeThemeCommand c) {
    if (c.getValue() == applicationContext.getActiveTheme()) {
      c.silence();
    } else {
      // FIXME Should this not result in a goTo to a theme place?
      GWTProd.warn("Changing theme but not changing activity.");
      applicationContext.setActiveTheme(c.getValue());
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }

}
