/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;

/**
 * Client side implementation of Road Vehicles.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class Vehicles extends AbstractSubSource {

  private String vehicleType;
  private String timeUnit;

  public static final @JsOverlay void init(final Vehicles props, final VehicleType type) {
    AbstractSubSource.init(props);
    props.setTimeUnit(TimeUnit.DAY);
    props.setVehiclesType(type);
  }

  public final @JsOverlay VehicleType getVehicleType() {
    return VehicleType.safeValueOf(vehicleType);
  }

  public final @JsOverlay void setVehiclesType(final VehicleType type) {
    this.vehicleType = type.name();
  }

  public final @JsOverlay TimeUnit getTimeUnit() {
    return TimeUnit.valueOf(timeUnit);
  }

  public final @JsOverlay void setTimeUnit(final TimeUnit timeUnit) {
    this.timeUnit = timeUnit.name();
  }

}
