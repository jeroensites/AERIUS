/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.input;

import java.util.function.Supplier;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import nl.aerius.vuelidate.Validations;
import nl.aerius.wui.vue.transition.VerticalCollapse;

/**
 * Wraps an input element and displays a label next to the input element. In
 * case of input errors it shows a border around in the input element and
 * displays the error text below the labe/input row.
 */
@Component(components = {
    VerticalCollapse.class
})
public class LabeledInputComponent implements IsVueComponent {
  /**
   * Label to show
   */
  @Prop String label;
  /**
   * Form name of the input element.
   */
  @Prop String name;
  /**
   * Whether to include input warning|error styles (default: yes)
   */
  @Prop boolean inputStyles;
  /**
   * Validations object bound to the input. Called to check for errors.
   */
  @Prop Validations errorValidation;
  /**
   * Validations object bound to a second input. Called to check for errors.
   */
  @Prop Validations errorValidation2;
  /**
   * Override for showing warning status.
   */
  @Prop boolean showWarning;
  /**
   * Supplier that should return a customized warning message in case of an
   * validation error.
   */
  @Prop Supplier<String> warningMessage;
  /**
   * Override for showing error status.
   */
  @Prop boolean showError;
  /**
   * Supplier that should return a customized error message in case of an
   * validation error.
   */
  @Prop Supplier<String> errorMessage;
  /**
   * Optional unit specifier for the input value.
   */
  @Prop String unit;

  /**
   * Optional division, in fractions
   */
  @Prop String division;

  /**
   * Optional value to specify whether the background should be excluded.
   */
  @Prop boolean transparent;

  /**
   * Optional value to specify whether a gap should appear
   */
  @Prop boolean gap;

  @PropDefault("inputStyles")
  boolean inputStylesDefault() {
    return true;
  }

  @PropDefault("showError")
  boolean showErrorDefault() {
    return false;
  }

  @PropDefault("showWarning")
  boolean showWarningDefault() {
    return false;
  }

  @PropDefault("division")
  String divisionDefault() {
    return "4fr 6fr";
  }

  @PropDefault("gap")
  boolean gapDefault() {
    return true;
  }

  @PropDefault("transparent")
  boolean transparentDefault() {
    return false;
  }

  @Computed
  boolean hasError() {
    return errorValidation != null && errorValidation.error
        || errorValidation2 != null && errorValidation2.error;
  }
}
