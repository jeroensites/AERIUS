/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.air.scenario.importer;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.HasValidations;
import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.vue.activity.VueView;
import nl.overheid.aerius.wui.application.components.importer.ApplicationImportButtonComponent;
import nl.overheid.aerius.wui.application.components.input.CheckBoxComponent;
import nl.overheid.aerius.wui.application.components.input.GroupedInputComponent;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.map.MapComponent;
import nl.overheid.aerius.wui.application.components.views.DefaultTitleView;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.event.ExportStartEvent;
import nl.overheid.aerius.wui.application.ui.main.VerticalCollapse;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(
    components = {
    DefaultTitleView.class,
    ApplicationImportButtonComponent.class,
    GroupedInputComponent.class,
    LabeledInputComponent.class,
    CheckBoxComponent.class,
    MapComponent.class,
    VerticalCollapse.class,
    },
    directives = {
    ValidateDirective.class
    },
    customizeOptions = ScenarioImportValidator.class)
public class ScenarioImportView extends BasicVueEventComponent implements VueView<ScenarioImportActivity>, HasCreated, HasValidations {
  private static final ScenarioImportViewEventBinder EVENT_BINDER = GWT.create(ScenarioImportViewEventBinder.class);

  interface ScenarioImportViewEventBinder extends EventBinder<ScenarioImportView> {}

  @Prop ScenarioImportActivity presenter;

  @Inject @Data ApplicationContext appContext;

  @Prop EventBus eventBus;

  @Inject PlaceController placeController;

  @Data @Inject ScenarioContext context;
  @Data @Inject CalculationJobContext calculationContext;

  @Data String name;

  @Data String calculationYear;
  @Data String srm2 = "null";

  @Data boolean notifyAfterCalculate = true;

  @Data String email = "";

  @JsProperty(name = "$v") ScenarioImportValidations validation;

  @Data boolean imported;
  @Data boolean exporting;

  @Computed("isFormInvalid")
  public boolean isFormInvalid() {
    return validation.isInvalid() || !isEmailValid();
  }

  @Computed("isEmailValid")
  public boolean isEmailValid() {
    return  !notifyAfterCalculate || !email.isEmpty();
  }

  @Override
  @Computed
  public ScenarioImportValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);

    calculationYear = "2020"; // TODO AER3-118 appContext.getConfiguration().calculationYearDefault();

    setEventBus(eventBus);
  }

  @EventHandler
  public void onExportStartEvent(final ExportStartEvent e) {
    exporting = true;
  }

  @JsMethod
  public void onExport() {
    if (context.getScenarioCode() == null) {
      return;
    }

    validation.$touch();
    if (isFormInvalid()) {
      return;
    }

// TODO AER3-118    ScenarioImportViewEditor.flush(this, scenario -> presenter.startExport(context.getScenarioCode(), scenario));
  }

  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
