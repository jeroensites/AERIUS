/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsource;

import java.util.Optional;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.google.web.bindery.event.shared.EventBus;

import ol.geom.Geometry;
import ol.geom.GeometryType;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.command.source.EmissionSourcePersistGeometryCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.InputWarningComponent;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceDrawGeometryCommand;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceModifyGeometryCommand;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.util.WKTUtil;

/**
 * Component to handle editing the location of a emission source.
 *
 * This component manages the interaction on the map through events.
 *
 * TODO Refactor entirely and model after BuildingLocationEditorComponent
 * instead.
 */
@Component(components = {
    ButtonIcon.class,
    MinimalInputComponent.class,
    TooltipComponent.class,
    VerticalCollapse.class,
    ValidationBehaviour.class,
    InputWarningComponent.class,
    SmartWktInputComponent.class,
})
public class EmissionSourceLocationView extends ErrorWarningValidator {
  @Prop GeometryType[] disabledGeometries;

  @Prop EventBus eventBus;

  @Prop EmissionSourceFeature source;
  @Prop boolean open;

  @Data GeometryType geometryType = GeometryType.Point;

  @Data String locationString;

  @Data Geometry drawingGeometry;

  @PropDefault("open")
  boolean openDefault() {
    return true;
  }

  @PropDefault("disabledGeometries")
  GeometryType[] disabledGeometriesDefault() {
    return new GeometryType[0];
  }

  @JsMethod
  public String getGeometryTypeText() {
    return Optional.ofNullable(geometryType)
        .map(v -> v.name())
        .map(v -> i18n.esGeometryType(v))
        .orElse("");
  }

  @Watch(value = "source", isImmediate = true)
  void onSourceChange(final EmissionSourceFeature neww) {
    observeGeometry(source.getGeometry());
  }

  @Watch(value = "open", isImmediate = true)
  public void onOpenChange(final boolean open) {
    if (open) {
      eventBus.fireEvent(new EmissionSourceModifyGeometryCommand(source, v -> observeGeometry(v)));
      eventBus.fireEvent(new EmissionSourceDrawGeometryCommand(geometryType, v -> startDraw(v), v -> {}, v -> finishDraw(v)));
    } else {
      eventBus.fireEvent(new EmissionSourceDrawGeometryCommand());
      eventBus.fireEvent(new EmissionSourceModifyGeometryCommand());
    }
  }

  private void startDraw(final Geometry v) {
    drawingGeometry = v;
  }

  private void finishDraw(final Geometry v) {
    drawingGeometry = null;
  }

  /**
   * TODO This is temporary and specific to road/shipping: AER3-1169
   */
  @Computed("isIllegalGeometryType")
  public boolean isIllegalGeometryType() {
    return source != null
        && (source.getEmissionSourceType() == EmissionSourceType.SRM2_ROAD
            || source.getEmissionSourceType() == EmissionSourceType.SHIPPING_MARITIME_MARITIME
            || source.getEmissionSourceType() == EmissionSourceType.SHIPPING_INLAND
            || source.getEmissionSourceType() == EmissionSourceType.SHIPPING_MARITIME_INLAND)
        && geometryType != GeometryType.LineString;
  }

  @JsMethod
  public void drawSource(final GeometryType type) {
    geometryType = type;
    eventBus.fireEvent(new EmissionSourceDrawGeometryCommand(type, v -> startDraw(v), v -> {}, v -> finishDraw(v)));
  }

  @JsMethod
  public boolean isSourceType(final GeometryType type) {
    return geometryType == type;
  }

  @Computed
  public Geometry getGeometry() {
    return Optional.ofNullable(drawingGeometry)
        .orElse(source.getGeometry());
  }

  @Computed
  public String getGeometryStatistic() {
    return Optional.ofNullable(locationString)
        .map(v -> WKTUtil.tryReadGeometry(v))
        .map(v -> v.getGeometry())
        .map(v -> GeoUtil.getGeometryStatistic(v))
        .orElse("");
  }

  @JsMethod
  public void updateWkt(final String wkt) {
    locationString = wkt;
  }

  @JsMethod
  public void saveGeometry(final Geometry geometry) {
    eventBus.fireEvent(new EmissionSourcePersistGeometryCommand(geometry, source));
  }

  private void observeGeometry(final Geometry geometry) {
    geometryType = Optional.ofNullable(geometry)
        .map(v -> GeometryType.valueOf(geometry.getType()))
        .orElse(GeometryType.Point);
  }
}
