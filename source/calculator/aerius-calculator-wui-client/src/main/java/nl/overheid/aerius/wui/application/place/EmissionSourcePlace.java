/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.place;

import java.util.Map;
import java.util.function.Supplier;

import nl.aerius.wui.place.PlaceTokenizer;
import nl.overheid.aerius.shared.domain.Theme;

/**
 * Place to display a single theme source.
 */
public class EmissionSourcePlace extends MainThemePlace {

  private static final String PLACE_KEY_SOURCE = "source";

  public static class Tokenizer extends MainThemePlace.Tokenizer<EmissionSourcePlace> {

    public Tokenizer(final Supplier<EmissionSourcePlace> provider, final Theme theme, final String... postfix) {
      super(provider, theme, postfix);
    }

    @Override
    protected void updatePlace(final Map<String, String> tokens, final EmissionSourcePlace place) {
      if (!tokens.isEmpty()) {
        place.setEmissionSourceId(tokens.entrySet().iterator().next().getKey());
      }
    }

    @Override
    protected void setTokenMap(final EmissionSourcePlace place, final Map<String, String> tokens) {
      final String id = place.getEmissionSourceId();

      if (id != null) {
        tokens.put(String.valueOf(id), null);
      }
    }
  }

  private String esid;

  public <P extends MainThemePlace> EmissionSourcePlace(final Theme theme) {
    this(createTokenizer(theme));
  }

  public <P extends MainThemePlace> EmissionSourcePlace(final PlaceTokenizer<P> tokenizer) {
    super(tokenizer);
  }

  public static PlaceTokenizer<EmissionSourcePlace> createTokenizer(final Theme theme) {
    return new Tokenizer(() -> new EmissionSourcePlace(theme), theme, PLACE_KEY_SOURCE);
  }

  public String getEmissionSourceId() {
    return esid;
  }

  public void setEmissionSourceId(final String esid) {
    this.esid = esid;
  }

  @Override
  public <T> T visit(final PlaceVisitor<T> visitor) {
    return visitor.apply(this);
  }
}
