/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ol.Feature;
import ol.format.GeoJson;
import ol.format.GeoJsonFeatureOptions;

import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.context.calculation.SituationHandle;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.calculation.EmissionSourceCalculator;
import nl.overheid.aerius.wui.application.util.json.JsonBuilder;
import nl.overheid.aerius.wui.application.util.json.JsonSerializable;

/**
 * Object containing situation specific information.
 *
 * @see nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation
 */
@JsType
public class SituationContext implements JsonSerializable {

  public static final double DEFAULT_NETTING_FACTOR = 0.3;
  private final String code;
  private @JsProperty String id = String.valueOf(Math.random());
  private @JsProperty String year = null;
  private @JsProperty String name = null;
  private @JsProperty SituationType type = null;
  private @JsProperty Double nettingFactor = null;
  private @JsProperty List<EmissionSourceFeature> sources = new ArrayList<>();
  private @JsProperty List<BuildingFeature> buildings = new ArrayList<>();

  public SituationContext(final String code) {
    this.code = code;
  }

  public void copyFrom(final SituationContext original) {
    type = original.getType();
    year = original.getYear();
    nettingFactor = original.getNettingFactor();

    setSources(original.getSources().stream()
        .map(source -> FeatureUtil.clone(source))
        .collect(Collectors.toList()));
    setBuildings(original.getBuildings().stream()
        .map(building -> FeatureUtil.clone(building))
        .collect(Collectors.toList()));
  }

  public String getSituationCode() {
    return code;
  }

  public String getId() {
    return id;
  }

  public void setId(final String id) {
    this.id = id;
  }

  public String getYear() {
    return year;
  }

  public void setYear(final String year) {
    this.year = String.valueOf(year);
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public SituationType getType() {
    return type;
  }

  public void setType(final SituationType type) {
    if (type == SituationType.NETTING) {
      if (this.type != SituationType.NETTING) {
        setNettingFactor(DEFAULT_NETTING_FACTOR);
      }
    } else {
      setNettingFactor(null);
    }

    this.type = type;
  }

  public Double getNettingFactor() {
    return nettingFactor;
  }

  public void setNettingFactor(final Double nettingFactor) {
    this.nettingFactor = nettingFactor;
  }

  /**
   * Return a guarded java.util.List ensuring the underlying list does not get
   * modified externally.
   *
   * This should be used for viewing.
   */
  public List<EmissionSourceFeature> getSources() {
    return new ArrayList<>(sources);
  }

  public int getSourcesSize() {
    return sources.size();
  }

  public List<EmissionSourceFeature> getOriginalSources() {
    return sources;
  }

  @Deprecated
  public void setSources(final List<EmissionSourceFeature> sources) {
    this.sources = new ArrayList<>(sources);
  }

  /**
   * Return a guarded java.util.List ensuring the underlying list does not get
   * modified externally.
   *
   * This should be used for viewing.
   */
  public List<BuildingFeature> getBuildings() {
    // Return a guarded list so this list does not get modified externally
    return new ArrayList<>(buildings);
  }

  public int getBuildingsSize() {
    return buildings.size();
  }

  /**
   * Return the unguarded/unmodified list of buildings. To be used for editing.
   */
  public List<BuildingFeature> getOriginalBuildings() {
    return buildings;
  }

  @Deprecated
  public void setBuildings(final List<BuildingFeature> buildings) {
    this.buildings = new ArrayList<>(buildings);
  }

  public double getTotalEmission(final Substance substance) {
    return EmissionSourceCalculator.sum(getSources(), substance);
  }

  public EmissionSourceFeature findSource(final String gmlId) {
    return getSources().stream()
        .filter(v -> gmlId.equals(v.getGmlId()))
        .findFirst().orElse(null);
  }

  public BuildingFeature findBuilding(final String gmlId) {
    return getBuildings().stream()
        .filter(v -> gmlId.equals(v.getGmlId()))
        .findFirst().orElse(null);
  }

  public BuildingFeature findBuildingById(final String id) {
    return getBuildings().stream()
        .filter(v -> id.equals(v.getId()))
        .findFirst().orElse(null);
  }

  @Override
  public Object toJSON() {
    return new JsonBuilder(this)
        .include("id", "year", "name", "nettingFactor")
        .set("type", type.toString())
        .set("emissionSources", new GeoJson().writeFeaturesObject(sources.toArray(new Feature[sources.size()]), new GeoJsonFeatureOptions()))
        .set("buildings", new GeoJson().writeFeaturesObject(buildings.toArray(new Feature[buildings.size()]), new GeoJsonFeatureOptions()))
        .build();
  }

  public static SituationHandle handleFromSituation(final SituationContext context) {
    final SituationHandle handle = new SituationHandle();
    handle.setCode(context.getSituationCode());
    handle.setType(context.getType());
    handle.setName(context.getName());
    handle.setId(context.getId());
    return handle;
  }
}
