/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import java.util.List;

import ol.OLFactory;
import ol.format.Wkt;
import ol.format.WktReadOptions;
import ol.geom.Geometry;
import ol.proj.Projection;

import nl.overheid.aerius.geo.shared.EPSG;

public class WKTUtil {
  private static final Wkt WKT = new Wkt();

  public static class GeometryResult {
    private Geometry geometry;
    private Exception ex;

    public Geometry getGeometry() {
      return geometry;
    }

    public void setGeometry(final Geometry geometry) {
      this.geometry = geometry;
    }

    public Exception getEx() {
      return ex;
    }

    public void setEx(final Exception ex) {
      this.ex = ex;
    }
  }

  private static List<String> supportedConversionCodes;
  private static String targetEpsgCode;

  public static GeometryResult tryReadGeometry(final String wktInput) {
    final GeometryResult result = new GeometryResult();

    String wktPart;
    String sridPart;

    final String[] parts = wktInput.split(";", 2);
    if (parts.length == 1) {
      sridPart = null;
      wktPart = parts[0];
    } else {
      sridPart = parts[0];
      wktPart = parts[1];
    }

    try {
      final Geometry geometry;
      if (sridPart == null) {
        geometry = WKT.readGeometry(wktPart);
      } else {
        final String[] sridParts = sridPart.split("=", 2);

        final String epsgCode = "EPSG:" + (sridParts.length == 1 ? sridParts[0] : sridParts[1]);

        if (supportedConversionCodes.contains(epsgCode)) {
          final WktReadOptions opts = OLFactory.createOptions();
          opts.setDataProjection(Projection.get(epsgCode));
          opts.setFeatureProjection(Projection.get(targetEpsgCode));
          geometry = WKT.readGeometry(wktPart, opts);
        } else {
          // TODO Provide error information (UK only)
          geometry = null;
        }
      }

      result.setGeometry(geometry);

    } catch (final Exception e) {
      result.setEx(e);
    }

    return result;
  }

  public static void initReference(final EPSG epsg, final List<String> conversionCodes) {
    supportedConversionCodes = conversionCodes;
    targetEpsgCode = epsg.getEpsgCode();
  }
}
