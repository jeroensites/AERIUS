/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.modal.notification;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.gwt.resources.client.DataResource;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.command.misc.ToggleNotificationDisplayCommand;
import nl.overheid.aerius.wui.application.context.NotificationContext;
import nl.overheid.aerius.wui.application.resources.R;
import nl.overheid.aerius.wui.vue.BasicVueComponent;
import nl.overheid.aerius.wui.vue.VectorDirective;

@Component(directives = {
    VectorDirective.class
})
public class NotificationComponentButton extends BasicVueComponent implements IsVueComponent {
  @Prop EventBus eventBus;

  @Inject @Data NotificationContext context;

  @JsMethod
  DataResource getIcon() {
    return R.images().iconNotification();
  }

  @JsMethod
  public void onNotificationClick() {
    eventBus.fireEvent(new ToggleNotificationDisplayCommand());
  }

  @Computed
  public String getCorrectedNotificationCount() {
    final int count = context.getNotificationCount();
    return count > 99 ? ">" : String.valueOf(count);
  }
}
