/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.importer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.Event;
import elemental2.dom.File;
import elemental2.dom.HTMLInputElement;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.wui.application.command.AutomatedUnregisterFileCommand;
import nl.overheid.aerius.wui.application.command.ImportFilesCommand;
import nl.overheid.aerius.wui.application.command.RegisterFileCommand;
import nl.overheid.aerius.wui.application.command.UnregisterFileCommand;
import nl.overheid.aerius.wui.application.command.UnregisterFilesCommand;
import nl.overheid.aerius.wui.application.command.UserImportCalculationPointsRequestedCommand;
import nl.overheid.aerius.wui.application.command.UserImportRequestedCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.modal.DialogModalComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.FileContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.event.AutomatedUnregisterFileEvent;
import nl.overheid.aerius.wui.application.event.ImportCompleteEvent;
import nl.overheid.aerius.wui.application.event.ImportFailureEvent;
import nl.overheid.aerius.wui.application.event.RegisterFileEvent;
import nl.overheid.aerius.wui.application.ui.main.FadeTransition;
import nl.overheid.aerius.wui.application.ui.main.HorizontalCollapse;
import nl.overheid.aerius.wui.application.ui.main.VerticalCollapse;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-import-modal", components = {
    SubstanceImportModalComponent.class,
    DialogModalComponent.class,
    FilesListComponent.class,
    VerticalCollapse.class,
    HorizontalCollapse.class,
    FadeTransition.class,
    FilesWarnings.class,
    FilesErrors.class,
    ErrorsComponent.class,
    ButtonIcon.class,
    SimplifiedListBoxComponent.class,
    LabeledInputComponent.class,
})
public class ApplicationImportModalComponent extends BasicVueComponent implements IsVueComponent, HasCreated {
  private static final ApplicationImportModalEventBinder EVENT_BINDER = GWT.create(ApplicationImportModalEventBinder.class);

  interface ApplicationImportModalEventBinder extends EventBinder<ApplicationImportModalComponent> {}

  public enum Tab {
    FILES,
    WARNINGS,
    ERRORS,
    GLOBAL,
  }

  public enum ImportFilter {
    ALL,
    BUILDINGS,
    SOURCES,
  }

  public enum ImportType {
    GENERAL,
    SITUATION,
  }

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data FileContext fileContext;

  @Inject EnvironmentConfiguration cfg;

  @Prop EventBus eventBus;

  @Ref DialogModalComponent modal;

  @Data Tab tab = Tab.FILES;
  @Data boolean showModal;
  @Data boolean isDragging;
  @Data boolean isSaving;
  @Data boolean hasFileListErrors;
  @Data boolean isCalculationPointsImport;
  @Data FileUploadStatus selectedFileToImportIntoActiveSituation;

  @Data ImportFilter selectedImportFilter = ImportFilter.ALL;

  @Data @JsProperty List<FileUploadStatus> brnFilesToImport = new ArrayList<>();

  @Override
  public void created() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Computed
  public List<ImportFilter> getImportFilters() {
    return getAllFilterOptionsForFiles(fileContext.getFiles());
  }

  private List<ImportFilter> getAllFilterOptionsForFiles(final List<FileUploadStatus> files) {
    final int totalBuildings = files.stream()
        .map(v -> v.getSituationStats())
        .filter(v -> v != null)
        .mapToInt(v -> v.getBuildings())
        .sum();
    final int totalSources = files.stream()
        .map(v -> v.getSituationStats())
        .filter(v -> v != null)
        .mapToInt(v -> v.getSources())
        .sum();

    final List<ImportFilter> filters = new ArrayList<>();

    filters.add(ImportFilter.ALL);
    if (totalBuildings > 0 && totalSources > 0) {
      filters.add(ImportFilter.BUILDINGS);
      filters.add(ImportFilter.SOURCES);
    }

    return filters;
  }

  @Computed("hasMultipleFilterOptions")
  public boolean hasMultipleFilterOptions() {
    return getImportFilters().size() > 1;
  }

  @JsMethod
  public void selectImportFilter(final ImportFilter filter) {
    this.selectedImportFilter = filter;
  }

  @JsMethod
  public void onOpenFileInputClick() {
    ((InputElement) Document.get().getElementById("fileInput")).click();
  }

  @JsMethod
  public boolean hasActiveSituation() {
    return scenarioContext.hasActiveSituation();
  }

  @JsMethod
  public int getMaxNumberOfSituations() {
    return applicationContext.getMaxNumberOfSituations(hasActiveSituation());
  }

  @Computed
  public ImportType getImportType() {
    return hasActiveSituation() ? ImportType.SITUATION : ImportType.GENERAL;
  }

  @Computed("canSubmit")
  public boolean canSubmit() {
    return hasFiles() && !hasErrors() && !hasFileListErrors && isValidated();
  }

  @Computed("canUploadMoreFiles")
  public boolean canUploadMoreFiles() {
    return fileContext.getFiles().stream()
        .filter(fus -> !fus.isCalculationPointStatus())
        .count() < getMaxNumberOfSituations();
  }

  @Computed("isEmpty")
  public boolean isEmpty() {
    return fileContext.getFiles().isEmpty() && brnFilesToImport.size() == 0;
  }

  @Computed("hasGlobal")
  public boolean hasGlobal() {
    return getGlobalCount() > 0;
  }

  @Computed
  public int getGlobalCount() {
    // Reserve for 'actual' import errors (i.e. in relation to existing scenario,
    // which is WIP)
    return 0;
  }

  @Computed("hasErrors")
  public boolean hasErrors() {
    return getErrorCount() > 0;
  }

  @Computed
  public int getErrorCount() {
    return (int) Stream.concat(fileContext.getFiles().stream().flatMap(v -> v.getErrors().stream()), fileContext.getErrors().stream())
        .count();
  }

  @Computed
  public String getErrorCountText() {
    final int count = getErrorCount();
    return count >= RequestMappings.MAX_LIST_SIZE_SANITY
        ? ">" + RequestMappings.MAX_LIST_SIZE_SANITY
        : String.valueOf(count);
  }

  @Computed("hasWarnings")
  public boolean hasWarnings() {
    return getWarningCount() > 0;
  }

  @Computed
  public int getWarningCount() {
    return (int) Stream.concat(fileContext.getFiles().stream().flatMap(v -> v.getWarnings().stream()), fileContext.getWarnings().stream())
        .count();
  }

  @Computed
  public String getWarningCountText() {
    final int count = getWarningCount();
    return count >= RequestMappings.MAX_LIST_SIZE_SANITY
        ? ">" + RequestMappings.MAX_LIST_SIZE_SANITY
        : String.valueOf(count);
  }

  @JsMethod
  public void selectFileToImportIntoActiveSituation(final FileUploadStatus file) {
    this.selectedFileToImportIntoActiveSituation = file;
  }

  @JsMethod
  public void removeFile(final FileUploadStatus item) {
    if (hasActiveSituation()) {
      if (item == this.selectedFileToImportIntoActiveSituation) {
        this.selectedFileToImportIntoActiveSituation = null;
      }

      if (isCalculationPointsImport) {
        // Unregister only the selected file if we're dealing with a calculation points import.
        eventBus.fireEvent(new UnregisterFileCommand(item));
      } else {
        // Unregister the 1 visible file + all files with the same filename
        // Since we only support importing 1 file at a time for importing into
        // situations,
        // deleting all fileContext files is similar to deleting on a specific filename
        eventBus.fireEvent(new UnregisterFilesCommand(fileContext.getFiles()));
      }
    } else {
      eventBus.fireEvent(new UnregisterFileCommand(item));
    }
  }

  @JsMethod
  public void hasFileListErrors(final boolean hasFileListErrors) {
    this.hasFileListErrors = hasFileListErrors;
  }

  @Computed(value = "hasFiles")
  public boolean hasFiles() {
    return !isEmpty();
  }

  @Computed(value = "hasBrnFilesToUpload")
  public boolean hasBrnFilesToUpload() {
    return !brnFilesToImport.isEmpty();
  }

  @Computed(value = "isValidated")
  public boolean isValidated() {
    return fileContext.getFiles().stream()
        .allMatch(v -> v.isValidated());
  }

  @JsMethod
  public void open() {
    selectedImportFilter = ImportFilter.ALL;
    showModal = true;
  }

  @EventHandler
  public void onUserImportCommand(final UserImportRequestedCommand c) {
    showModal = true;
    isCalculationPointsImport = false;
  }

  @EventHandler
  public void onUserImportCalculationPointsCommand(final UserImportCalculationPointsRequestedCommand c) {
    showModal = true;
    isCalculationPointsImport = true;
  }

  @JsMethod
  public void modalSubmit() {
    List<FileUploadStatus> filesToUpload = new ArrayList<>();
    if (hasActiveSituation() && !isCalculationPointsImport) {
      filesToUpload.add(selectedFileToImportIntoActiveSituation);
    } else if (isCalculationPointsImport) {
      fileContext.getFiles().stream()
          .filter(fus -> !fus.isCalculationPointStatus())
          .forEach(fus -> eventBus.fireEvent(new AutomatedUnregisterFileCommand(fus)));

      filesToUpload = fileContext.getFiles().stream()
          .filter(fus -> fus.isCalculationPointStatus())
          .collect(Collectors.toList());
    } else {
      filesToUpload = fileContext.getFiles();
    }
    eventBus.fireEvent(new ImportFilesCommand(filesToUpload, !isCalculationPointsImport && hasActiveSituation(), selectedImportFilter));
  }

  @EventHandler
  public void onImportFailureEvent(final ImportFailureEvent e) {
    modal.cancelClose();
  }

  @EventHandler
  public void onImportCompleteEvent(final ImportCompleteEvent e) {
    selectedFileToImportIntoActiveSituation = null;
    showModal = false;
  }

  @EventHandler
  public void onRegisterFileEvent(final RegisterFileEvent e) {
    if (isCalculationPointsImport && !e.getValue().isCalculationPointStatus()) {
      eventBus.fireEvent(new AutomatedUnregisterFileCommand(e.getValue()));
    }
  }

  @EventHandler
  public void onAutomatedUnregisterFileEvent(final AutomatedUnregisterFileEvent e) {
    if (isCalculationPointsImport && fileContext.getFiles().isEmpty()) {
      NotificationUtil.broadcastWarning(eventBus, i18n.errorImportNoCalculationPointsFound());
    }
  }

  @JsMethod
  public void modalCancel() {
    confirmClose();
  }

  public void confirmClose() {
    if (!hasFiles() || Window.confirm(i18n.importMayCloseConfirm())) {
      brnFilesToImport.clear();
      selectedFileToImportIntoActiveSituation = null;
      showModal = false;
    }
  }

  @JsMethod
  public void clear() {
    fileContext.clearFiles();
  }

  @JsMethod
  public void filesChange(final Event e) {
    final HTMLInputElement input = (HTMLInputElement) e.target;
    for (int i = 0; i < input.files.length; i++) {
      handleFileUpload(input, i);
    }

    // Clear the input value. The browser performs extra validation which may bother
    // us the next time the input field is used.
    input.value = "";
    isDragging = false;
    tab = Tab.FILES;
  }

  private void handleFileUpload(final HTMLInputElement input, final int index) {
    final File file = input.files.item(index);
    final String fileExtension = file.name.substring(file.name.lastIndexOf(".") + 1, file.name.length());

    if (fileExtension.equals(FileFormat.BRN.getExtension())) {
      brnFilesToImport.add(addFileToFileUploadStatus(file));
    } else {
      addFile(addFileToFileUploadStatus(file));
    }
  }

  private FileUploadStatus addFileToFileUploadStatus(final File file) {
    final FileUploadStatus fileUploadStatus = new FileUploadStatus();
    fileUploadStatus.setFile(file);
    return fileUploadStatus;
  }

  @JsMethod
  public void importBrnFiles(final List<FileUploadStatus> brnFiles) {
    brnFiles.forEach(brnFile -> {
      addFile(brnFile);
    });
  }

  private void addFile(final FileUploadStatus fileUploadStatus) {
    if (hasActiveSituation() && this.selectedFileToImportIntoActiveSituation == null) {
      // Only set the import year when importing into a situation. Otherwise, let the import figure out it's own year.
      fileUploadStatus.setImportYear(scenarioContext.getActiveSituation().getYear());
      this.selectedFileToImportIntoActiveSituation = fileUploadStatus;
    }

    eventBus.fireEvent(new RegisterFileCommand(fileUploadStatus));
  }
}
