/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.maritime;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.application.domain.source.shipping.base.AbstractShipping;

/**
 * Client side implementation of Maritime Shipping.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class MaritimeShipping extends AbstractShipping {

  private String maritimeShippingType;
  private int movementsPerTimeUnit;
  private String timeUnit;

  public static final @JsOverlay void init(final MaritimeShipping props, final MaritimeShippingType type) {
    AbstractSubSource.init(props);
    props.setDescription("");
    props.setMaritimeShippingType(type);
    props.setMovementsPerTimeUnit(0);
    props.setTimeUnit(TimeUnit.YEAR);
  }

  public final @JsOverlay MaritimeShippingType getMaritimeShippingType() {
    return MaritimeShippingType.safeValueOf(maritimeShippingType);
  }

  public final @JsOverlay void setMaritimeShippingType(final MaritimeShippingType type) {
    this.maritimeShippingType = type.name();
  }

  public final @JsOverlay int getMovementsPerTimeUnit() {
    return movementsPerTimeUnit;
  }

  public final @JsOverlay void setMovementsPerTimeUnit(final int movementsPerTimeUnit) {
    this.movementsPerTimeUnit = movementsPerTimeUnit;
  }

  public final @JsOverlay TimeUnit getTimeUnit() {
    return TimeUnit.valueOf(timeUnit);
  }

  public final @JsOverlay void setTimeUnit(final TimeUnit timeUnit) {
    this.timeUnit = timeUnit.name();
  }

}
