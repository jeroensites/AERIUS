/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.ImplementedBy;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.export.ExportOptions;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;

@ImplementedBy(CalculationServiceAsyncImpl.class)
public interface CalculationServiceAsync {

  void startCalculation(final ExportOptions exportOptions, final ScenarioContext scenarioContext, final Theme theme, final AsyncCallback<String> callback);

  void getCalculationStatus(final String calculationCode, AsyncCallback<CalculationInfo> callback);

  void cancelCalculation(final String calculationCode, final Theme theme, final AsyncCallback<String> callback);

  void deleteCalculation(String jobKey, final AsyncCallback<String> callback);

  void getSituationResultsSummary(final String calculationCode, final SituationResultsKey situationResultsKey,
      final AsyncCallback<SituationResultsSummary> callback);
}
