/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.maritime;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

/**
 * Client side implementation of StandardMaritimeShipping props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class StandardMaritimeShipping extends MaritimeShipping {

  private String shipCode;

  public static final @JsOverlay StandardMaritimeShipping create() {
    final StandardMaritimeShipping props = new StandardMaritimeShipping();
    MaritimeShipping.init(props, MaritimeShippingType.STANDARD);
    props.setShipCode("");
    return props;
  }

  public final @JsOverlay String getShipCode() {
    return shipCode;
  }

  public final @JsOverlay void setShipCode(final String shipCode) {
    this.shipCode = shipCode;
  }

}
