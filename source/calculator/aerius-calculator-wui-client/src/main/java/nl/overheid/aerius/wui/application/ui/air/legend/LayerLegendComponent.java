/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.air.legend;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.overheid.aerius.geo.domain.legend.Legend;
import nl.overheid.aerius.geo.domain.legend.TextLegend;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ColorLabelsLegendComponent.class,
    TextLegendComponent.class,
})
public class LayerLegendComponent extends BasicVueComponent implements IsVueComponent {
  @Prop Legend legend;

  @JsMethod
  public String getLegendType() {
    if (legend == null) {
      return null;
    }

    if (legend instanceof ColorLabelsLegend) {
      return ColorLabelsLegendComponentFactory.get().getComponentTagName();
    } else if (legend instanceof TextLegend) {
      return TextLegendComponentFactory.get().getComponentTagName();
    } else {
      return null;
    }
  }
}
