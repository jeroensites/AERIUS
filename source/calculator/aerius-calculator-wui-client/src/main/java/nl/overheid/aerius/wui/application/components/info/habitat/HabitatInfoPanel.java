/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.info.habitat;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.ImaerConstants;
import nl.overheid.aerius.shared.domain.info.HabitatSurfaceType;
import nl.overheid.aerius.wui.application.components.info.BasicInfoRowComponent;
import nl.overheid.aerius.wui.application.domain.info.HabitatInfo;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    BasicInfoRowComponent.class
})
public class HabitatInfoPanel extends BasicVueComponent implements IsVueComponent {
  @Prop @JsProperty HabitatInfo habitatInfo;
  @Prop boolean habitatTypePopupShowing;

  @JsMethod
  public double getRelevantMapped() {
    return habitatInfo.getAdditionalSurface(HabitatSurfaceType.RELEVANT_MAPPED) / ImaerConstants.M2_TO_HA;
  }

  @JsMethod
  public double getRelevantCartographic() {
    return habitatInfo.getAdditionalSurface(HabitatSurfaceType.RELEVANT_CARTOGRAPHIC) / ImaerConstants.M2_TO_HA;
  }

}
