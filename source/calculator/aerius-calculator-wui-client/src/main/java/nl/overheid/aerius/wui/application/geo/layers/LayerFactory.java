/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import nl.overheid.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.shared.config.AppThemeConfiguration;
import nl.overheid.aerius.shared.constants.DrivingSide;
import nl.overheid.aerius.wui.application.geo.layers.building.BuildingMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.building.ModifyBuildingGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.building.SelectedBuildingMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.building.SituationBuildingGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.calculationpoint.CalculationPointMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.habitats.HabitatHoverLayer;
import nl.overheid.aerius.wui.application.geo.layers.habitats.HabitatSelectLayer;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationMarkersLayer;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationResultLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.GenericSourceGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.GeometryHighlightLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.InfoMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.ModifySourceGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.RoadSourceGeometryLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.SelectedSourceMarkerLayer;
import nl.overheid.aerius.wui.application.geo.layers.source.SourceLayerGroup;
import nl.overheid.aerius.wui.application.geo.layers.source.SourceMarkerLayer;

public interface LayerFactory {
  SourceLayerGroup createSourceLayerGroup(final int zIndex);
  GenericSourceGeometryLayer createGenericSourceGeometryLayer(final LayerInfo info, final int zIndex);
  SourceMarkerLayer createSourceMarkerLayer(final LayerInfo info, final int zIndex);
  SelectedSourceMarkerLayer createSelectedSourceMarkerLayer(final LayerInfo info, final int zIndex);
  ModifySourceGeometryLayer createModifySourceGeometryLayer(final LayerInfo info, final int zIndex);
  CalculationPointMarkerLayer createCalculationPointMarkerLayer(final int zIndex);

  RoadSourceGeometryLayer createRoadSourceGeometryLayer(final AppThemeConfiguration themeConfiguration, final DrivingSide drivingSide,
      final int zIndex);

  SituationBuildingGeometryLayer createSituationBuildingGeometryLayer(LayerInfo info, int zIndex);
  BuildingMarkerLayer createBuildingMarkerLayer(final LayerInfo info, final int zIndex);
  SelectedBuildingMarkerLayer createSelectedBuildingMarkerLayer(LayerInfo info, int zIndex);
  ModifyBuildingGeometryLayer createModifyBuildingGeometryLayer(LayerInfo info, int zIndex);

  CalculationResultLayer createCalculationResultLayer(final AppThemeConfiguration themeConfiguration);
  CalculationMarkersLayer createCalculationMarkersLayer();

  HabitatSelectLayer createHabitatSelectionLayer();
  HabitatHoverLayer createHabitatHoverLayer();

  InfoMarkerLayer createInfoMarkerLayer();
  GeometryHighlightLayer createGeometryHighlightLayer();

  BackgroundDepositionLayer createBackgroundDepositionLayer(final AppThemeConfiguration themeConfiguration);

  CalculationBoundaryLayer createCalculationBoundaryLayer(final String boundaryWkt);
}
