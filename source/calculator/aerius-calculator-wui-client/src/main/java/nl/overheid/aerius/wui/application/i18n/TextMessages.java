/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.PluralCount;
import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.components.importer.ApplicationImportModalComponent.ImportFilter;
import nl.overheid.aerius.wui.application.components.importer.ApplicationImportModalComponent.ImportType;

public interface TextMessages {
  String dropdownPlaceholder();

  String importTitle();
  String importBrnTitle();
  String importButton();
  String importGeneralDescription();
  String importText(@Select ImportType importType);
  String importBrnText();
  String importTextCalculationPoints();
  String importTextDrag(@Select ImportType importType);
  String importTextDragging(@Select ImportType importType);
  String importTextSaving();
  String importFileTab();
  String importErrorTab();
  String importWarningTab();
  String importGlobalTab();
  String importCohesionErrorsTitle();
  String importCohesionWarningsTitle();
  String importFileUploadError();
  String importMayCloseConfirm();
  String importRemoveAll();
  String importOpenFileDialog(@Select ImportType importType);
  String importFilterLabel();
  String importFilterText(@Select ImportFilter filter);
  String importSrm2OptionOwn();
  String importSrm2OptionYear(String option);
  String importSrm2OptionDefault();
  String importErrorOverflow();
  String importWarningOverflow();
  String importSituationHeaderTitle();
  String importDownloadLog();
  String importBrnChooseSubstance();
  String importResultsOrNot();
  String importYearAutoSnapText(String oldYear, String newYear);

  String situationViewSources();
  String situationViewCalculationPoints();
  String situationViewBuildings();

  String scenarioCreateSource();
  String scenarioPreferenceLabelYear();
  String scenarioPreferenceLabelName();
  String scenarioPreferenceLabelDescription();
  String scenarioPreferenceLabelSRM2();
  String scenarioPreferenceLabelNotification();
  String scenarioPreferenceLabelNotificationAfterValidate();
  String scenarioPreferenceLabelNotificationAfterCalculate();
  String scenarioPreferenceLabelEmail();
  String scenarioSubmit();
  String scenarioCalculate();
  String scenarioExport();
  String scenarioExportFileDownload(String fileUrl);
  String scenarioImportComplete(@PluralCount int files);
  String scenarioExportError(final String errorMessage);

  String buttonOk();
  String buttonCancel();

  String notificationHeader();
  String notificationNoMessages();
  String notificationNumberOfMessages(final int number);
  String notificationRemoveAllMessages();
  String notificationDateFormat();
  String notificationDateTimeFormat();
  String notificationExportStarted();
  String notificationCalculationStarted();
  String notificationCalculationNotStartedNoSources(final String situationName);
  String notificationCalculationNotStartedAlreadyRunning();
  String notificationCalculationBeingCancelled();
  String notificationCalculationCancelled();
  String notificationCalculationError(String message);
  String notificationCalculationCompleted();
  String notificationCalculationStartError(String message);
  String notificationCalculationCancelledError(String message);
  String notificationCalculationStatusError(String message);
  String notificationSourcesUpdatedBeforeEmissionsCalculated();
  String notificationSourceUpdatedBeforeEmissionsCalculated(String sourceLabel);

  String sourceShipWaterwayNotDetermined();
  String sourceShipWaterwayInconclusiveRoute(String bestFitWaterway, String otherWaterways);

  String flagsExtendedResults();

  String situationNameLabel();
  String situationTypeLabel();
  String situationYearLabel();
  String situationNettingFactorLabel();
  String situationDeleteTitle();
  String situationType(@Select SituationType situation);
  String situationType2(@Select SituationType situation);

  String clButtonNewSource();
  String clButtonImportFile();
  String clPageTitle();
  String clPageInfo1();
  String clPageInfo2(String quickStartUrl);
}
