/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import static jsinterop.annotations.JsPackage.GLOBAL;

import ol.Feature;
import ol.geom.Geometry;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.geo.wui.util.OL3GeometryUtil;

/**
 * Represents a label for an emission source or building
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class LabelFeature extends Feature {

  public static @JsOverlay LabelFeature create(final String id, final Geometry geometry, final String label, final String color, final Feature referenceFeature) {
    final LabelFeature labelFeature = new LabelFeature();
    labelFeature.setId(id);
    labelFeature.setGeometry(OL3GeometryUtil.getMiddlePointOfGeometry(geometry));
    labelFeature.setLabel(label);
    labelFeature.setColor(color);
    labelFeature.setReferenceFeature(referenceFeature);
    return labelFeature;
  }

  public final @JsOverlay String getLabel() {
    return get("label") == null ? "" : Js.asString(get("label"));
  }

  public final @JsOverlay void setLabel(final String label) {
    set("label", label);
  }

  public final @JsOverlay String getColor() {
    return get("color") == null ? "" : Js.asString(get("color"));
  }

  public final @JsOverlay void setColor(final String color) {
    set("color", color);
  }

  public final @JsOverlay Feature getReferenceFeature() {
    return get("referenceFeature");
  }

  public final @JsOverlay void setReferenceFeature(final Feature feature) {
    set("referenceFeature", feature);
  }

}
