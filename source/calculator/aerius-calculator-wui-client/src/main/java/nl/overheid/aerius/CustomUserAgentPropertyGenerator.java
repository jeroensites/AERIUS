/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;

import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.linker.ConfigurationProperty;
import com.google.gwt.core.ext.linker.PropertyProviderGenerator;
import com.google.gwt.user.rebind.SourceWriter;
import com.google.gwt.user.rebind.StringSourceWriter;
import com.google.gwt.useragent.rebind.UserAgentGenerator;
import com.google.gwt.useragent.rebind.UserAgentPropertyGenerator;

/**
 * Generator which writes out the JavaScript for determining the value of the <code>user.agent</code> selection property.
 */
public class CustomUserAgentPropertyGenerator implements PropertyProviderGenerator {

  /**
   * The list of {@code user.agent} values listed here should be kept in sync with {@code UserAgent.gwt.xml}.
   * <p>
   * Note that the order of enums matter as the script selection is based on running these predicates in order and matching the first one that returns
   * {@code true}.
   * <p>
   * Also note that, {@code docMode < 11} in predicates for older IEs exists to ensures we never choose them for IE11 (we know that they will not work
   * for IE11).
   */
  private enum UserAgent {
    chrome("return (uaExtended.indexOf('webkit') != -1);"),
    edge("return (uaExtended.indexOf('gecko') != -1 && (docModeExtended > 11));"),
    ie11("return (uaExtended.indexOf('gecko') != -1 && (docModeExtended == 11));"),
    ie10("return (uaExtended.indexOf('msie') != -1 && (docModeExtended >= 10 && docModeExtended < 11));"),
    ie9("return (uaExtended.indexOf('msie') != -1 && (docModeExtended >= 9 && docModeExtended < 11));"),
    ie8("return (uaExtended.indexOf('msie') != -1 && (docModeExtended >= 8 && docModeExtended < 11));"),
    firefox("return (uaExtended.indexOf('gecko') != -1 || docModeExtended >= 11);");

    private final String predicateBlock;

    private UserAgent(final String predicateBlock) {
      this.predicateBlock = predicateBlock;
    }

    private static Set<String> getKnownAgents() {
      final Set<String> userAgents = new HashSet<String>();
      for (final UserAgent userAgent : values()) {
        userAgents.add(userAgent.name());
      }
      return userAgents;
    }
  }

  private static final String PROPERTY_USER_AGENT = "user.agent.extended";

  /**
   * Writes out the JavaScript function body for determining the value of the <code>user.agent</code> selection property. This method is used to
   * create the selection script and by {@link UserAgentGenerator} to assert at runtime that the correct user agent permutation is executing.
   */
  static void writeUserAgentPropertyJavaScript(final SourceWriter body,
      final SortedSet<String> possibleValues, String fallback) {

    // write preamble
    body.println("var uaExtended = navigator.userAgent.toLowerCase();");
    body.println("var docModeExtended = $doc.documentMode;");

    for (final UserAgent userAgent : UserAgent.values()) {
      // write only selected user agents
      if (possibleValues.contains(userAgent.name())) {
        body.println("if ((function() { ");
        body.indentln(userAgent.predicateBlock);
        body.println("})()) return '%s';", userAgent.name());
      }
    }

    // default return
    if (fallback == null) {
      fallback = "unknown";
    }
    body.println("return '" + fallback + "';");
  }

  @Override
  public String generate(final TreeLogger logger, final SortedSet<String> possibleValues, final String fallback,
      final SortedSet<ConfigurationProperty> configProperties) {
    assertUserAgents(logger, possibleValues);

    final StringSourceWriter body = new StringSourceWriter();
    body.println("{");
    body.indent();
    writeUserAgentPropertyJavaScript(body, possibleValues, fallback);
    body.outdent();
    body.println("}");

    return body.toString();
  }

  private static void assertUserAgents(final TreeLogger logger, final SortedSet<String> possibleValues) {
    final HashSet<String> unknownValues = new HashSet<String>(possibleValues);
    unknownValues.removeAll(UserAgent.getKnownAgents());
    if (!unknownValues.isEmpty()) {
      logger.log(TreeLogger.WARN, "Unrecognized " + PROPERTY_USER_AGENT
          + " values " + unknownValues + ", possibly due to UserAgent.gwt.xml and "
          + UserAgentPropertyGenerator.class.getName() + " being out of sync.");
    }
  }
}