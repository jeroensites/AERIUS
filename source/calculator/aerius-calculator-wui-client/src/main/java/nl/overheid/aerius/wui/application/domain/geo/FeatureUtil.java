/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.domain.geo;

import java.util.List;

import ol.Collection;
import ol.Feature;

import elemental2.core.Global;

import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;

public class FeatureUtil {

  private FeatureUtil() {
  }

  /**
   * Finds the next available ID in the list, assigns it to the Feature and adds
   * it to a list
   *
   * @param collection The feature collection to be added to
   * @param feature    The emission source to be added
   */
  public static <F extends Feature> void addAndSetId(final List<F> collection, final F feature) {
    final ConsecutiveIdUtil.IndexIdPair newId = getConsecutiveIdUtil(collection).generate();

    feature.setId(newId.getId());
    collection.add(newId.getIndex(), feature);
  }

  /**
   * Finds the next available ID in the list and returns it
   *
   * @param collection The feature collection to be added to
   */
  public static <F extends Feature> String getNextLabel(final Collection<F> collection) {
    return getConsecutiveIdUtil(collection).generate().getId();
  }

  public static <F extends Feature> ConsecutiveIdUtil getConsecutiveIdUtil(final Collection<F> collection) {
    return new ConsecutiveIdUtil(
        i -> Integer.toString(i + 1),
        i -> collection.getArray()[i].getId(),
        () -> collection.getLength());
  }

  /**
   * Finds the next available ID in the list and returns it
   *
   * @param collection The feature collection to be added to
   */
  public static <F extends Feature> String getNextLabel(final List<F> collection) {
    return getConsecutiveIdUtil(collection).generate().getId();
  }

  public static <F extends Feature> ConsecutiveIdUtil getConsecutiveIdUtil(final List<F> collection) {
    return new ConsecutiveIdUtil(
        i -> Integer.toString(i + 1),
        i -> collection.get(i).getId(),
        () -> collection.size());
  }

  /**
   * Perform a file-style increment of the given string.
   *
   * If the label matches (x) at the end of the string where x is a number,
   * increment the number and return it. If not, add ` (1)` to the string and
   * return it.
   */
  public static String smartIncrementLabel(final String label) {
    if (label.matches(".+\\([1-9]+\\)$")) {
      final int beginBracket = label.lastIndexOf("(");
      final int endBracket = label.lastIndexOf(")");
      final String numPortion = label.substring(beginBracket + 1, endBracket);

      try {
        final int num = Integer.parseInt(numPortion);
        final String labelBare = label.substring(0, beginBracket);
        return labelBare + "(" + (num + 1) + ")";
      } catch (final NumberFormatException e) {
        return label + " (1)";
      }
    } else {
      return label + " (1)";
    }
  }

  /**
   * Deep copies a {@link Feature}.
   * Includes copying ID.
   *
   * @param originalFeature The original feature object.
   * @return new feature
   */
  public static <F extends Feature> F clone(final F originalFeature) {
    final Feature feature = originalFeature.clone();
    // ol.Feature.clone does not create a deep copy. Hence copy properties.
    copyProperties(feature, feature);
    // ol.Feature.clone does not copy the id, so be sure to set it in this case.
    feature.setId(originalFeature.getId());
    return Js.cast(feature);
  }

  /**
   * Deep copies a {@link Feature}.
   * Feature will be copied without id (and gmlId).
   *
   * @param originalFeature The original feature object.
   * @return new feature
   */
  public static <F extends Feature> F cloneWithoutIds(final F originalFeature) {
    final Feature feature = originalFeature.clone();
    // ol.Feature.clone does not create a deep copy. Hence copy properties.
    copyProperties(feature, feature);
    // ol.Feature.clone does not copy the id, which is fine in this case.
    // However, we also want to clear GML ID (perhaps not all features have this, but shouldn't hurt in that case).
    feature.set("gmlId", null);
    return Js.cast(feature);
  }

  /**
   * Deep copies the source feature to the target feature.
   *
   * @param sourceFeature The feature to copy
   * @param targetFeature The feature to set the data of the source feature
   */
  public static <F extends Feature> void copy(final F sourceFeature, final F targetFeature) {
    copyProperties(sourceFeature, targetFeature);

    targetFeature.setGeometry(sourceFeature.getGeometry());
  }

  /**
   * Copies properties of a feature without the geometry data.
   *
   * @param <F>
   * @param sourceFeature
   * @param targetFeature
   */
  private static <F extends Feature> void copyProperties(final F sourceFeature, final Feature targetFeature) {
    // Because clone makes a shallow copy of the properties we need to set the
    // properties by re-reading it as json.
    final JsPropertyMap<Object> properties = Js.cast(Global.JSON.parse(Global.JSON.stringify(sourceFeature.getProperties())));
    // Because properties contains everything, including geometry which can't be set
    // as property it must be deleted and added via the method.
    properties.delete("geometry");
    targetFeature.setProperties(properties);
  }
}
