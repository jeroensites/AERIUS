/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.results.progress;

import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;

public enum CalculationState {
  QUEUED(1),
  INITIALISED(2),
  RUNNING(3),
  PARTIALLY_COMPLETED(4),
  FINISHED(5),
  FINISHED_WITHOUT_SUCCESS(5, false);

  public static final int STEPS = 5;

  private final int value;
  private final boolean happy;

  private CalculationState(final int value) {
    this(value, true);
  }

  private CalculationState(final int value, final boolean happy) {
    this.value = value;
    this.happy = happy;
  }

  public boolean isBefore(final int step) {
    return value < step;
  }

  public boolean isAfter(final int step) {
    return value > step;
  }

  public static CalculationState fromJob(final JobProgress jobProgress) {
    CalculationState state = CalculationState.QUEUED;
    if (jobProgress == null || jobProgress.getState() == null) {
      return state;
    }

    switch (jobProgress.getState()) {
    case ERROR:
    case CANCELLED:
    case DELETED:
      state = CalculationState.FINISHED_WITHOUT_SUCCESS;
      break;
    case COMPLETED:
      state = CalculationState.FINISHED;
      break;
    case RUNNING:
      if (jobProgress.getHexagonCount() == 0) {
        state = CalculationState.RUNNING;
      } else {
        state = CalculationState.PARTIALLY_COMPLETED;
      }
      break;
    case INITIALIZED:
      state = CalculationState.INITIALISED;
      break;
    default:
      state = CalculationState.QUEUED;
      break;
    }

    return state;
  }

  public boolean isSuccess() {
    return isHappy();
  }

  public boolean isHappy() {
    return happy;
  }
}
