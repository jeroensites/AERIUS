/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.activity;

import nl.overheid.aerius.wui.application.place.MainThemePlace;
import nl.overheid.aerius.wui.application.place.OverviewPlace;
import nl.overheid.aerius.wui.application.place.PrintWnbPlace;
import nl.overheid.aerius.wui.application.print.wnb.PrintWnbActivity;
import nl.overheid.aerius.wui.application.ui.nca.NcaThemeActivity;
import nl.overheid.aerius.wui.application.ui.overview.OverviewActivity;
import nl.overheid.aerius.wui.application.ui.unsupported.NotSupportedActivity;
import nl.overheid.aerius.wui.application.ui.wnb.WnbThemeActivity;

public interface ApplicationActivityFactory {
  OverviewActivity createOverviewActivity(OverviewPlace place);

  NotSupportedActivity createNotSupportedActivity();

  NcaThemeActivity createNcaActivity(MainThemePlace place);

  WnbThemeActivity createWnbActivity(MainThemePlace place);

  PrintWnbActivity createPrintWnbActivity(PrintWnbPlace place);

}
