/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.context;

import javax.inject.Singleton;

import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;

@Singleton
public class CalculationPointEditorContext {

  private CalculationPointFeature calculationPoint = null;
  private CalculationPointFeature original = null;

  public CalculationPointFeature getCalculationPoint() {
    return calculationPoint;
  }

  public void setCalculationPoint(final CalculationPointFeature calculationPoint) {
    this.calculationPoint = calculationPoint;
  }

  public boolean isEditing() {
    return calculationPoint != null;
  }

  public CalculationPointFeature getOriginal() {
    return original;
  }

  public void setOriginal(final CalculationPointFeature original) {
    this.original = original;
  }
}
