/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.calculation;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.shared.config.AppThemeConfiguration;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculateContext;
import nl.overheid.aerius.wui.application.event.ChangeThemeEvent;

/**
 * This daemon is responsible for managing configuration state before starting a
 * calculation.
 */
public class CalculationPreparationDaemon extends BasicEventComponent implements Daemon {
  private static final CalculationPreparationDaemonEventBinder EVENT_BINDER = GWT.create(CalculationPreparationDaemonEventBinder.class);

  interface CalculationPreparationDaemonEventBinder extends EventBinder<CalculationPreparationDaemon> {}

  @Inject ApplicationContext applicationContext;
  @Inject CalculateContext calculateContext;

  @EventHandler
  public void onChangeThemeEvent(final ChangeThemeEvent event) {
    final AppThemeConfiguration theme = applicationContext.getActiveThemeConfiguration();

    if (theme != null) {
      calculateContext.getCalculationOptions().setCalculationType(theme.getDefaultCalculationType());
      calculateContext.setMeteoYear(theme.getDefaultMeteoYear());
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
