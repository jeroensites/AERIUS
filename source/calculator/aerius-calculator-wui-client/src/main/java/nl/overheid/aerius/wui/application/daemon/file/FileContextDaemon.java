/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.file;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.future.AppAsyncCallback;
import nl.overheid.aerius.wui.application.command.AutomatedUnregisterFileCommand;
import nl.overheid.aerius.wui.application.command.RegisterFileCommand;
import nl.overheid.aerius.wui.application.command.UnregisterFileCommand;
import nl.overheid.aerius.wui.application.command.UnregisterFilesCommand;
import nl.overheid.aerius.wui.application.context.FileContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.event.AutomatedUnregisterFileEvent;
import nl.overheid.aerius.wui.application.event.ImportCompleteEvent;
import nl.overheid.aerius.wui.application.event.ImportFailureEvent;
import nl.overheid.aerius.wui.application.service.FileServiceAsync;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

/**
 * Daemon process to manage uploading of files.
 */
@Singleton
public class FileContextDaemon extends BasicEventComponent implements Daemon {
  private static final ScenarioContextDaemonEventBinder EVENT_BINDER = GWT.create(ScenarioContextDaemonEventBinder.class);

  interface ScenarioContextDaemonEventBinder extends EventBinder<FileContextDaemon> {}

  @Inject FileServiceAsync service;
  @Inject ScenarioContext scenarioContext;
  @Inject FileContext fileContext;
  @Inject FileValidationPoller poller;

  @EventHandler
  public void onImportFailureEvent(final ImportFailureEvent e) {
    fileContext.clearFiles();
  }

  @EventHandler
  public void onImportCompleteEvent(final ImportCompleteEvent e) {
    fileContext.clearFiles();
  }

  @EventHandler
  public void onRegisterFileCommand(final RegisterFileCommand c) {
    final FileUploadStatus file = c.getValue();
    fileContext.add(file);

    service.uploadFile(file, AppAsyncCallback.create(fileCode -> {
      file.setFileCode(fileCode);
      registerStatusCheck(file);
    }));
  }

  @EventHandler
  public void onUnregisterFileCommand(final UnregisterFileCommand c) {
    final FileUploadStatus file = c.getValue();
    deleteFile(file);
  }

  @EventHandler
  public void onAutomatedUnregisterFileCommand(final AutomatedUnregisterFileCommand c) {
    c.silence();

    final FileUploadStatus file = c.getValue();
    deleteFile(file, () -> eventBus.fireEvent(new AutomatedUnregisterFileEvent(file)));
  }

  @EventHandler
  public void onUnregisterFilesCommand(final UnregisterFilesCommand c) {
    final List<FileUploadStatus> files = c.getValue();
    files.forEach(this::deleteFile);
  }

  private void deleteFile(final FileUploadStatus file) {
    deleteFile(file, null);
  }

  private void deleteFile(final FileUploadStatus file, final Runnable onComplete) {
    // If the file upload was not successful, remove locally
    if (!file.isFailed() && !file.isComplete() || file.isFailed()) {
      fileContext.remove(file);
      Optional.ofNullable(onComplete).ifPresent(runnable -> runnable.run());
    } else {
      // Otherwise remove gracefully
      file.setPending(true);
      service.removeFile(file, b -> {
        // Remove regardless of response
        fileContext.remove(file);
        poller.start();
        Optional.ofNullable(onComplete).ifPresent(runnable -> runnable.run());
      });
    }
  }

  private void registerStatusCheck(final FileUploadStatus file) {
    file.setPending(true);

    if (!poller.isRunning()) {
      poller.start();
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
