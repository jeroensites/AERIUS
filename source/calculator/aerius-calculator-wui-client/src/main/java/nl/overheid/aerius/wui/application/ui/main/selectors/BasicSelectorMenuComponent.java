/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.main.selectors;

import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Emit;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasBeforeDestroy;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.HTMLElement;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.wui.application.command.misc.DeactivateSelectorMenuCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.nav.selector.SelectorItem;
import nl.overheid.aerius.wui.application.components.nav.selector.SelectorItemOption;
import nl.overheid.aerius.wui.application.util.AccessibilityUtil;
import nl.overheid.aerius.wui.application.util.AccessibilityUtil.Register;

@Component(components = {
    ButtonIcon.class,
    VerticalCollapseGroup.class
})
public class BasicSelectorMenuComponent implements IsVueComponent, HasBeforeDestroy, HasMounted {
  @Prop SelectorItem item;

  @Prop @JsProperty List<SelectorItemOption> options;
  @Prop SelectorItemOption selectedOption;

  @Prop EventBus eventBus;

  @Ref HTMLElement main;

  private Register register;

  @JsMethod
  public boolean isSelected(final SelectorItemOption option) {
    return item.isSelected(option);
  }

  @JsMethod
  @Emit
  public void selectOption(final SelectorItemOption option) {}

  @JsMethod
  public void deactivateSelectorMenu() {
    eventBus.fireEvent(new DeactivateSelectorMenuCommand());
  }

  @Watch(value = "item", isImmediate = true)
  public void onItemChange(final SelectorItem item) {
    Vue.nextTick(() -> AccessibilityUtil.findFirstFocusable(main).focus());
  }

  @Override
  public void beforeDestroy() {
    register.removeHandle();
  }

  @Override
  public void mounted() {
    register = AccessibilityUtil.trapFocus(main);
  }
}
