/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.calculation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.command.CalculationCancelCommand;
import nl.overheid.aerius.wui.application.command.CalculationResetCommand;
import nl.overheid.aerius.wui.application.command.CalculationStartCommand;
import nl.overheid.aerius.wui.application.command.RestoreCalculationContextCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.PrintWnbContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.application.event.CalculationCancelEvent;
import nl.overheid.aerius.wui.application.event.CalculationCompleteEvent;
import nl.overheid.aerius.wui.application.event.CalculationStartEvent;
import nl.overheid.aerius.wui.application.event.CalculationStatusEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.place.ResultsPlace;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.CalculationServiceAsync;

@Singleton
public class CalculationDaemon extends BasicEventComponent implements Daemon {
  private static final CalculationDaemonEventBinder EVENT_BINDER = GWT.create(CalculationDaemonEventBinder.class);

  interface CalculationDaemonEventBinder extends EventBinder<CalculationDaemon> {}

  private static final AsyncCallback<String> SILENT_CALLBACK = new AsyncCallback<String>() {
    @Override
    public void onSuccess(final String result) {}
    @Override
    public void onFailure(final Throwable caught) {};
  };

  @Inject CalculationStatusPoller calculationStatusPoller;
  @Inject CalculationServiceAsync calculationService;
  @Inject ScenarioContext scenarioContext;
  @Inject CalculationContext calculationContext;
  @Inject ApplicationContext applicationContext;
  @Inject PlaceController placeController;
  @Inject PrintWnbContext printWnbContext;

  @EventHandler
  public void onCalculationStartCommand(final CalculationStartCommand c) {
    c.silence();

    final List<String> problems = findCalculationStartProblems();
    if (!problems.isEmpty()) {
      // Broadcast an error for each problem
      problems.stream()
          // .limit(1) // Limit to one notification (?)
          .forEach(problem -> NotificationUtil.broadcastError(eventBus, problem));
      return;
    }
    deletePreviousCalculation();
    final CalculationJobContext calculation = calculationFromScenarioContext();
    calculationContext.setActiveCalculation(calculation);
    placeController.goTo(new ResultsPlace(applicationContext.getActiveTheme()));

    // Start the calculation if there are no problems
    calculationService.startCalculation(c.getValue(), scenarioContext, applicationContext.getActiveTheme(),
        AppAsyncCallback.create(
            calculationCode -> {
              calculation.setCalculationCode(calculationCode);

              eventBus.fireEvent(c.createEvent(c.getValue()));

              calculationStatusPoller.start(calculationCode);
            }, e -> handleCalculationStartError(calculation, e)));
  }

  public void deletePreviousCalculation() {
    if (calculationContext.getLastKnownJobKey() != null) {
        calculationService.deleteCalculation(calculationContext.getLastKnownJobKey(), SILENT_CALLBACK);
    }
  }

  private void handleCalculationStartError(final CalculationJobContext calculation, final Throwable e) {
    final String message = e.getMessage();
    final JobProgress jobProgress = new JobProgress();
    jobProgress.setState(JobState.ERROR);
    jobProgress.setErrorMessage(message);
    calculation.getCalculationInfo().setJobProgress(jobProgress);
    NotificationUtil.broadcastError(eventBus, M.messages().notificationCalculationStartError(message));
  }

  @EventHandler
  public void onRestoreCalculationContextCommand(final RestoreCalculationContextCommand c) {
    // Silence because this event is being called in the callback
    c.silence();

    final CalculationJobContext calculationJobContext = calculationFromScenarioContext();
    calculationJobContext.setCalculationCode(printWnbContext.getJobId());

    // Set these values specifically for rendering map markers in Project Calculation PDF.
    calculationJobContext.getResultContext().setResultType(printWnbContext.getScenarioResultType());
    calculationJobContext.getResultContext().setHexagonType(printWnbContext.getSummaryHexagonType());

    calculationContext.setActiveCalculation(calculationJobContext);

    calculationService.getCalculationStatus(printWnbContext.getJobId(), AeriusRequestCallback.create(v -> {
      // Broadcast status events to set calculation status context.
      eventBus.fireEvent(new CalculationStatusEvent(v));

      eventBus.fireEvent(c.getEvent());
    }));
  }

  private CalculationJobContext calculationFromScenarioContext() {
    return createAndAddCalculation(job -> {
      final String stateHash = ScenarioContext.calculateHash(scenarioContext);
      job.setInputHash(stateHash);

      // TODO Fill with all handles that are being calculated (see CalculateContext,
      // also refactor that one)
      job.setSituations(scenarioContext.getSituations().stream()
          .map(situation -> SituationContext.handleFromSituation(situation))
          .collect(Collectors.toList()));

      final ResultSummaryContext resultContext = job.getResultContext();
      resultContext.setSituationHandle(SituationContext.handleFromSituation(scenarioContext.getActiveSituation()));
    });
  }

  /**
   * Create a new calculation and add to the context
   *
   * @param calculationBuilder function to change situation properties before the
   *                           situation is added
   */
  public CalculationJobContext createAndAddCalculation(final Consumer<CalculationJobContext> calculationBuilder) {
    final ConsecutiveIdUtil.IndexIdPair newId = calculationContext.createConsecutiveCalculationIdUtil().generate();

    final CalculationJobContext calculationJobContext = new CalculationJobContext();

    calculationContext.addCalculation(newId.getIndex(), calculationJobContext);

    // Set default values
    calculationBuilder.accept(calculationJobContext);

    return calculationJobContext;
  }

  @EventHandler
  public void onCalculationStatusEvent(final CalculationStatusEvent e) {
    final CalculationInfo calculationInfo = e.getValue();
    calculationContext.getActiveCalculation().setCalculationInfo(calculationInfo);

    switch (calculationInfo.getJobProgress().getState()) {
    case UNDEFINED:
      // TODO Is this possible? should we report an error?
      break;
    case INITIALIZED:
      // Do nothing
      break;
    case RUNNING:
      // Do nothing
      break;
    case DELETED:
      // Do nothing
      // TODO Do something? Is this behaviour defined?
      break;
    case CANCELLED:
      eventBus.fireEvent(new CalculationCancelEvent());
      break;
    case ERROR:
      NotificationUtil.broadcastError(eventBus, M.messages().notificationCalculationError(calculationInfo.getJobProgress().getErrorMessage()));
      break;
    case COMPLETED:
      eventBus.fireEvent(new CalculationCompleteEvent(calculationContext.getActiveCalculationCode()));
      break;
    }
  }

  @EventHandler
  public void onCalculationCompleteEvent(final CalculationCompleteEvent e) {
    NotificationUtil.broadcastMessage(eventBus, M.messages().notificationCalculationCompleted(), true);
  }

  /**
   * Do a hard reset without trying graceful cancellation (this command would
   * typically be fired after the calculation has already gone kaput)
   */
  @EventHandler
  public void onCalculationResetCommand(final CalculationResetCommand c) {
    calculationContext.reset();
    calculationStatusPoller.stop();
    placeController.goTo(new CalculatePlace(applicationContext.getActiveTheme()));
  }

  @EventHandler
  public void onCalculationCancelCommand(final CalculationCancelCommand c) {
    c.silence();

    final String calculationCode = calculationContext.getActiveCalculation().getCalculationCode();
    if (calculationCode == null) {
      calculationContext.reset();
    } else {
      calculationService.cancelCalculation(calculationCode, applicationContext.getActiveTheme(),
          AppAsyncCallback.create(res -> {
            // Notify a calculation is in the process of being cancelled
            NotificationUtil.broadcastMessage(eventBus, M.messages().notificationCalculationBeingCancelled());
          }, e -> NotificationUtil.broadcastError(eventBus, M.messages().notificationCalculationCancelledError(e.getMessage()))));
    }
  }

  @EventHandler
  public void onCalculationStartEvent(final CalculationStartEvent e) {
    NotificationUtil.broadcastMessage(eventBus, M.messages().notificationCalculationStarted(), true);
  }

  @EventHandler
  public void onCalculationCancelEvent(final CalculationCancelEvent e) {
    NotificationUtil.broadcastMessage(eventBus, M.messages().notificationCalculationCancelled());
    calculationContext.reset();

    placeController.goTo(new CalculatePlace(applicationContext.getActiveTheme()));
  }

  /**
   * Collate all immediate problems before starting a calculation
   */
  private List<String> findCalculationStartProblems() {
    final List<String> problems = scenarioContext.getSituations().stream()
        .filter(v -> v.getSources().isEmpty())
        .map(v -> M.messages().notificationCalculationNotStartedNoSources(v.getName()))
        .collect(Collectors.toCollection(() -> new ArrayList<>()));

    if (calculationContext.hasActiveCalculation()
        && !calculationContext.getActiveCalculation().isCalculationFinalState()) {
      problems.add(M.messages().notificationCalculationNotStartedAlreadyRunning());
    }

    return problems;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
