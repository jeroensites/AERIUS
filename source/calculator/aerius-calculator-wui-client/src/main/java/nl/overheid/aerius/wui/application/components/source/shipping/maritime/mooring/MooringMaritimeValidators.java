/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.shipping.maritime.mooring;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidationOptions;
import nl.aerius.vuelidate.Validations;
import nl.aerius.vuelidate.ValidatorBuilder;

/**
 * Validators for mooring maritime editing.
 */
class MooringMaritimeValidators extends ValidationOptions<MooringMaritimeShippingDetailEditorStandardComponent> {
  private static final int ZERO_PERCENT = 0;
  private static final int HUNDRED_PERCENT = 100;

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  static class MaritimeMovementValidations extends Validations {
    public @JsProperty Validations descriptionV;
    public @JsProperty Validations shipsPerTimeUnitV;
    public @JsProperty Validations averageResidenceTimeV;
    public @JsProperty Validations shorePowerFactorV;
    public @JsProperty Validations shipCodeV;
  }

  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final MooringMaritimeShippingDetailEditorStandardComponent instance = Js.uncheckedCast(options);

    v.install("descriptionV", () -> instance.descriptionV = null,
        ValidatorBuilder.create()
        .required());
    v.install("shipCodeV", () -> instance.shipCodeV = null,
        ValidatorBuilder.create()
        .required());
    v.install("shipsPerTimeUnitV", () -> instance.shipsPerTimeUnitV = null,
        ValidatorBuilder.create()
        .required()
        .integer()
        .minValue(0));
    v.install("averageResidenceTimeV", () -> instance.averageResidenceTimeV = null,
        ValidatorBuilder.create()
        .required()
        .integer()
        .minValue(0));
    v.install("shorePowerFactorV", () -> instance.shorePowerFactorV = null,
        ValidatorBuilder.create()
        .required()
        .decimal()
        .minValue(ZERO_PERCENT)
        .maxValue(HUNDRED_PERCENT));
  }
}
