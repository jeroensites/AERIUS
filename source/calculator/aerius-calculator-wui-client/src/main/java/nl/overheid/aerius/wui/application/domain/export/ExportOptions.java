/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.domain.export;

import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.wui.application.util.json.JsonSerializable;

@JsType
public class ExportOptions implements JsonSerializable {

  private @JsProperty String jobType = JobType.REPORT.name();
  private @JsProperty String email;
  private @JsProperty String exportType;

  public static Builder builder() {
    return new Builder();
  }

  public JobType getJobType() {
    return JobType.valueOf(jobType);
  }

  public void setJobType(JobType jobType) {
    this.jobType = jobType.name();
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public ExportType getExportType() {
    return ExportType.valueOf(exportType);
  }

  public void setExportType(ExportType exportType) {
    this.exportType = exportType.name();
  }

  public static class Builder {
    private final ExportOptions exportOptions;

    public Builder() {
      exportOptions = new ExportOptions();
      exportOptions.setJobType(JobType.CALCULATION);
      exportOptions.setExportType(ExportType.CALCULATION_UI);
    }

    public Builder jobType(JobType jobType) {
      exportOptions.setJobType(jobType);
      return this;
    }

    public Builder email(String email) {
      exportOptions.setEmail(email);
      return this;
    }

    public Builder exportType(ExportType exportType) {
      exportOptions.setExportType(exportType);
      return this;
    }

    public ExportOptions build() {
      return exportOptions;
    }
  }
}
