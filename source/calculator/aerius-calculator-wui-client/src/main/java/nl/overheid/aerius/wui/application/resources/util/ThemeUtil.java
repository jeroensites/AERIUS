/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.resources.util;

import com.google.gwt.resources.client.DataResource;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.resources.R;

public class ThemeUtil {

  /**
   * Returns the icon for the given theme.
   *
   * @param theme
   * @return icon data resource
   */
  public static String getIconClass(final Theme theme) {
    switch (theme) {
    case RBL:
      return "icon-theme-particulate-matter";
    case WNB:
      return "icon-theme-nitrogen-deposition";
    default:
      return null;
    }
  }

  /**
   * Returns the icon for the given theme.
   *
   * @param theme
   * @return icon data resource
   */
  public static DataResource getIcon(final Theme theme) {
    switch (theme) {
    case RBL:
      return R.images().themeAirQuality();
    case WNB:
      return R.images().themeNitrogenDeposition();
    default:
      return null;
    }
  }
}
