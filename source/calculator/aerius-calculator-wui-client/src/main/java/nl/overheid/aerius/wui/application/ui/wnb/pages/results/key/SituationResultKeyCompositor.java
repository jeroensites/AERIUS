/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.results.key;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculateContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.SituationHandle;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    SimplifiedListBoxComponent.class
})
public class SituationResultKeyCompositor extends BasicVueComponent implements HasMounted {
  @Prop ResultSummaryContext resultContext;

  @Data @Inject CalculationContext calculationContext;
  @Data @Inject CalculateContext calculateContext;
  @Data @Inject ScenarioContext scenarioContext;
  @Data @Inject ApplicationContext applicationContext;

  @Computed
  public List<ScenarioResultType> getResultTypes() {
    return ScenarioResultType.getResultTypesForSituationType(resultContext.getSituationHandle().getType());
  }

  @Watch(value = "getResultTypes()", isImmediate = true)
  public void onResultTypeChange(final List<ScenarioResultType> types) {
    // If the list of available types changes, set an appropriate result type
    if (!types.contains(resultContext.getResultType())) {
      types.stream().findFirst().ifPresent(v -> selectResultType(v));
    }
  }

  @JsMethod
  public List<EmissionResultKey> getEmissionResultKeys() {
    return applicationContext.getActiveThemeConfiguration().getEmissionResultKeys();
  }

  @JsMethod
  public SummaryHexagonType[] getHexagonTypes() {
    final List<SummaryHexagonType> hexagonTypes = new ArrayList<>();

    if (calculateContext.getCalculationOptions().getCalculationType() == CalculationType.PERMIT) {
      hexagonTypes.add(SummaryHexagonType.EXCEEDING_HEXAGONS);
      hexagonTypes.add(SummaryHexagonType.RELEVANT_HEXAGONS);
    }

    if (!scenarioContext.getCalculationPoints().isEmpty()) {
      hexagonTypes.add(SummaryHexagonType.CUSTOM_CALCULATION_POINTS);
    }

    return hexagonTypes.toArray(new SummaryHexagonType[0]);
  }

  @JsMethod
  public void selectSituation(final SituationHandle option) {
    final SituationResultsKey copy = resultContext.getSituationResultsKey().copy();
    copy.setSituationHandle(option);
    selectKey(copy);
  }

  @JsMethod
  public void selectResultType(final ScenarioResultType option) {
    final SituationResultsKey copy = resultContext.getSituationResultsKey().copy();
    copy.setResultType(option);
    selectKey(copy);
  }

  @JsMethod
  public void selectEmissionResultKey(final EmissionResultKey option) {
    final SituationResultsKey copy = resultContext.getSituationResultsKey().copy();
    copy.setEmissionResultKey(option);
    selectKey(copy);
  }

  @JsMethod
  public void selectHexagonType(final SummaryHexagonType option) {
    final SituationResultsKey copy = resultContext.getSituationResultsKey().copy();
    copy.setHexagonType(option);
    selectKey(copy);
  }

  private void selectKey(final SituationResultsKey key) {
    vue().$emit("select", key);
  }

  @Override
  public void mounted() {
    final SituationResultsKey copy = resultContext.getSituationResultsKey().copy();
    copy.setHexagonType(Arrays.stream(getHexagonTypes()).findFirst().orElse(SummaryHexagonType.EXCEEDING_HEXAGONS));
    selectKey(copy);
  }
}
