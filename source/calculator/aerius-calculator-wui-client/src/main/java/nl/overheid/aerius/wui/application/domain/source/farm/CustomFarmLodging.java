/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farm;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.domain.source.EmissionFactors;

/**
 * Client side implementation of customFarmLodgings props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class CustomFarmLodging extends FarmLodging {

  private String description;
  private EmissionFactors emissionFactors;
  private String animalCode;

  public static final @JsOverlay CustomFarmLodging create() {
    final CustomFarmLodging props = new CustomFarmLodging();
    FarmLodging.init(props, FarmLodgingType.CUSTOM);
    props.setDescription("");
    props.setAnimalCode("");
    props.setEmissionFactors(EmissionFactors.create());
    return props;
  }

  public final @JsOverlay String getDescription() {
    return description;
  }

  public final @JsOverlay void setDescription(final String description) {
    this.description = description;
  }

  public final @JsOverlay double getEmissionFactor(final Substance substance) {
    return emissionFactors.getEmissionFactor(substance);
  }

  public final @JsOverlay void setEmissionFactor(final Substance substance, final double emission) {
    emissionFactors.setEmissionFactor(substance, emission);
  }

  public final @JsOverlay void setEmissionFactors(final EmissionFactors emissionFactors) {
    this.emissionFactors = emissionFactors;
  }

  private final @JsOverlay EmissionFactors emissionFactors() {
    return emissionFactors;
  }

  public final @JsOverlay String getAnimalCode() {
    return animalCode;
  }

  public final @JsOverlay void setAnimalCode(String animalCode) {
    this.animalCode = animalCode;
  }

}
