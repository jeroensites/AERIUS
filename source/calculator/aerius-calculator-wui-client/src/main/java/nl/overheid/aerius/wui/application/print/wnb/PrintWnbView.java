/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.wnb;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.wui.application.components.modal.notification.NotificationComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.PrintWnbContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.print.PrintCompleteIndicator;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    PrintCompleteIndicator.class,
    ProjectCalculationCoverPage.class,
    Overview.class,
    SituationSummaries.class,
    Results.class,
    EmissionSources.class,
    NotificationComponent.class
})
public class PrintWnbView extends BasicVueView {
  @Prop PrintWnbActivity presenter;

  @Prop EventBus eventBus;

  @Inject ApplicationContext applicationContext;
  @Inject EnvironmentConfiguration cfg;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data PrintWnbContext context;

  @Computed("hasSituation")
  public boolean hasSituation() {
    return scenarioContext.hasActiveSituation();
  }

  @Computed
  public SituationContext getSituation() {
    return scenarioContext.getActiveSituation();
  }

  @Computed
  public List<EmissionSourceFeature> getAllEmissionSources() {
    return Optional.ofNullable(getSituation())
        .map(v -> v.getSources())
        .orElse(new ArrayList<>());
  }

  @Computed
  public List<EmissionSourceFeature> getEmissionSources() {
    return getAllEmissionSources().stream()
        .filter(v -> !RoadEmissionSourceTypes.isRoadSource(v))
        .collect(Collectors.toList());
  }

  @Computed
  public String getReleaseDetailsUrl() {
    return applicationContext.getConfiguration().getSettings().getSetting(SharedConstantsEnum.RELEASE_DETAILS_URL);
  }

  @Computed
  public String getAeriusVersion() {
    return cfg.getApplicationVersion();
  }

  @Computed
  public String getDatabaseVersion() {
    return applicationContext.getConfiguration().getSettings().getSetting(SharedConstantsEnum.DATABASE_VERSION);
  }
}
