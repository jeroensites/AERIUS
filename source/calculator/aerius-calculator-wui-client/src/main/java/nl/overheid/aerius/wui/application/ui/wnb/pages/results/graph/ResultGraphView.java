/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.results.graph;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import elemental2.core.Global;
import elemental2.dom.CustomEvent;

import jsinterop.annotations.JsMethod;
import jsinterop.base.Js;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.daemon.calculation.ClearAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.daemon.calculation.DeselectResultGraphRangeCommand;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSelectionContext;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSummaryContext;
import nl.overheid.aerius.wui.application.daemon.calculation.SelectAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.daemon.calculation.SelectResultGraphRangeCommand;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsAreaSummary;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    CollapsiblePanel.class,
    ButtonIcon.class,
    VerticalCollapseGroup.class
})
public class ResultGraphView extends BasicVueComponent {
  @Prop EventBus eventBus;
  @Prop(required = true) ResultSummaryContext context;

  @Data @Inject ResultSelectionContext selectionContext;

  @JsMethod
  public void openPanel(final SituationResultsAreaSummary summary) {
    final int id = summary.getAssessmentArea().getId();
    eventBus.fireEvent(new SelectAssessmentAreaCommand(id));
  }

  @JsMethod
  public void closePanel() {
    eventBus.fireEvent(new ClearAssessmentAreaCommand());
  }

  @JsMethod
  public boolean isOpen(final int id) {
    return id == selectionContext.getSelectedAssessmentArea();
  }

  @JsMethod
  public String getDistributionData(final SituationResultsAreaSummary d) {
    return Global.JSON.stringify(d.getChartResults());
  }

  @JsMethod
  public void selectBar(final CustomEvent input) {
    eventBus.fireEvent(new SelectResultGraphRangeCommand(Js.uncheckedCast(input.detail)));
  }

  @JsMethod
  public void deselectBar(final CustomEvent input) {
    eventBus.fireEvent(new DeselectResultGraphRangeCommand(Js.uncheckedCast(input.detail)));
  }

  @JsMethod
  public void zoomToArea(final SituationResultsAreaSummary areaSummary) {
    eventBus.fireEvent(GeoUtil.createMapSetExtendCommand(areaSummary.getAssessmentArea().getBounds()));
  }
}
