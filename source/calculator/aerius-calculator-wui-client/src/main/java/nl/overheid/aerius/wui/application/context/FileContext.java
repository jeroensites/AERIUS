/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.js.exception.AeriusJsExceptionMessage;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

/**
 * Simple state object for pending files in the given scenario and situation.
 */
@Singleton
public class FileContext {
  @JsProperty private final List<FileUploadStatus> files = new ArrayList<>();

  @JsProperty private List<AeriusJsExceptionMessage> errors = new ArrayList<>();
  @JsProperty private List<AeriusJsExceptionMessage> warnings = new ArrayList<>();

  public Optional<FileUploadStatus> findFile(final String uuid) {
    return files.stream()
        .filter(v -> uuid.equals(v.getFileCode()))
        .findFirst();
  }

  public List<FileUploadStatus> getFiles() {
    return files;
  }

  public List<AeriusJsExceptionMessage> getErrors() {
    return errors;
  }

  public List<AeriusJsExceptionMessage> getWarnings() {
    return warnings;
  }

  public void add(final FileUploadStatus file) {
    files.add(file);
  }

  public void remove(final FileUploadStatus file) {
    files.remove(file);
  }

  public void clearFiles() {
    files.clear();
  }

  public void setErrors(final List<AeriusJsExceptionMessage> errors) {
    this.errors = errors;
  }

  public void setWarnings(final List<AeriusJsExceptionMessage> warnings) {
    this.warnings = warnings;
  }
}
