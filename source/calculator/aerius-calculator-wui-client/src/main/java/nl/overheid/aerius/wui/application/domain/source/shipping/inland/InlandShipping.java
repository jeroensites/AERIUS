/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.shipping.inland;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.application.domain.source.shipping.base.AbstractShipping;

/**
 * Client side implementation of Inland Shipping.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public abstract class InlandShipping extends AbstractShipping {

  private String inlandShippingType;
  private String timeUnitAtoB;
  private String timeUnitBtoA;
  private int movementsAtoBPerTimeUnit;
  private int movementsBtoAPerTimeUnit;
  private int percentageLadenAtoB;
  private int percentageLadenBtoA;

   public static final @JsOverlay void init(final InlandShipping props, final InlandShippingType type) {
     AbstractSubSource.init(props);
     props.setDescription("");
     props.setTimeUnitAtoB(TimeUnit.YEAR);
     props.setTimeUnitBtoA(TimeUnit.YEAR);
     props.setMovementsAtoBPerTimeUnit(0);
     props.setMovementsBtoAPerTimeUnit(0);
     props.setPercentageLadenAtoB(0);
     props.setPercentageLadenBtoA(0);
     props.setInlandShippingType(type);
   }

   public final @JsOverlay InlandShippingType getInlandShippingType() {
     return InlandShippingType.safeValueOf(inlandShippingType);
   }

   public final @JsOverlay void setInlandShippingType(final InlandShippingType type) {
     this.inlandShippingType = type.name();
   }

  public final @JsOverlay TimeUnit getTimeUnitAtoB() {
    return TimeUnit.valueOf(timeUnitAtoB);
  }

  public final @JsOverlay void setTimeUnitAtoB(final TimeUnit timeUnitAtoB) {
    this.timeUnitAtoB = timeUnitAtoB.name();
  }

  public final @JsOverlay TimeUnit getTimeUnitBtoA() {
    return TimeUnit.valueOf(timeUnitBtoA);
  }

  public final @JsOverlay void setTimeUnitBtoA(final TimeUnit timeUnitBtoA) {
    this.timeUnitBtoA = timeUnitBtoA.name();
  }

  public final @JsOverlay int getMovementsAtoBPerTimeUnit() {
    return movementsAtoBPerTimeUnit;
  }

  public final @JsOverlay void setMovementsAtoBPerTimeUnit(final int movementsAtoBPerTimeUnit) {
    this.movementsAtoBPerTimeUnit = movementsAtoBPerTimeUnit;
  }

  public final @JsOverlay int getMovementsBtoAPerTimeUnit() {
    return movementsBtoAPerTimeUnit;
  }

  public final @JsOverlay void setMovementsBtoAPerTimeUnit(final int movementsBtoAPerTimeUnit) {
    this.movementsBtoAPerTimeUnit = movementsBtoAPerTimeUnit;
  }

  public final @JsOverlay int getPercentageLadenAtoB() {
    return percentageLadenAtoB;
  }

  public final @JsOverlay void setPercentageLadenAtoB(final int percentageLadenAtoB) {
    this.percentageLadenAtoB = percentageLadenAtoB;
  }

  public final @JsOverlay int getPercentageLadenBtoA() {
    return percentageLadenBtoA;
  }

  public final @JsOverlay void setPercentageLadenBtoA(final int percentageLadenBtoA) {
    this.percentageLadenBtoA = percentageLadenBtoA;
  }

}
