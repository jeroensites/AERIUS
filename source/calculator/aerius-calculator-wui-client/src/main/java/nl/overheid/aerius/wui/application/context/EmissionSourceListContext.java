/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.Optional;

import javax.inject.Singleton;

import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

@Singleton
public class EmissionSourceListContext {
  private EmissionSourceFeature selection = null;
  private EmissionSourceFeature peekSource = null;

  public EmissionSourceFeature getLooseSelection() {
    return Optional.ofNullable(selection)
        .orElse(peekSource);
  }

  public EmissionSourceFeature getSelection() {
    return selection;
  }

  public void setSelection(final EmissionSourceFeature selection) {
    this.selection = selection;
  }

  public boolean hasLooseSelection() {
    return getLooseSelection() != null;
  }

  public boolean hasSelection() {
    return selection != null;
  }

  public void reset() {
    // Note: If expanding with more resets, make sure assumptions are still met
    selection = null;
    peekSource = null;
  }

  public boolean isSelected(final EmissionSourceFeature source) {
    if (source == null || selection == null) {
      return false;
    }

    return selection.getId().equals(source.getId());
  }

  public void setPeekSource(final EmissionSourceFeature peekSource) {
    this.peekSource = peekSource;
  }

  public void clearPeekSource() {
    this.peekSource = null;
  }
}
