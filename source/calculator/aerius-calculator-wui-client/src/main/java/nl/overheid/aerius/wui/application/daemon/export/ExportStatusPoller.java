/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.export;

import javax.inject.Inject;

import com.google.gwt.user.client.Timer;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.event.HasEventBus;
import nl.aerius.wui.util.NotificationUtil;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.application.event.ExportCalculationCompleteEvent;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.ExportServiceAsync;

public class ExportStatusPoller extends BasicEventComponent implements HasEventBus {
  private static final int INTERVAL = 1000;

  @Inject ExportServiceAsync service;

  private final String calculationKey;

  private boolean running;
  private final Timer timer = new Timer() {
    @Override
    public void run() {
      doFetch();
    }
  };

  @Inject
  public ExportStatusPoller(final @Assisted EventBus eventBus, final @Assisted String exportCode) {
    this.calculationKey = exportCode;

    setEventBus(eventBus);
  }

  public void start() {
    if (running) {
      return;
    }

    running = true;
    SchedulerUtil.delay(this::doFetch);
  }

  public boolean isRunning() {
    return running;
  }

  private void doFetch() {
    service.retrieveExportStatus(calculationKey, AeriusRequestCallback.create(v -> {
      if (!v.getJobProgress().getState().isFinalState()) {
        timer.schedule(INTERVAL);
      } else {
        eventBus.fireEvent(new ExportCalculationCompleteEvent(v));
      }

    }, e -> NotificationUtil.broadcastError(eventBus, e)));
  }
}
