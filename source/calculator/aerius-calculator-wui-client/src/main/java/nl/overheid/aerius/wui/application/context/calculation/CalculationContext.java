/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.calculation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;

@Singleton
public class CalculationContext {
  private String lastKnownCalculationId;
  private String activeInternalCalculationId = null;
  private @JsProperty final List<CalculationJobContext> calculations = new ArrayList<>();

  public CalculationJobContext getActiveCalculation() {
    return getCalculation(getActiveInternalCalculationId());
  }

  /**
   * The active calculation /id/, NOTE, this is the _internal id_ and /not/ the
   * calculationCode
   */
  public String getActiveInternalCalculationId() {
    return activeInternalCalculationId;
  }

  /**
   * Return the active calculation code, NOTE, this value is not guaranteed to be
   * set.
   */
  public String getActiveCalculationCode() {
    return Optional.ofNullable(getCalculation(activeInternalCalculationId))
        .map(v -> v.getCalculationCode())
        .orElse(null);
  }

  public boolean hasActiveCalculation() {
    return activeInternalCalculationId != null;
  }

  public CalculationJobContext getCalculation(final String internalId) {
    for (final CalculationJobContext calculation : calculations) {
      if (calculation.getInternalId().equals(internalId)) {
        return calculation;
      }
    }

    return null;
  }

  public void addCalculation(final int idx, final CalculationJobContext calculationContext) {
    calculations.add(idx, calculationContext);
  }

  public List<CalculationJobContext> getCalculations() {
    return calculations;
  }

  public ConsecutiveIdUtil createConsecutiveCalculationIdUtil() {
    return new ConsecutiveIdUtil(
        i -> Integer.toString(i + 1),
        i -> calculations.get(i).getCalculationCode(),
        () -> calculations.size());
  }

  public void setActiveCalculation(final CalculationJobContext calculation) {
    activeInternalCalculationId = calculation.getInternalId();
    lastKnownCalculationId = activeInternalCalculationId;
  }

  public void reset() {
    activeInternalCalculationId = null;
  }

  public String getLastKnownJobKey() {
    final CalculationJobContext context = getCalculation(lastKnownCalculationId);

    return context == null ? null : context.getCalculationCode();
  }

  public boolean isActiveCalculation(final String calculationCode) {
    return Optional.ofNullable(getActiveCalculation())
        .map(v -> v.getCalculationCode())
        .map(v -> calculationCode.equals(v))
        .orElse(false);
  }
}
