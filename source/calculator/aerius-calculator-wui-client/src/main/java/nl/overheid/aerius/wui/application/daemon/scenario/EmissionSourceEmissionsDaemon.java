/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.core.JsArray;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.GWTAtomicInteger;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.context.EmissionSourceEmissionsContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSubSourceFeature;
import nl.overheid.aerius.wui.application.event.situation.SituationChangedYearEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceAddedEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceChangeEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceUpdatedEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.SourceServiceAsync;

/**
 * Daemon in charge of keeping emissions for emission sources up to date.
 */
public class EmissionSourceEmissionsDaemon extends BasicEventComponent {
  private static final EmissionSourceEmissionsDaemonEventBinder EVENT_BINDER = GWT.create(EmissionSourceEmissionsDaemonEventBinder.class);

  interface EmissionSourceEmissionsDaemonEventBinder extends EventBinder<EmissionSourceEmissionsDaemon> {
  }

  private static final List<EmissionSourceType> YEAR_DEPENDANT_TYPES = Arrays.asList(
      EmissionSourceType.SRM1_ROAD, EmissionSourceType.SRM2_ROAD,
      EmissionSourceType.SHIPPING_INLAND, EmissionSourceType.SHIPPING_INLAND_DOCKED,
      EmissionSourceType.SHIPPING_MARITIME_INLAND, EmissionSourceType.SHIPPING_MARITIME_MARITIME, EmissionSourceType.SHIPPING_MARITIME_DOCKED);

  @Inject EmissionSourceEmissionsContext emissionsContext;
  @Inject ScenarioContext scenarioContext;
  @Inject SourceServiceAsync sourceServiceAsync;

  private final GWTAtomicInteger serviceCallCounter = new GWTAtomicInteger();
  private final GWTAtomicInteger serviceResultCounter = new GWTAtomicInteger();

  @EventHandler
  public void onEmissionSourceAddedEvent(final EmissionSourceAddedEvent event) {
    handleSourceChangeEvent(event);
  }

  @EventHandler
  public void onEmissionSourceUpdatedEvent(final EmissionSourceUpdatedEvent event) {
    handleSourceChangeEvent(event);
  }

  @EventHandler
  public void onYearChangedEvent(final SituationChangedYearEvent event) {
    final SituationContext situation = event.getValue();
    final List<EmissionSourceFeature> updateableSources = situation.getSources().stream()
        .filter(EmissionSourceEmissionsDaemon::hasUpdatableYearEmissions)
        .collect(Collectors.toList());
    if (!updateableSources.isEmpty()) {
      fetchUpdatedEmissions(updateableSources, situation.getYear());
    }
  }

  private void handleSourceChangeEvent(final EmissionSourceChangeEvent event) {
    final SituationContext situation = scenarioContext.getSituation(event.getSituationId());
    final EmissionSourceFeature source = event.getValue();
    if (source != null && situation != null && hasUpdatableEmissions(source)) {
      fetchUpdatedEmissions(Arrays.asList(source), situation.getYear());
    }
  }

  private static boolean hasUpdatableEmissions(final EmissionSourceFeature source) {
    /*
     * For now update all non-generic emissions.
     *
     * For some other source types it might be possible to do the update in frontend,
     * but that should still be handled in this daemon. That way, there is one spot to update emissions.
     *
     * Even in simple cases I would keep calling the server to update emissions however:
     * If it's a simple calculation, it'll be a fast response, and it won't impact the server/user too much.
     * And if it's complex, implementing the emission calculation in a second location (frontend)
     * can cause slight differences, probably needs quite some data and needs to be maintained...
     */
    return source.getEmissionSourceType() != EmissionSourceType.GENERIC;
  }

  private static boolean hasUpdatableYearEmissions(final EmissionSourceFeature source) {
    return YEAR_DEPENDANT_TYPES.contains(source.getEmissionSourceType());
  }

  private void fetchUpdatedEmissions(final List<EmissionSourceFeature> sources, final String year) {
    serviceCallCounter.incrementAndGet();
    emissionsContext.setLoading(true);
    sourceServiceAsync.refreshEmissions(sources, year, AeriusRequestCallback.create(
        results -> updateEmissions(sources, results),
        this::exceptionHandling));
  }

  private void updateEmissions(final List<EmissionSourceFeature> existingSources, final List<EmissionSourceFeature> updatedSources) {
    if (updatedSources.size() == existingSources.size()) {
      for (int i = 0; i < existingSources.size(); i++) {
        updateEmissionsSource(existingSources.get(i), updatedSources.get(i));
      }
    } else {
      NotificationUtil.broadcastError(eventBus, M.messages().notificationSourcesUpdatedBeforeEmissionsCalculated());
    }
    handledServiceCall();
  }

  private void updateEmissionsSource(final EmissionSourceFeature existingSource, final EmissionSourceFeature updatedSource) {
    if (existingSource != null && updatedSource != null && existingSource.getId().equals(updatedSource.getId())) {
      // For now update the emissions of the existing source, just to check if that works.
      // The entire object could be replaced I guess, but this seems just as secure.
      existingSource.setEmissions(updatedSource.getEmissions());
      // Update subsources for those sources that have those (currently all but generic)
      if (existingSource.getEmissionSourceType() != EmissionSourceType.GENERIC) {
        updateEmissionsForSubSources(
            ((EmissionSubSourceFeature<?>) existingSource).getSubSources(),
            ((EmissionSubSourceFeature<?>) updatedSource).getSubSources(),
            updatedSource.getLabel());
      }
    }
  }

  private void updateEmissionsForSubSources(final JsArray<? extends AbstractSubSource> existingSubSources,
      final JsArray<? extends AbstractSubSource> updatedSubSources, final String sourceLabel) {
    if (existingSubSources == null || updatedSubSources == null) {
      return;
    }
    if (existingSubSources.length != updatedSubSources.length) {
      NotificationUtil.broadcastError(eventBus, M.messages().notificationSourceUpdatedBeforeEmissionsCalculated(sourceLabel));
      return;
    }
    for (int i = 0; i < existingSubSources.length; i++) {
      existingSubSources.getAt(i).setEmissions(updatedSubSources.getAt(i).getEmissions());
    }
  }

  private void exceptionHandling(final Throwable e) {
    NotificationUtil.broadcastError(eventBus, e);
    handledServiceCall();
  }

  private void handledServiceCall() {
    final int count = serviceResultCounter.incrementAndGet();
    if (count == serviceCallCounter.get()) {
      emissionsContext.setLoading(false);
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
