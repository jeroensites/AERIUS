/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.google.gwt.dom.client.Element;

import elemental2.dom.DomGlobal;
import elemental2.dom.Event;
import elemental2.dom.EventTarget;
import elemental2.dom.KeyboardEvent;

import jsinterop.base.Js;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.easter.TetrisGame;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.Place;
import nl.overheid.aerius.geo.command.MapPanDownCommand;
import nl.overheid.aerius.geo.command.MapPanLeftCommand;
import nl.overheid.aerius.geo.command.MapPanRightCommand;
import nl.overheid.aerius.geo.command.MapPanUpCommand;
import nl.overheid.aerius.geo.command.MapZoomInCommand;
import nl.overheid.aerius.geo.command.MapZoomOutCommand;
import nl.overheid.aerius.wui.application.command.InfoPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.LayerPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.MeasureToggleCommand;
import nl.overheid.aerius.wui.application.command.SearchPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.ToggleHotkeysCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingCreateNewCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDuplicateSelectedCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditNewCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.RemoveCalculationPointCommand;
import nl.overheid.aerius.wui.application.command.misc.ToggleNotificationDisplayCommand;
import nl.overheid.aerius.wui.application.command.situation.CreateEmptySituationCommand;
import nl.overheid.aerius.wui.application.command.situation.SwitchSituationCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceCreateFirstCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceCreateNewCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDeleteSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDuplicateSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSelectedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceFeatureToggleSelectCommand;
import nl.overheid.aerius.wui.application.command.source.FeatureZoomCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.BuildingListContext;
import nl.overheid.aerius.wui.application.context.CalculationPointEditorContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceListContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.LeftPanelToggleCommand;
import nl.overheid.aerius.wui.application.place.ApplicationPlaceController;
import nl.overheid.aerius.wui.application.place.CalculatePlace;
import nl.overheid.aerius.wui.application.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourceListPlace;
import nl.overheid.aerius.wui.application.place.ExportPlace;
import nl.overheid.aerius.wui.application.place.ResultsPlace;
import nl.overheid.aerius.wui.application.ui.wnb.pages.calculationpoints.CalculationPointToggleSelectFeatureCommand;
import nl.overheid.aerius.wui.application.ui.wnb.pages.calculationpoints.FetchCalculationPointSelectionEvent;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.ScenarioInputListContext;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.ScenarioInputListContext.ViewMode;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.building.BuildingToggleSelectFeatureCommand;

public class HotKeyDaemon extends BasicEventComponent implements Daemon {
  private static final String CREATION_MODE_KEY = "KeyC";
  private static final String SITUATION_MODE_KEY = "KeyQ";

  @Inject ApplicationContext appContext;
  @Inject ScenarioContext scenarioContext;
  @Inject ApplicationPlaceController placeController;

  @Inject ScenarioInputListContext inputListContext;
  @Inject EmissionSourceListContext emissionSourceListContext;
  @Inject BuildingListContext buildingListContext;

  @Inject CalculationPointEditorContext calculationPointsEditorContext;

  private boolean situationMode;
  private boolean creationMode;

  @Override
  public void init() {
    DomGlobal.document.addEventListener("keydown", e -> handleKeyDown(e));
    DomGlobal.document.addEventListener("keyup", e -> handleKeyUp(e));
  }

  private void handleKeyDown(final Event e) {
    if (e instanceof KeyboardEvent) {
      handleKeyDown((KeyboardEvent) e);
    }
  }

  private void handleKeyUp(final Event e) {
    if (e instanceof KeyboardEvent) {
      handleKeyUp((KeyboardEvent) e);
    }
  }

  private void handleKeyDown(final KeyboardEvent e) {
    // Don't handle key if easter is active
    if (TetrisGame.isPlaying()) {
      return;
    }

    // Check ctrl+* shortcuts before input
    final boolean ctrl = e.ctrlKey;
    switch (e.code) {
    case "KeyK": // Also allow ctrl+K for search (undocumented)
      if (ctrl) {
        eventBus.fireEvent(new SearchPopupToggleCommand());
        e.preventDefault(); // Prevent default browser shortcut (also search)
      }
      break;
    }

    // Cancel if input element has focus
    final boolean isInput = isInput(e.target);
    if (isInput) {
      return;
    }

    switch (e.key) {
    case "+":
      // Zoom in if ctrl is not pressed
      if (!ctrl) {
        eventBus.fireEvent(new MapZoomInCommand());
      }
      break;
    case "-":
      // Zoom out if ctrl is not pressed
      if (!ctrl) {
        eventBus.fireEvent(new MapZoomOutCommand());
      }
      break;
    case "ArrowRight":
      eventBus.fireEvent(new MapPanRightCommand());
      break;
    case "ArrowLeft":
      eventBus.fireEvent(new MapPanLeftCommand());
      break;
    case "ArrowUp":
      eventBus.fireEvent(new MapPanUpCommand());
      break;
    case "ArrowDown":
      eventBus.fireEvent(new MapPanDownCommand());
      break;
    case "?":
      eventBus.fireEvent(new ToggleHotkeysCommand());
      break;
    }
  }

  private void handleKeyUp(final KeyboardEvent e) {
    // Don't handle key if easter is active
    if (TetrisGame.isPlaying()) {
      return;
    }

    final boolean isInput = isInput(e.target);

    // Hotkeys that should always work (regardless of input)
    handlePriorityKeys(e);

    if (isInput) {
      return;
    }

    // Hotkeys that should not work if within an input paradigm
    handleSecondaryKeys(e);

    if (creationMode && !CREATION_MODE_KEY.equals(e.code)) {
      creationMode = false;
    }
    if (situationMode && !SITUATION_MODE_KEY.equals(e.code)) {
      situationMode = false;
    }
  }

  private void handleSecondaryKeys(final KeyboardEvent e) {
    final boolean shift = e.shiftKey;

    switch (e.code) {
    case "KeyN":
      eventBus.fireEvent(new ToggleNotificationDisplayCommand());
      break;
    case "KeyL":
      eventBus.fireEvent(new LayerPopupToggleCommand());
      break;
    case "KeyI":
      eventBus.fireEvent(new InfoPopupToggleCommand());
      break;
    case "KeyS":
      if (creationMode) {
        eventBus.fireEvent(new CreateEmptySituationCommand(true));
      } else {
        eventBus.fireEvent(new SearchPopupToggleCommand());
      }
      break;
    case "KeyM":
      eventBus.fireEvent(new MeasureToggleCommand());
      break;
    case "KeyF":
      eventBus.fireEvent(new LeftPanelToggleCommand());
      break;
    case "KeyH":
      eventBus.fireEvent(new ToggleHotkeysCommand());
      break;
    case SITUATION_MODE_KEY:
      situationMode = true;
      break;
    case CREATION_MODE_KEY:
      if (creationMode) {
        doInCurrentInput(
            () -> eventBus.fireEvent(new EmissionSourceCreateNewCommand()),
            () -> eventBus.fireEvent(new BuildingCreateNewCommand()),
            () -> eventBus.fireEvent(new CalculationPointEditNewCommand()));
        creationMode = false;
      } else {
        creationMode = true;
      }
      break;
    case "KeyD":
      doWithSelectedFeature(
          v -> eventBus.fireEvent(new EmissionSourceDeleteSelectedCommand()),
          v -> eventBus.fireEvent(new BuildingDeleteSelectedCommand()),
          v -> eventBus.fireEvent(new RemoveCalculationPointCommand(v)));
      break;
    case "KeyV":
      doWithSelectedFeature(
          v -> eventBus.fireEvent(new EmissionSourceDuplicateSelectedCommand()),
          v -> eventBus.fireEvent(new BuildingDuplicateSelectedCommand()),
          v -> {});
      break;
    case "KeyE":
      if (creationMode) {
        if (ScenarioContext.hasAnySource(scenarioContext)) {
          eventBus.fireEvent(new EmissionSourceCreateNewCommand());
        } else {
          eventBus.fireEvent(new EmissionSourceCreateFirstCommand());
        }
      } else {
        doWithSelectedFeature(
            v -> eventBus.fireEvent(new EmissionSourceEditSelectedCommand()),
            v -> eventBus.fireEvent(new BuildingEditSelectedCommand()),
            v -> {});
      }
      break;
    case "KeyZ":
      doWithSelectedFeature(
          v -> eventBus.fireEvent(new FeatureZoomCommand(v)),
          v -> eventBus.fireEvent(new FeatureZoomCommand(v)),
          v -> eventBus.fireEvent(new FeatureZoomCommand(v)));
      break;
    case "KeyG": // Also allow G (undocumented)
    case "KeyB":
      if (creationMode) {
        eventBus.fireEvent(new BuildingCreateNewCommand());
      }
      break;
    case "KeyR": // Also allow R (undocumented)
    case "KeyA":
      if (creationMode) {
        eventBus.fireEvent(new CalculationPointEditNewCommand());
      }
      break;
    case "Digit1":
      if (shift) {
        placeController.goTo(new EmissionSourceListPlace(appContext.getActiveTheme()));
      }
      break;
    case "Digit2":
      if (shift) {
        placeController.goTo(new CalculationPointsPlace(appContext.getActiveTheme()));
      }
      break;
    case "Digit3":
      if (shift) {
        placeController.goTo(new CalculatePlace(appContext.getActiveTheme()));
      }
      break;
    case "Digit4":
      if (shift) {
        placeController.goTo(new ResultsPlace(appContext.getActiveTheme()));
      }
      break;
    case "Digit5":
      if (shift) {
        placeController.goTo(new ExportPlace(appContext.getActiveTheme()));
      }
      break;
    }

    switch (e.key) {
    case "1":
    case "2":
    case "3":
    case "4":
    case "5":
    case "6":
    case "7":
    case "8":
    case "9":
      final int num = Integer.parseInt(e.key);
      if (situationMode) {
        selectSituation(num);
      } else {
        selectFeatureNumber(num);
      }
      break;
    }
  }

  private void selectSituation(final int num) {
    final List<SituationContext> situations = scenarioContext.getSituations();
    if (situations.size() >= num) {
      final SituationContext sit = situations.get(num - 1);
      eventBus.fireEvent(new SwitchSituationCommand(sit.getSituationCode()));
    }
  }

  private void doInCurrentInput(final Runnable emissionSourceRunnable, final Runnable buildingRunnable, final Runnable calculationPointRunnable) {
    final Place currentPlace = placeController.getPlace();
    if (currentPlace instanceof EmissionSourceListPlace) {
      if (inputListContext.getViewMode() == ViewMode.BUILDING) {
        Optional.ofNullable(buildingRunnable)
            .ifPresent(v -> v.run());
      } else if (inputListContext.getViewMode() == ViewMode.EMISSION_SOURCES) {
        Optional.ofNullable(emissionSourceRunnable)
            .ifPresent(v -> v.run());
      }
    } else if (currentPlace instanceof CalculationPointsPlace && !calculationPointsEditorContext.isEditing()) {
      Optional.ofNullable(calculationPointRunnable)
          .ifPresent(v -> v.run());
    }
  }

  private void selectFeatureNumber(final int num) {
    final Place currentPlace = placeController.getPlace();
    if (currentPlace instanceof EmissionSourceListPlace) {
      if (inputListContext.getViewMode() == ViewMode.BUILDING) {
        final List<BuildingFeature> buildings = scenarioContext.getActiveSituation().getBuildings();
        if (buildings.size() >= num) {
          final BuildingFeature feature = buildings.get(num - 1);
          eventBus.fireEvent(new BuildingToggleSelectFeatureCommand(feature, false));
        }
      } else {
        final List<EmissionSourceFeature> sources = scenarioContext.getActiveSituation().getSources();
        if (sources.size() >= num) {
          final EmissionSourceFeature feature = sources.get(num - 1);
          eventBus.fireEvent(new EmissionSourceFeatureToggleSelectCommand(feature));
        }
      }
    } else if (currentPlace instanceof CalculationPointsPlace && !calculationPointsEditorContext.isEditing()) {
      final List<CalculationPointFeature> calculationPoints = scenarioContext.getCalculationPoints();
      if (calculationPoints.size() >= num) {
        final CalculationPointFeature feature = calculationPoints.get(num - 1);
        eventBus.fireEvent(new CalculationPointToggleSelectFeatureCommand(feature));
      }
    }
  }

  private void doWithSelectedFeature(final Consumer<EmissionSourceFeature> emissionSourceConsumer,
      final Consumer<BuildingFeature> buildingConsumer,
      final Consumer<CalculationPointFeature> assessmentPointConsumer) {
    final Place currentPlace = placeController.getPlace();
    if (currentPlace instanceof EmissionSourceListPlace) {
      if (inputListContext.getViewMode() == ViewMode.BUILDING) {
        Optional.ofNullable(buildingListContext.getLooseSelection())
            .ifPresent(buildingConsumer);
      } else if (inputListContext.getViewMode() == ViewMode.EMISSION_SOURCES) {
        Optional.ofNullable(emissionSourceListContext.getLooseSelection())
            .ifPresent(emissionSourceConsumer);
      }
    } else if (currentPlace instanceof CalculationPointsPlace && !calculationPointsEditorContext.isEditing()) {
      final FetchCalculationPointSelectionEvent evt = new FetchCalculationPointSelectionEvent();
      eventBus.fireEvent(evt);
      Optional.ofNullable(evt.getValue())
          .ifPresent(assessmentPointConsumer);
    }
  }

  private void handlePriorityKeys(final KeyboardEvent e) {
    final boolean ctrlKey = e.ctrlKey;

    switch (e.code) {
    case "Escape":
      if (ctrlKey) {
        ((Element) Js.cast(e.target)).blur();
      }
      break;
    }
  }

  private boolean isInput(final EventTarget target) {
    final String tag = ((Element) Js.cast(target)).getTagName().toLowerCase();
    return "input".equals(tag) || "textarea".equals(tag) || "select".equals(tag);
  }
}
