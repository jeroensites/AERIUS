/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.place;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import javax.inject.Inject;

import nl.aerius.wui.place.Place;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;

/**
 * A simple wrapper around the configured PlaceController which does stateful
 * context-based checks. Can be used to safely (try to) navigate to a place that
 * is disabled, aswell as to check disabled-status.
 */
public class ApplicationPlaceController implements PlaceController {
  @Inject PlaceController placeController;

  @Inject ScenarioContext scenarioContext;
  @Inject CalculationContext calculationContext;

  private final Map<Class<?>, Supplier<Boolean>> enabledConditions = new HashMap<>();

  public ApplicationPlaceController() {
    enabledConditions.put(EmissionSourceListPlace.class, () -> scenarioContext.hasSituations());
    enabledConditions.put(CalculationPointsPlace.class, () -> scenarioContext.hasSituations());
    enabledConditions.put(CalculatePlace.class, () -> ScenarioContext.hasAnyEmissions(scenarioContext));
    enabledConditions.put(ResultsPlace.class, () -> calculationContext.hasActiveCalculation());
    enabledConditions.put(ExportPlace.class, () -> ScenarioContext.hasAnyContent(scenarioContext));
  }

  @Override
  public Place getPlace() {
    return placeController.getPlace();
  }

  @Override
  public Place getPreviousPlace() {
    return placeController.getPreviousPlace();
  }

  @Override
  public void goTo(final Place place) {
    if (isEnabled(place.getClass())) {
      placeController.goTo(place);
    } else {
      // Don't do anything if not enabled
    }
  }

  @Override
  public void goTo(final Place place, final boolean silent) {
    if (isEnabled(place.getClass())) {
      placeController.goTo(place, silent);
    } else {
      // Don't do anything if not enabled
    }
  }

  public boolean isEnabled(final Class clazz) {
    if (clazz == null) {
      return false;
    }

    return enabledConditions
        .getOrDefault(clazz, () -> true)
        .get();
  }
}
