/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.wnb.pages.calculationpoints;

import com.google.gwt.event.dom.client.DragEnterEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.RootPanel;

import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.command.UserImportCalculationPointsRequestedCommand;
import nl.overheid.aerius.wui.application.components.map.MapComponentFactory;
import nl.overheid.aerius.wui.application.ui.main.MainView.FlexView;
import nl.overheid.aerius.wui.application.ui.pages.ThemeDelegatedActivity;
import nl.overheid.aerius.wui.application.ui.pages.ThemeView;

public class CalculationPointsActivity extends BasicEventComponent implements ThemeDelegatedActivity {

  private HandlerRegistration dragRegister;

  @Override
  public void onStart(final ThemeView view) {
    view.setDelegatedPresenter(this);
    view.setLeftView(CalculationPointsViewFactory.get(), FlexView.RIGHT);

    view.setNoMiddleView();
    view.setRightView(MapComponentFactory.get());
    view.attachMap(() -> view.rightComponent);

    // Note: this way of handling might not be the best because the drag-enter handler in ThemeActivity will also fire.
    // However, since this handler is added after the drag handler from ThemeActivity it will trigger last. Making sure the field
    // ApplicationImportModalComponent.isCalculationPointsImport will be set to true when it is relevant.
    dragRegister = RootPanel.get().addDomHandler(event -> eventBus.fireEvent(new UserImportCalculationPointsRequestedCommand()), DragEnterEvent.getType());
  }


  @Override
  public void onStop() {
    dragRegister.removeHandler();
  }
}
