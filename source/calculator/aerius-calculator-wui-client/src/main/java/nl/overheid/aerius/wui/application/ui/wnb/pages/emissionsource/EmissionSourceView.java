/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsource;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.vue.directives.VectorDirective;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.geo.command.MapResizeSequenceCommand;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCancelCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSaveCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceSectorChangeCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSubSourceDeselectFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSubSourceSelectFeatureCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.feature.FeatureHeading;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyCancelSaveComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.OPSCharacteristicsEditorComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.adms.ADMSCharacteristicsEditorComponent;
import nl.overheid.aerius.wui.application.components.source.farmland.FarmLandEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.FarmLodgingEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.generic.GenericDetailEditorComponent;
import nl.overheid.aerius.wui.application.components.source.offroad.OffRoadMobileEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.road.RoadEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.road.characteristics.SRM2CharacteristicsEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.InlandMooringEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.InlandMooringShippingEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.inland.InlandShippingEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.MaritimeMooringEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.MaritimeShippingEmissionEditorComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.maritime.mooring.MooringMaritimeShippingEmissionEditorComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceValidationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.InlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MooringInlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MooringMaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.place.BuildingPlace;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    FeatureHeading.class,

    LabeledInputComponent.class,
    SimplifiedListBoxComponent.class,
    CollapsiblePanel.class,

    EmissionSourceLocationView.class,

    ADMSCharacteristicsEditorComponent.class,
    OPSCharacteristicsEditorComponent.class,
    SRM2CharacteristicsEditorComponent.class,

    ErrorWarningIconComponent.class,

    GenericDetailEditorComponent.class,
    FarmLodgingEmissionEditorComponent.class,
    FarmLandEmissionEditorComponent.class,
    RoadEmissionEditorComponent.class,
    MaritimeShippingEmissionEditorComponent.class,
    OffRoadMobileEmissionEditorComponent.class,
    InlandMooringEditorComponent.class,
    MaritimeMooringEditorComponent.class,
    InlandShippingEmissionEditorComponent.class,
    InlandMooringShippingEmissionEditorComponent.class,
    MooringMaritimeShippingEmissionEditorComponent.class,

    ModifyCancelSaveComponent.class,
    VerticalCollapse.class,
}, directives = {
    VectorDirective.class,
})
public class EmissionSourceView extends BasicVueView implements HasCreated {
  enum EmissionSourcePanel {
    LOCATION,
    SOURCE_CHARACTERISTICS,
    SHIPPING_DOCK,
    EMISSION_SOURCE_TYPE
  }

  @Prop EmissionSourcePresenter presenter;

  @Prop EventBus eventBus;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data EmissionSourceEditorContext editorContext;
  @Inject @Data EmissionSourceValidationContext validationContext;
  @Inject @Data ScenarioContext scenarioContext;

  @Inject PlaceController placeController;

  @Data EmissionSourcePanel openPanel;

  @Data SectorGroup selectedSectorGroup;

  @Override
  public void created() {
    if (!editorContext.isEditing()) {
      return;
    }

    validationContext.reset();
    openPanel = determineBestOpenPanel();
    selectedSectorGroup = applicationContext.getConfiguration().getSectorCategories()
        .findSectorGroupFromSectorId(getSource().getSectorId());
  }

  @Computed
  public EmissionSourceFeature getSource() {
    return editorContext.getSource();
  }

  @Computed
  public List<SectorGroup> getSectorGroups() {
    return applicationContext.getActiveThemeConfiguration().getSectorGroups();
  }

  @Computed
  public boolean hasMultipleSectors() {
    return getAvailableSectors().size() > 1;
  }

  @Computed("hasCharacteristics")
  public boolean hasCharacteristics() {
    return isEditableSource()
        && (isESType(EmissionSourceType.SRM2_ROAD)
            || isCType(CharacteristicsType.OPS)
            || isCType(CharacteristicsType.ADMS));
  }

  @Computed("sourceCharacteristicsType")
  public String getSourceCharacteristicsType() {
    return EmissionEditorComponentUtil.getSourceCharacteristicsComponent(getSource());
  }

  @Computed("emissionEditorType")
  public String getEmissionEditorType() {
    return EmissionEditorComponentUtil.getEmissionEditorComponenbt(getSource());
  }

  @Computed("sourceCharacteristicsDebugId")
  public String getSourceCharacteristicsDebugId() {
    String type;

    if (isESType(EmissionSourceType.SRM2_ROAD)) {
      type = "collapsibleSRM2Characteristics";
    } else if (isCType(CharacteristicsType.OPS)) {
      type = "collapsibleOPSCharacteristics";
    } else if (isCType(CharacteristicsType.ADMS)) {
      type = "collapsibleADMSCharacteristics";
    } else {
      type = null;
    }

    return type;
  }

  @JsMethod
  public void onLocationWarningChange(final boolean locationWarning) {
    validationContext.setLocationWarning(locationWarning);
  }

  @JsMethod
  public void onLocationErrorChange(final boolean locationError) {
    validationContext.setLocationError(locationError);
  }

  @JsMethod
  public void onCharacteristicsWarningChange(final boolean characteristicsWarning) {
    validationContext.setCharacteristicsWarning(characteristicsWarning);
  }

  @JsMethod
  public void onCharacteristicsErrorChange(final boolean characteristicsError) {
    validationContext.setCharacteristicsError(characteristicsError);
  }

  @JsMethod
  public void onEmissionEditorWarningChange(final boolean emissionEditorWarning) {
    validationContext.setEmissionEditorWarning(emissionEditorWarning);
  }

  @JsMethod
  public void onEmissionEditorErrorChange(final boolean emissionEditorError) {
    validationContext.setEmissionEditorError(emissionEditorError);
  }

  @JsMethod
  public String getId() {
    return getSource().getId();
  }

  @Computed
  public String getSectorColor() {
    return ColorUtil.webColorSector(getSource().getSectorId());
  }

  @Computed
  public String geometryType() {
    return Optional.ofNullable(getSource().getGeometry())
        .map(v -> v.getType())
        .map(v -> i18n.esGeometryType(v))
        .orElseGet(() -> i18n.esGeometryType(null));
  }

  @Computed
  public String getLabel() {
    return getSource().getLabel();
  }

  @Computed
  public void setLabel(final String label) {
    getSource().setLabel(label);
  }

  @JsMethod
  public boolean isSourceAvailable() {
    return editorContext.isEditing();
  }

  @JsMethod
  public boolean isCType(final CharacteristicsType type) {
    return getSource().getCharacteristicsType() == type;
  }

  @JsMethod
  public boolean isESType(final EmissionSourceType type) {
    return getSource().getEmissionSourceType() == type;
  }

  @Computed("hasSector")
  public boolean hasSector() {
    return getSource().getSectorId() != 0;
  }

  @Computed("hasSectorGroup")
  public boolean hasSectorGroup() {
    return selectedSectorGroup != null;
  }

  @Computed("isEditableSource")
  public boolean isEditableSource() {
    return hasSector();
  }

  @Computed
  public List<Sector> getAvailableSectors() {
    return applicationContext.getConfiguration().getSectorCategories().getSectors().stream()
        .filter(v -> v.getSectorGroup() == selectedSectorGroup)
        .collect(Collectors.toList());
  }

  @Computed("hasMooring")
  public boolean hasMooring() {
    // For now only show editable mooring part for maritime inland routes.
    // Technically it's possible for maritime maritime routes and inland routes as
    // well, but since it has no effect on emission calculation that part is left
    // out. To re-enable, add checks on EmissionSourceType.SHIPPING_INLAND or
    // EmissionSourceType.SHIPPING_MARITIME_MARITIME For now, only show the block if
    // the source already has mooring connections (for instance, due to import of
    // old GML)
    return isEditableSource() && (isESType(EmissionSourceType.SHIPPING_MARITIME_INLAND) || hasExistingMooring());
  }

  private boolean hasExistingMooring() {
    return hasExistingMooringMaritime() || hasExistingMooringInland();
  }

  private boolean hasExistingMooringMaritime() {
    if (isESType(EmissionSourceType.SHIPPING_MARITIME_MARITIME) || isESType(EmissionSourceType.SHIPPING_MARITIME_INLAND)) {
      final MaritimeShippingESFeature source = (MaritimeShippingESFeature) getSource();
      return source.getMooringAId() != null || source.getMooringBId() != null;
    } else {
      return false;
    }
  }

  private boolean hasExistingMooringInland() {
    if (isESType(EmissionSourceType.SHIPPING_INLAND)) {
      final InlandShippingESFeature source = (InlandShippingESFeature) getSource();
      return source.getMooringAId() != null || source.getMooringBId() != null;
    } else {
      return false;
    }
  }

  @Computed
  public List<MooringInlandShippingESFeature> getMooringInland() {
    return scenarioContext.getActiveSituation().getSources().stream()
        .filter(v -> EmissionSourceType.SHIPPING_INLAND_DOCKED == v.getEmissionSourceType())
        .map(v -> (MooringInlandShippingESFeature) v)
        .collect(Collectors.toList());
  }

  @Computed
  public List<MooringMaritimeShippingESFeature> getMooringMaritime() {
    return scenarioContext.getActiveSituation().getSources().stream()
        .filter(v -> EmissionSourceType.SHIPPING_MARITIME_DOCKED == v.getEmissionSourceType())
        .map(v -> (MooringMaritimeShippingESFeature) v)
        .collect(Collectors.toList());
  }

  @JsMethod
  public void selectSectorGroupType(final SectorGroup option) {
    eventBus.fireEvent(new EmissionSubSourceDeselectFeatureCommand());
    selectedSectorGroup = option;

    final List<Sector> lst = getAvailableSectors();
    if (lst.size() == 1) {
      changeSector(lst.get(0));
    } else {
      getSource().setSectorId(0); // Resets the sector ID
    }
  }

  @JsMethod
  public void changeSector(final Sector sector) {
    eventBus.fireEvent(new EmissionSourceSectorChangeCommand(sector));
  }

  @JsMethod
  protected void openPanel(final EmissionSourcePanel panel) {
    openPanel = panel;
  }

  @JsMethod
  protected void closePanel(final EmissionSourcePanel panel) {
    openPanel = null;
  }

  @JsMethod
  public boolean isOpen(final EmissionSourcePanel esp) {
    return esp == openPanel;
  }

  @JsMethod
  public void cancel() {
    eventBus.fireEvent(new EmissionSourceEditCancelCommand());
  }

  @JsMethod
  public void closeWarning() {
    setNotificationState(false);
  }

  @JsMethod
  public void closeError() {
    setNotificationState(false);
  }

  @Computed
  public boolean displayFormProblems() {
    return validationContext.isDisplayFormProblems();
  }

  @JsMethod
  public void setNotificationState(final boolean confirm) {
    validationContext.setDisplayFormProblems(confirm);
  }

  @Computed("hasValidationWarnings")
  public boolean hasValidationWarnings() {
    return validationContext.hasWarnings();
  }

  @Computed("hasValidationErrors")
  public boolean hasValidationErrors() {
    return !isEditableSource()
        || validationContext.hasErrors();
  }

  @JsMethod
  public void trySave() {
    // Try to save and open the first panel that has errors (if any)
    if (validationContext.getCharacteristicsError()) {
      openPanel = EmissionSourcePanel.SOURCE_CHARACTERISTICS;
    } else if (validationContext.getLocationError()) {
      openPanel = EmissionSourcePanel.LOCATION;
    } else if (validationContext.getEmissionEditorError()
        || validationContext.getDetailEditorError()) {
      openPanel = EmissionSourcePanel.EMISSION_SOURCE_TYPE;
    }
  }

  @JsMethod
  public void save() {
    eventBus.fireEvent(new EmissionSourceEditSaveCommand());
  }

  @JsMethod
  public void resetSelectedSubSource() {
    eventBus.fireEvent(new MapResizeSequenceCommand());
    eventBus.fireEvent(new EmissionSubSourceDeselectFeatureCommand());
  }

  @JsMethod
  public void editSubSource(final int idx) {
    eventBus.fireEvent(new MapResizeSequenceCommand());
    eventBus.fireEvent(new EmissionSubSourceSelectFeatureCommand(idx));
  }

  private EmissionSourcePanel determineBestOpenPanel() {
    final boolean cameFromBuilding = placeController.getPreviousPlace() instanceof BuildingPlace;
    return cameFromBuilding ? EmissionSourcePanel.SOURCE_CHARACTERISTICS
        : getSource().getGeometry() == null
            ? EmissionSourcePanel.LOCATION
            : EmissionSourcePanel.EMISSION_SOURCE_TYPE;
  }
}
