/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.main;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.easter.leaderboard.TetrisLeaderboardComponent;
import nl.overheid.aerius.geo.command.MapResizeSequenceCommand;
import nl.overheid.aerius.wui.application.components.help.HotKeyPanelComponent;
import nl.overheid.aerius.wui.application.components.importer.ApplicationImportModalComponent;
import nl.overheid.aerius.wui.application.components.info.InfoPanelModal;
import nl.overheid.aerius.wui.application.components.layer.LayerPanelModal;
import nl.overheid.aerius.wui.application.components.modal.notification.NotificationComponent;
import nl.overheid.aerius.wui.application.components.modal.notification.NotificationComponentButton;
import nl.overheid.aerius.wui.application.components.nav.NavigationComponent;
import nl.overheid.aerius.wui.application.components.systeminfo.SystemInfoPanel;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.context.SystemInfoContext;
import nl.overheid.aerius.wui.application.daemon.misc.PlaceContextDaemon;
import nl.overheid.aerius.wui.application.event.LeftPanelToggleCommand;
import nl.overheid.aerius.wui.application.ui.main.selectors.SituationMenuComponent;
import nl.overheid.aerius.wui.application.ui.main.selectors.SubstanceMenuComponent;
import nl.overheid.aerius.wui.application.ui.main.selectors.ThemeMenuComponent;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    SystemInfoPanel.class,
    NavigationComponent.class,

    SituationMenuComponent.class,
    ThemeMenuComponent.class,
    SubstanceMenuComponent.class,

    ApplicationImportModalComponent.class,

    NotificationComponent.class,
    NotificationComponentButton.class,

    LayerPanelModal.class,
    InfoPanelModal.class,
    HotKeyPanelComponent.class,
    HorizontalCollapse.class,

    TetrisLeaderboardComponent.class,
})
public class MainView extends BasicVueComponent implements IsVueComponent, HasCreated, HasMounted, HasDestroyed {
  private static final MainViewEventBinder EVENT_BINDER = GWT.create(MainViewEventBinder.class);

  interface MainViewEventBinder extends EventBinder<MainView> {}

  public enum FlexView {
    /**
     * Sets the left pane to fill the remaining space on the page.
     */
    LEFT,
    /**
     * Sets the right pane to fill the remaining space on the page.
     */
    RIGHT;
  }

  @Prop String left;
  @Prop String middle;
  @Prop String right;

  @Prop boolean hasLeft;
  @Prop boolean hasMiddle;
  @Prop boolean hasRight;

  @Prop boolean softMiddle;

  @Data boolean closed;

  @Data @Inject NavigationContext navigationContext;

  @Prop EventBus eventBus;

  @Inject PlaceContextDaemon placeDaemon;
  @Inject SystemInfoContext systemInfoContext;

  @Data FlexView flexView;
  private HandlerRegistration handlers;

  @PropDefault("softMiddle")
  boolean softMiddleDefault() {
    return false;
  }

  @Computed
  boolean hasSystemInfo() {
    return systemInfoContext.hasSystemInfoMessage();
  }

  @Watch("left")
  public void onLeftSLotChange(final Boolean old, final Boolean neww) {
    eventBus.fireEvent(new MapResizeSequenceCommand());
  }

  @Watch("middle")
  public void onMiddleSlotChange(final Boolean old, final Boolean neww) {
    eventBus.fireEvent(new MapResizeSequenceCommand());
  }

  @Watch("right")
  public void onRightSlotChange(final Boolean old, final Boolean neww) {
    eventBus.fireEvent(new MapResizeSequenceCommand());
  }

  @Watch("hasMiddle")
  public void onMiddleChange(final Boolean old, final Boolean neww) {
    eventBus.fireEvent(new MapResizeSequenceCommand());
  }

  @Watch("softMiddle")
  public void onSoftMiddleChange(final Boolean old, final Boolean neww) {
    eventBus.fireEvent(new MapResizeSequenceCommand());
  }

  @Watch("closed")
  public void onClosedChange(final Boolean old, final Boolean neww) {
    eventBus.fireEvent(new MapResizeSequenceCommand());
  }

  @EventHandler
  public void onLeftPanelToggleCommand(final LeftPanelToggleCommand c) {
    toggleClosed();
  }

  @JsMethod
  public void toggleClosed() {
    closed = !closed;
    eventBus.fireEvent(new LeftPanelCloseEvent(closed));
  }

  @Override
  public void created() {
    placeDaemon.register(() -> closed = !closed, b -> closed = !b);
    handlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    handlers.removeHandler();
  }

  @Override
  public void mounted() {
    GWTProd.log("[MainView] mounted");
  }

  @Computed("isLeftView")
  public boolean isLeftView() {
    return flexView == FlexView.LEFT;
  }

  @Computed("isRightView")
  public boolean isRightView() {
    return flexView == FlexView.RIGHT;
  }

  @Watch("flexView")
  public void onFlexViewChange() {
    eventBus.fireEvent(new MapResizeSequenceCommand());
  }

  /**
   * Set which panel will auto grow when the width of the whole page is scaled
   * larger than what is minimal required.
   *
   * @param flexView panel to grow
   */
  public void setFlexView(final FlexView flexView) {
    this.flexView = flexView;
  }
}
