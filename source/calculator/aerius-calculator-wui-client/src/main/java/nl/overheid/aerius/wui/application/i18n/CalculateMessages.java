/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Text specific for calculate settings pages.
 */
public interface CalculateMessages {
  String calculationType(@Select Theme theme, @Select CalculationType calculationType);
  String calculateTitle();
  String calculateScenarioSummaryTitle();
  String calculateScenarioIndexHeader();
  String calculateScenarioHeader();
  String calculateScenarioTypeHeader();
  String calculateYearHeader();
  String calculateNettingHeader();
  String calculateEmissionSourcesHeader();
  String calculateDetailTitle();
  String calculateReferenceSituation();
  String calculateNettingSituation();
  String calculateType(@Select Theme theme);
  String calculateMeteoLabel();
  String calculateEmissionResultKey(@Select EmissionResultKey emissionResultKey);
  String calculateIncludedScenario();
  String calculateEmptyListSign();
}
