/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.navigation;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.application.command.misc.DeactivateSelectorMenuCommand;
import nl.overheid.aerius.wui.application.command.misc.ToggleSelectorMenuCommand;
import nl.overheid.aerius.wui.application.context.NavigationContext;

public class NavigationDaemon extends BasicEventComponent {
  private static final NavigationDaemonEventBinder EVENT_BINDER = GWT.create(NavigationDaemonEventBinder.class);

  interface NavigationDaemonEventBinder extends EventBinder<NavigationDaemon> {}

  @Inject NavigationContext context;

  @EventHandler
  public void onToggleSelectorMenuCommand(final ToggleSelectorMenuCommand c) {
    context.toggleSelectorMenu(c.getValue());
  }

  @EventHandler
  public void onDeactivateSelectorMenuCommand(final DeactivateSelectorMenuCommand c) {
    context.deactivateSelectorMenu();
  }

  @EventHandler
  public void onPlaceChangeEvent(final PlaceChangeEvent e) {
    context.deactivateSelectorMenu();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
