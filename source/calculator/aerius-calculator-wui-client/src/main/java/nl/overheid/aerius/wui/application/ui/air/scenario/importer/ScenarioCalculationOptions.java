/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.air.scenario.importer;

import javax.annotation.Nullable;

public class ScenarioCalculationOptions {

  private final String calculationYear;

  private final String name;

  private final String srm2;

  private final boolean notifyAfterCalculate;

  private final String email;

  private ScenarioCalculationOptions(
      final String calculationYear,
      final String name,
      @Nullable final String srm2,
      final boolean notifyAfterCalculate,
      @Nullable final String email) {
    this.calculationYear = calculationYear;
    this.name = name;
    this.srm2 = srm2;
    this.notifyAfterCalculate = notifyAfterCalculate;
    this.email = email;
  }

  public static Builder builder() {
    return new ScenarioCalculationOptions.Builder();
  }

  public String calculationYear() {
    return calculationYear;
  }

  public String name() {
    return name;
  }

  @Nullable

  public String srm2() {
    return srm2;
  }

  public boolean notifyAfterCalculate() {
    return notifyAfterCalculate;
  }

  @Nullable

  public String email() {
    return email;
  }

  @Override
  public String toString() {
    return "ScenarioCalculationOptions{"
        + "calculationYear=" + calculationYear + ", "
        + "name=" + name + ", "
        + "srm2=" + srm2 + ", "
        + "notifyAfterCalculate=" + notifyAfterCalculate + ", "
        + "email=" + email
        + "}";
  }

  @Override
  public boolean equals(final Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof ScenarioCalculationOptions) {
      final ScenarioCalculationOptions that = (ScenarioCalculationOptions) o;
      return this.calculationYear.equals(that.calculationYear())
          && this.name.equals(that.name())
          && (this.srm2 == null ? that.srm2() == null : this.srm2.equals(that.srm2()))
          && this.notifyAfterCalculate == that.notifyAfterCalculate()
          && (this.email == null ? that.email() == null : this.email.equals(that.email()));
    }
    return false;
  }

  @Override
  public int hashCode() {
    int h$ = 1;
    h$ *= 1000003;
    h$ ^= calculationYear.hashCode();
    h$ *= 1000003;
    h$ ^= name.hashCode();
    h$ *= 1000003;
    h$ ^= (srm2 == null) ? 0 : srm2.hashCode();
    h$ *= 1000003;
    h$ ^= notifyAfterCalculate ? 1231 : 1237;
    h$ *= 1000003;
    h$ ^= (email == null) ? 0 : email.hashCode();
    return h$;
  }

  static final class Builder {
    private String calculationYear;
    private String name;
    private String srm2;
    private Boolean notifyAfterCalculate;
    private String email;

    Builder() {
    }

    public ScenarioCalculationOptions.Builder calculationYear(final String calculationYear) {
      if (calculationYear == null) {
        throw new NullPointerException("Null calculationYear");
      }
      this.calculationYear = calculationYear;
      return this;
    }

    public ScenarioCalculationOptions.Builder name(final String name) {
      if (name == null) {
        throw new NullPointerException("Null name");
      }
      this.name = name;
      return this;
    }

    public ScenarioCalculationOptions.Builder srm2(final String srm2) {
      this.srm2 = srm2;
      return this;
    }

    public ScenarioCalculationOptions.Builder notifyAfterCalculate(final boolean notifyAfterCalculate) {
      this.notifyAfterCalculate = notifyAfterCalculate;
      return this;
    }

    public ScenarioCalculationOptions.Builder email(final String email) {
      this.email = email;
      return this;
    }

    public ScenarioCalculationOptions build() {
      String missing = "";
      if (this.calculationYear == null) {
        missing += " calculationYear";
      }
      if (this.name == null) {
        missing += " name";
      }
      if (this.notifyAfterCalculate == null) {
        missing += " notifyAfterCalculate";
      }
      if (!missing.isEmpty()) {
        throw new IllegalStateException("Missing required properties:" + missing);
      }
      return new ScenarioCalculationOptions(
          this.calculationYear,
          this.name,
          this.srm2,
          this.notifyAfterCalculate,
          this.email);
    }
  }
}
