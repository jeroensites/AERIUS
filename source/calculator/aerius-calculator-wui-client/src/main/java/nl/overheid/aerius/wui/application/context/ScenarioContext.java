/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.inject.Singleton;

import ol.Feature;
import ol.format.GeoJson;
import ol.format.GeoJsonFeatureOptions;

import elemental2.core.Global;

import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationOptions;
import nl.overheid.aerius.wui.application.domain.geo.CalculationPointUtil;
import nl.overheid.aerius.wui.application.domain.importer.ScenarioMetaData;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.util.json.JsonBuilder;
import nl.overheid.aerius.wui.application.util.json.JsonSerializable;

/**
 * Object to represent a full scenario that can be calculated. A scenario can
 * consist of multiple situations.
 *
 * @see nl.overheid.aerius.shared.domain.v2.scenario.Scenario
 */
@JsType
@Singleton
public class ScenarioContext implements JsonSerializable {
  private final @JsProperty List<SituationContext> situations = new ArrayList<>();
  private String scenarioCode = null;
  private String activeSituationCode = null;
  private ScenarioMetaData scenarioMetaData = ScenarioMetaData.create();
  private final @JsProperty List<CalculationPointFeature> calculationPoints = new ArrayList<>();

  public boolean hasScenario() {
    return scenarioCode != null;
  }

  public String getScenarioCode() {
    return scenarioCode;
  }

  public void setScenarioCode(final String scenarioCode) {
    this.scenarioCode = scenarioCode;
  }

  public SituationContext getActiveSituation() {
    return getSituation(getActiveSituationCode());
  }

  public SituationContext getSituation(final String situationCode) {
    for (final SituationContext situation : situations) {
      if (situation.getSituationCode().equals(situationCode)) {
        return situation;
      }
    }

    return null;
  }

  public boolean deleteSituation(final SituationContext situation) {
    // Also set active situation code to null if that is the one being removed
    if (activeSituationCode != null && activeSituationCode.equals(situation.getSituationCode())) {
      activeSituationCode = null;
    }

    return situations.remove(situation);
  }

  public boolean hasSituations() {
    return !situations.isEmpty();
  }

  public boolean hasActiveSituation() {
    return activeSituationCode != null;
  }

  public String getActiveSituationCode() {
    return activeSituationCode;
  }

  /**
   * Sets a new active situation code. It returns true if the new code was
   * different as the current code or if the current code was null.
   *
   * @param activeSituationCode situation code to set.
   * @return true if activation code set is different from the current
   */
  public boolean setActiveSituationCode(final String activeSituationCode) {
    final boolean changed = this.activeSituationCode == null || !this.activeSituationCode.equals(activeSituationCode);
    this.activeSituationCode = activeSituationCode;
    return changed;
  }

  public List<SituationContext> getSituations() {
    return situations;
  }

  public List<CalculationPointFeature> getCalculationPoints() {
    return calculationPoints;
  }

  /**
   * Add all calculation points to this scenario context ignoring points that overlap. Returns the list of added calculation points.
   */
  public List<CalculationPointFeature> addCalculationPointsIgnoreOverlapping(final List<CalculationPointFeature> calculationPoints) {
    final List<CalculationPointFeature> addedCalculationPoints = new ArrayList<>();
    for (final CalculationPointFeature feature : calculationPoints) {
      this.addCalculationPointIgnoreOverlapping(feature).ifPresent(addedCalculationPoints::add);
    }
    return addedCalculationPoints;
  }

  private Optional<CalculationPointFeature> addCalculationPointIgnoreOverlapping(final CalculationPointFeature newFeature) {
    if (newFeature == null || newFeature.getGeometry() == null) {
      return Optional.empty();
    }

    for (final CalculationPointFeature feature : this.calculationPoints) {
      if (feature.getGeometry().getExtent().containsExtent(newFeature.getGeometry().getExtent())) {
        return Optional.empty();
      }
    }

    CalculationPointUtil.addAndSetId(this.calculationPoints, newFeature);
    return Optional.of(newFeature);
  }

  public String toJSONString(final boolean includeMetaData, final CalculationOptions calculationOptions) {
    return Global.JSON.stringify(toJSON(includeMetaData, calculationOptions));
  }

  @Override
  public Object toJSON() {
    return toJSON(true, new CalculationOptions());
  }

  private Object toJSON(final boolean includeMetaData, final CalculationOptions calculationOptions) {
    final JsonBuilder jsonBuilder = new JsonBuilder(this);
    jsonBuilder.set("situations", this.situations.toArray());
    jsonBuilder.set("customPoints",
        new GeoJson().writeFeaturesObject(this.calculationPoints.toArray(new Feature[calculationPoints.size()]), new GeoJsonFeatureOptions()));

    if (includeMetaData) {
      jsonBuilder.set("metaData", this.scenarioMetaData);
    }
    if (calculationOptions != null) {
      jsonBuilder.set("options", calculationOptions);
    }
    return jsonBuilder.build();
  }

  public boolean isActiveSituation(final SituationContext value) {
    // If either the value given or the active situation is null, return false
    if (value == null || activeSituationCode == null) {
      return false;
    }

    // Just throw here because this should be impossible.
    if (value.getSituationCode() == null) {
      throw new IllegalStateException("SituationContext is in an illegal state: no id when presented for id comparison");
    }

    return value.getSituationCode().equals(activeSituationCode);
  }

  /**
   * Note: This should in ordinary circumstances /only/ be called from
   * SituationDaemon, which does sanity checks.
   */
  public void addSituation(final int idx, final SituationContext situationContext) {
    situations.add(idx, situationContext);
  }

  public ConsecutiveIdUtil createConsecutiveSituationIdUtil() {
    return new ConsecutiveIdUtil(
        i -> Integer.toString(i + 1),
        i -> situations.get(i).getSituationCode(),
        situations::size);
  }

  public ScenarioMetaData getScenarioMetaData() {
    return scenarioMetaData;
  }

  public void setScenarioMetaData(final ScenarioMetaData scenarioMetaData) {
    this.scenarioMetaData = scenarioMetaData;
  }

  public static BuildingFeature findBuilding(final SituationContext context, final String id) {
    return context.getBuildings().stream()
        .filter(v -> id.equals(v.getId()))
        .findFirst().orElse(null);
  }

  /**
   * Static search method for emission sources linked to a building
   */
  public static List<EmissionSourceFeature> findBuildingLinkedSources(final ScenarioContext context, final String situationCode,
      final String buildingId) {
    return context.getSituations().stream()
        .filter(v -> situationCode.equals(v.getSituationCode()))
        .flatMap(v -> v.getSources().stream())
        .filter(v -> v.getCharacteristicsType() == CharacteristicsType.OPS)
        .filter(v -> ((OPSCharacteristics) Js.uncheckedCast(v.getCharacteristics())).isBuildingInfluence())
        .filter(v -> buildingId.equals(((OPSCharacteristics) Js.uncheckedCast(v.getCharacteristics())).getBuildingId()))
        .collect(Collectors.toList());
  }

  // This is a computed util method and as such it is made a public static
  public static String calculateHash(final ScenarioContext scenarioContext) {
    // This is quite the poor man's hash function, but it'll do.
    return String.valueOf(scenarioContext.toJSONString(false, null).hashCode());
  }

  public static boolean hasAnyEmissions(final ScenarioContext scenarioContext) {
    return scenarioContext.getSituations().stream()
        .anyMatch(sit -> sit.getSources().stream()
            .anyMatch(src -> src.getEmission(Substance.NOX) > 0
                || src.getEmission(Substance.NH3) > 0));
  }

  public static boolean hasAnyContent(final ScenarioContext scenarioContext) {
    return scenarioContext.getSituations().stream()
        .anyMatch(sit -> !sit.getSources().isEmpty()
            || !sit.getBuildings().isEmpty());
  }

  public static boolean hasAnySource(final ScenarioContext scenarioContext) {
    return scenarioContext.getSituations().stream()
        .anyMatch(sit -> !sit.getSources().isEmpty());
  }
}
