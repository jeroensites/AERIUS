/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.main.selectors;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.command.ChangeThemeCommand;
import nl.overheid.aerius.wui.application.command.misc.DeactivateSelectorMenuCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.nav.selector.SelectorItem;
import nl.overheid.aerius.wui.application.components.nav.selector.SelectorItemOption;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    BasicSelectorMenuComponent.class,
    ButtonIcon.class
})
public class ThemeMenuComponent extends BasicVueComponent implements IsVueComponent {
  @Prop EventBus eventBus;
  @Prop SelectorItem item;

  @JsMethod
  public void selectOption(final SelectorItemOption option) {
    eventBus.fireEvent(new DeactivateSelectorMenuCommand());
    eventBus.fireEvent(new ChangeThemeCommand((Theme) option.getValue()));
  }

  @Computed
  public SelectorItemOption getSelectedOption() {
    return item.getSelectedOption();
  }
}
