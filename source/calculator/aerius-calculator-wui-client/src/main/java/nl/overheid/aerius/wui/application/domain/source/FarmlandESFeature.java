/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import elemental2.core.JsArray;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.domain.source.farmland.FarmlandActivity;

/**
 * Feature object for Farm Land Emission Sources with OPS characteristics.
 */
@JsType(namespace = GLOBAL, name = "ol.Feature", isNative = true)
public class FarmlandESFeature extends EmissionSubSourceFeature<FarmlandActivity> {

  /**
   * @return Returns a new {@link FarmlandESFeature} object.
   */
  public static @JsOverlay FarmlandESFeature create(final CharacteristicsType type) {
    final FarmlandESFeature feature = new FarmlandESFeature();
    init(feature, EmissionSourceType.FARMLAND);
    if (type == CharacteristicsType.OPS) {
      feature.setCharacteristics(OPSCharacteristics.create());
    } else if (type == CharacteristicsType.ADMS) {
      feature.setCharacteristics(ADMSCharacteristics.create());
    }
    feature.setSubSources(new JsArray<>());
    return feature;
  }

  /**
   * @return Returns model characteristics as OPS characteristics.
   */
  public final @JsOverlay OPSCharacteristics getOPSCharacteristics() {
    return Js.uncheckedCast(getCharacteristics());
  }
}
