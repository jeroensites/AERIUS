/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.collapsible;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.PropDefault;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.ui.main.VerticalCollapse;
import nl.overheid.aerius.wui.vue.BasicVueComponent;
import nl.overheid.aerius.wui.vue.VectorDirective;

/**
 * Emits:
 *
 * <pre>
 * - user-open: when the user opens the panel
 * - panelOpened: when the user opens the panel (legacy naming)
 * - user-close: when the user closes the panel
 * - panelClosed: when the user closes the panel (legacy naming)
 * - toggle: when the user opens or closes the panel
 * - opened: when the user opens or closes the panel (legacy naming)
 * - open: when the panel opens close: when the panel closes
 * </pre>
 */
@Component(components = {
    VerticalCollapse.class,
    CollapsibleButton.class
}, directives = {
    VectorDirective.class
})
public class CollapsiblePanel extends BasicVueComponent implements IsVueComponent {
  @Prop String title;
  @Prop boolean open;

  @Prop boolean contentVisible;

  @Data boolean openData;

  @PropDefault("contentVisible")
  boolean contentVisibleDefault() {
    return true;
  }

  @PropDefault("open")
  boolean openDefault() {
    return false;
  }

  @Watch(value = "open", isImmediate = true)
  public void onOpenUpdate(final boolean neww, final boolean old) {
    openData = Boolean.TRUE.equals(neww);
  }

  @Watch(value = "openData")
  public void onOpenDataUpdate(final boolean neww, final boolean old) {
    if (neww) {
      vue().$emit("open");
    } else {
      vue().$emit("close");
    }
  }

  @JsMethod
  public void toggleOpen() {
    openData = !openData;

    vue().$emit("opened", this);
    vue().$emit("toggle", this);
    if (openData) {
      vue().$emit("user-open");
      vue().$emit("panelOpened");
    } else {
      vue().$emit("user-close");
      vue().$emit("panelClosed");
    }
  }

}
