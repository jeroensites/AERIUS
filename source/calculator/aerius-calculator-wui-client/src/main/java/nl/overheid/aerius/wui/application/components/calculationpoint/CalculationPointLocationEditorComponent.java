/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.calculationpoint;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasBeforeDestroy;
import com.google.web.bindery.event.shared.EventBus;

import ol.format.Wkt;
import ol.geom.Geometry;

import elemental2.dom.HTMLInputElement;
import elemental2.dom.InputEvent;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointDrawCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.MinimalInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointChangeEvent;
import nl.overheid.aerius.wui.application.geo.util.GeoType;

@Component(components = {
    ButtonIcon.class,
    TooltipComponent.class,
    MinimalInputComponent.class,
    ValidationBehaviour.class,
})
public class CalculationPointLocationEditorComponent extends ErrorWarningValidator implements HasBeforeDestroy {
  private static final Wkt WKT = new Wkt();

  @Prop CalculationPointFeature calculationPoint;
  @Prop EventBus eventBus;
  @Prop boolean open;
  @Prop boolean showErrors;

  @Data String locationString = "";
  @Data boolean geometryValid = false;

  @Override
  public void beforeDestroy() {
    eventBus.fireEvent(new CalculationPointDrawCommand(null));
  }

  @Watch(value = "open", isImmediate = true)
  public void onOpenChange(final boolean open) {
    if (open) {
      eventBus.fireEvent(new CalculationPointDrawCommand(geometry -> updateFromMap(geometry)));
    } else {
      eventBus.fireEvent(new CalculationPointDrawCommand(null));
    }
  }

  @Watch(value = "calculationPoint", isImmediate = true)
  public void onCalculationPointChange(final CalculationPointFeature calculationPointFeature) {
    if (calculationPointFeature != null && calculationPointFeature.getGeometry() != null) {
      locationString = OL3GeometryUtil.toWktString(calculationPointFeature.getGeometry());
      setGeometryValid(true);
    }
  }

  @JsMethod
  public void addPoint() {
    eventBus.fireEvent(new CalculationPointDrawCommand(geometry -> updateFromMap(geometry)));
  }

  @JsMethod
  public boolean isValidWktGeometry() {
    try {
      WKT.readGeometry(locationString);
      return validateGeometry();
    } catch (final Exception e) {
      return false;
    }
  }

  @JsMethod
  public void updateFromDirectInput(final InputEvent event) {
    final String wktInput = ((HTMLInputElement) event.target).value;
    locationString = wktInput;

    try {
      final Geometry geometry = WKT.readGeometry(wktInput);

      updateGeometry(geometry);

      setGeometryValid(true);
    } catch (final RuntimeException e) {
      setGeometryValid(false);
    }
  }

  private void updateFromMap(final Geometry geometry) {
    updateGeometry(geometry);
    locationString = OL3GeometryUtil.toWktString(geometry);
    setGeometryValid(true);
  }

  private void updateGeometry(final Geometry geometry) {
    geometry.addChangeListener(v -> validateGeometry());

    calculationPoint.setGeometry(geometry);
    eventBus.fireEvent(new CalculationPointChangeEvent(calculationPoint));
  }

  private boolean validateGeometry() {
    return geometryValid = GeoType.POINT.is(calculationPoint.getGeometry());
  }

  private void setGeometryValid(final boolean isValid) {
    geometryValid = isValid;
  }
}
