/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.context.dummy;

import com.google.gwt.user.client.Random;

import ol.Coordinate;
import ol.geom.Geometry;
import ol.geom.LineString;
import ol.geom.Point;
import ol.geom.Polygon;

import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;

public class RandomGeometry {

  private RandomGeometry() {
  }

  public static Geometry randomGeometry() {
    return randomGeometry(null);
  }

  public static Geometry randomGeometry(GeometryType geometryType) {
    if (geometryType == null) {
      switch (Random.nextInt(3)) {
      case 0:
        geometryType = GeometryType.POINT;
        break;
      case 1:
        geometryType = GeometryType.LINESTRING;
        break;
      default:
        geometryType = GeometryType.POLYGON;
      }
    }

    switch (geometryType) {
    case POLYGON:
      final double xP = randomX();
      final double yP = randomY();

      final double length = randomLength() * 2;
      final double width = randomLength();

      final double fuzz = randomLength() - 30;

      return new Polygon(new Coordinate[][] {
          { rCd(xP, yP, 0, 0), rCd(xP, yP, length + fuzz, fuzz), rCd(xP, yP, length, width + fuzz), rCd(xP, yP, fuzz, width), rCd(xP, yP, 0, 0) } });
    case LINESTRING:
      final double xL = randomX();
      final double yL = randomY();
      return new LineString(new Coordinate[] { rCd(xL, yL, 0, 0), rCd(xL, yL, 50, 50), rCd(xL, yL, 100, 40) });
    default:
      return new Point(rCd(randomX(), randomY(), 0, 0));
    }
  }

  private static double randomLength() {
    return 10 + Random.nextInt(50);
  }

  private static double randomX() {
    return 100000 + Random.nextInt(100000);
  }

  private static double randomY() {
    return 400000 + Random.nextInt(200000);
  }

  private static Coordinate rCd(final double x, final double y, final double oX, final double oY) {
    return new Coordinate(x + oX, y + oY);
  }

}
