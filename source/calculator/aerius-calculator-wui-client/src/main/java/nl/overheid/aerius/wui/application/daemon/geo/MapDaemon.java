/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.geo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Feature;
import ol.OLFactory;
import ol.Overlay;
import ol.OverlayOptions;
import ol.Pixel;
import ol.format.GeoJson;
import ol.format.GeoJsonOptions;
import ol.geom.Geometry;
import ol.geom.Polygon;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.NotificationUtil;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.geo.command.InteractionAddedCommand;
import nl.overheid.aerius.geo.command.InteractionRemoveCommand;
import nl.overheid.aerius.geo.command.LayerAddedCommand;
import nl.overheid.aerius.geo.command.LayerRemovedCommand;
import nl.overheid.aerius.geo.command.MapSetExtentCommand;
import nl.overheid.aerius.geo.command.OverlayAddedCommand;
import nl.overheid.aerius.geo.command.OverlayRemoveCommand;
import nl.overheid.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.geo.domain.IsOverlay;
import nl.overheid.aerius.geo.event.MapSetExtentEvent;
import nl.overheid.aerius.geo.wui.MapLayoutPanel;
import nl.overheid.aerius.geo.wui.util.OL3MapUtil;
import nl.overheid.aerius.shared.config.AppThemeConfiguration;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.command.MeasureToggleCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationResultsZoomCommand;
import nl.overheid.aerius.wui.application.command.source.FeatureZoomCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.MeasureContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.ChangeThemeEvent;
import nl.overheid.aerius.wui.application.event.SituationResultsZoomEvent;
import nl.overheid.aerius.wui.application.geo.MeasureTool;
import nl.overheid.aerius.wui.application.geo.interactions.SourceClickInteraction;
import nl.overheid.aerius.wui.application.geo.layers.BackgroundDepositionLayer;
import nl.overheid.aerius.wui.application.geo.layers.LayerFactory;
import nl.overheid.aerius.wui.application.geo.layers.habitats.HabitatHoverLayer;
import nl.overheid.aerius.wui.application.geo.layers.habitats.HabitatSelectLayer;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationMarkersLayer;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationResultLayer;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.i18n.M;

/**
 * Daemon to manage map changes.
 */
@Singleton
public class MapDaemon extends BasicEventComponent implements Daemon {
  private static final String HABITAT_AREAS_SENSITIVITY_NAME = "calculator:wms_habitat_areas_sensitivity_view";

  private static final double BOUNDING_BOX_SCALE = 1.3;

  public static final int SONAR_REMOVE_TIME = 11_000;

  interface MapDaemonEventBinder extends EventBinder<MapDaemon> {}

  private final MapDaemonEventBinder EVENT_BINDER = GWT.create(MapDaemonEventBinder.class);

  @Inject ScenarioContext scenarioContext;
  @Inject CalculationContext calculationContext;

  private @Inject ApplicationContext applicationContext;
  private @Inject MapLayoutPanel map;
  private @Inject LayerFactory layerFactory;

  private @Inject ResultLayerContext resultLayerContext;
  private @Inject MeasureContext measureContext;

  private EventBus eventBus;

  private final MeasureTool measureTool;

  // Keep record of sent situation results zoom commands by their extent
  // used for timing the result zoom event properly (i.e. only after handling the set extend command and the set extend event has fired).
  private final Map<String, SituationResultsKey> situationResultsZoomCommandsIssued = new HashMap<>();

  public MapDaemon() {
    measureTool = new MeasureTool(overlay -> {
      eventBus.fireEvent(new OverlayAddedCommand(overlay));
    });
  }

  @EventHandler
  public void onFocusEmissionSourceCommand(final FeatureZoomCommand c) {
    final Geometry geom = c.getValue().getGeometry();
    if (geom == null) {
      GWTProd.warn("Feature that does not have a geometry was focused.");
      return;
    }

    eventBus.fireEvent(GeoUtil.createMapSetExtendCommand(geom));
  }

  @EventHandler
  public void onSonarCommand(final SonarCommand c) {
    final DivElement innerSonar = Document.get().createDivElement();
    innerSonar.setClassName("aer-inner-sonar");

    final DivElement sonar = Document.get().createDivElement();
    sonar.setClassName("aer-sonar");
    sonar.appendChild(innerSonar);

    final DivElement animationSonar = Document.get().createDivElement();
    animationSonar.setClassName("aer-animation-sonar");

    final DivElement outerSonar = Document.get().createDivElement();
    outerSonar.setClassName("aer-outer-sonar");
    outerSonar.appendChild(animationSonar);
    outerSonar.appendChild(sonar);

    final OverlayOptions options = OLFactory.createOptions();
    options.setElement(outerSonar);
    options.setInsertFirst(false);
    options.setStopEvent(false);
    options.setPositioning("top-left");
    options.setOffset(new Pixel(0, 0));
    final Overlay tooltip = new Overlay(options);

    tooltip.setPosition(c.getValue().getFirstCoordinate());

    final IsOverlay<Overlay> overlay = () -> tooltip;

    eventBus.fireEvent(new OverlayAddedCommand(overlay));
    SchedulerUtil.delay(() -> {
      eventBus.fireEvent(new OverlayRemoveCommand(overlay));
    }, SONAR_REMOVE_TIME);
  }

  @EventHandler
  public void onMeasureToggleCommand(final MeasureToggleCommand c) {
    if (measureContext.isActive()) {
      eventBus.fireEvent(new InteractionRemoveCommand(measureTool.getInteraction()));
      eventBus.fireEvent(new LayerRemovedCommand(measureTool.getLayer()));
      measureTool.getOverlays().forEach(overlay -> {
        eventBus.fireEvent(new OverlayRemoveCommand(overlay));
      });
      measureTool.clear();
    } else {
      eventBus.fireEvent(new InteractionAddedCommand(measureTool.getInteraction()));
      eventBus.fireEvent(new LayerAddedCommand(measureTool.getLayer()));
    }

    measureContext.setActive(!measureContext.isActive());
  }

  @EventHandler
  public void onChangeThemeEvent(final ChangeThemeEvent e) {
    final Theme eventTheme = e.getValue();

    // TODO This seems to be adding layers and not remove layers resulting from
    // previous occurrences of this event?
    GWTProd.warn("Changing layers as a result of theme change."); // Warning in console to bring attention to the above
    resultLayerContext.clearResultLayers();

    final ApplicationConfiguration appConfig = applicationContext.getConfiguration();
    final AppThemeConfiguration appThemeConfig = appConfig.getAppThemeConfiguration(eventTheme);

    // Add baselayers to map
    OL3MapUtil.createLayers(appThemeConfig.getBaseLayers(), this::handleLayerError)
        .forEach(baseLayer -> eventBus.fireEvent(new LayerAddedCommand(baseLayer)));

    // Add calculation boundary layer
    final String calculatorBoundary = appConfig.getSettings().getSetting(SharedConstantsEnum.CALCULATOR_BOUNDARY);
    if (calculatorBoundary != null && !calculatorBoundary.isEmpty()) {
      eventBus.fireEvent(new LayerAddedCommand(layerFactory.createCalculationBoundaryLayer(calculatorBoundary)));
    }

    // Add layers to map
    final List<IsLayer<?>> layers = OL3MapUtil.createLayers(appThemeConfig.getLayers(), this::handleLayerError);
    layers.forEach(layer -> eventBus.fireEvent(new LayerAddedCommand(layer)));

    final BackgroundDepositionLayer backgroundDepositionLayer = layerFactory.createBackgroundDepositionLayer(appThemeConfig);
    eventBus.fireEvent(new LayerAddedCommand(backgroundDepositionLayer));

    // Add source layers
    int zIndex = 0;
    eventBus.fireEvent(new LayerAddedCommand(layerFactory.createSourceLayerGroup(zIndex++)));
    eventBus.fireEvent(new LayerAddedCommand(layerFactory.createRoadSourceGeometryLayer(appThemeConfig,
        appConfig.getSettings().getSetting(SharedConstantsEnum.DRIVING_SIDE),
        zIndex++)));

    // Add calculation point marker layer
    eventBus.fireEvent(new LayerAddedCommand(layerFactory.createCalculationPointMarkerLayer(zIndex++)));

    // Create habitat selection and hover layers
    final HabitatHoverLayer habitatHoverLayer = layerFactory.createHabitatHoverLayer();
    final HabitatSelectLayer habitatSelectionLayer = layerFactory.createHabitatSelectionLayer();
    eventBus.fireEvent(new LayerAddedCommand(habitatHoverLayer));
    eventBus.fireEvent(new LayerAddedCommand(habitatSelectionLayer));

    // Find the sensitive habitat layer
    final Optional<IsLayer<?>> habitatSensitivityLayer = layers.stream().filter(v -> HABITAT_AREAS_SENSITIVITY_NAME.equals(v.getInfo().getName()))
        .findFirst();

    if (habitatSensitivityLayer.isPresent()) {
      resultLayerContext.setHabitatLayer(habitatSensitivityLayer.get());
    } else {
      GWTProd.warn("No habitat sensitivity layer could be found.");
    }

    // Add result layers
    final CalculationResultLayer resultLayer = layerFactory.createCalculationResultLayer(appThemeConfig);
    final CalculationMarkersLayer markersLayer = layerFactory.createCalculationMarkersLayer();
    markersLayer.asLayer().setVisible(false);
    resultLayerContext.setResultLayer(resultLayer);
    resultLayerContext.setMarkersLayer(markersLayer);
    eventBus.fireEvent(new LayerAddedCommand(resultLayer));
    eventBus.fireEvent(new LayerAddedCommand(markersLayer));

    eventBus.fireEvent(new LayerAddedCommand(layerFactory.createInfoMarkerLayer()));
    eventBus.fireEvent(new LayerAddedCommand(layerFactory.createGeometryHighlightLayer()));

    // Add a source click interaction layer (for source selections)
    eventBus.fireEvent(new InteractionAddedCommand(new SourceClickInteraction(eventBus)));
  }

  private void handleLayerError(final String layerTitle) {
    NotificationUtil.broadcastError(eventBus, M.messages().errorLayerCouldNotBeLoaded(layerTitle));
  }

  @EventHandler
  public void onSituationZoomCommand(final SituationResultsZoomCommand c) {
    c.silence();
    final SituationResultsKey situationResultsKey = c.getValue();

    final List<EmissionSourceFeature> emissionSourceFeatures = Optional
        .ofNullable(scenarioContext.getSituation(situationResultsKey.getSituationHandle().getCode()))
        .map(sc -> sc.getSources())
        .orElse(Arrays.asList());

    final List<BuildingFeature> buildingFeatures = Optional
        .ofNullable(scenarioContext.getSituation(situationResultsKey.getSituationHandle().getCode()))
        .map(sc -> sc.getBuildings())
        .orElse(Arrays.asList());

    final List<Feature> markerFeatures = Optional.ofNullable(calculationContext.getActiveCalculation())
        .map(ac -> ac.getResultContext())
        .map(rc -> rc.getResultSummary(situationResultsKey))
        .map(rs -> rs.getMarkers().asList().stream().map(rm -> {
          final Feature markerFeature = new Feature();
          markerFeature.setGeometry(new GeoJson().readGeometry(rm.getPoint(), new GeoJsonOptions()));
          return markerFeature;
        }).collect(Collectors.toList()))
        .orElse(Arrays.asList());

    // Assume that the situation contains at least one emission source to zoom to,
    // since we need an initial extent.
    emissionSourceFeatures.stream().findFirst()
        .map(feature -> feature.getGeometry())
        .map(geom -> geom.getExtent())
        .map(extent -> {
          emissionSourceFeatures.forEach(esf -> extent.extend(esf.getGeometry().getExtent()));
          buildingFeatures.forEach(bsf -> extent.extend(bsf.getGeometry().getExtent()));
          markerFeatures.forEach(mf -> extent.extend(mf.getGeometry().getExtent()));
          return extent;
        })
        .ifPresent(extent -> {
          final Polygon polygon = Polygon.fromExtent(extent);
          polygon.scale(BOUNDING_BOX_SCALE, BOUNDING_BOX_SCALE);
          final MapSetExtentCommand mapSetExtendCommand = GeoUtil.createMapSetExtendCommand(polygon);

          // Don't animate zoom as long as SituationResultsZoomCommand is only fired from print wnb context.
          mapSetExtendCommand.setAnimated(false);
          eventBus.fireEvent(mapSetExtendCommand);
          situationResultsZoomCommandsIssued.put(mapSetExtendCommand.getValue(), c.getValue());
        });
  }

  @EventHandler
  public void onMapSetExtentEvent(final MapSetExtentEvent e) {
    final SituationResultsKey situationResultsKey = situationResultsZoomCommandsIssued.get(e.getValue());
    if (situationResultsKey != null) {
      eventBus.fireEvent(new SituationResultsZoomEvent(situationResultsKey));
      situationResultsZoomCommandsIssued.remove(e.getValue());
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    this.eventBus = eventBus;
    EVENT_BINDER.bindEventHandlers(this, eventBus);
    map.setEventBus(eventBus);
  }
}
