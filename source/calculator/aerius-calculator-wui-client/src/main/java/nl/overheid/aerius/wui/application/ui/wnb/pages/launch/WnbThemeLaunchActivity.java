/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.launch;

import com.google.gwt.event.dom.client.DragEnterEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.application.command.UserImportRequestedCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.place.ThemeLaunchPlace;
import nl.overheid.aerius.wui.application.ui.main.MainView.FlexView;
import nl.overheid.aerius.wui.application.ui.pages.ThemeDelegatedActivity;
import nl.overheid.aerius.wui.application.ui.pages.ThemeView;

public class WnbThemeLaunchActivity extends BasicEventComponent implements WnbThemeLaunchPresenter, ThemeDelegatedActivity {
  @Inject PlaceController placeController;
  @Inject ScenarioContext scenarioContext;
  @Inject ApplicationContext applicationContext;

  private HandlerRegistration dragRegister;

  @Inject
  public WnbThemeLaunchActivity(@Assisted final ThemeLaunchPlace place) {
    GWTProd.log("WnbThemeLaunchActivity");
  }

  @Override
  public void onStart(final ThemeView view) {
    view.setDelegatedPresenter(this);
    view.setRightView(WnbThemeLaunchViewFactory.get(), FlexView.RIGHT);
    view.setOnlyRightView();
    dragRegister = RootPanel.get().addDomHandler(event -> eventBus.fireEvent(new UserImportRequestedCommand()), DragEnterEvent.getType());
  }

  @Override
  public void onStop() {
    dragRegister.removeHandler();
  }
}
