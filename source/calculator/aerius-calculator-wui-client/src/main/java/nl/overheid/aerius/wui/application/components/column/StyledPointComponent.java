/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.column;

import java.util.Optional;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import ol.Feature;

import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-styled-point")
public class StyledPointComponent extends BasicVueComponent implements IsVueComponent {
  @Prop Feature feature;
  @Prop String backColor;
  @Prop String foreColor;
  @Prop String borderRadius;

  @Computed
  public String getId() {
    return Optional.ofNullable(feature)
        .map(v -> v.getId())
        .map(v -> v.startsWith("-") ? v.substring(1) : v)
        .orElse("-");
  }
}
