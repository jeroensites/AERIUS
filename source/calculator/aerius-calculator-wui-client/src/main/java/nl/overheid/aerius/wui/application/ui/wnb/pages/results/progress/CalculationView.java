/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.results.progress;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.directives.VectorDirective;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.ServerUsage;
import nl.overheid.aerius.wui.application.command.CalculationCancelCommand;
import nl.overheid.aerius.wui.application.command.CalculationResetCommand;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    ProgressBarView.class,
    VerticalCollapse.class
}, directives = {
    VectorDirective.class,
})
public class CalculationView extends BasicVueView {
  @Prop EventBus eventBus;
  @Prop CalculationInfo calculationInfo;

  @Computed
  public ServerUsage getServerUsage() {
    return calculationInfo.getCalculationServerUsage();
  }

  @Computed("isWarning")
  public boolean isWarning() {
    final ServerUsage serverUsage = getServerUsage();
    return serverUsage == ServerUsage.ABOVE_AVERAGE
        || serverUsage == ServerUsage.VERY_HIGH;
  }

  @Computed
  public CalculationState getState() {
    return CalculationState.fromJob(calculationInfo.getJobProgress());
  }

  @Computed
  public JobProgress getJobProgress() {
    return calculationInfo.getJobProgress();
  }

  @JsMethod
  public void resetCalculation() {
    eventBus.fireEvent(new CalculationResetCommand());
  }

  @JsMethod
  public void stopCalculation() {
    eventBus.fireEvent(new CalculationCancelCommand());
  }
}
