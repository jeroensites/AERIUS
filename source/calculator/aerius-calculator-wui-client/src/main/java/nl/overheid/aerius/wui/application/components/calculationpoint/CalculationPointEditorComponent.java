/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.calculationpoint;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.directives.VectorDirective;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditCancelCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.CalculationPointEditSaveCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.feature.FeatureHeading;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyCancelSaveComponent;
import nl.overheid.aerius.wui.application.context.CalculationPointEditorContext;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsource.ErrorWarningIconComponent;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    LabeledInputComponent.class,
    ModifyCancelSaveComponent.class,
    CollapsiblePanel.class,
    CalculationPointLocationEditorComponent.class,
    ErrorWarningIconComponent.class,
    FeatureHeading.class,
}, directives = {
    VectorDirective.class,
})
public class CalculationPointEditorComponent extends BasicVueEventComponent {

  @Prop EventBus eventBus;
  @Prop CalculationPointFeature calculationPoint;

  @Data boolean locationPanelOpen = true;
  @Data boolean showValidationMessages = false;
  @Data boolean locationError;

  @Inject @Data CalculationPointEditorContext context;

  @Computed
  public String getCalculationPointTitle() {
    return context.getOriginal() == null ? i18n.calculationPointsNewTitle() : i18n.calculationPointsEditTitle();
  }

  @Computed
  public String getLabel() {
    return calculationPoint.getLabel();
  }

  @Computed
  public void setLabel(final String label) {
    calculationPoint.setLabel(label);
  }

  @JsMethod
  public void cancel() {
    eventBus.fireEvent(new CalculationPointEditCancelCommand());
  }

  @JsMethod
  public void onLocationErrorsChange(final boolean locationError) {
    this.locationError = locationError;
  }

  @JsMethod
  public void save() {
    eventBus.fireEvent(new CalculationPointEditSaveCommand());
  }

  @JsMethod
  public void showValidationMessages(final boolean confirm) {
    showValidationMessages = confirm;
  }

  @JsMethod
  public void closeError() {
    showValidationMessages = false;
  }
}
