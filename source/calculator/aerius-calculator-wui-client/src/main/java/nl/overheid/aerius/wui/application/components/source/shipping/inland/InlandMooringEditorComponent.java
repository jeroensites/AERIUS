/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.shipping.inland;

import java.util.List;
import java.util.stream.Collectors;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.shipping.MooringType;
import nl.overheid.aerius.wui.application.domain.source.InlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MooringInlandShippingESFeature;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-inland-mooring-emission-editor",
components = {
    LabeledInputComponent.class
})
public class InlandMooringEditorComponent extends BasicVueComponent {
  @Prop EventBus eventBus;
  @Prop InlandShippingESFeature source;
  @Prop @JsProperty List<MooringInlandShippingESFeature> mooringInland;

  @JsMethod
  public boolean isESType(final EmissionSourceType type) {
    return source.getEmissionSourceType() == type;
  }

  @Computed
  protected String getMooringAId() {
    return source.getMooringAId() == null ? "" : source.getMooringAId();
  }

  @Computed
  protected void setMooringAId(final String id) {
    source.setMooringAId("".equals(id) ? null : id);
  }

  @Computed
  protected String getMooringBId() {
    return source.getMooringBId() == null ? "" : source.getMooringBId();
  }

  @Computed
  protected void setMooringBId(final String id) {
    source.setMooringBId("".equals(id) ? null : id);
  }

  @JsMethod
  protected List<MooringInlandShippingESFeature> getAllowedMoorings(final MooringType mooringType) {
    return mooringInland.stream()
        .filter(m -> includeMooringType(m, mooringType))
        .collect(Collectors.toList());
  }

  @JsMethod
  protected String getLabel(final MooringInlandShippingESFeature feature) {
    return feature.getLabel().isEmpty() ? feature.getId() : feature.getLabel();
  }

  private boolean includeMooringType(final MooringInlandShippingESFeature feature, final MooringType mooringType) {
    return MooringType.A.equals(mooringType) ? !feature.getId().equals(getMooringBId()) : !feature.getId().equals(getMooringAId());
  }
}
