/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.command.ImportSituationParcelCommand;
import nl.overheid.aerius.wui.application.command.situation.CreateEmptySituationCommand;
import nl.overheid.aerius.wui.application.command.situation.DuplicateSituationCommand;
import nl.overheid.aerius.wui.application.command.situation.GoToSituationCommand;
import nl.overheid.aerius.wui.application.command.situation.SwitchSituationCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceCreateFirstCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.dummy.DummyCalculationPoints;
import nl.overheid.aerius.wui.application.context.dummy.DummySituationContext;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointsAddedEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationAddedEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationDeletedEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.EmissionSourceListPlace;
import nl.overheid.aerius.wui.application.place.ThemeLaunchPlace;

public class SituationDaemon extends BasicEventComponent {
  private static final SituationDaemonEventBinder EVENT_BINDER = GWT.create(SituationDaemonEventBinder.class);

  interface SituationDaemonEventBinder extends EventBinder<SituationDaemon> {}

  @Inject ApplicationContext applicationContext;
  @Inject ScenarioContext scenarioContext;

  @Inject PlaceController placeController;

  @EventHandler
  public void onCreateFirstSourceCommand(final EmissionSourceCreateFirstCommand c) {
    // TODO Don't create a dummy context for dev convenience.
    // NOTE: This is currently still useful for developers, so leave it in until no
    // longer needed. TODO remains as a reminder.
    if (GWTProd.isDev()) {
      createMockSituation();
      createMockCalculationPoints();
      placeController.goTo(new EmissionSourceListPlace(applicationContext.getActiveTheme()));
    } else {
      // In production mode, create an empty situation, and move to the emission
      // source list
      final SituationContext situation = createAndAddSituation(v -> {});

      if (situation != null) {
        eventBus.fireEvent(new SwitchSituationCommand(situation.getSituationCode()));
        placeController.goTo(new EmissionSourceListPlace(applicationContext.getActiveTheme()));
      }
    }
  }

  private void createMockSituation() {
    final SituationContext situation = createAndAddSituation(v -> DummySituationContext.dummy(v));

    if (situation != null) {
      eventBus.fireEvent(new SwitchSituationCommand(situation.getSituationCode()));
    }
  }

  private void createMockCalculationPoints() {
    final List<CalculationPointFeature> dummies = DummyCalculationPoints.calculationPoints();
    scenarioContext.getCalculationPoints().addAll(dummies);
    eventBus.fireEvent(new CalculationPointsAddedEvent(dummies));
  }

  @EventHandler
  public void onCreateMockSituationCommand(final InitializeMockSituationCommand c) {
    GWTProd.log("Creating mock situation..");
    createMockSituation();
  }

  @EventHandler
  public void onGoToSituationCommand(final GoToSituationCommand c) {
    placeController.goTo(new EmissionSourceListPlace(applicationContext.getActiveTheme()));
    eventBus.fireEvent(new SwitchSituationCommand(c.getValue()));
  }

  @EventHandler
  public void onDuplicateSituationCommand(final DuplicateSituationCommand c) {
    final SituationContext context = scenarioContext.getSituation(c.getValue());

    // Duplicate situation
    createAndAddSituation(sc -> {
      sc.copyFrom(context);

      if (sc.getType() == SituationType.REFERENCE || sc.getType() == SituationType.NETTING) {
        sc.setType(SituationType.PROPOSED);
      }
    });
  }

  @EventHandler
  public void onCreateEmptySituationCommand(final CreateEmptySituationCommand c) {
    Optional.ofNullable(createAndAddSituation(sc -> {}))
        .ifPresent(sit -> {
          if (c.isAlsoSwitch()) {
            eventBus.fireEvent(new SwitchSituationCommand(sit.getSituationCode()));
          }
        });
  }

  @EventHandler
  public void onImportSituationParcelCommand(final ImportSituationParcelCommand c) {
    createAndAddSituation(c.getValue());
  }

  /**
   * Create a new situation and add to the context
   *
   * @param situationBuilder function to change situation properties before the
   *                         situation is added
   */
  public SituationContext createAndAddSituation(final Consumer<SituationContext> situationBuilder) {
    final ConsecutiveIdUtil.IndexIdPair newId = scenarioContext.createConsecutiveSituationIdUtil().generate();

    final String situationCode = newId.getId();
    final SituationContext situationContext = new SituationContext(situationCode);

    // Set default values
    situationContext.setName(M.messages().navigationSituationShortLabel(situationCode));
    situationContext.setYear(applicationContext.getActiveThemeConfiguration().getCalculationYearDefault());
    situationContext.setType(SituationType.PROPOSED); // TODO What should be the default here? If it's null it leads to trouble.
    situationBuilder.accept(situationContext);

    // Guard the year against empty or 0-value by resetting to the default year
    // (ideally the situationBuilder doesn't set these values in the first place,
    // but we shouldn't assume that they don't at this stage)
    if ("0".equals(situationContext.getYear())
        || "".equals(situationContext.getYear())) {
      // Warn to console because the situation builder ideally shouldn't do this
      GWTProd.warn("SituationBuilder set the calculation year to an invalid value (" + situationContext.getYear() + ") and has been corrected");
      situationContext.setYear(applicationContext.getActiveThemeConfiguration().getCalculationYearDefault());
    }

    snapYear(situationContext, applicationContext.getActiveThemeConfiguration().getCalculationYears(), eventBus);

    // Check if the situation can be added (and warn), return null if not
    if (!checkTooManySituations(situationContext)) {
      return null;
    }

    scenarioContext.addSituation(newId.getIndex(), situationContext);
    eventBus.fireEvent(new SituationAddedEvent(situationContext));

    return situationContext;
  }

  private boolean checkTooManySituations(final SituationContext neww) {
    if (neww.getType() == SituationType.REFERENCE && hasMaximumSituationsOfType(SituationType.REFERENCE)) {
      NotificationUtil.broadcastWarning(eventBus, M.messages()
          .warningCreateTooManyReferenceSituations(applicationContext.getMaxNumberOfSituationsForType(SituationType.REFERENCE)));
      return false;
    } else if (neww.getType() == SituationType.NETTING && hasMaximumSituationsOfType(SituationType.NETTING)) {
      NotificationUtil.broadcastWarning(eventBus, M.messages()
          .warningCreateTooManyNettingSituations(applicationContext.getMaxNumberOfSituationsForType(SituationType.NETTING)));
      return false;
    } else if (hasMaxNumberOfSituations()) {
      NotificationUtil.broadcastWarning(eventBus, M.messages().warningCreateTooManySituations(applicationContext.getMaxNumberOfSituations(false)));
      return false;
    } else {
      return true;
    }
  }

  private long getAmountOfSituationsWithType(final SituationType type) {
    return scenarioContext.getSituations().stream().filter(s -> s.getType() == type).count();
  }

  private boolean hasMaximumSituationsOfType(final SituationType type) {
    return getAmountOfSituationsWithType(type) >= applicationContext.getMaxNumberOfSituationsForType(type);
  }

  private boolean hasMaxNumberOfSituations() {
    final int amountOfSituations = scenarioContext.getSituations().size();
    return amountOfSituations >= applicationContext.getMaxNumberOfSituations(false);
  }

  @EventHandler
  public void onSituationDeleteEvent(final SituationDeletedEvent e) {
    if (scenarioContext.hasSituations()) {
      // Switch to another situation if there are any
      final SituationContext situationContext = scenarioContext.getSituations().get(0);
      eventBus.fireEvent(new SwitchSituationCommand(situationContext.getSituationCode()));
    } else {
      // If no situations are left, go back to start
      placeController.goTo(new ThemeLaunchPlace(applicationContext.getActiveTheme()));
    }
  }

  /**
   * Given a situation, snap its calculation year to the closest of the given
   * years.
   *
   * @param situationContext the situation to snap the year of
   * @param calculationYears the available years
   */
  private static void snapYear(final SituationContext situation, final List<String> calculationYears, final EventBus eventBus) {
    final int intYear = Integer.parseInt(situation.getYear());

    calculationYears.stream()
        .min((o1, o2) -> Math.abs(Integer.parseInt(o1) - intYear) - Math.abs(Integer.parseInt(o2) - intYear))
        .filter(v -> !v.equals(String.valueOf(intYear)))
        .ifPresent(newYear -> {
          situation.setYear(newYear);
          NotificationUtil.broadcastMessage(eventBus, M.messages().importYearAutoSnapText(String.valueOf(intYear), newYear));
        });
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
