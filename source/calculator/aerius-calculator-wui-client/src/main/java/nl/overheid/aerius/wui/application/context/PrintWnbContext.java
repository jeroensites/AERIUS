/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.Arrays;
import java.util.List;

import com.google.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;

@Singleton
public class PrintWnbContext {

  // Ordering in this list matters, it is used to order situations in the Overview
  // component.
  public static final @JsProperty List<SituationType> PROJECT_CALCULATION_SITUATION_TYPES = Arrays.asList(
      SituationType.REFERENCE,
      SituationType.PROPOSED,
      SituationType.NETTING);

  private boolean complete = false;
  private boolean failure = false;

  private String jobId;
  private String reference;
  private String proposedSituationId;
  private ScenarioResultType scenarioResultType = ScenarioResultType.PROJECT_CALCULATION;
  private EmissionResultKey emissionResultKey = EmissionResultKey.NOXNH3_DEPOSITION;
  private SummaryHexagonType summaryHexagonType = SummaryHexagonType.EXCEEDING_HEXAGONS;

  public void finish() {
    complete = true;
  }

  public void fail() {
    failure = true;
  }

  public boolean isFailure() {
    return failure;
  }

  public boolean isComplete() {
    return complete;
  }

  public String getJobId() {
    return jobId;
  }

  public void setJobId(final String jobId) {
    this.jobId = jobId;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(final String reference) {
    this.reference = reference;
  }

  public String getProposedSituationId() {
    return proposedSituationId;
  }

  public void setProposedSituationId(final String proposedSituationId) {
    this.proposedSituationId = proposedSituationId;
  }

  public ScenarioResultType getScenarioResultType() {
    return scenarioResultType;
  }

  public void setScenarioResultType(final ScenarioResultType scenarioResultType) {
    this.scenarioResultType = scenarioResultType;
  }

  public EmissionResultKey getEmissionResultKey() {
    return emissionResultKey;
  }

  public void setEmissionResultKey(final EmissionResultKey emissionResultKey) {
    this.emissionResultKey = emissionResultKey;
  }

  public SummaryHexagonType getSummaryHexagonType() {
    return summaryHexagonType;
  }

  public void setSummaryHexagonType(final SummaryHexagonType summaryHexagonType) {
    this.summaryHexagonType = summaryHexagonType;
  }
}
