/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.components.source.road;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.v2.source.road.TrafficDirection;
import nl.overheid.aerius.wui.application.components.modify.ModifyListComponent;
import nl.overheid.aerius.wui.application.components.source.SubSourceEditorComponent;
import nl.overheid.aerius.wui.application.components.source.farmlodging.SubSourceValidationBehaviour;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceEmptyError;
import nl.overheid.aerius.wui.application.components.source.validation.SubSourceValidatedRowComponent;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;

@Component(name = "aer-road-emission-editor", components = {
    ModifyListComponent.class,
    RoadEmissionEditorRowComponent.class,
    SubSourceValidationBehaviour.class,
    SubSourceValidatedRowComponent.class,
    SubSourceEmptyError.class,
    VerticalCollapseGroup.class,
})
public class RoadEmissionEditorComponent extends SubSourceEditorComponent implements HasCreated {
  @Prop RoadESFeature source;

  @Data RoadEmissionEditorActivity presenter;
  @Data TrafficDirection toggleSelect;

  @Watch(value = "source", isImmediate = true)
  public void watchSource() {
    resetSelected();
    toggleSelect = source.getTrafficDirection() == null ? TrafficDirection.BOTH : source.getTrafficDirection();
  }

  @Computed("roadSource")
  protected RoadESFeature getRoadSource() {
    return source;
  }

  @Override
  public void created() {
    presenter = new RoadEmissionEditorActivity();
    presenter.setView(this);
  }

  @JsMethod
  protected void selectSource(final Number index, final Vehicles selectedSource) {
    if (selectedIndex.equals(index.intValue())) {
      selectedIndex = -1;
      resetSelected();
    } else {
      selectedIndex = index.intValue();
      presenter.selectedSource(selectedSource);
      editSource();
    }
  }

  @JsMethod
  protected void setToggle(final TrafficDirection trafficDirection) {
    toggleSelect = trafficDirection == null ? TrafficDirection.BOTH : trafficDirection;
    source.setTrafficDirection(trafficDirection);
  }

  @Computed("directionIconName")
  protected String getDirectionIconName() {
    String iconName = "";
    switch (toggleSelect) {
    case BOTH:
      iconName = "icon-traffic-flow-both-directions";
      break;
    case A_TO_B:
      iconName = "icon-traffic-flow-a-to-b";
      break;
    case B_TO_A:
      iconName = "icon-traffic-flow-b-to-a";
      break;
    }
    return iconName;
  }

}
