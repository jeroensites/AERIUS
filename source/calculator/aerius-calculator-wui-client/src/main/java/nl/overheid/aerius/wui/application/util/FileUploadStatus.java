/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.util;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.DOM;

import elemental2.dom.File;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.js.exception.AeriusJsExceptionMessage;
import nl.overheid.aerius.js.file.SituationStats;
import nl.overheid.aerius.js.file.ValidationStatus;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.scenario.SituationType;

public class FileUploadStatus {
  private String key = DOM.createUniqueId();
  private File file = null;
  private String fileCode = null;
  private String situationName = null;
  private SituationType situationType = null;
  private String importYear = null;
  private Substance substance = null;
  private Boolean includeResults = null;

  private double percentage = 0;

  private boolean validated = false;
  private boolean complete = false;
  private boolean failed = false;
  private boolean pending = false;

  @JsProperty private final List<AeriusJsExceptionMessage> exceptions = new ArrayList<>();
  @JsProperty private final List<AeriusJsExceptionMessage> warnings = new ArrayList<>();

  private ValidationStatus status = null;
  private SituationStats situationStats = null;

  public String getKey() {
    return key;
  }

  public void setKey(final String key) {
    this.key = key;
  }

  public File getFile() {
    return file;
  }

  public void setFile(final File file) {
    this.file = file;
  }

  public String getFileCode() {
    return fileCode;
  }

  public void setFileCode(final String fileCode) {
    this.fileCode = fileCode;
  }

  public String getSituationName() {
    return situationName;
  }

  public void setSituationName(final String situationName) {
    this.situationName = situationName;
  }

  public SituationType getSituationType() {
    return situationType;
  }

  public void setSituationType(final SituationType situationType) {
    this.situationType = situationType;
  }

  public String getImportYear() {
    return importYear;
  }

  public void setImportYear(final String importYear) {
    this.importYear = importYear;
  }

  public Substance getSubstance() {
    return substance;
  }

  public void setSubstance(final Substance substance) {
    this.substance = substance;
  }

  public Boolean getIncludeResults() {
    return includeResults;
  }

  public void setIncludeResults(final Boolean includeResults) {
    this.includeResults = includeResults;
  }

  public double getPercentage() {
    return percentage;
  }

  public void setCompleteRatio(final double percentage) {
    this.percentage = percentage;
  }

  public boolean isComplete() {
    return complete;
  }

  public void setComplete(final boolean complete) {
    this.complete = complete;
  }

  public boolean isFailed() {
    return failed;
  }

  public void setFailed(final boolean failed) {
    this.failed = failed;
  }

  public boolean isPending() {
    return pending;
  }

  public void setPending(final boolean pending) {
    this.pending = pending;
  }

  public boolean hasErrors() {
    return !exceptions.isEmpty();
  }

  public List<AeriusJsExceptionMessage> getErrors() {
    return exceptions;
  }

  public boolean hasWarnings() {
    return !warnings.isEmpty();
  }

  public List<AeriusJsExceptionMessage> getWarnings() {
    return warnings;
  }

  public void setStatus(final ValidationStatus status) {
    this.status = status;
  }

  public ValidationStatus getStatus() {
    return status;
  }

  public SituationStats getSituationStats() {
    return situationStats;
  }

  public void setSituationStats(final SituationStats situationStats) {
    this.situationStats = situationStats;
  }

  public void clear() {
    warnings.clear();
    exceptions.clear();
  }

  @Override
  public String toString() {
    return "FileUploadStatus [file=" + file.name + "]";
  }

  public boolean isValidated() {
    return validated;
  }

  public void setValidated(final boolean validated) {
    this.validated = validated;
  }

  public boolean isCalculationPointStatus() {
    return situationStats != null && situationStats.getCalculationPoints() > 0;
  }
}
