/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.building;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Collection;
import ol.Feature;
import ol.OLFactory;
import ol.geom.Point;
import ol.layer.Layer;
import ol.layer.VectorLayerOptions;
import ol.source.Cluster;
import ol.source.ClusterOptions;
import ol.source.Vector;
import ol.source.VectorOptions;
import ol.style.Icon;
import ol.style.IconOptions;
import ol.style.Style;
import ol.style.StyleOptions;

import jsinterop.base.Js;

import nl.overheid.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.geo.icon.DynamicSvgIcon;
import nl.overheid.aerius.wui.application.command.NameLabelVisibilityCommand;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingAddCommand;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingClearCommand;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.event.building.BuildingAddedEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingDeletedEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingListChangeEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingRegisterModifyGeometryEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingUpdatedEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.geo.icons.LabelSvgIconBuilder;
import nl.overheid.aerius.wui.application.geo.layers.LabelFeature;
import nl.overheid.aerius.wui.application.geo.layers.OptimizedCollection;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.i18n.M;

public class BuildingMarkerLayer implements IsLayer<Layer> {
  private static final BuildingMarkerLayerEventBinder EVENT_BINDER = GWT.create(BuildingMarkerLayerEventBinder.class);

  interface BuildingMarkerLayerEventBinder extends EventBinder<BuildingMarkerLayer> {}

  // Minimum number of pixels between source labels
  private static final int CLUSTER_MINIMUM_PIXEL_DISTANCE = 60;

  // Resolution below which to switch to labeling individual sources
  private static final double CLUSTER_MINIMUM_RESOLUTION = 1.5;

  private static String selectedFeatureId = null;

  private static final LabelSvgIconBuilder LABEL_SVG_ICON_BUILDER = new LabelSvgIconBuilder();

  private final LayerInfo info;
  private final ol.layer.Vector layer;

  private final Map<String, Collection<Feature>> featuresMap = new HashMap<>();
  private final Map<String, Map<String, LabelFeature>> labelFeaturesById = new HashMap<>();
  private final Map<String, Cluster> sources = new HashMap<>();

  private boolean showNameLabels = false;
  private final MapConfigurationContext mapConfigurationContext;

  @Inject
  public BuildingMarkerLayer(@Assisted final LayerInfo info, final EventBus eventBus, @Assisted final int zIndex,
      final MapConfigurationContext mapConfigurationContext) {
    this.info = info;
    this.mapConfigurationContext = mapConfigurationContext;

    final VectorLayerOptions vectorLayerOptions = OLFactory.createOptions();
    vectorLayerOptions.setStyle((feature, v) -> getStyleFunction(feature, v));

    layer = new ol.layer.Vector(vectorLayerOptions);
    layer.setZIndex(zIndex);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  private Collection<Feature> getFeatureCollection(final String situationId) {
    return featuresMap.get(situationId);
  }

  @EventHandler
  public void onTemporaryBuildingAddCommand(final TemporaryBuildingAddCommand c) {
    selectedFeatureId = c.getValue().getId();
    change();
  }

  @EventHandler
  public void onTemporaryBuildingClearCommand(final TemporaryBuildingClearCommand c) {
    selectedFeatureId = null;
    change();
  }

  @EventHandler
  public void onBuildingListChangeEvent(final BuildingListChangeEvent e) {
    init(e.getSituationId(), e.getValue());
  }

  @EventHandler
  public void onBuildingAddedEvent(final BuildingAddedEvent e) {
    addLabelFeature(e.getSituationId(), e.getValue());
  }

  @EventHandler
  public void onBuildingDeletedEvent(final BuildingDeletedEvent e) {
    removeLabelFeature(e.getSituationId(), e.getValue());
  }

  @EventHandler
  public void onBuildingUpdatedEvent(final BuildingUpdatedEvent e) {
    if (getFeatureCollection(e.getSituationId()) != null) {
      if (removeLabelFeature(e.getSituationId(), e.getValue())) {
        addLabelFeature(e.getSituationId(), e.getValue());
      }
    }
  }

  private void addLabelFeature(final String situationId, final BuildingFeature building) {
    final LabelFeature labelFeature = getOrCreateLabelFeature(situationId, building);
    if (labelFeature != null) {
      getFeatureCollection(situationId).push(labelFeature);
    }
  }

  private boolean removeLabelFeature(final String situationId, final BuildingFeature building) {
    final LabelFeature labelFeature = labelFeaturesById.get(situationId).remove(building.getId());
    if (labelFeature != null) {
      getFeatureCollection(situationId).remove(labelFeature);
      return true;
    }
    return false;
  }

  private void init(final String situationId, final List<BuildingFeature> features) {
    // Clean the old buildings if any
    final Vector oldVectorSource = layer.getSource();
    if (oldVectorSource != null) {
      oldVectorSource.clear(true);
    }

    // Construct labelFeature storage
    labelFeaturesById.put(situationId, new HashMap<>());

    final LabelFeature[] array = features.stream()
        .map(building -> getOrCreateLabelFeature(situationId, building))
        .filter(building -> building != null)
        .toArray(len -> new LabelFeature[len]);

    final Collection<Feature> olFeatures = new OptimizedCollection<Feature>(array);
    featuresMap.put(situationId, olFeatures);

    final VectorOptions vectorSourceOptions = OLFactory.createOptions();
    vectorSourceOptions.setFeatures((Collection<Feature>) Js.uncheckedCast(featuresMap.get(situationId)));
    final Vector vectorSource = new Vector(vectorSourceOptions);

    final ClusterOptions clusterOptions = new ClusterOptions();
    clusterOptions.setDistance(CLUSTER_MINIMUM_PIXEL_DISTANCE);
    clusterOptions.setSource(vectorSource);
    clusterOptions.setGeometryFunction(feature -> {
      if (feature.getId().equals(selectedFeatureId)) {
        return null;
      }
      return (Point) feature.getGeometry();
    });

    final Cluster cluster = new Cluster(clusterOptions);
    sources.put(situationId, cluster);
    layer.setSource(cluster);
  }

  public void onBuildingModifyGeometry(final BuildingRegisterModifyGeometryEvent e) {
    change();
  }

  private void change() {
    layer.getSource().changed();
    ((Cluster) layer.getSource()).getSource().changed();
  }

  @EventHandler
  public void onSituationSwitch(final SituationSwitchEvent e) {
    getFeatureCollection(e.getValue());
    layer.setSource(sources.get(e.getValue()));
  }

  @EventHandler
  public void onNameLabelVisibilityCommand(final NameLabelVisibilityCommand e) {
    this.showNameLabels = e.getValue();
    change();
  }

  private Style[] getStyleFunction(final Feature feature, final double resolution) {
    final LabelFeature[] features = feature.get("features");
    if (features.length == 0) {
      // This method can be called without any sources. So we just return null as
      // there is nothing to show.
      return null;
    }
    final boolean isClustered = features.length > 1 && resolution > CLUSTER_MINIMUM_RESOLUTION;

    final List<LabelSvgIconBuilder.Label> markers = new ArrayList<>();
    if (isClustered) {

      final LabelSvgIconBuilder.Label marker = new LabelSvgIconBuilder.Label(
          M.messages().buildingMarkerClusterLabel(features.length),
          LabelStyle.getLabelColor(features));
      markers.add(marker);

    } else {
      for (final LabelFeature f : features) {
        final String label = f.getId().equals(selectedFeatureId) ? f.getId() : showNameLabels ? f.getLabel() : f.getId();
        final String correctedLabel = label.startsWith("-") ? label.substring(1) : label;

        final LabelSvgIconBuilder.Label marker = new LabelSvgIconBuilder.Label(
            correctedLabel, LabelStyle.getLabelColor(f));
        markers.add(marker);
      }
    }

    final DynamicSvgIcon svg = LABEL_SVG_ICON_BUILDER.getSvgIcon(markers, false, LabelSvgIconBuilder.LabelStyle.BUILDING);

    final IconOptions iconOptions = new IconOptions();
    iconOptions.setSrc(svg.getSrc());
    iconOptions.setAnchor(svg.getAnchorInPx());
    iconOptions.setAnchorXUnits("pixels");
    iconOptions.setAnchorYUnits("pixels");
    iconOptions.setScale(mapConfigurationContext.getIconScale());
    final Icon icon = new Icon(iconOptions);

    final StyleOptions styleOptions = new StyleOptions();
    styleOptions.setImage(icon);
    final Style style = new Style(styleOptions);

    return new Style[] { style };
  }

  private LabelFeature getOrCreateLabelFeature(final String situationId, final BuildingFeature building) {
    if (labelFeaturesById.get(situationId).get(building.getId()) == null) {
      labelFeaturesById.get(situationId).put(building.getId(), createLabelFeature(building));
    }
    return labelFeaturesById.get(situationId).get(building.getId());
  }

  private LabelFeature createLabelFeature(final BuildingFeature building) {
    return LabelFeature.create(building.getId(), building.getGeometry(), building.getLabel(), LabelStyle.getLabelColor(building), building);
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public Optional<LayerInfo> getInfoOptional() {
    return Optional.of(info);
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
