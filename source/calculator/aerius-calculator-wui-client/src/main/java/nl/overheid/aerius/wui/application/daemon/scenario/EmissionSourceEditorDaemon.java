/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.List;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.config.AppThemeConfiguration;
import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceAddCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceCreateNewCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceCreateNewEvent;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCancelCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCancelEvent;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSaveCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSaveEvent;
import nl.overheid.aerius.wui.application.command.source.EmissionSourcePersistGeometryCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceSectorChangeCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSubSourceDeselectFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSubSourceSelectFeatureCommand;
import nl.overheid.aerius.wui.application.command.source.TemporaryEmissionSourceAddCommand;
import nl.overheid.aerius.wui.application.command.source.TemporaryEmissionSourceClearCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceListContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.GenericESFeature;
import nl.overheid.aerius.wui.application.domain.source.util.EmissionSourceFeatureUtil;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceUpdatedEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.EmissionSourceListPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;

public class EmissionSourceEditorDaemon extends BasicEventComponent {
  private static final EmissionSourceEditorDaemonEventBinder EVENT_BINDER = GWT.create(EmissionSourceEditorDaemonEventBinder.class);

  interface EmissionSourceEditorDaemonEventBinder extends EventBinder<EmissionSourceEditorDaemon> {}

  @Inject ApplicationContext applicationContext;
  @Inject ScenarioContext scenarioContext;
  @Inject EmissionSourceEditorContext editorContext;
  @Inject EmissionSourceListContext listContext;

  @Inject PlaceController placeController;

  private EmissionSourceFeature originalEmissionSource;
  private String temporarySituationId;
  private EmissionSourceFeature temporaryEmissionSource;

  @EventHandler
  public void onEmissionSourceCreateNewCommand(final EmissionSourceCreateNewCommand c) {
    final EmissionSourceFeature source = createNewEmissionSource();

    final List<EmissionSourceFeature> sources = scenarioContext.getActiveSituation().getSources();
    final String nextLabel = FeatureUtil.getNextLabel(sources);
    source.setLabel(M.messages().esLabelDefaultPrefix(nextLabel));

    final ConsecutiveIdUtil.IndexIdPair newId = FeatureUtil.getConsecutiveIdUtil(sources).generate();
    source.setId(newId.getId());

    beginEmissionSourceEdit(source);
  }

  @EventHandler
  public void onEmissionSourceCreateNewEvent(final EmissionSourceCreateNewEvent c) {
    placeController.goTo(new EmissionSourcePlace(applicationContext.getActiveTheme()));
  }

  @EventHandler
  public void onEmissionSourceEditCommand(final EmissionSourceEditCommand c) {
    final EmissionSourceFeature clone = FeatureUtil.clone(c.getValue());

    beginEmissionSourceEdit(clone, c.getValue());
  }

  private EmissionSourceFeature createNewEmissionSource() {
    final EmissionSourceFeature emissionSource = GenericESFeature.create(applicationContext.getActiveThemeConfiguration().getCharacteristicsType());

    final String nextLabel = FeatureUtil.getNextLabel(scenarioContext.getActiveSituation().getSources());
    emissionSource.setLabel(M.messages().esLabelDefaultPrefix(nextLabel));

    /**
     * TODO/FIXME OLD PROBLEM (doesn't exist here anymore, but the underlying issue
     * still does)
     *
     * <pre>
     *  Hmm, not very happy with a command being fired from inside, effectively, a command.
     *  But this is the many-th time I've seen it, so it must make at least intuitive sense to do so.
     *  Currently, this command is deferred until after the previous command's event is handled.
     *  Maybe have to look into re-ordering the way the command bus prioritises commands over
     *  events so that if indeed this is a common and expected pattern that no strange things
     *  can happen.
     *  i.e. for the situation where 'commandB' is fired from within the handler that handles 'commandA',
     *  maybe the order should be:
     *  commandA -> commandB -> commandA'sEvent -> commandB'sEvent
     *  rather than the current
     *  commandA -> commandA'sEvent -> commandB -> commandB'sEvent
     * </pre>
     */

    return emissionSource;
  }

  @EventHandler
  public void onEmissionSourcePersistGeometryCommand(final EmissionSourcePersistGeometryCommand c) {
    final EmissionSourceFeature source = editorContext.getSource();

    if (source == null) {
      // TODO Remove when confident this will no longer occur, this gets hit first
      // when a regression is implemented
      GWTProd.warn("Something is wishing for us to modify a source in some way, but a source isn't being edited.");
    } else {
      source.setGeometry(c.getValue());
      eventBus.fireEvent(new EmissionSourceUpdatedEvent(source, scenarioContext.getActiveSituation().getId()));
    }
  }

  @EventHandler
  public void onEmissionSourceEditSaveCommand(final EmissionSourceEditSaveCommand c) {
    if (originalEmissionSource == null) {
      // Not a copy, so purge IDs so they can be set properly
      final EmissionSourceFeature source = FeatureUtil.cloneWithoutIds(editorContext.getSource());

      eventBus.fireEvent(new EmissionSourceAddCommand(source, scenarioContext.getActiveSituation().getSituationCode()));
    } else {
      FeatureUtil.copy(editorContext.getSource(), originalEmissionSource);
      listContext.setSelection(originalEmissionSource);
      eventBus.fireEvent(new EmissionSourceUpdatedEvent(originalEmissionSource, scenarioContext.getActiveSituation().getSituationCode()));
      originalEmissionSource = null;
    }

    finishEmissionSourceEdit(editorContext.getSource());
  }

  @EventHandler
  public void onEmissionSourceEditSaveEvent(final EmissionSourceEditSaveEvent e) {
    exitEmissionSourcePlace();
  }

  @EventHandler
  public void onEmissionSourceEditCancelCommand(final EmissionSourceEditCancelCommand c) {
    finishEmissionSourceEdit(editorContext.getSource());
  }

  @EventHandler
  public void onEmissionSourceEditCancelEvent(final EmissionSourceEditCancelEvent c) {
    exitEmissionSourcePlace();
  }

  private void exitEmissionSourcePlace() {
    SchedulerUtil.delay(() -> placeController.goTo(new EmissionSourceListPlace(applicationContext.getActiveTheme())));
  }

  @EventHandler
  public void onEmissionSubSourceDeselectFeatureCommand(final EmissionSubSourceDeselectFeatureCommand c) {
    editorContext.resetSubSourceSelection();
  }

  @EventHandler
  public void onEmissionSubSourceSelectFeatureCommand(final EmissionSubSourceSelectFeatureCommand c) {
    editorContext.setSubSourceSelection(c.getValue());
  }

  @EventHandler
  public void onChangeEmissionSourceSectorCommand(final EmissionSourceSectorChangeCommand c) {
    final AppThemeConfiguration themeConfig = applicationContext.getActiveThemeConfiguration();
    final EmissionSourceFeature newSource = EmissionSourceFeatureUtil.changeEmissionsSourceFeature(c.getValue(), editorContext.getSource(),
        themeConfig.getSectorPropertiesSet(), themeConfig.getCharacteristicsType());

    swapSource(newSource);
  }

  private void swapSource(final EmissionSourceFeature newSource) {
    eventBus.fireEvent(new TemporaryEmissionSourceClearCommand(temporaryEmissionSource, temporarySituationId));
    temporaryEmissionSource = newSource;
    editorContext.setSource(newSource);
    eventBus.fireEvent(new TemporaryEmissionSourceAddCommand(temporaryEmissionSource, temporarySituationId));
  }

  private void beginEmissionSourceEdit(final EmissionSourceFeature source) {
    beginEmissionSourceEdit(source, null);
  }

  private void beginEmissionSourceEdit(final EmissionSourceFeature source, final EmissionSourceFeature original) {
    if (temporaryEmissionSource != null) {
      // Ideally we shouldn't be cleaning up (although it can be legitimate), so warn
      // about this occurring
      GWTProd.warn("Clearing temporary emission source at a point where ideally we shouldn't be doing that.");
      finishEmissionSourceEdit(temporaryEmissionSource);
    }

    editorContext.setSource(source);

    temporaryEmissionSource = source;
    temporaryEmissionSource.setId("-" + source.getId());
    temporarySituationId = scenarioContext.getActiveSituationCode();
    eventBus.fireEvent(new TemporaryEmissionSourceAddCommand(source, temporarySituationId));

    if (original != null) {
      originalEmissionSource = original;
    } else {
      originalEmissionSource = null;
    }
  }

  private void finishEmissionSourceEdit(final EmissionSourceFeature source) {
    if (temporaryEmissionSource != null) {
      eventBus.fireEvent(new TemporaryEmissionSourceClearCommand(temporaryEmissionSource, temporarySituationId));
      temporaryEmissionSource = null;
      temporarySituationId = null;
    }

    editorContext.reset();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
