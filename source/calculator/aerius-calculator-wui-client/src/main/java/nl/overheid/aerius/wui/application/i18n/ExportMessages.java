/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.i18n;

/**
 * Text specific for export page.
 */
public interface ExportMessages {
  String exportButton();
  String exportEmailLabel();
  String exportEmailPlaceholder();
  String exportErrorNoEmissions();
  String exportGMLExport();
  String exportGMLIncludingResults();
  String exportGMLScenario();
  String exportGMLWithMetaData();
  String exportPDFSituationsLabel();
  String exportPDFSituationsMissing();
  String exportTitle();
  String exportToggleGML();
  String exportToggleReport();
  String exportAlreadyRunningWarning();

  String scenarioMetaDataCity();
  String scenarioMetaDataCorporation();
  String scenarioMetaDataDescription();
  String scenarioMetaDataLocation();
  String scenarioMetaDataPostcode();
  String scenarioMetaDataProjectName();
  String scenarioMetaDataStreetAddress();
}
