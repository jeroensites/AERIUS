/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.results.habitat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.wui.application.command.HabitatTypeHoverActiveCommand;
import nl.overheid.aerius.wui.application.command.HabitatTypeHoverInactiveCommand;
import nl.overheid.aerius.wui.application.command.HabitatTypeSelectToggleCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.daemon.calculation.ClearAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSelectionContext;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSummaryContext;
import nl.overheid.aerius.wui.application.daemon.calculation.SelectAssessmentAreaCommand;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsAreaSummary;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    CollapsiblePanel.class,
    SimplifiedListBoxComponent.class,
    ButtonIcon.class,
    ResultsHabitatListView.class,
    VerticalCollapseGroup.class
})
public class ResultHabitatView extends BasicVueComponent {

  private static final Map<ScenarioResultType, List<ResultStatisticType>> habitatResultTypes = new HashMap<>();
  static {
    habitatResultTypes.put(ScenarioResultType.SITUATION_RESULT, Arrays.asList(
        ResultStatisticType.MAX_CONTRIBUTION, ResultStatisticType.MAX_TOTAL));
    habitatResultTypes.put(ScenarioResultType.PROJECT_CALCULATION, Arrays.asList(
        ResultStatisticType.MAX_INCREASE, ResultStatisticType.MAX_DECREASE, ResultStatisticType.MAX_TOTAL));
    habitatResultTypes.put(ScenarioResultType.MAX_TEMPORARY_CONTRIBUTION, Arrays.asList(
        ResultStatisticType.MAX_TEMP_CONTRIBUTION, ResultStatisticType.MAX_TOTAL));
    habitatResultTypes.put(ScenarioResultType.MAX_TEMPORARY_EFFECT, Arrays.asList(
        ResultStatisticType.MAX_TEMP_INCREASE, ResultStatisticType.MAX_TOTAL));
  }

  @Prop EventBus eventBus;
  @Prop(required = true) ResultSummaryContext context;

  @Data @Inject CalculationContext calculationContext;
  @Data @Inject ApplicationContext applicationContext;
  @Data @Inject ResultSelectionContext selectionContext;

  private final static List<ResultStatisticType> PROJECT_RESULTS = new ArrayList<>();
  private final static List<ResultStatisticType> SITUATION_RESULTS = new ArrayList<>();
  static {
    PROJECT_RESULTS.add(ResultStatisticType.MAX_TOTAL);
    PROJECT_RESULTS.add(ResultStatisticType.MAX_DECREASE);
    PROJECT_RESULTS.add(ResultStatisticType.MAX_INCREASE);

    SITUATION_RESULTS.add(ResultStatisticType.MAX_CONTRIBUTION);
    SITUATION_RESULTS.add(ResultStatisticType.MAX_TOTAL);
  }

  @Computed
  public DepositionValueDisplayType getDepositionType() {
    return applicationContext.getActiveThemeConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @JsMethod
  public void toggleHabitatSelection(final String habitatTypeCode) {
    eventBus.fireEvent(new HabitatTypeSelectToggleCommand(habitatTypeCode));
  }

  @JsMethod
  public void setHabitatHighlightActive(final String habitatTypeCode) {
    eventBus.fireEvent(new HabitatTypeHoverActiveCommand(habitatTypeCode));
  }

  @JsMethod
  public void setHabitatHighlightInactive(final String habitatTypeCode) {
    eventBus.fireEvent(new HabitatTypeHoverInactiveCommand(habitatTypeCode));
  }

  @Computed
  public List<ResultStatisticType> getHabitatStatisticTypes() {
    return Optional.ofNullable(habitatResultTypes.get(getResultContext().getResultType())).orElse(new ArrayList<>());
  }

  @Computed
  public CalculationJobContext getCalculationJob() {
    return calculationContext.getActiveCalculation();
  }

  @JsMethod
  public ResultSummaryContext getResultContext() {
    return Optional.ofNullable(getCalculationJob())
        .map(v -> v.getResultContext())
        .orElseThrow(() -> new RuntimeException("Could not retrieve result context."));
  }

  @JsMethod
  public void openPanel(final SituationResultsAreaSummary summary) {
    final int id = summary.getAssessmentArea().getId();
    eventBus.fireEvent(new SelectAssessmentAreaCommand(id));
  }

  @JsMethod
  public void closePanel() {
    eventBus.fireEvent(new ClearAssessmentAreaCommand());
  }

  @JsMethod
  public boolean isOpen(final int id) {
    return id == selectionContext.getSelectedAssessmentArea();
  }

  @JsMethod
  public void selectHabitatStatisticsType(final ResultStatisticType type) {
    eventBus.fireEvent(new SelectResultHabitatStatisticTypeCommand(type));
  }

  @JsMethod
  public void zoomToArea(final SituationResultsAreaSummary areaSummary) {
    eventBus.fireEvent(GeoUtil.createMapSetExtendCommand(areaSummary.getAssessmentArea().getBounds()));
  }

  public static Map<ScenarioResultType, List<ResultStatisticType>> getHabitatStatisticsTypes() {
    return Collections.unmodifiableMap(habitatResultTypes);
  }
}
