/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.resources.util;

import java.util.Locale;

public final class FontIconUtil {

  private FontIconUtil() {
    // Util class
  }
  /**
   * Returns the icon class name as found in the iconcs.css file:
   * <pre>icon-enum</pre>
   *
   * @param enm enum representing the icon
   * @return CSS class name for the icon.
   */
  public static String fontClassName(final Enum<?> enm) {
    return fontClassName(enumname(enm));
  }

  /**
   * Returns the icon class name as found in the iconcs.css file.
   * <pre>icon-prefix-enum</pre>
   *
   * @param prefix prefix for the enum
   * @param enm enum representing the icon
   * @return CSS class name for the icon.
   */
  public static String fontClassName(final String prefix, final Enum<?> enm) {
    return fontClassName(prefix, enumname(enm));
  }

  private static String enumname(final Enum<?> enm) {
    return enm.name().replace('_', '-').toLowerCase(Locale.ROOT);
  }

  /**
   * Returns the icon class name as found in the iconcs.css file.
   * <pre>icon-prefix-name</pre>
   *
   * @param prefix prefix for the name
   * @param name second part of the icon name
   * @return CSS class name for the icon.
   */
  public static String fontClassName(final String prefix, final String name) {
    return fontClassName(prefix + "-" + name);
  }

  /**
   * Returns the icon class name as found in the iconcs.css file.
   * <pre>icon-name</pre>
   *
   * @param name name part of the icon name
   * @return CSS class name for the icon.
   */
  public static String fontClassName(final String name) {
    return "icon-" + name;
  }

}
