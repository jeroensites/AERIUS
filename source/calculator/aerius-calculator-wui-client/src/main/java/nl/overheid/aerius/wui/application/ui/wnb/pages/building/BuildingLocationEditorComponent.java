/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.building;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.format.Wkt;
import ol.geom.Circle;
import ol.geom.Geometry;
import ol.geom.GeometryType;
import ol.geom.Point;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.dev.GWTProd;
import nl.overheid.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.wui.application.command.building.BuildingDrawGeometryCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingModifyGeometryCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.event.building.BuildingDrawGeometryEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingEndModifyGeometryEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingStartModifyGeometryEvent;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.geo.util.OrientedEnvelope;
import nl.overheid.aerius.wui.application.ui.main.VerticalCollapse;
import nl.overheid.aerius.wui.application.ui.wnb.pages.building.BuildingLocationEditorValidators.BuildingLocationEditorValidations;

@Component(customizeOptions = {
    BuildingLocationEditorValidators.class
}, components = {
    BuildingEnvelopeComponent.class,
    TooltipComponent.class,
    LabeledInputComponent.class,
    ButtonIcon.class,
    VerticalCollapse.class,
    ValidationBehaviour.class,
}, directives = {
    ValidateDirective.class
})
public class BuildingLocationEditorComponent extends ErrorWarningValidator implements IsVueComponent, HasCreated, HasDestroyed {
  private static final BuildingLocationEditorComponentEventBinder EVENT_BINDER = GWT.create(BuildingLocationEditorComponentEventBinder.class);

  interface BuildingLocationEditorComponentEventBinder extends EventBinder<BuildingLocationEditorComponent> {}

  private static final Wkt WKT = new Wkt();

  @Prop BuildingFeature building;
  @Prop EventBus eventBus;

  @Data String bagSearchQuery;

  @Data String wktGeometryV;

  @Data GeometryType geometryType;

  @Data String heightV;
  @Data String radiusV;
  @JsProperty(name = "$v") BuildingLocationEditorValidations validation;

  private boolean embargo;
  private HandlerRegistration registration;

  @Data Geometry drawGeometry;

  @Computed
  public BuildingLocationEditorValidations getV() {
    return validation;
  }

  @Computed
  public String getWktGeometry() {
    return wktGeometryV;
  }

  @Computed
  public void setWktGeometry(final String wktGeometry) {
    wktGeometryV = wktGeometry;

    try {
      drawGeometry = WKT.readGeometry(wktGeometry);
      if (embargo) {
        updateValidity();
        return;
      }

      if (GeoType.POINT.is(drawGeometry)) {
        if (building.getRadius() == null) {
          // Warn, this happens for mock buildings, but _can_ conveivably happen for
          // ordinary building and botched imports
          GWTProd.warn("Radius is null while geometry is a point.", building);
          building.setRadius(0D);
        }

        // Set Point wkt as Circle geometry
        building.setGeometry(new Circle(((Point) drawGeometry).getCoordinates(), building.getRadius()));
      } else {
        // TODO Persist these things via event instead?
        building.setRadius(null);
        building.setGeometry(drawGeometry);
      }
    } catch (final Exception e) {
      // Eat
    }

    updateValidity();
  }

  @EventHandler
  public void onBuildingStartModifyGeometryEvent(final BuildingStartModifyGeometryEvent e) {
    embargo = true;
  }

  @EventHandler
  public void onBuildingEndModifyGeometryEvent(final BuildingEndModifyGeometryEvent e) {
    embargo = false;
  }

  @Watch(value = "building", isImmediate = true, isDeep = true)
  public void onBuildingChangeDeep(final BuildingFeature neww) {
    if (neww == null) {
      return;
    }

    updateGeometry(neww.getGeometry());
  }

  @Watch(value = "building", isImmediate = true)
  public void onBuildingChange(final BuildingFeature neww) {
    if (neww == null) {
      return;
    }

    geometryType = Js.isTruthy(neww.getRadius()) ? GeometryType.Circle : GeometryType.Polygon;
    heightV = String.valueOf(neww.getHeight());
    radiusV = neww.getRadius() == null ? "" : String.valueOf(neww.getRadius());
    updateGeometry(neww.getGeometry());

    eventBus.fireEvent(new BuildingModifyGeometryCommand(neww));
    eventBus.fireEvent(new BuildingDrawGeometryCommand(geometryType));
  }

  private void updateGeometry(final Geometry geometry) {
    if (geometry == null) {
      wktGeometryV = null;
    } else {
      if (GeoType.CIRCLE.is(geometry)) {
        // Display circle as point
        wktGeometryV = OL3GeometryUtil.toWktString(new Point(((Circle) geometry).getCenter()));
      } else {
        wktGeometryV = OL3GeometryUtil.toWktString(geometry);
        drawGeometry = WKT.readGeometry(wktGeometryV);
      }
    }

    updateValidity();
  }

  @Computed
  protected String getRadius() {
    return radiusV;
  }

  @Computed
  protected void setRadius(final String radius) {
    ValidationUtil.setSafeDoubleValue(v -> {
      // Only set radius if the geometry is actually a circle (this covers the case
      // where the user selects the circle tool, but has not yet created a geometry)
      if (GeoType.CIRCLE.is(building.getGeometry())) {
        building.setRadius(v);
        building.setGeometry(new Circle(((Circle) building.getGeometry()).getCenter(), building.getRadius()));
      }
    }, radius, 0D);
    radiusV = radius;
  }

  @Computed
  protected String getBuildingHeight() {
    return heightV;
  }

  @Computed
  protected void setBuildingHeight(final String height) {
    ValidationUtil.setSafeDoubleValue(v -> building.setHeight(v), height, 0D);
    heightV = height;
  }

  @JsMethod
  public boolean isValidWktGeometry() {
    try {
      final Geometry geometry = WKT.readGeometry(wktGeometryV);

      return isCircularBuilding() && GeoType.POINT.is(geometry)
          || isPolygonBuilding() && GeoType.POLYGON.is(geometry);
    } catch (final Exception e) {
      return false;
    }
  }

  @JsMethod
  public boolean isValidGeometry() {
    try {
      final OrientedEnvelope envelope = GeoUtil.determineOrientedEnvelope(building.getGeometry());

      // A null envelope is not valid
      return envelope != null;
    } catch (final Exception e) {
      return false;
    }
  }

  private void updateValidity() {
    final boolean valid = isValidWktGeometry()
        && isValidGeometry()
        && !validation.invalid;

    vue().$emit("validity-change", valid);
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
    registration = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void destroyed() {
    registration.removeHandler();
  }

  @JsMethod
  public void addCircleBuilding() {
    // Do nothing if already drawing circle
    if (geometryType == GeometryType.Circle) {
      return;
    }

    geometryType = GeometryType.Circle;
    // Initialize with present geometry if it is a circle (this sets the radius)
    if (GeoType.CIRCLE.is(building.getGeometry())) {
      eventBus.fireEvent(new BuildingDrawGeometryEvent(building.getGeometry()));
    }

    eventBus.fireEvent(new BuildingDrawGeometryCommand(geometryType));
    eventBus.fireEvent(new BuildingModifyGeometryCommand(building));
  }

  @JsMethod
  public void addPolygonBuilding() {
    // Do nothing if already drawing polygon
    if (geometryType == GeometryType.Polygon) {
      return;
    }

    geometryType = GeometryType.Polygon;
    building.setRadius(null);
    eventBus.fireEvent(new BuildingDrawGeometryCommand(geometryType));
    eventBus.fireEvent(new BuildingModifyGeometryCommand(building));
  }

  @JsMethod
  protected String getHeightValidationError() {
    return i18n.ivDoubleRangeBetween(i18n.buildingLocationHeight(),
        OPSLimits.SCOPE_BUILDING_OUTFLOW_HEIGHT_MINIMUM, OPSLimits.SCOPE_BUILDING_OUTFLOW_HEIGHT_MAXIMUM);
  }

  @Computed("showGeometryError")
  public boolean showGeometryError() {
    return wktGeometryV != null && !wktGeometryV.isEmpty() && !isValidWktGeometry();
  }

  @Computed("isCircularBuilding")
  public boolean isCircularBuilding() {
    return geometryType == GeometryType.Circle;
  }

  @Computed("isPolygonBuilding")
  public boolean isPolygonBuilding() {
    return geometryType == GeometryType.Polygon;
  }
}
