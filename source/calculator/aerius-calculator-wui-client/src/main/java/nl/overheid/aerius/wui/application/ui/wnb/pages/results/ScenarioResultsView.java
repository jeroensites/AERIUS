/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.results;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.directives.VectorDirective;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.wui.application.command.ChangeSituationResultKeyCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSummaryContext;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultViewContext;
import nl.overheid.aerius.wui.application.domain.calculation.CalculationInfo;
import nl.overheid.aerius.wui.application.domain.calculation.JobProgress;
import nl.overheid.aerius.wui.application.domain.result.CustomCalculationPointResult;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsAreaSummary;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsSummary;
import nl.overheid.aerius.wui.application.event.ResultsViewSelectionCommand;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.custompoints.CustomPointsView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.custompoints.CustomPointsViewFactory;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.graph.ResultGraphView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.graph.ResultGraphViewFactory;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.habitat.ResultHabitatView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.habitat.ResultHabitatViewFactory;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.habitat.ResultsHabitatListView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.key.SituationResultKeyCompositor;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.progress.CalculationView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.stats.ResultsStatisticsView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.tab.TabViewComponent;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.table.ResultTableView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.table.ResultTableViewFactory;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    ButtonIcon.class,
    CalculationView.class,
    CollapsiblePanel.class,
    ResultsHabitatListView.class,
    ResultsStatisticsView.class,
    SimplifiedListBoxComponent.class,
    VerticalCollapse.class,
    TooltipComponent.class,
    SituationResultKeyCompositor.class,

    TabViewComponent.class,
    ResultGraphView.class,
    ResultTableView.class,
    ResultHabitatView.class,
    CustomPointsView.class,
}, directives = {
    VectorDirective.class,
})
public class ScenarioResultsView extends BasicVueEventComponent {
  @Prop EventBus eventBus;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data CalculationContext calculationContext;
  @Inject @Data ResultSummaryContext resultSummaryContext;

  @Inject @Data ResultViewContext viewContext;

  @Computed("isCalculationSourceDiscrepancy")
  public boolean isCalculationSourceDiscrepancy() {
    return Optional.ofNullable(getCalculationJob())
        .map(v -> v.getInputHash())
        .map(v -> !v.equals(ScenarioContext.calculateHash(scenarioContext)))
        .orElse(false);
  }

  @JsMethod
  public void selectSituationResultKey(final SituationResultsKey key) {
    eventBus.fireEvent(new ChangeSituationResultKeyCommand(key));
  }

  @Computed
  public List<ResultView> getResultsViewTabs() {
    if (getResultContext().getHexagonType() == SummaryHexagonType.CUSTOM_CALCULATION_POINTS) {
      return Collections.emptyList();
    } else {
      return applicationContext.getActiveThemeConfiguration().getResultViews();
    }
  }

  @JsMethod
  public String getResultViewComponent(final ResultView tab) {
    if (getResultContext().getHexagonType() == SummaryHexagonType.CUSTOM_CALCULATION_POINTS) {
      return CustomPointsViewFactory.get().getComponentTagName();
    }

    if (tab == null) {
      return "";
    }

    switch (tab) {
    case DEPOSITION_DISTRIBUTION:
      return ResultGraphViewFactory.get().getComponentTagName();
    case MARKERS:
      return ResultTableViewFactory.get().getComponentTagName();
    case HABITAT_TYPES:
      return ResultHabitatViewFactory.get().getComponentTagName();
    default:
      return null;
    }
  }

  @Computed
  public List<ResultStatisticType> getHabitatStatisticTypes() {
    final List<ResultStatisticType> situationResults = new ArrayList<>();
    situationResults.add(ResultStatisticType.MAX_CONTRIBUTION);
    situationResults.add(ResultStatisticType.MAX_TOTAL);

    final List<ResultStatisticType> projectResults = new ArrayList<>();
    projectResults.add(ResultStatisticType.MAX_TOTAL);
    projectResults.add(ResultStatisticType.MAX_DECREASE);
    projectResults.add(ResultStatisticType.MAX_INCREASE);

    return getResultContext().getResultType() == ScenarioResultType.PROJECT_CALCULATION
        ? projectResults
        : situationResults;
  }

  @JsMethod
  public void selectTab(final ResultView tab) {
    eventBus.fireEvent(new ResultsViewSelectionCommand(tab));
  }

  @JsMethod
  public boolean hasAnyResultValue() {
    return Optional.ofNullable(getResultsSummary())
        .map(v -> v.getStatistics())
        .isPresent();
  }

  @JsMethod
  public boolean hasResults() {
    final Optional<SituationResultsAreaSummary[]> areaResults = Optional.ofNullable(getResultsSummary())
        .map(v -> v.getAreaStatistics());

    final Optional<CustomCalculationPointResult[]> pointResults = Optional.ofNullable(getResultsSummary())
        .map(v -> v.getCustomPointResults());

    return (areaResults.isPresent() && areaResults.get().length > 0) ||
        (pointResults.isPresent() && pointResults.get().length > 0);
  }

  @Computed
  public CalculationJobContext getCalculationJob() {
    return calculationContext.getActiveCalculation();
  }

  @Computed
  public CalculationInfo getCalculationInfo() {
    return Optional.ofNullable(getCalculationJob())
        .map(v -> v.getCalculationInfo())
        .orElseThrow(() -> new RuntimeException("Could not retrieve calculation info."));
  }

  @Computed
  public JobProgress getJobProgress() {
    return Optional.ofNullable(getCalculationInfo())
        .map(v -> v.getJobProgress())
        .orElseGet(() -> new JobProgress());
  }

  @Computed
  public ResultSummaryContext getResultContext() {
    return Optional.ofNullable(getCalculationJob())
        .map(v -> v.getResultContext())
        .orElseThrow(() -> new RuntimeException("Could not retrieve result context."));
  }

  @Computed
  public SituationResultsSummary getResultsSummary() {
    return Optional.ofNullable(getCalculationJob())
        .map(v -> v.getResultContext())
        .map(v -> v.getActiveResultSummary())
        .orElse(null);
  }
}
