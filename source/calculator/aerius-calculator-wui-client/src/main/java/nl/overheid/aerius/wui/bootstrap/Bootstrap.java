/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.bootstrap;

import java.util.Date;

import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.VueGWT;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Document;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.util.ExceptionHelper;
import nl.overheid.aerius.wui.Application;
import nl.overheid.aerius.wui.base.i18n.BaseM;
import nl.overheid.aerius.wui.vue.DebugDirectiveOptions;

public class Bootstrap implements EntryPoint {
  @Override
  public void onModuleLoad() {
    final long startTime = new Date().getTime();
    GWT.setUncaughtExceptionHandler(e -> displayBootError(e));
    GWTProd.info("Hello! Welcome to the AERIUS Console!");

    VueGWT.initWithoutVueLib();
    VueGWT.onReady(() -> {
      initVueLibraries();
      registerDirectives();

      GWTProd.info("Vue initialised in " + (new Date().getTime() - startTime) + "ms");

      GWT.runAsync(new RunAsyncCallback() {
        @Override
        public void onFailure(final Throwable caught) {
          GWTProd.error("Bootstrapper failed. " + caught.getMessage());
        }

        @Override
        public void onSuccess() {
          GWTProd.info("Bootstrapper initialised in " + (new Date().getTime() - startTime) + "ms");

          Scheduler.get().scheduleFinally(() -> {
            Application.A.create(() -> GWTProd.info("Application loaded in " + (new Date().getTime() - startTime) + "ms"));
          });
        }
      });
    });
  }

  public static void displayBootError(final Throwable e) {
    final String errorMessage = BaseM.getErrorMessage(ExceptionHelper.findCause(e));

    GWTProd.error("Aborting startup due to fatal error: ", errorMessage, e);
    Document.get().getElementById("splash-loader").removeFromParent();
    Document.get().getElementById("splash-message").setInnerText(errorMessage);
  }

  private void registerDirectives() {
    Vue.directive("debug", new DebugDirectiveOptions());
  }

  private void initVueLibraries() {
    // Vue.component("v-select", VueSelectComponentFactory.get());
  }
}
