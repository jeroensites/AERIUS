/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.nav;

import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasActivated;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import elemental2.dom.HTMLElement;

import nl.aerius.wui.easter.EasterUtil;
import nl.aerius.wui.easter.EasterUtil.EasterAttachment;
import nl.aerius.wui.event.PlaceChangeEvent;
import nl.aerius.wui.place.Place;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.application.components.logo.AeriusLogoComponent;
import nl.overheid.aerius.wui.application.components.nav.selector.SelectorItem;
import nl.overheid.aerius.wui.application.components.nav.selector.SelectorItemComponent;
import nl.overheid.aerius.wui.application.context.NavigationContext;

@Component(components = {
    AeriusLogoComponent.class,
    SelectorItemComponent.class,
    NavigationItemComponent.class,
    ButtonItemComponent.class,
})
public class NavigationComponent implements IsVueComponent, HasActivated, HasCreated, HasMounted {
  interface NavigationComponentEventBinder extends EventBinder<NavigationComponent> {}

  private final NavigationComponentEventBinder EVENT_BINDER = GWT.create(NavigationComponentEventBinder.class);

  @Prop(required = true) EventBus eventBus;

  @Inject @Data NavigationContext context;

  @Data Place place;

  @Inject PlaceController placeController;

  @Ref HTMLElement easter;
  @Data EasterAttachment easterAttachment = null;

  @Computed
  public String getEasterText() {
    return easterAttachment == null ? "-" : easterAttachment.getCount() < 3 ? String.valueOf(easterAttachment.getCount()) : "-";
  }

  @Override
  public void mounted() {
    easterAttachment = EasterUtil.attachTo(easter, eventBus);
  }

  @Computed
  public List<SelectorItem> getSelectorItems() {
    return context.getSelectorItems();
  }

  @Computed
  public List<NavigationItem> getNavigationItems() {
    return context.getNavigationItems();
  }

  @Computed
  public List<ButtonItem> getButtonItems() {
    return context.getButtonItems();
  }

  @EventHandler
  public void onPlaceChangeEvent(final PlaceChangeEvent e) {
    place = e.getValue();
  }

  @Override
  public void created() {
    activated();
  }

  @Override
  public void activated() {
    place = placeController.getPlace();

    if (eventBus != null) {
      EVENT_BINDER.bindEventHandlers(this, eventBus);
    }
  }
}
