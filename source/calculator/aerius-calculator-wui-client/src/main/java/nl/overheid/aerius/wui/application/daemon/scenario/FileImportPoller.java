/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.google.web.bindery.event.shared.EventBus;

import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.command.ImportSituationParcelCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.AddCalculationPointsCommand;
import nl.overheid.aerius.wui.application.command.situation.SwitchSituationCommand;
import nl.overheid.aerius.wui.application.components.importer.ApplicationImportModalComponent.ImportFilter;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcel;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcelConverter;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.event.ImportCompleteEvent;
import nl.overheid.aerius.wui.application.event.ImportFailureEvent;
import nl.overheid.aerius.wui.application.service.ScenarioServiceAsync;
import nl.overheid.aerius.wui.application.util.FileUploadStatus;

/**
 * Poller to retrieve imported situations from the server.
 *
 * Situations are identified by the file code as returned when the file(s)
 * containing the situation information was uploaded.
 */
public class FileImportPoller {
  private static final int INTERVAL = 1000;
  @Inject ScenarioServiceAsync service;
  @Inject ImportParcelConverter converter;
  @Inject private ScenarioContext scenarioContext;
  @Inject private EventBus eventBus;
  private List<FileUploadStatus> files;
  private String firstFileCode;
  private boolean running;

  private int count;

  public void start(final List<FileUploadStatus> files, final boolean saveIntoCurrentSituation, final ImportFilter importFilter) {
    count = files.size();
    if (running) {
      return;
    }
    this.files = files;
    firstFileCode = files.get(0).getFileCode();
    running = true;
    SchedulerUtil.delay(() -> this.doFetch(saveIntoCurrentSituation, importFilter));
  }

  public boolean isRunning() {
    return running;
  }

  private void doFetch(final boolean saveIntoCurrentSituation, final ImportFilter importFilter) {
    final FileUploadStatus file = files.get(0);
    service.importSituation(file.getFileCode(), AppAsyncCallback.create(parcel -> {
      tryProcess(file, parcel, saveIntoCurrentSituation, importFilter);
    }, e -> {
      running = false;
      eventBus.fireEvent(new ImportFailureEvent());
    }));
  }

  private void tryProcess(final FileUploadStatus file, final ImportParcel parcel, final boolean addToCurrent, final ImportFilter importFilter) {
    if (parcel == null) {
      schedule(addToCurrent, importFilter);
    } else {
      process(file, parcel, addToCurrent, importFilter);
    }
  }

  private void process(final FileUploadStatus file, final ImportParcel parcel, final boolean addToCurrent, final ImportFilter importFilter) {
    files.remove(file);

    // A parcel either contains calculation points, or emission sources and/or buildings for a situation.
    final CalculationPointFeature[] calculationPoints = converter.getCalculationPoints(parcel.getCalculationPoints());
    if (calculationPoints.length > 0) {
      eventBus.fireEvent(new AddCalculationPointsCommand(Arrays.asList(calculationPoints)));
    } else if (addToCurrent) {
      final SituationContext sc = this.scenarioContext.getActiveSituation();
      converter.addParcelToSituation(parcel.getSituation(), sc, importFilter);
    } else {
      constructSituationContext(file, parcel, importFilter);
    }

    // Metadata from a parcel is always imported
    converter.addMetaData(parcel, scenarioContext);
    finishImport(addToCurrent, importFilter);
  }

  private void constructSituationContext(final FileUploadStatus file, final ImportParcel parcel, final ImportFilter importFilter) {
    // Hand off as an import command
    eventBus.fireEvent(new ImportSituationParcelCommand(sc -> {
      converter.convertParcelIntoSituation(parcel.getSituation(), sc, importFilter);
      sc.setType(file.getSituationType());

      if (firstFileCode.equals(file.getFileCode())) {
        eventBus.fireEvent(new SwitchSituationCommand(sc.getSituationCode()));
      }
    }));
  }

  private void finishImport(final boolean addToCurrent, final ImportFilter importFilter) {
    if (files.isEmpty()) {
      running = false;
      eventBus.fireEvent(new ImportCompleteEvent(count));
    } else {
      doFetch(addToCurrent, importFilter);
    }
  }

  private void schedule(final boolean addToCurrent, final ImportFilter importFilter) {
    SchedulerUtil.delay(() -> doFetch(addToCurrent, importFilter), INTERVAL);
  }
}
