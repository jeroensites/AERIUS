/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.List;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.geom.Circle;
import ol.geom.Geometry;

import jsinterop.base.Js;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.util.ConsecutiveIdUtil;
import nl.overheid.aerius.wui.application.command.building.BuildingAddCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingCreateNewCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingCreateNewEvent;
import nl.overheid.aerius.wui.application.command.building.BuildingEditCancelCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditCancelEvent;
import nl.overheid.aerius.wui.application.command.building.BuildingEditCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditEvent;
import nl.overheid.aerius.wui.application.command.building.BuildingEditSaveCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditSaveEvent;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingAddCommand;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingClearCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.BuildingEditorContext;
import nl.overheid.aerius.wui.application.context.BuildingListContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.source.ADMSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.event.building.BuildingAddedEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingDrawGeometryEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingUpdatedEvent;
import nl.overheid.aerius.wui.application.geo.util.GeoType;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.BuildingPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourceListPlace;
import nl.overheid.aerius.wui.application.place.EmissionSourcePlace;

public class BuildingEditorDaemon extends BasicEventComponent {
  private static final BuildingEditorDaemonEventBinder EVENT_BINDER = GWT.create(BuildingEditorDaemonEventBinder.class);

  interface BuildingEditorDaemonEventBinder extends EventBinder<BuildingEditorDaemon> {}

  @Inject PlaceController placeController;

  @Inject EmissionSourceEditorContext sourceEditorContext;
  @Inject BuildingEditorContext context;
  @Inject BuildingListContext listContext;
  @Inject ScenarioContext scenarioContext;
  @Inject ApplicationContext applicationContext;

  private BuildingFeature originalBuilding;
  private String temporarySituationId;
  private BuildingFeature temporaryBuilding;

  @EventHandler
  public void onBuildingCreateNewCommand(final BuildingCreateNewCommand c) {
    final BuildingFeature building = BuildingFeature.create();

    final List<BuildingFeature> buildings = scenarioContext.getActiveSituation().getBuildings();
    final String nextLabel = FeatureUtil.getNextLabel(buildings);
    building.setLabel(M.messages().buildingLabelDefaultPrefix(nextLabel));

    final ConsecutiveIdUtil.IndexIdPair newId = FeatureUtil.getConsecutiveIdUtil(buildings).generate();
    building.setId(newId.getId());

    beginBuildingEdit(building);
  }

  @EventHandler
  public void onBuildingEditCommand(final BuildingEditCommand c) {
    final BuildingFeature clone = FeatureUtil.clone(c.getValue());

    beginBuildingEdit(clone, c.getValue());
  }

  @EventHandler
  public void onBuildingEditEvent(final BuildingEditEvent e) {
    final BuildingPlace place = new BuildingPlace(applicationContext.getActiveTheme());
    place.setBuildingId(e.getValue().getGmlId());
    placeController.goTo(place);
  }

  @EventHandler
  public void onBuildingCreateNewEvent(final BuildingCreateNewEvent e) {
    placeController.goTo(new BuildingPlace(applicationContext.getActiveTheme()));
  }

  @EventHandler
  public void onBuildingDrawGeometryEvent(final BuildingDrawGeometryEvent e) {
    if (!context.isEditing()) {
      return;
    }

    final BuildingFeature building = context.getBuilding();
    final Geometry geometry = e.getValue();
    if (GeoType.CIRCLE.is(geometry)) {
      building.setRadius(((Circle) geometry).getRadius());
    }

    building.setGeometry(geometry);
    eventBus.fireEvent(new BuildingUpdatedEvent(building, scenarioContext.getActiveSituationCode()));
  }

  @EventHandler
  public void onBuildingEditSaveCommand(final BuildingEditSaveCommand c) {
    if (originalBuilding == null) {
      eventBus.fireEvent(new BuildingAddCommand(context.getBuilding()));
    } else {
      FeatureUtil.copy(context.getBuilding(), originalBuilding);
      listContext.setSingleSelection(originalBuilding);
      eventBus.fireEvent(new BuildingUpdatedEvent(originalBuilding, scenarioContext.getActiveSituationCode()));
      originalBuilding = null;
    }

    finishBuildingEdit(context.getBuilding());
  }

  @EventHandler
  public void onBuildingAddedEvent(final BuildingAddedEvent c) {
    listContext.setSingleSelection(c.getValue());
  }

  @EventHandler
  public void onBuildingEditSaveEvent(final BuildingEditSaveEvent e) {
    exitBuildingPlace();
  }

  @EventHandler
  public void onBuildingEditCancelCommand(final BuildingEditCancelCommand c) {
    finishBuildingEdit(context.getBuilding());
  }

  @EventHandler
  public void onBuildingEditCancelEvent(final BuildingEditCancelEvent e) {
    exitBuildingPlace();
  }

  private void exitBuildingPlace() {
    // TODO What was feared in EmissionSourceEditorDaemon:120 has come to pass,
    // we'll need to change command/event subcommand/subevent order in order to get
    // this to work without a delay
    SchedulerUtil.delay(() -> {
      // If a source is being edited, go there
      if (sourceEditorContext.isEditing()) {
        final EmissionSourceFeature source = sourceEditorContext.getSource();

        if (source.getCharacteristicsType() == CharacteristicsType.OPS) {
          final OPSCharacteristics chars = Js.uncheckedCast(source.getCharacteristics());

          chars.setBuildingId(context.getBuilding().getGmlId());
        } else if (source.getCharacteristicsType() == CharacteristicsType.ADMS) {
          final ADMSCharacteristics chars = Js.uncheckedCast(source.getCharacteristics());

          chars.setBuildingId(context.getBuilding().getGmlId());
        }
        if (source.getCharacteristicsType() != null) {
          final EmissionSourcePlace place = new EmissionSourcePlace(applicationContext.getActiveTheme());
          place.setEmissionSourceId(source.getId());
          placeController.goTo(place);
          return;
        }
      }

      // Else go to the emission source overview
      // TODO Create explicit building overview place? or denote this state in some
      // other way
      placeController.goTo(new EmissionSourceListPlace(applicationContext.getActiveTheme()));
    });
  }

  /**
   * Begin editing a building.
   */
  private void beginBuildingEdit(final BuildingFeature building) {
    beginBuildingEdit(building, null);
  }

  private void beginBuildingEdit(final BuildingFeature building, final BuildingFeature original) {
    if (temporaryBuilding != null) {
      // Ideally we shouldn't be cleaning up (although it can be legitimate), so warn
      // about this occurring
      GWTProd.warn("Clearing temporary building at a point where ideally we shouldn't be doing that.");
      finishBuildingEdit(temporaryBuilding);
    }

    context.setBuilding(building);

    temporaryBuilding = building;
    temporaryBuilding.setId("-" + building.getId());
    temporarySituationId = scenarioContext.getActiveSituationCode();
    eventBus.fireEvent(new TemporaryBuildingAddCommand(building, temporarySituationId));

    if (original != null) {
      originalBuilding = original;
    } else {
      originalBuilding = null;
    }
  }

  private void finishBuildingEdit(final BuildingFeature building) {
    if (temporaryBuilding != null) {
      eventBus.fireEvent(new TemporaryBuildingClearCommand(temporaryBuilding, temporarySituationId));
      temporaryBuilding = null;
      temporarySituationId = null;
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
