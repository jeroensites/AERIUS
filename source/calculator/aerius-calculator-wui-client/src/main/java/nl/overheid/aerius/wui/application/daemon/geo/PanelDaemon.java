/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.geo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.search.wui.event.SearchSuggestionSelectionEvent;
import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.wui.application.command.InfoPopupHiddenCommand;
import nl.overheid.aerius.wui.application.command.InfoPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.InfoPopupVisibleCommand;
import nl.overheid.aerius.wui.application.command.LayerPopupHiddenCommand;
import nl.overheid.aerius.wui.application.command.LayerPopupToggleCommand;
import nl.overheid.aerius.wui.application.command.LayerPopupVisibleCommand;
import nl.overheid.aerius.wui.application.command.SearchPopupToggleCommand;
import nl.overheid.aerius.wui.application.context.InfoPanelContext;
import nl.overheid.aerius.wui.application.context.LayerPanelContext;
import nl.overheid.aerius.wui.application.context.PanelContext;
import nl.overheid.aerius.wui.application.context.SearchPanelContext;
import nl.overheid.aerius.wui.application.context.SimplePanelContext;

/*
 * Daemon for the InfoPanel and LayerPanel
 */
public class PanelDaemon extends BasicEventComponent implements Daemon {
  private static final PanelDaemonEventBinder EVENT_BINDER = GWT.create(PanelDaemonEventBinder.class);

  interface PanelDaemonEventBinder extends EventBinder<PanelDaemon> {}

  @Inject InfoPanelContext infoContext;
  @Inject LayerPanelContext layerContext;
  @Inject SearchPanelContext searchContext;

  private PanelContext activePanelContext;

  private final List<PanelContext> panelContexts = new ArrayList<>();

  /**
   * A collection of contexts not to consider in the activation logic
   */
  private final Set<PanelContext> blacklistedContexts = new HashSet<>();

  @Override
  public void init() {
    panelContexts.add(layerContext);
    panelContexts.add(infoContext);
    panelContexts.add(searchContext);
  }

  @EventHandler
  public void onPanelDock(final PanelDockCommand c) {
    blacklistedContexts.remove(c.getValue());
    setActive(c.getValue());
  }

  @EventHandler
  public void onPanelUndock(final PanelUndockCommand c) {
    blacklistedContexts.add(c.getValue());
    activateRemainingVisible();
  }

  @EventHandler
  public void onPanelActive(final PanelActivateCommand c) {
    if (blacklistedContexts.contains(c.getValue())) {
      return;
    }

    setActive(c.getValue());
  }

  @EventHandler
  public void onInfoPopupVisibleCommand(final InfoPopupVisibleCommand c) {
    setActiveVisible(infoContext);
  }

  @EventHandler
  public void onInfoPopupHiddenCommand(final InfoPopupHiddenCommand c) {
    setInactiveInvisible(infoContext);
  }

  @EventHandler
  public void onInfoPopupToggleCommand(final InfoPopupToggleCommand c) {
    c.resolve(toggleActiveVisible(infoContext));
  }

  @EventHandler
  public void onLayerPopupVisibleCommand(final LayerPopupVisibleCommand c) {
    setActiveVisible(layerContext);
  }

  @EventHandler
  public void onLayerPopupHiddenCommand(final LayerPopupHiddenCommand c) {
    setInactiveInvisible(layerContext);
  }

  @EventHandler
  public void onSearchPopupToggleCommand(final SearchPopupToggleCommand c) {
    c.resolve(toggleActiveVisible(searchContext));
  }

  @EventHandler
  public void onSearchSuggestionSelectionEvent(final SearchSuggestionSelectionEvent e) {
    setInactiveInvisible(searchContext);
  }

  @EventHandler
  public void onLayerPopupToggleCommand(final LayerPopupToggleCommand c) {
    c.resolve(toggleActiveVisible(layerContext));
  }

  private boolean toggleActiveVisible(final SimplePanelContext context) {
    final boolean attached = !blacklistedContexts.contains(context);
    final boolean result = context.togglePanelShowing(attached);
    if (!attached) {
      return result;
    }

    if (context.isActive()) {
      setActive(context);
    } else {
      activateRemainingVisible();
    }

    return result;
  }

  private void setActiveVisible(final PanelContext context) {
    context.setPanelShowing(true);
    setActive(context);
  }

  private void setActive(final PanelContext context) {
    deactivateExcept(context);

    activePanelContext = context;
    activePanelContext.setPanelActive(true);
  }

  private void deactivateExcept(final PanelContext context) {
    panelContexts.stream()
        .filter(v -> v != context)
        .filter(v -> v.isActive())
        .filter(v -> !blacklistedContexts.contains(v))
        .forEach(v -> v.setPanelActive(false));
  }

  private void setInactiveInvisible(final PanelContext context) {
    context.setPanelShowing(false);
    deactivateExcept(context);
    activateRemainingVisible();
  }

  private void activateRemainingVisible() {
    panelContexts.stream()
        .filter(v -> v.isShowing())
        .filter(v -> !blacklistedContexts.contains(v))
        .findFirst().ifPresent(panel -> setActive(panel));
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
