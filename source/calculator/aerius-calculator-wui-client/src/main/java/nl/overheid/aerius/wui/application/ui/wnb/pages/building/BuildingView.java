/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.building;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.wui.application.command.building.BuildingEditCancelCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingEditSaveCommand;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.feature.FeatureHeading;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.modify.ModifyCancelSaveComponent;
import nl.overheid.aerius.wui.application.context.BuildingEditorContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    FeatureHeading.class,
    LabeledInputComponent.class,
    CollapsiblePanel.class,
    ModifyCancelSaveComponent.class,
    BuildingLocationEditorComponent.class
})
public class BuildingView extends BasicVueComponent implements IsVueComponent {
  @Prop EventBus eventBus;

  @Inject @Data ScenarioContext scenarioContext;
  @Inject @Data BuildingEditorContext context;

  @Data boolean locationErrors;
  @Data boolean locationWarnings;

  @Data boolean confirmState;

  @Computed
  public String getBuildingTitle() {
    final BuildingFeature building = getBuilding();
    final String id = building.getId().startsWith("-") ? building.getId().substring(1) : building.getId();

    final BuildingFeature match = scenarioContext.getActiveSituation().findBuildingById(id);

    return match == null ? i18n.buildingTitleNew() : i18n.buildingTitleEdit();
  }

  @Computed
  public BuildingFeature getBuilding() {
    return context.getBuilding();
  }

  @Computed("isBuildingAvailable")
  public boolean isBuildingAvailable() {
    return context.isEditing();
  }

  @Computed
  public String getLabel() {
    return getBuilding().getLabel();
  }

  @Computed
  public void setLabel(final String label) {
    getBuilding().setLabel(label);
  }

  @JsMethod
  public void cancel() {
    eventBus.fireEvent(new BuildingEditCancelCommand());
  }

  @JsMethod
  public void save() {
    eventBus.fireEvent(new BuildingEditSaveCommand());
  }

  @JsMethod
  public void setConfirmState(final boolean confirmState) {
    this.confirmState = confirmState;
  }

  @JsMethod
  public void onBuildingErrorsChange(final boolean locationErrors) {
    this.locationErrors = locationErrors;
  }

  @JsMethod
  public void onBuildingWarningsChange(final boolean locationWarnings) {
    this.locationWarnings = locationWarnings;
  }
}
