/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.results.custompoints;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.result.range.ColorRange;
import nl.overheid.aerius.shared.domain.result.range.ColorRangeType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSelectionContext;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.CustomCalculationPointResult;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.habitat.ResultsHabitatListView;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    CollapsiblePanel.class,
    SimplifiedListBoxComponent.class,
    ButtonIcon.class,
    ResultsHabitatListView.class,
    VerticalCollapseGroup.class
})
public class CustomPointsView extends BasicVueEventComponent implements HasMounted {

  @Prop protected EventBus eventBus;
  @Prop(required = true) ResultSummaryContext context;

  @Data @Inject ResultSelectionContext selectionContext;
  @Data @Inject ApplicationContext applicationContext;
  @Data @Inject CalculationContext calculationContext;
  @Data @Inject protected ScenarioContext scenarioContext;

  @Data CustomPointsSortColumn sortColumn = CustomPointsSortColumn.VALUE;
  @Data Boolean sortAscending = false;

  @Data @JsProperty Map<Integer, String> customPointNames = new HashMap<>();

  @JsMethod
  public String formatValueFor(final double value) {
    return MessageFormatter.formatDeposition(value, applicationContext.getActiveThemeConfiguration().getEmissionResultValueDisplaySettings());
  }

  @Computed
  public EmissionResultValueDisplaySettings.DepositionValueDisplayType getDepositionType() {
    return applicationContext.getActiveThemeConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @JsMethod
  public ResultSummaryContext getResultContext() {
    return Optional.ofNullable(getCalculationJob())
        .map(v -> v.getResultContext())
        .orElseThrow(() -> new RuntimeException("Could not retrieve result context."));
  }

  @JsMethod
  public List<CustomCalculationPointResult> getCalculationPoints() {
    final Comparator<CustomCalculationPointResult> comparator = (p1, p2) -> {
      if (sortColumn == CustomPointsSortColumn.ID) {
        return Integer.compare(p1.getCustomPointId(), p2.getCustomPointId()) * (Boolean.TRUE.equals(sortAscending) ? 1 : -1);
      } else if (sortColumn == CustomPointsSortColumn.VALUE) {
        return Double.compare(p1.getResult(), p2.getResult()) * (Boolean.TRUE.equals(sortAscending) ? 1 : -1);
      } else {
        return 0;
      }
    };
    return Arrays.stream(getCustomPointResults()).sorted(comparator).collect(Collectors.toList());
  }

  protected CustomCalculationPointResult[] getCustomPointResults() {
    return getResultContext().getActiveResultSummary().getCustomPointResults();
  }

  @JsMethod
  public void clickId() {
    if (sortColumn == CustomPointsSortColumn.ID) {
      sortAscending = !sortAscending;
    } else {
      sortColumn = CustomPointsSortColumn.ID;
      sortAscending = true;
    }
  }

  @JsMethod
  public void clickValue() {
    if (sortColumn == CustomPointsSortColumn.VALUE) {
      sortAscending = !sortAscending;
    } else {
      sortColumn = CustomPointsSortColumn.VALUE;
      sortAscending = false;
    }
  }

  @JsMethod
  public String getColorForValue(final double value) {
    final ColorRangeType colorRangeType = getResultContext().getResultType() == ScenarioResultType.SITUATION_RESULT ||
        getResultContext().getResultType() == ScenarioResultType.MAX_TEMPORARY_CONTRIBUTION
        ? ColorRangeType.DEPOSITION_CONTRIBUTION
        : ColorRangeType.DEPOSITION_DIFFERENCE;

    final List<ColorRange> colorRange = applicationContext.getActiveThemeConfiguration().getColorRange(colorRangeType);
    final ColorRange range = colorRange.stream()
        .sorted(Comparator.reverseOrder())
        .filter(v -> value >= v.getLowerValue())
        .findFirst().orElse(colorRange.get(0));

    return range.getColor();
  }

  @JsMethod
  public String getCustomPointName(final int customPointId) {
    return customPointNames.get(customPointId);
  }

  @Computed
  public CalculationJobContext getCalculationJob() {
    return calculationContext.getActiveCalculation();
  }

  @Override
  public void mounted() {
    customPointNames = new HashMap<>();
    updateCustomPointLabels();
  }

  protected void updateCustomPointLabels() {
    scenarioContext.getCalculationPoints().forEach(cp -> customPointNames.put(cp.getCustomPointId(), cp.getLabel()));
  }
}
