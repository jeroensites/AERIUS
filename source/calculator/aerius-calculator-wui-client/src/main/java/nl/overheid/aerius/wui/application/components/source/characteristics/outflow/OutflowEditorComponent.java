/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.characteristics.outflow;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;
import jsinterop.base.Js;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.ValidationUtil;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.ops.HeatContentType;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.building.BuildingSelectorComponent;
import nl.overheid.aerius.wui.application.components.source.characteristics.outflow.OutflowValidators.OutflowValidations;
import nl.overheid.aerius.wui.application.components.source.validation.ErrorWarningValidator;
import nl.overheid.aerius.wui.application.components.source.validation.HasValidators;
import nl.overheid.aerius.wui.application.components.source.validation.ValidationBehaviour;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;

@Component(customizeOptions = {
    OutflowValidators.class
}, directives = {
    ValidateDirective.class
}, components = {
    LabeledInputComponent.class,
    OutflowForcedEditorComponent.class,
    OutflowNotForcedEditorComponent.class,
    BuildingSelectorComponent.class,
    VerticalCollapse.class,

    ValidationBehaviour.class,
})
public class OutflowEditorComponent extends ErrorWarningValidator implements IsVueComponent, HasCreated, HasValidators<OutflowValidations> {
  @Prop EmissionSourceFeature source;
  @Prop EventBus eventBus;

  @Data OPSCharacteristics characteristics;

  @JsProperty(name = "$v") OutflowValidations validation;
  @Data String emissionHeightV;
  @Data String emissionHeightWithBuildingV;

  @Override
  @Computed
  public OutflowValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  @JsMethod
  public boolean isBuildingInfluence() {
    return characteristics.isBuildingInfluence();
  }

  @Computed
  protected String getHeatContentType() {
    return characteristics.getHeatContentType().name();
  }

  @Computed
  protected void setHeatContentType(final String selectedHeatContentType) {
    characteristics.setHeatContentType(HeatContentType.safeValueOf(selectedHeatContentType));
  }

  @Computed
  protected String getEmissionHeight() {
    return emissionHeightV;
  }

  @Computed
  protected void setEmissionHeight(final String emissionHeight) {
    ValidationUtil.setSafeDoubleValue(v -> characteristics.setEmissionHeight(v), emissionHeight, 0.0);
    emissionHeightV = emissionHeight;
    emissionHeightWithBuildingV = emissionHeight;
  }

  @Watch(value = "source", isImmediate = true)
  public void watchSource() {
    characteristics = source == null ? null : Js.uncheckedCast(source.getCharacteristics());
  }

  @Watch(value = "characteristics", isImmediate = true)
  public void watchCharacteristics() {
    emissionHeightV = String.valueOf(characteristics.getEmissionHeight());
    emissionHeightWithBuildingV = String.valueOf(characteristics.getEmissionHeight());
  }

  @JsMethod
  protected boolean isForcedHeat() {
    return characteristics.getHeatContentType() == HeatContentType.FORCED;
  }

  @JsMethod
  protected String getEmissionHeightWarning() {
    return i18n.ivBuildingInfluenceRangeBetween(i18n.sourceEmissionHeightLabel(),
        OPSLimits.SCOPE_BUILDING_OUTFLOW_HEIGHT_MINIMUM, OPSLimits.SCOPE_BUILDING_OUTFLOW_HEIGHT_MAXIMUM);
  }

  @JsMethod
  protected String getEmissionHeightError() {
    return i18n.ivDoubleRangeBetween(i18n.sourceEmissionHeightLabel(),
        OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM, OPSLimits.SOURCE_EMISSION_HEIGHT_MAXIMUM);
  }

  @Computed("isEmissionHeightError")
  public boolean isEmissionHeightError() {
    return validation.emissionHeightV.invalid;
  }

  /**
   * Triggered _only_ when there is a building influence
   */
  @Computed("isEmissionHeightWarning")
  public boolean isEmissionHeightWarning() {
    return isBuildingInfluence() && validation.emissionHeightWithBuildingV.invalid;
  }
}
