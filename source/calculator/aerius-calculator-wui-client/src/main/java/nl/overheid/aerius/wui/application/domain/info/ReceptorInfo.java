/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.info;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ReceptorInfo {

  private @JsProperty Natura2000Info[] naturaInfo;
  private @JsProperty HabitatInfo[] habitatTypeInfo;
  private @JsProperty EmissionResultInfo emissionResultInfo;

  public final @JsOverlay Natura2000Info[] getNaturaInfo() {
    return naturaInfo;
  }

  public final @JsOverlay HabitatInfo[] getHabitatTypeInfo() {
    return habitatTypeInfo;
  }

  public final @JsOverlay EmissionResultInfo getEmissionResultInfo() {
    return emissionResultInfo;
  }

}
