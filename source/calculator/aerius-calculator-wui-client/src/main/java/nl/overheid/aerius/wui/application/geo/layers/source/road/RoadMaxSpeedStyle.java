/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source.road;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.stream.Collectors;

import ol.OLFactory;
import ol.style.Style;

import nl.overheid.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.overheid.aerius.geo.domain.legend.LegendType;
import nl.overheid.aerius.shared.config.AppThemeConfiguration;
import nl.overheid.aerius.shared.domain.result.range.ColorRange;
import nl.overheid.aerius.shared.domain.result.range.ColorRangeType;
import nl.overheid.aerius.wui.application.domain.source.RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.SRM2RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.StandardVehicles;
import nl.overheid.aerius.wui.application.domain.source.road.VehicleType;
import nl.overheid.aerius.wui.application.geo.layers.source.RoadSourceGeometryLayer;
import nl.overheid.aerius.wui.application.i18n.M;

public class RoadMaxSpeedStyle implements RoadStyle {

  private static final int UNKNOWN_MAX_SPEED = 0;

  private final Map<Integer, List<Style>> styleMap;
  private final ColorLabelsLegend legend;

  public RoadMaxSpeedStyle(final AppThemeConfiguration themeConfiguration) {
    this.styleMap = themeConfiguration.getColorRange(ColorRangeType.ROAD_MAX_SPEED).stream()
        .collect(Collectors.toMap(
            cr -> (int) cr.getLowerValue(),
            cr -> Collections.singletonList(OLFactory.createStyle(OLFactory.createStroke(OLFactory.createColor(cr.getColor()),
                RoadSourceGeometryLayer.DEFAULT_ROAD_WIDTH)))
      ));

    this.legend = new ColorLabelsLegend(
        themeConfiguration.getColorRange(ColorRangeType.ROAD_MAX_SPEED).stream()
            .map(ColorRange::getLowerValue)
            .map(speed -> {
          if (speed == UNKNOWN_MAX_SPEED) {
            return M.messages().esRoadCategoryUnknown();
          } else {
            return M.messages().esRoadCategory(speed.intValue());
          }
        }).toArray(String[]::new),
        themeConfiguration.getColorRange(ColorRangeType.ROAD_MAX_SPEED).stream()
            .map(ColorRange::getColor).toArray(String[]::new),
        LegendType.LINE
    );
  }

  @Override
  public List<Style> getStyle(final RoadESFeature feature, final nl.overheid.aerius.shared.domain.v2.source.road.VehicleType vehicleType,
      final double resolution) {
    // Only SRM2 features are supported in this layer
    if (feature instanceof SRM2RoadESFeature) {
      // Only freeways have a maximum speed
      if (((SRM2RoadESFeature) feature).isFreeway()) {
        final OptionalInt maxSpeed = Arrays.stream(feature.getSubSources()
            .filter((e, i, a) -> e.getVehicleType() == VehicleType.STANDARD))
            .mapToInt(v -> ((StandardVehicles) v).getMaximumSpeed())
            .max();
        return styleMap.get(maxSpeed.orElse(UNKNOWN_MAX_SPEED));
      } else {
        return styleMap.get(UNKNOWN_MAX_SPEED);
      }
    } else {
      return styleMap.get(UNKNOWN_MAX_SPEED);
    }
  }

  @Override
  public ColorLabelsLegend getLegend() {
    return legend;
  }

}
