/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.domain.source;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.ops.HeatContentType;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.ops.OutflowDirectionType;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;

/**
 * Client side implementation of OPS props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class OPSCharacteristics extends SourceCharacteristics {

  private String heatContentType;
  private double heatContent;
  private double emissionTemperature;
  private double outflowDiameter;
  private double outflowVelocity;
  private String outflowDirection;
  private double emissionHeight;
  private String buildingId;
  private int diameter;
  private double spread;
  private String diurnalVariation;
  private int particleSizeDistribution;

  public static final @JsOverlay OPSCharacteristics create() {
    final OPSCharacteristics props = Js.uncheckedCast(JsPropertyMap.of());
    props.setCharacteristicsType(CharacteristicsType.OPS);
    props.setEmissionHeight(5d);
    props.setHeatContentType(HeatContentType.NOT_FORCED);
    props.setHeatContent(0.0);
    props.setEmissionTemperature(OPSLimits.AVERAGE_SURROUNDING_TEMPERATURE);
    props.setOutflowDiameter(OPSLimits.SCOPE_NO_BUILDING_OUTFLOW_DIAMETER_MINIMUM);
    props.setOutflowVelocity(0.0);
    props.setOutflowDirection(OutflowDirectionType.VERTICAL);
    props.setEmissionHeight(0.0);
    props.setDiameter(0);
    props.setSpread(0.0);
    props.setDiurnalVariation(DiurnalVariation.CONTINUOUS);
    props.setParticleSizeDistribution(0);
    props.setBuildingId(null);
    return props;
  }

  public final @JsOverlay HeatContentType getHeatContentType() {
    return HeatContentType.safeValueOf(heatContentType);
  }

  public final @JsOverlay void setHeatContentType(final HeatContentType heatContentType) {
    this.heatContentType = heatContentType.name();
  }

  public final @JsOverlay double getHeatContent() {
    return heatContent;
  }

  public final @JsOverlay void setHeatContent(final double heatContent) {
    this.heatContent = heatContent;
  }

  public final @JsOverlay double getEmissionTemperature() {
    return emissionTemperature;
  }

  public final @JsOverlay void setEmissionTemperature(final double emissionTemperature) {
    this.emissionTemperature = emissionTemperature;
  }

  public final @JsOverlay double getOutflowDiameter() {
    return outflowDiameter;
  }

  public final @JsOverlay void setOutflowDiameter(final double outflowDiameter) {
    this.outflowDiameter = outflowDiameter;
  }

  public final @JsOverlay double getOutflowVelocity() {
    return outflowVelocity;
  }

  public final @JsOverlay void setOutflowVelocity(final double outflowVelocity) {
    this.outflowVelocity = outflowVelocity;
  }

  public final @JsOverlay OutflowDirectionType getOutflowDirection() {
    return OutflowDirectionType.safeValueOf(outflowDirection);
  }

  public final @JsOverlay void setOutflowDirection(final OutflowDirectionType outflowDirection) {
    this.outflowDirection = outflowDirection.name();
  }

  public final @JsOverlay double getEmissionHeight() {
    return emissionHeight;
  }

  public final @JsOverlay void setEmissionHeight(final double emissionHeight) {
    this.emissionHeight = emissionHeight;
  }

  public final @JsOverlay boolean isBuildingInfluence() {
    return buildingId != null;
  }

  public final @JsOverlay int getDiameter() {
    return diameter;
  }

  public final @JsOverlay void setDiameter(final int diameter) {
    this.diameter = diameter;
  }

  public final @JsOverlay double getSpread() {
    return spread;
  }

  public final @JsOverlay void setSpread(final double spread) {
    this.spread = spread;
  }

  public final @JsOverlay DiurnalVariation getDiurnalVariation() {
    return DiurnalVariation.safeValueOf(diurnalVariation);
  }

  public final @JsOverlay void setDiurnalVariation(final DiurnalVariation diurnalVariation) {
    setDiurnalVariation(diurnalVariation.name());
  }

  public final @JsOverlay void setDiurnalVariation(final String diurnalVariation) {
    this.diurnalVariation = diurnalVariation;
  }

  public final @JsOverlay int getParticleSizeDistribution() {
    return particleSizeDistribution;
  }

  public final @JsOverlay void setParticleSizeDistribution(final int particleSizeDistribution) {
    this.particleSizeDistribution = particleSizeDistribution;
  }

  public final @JsOverlay void setBuildingId(final String buildingId) {
    this.buildingId = buildingId;
  }

  public final @JsOverlay String getBuildingId() {
    return buildingId;
  }
}
