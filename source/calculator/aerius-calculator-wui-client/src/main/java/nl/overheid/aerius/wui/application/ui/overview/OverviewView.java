/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.overview;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasActivated;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.application.components.importer.ApplicationImportButtonComponent;
import nl.overheid.aerius.wui.application.components.importer.ApplicationImportModalComponent;
import nl.overheid.aerius.wui.application.components.nav.HeaderComponent;
import nl.overheid.aerius.wui.vue.BasicVueView;
import nl.overheid.aerius.wui.vue.MaskDirective;

@Component(components = {
    HeaderComponent.class,
    ApplicationImportButtonComponent.class,
    ApplicationImportModalComponent.class
}, directives = MaskDirective.class)
public class OverviewView extends BasicVueView implements HasActivated {
  @Prop OverviewActivity presenter;

  @Prop EventBus eventBus;

  @Override
  public void activated() {
    presenter.setView(this);
  }
}
