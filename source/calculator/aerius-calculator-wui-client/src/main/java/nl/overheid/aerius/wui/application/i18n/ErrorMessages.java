/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.LocalizableResource.Description;

public interface ErrorMessages {
  @Description("Could not load application configuration.")
  String errorCouldNotLoadApplicationConfiguration();
  String errorNumeric(String input);
  String errorDecimal(String input);
  String errorNotValidEntry(String input);
  String ivBuildingInfluenceRangeBetween(String label, double min, double max);
  String ivBuildingInfluenceExact(String label, String range);
  String ivBuildingInfluenceExactHeatContent(String label, String valid);
  String ivExactHeatContent(String label, double min, double max, double except);
  String ivRoadBarrierBetween(String label, double min, double max);
  String ivRoadBarrierHeightUndefined(String label, double min);
  String ivRoadBarrierHeightUpperLimit(String label, double max);
  String ivRoadBarrierDistanceUpperLimit(String label, double max);
  String ivDoubleRangeBetween(String label, double min, double max);
  String ivDoubleLowerlimit(String label, double min);
  String ivIntegerRangeBetween(String label, int min, int max);
  String ivIntegerLowerlimit(String label, int min);
  String errorDescriptionRequired();
  String errorShipCategoryRequired();
  String errorShipLaden();
  String errorShipTypeWaterwayCombination();
  String errorRoadSpeedRequired();
  String errorNumberOfVehicles();
  String errorStagnationFraction();
  String errorEmail(String email);
  String errorLayerCouldNotBeLoaded(String layerName);

  String errorImportTooManySituations(int amount);
  String errorImportTooManyReferenceSituations(int amount);
  String errorImportTooManyNettingSituations(int amount);
  String errorImportNoCalculationPointsFound();

  String errorShipShorePowerFactor();
}
