/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;

/**
 * Client side implementation of StandardVehicles props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class StandardVehicles extends Vehicles {

  private int maximumSpeed;
  private Boolean strictEnforcement;
  private ValuesPerVehicleTypes valuesPerVehicleTypes;

  public static final @JsOverlay StandardVehicles create() {
    final StandardVehicles props = new StandardVehicles();
    Vehicles.init(props, VehicleType.STANDARD);
    props.setValuesPerVehicleTypes(ValuesPerVehicleTypes.create());
    props.setTimeUnit(TimeUnit.DAY);
    props.setMaximumSpeed(0);
    props.setStrictEnforcement(false);
    return props;
  }

  public final @JsOverlay int getMaximumSpeed() {
    return maximumSpeed;
  }

  public final @JsOverlay void setMaximumSpeed(final int maximumSpeed) {
    this.maximumSpeed = maximumSpeed;
  }

  public final @JsOverlay boolean isStrictEnforcement() {
    return Boolean.TRUE.equals(strictEnforcement);
  }

  public final @JsOverlay void setStrictEnforcement(final Boolean strictEnforcement) {
    this.strictEnforcement = strictEnforcement;
  }

  public final @JsOverlay ValuesPerVehicleType getValuesPerVehicleType(final nl.overheid.aerius.shared.domain.v2.source.road.VehicleType vehicleType) {
    return valuesPerVehicleTypes.getValuePerVehicleType(vehicleType);
  }

  public final @JsOverlay void setValuesPerVehicleType(final nl.overheid.aerius.shared.domain.v2.source.road.VehicleType vehicleType,
      final ValuesPerVehicleType valuesPerVehicleType) {
    this.valuesPerVehicleTypes.setValuePerVehicleType(vehicleType, valuesPerVehicleType);
  }

  public final @JsOverlay ValuesPerVehicleTypes getValuesPerVehicleTypes() {
    return valuesPerVehicleTypes;
  }

  public final @JsOverlay void setValuesPerVehicleTypes(final ValuesPerVehicleTypes valuesVehicleType) {
    this.valuesPerVehicleTypes = valuesVehicleType;
  }

}
