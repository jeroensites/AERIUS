/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;

import ol.Feature;
import ol.style.Style;

import nl.overheid.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadEmissionSourceTypes;
import nl.overheid.aerius.wui.application.geo.layers.FeatureStyle;
import nl.overheid.aerius.wui.application.geo.util.GeoType;

public class GenericSourceGeometryLayer extends SourceGeometryLayer {

  @Inject
  public GenericSourceGeometryLayer(@Assisted final LayerInfo info, final EventBus eventBus, @Assisted final int zIndex) {
    super(info, eventBus, zIndex);
  }

  @Override
  protected Style[] getStyle(final Feature feature, final double resolution) {
    final EmissionSourceFeature source = (EmissionSourceFeature) feature;

    if (GeoType.POINT.is(feature.getGeometry())) {
      return NO_RENDERING;
    }

    if (RoadEmissionSourceTypes.isRoadSource(source)) {
      return NO_RENDERING;
    }

    if (isBeingObsoleted(source.getId(), getGeometryEditingFeatureId())) {
      return FeatureStyle.OBSOLETE_STYLING;
    }

    if (source.getId().startsWith("-")) {
      return FeatureStyle.EDIT_STYLING;
    }

    return FeatureStyle.DEFAULT_STYLING;
  }

  /**
   * Return whether the feature id matches the id being edited (which will be
   * prepended with -)
   */
  private boolean isBeingObsoleted(final String featureId, final String editingId) {
    if (editingId != null && editingId.startsWith("-")) {
      final String cleanId = editingId.substring(1);

      return featureId.equals(cleanId);
    }

    return false;
  }

}
