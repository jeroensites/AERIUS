/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.i18n;

import ol.geom.Point;

import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;

/**
 * Util class for formatting numbers.
 */
public class MessageFormatter {
  private static final int MINIMUM_THRESHOLD = 10_000;
  private static final int KILO_IN_TON = 1_000;
  private static final int M_IN_HA = 10_000;

  private static final int EMISSION_VALUE_ONE_PRECISE = 1;
  private static final int SURFACE_PRECISION = 2;
  private static final int DISTANCE_PRECISION = 2;

  private MessageFormatter() {
  }

  public static String formatEmissionWithUnitSmart(final double em) {
    final String withUnit;
    if (roundedEmissionUnderMinimumThreshold(em)) {
      withUnit = M.messages().unitKgPerY(formatEmissionToKg(em));
    } else {
      withUnit = M.messages().unitTonnesPerY(formatEmissionToTon(em));
    }
    return withUnit;
  }

  public static String getEmissionUnitSmart(final double em) {
    if (roundedEmissionUnderMinimumThreshold(em)) {
      return M.messages().unitSingularKgPerY();
    }
    return M.messages().unitSingularTonPerY();
  }

  public static String formatEmissionWithoutUnitSmart(final double em) {
    if (roundedEmissionUnderMinimumThreshold(em)) {
      return formatEmissionToKg(em);
    }
    return formatEmissionToTon(em);
  }

  private static boolean roundedEmissionUnderMinimumThreshold(final double em) {
    final double scale = Math.pow(10, EMISSION_VALUE_ONE_PRECISE);
    return Math.abs(Math.round(em * scale)) < MINIMUM_THRESHOLD * scale;
  }

  public static String toFixed(final double value, final int length) {
    return M.messages().decimalNumberFixed(value, length);
  }

  public static String toCapped(final double value, final int length) {
    return M.messages().decimalNumberCapped(value, length);
  }

  public static String formatEmission(final double em, final int precision) {
    return toFixed(em, precision);
  }

  public static String formatEmissionOnePrecise(final double em) {
    return formatEmission(em, EMISSION_VALUE_ONE_PRECISE);
  }

  public static String formatPoint(final Point point) {
    return M.messages().point(point.getCoordinates().getX(), point.getCoordinates().getY());
  }

  public static String formatDistance(final double distance) {
    return M.messages().decimalNumberFixed(distance, DISTANCE_PRECISION);
  }

  public static String formatDistanceWithUnit(final double distance) {
    return M.messages().unitM(formatDistance(distance));
  }

  public static String formatEmissionHeight(final double emissionHeight) {
    return M.messages().unitM(toFixed(emissionHeight, 1));
  }

  public static String formatHeatContent(final double heatContent) {
    return M.messages().unitMW(toFixed(heatContent, 3));
  }

  private static String formatEmissionToTon(final double em) {
    return formatEmissionOnePrecise(em / KILO_IN_TON);
  }

  private static String formatEmissionToKg(final double em) {
    return formatEmissionOnePrecise(em);
  }

  /* Deposition formatting. */

  /**
   * Returns a formatted deposition value with the given unit and precision.
   *
   * @param deposition
   * @param settings
   * @param precision
   * @return
   */
  public static String formatDeposition(final Double deposition, final EmissionResultValueDisplaySettings settings, final int precision) {
    return deposition == null ? "-" : toFixed(convertDepositionValue(deposition, settings), precision);
  }

  public static String formatDeposition(final Number deposition, final EmissionResultValueDisplaySettings settings) {
    return deposition == null ? "-" : MessageFormatter.formatDeposition(deposition.doubleValue(), settings);
  }

  public static String formatDeposition(final double deposition, final EmissionResultValueDisplaySettings settings) {
    return toFixed(convertDepositionValue(deposition, settings), settings.getDepositionValueRoundingLength());
  }

  public static String formatDepositionWithUnit(final Double deposition, final EmissionResultValueDisplaySettings settings) {
    return formatDepositionWithUnit(deposition, settings, settings.getDepositionValueRoundingLength());
  }

  public static String formatDepositionWithUnit(final Double deposition, final EmissionResultValueDisplaySettings settings, final int precision) {
    return deposition == null ? "-" : formatDepositionValueDisplayUnit(formatDeposition(deposition, settings, precision), settings);
  }

  private static double convertDepositionValue(final double depositionValue, final EmissionResultValueDisplaySettings settings) {
    return depositionValue * settings.getConversionFactor();
  }

  private static String formatDepositionValueDisplayUnit(final String depositionValue, final EmissionResultValueDisplaySettings settings) {
    return M.messages().unitDeposition(settings.getDisplayType(), depositionValue);
  }

  /* Surface formatting. */

  public static String formatSurfaceToHectare(final Double surfaceInMeters) {
    return surfaceInMeters == null ? "-" : formatSurfaceToHectareInternal(surfaceInMeters.doubleValue());
  }

  public static String formatSurfaceToHectareInternal(final double surfaceInMeters) {
    return toFixed(surfaceInMeters / M_IN_HA, SURFACE_PRECISION);
  }

  public static String formatSurfaceToHectareWithUnit(final Double surfaceInMeters) {
    return M.messages().unitHa(formatSurfaceToHectare(surfaceInMeters));
  }

  public static String formatPercentage(final double value) {
    return M.messages().unitPercentage(toFixed(value, 0));
  }
}
