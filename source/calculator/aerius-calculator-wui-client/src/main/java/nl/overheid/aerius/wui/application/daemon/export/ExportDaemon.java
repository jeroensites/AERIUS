/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.export;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.application.command.ExportStartCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ExportContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculateContext;
import nl.overheid.aerius.wui.application.event.ExportCalculationCompleteEvent;
import nl.overheid.aerius.wui.application.event.ExportStartEvent;
import nl.overheid.aerius.wui.application.factories.ExportStatusPollerFactory;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.CalculationServiceAsync;
import nl.overheid.aerius.wui.application.service.FileServiceAsync;

@Singleton
public class ExportDaemon extends BasicEventComponent implements Daemon {
  private static final ExportDaemonEventBinder EVENT_BINDER = GWT.create(ExportDaemonEventBinder.class);

  interface ExportDaemonEventBinder extends EventBinder<ExportDaemon> {}

  @Inject ApplicationContext applicationContext;
  @Inject FileServiceAsync fileService;
  @Inject CalculateContext calculateContext;
  @Inject ScenarioContext scenarioContext;
  @Inject ExportContext exportContext;
  @Inject CalculationServiceAsync calculationServiceAsync;
  @Inject ExportStatusPollerFactory exportPollerFactory;

  private String latestExportHash;

  @EventHandler
  public void onCalculationExportStartCommand(final ExportStartCommand c) {
    c.silence();

    // Bail if there's already an export with this exact input running
    final String exportHash = scenarioContext.toJSONString(exportContext.isIncludeScenarioMetaData(), calculateContext.getCalculationOptions());
    if (exportHash.equals(latestExportHash)) {
      NotificationUtil.broadcastWarning(eventBus, M.messages().exportAlreadyRunningWarning());
      return;
    }

    latestExportHash = exportHash;
    calculationServiceAsync.startCalculation(c.getValue(), scenarioContext, applicationContext.getActiveTheme(),
        AeriusRequestCallback.create(calculationKey -> {
          // ToDo: enable and adjust poller to show the generated download link from
          // fileservice (see AER3-806)
          exportPollerFactory.create(eventBus, calculationKey).start();
          eventBus.fireEvent(c.createEvent());
        }, e -> {
          latestExportHash = null;
          throw new RuntimeException(e);
        }));
  }

  @EventHandler
  public void onExportStartEvent(final ExportStartEvent e) {
    NotificationUtil.broadcastMessage(eventBus, M.messages().notificationExportStarted());
  }

  @EventHandler
  public void onExportCalculationCompleteEvent(final ExportCalculationCompleteEvent c) {
    latestExportHash = null;
    if (JobState.COMPLETED.equals(c.getValue().getJobProgress().getState())) {
      final String fileUrl = c.getValue().getJobProgress().getResultUrl();
      NotificationUtil.broadcastMessage(eventBus, M.messages().scenarioExportFileDownload(fileUrl));
    } else {
      NotificationUtil.broadcastMessage(eventBus, M.messages().scenarioExportError(c.getValue().getJobProgress().getErrorMessage()));

    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
