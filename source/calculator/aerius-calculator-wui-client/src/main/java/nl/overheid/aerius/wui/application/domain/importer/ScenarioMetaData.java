/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.domain.importer;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class ScenarioMetaData {

  private String reference;
  private String corporation;
  private String projectName;
  private String description;
  private String streetAddress;
  private String postcode;
  private String city;

  public static final @JsOverlay ScenarioMetaData create() {
    final ScenarioMetaData scenarioMetaData = new ScenarioMetaData();
    scenarioMetaData.setReference("");
    scenarioMetaData.setCorporation("");
    scenarioMetaData.setProjectName("");
    scenarioMetaData.setDescription("");
    scenarioMetaData.setStreetAddress("");
    scenarioMetaData.setPostcode("");
    scenarioMetaData.setCity("");
    return scenarioMetaData;
  }

  public final @JsOverlay String getCity() {
    return city;
  }

  public final @JsOverlay void setCity(final String city) {
    this.city = city;
  }

  public final @JsOverlay String getPostcode() {
    return postcode;
  }

  public final @JsOverlay void setPostcode(final String postcode) {
    this.postcode = postcode;
  }

  public final @JsOverlay String getStreetAddress() {
    return streetAddress;
  }

  public final @JsOverlay void setStreetAddress(final String streetAddress) {
    this.streetAddress = streetAddress;
  }

  public final @JsOverlay String getDescription() {
    return description;
  }

  public final @JsOverlay void setDescription(final String description) {
    this.description = description;
  }

  public final @JsOverlay String getProjectName() {
    return projectName;
  }

  public final @JsOverlay void setProjectName(final String projectName) {
    this.projectName = projectName;
  }

  public final @JsOverlay String getCorporation() {
    return corporation;
  }

  public final @JsOverlay void setCorporation(final String corporation) {
    this.corporation = corporation;
  }

  public final @JsOverlay String getReference() {
    return reference;
  }

  public final @JsOverlay void setReference(final String reference) {
    this.reference = reference;
  }
}
