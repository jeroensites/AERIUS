/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.source;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Collection;
import ol.Feature;
import ol.OLFactory;
import ol.layer.Layer;
import ol.layer.VectorLayerOptions;
import ol.source.Vector;
import ol.source.VectorOptions;
import ol.style.Icon;
import ol.style.IconOptions;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.overheid.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.geo.icon.DynamicSvgIcon;
import nl.overheid.aerius.geo.wui.util.OL3GeometryUtil;
import nl.overheid.aerius.wui.application.command.NameLabelVisibilityCommand;
import nl.overheid.aerius.wui.application.command.source.TemporaryEmissionSourceAddCommand;
import nl.overheid.aerius.wui.application.command.source.TemporaryEmissionSourceClearCommand;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.geo.icons.LabelSvgIconBuilder;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;

public class SelectedSourceMarkerLayer implements IsLayer<Layer> {

  private final Collection<Feature> selectedFeature = new Collection<>();

  private static final LabelSvgIconBuilder LABEL_SVG_ICON_BUILDER = new LabelSvgIconBuilder();

  private final LayerInfo info;
  private final ol.layer.Vector layer;

  private final SourceMarkerLayerEventBinder EVENT_BINDER = GWT.create(SourceMarkerLayerEventBinder.class);

  private boolean showNameLabels = false;

  interface SourceMarkerLayerEventBinder extends EventBinder<SelectedSourceMarkerLayer> {}

  @Inject
  public SelectedSourceMarkerLayer(@Assisted final LayerInfo info, final EventBus eventBus, @Assisted final int zIndex) {
    this.info = info;

    final VectorLayerOptions vectorLayerOptions = OLFactory.createOptions();
    vectorLayerOptions.setStyle((feature, v) -> getStyle(feature, v));

    final VectorOptions vectorSourceOptions = OLFactory.createOptions();
    vectorSourceOptions.setFeatures(selectedFeature);
    final Vector vectorSource = new Vector(vectorSourceOptions);

    layer = new ol.layer.Vector(vectorLayerOptions);
    layer.setZIndex(zIndex);
    layer.setSource(vectorSource);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onTemporaryEmissionSourceAddCommand(final TemporaryEmissionSourceAddCommand c) {
    if (c.getValue() != null) {
      selectedFeature.push(c.getValue());
    }
    layer.getSource().changed();
  }

  @EventHandler
  public void onTemporaryEmissionSourceClearCommand(final TemporaryEmissionSourceClearCommand c) {
    if (c.getValue() != null) {
      selectedFeature.remove(c.getValue());
    }
    layer.getSource().changed();
  }

  @EventHandler
  public void onSituationSwitch(final SituationSwitchEvent e) {
    selectedFeature.clear();
    layer.getSource().changed();
  }

  @EventHandler
  public void onNameLabelVisibilityCommand(final NameLabelVisibilityCommand e) {
    this.showNameLabels = e.getValue();

    layer.getSource().changed();
  }

  private Style[] getStyle(final Feature feature, final double resolution) {
    final EmissionSourceFeature f = (EmissionSourceFeature) feature;

    final List<LabelSvgIconBuilder.Label> markers = new ArrayList<>();

    final String label = showNameLabels ? f.getLabel() : f.getId();
    final String correctedLabel = label.startsWith("-") ? label.substring(1) : label;

    final LabelSvgIconBuilder.Label marker = new LabelSvgIconBuilder.Label(
        correctedLabel, LabelStyle.getLabelColor(f));
    markers.add(marker);

    final DynamicSvgIcon svg = LABEL_SVG_ICON_BUILDER.getSvgIcon(markers, true, LabelSvgIconBuilder.LabelStyle.EMISSION_SOURCE);

    final IconOptions iconOptions = new IconOptions();
    iconOptions.setSrc(svg.getSrc());
    iconOptions.setAnchor(svg.getAnchorInPx());
    iconOptions.setAnchorXUnits("pixels");
    iconOptions.setAnchorYUnits("pixels");
    final Icon icon = new Icon(iconOptions);

    final StyleOptions styleOptions = new StyleOptions();
    styleOptions.setImage(icon);

    final Style style = new Style(styleOptions);
    style.setGeometry(OL3GeometryUtil.getMiddlePointOfGeometry(f.getGeometry()));

    return new Style[] { style };
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public Optional<LayerInfo> getInfoOptional() {
    return Optional.of(info);
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
