/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.DataResource.MimeType;

import nl.overheid.aerius.geo.resources.GeoResources;
import nl.overheid.aerius.wui.application.resources.images.icon.IconResources;
import nl.overheid.aerius.wui.application.resources.images.markers.MarkerResources;
import nl.overheid.aerius.wui.application.resources.images.misc.MiscellaneousResources;
import nl.overheid.aerius.wui.application.resources.images.print.PrintResources;
import nl.overheid.aerius.wui.application.resources.images.theme.ThemeResources;

/**
 * Image Resource interface to all images.
 */
public interface ImageResources extends ClientBundle, ThemeResources, IconResources, MiscellaneousResources, GeoResources, MarkerResources,
    PrintResources {
  @Source("images/logo.svg")
  @MimeType("image/svg+xml")
  DataResource logo();

  @Source("images/loading-spinner.svg")
  @MimeType("image/svg+xml")
  DataResource loadingSpinner();

}
