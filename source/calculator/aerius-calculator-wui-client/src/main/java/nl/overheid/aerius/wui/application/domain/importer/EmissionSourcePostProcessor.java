/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.importer;

import jsinterop.base.Js;

import nl.overheid.aerius.shared.domain.ops.HeatContentType;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.ops.OutflowDirectionType;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.shared.domain.v2.source.road.TrafficDirection;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.domain.source.SRM1RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.SRM2RoadESFeature;

public final class EmissionSourcePostProcessor {
  private EmissionSourcePostProcessor() {}

  /**
   * Initialize properties with default values (if non-existent) so it is made
   * reactive in the UI later on.
   *
   * This method will be very boilerplatey until the root issue that's making us
   * do this is resolved.
   */
  public static void postProcessEmissionSource(final EmissionSourceFeature source) {
    if (source.getEmissionSourceType() == EmissionSourceType.SRM1_ROAD) {
      final SRM1RoadESFeature road = (SRM1RoadESFeature) source;
      if (road.getTrafficDirection() == null) {
        road.setTrafficDirection(TrafficDirection.BOTH);
      }
    }

    if (source.getEmissionSourceType() == EmissionSourceType.SRM2_ROAD) {
      final SRM2RoadESFeature road = (SRM2RoadESFeature) source;
      if (road.getBarrierLeft() == null) {
        road.setBarrierLeft(null);
      }
      if (road.getBarrierRight() == null) {
        road.setBarrierRight(null);
      }

      if (road.getTrafficDirection() == null) {
        road.setTrafficDirection(TrafficDirection.BOTH);
      }
    }

    if (source.getCharacteristicsType() == CharacteristicsType.OPS) {
      final OPSCharacteristics chars = (OPSCharacteristics) source.getCharacteristics();
      if (chars.getBuildingId() == null) {
        chars.setBuildingId(null);
      }

      if (chars.getHeatContentType() == HeatContentType.FORCED) {
        chars.setHeatContent(0.0);

        // Set the default temp if there is no temperature (see AER-125 and AER3-840)
        if (Js.isFalsy(chars.getEmissionTemperature())) {
          chars.setEmissionTemperature(OPSLimits.AVERAGE_SURROUNDING_TEMPERATURE);
        }
      } else {
        chars.setEmissionTemperature(OPSLimits.AVERAGE_SURROUNDING_TEMPERATURE);
        chars.setOutflowDiameter(OPSLimits.SCOPE_NO_BUILDING_OUTFLOW_DIAMETER_MINIMUM);
        chars.setOutflowVelocity(0.0);
        chars.setOutflowDirection(OutflowDirectionType.VERTICAL);
      }
    }
  }
}
