/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.util;

public class OrientedEnvelope {
  private final boolean rectangular;

  private final double orientation;
  private final double width;
  private final double length;

  /**
   * @param orientation angle in degrees
   * @param width       in meters
   * @param length      in meters
   */
  public OrientedEnvelope(final double orientation, final double width, final double length, final boolean rectangular) {
    this.orientation = orientation;
    this.width = width;
    this.length = length;
    this.rectangular = rectangular;
  }

  /**
   * Retrieve the envelope orientation, in degrees
   */
  public double getOrientation() {
    return orientation;
  }

  public double getWidth() {
    return width;
  }

  public double getLength() {
    return length;
  }

  public boolean isRectangular() {
    return rectangular;
  }
}
