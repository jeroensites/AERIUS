/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.calculation;

import javax.inject.Inject;

import com.google.gwt.user.client.Timer;
import com.google.web.bindery.event.shared.EventBus;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.util.NotificationUtil;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.event.CalculationCancelEvent;
import nl.overheid.aerius.wui.application.event.CalculationStatusEvent;
import nl.overheid.aerius.wui.application.service.AeriusRequestCallback;
import nl.overheid.aerius.wui.application.service.CalculationServiceAsync;

public class CalculationStatusPoller {
  private static final int INTERVAL = 2000;

  private static final int RETRY_AMOUNT = 5;

  @Inject CalculationServiceAsync service;
  @Inject EventBus eventBus;

  private String jobId;

  private boolean running;
  private final Timer timer = new Timer() {
    @Override
    public void run() {
      if (!running) {
        return;
      }

      doFetchCalculationStatus();
    }
  };

  private int retriesLeft = RETRY_AMOUNT;

  public void start(final String jobId) {
    this.jobId = jobId;
    if (running) {
      return;
    }

    running = true;
    SchedulerUtil.delay(() -> doFetchCalculationStatus());
  }

  public void stop() {
    running = false;
  }

  public boolean isRunning() {
    return running;
  }

  private void doFetchCalculationStatus() {
    service.getCalculationStatus(jobId, AeriusRequestCallback.create(v -> {
      // Reset retry amount on success
      retriesLeft = RETRY_AMOUNT;
      if (!running) {
        return;
      }

      eventBus.fireEvent(new CalculationStatusEvent(v));

      if (v.getJobProgress().getState().isFinalState()) {
        running = false;
      } else {
        timer.schedule(INTERVAL);
      }
    }, e -> {
      // Retry a couple times before erroring out
      if (retriesLeft == 0) {
        running = false;
        NotificationUtil.broadcastError(eventBus, e);
        // TODO: Better define what should happen here
        eventBus.fireEvent(new CalculationCancelEvent());
        return;
      }

      GWTProd.warn("Error while fetching status, retrying " + retriesLeft + " times.");
      retriesLeft -= 1;
      timer.schedule(INTERVAL);
    }));
  }

  // TODO: Get graph data, using CalculationNatureReserveResults

}
