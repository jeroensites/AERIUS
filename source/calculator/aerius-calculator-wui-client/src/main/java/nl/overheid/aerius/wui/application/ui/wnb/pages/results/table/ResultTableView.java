/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.results.table;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.gwt.resources.client.DataResource;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.transition.VerticalCollapseGroup;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.collapsible.CollapsiblePanel;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSelectionContext;
import nl.overheid.aerius.wui.application.daemon.calculation.ResultSummaryContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsAreaSummary;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsStatistics;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationMarker;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationMarkers;
import nl.overheid.aerius.wui.application.geo.util.GeoUtil;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.stats.ResultsStatisticsFormatter;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    CollapsiblePanel.class,
    ButtonIcon.class,
    VerticalCollapseGroup.class
})
public class ResultTableView extends BasicVueComponent {
  @Prop EventBus eventBus;
  @Prop(required = true) ResultSummaryContext context;

  @Data @Inject ResultSelectionContext selectionContext;
  @Data @Inject ApplicationContext applicationContext;

  @Inject ResultsStatisticsFormatter resultsStatisticsFormatter;

  @Computed
  public DepositionValueDisplayType getDisplayType() {
    return applicationContext.getActiveThemeConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @JsMethod
  public DataResource getCalculationMarker(final ResultStatisticType statisticType) {
    final CalculationMarker calculationMarker = CalculationMarker.getCalculationMarker(context.getResultType(), statisticType);
    return CalculationMarkers.getMarkerImage(Collections.singleton(calculationMarker));
  }

  @Computed
  public List<ResultStatisticType> getScenarioResultTypes() {
    return ResultStatisticType.getValuesForResult(context.getResultType(), context.getHexagonType()).stream().filter(
        statisticType -> CalculationMarker.getCalculationMarker(context.getResultType(), statisticType) != null).collect(Collectors.toList());
  }

  @JsMethod
  public String formatValueFor(final SituationResultsStatistics statistics, final ResultStatisticType resultStatisticType) {
    return resultsStatisticsFormatter.formatStatisticType(statistics, resultStatisticType);
  }

  @JsMethod
  public void zoomToArea(final SituationResultsAreaSummary areaSummary) {
    eventBus.fireEvent(GeoUtil.createMapSetExtendCommand(areaSummary.getAssessmentArea().getBounds()));
  }
}
