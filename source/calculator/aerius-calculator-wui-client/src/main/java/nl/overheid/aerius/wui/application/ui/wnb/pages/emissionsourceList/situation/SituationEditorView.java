/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.situation;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.vuelidate.ValidateDirective;
import nl.aerius.vuelidate.util.VuelidateUtil;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.util.NotificationUtil;
import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.wui.application.command.situation.SituationChangeNettingFactorCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationChangeTypeCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationChangeYearCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationDeleteCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationRenameCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.input.LabeledInputComponent;
import nl.overheid.aerius.wui.application.components.input.drop.SimplifiedListBoxComponent;
import nl.overheid.aerius.wui.application.components.tooltip.TooltipComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.situation.SituationValidators.SituationValidations;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(components = {
    ButtonIcon.class,
    TooltipComponent.class,
    SimplifiedListBoxComponent.class,
    LabeledInputComponent.class,
    VerticalCollapse.class
}, customizeOptions = SituationValidators.class, directives = ValidateDirective.class)
public class SituationEditorView extends BasicVueComponent implements HasCreated {
  @Prop EventBus eventBus;
  @Prop SituationContext situation;

  @Data @JsProperty List<SituationType> situationTypes = new ArrayList<>();
  @Data @JsProperty List<String> situationYears = new ArrayList<>();

  @Data String situationName;
  @Data SituationType situationType;
  @Data String situationYear;
  @Data Double situationNettingFactor;

  @Inject @Data ApplicationContext applicationContext;
  @Inject @Data ScenarioContext scenarioContext;

  private boolean embargo = false;

  @Data boolean editMode = false;

  @JsProperty(name = "$v") SituationValidations validation;

  @Computed
  public SituationValidations getV() {
    return validation;
  }

  @Watch("situationNettingFactor")
  void onSituationNettingFactorChange(final Double neww) {
    startEdit();
  }

  @Watch(value = "situation", isImmediate = true)
  public void onSituationChange(final SituationContext neww) {
    if (neww != null && neww.getType() == null) {
      GWTProd.error("Invalid situation: ", neww);
    }

    cancelEdit();
    if (neww != null) {
      situationType = neww.getType();
      situationYear = neww.getYear();
    }
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
  }

  private void update() {
    if (situation == null) {
      return;
    }

    if (!editMode) {
      resetModel();
    }
  }

  private void resetModel() {
    embargo = true;

    situationName = situation.getName();
    situationType = situation.getType();
    situationYear = situation.getYear();
    situationNettingFactor = situation.getNettingFactor();
    validation.$reset();

    Vue.nextTick(() -> embargo = false);
  }

  @JsMethod
  public void selectSituationType(final SituationType option) {
    startEdit();
    this.situationType = option;
    if (this.situationType.equals(SituationType.NETTING)) {
      situationNettingFactor = SituationContext.DEFAULT_NETTING_FACTOR;
    }
  }

  @JsMethod
  public void selectSituationYear(final String option) {
    startEdit();
    this.situationYear = option;
  }

  @JsMethod
  public void deleteSituation() {
    if (Window.confirm(i18n.navigationSituationDeleteConfirm(situation.getName()))) {
      eventBus.fireEvent(new SituationDeleteCommand(situation));
    }
  }

  @Computed("nettingSituation")
  public boolean isNettingSituation() {
    return situationType != null && situationType == SituationType.NETTING;
  }

  @JsMethod
  protected String nettingFactorError() {
    return i18n.ivDoubleRangeBetween(i18n.situationNettingFactorLabel(), 0, 1);
  }

  @Computed
  protected Double getNettingFactor() {
    return situationNettingFactor;
  }

  public boolean isError() {
    return validation.situationNettingFactor.error;
  }

  @JsMethod
  public void startEdit() {
    if (embargo) {
      return;
    }

    editMode = true;
  }

  @JsMethod
  public void cancelEdit() {
    editMode = false;
    update();
  }

  @JsMethod
  public void applyEdit() {
    if (!isError()) {
      boolean isSituationTypeError = false;

      if (!situation.getName().equals(situationName)) {
        eventBus.fireEvent(new SituationRenameCommand(situation, situationName));
      }

      if (!situation.getType().equals(situationType)) {
        isSituationTypeError = !this.setTypeOfSituationOrBroadcastWarning();
      }

      if (!situation.getYear().equals(situationYear)) {
        eventBus.fireEvent(new SituationChangeYearCommand(situation, situationYear));
      }

      // Always allow an existing NETTING situation to update,
      // but only allow a new one if the max amount of netting situations hasn't been reached
      if (situationType == SituationType.NETTING && !isSituationTypeError) {
        eventBus.fireEvent(new SituationChangeNettingFactorCommand(situation, situationNettingFactor));
      }

      cancelEdit();
    }
  }

  private boolean setTypeOfSituationOrBroadcastWarning() {
    // Only allow changing to a situation if the max for that SituationType hasn't been reached yet
    if (hasTooManySituationsOfType(SituationType.REFERENCE)) {
      NotificationUtil.broadcastWarning(eventBus, M.messages()
          .warningCreateTooManyReferenceSituations(applicationContext.getMaxNumberOfSituationsForType(SituationType.REFERENCE)));
      return false;
    } else if (hasTooManySituationsOfType(SituationType.NETTING)) {
      NotificationUtil.broadcastWarning(eventBus, M.messages()
          .warningCreateTooManyNettingSituations(applicationContext.getMaxNumberOfSituationsForType(SituationType.NETTING)));
      return false;
    } else {
      eventBus.fireEvent(new SituationChangeTypeCommand(situation, situationType));
      return true;
    }
  }

  private long getAmountOfSituationsWithType(final SituationType type) {
    return scenarioContext.getSituations().stream().filter(s -> s.getType() == type).count();
  }

  private boolean hasTooManySituationsOfType(final SituationType type) {
    return situationType == type &&
        getAmountOfSituationsWithType(type) >= applicationContext.getMaxNumberOfSituationsForType(type);
  }

}
