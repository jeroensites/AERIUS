/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.overview;

import com.google.gwt.event.dom.client.DragEnterEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.vue.activity.AbstractVueActivity;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.command.UserImportRequestedCommand;
import nl.overheid.aerius.wui.application.place.OverviewPlace;
import nl.overheid.aerius.wui.application.place.ThemeLaunchPlace;

public class OverviewActivity extends AbstractVueActivity<OverviewPresenter, OverviewView, OverviewViewFactory>
implements OverviewPresenter {
  @Inject PlaceController placeController;

  private HandlerRegistration dragRegister;

  @Inject
  public OverviewActivity(final @Assisted OverviewPlace overviewPlace) {
    super(OverviewViewFactory.get());
  }

  @Override
  public void onStart() {
    dragRegister = RootPanel.get().addDomHandler(event -> eventBus.fireEvent(new UserImportRequestedCommand()), DragEnterEvent.getType());
  }

  @Override
  public void onStop() {
    dragRegister.removeHandler();
  }

  @Override
  public OverviewActivity getPresenter() {
    return this;
  }

  public boolean isEnabled(final Theme theme) {
    return true;
  }

  public void selectTheme(final Theme theme) {
    placeController.goTo(new ThemeLaunchPlace(theme));
  }
}
