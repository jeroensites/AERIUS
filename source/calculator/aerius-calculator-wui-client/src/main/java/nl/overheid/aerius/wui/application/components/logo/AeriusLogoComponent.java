/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.logo;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.application.resources.ImageResources;
import nl.overheid.aerius.wui.application.resources.R;

@Component(name = "aer-logo")
public class AeriusLogoComponent implements IsVueComponent {
  @Inject PlaceController placeController;

  @Computed
  public ImageResources img() {
    return R.images();
  }

  @JsMethod
  public void onLogoClick() {
//TODO AER3-118    placeController.goTo(new OverviewPlace());
  }
}
