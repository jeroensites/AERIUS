/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.wnb.results;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsMethod;

import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.PrintWnbContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationJobContext;
import nl.overheid.aerius.wui.application.domain.info.AssessmentArea;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsAreaSummary;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsStatistics;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.stats.ResultsStatisticsFormatter;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(name = "result-summary-table", components = {
    ResultSummaryCalculationPointsTable.class,
})
public class ResultSummaryTable extends BasicVueEventComponent implements HasCreated {
  private static final ResultSummaryTableEventBinder EVENT_BINDER = GWT.create(ResultSummaryTableEventBinder.class);

  interface ResultSummaryTableEventBinder extends EventBinder<ResultSummaryTable> {}

  @Inject ApplicationContext applicationContext;

  @Inject @Data PrintWnbContext printWnbContext;
  @Inject ScenarioContext scenarioContext;
  @Inject CalculationContext calculationContext;
  @Inject ResultsStatisticsFormatter resultsStatisticsFormatter;
  @Data SituationResultsStatistics totalStatistics;
  @Prop EventBus eventBus;

  @Override
  public void created() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Computed
  public DepositionValueDisplayType getDisplayType() {
    return applicationContext.getActiveThemeConfiguration().getEmissionResultValueDisplaySettings().getDisplayType();
  }

  @Computed
  public String getIntro() {
    return i18n.pdfResultSummaryTableIntro(scenarioContext.getActiveSituation() != null ? scenarioContext.getActiveSituation().getName() : "");
  }

  @Computed
  public List<ResultStatisticType> getResultStatisticTypes() {
    return ResultStatisticType.getValuesForResult(printWnbContext.getScenarioResultType(), printWnbContext.getSummaryHexagonType());
  }

  @Computed
  public SituationResultsAreaSummary[] getAreas() {
    final Optional<CalculationJobContext> activeCalculation = Optional.ofNullable(calculationContext.getActiveCalculation());
    return activeCalculation.map(ac -> ac.getResultContext().getActiveResultSummary())
        .map(ars -> ars.getAreaStatistics())
        .orElse(new SituationResultsAreaSummary[0]);
  }

  @Computed
  public AssessmentArea[] getOmittedAreas() {
    final Optional<CalculationJobContext> activeCalculation = Optional.ofNullable(calculationContext.getActiveCalculation());
    return activeCalculation.map(ac -> ac.getResultContext().getActiveResultSummary())
        .map(ars -> ars.getOmittedAreas())
        .orElse(new AssessmentArea[0]);
  }

  @EventHandler
  public void onSituationResultsSelectedEvent(final SituationResultsSelectedEvent e) {
    final Optional<CalculationJobContext> activeCalculation = Optional.ofNullable(calculationContext.getActiveCalculation());
    totalStatistics = activeCalculation.map(ac -> ac.getResultContext().getActiveResultSummary())
        .map(ars -> ars.getStatistics())
        .orElse(new SituationResultsStatistics());
  }

  @JsMethod
  public String formatValueFor(final SituationResultsStatistics statistics, final ResultStatisticType type) {
    return resultsStatisticsFormatter.formatStatisticType(statistics, type);
  }
}
