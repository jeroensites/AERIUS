/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context.dummy;

import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.context.SituationContext;

/**
 * Generates a dummy connect until actual editting and importing works.
 */
public class DummySituationContext {
  public static SituationContext dummy(final SituationContext sc) {
    sc.setName("dummy");
    sc.setType(SituationType.PROPOSED);

//    emissionEditorDummies(sc);
    quickCalculationDummies(sc);
//    roadDummies(sc);

    return sc;
  }

  private static void roadDummies(final SituationContext sc) {
    sc.setSources(DummyEmissionSources.sources(12, false)
        .stream()
        .filter(v -> v.getEmissionSourceType() == EmissionSourceType.SRM2_ROAD)
        .collect(Collectors.toList()));
  }

  /**
   * Create dummies that are done calculating quickly
   */
  private static void quickCalculationDummies(final SituationContext sc) {
    sc.setSources(DummyEmissionSources.sources(1, false));
  }

  /**
   * Generate dummies for each emission source variant
   */
  private static void emissionEditorDummies(final SituationContext sc) {
    sc.setSources(DummyEmissionSources.sources(12, true));
    sc.setBuildings(DummyEmissionSources.buildings());
  }
}
