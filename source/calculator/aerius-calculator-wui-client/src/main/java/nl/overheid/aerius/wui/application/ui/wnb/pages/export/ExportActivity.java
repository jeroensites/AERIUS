/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.wnb.pages.export;

import nl.aerius.wui.event.BasicEventComponent;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.wui.application.command.ExportStartCommand;
import nl.overheid.aerius.wui.application.components.map.MapComponentFactory;
import nl.overheid.aerius.wui.application.domain.export.ExportOptions;
import nl.overheid.aerius.wui.application.ui.main.MainView;
import nl.overheid.aerius.wui.application.ui.pages.ThemeDelegatedActivity;
import nl.overheid.aerius.wui.application.ui.pages.ThemeView;

public class ExportActivity extends BasicEventComponent implements ThemeDelegatedActivity {

  @Override
  public void onStart(ThemeView view) {
    view.setDelegatedPresenter(this);
    view.setLeftView(ExportViewFactory.get(), MainView.FlexView.RIGHT);

    view.setNoMiddleView();
    view.setRightView(MapComponentFactory.get());
    view.attachMap(() -> view.rightComponent);
  }

  public void startExport(final JobType jobType, final ExportType exportType, final String email) {
    ExportOptions exportOptions = new ExportOptions();
    exportOptions.setEmail(email);
    exportOptions.setJobType(jobType);
    exportOptions.setExportType(exportType);
    eventBus.fireEvent(new ExportStartCommand(exportOptions));
  }
}
