/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.farmland;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.wui.application.domain.source.AbstractSubSource;

/**
 * Client side implementation of Farm land activity
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class FarmlandActivity extends AbstractSubSource {
  private String activityCode;

  public static @JsOverlay FarmlandActivity create(final FarmlandType farmlandType) {
    final FarmlandActivity props = new FarmlandActivity();
    AbstractSubSource.init(props);
    props.setEmission(Substance.NOX, 0D);
    props.setEmission(Substance.NH3, 0D);
    props.setActivityCode(farmlandType == null ? "" : farmlandType.name());
    return props;
  }

  public final @JsOverlay String getActivityCode() {
    return activityCode;
  }

  public final @JsOverlay void setActivityCode(final String activityCode) {
    this.activityCode = activityCode;
  }

}
