/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.place;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import nl.aerius.wui.place.PlaceTokenizer;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourcePlace;

public class ThemePlaceProvider {

  public static final ThemePlaceProvider INSTANCE = new ThemePlaceProvider();

  private final Map<Theme, Supplier<? extends MainThemePlace>> themes = new EnumMap<>(Theme.class);

  public ThemePlaceProvider() {
    themes.put(Theme.RBL, () -> new SourcePlace());
    themes.put(Theme.WNB, () -> new EmissionSourceListPlace(Theme.WNB));
  }

  public MainThemePlace getPlace(final Theme option) {
    return themes.get(option).get();
  }

  public static List<PlaceTokenizer<?>> tokenizers(final Theme theme) {
    return Arrays.asList(
        BuildingPlace.createTokenizer(theme),
        CalculatePlace.createTokenizer(theme),
        EmissionSourceListPlace.createTokenizer(theme),
        EmissionSourcePlace.createTokenizer(theme),
        ExportPlace.createTokenizer(theme),
        PreferencesPlace.createTokenizer(theme),
        ResultsPlace.createTokenizer(theme),
        ThemeLaunchPlace.createTokenizer(theme));
  }
}
