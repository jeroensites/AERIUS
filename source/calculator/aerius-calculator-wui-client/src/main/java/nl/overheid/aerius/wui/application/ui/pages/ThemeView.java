/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.pages;

import java.util.function.Supplier;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasActivated;
import com.axellience.vuegwt.core.client.vue.VueComponentFactory;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsProperty;

import nl.aerius.wui.activity.Presenter;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.components.map.MapComponent;
import nl.overheid.aerius.wui.application.context.NavigationContext;
import nl.overheid.aerius.wui.application.ui.main.MainView;
import nl.overheid.aerius.wui.application.ui.main.MainView.FlexView;
import nl.overheid.aerius.wui.application.ui.pages.language.LanguageView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.building.BuildingView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.calculate.CalculateView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.calculate.CalculationListView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.calculationpoints.CalculationPointsView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsource.EmissionSourceDetailView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsource.EmissionSourceView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.ScenarioInputListDetailView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsourceList.ScenarioInputListView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.export.ExportView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.launch.WnbThemeLaunchView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.preferences.PreferencesView;
import nl.overheid.aerius.wui.application.ui.wnb.pages.results.ResultsView;

/**
 * Base Theme view.
 *
 * This class should contain all possible views of all themes available in the components list below.
 * (Unless we find a way to make this theme specific).
 */
@Component(components = {
    MainView.class,

    ScenarioInputListView.class,
    ScenarioInputListDetailView.class,

    EmissionSourceView.class,
    EmissionSourceDetailView.class,

    CalculationPointsView.class,
    ExportView.class,
    WnbThemeLaunchView.class,
    PreferencesView.class,
    MapComponent.class,
    ResultsView.class,
    BuildingView.class,
    LanguageView.class,

    CalculateView.class,
    CalculationListView.class,
})
public class ThemeView implements IsVueComponent, HasActivated {
  @Inject PlaceController placeController;

  @Data @JsProperty NavigationContext navigation;

  @Prop EventBus eventBus;
  @Prop ThemePresenter presenter;

  @Data Presenter delegatedPresenter;

  @Ref MainView outer;
  @Ref public IsVueComponent leftComponent;
  @Ref public IsVueComponent middleComponent;
  @Ref public IsVueComponent rightComponent;

  @Data String left;
  @Data String middle;
  @Data String right;

  @Data boolean softMiddle = true;

  @Override
  public void activated() {
    presenter.setView(this);
  }

  public <P extends Presenter> void setDelegatedPresenter(final P presenter) {
    this.delegatedPresenter = presenter;
  }

  public void setLeftView(final VueComponentFactory<?> fact, final FlexView flexView) {
    left = fact.getComponentTagName();
    outer.setFlexView(flexView);
  }

  public void setOnlyRightView() {
    left = null;
    middle = null;
  }

  public void setOnlyLeftView() {
    middle = null;
    right = null;
  }

  public void setMiddleView(final VueComponentFactory<?> fact) {
    middle = fact.getComponentTagName();
  }

  public void setNoMiddleView() {
    middle = null;
  }

  public void setRightView(final VueComponentFactory<?> fact) {
    right = fact.getComponentTagName();
  }

  public void setRightView(final VueComponentFactory<?> fact, final FlexView flexView) {
    right = fact.getComponentTagName();
    outer.setFlexView(flexView);
  }

  public void attachMap(final Supplier<IsVueComponent> vueComponent) {
    SchedulerUtil.delay(() -> ((MapComponent) vueComponent.get()).attach());
  }

  public void setNavigation(final NavigationContext nav) {
    this.navigation = nav;
  }

  public void setSoftMiddle(final boolean soft) {
    softMiddle = soft;
  }
}
