/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.air.receptors.create;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;

import nl.overheid.aerius.wui.application.components.dummy.SimpleSecondaryDummyComponent;
import nl.overheid.aerius.wui.application.components.map.MapComponent;
import nl.overheid.aerius.wui.application.components.views.DefaultTitleView;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    DefaultTitleView.class,
    SimpleSecondaryDummyComponent.class,
    MapComponent.class })
public class ReceptorCreateView extends BasicVueView {
  @Prop ReceptorCreateActivity presenter;
}
