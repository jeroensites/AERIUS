/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.print.wnb;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.util.SchedulerUtil;
import nl.aerius.wui.vue.activity.AbstractVueActivity;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.wui.application.command.ChangeThemeCommand;
import nl.overheid.aerius.wui.application.command.RestoreCalculationContextCommand;
import nl.overheid.aerius.wui.application.command.RestoreScenarioContextCommand;
import nl.overheid.aerius.wui.application.command.situation.SwitchSituationCommand;
import nl.overheid.aerius.wui.application.context.PrintWnbContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.event.RestoreScenarioContextCompletedEvent;
import nl.overheid.aerius.wui.application.place.PrintWnbPlace;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

public class PrintWnbActivity extends AbstractVueActivity<PrintWnbPresenter, PrintWnbView, PrintWnbViewFactory> implements PrintWnbPresenter {

  // 1 minute
  private static final int RENDER_FINISH_FALLBACK_PERIOD = 60 * 1_000;

  interface PrintWnbActivityEventBinder extends EventBinder<PrintWnbActivity> {
  }

  private final PrintWnbActivityEventBinder EVENT_BINDER = GWT.create(PrintWnbActivityEventBinder.class);
  private final PrintWnbPlace place;
  @Inject PrintWnbContext context;
  @Inject EnvironmentConfiguration cfg;
  @Inject ScenarioContext scenarioContext;

  @Inject
  public PrintWnbActivity(final @Assisted PrintWnbPlace place) {
    super(PrintWnbViewFactory.get());

    this.place = place;
  }

  @Override
  public void onStart() {
    // Set theme to WNB
    eventBus.fireEvent(new ChangeThemeCommand(Theme.WNB));

    GWTProd.log("Scenario/situation file codes: ", place.getScenarioFileCodes());
    eventBus.fireEvent(new RestoreScenarioContextCommand(place.getScenarioFileCodes()));

    GWTProd.log("Job ID: ", place.getJobId());
    context.setJobId(place.getJobId());

    GWTProd.log("Reference: ", place.getReference());
    context.setReference(place.getReference());

    GWTProd.log("Proposed situation id: ", place.getProposedSituationId());
    context.setProposedSituationId(place.getProposedSituationId());

    SchedulerUtil.delay(() -> context.fail(), RENDER_FINISH_FALLBACK_PERIOD);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    setEventBus(eventBus, this, EVENT_BINDER);
  }

  @Override
  public PrintWnbActivity getPresenter() {
    return this;
  }

  @EventHandler
  public void handleScenarioContextRestored(final RestoreScenarioContextCompletedEvent e) {
    final String proposedSituationCode = place.getProposedSituationId();

    // Set active situation to the proposed situation this Project Calculation PDF is rendering.
    // Primary purpose is showing the right markers on the result summary map.
    eventBus.fireEvent(new SwitchSituationCommand(proposedSituationCode));

    eventBus.fireEvent(new RestoreCalculationContextCommand());
  }
}
