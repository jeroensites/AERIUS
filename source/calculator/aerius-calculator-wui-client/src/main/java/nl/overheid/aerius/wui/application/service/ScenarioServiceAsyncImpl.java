/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.service;

import javax.inject.Inject;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Singleton;

import jsinterop.base.JsPropertyMap;

import nl.aerius.wui.util.RequestUtil;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcel;
import nl.overheid.aerius.wui.config.EnvironmentConfiguration;

@Singleton
public class ScenarioServiceAsyncImpl implements ScenarioServiceAsync {

  @Inject EnvironmentConfiguration cfg;
  @Inject HeaderHelper hdr;
  @Inject FileServiceAsync fileService;

  @Override
  public void importSituation(final String fileCode, final AsyncCallback<ImportParcel> callback) {
    final String url = RequestUtil.prepareUrl(cfg.getConnectBaseUrl(), RequestMappings.CONNECT_UI_RETRIEVE_IMPORT_PARCEL,
        RequestMappings.FILE_CODE, fileCode);
    RequestUtil.doGetWithHeaders(url, hdr.defaultHeaders(), AeriusRequestCallback.createAsync(callback));
  }

  @Override
  public void importScenario(final String fileCode, final AsyncCallback<JsPropertyMap<?>> callback) {
    final String url = fileService.getFileDownloadUrl(fileCode);

    RequestUtil.doGetWithHeaders(url, hdr.defaultHeaders(), AeriusRequestCallback.createAsync(callback));
  }
}
