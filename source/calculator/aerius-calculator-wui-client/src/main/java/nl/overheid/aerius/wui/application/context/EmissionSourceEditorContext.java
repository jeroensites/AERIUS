/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import javax.inject.Singleton;

import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;

@Singleton
public class EmissionSourceEditorContext {
  private EmissionSourceFeature source = null;

  private int subSourceSelection = -1;

  public EmissionSourceFeature getSource() {
    return source;
  }

  public void setSource(final EmissionSourceFeature source) {
    this.source = source;
    resetSubSourceSelection();
  }

  public boolean isEditing() {
    return source != null;
  }

  public void resetSubSourceSelection() {
    subSourceSelection = -1;
  }

  public int getSubSourceSelection() {
    return subSourceSelection;
  }

  public void setSubSourceSelection(final int subSourceSelection) {
    this.subSourceSelection = subSourceSelection;
  }

  public boolean hasSubSourceSelection() {
    return subSourceSelection > -1;
  }

  public void reset() {
    source = null;
    resetSubSourceSelection();
  }
}
