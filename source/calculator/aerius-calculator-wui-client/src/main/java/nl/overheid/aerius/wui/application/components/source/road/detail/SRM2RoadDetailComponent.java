/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.source.road.detail;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.aerius.wui.vue.transition.VerticalCollapse;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.srm2.conversion.Sigma0Calculator;
import nl.overheid.aerius.wui.application.components.source.characteristics.MinMaxLabelComponent;
import nl.overheid.aerius.wui.application.domain.source.SRM2RoadESFeature;
import nl.overheid.aerius.wui.application.domain.source.road.RoadSideBarrier;
import nl.overheid.aerius.wui.application.domain.source.road.VehicleType;
import nl.overheid.aerius.wui.application.domain.source.road.Vehicles;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-srm2road-detail", components = {
    RoadDetailRowComponent.class,
    VerticalCollapse.class,
    MinMaxLabelComponent.class,
})
public class SRM2RoadDetailComponent extends BasicVueComponent implements IsVueComponent {
  @Prop @JsProperty SRM2RoadESFeature source;
  @Prop @JsProperty SectorCategories sectorCategories;
  @Prop @JsProperty List<Substance> substances;

  @Computed("substances")
  public List<Substance> getSubstances() {
    return substances;
  }

  @Computed("vehicleTypes")
  protected EnumSet<nl.overheid.aerius.shared.domain.v2.source.road.VehicleType> getVehicleTypes() {
    return EnumSet.allOf(nl.overheid.aerius.shared.domain.v2.source.road.VehicleType.class);
  }

  @JsMethod
  public String getSectorDescription(final int sectorId) {
    return sectorCategories.getSectorById(sectorId).getDescription();
  }

  @JsMethod
  public boolean hasVechicleType(final VehicleType vehicleType) {
    return getVehiclesByType(vehicleType).size() > 0;
  }

  @JsMethod
  public List<Vehicles> getVehiclesByType(final VehicleType vehicleType) {
    final List<Vehicles> list = new ArrayList<>();
    for (final Vehicles vehicle : source.getSubSources().asList()) {
      if (vehicle.getVehicleType().equals(vehicleType)) {
        list.add(vehicle);
      }
    }
    return list;
  }

  @JsMethod
  String barrierType(final RoadSideBarrier barrier) {
    return barrier == null ? "-" : i18n.esRoadBarrierType(barrier.getBarrierType());
  }

  @JsMethod
  public String barrierHeight(final RoadSideBarrier barrier) {
    return barrier == null ? "-" : i18n.unitM(i18n.decimalNumberFixed(barrier.getHeight(), 1));
  }

  @JsMethod
  public String barrierHeightCorrected(final RoadSideBarrier barrier) {
    if (barrier == null) {
      return "-";
    }

    final double usedValue = barrier.getHeight() < Sigma0Calculator.MIN_BARRIER_HEIGHT
        ? 0D
        : Math.min(Sigma0Calculator.MAX_BARRIER_HEIGHT, barrier.getHeight());
    return i18n.unitM(i18n.decimalNumberFixed(usedValue, 1));
  }

  @JsMethod
  public String barrierDistance(final RoadSideBarrier barrier) {
    return barrier == null ? "-" : i18n.unitM(i18n.decimalNumberFixed(barrier.getDistance(), 1));
  }

  @JsMethod
  public String barrierDistanceCorrected(final RoadSideBarrier barrier) {
    return barrier == null || barrier.getDistance() > Sigma0Calculator.MAX_BARRIER_DISTANCE
        ? "-"
        : i18n.unitM(i18n.decimalNumberFixed(barrier.getDistance(), 1));
  }

}
