/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.scenario;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.base.Js;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.aerius.wui.util.NotificationUtil;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceType;
import nl.overheid.aerius.wui.application.command.DeleteAllCalculationPointsCommand;
import nl.overheid.aerius.wui.application.command.ImportFilesCommand;
import nl.overheid.aerius.wui.application.command.RestoreScenarioContextCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingAddCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingDeleteCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.AddCalculationPointCommand;
import nl.overheid.aerius.wui.application.command.calculationpoint.AddCalculationPointsCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationChangeNettingFactorCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationChangeTypeCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationChangeYearCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationDeleteCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationRenameCommand;
import nl.overheid.aerius.wui.application.command.situation.SwitchSituationCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceAddCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceCreateFirstCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceDeleteCommand;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.PrintWnbContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.geo.CalculationPointUtil;
import nl.overheid.aerius.wui.application.domain.geo.FeatureUtil;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcelConverter;
import nl.overheid.aerius.wui.application.domain.importer.ImportParcelSituation;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.domain.source.EmissionSourceFeature;
import nl.overheid.aerius.wui.application.domain.source.InlandShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.MaritimeShippingESFeature;
import nl.overheid.aerius.wui.application.domain.source.OPSCharacteristics;
import nl.overheid.aerius.wui.application.event.ImportCompleteEvent;
import nl.overheid.aerius.wui.application.event.ImportFailureEvent;
import nl.overheid.aerius.wui.application.event.RestoreScenarioContextCompletedEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingListChangeEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointsAddedEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;
import nl.overheid.aerius.wui.application.event.source.EmissionSourceListChangeEvent;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.EmissionSourceListPlace;
import nl.overheid.aerius.wui.application.place.ThemeLaunchPlace;
import nl.overheid.aerius.wui.application.service.ScenarioServiceAsync;

@Singleton
public class ScenarioContextDaemon extends BasicEventComponent implements Daemon {
  private static final ScenarioContextDaemonEventBinder EVENT_BINDER = GWT.create(ScenarioContextDaemonEventBinder.class);

  interface ScenarioContextDaemonEventBinder extends EventBinder<ScenarioContextDaemon> {}

  private static final String BUILDING_PREFIX = "Gebouw.";

  @Inject PlaceController placeController;
  @Inject ScenarioServiceAsync service;
  @Inject FileImportPoller poller;
  @Inject ScenarioContext scenarioContext;
  @Inject ApplicationContext applicationContext;
  @Inject PrintWnbContext printWnbContext;
  @Inject ImportParcelConverter converter;
  private boolean pendingImport;

  @EventHandler
  public void onSituationSwitchCommand(final SwitchSituationCommand c) {
    c.setSilent(!scenarioContext.setActiveSituationCode(c.getValue()));
  }

  @EventHandler
  public void onSituationSwitchEvent(final SituationSwitchEvent e) {
    eventBus.fireEvent(new EmissionSourceListChangeEvent(scenarioContext.getActiveSituation().getSources(),
        scenarioContext.getActiveSituationCode()));

    eventBus.fireEvent(new BuildingListChangeEvent(scenarioContext.getActiveSituation().getBuildings(),
        scenarioContext.getActiveSituationCode()));
  }

  @EventHandler
  public void onSituationRenameCommand(final SituationRenameCommand c) {
    c.getValue().setName(c.getNewName());
  }

  @EventHandler
  public void onSituationChangeTypeCommand(final SituationChangeTypeCommand c) {
    c.getValue().setType(c.getNewType());
  }

  @EventHandler
  public void onSituationChangeYearCommand(final SituationChangeYearCommand c) {
    c.getValue().setYear(c.getNewYear());
  }

  @EventHandler
  public void onSituationChangeNettingFactorCommand(final SituationChangeNettingFactorCommand c) {
    c.getValue().setNettingFactor(c.getNewNettingFactor());
  }

  @EventHandler
  public void onSituationDeleteCommand(final SituationDeleteCommand c) {
    c.setSilent(!scenarioContext.deleteSituation(c.getValue()));
  }

  @EventHandler
  public void onImportFilesCommand(final ImportFilesCommand command) {
    if (pendingImport) {
      return;
    }
    poller.start(command.getValue(), command.isIntoActiveSituation(), command.getImportFilter());
  }

  @EventHandler
  public void onBuildingAddCommand(final BuildingAddCommand c) {
    final String situationCode = c.getSituationId() == null ? scenarioContext.getActiveSituationCode() : c.getSituationId();
    final List<BuildingFeature> buildings = scenarioContext.getSituation(situationCode).getOriginalBuildings();

    c.setSituationId(situationCode);

    final BuildingFeature feature = c.getValue();
    FeatureUtil.addAndSetId(buildings, feature);
    feature.setGmlId(BUILDING_PREFIX + feature.getId());
  }

  @EventHandler
  public void onBuildingDeleteCommand(final BuildingDeleteCommand c) {
    final String situationCode = c.getSituationId() == null ? scenarioContext.getActiveSituationCode() : c.getSituationId();
    final List<BuildingFeature> buildings = scenarioContext.getSituation(situationCode).getOriginalBuildings();

    // First, unlink the building entity
    ScenarioContext.findBuildingLinkedSources(scenarioContext, situationCode, c.getValue().getGmlId()).stream()
        .forEach(v -> {
          ((OPSCharacteristics) Js.uncheckedCast(v.getCharacteristics())).setBuildingId(null);
        });

    // Then, remove the building entity
    c.setSituationId(situationCode);
    c.setSilent(!buildings.remove(c.getValue()));
  }

  @EventHandler
  public void onEmissionSourceAddCommand(final EmissionSourceAddCommand c) {
    final String situationCode = c.getSituationId() == null ? scenarioContext.getActiveSituationCode() : c.getSituationId();
    final List<EmissionSourceFeature> sources = scenarioContext.getSituation(situationCode).getOriginalSources();

    c.setSituationId(situationCode);
    final EmissionSourceFeature sourceToAdd = c.getValue();
    FeatureUtil.addAndSetId(sources, sourceToAdd);
    if (sourceToAdd.getGmlId() == null) {
      sourceToAdd.setGmlId(sourceToAdd.getId());
    }
  }

  @EventHandler
  public void onEmissionSourceDeleteCommand(final EmissionSourceDeleteCommand c) {
    final String situationCode = c.getSituationId() == null ? scenarioContext.getActiveSituationCode() : c.getSituationId();
    final List<EmissionSourceFeature> sources = scenarioContext.getSituation(situationCode).getOriginalSources();
    final EmissionSourceFeature feature = c.getValue();
    boolean success = false;

    if (EmissionSourceType.SHIPPING_INLAND_DOCKED == feature.getEmissionSourceType()) {
      success = tryRemoveInlandDockingId(feature, sources);
    } else if (EmissionSourceType.SHIPPING_MARITIME_DOCKED == feature.getEmissionSourceType()) {
      success = tryRemoveMaritimeDockingId(feature, sources);
    } else {
      success = true;
      // no-op
    }

    if (success) {
      c.setSituationId(situationCode);
      c.setSilent(!sources.remove(c.getValue()));
    } else {
      c.setSilent(true);
    }
  }

  @EventHandler
  public void onImportFailureEvent(final ImportFailureEvent e) {
    pendingImport = false;
  }

  @EventHandler
  public void onImportCompleteEvent(final ImportCompleteEvent e) {
    pendingImport = false;

    if (!scenarioContext.hasActiveSituation()) {
      eventBus.fireEvent(new EmissionSourceCreateFirstCommand());
    }

    eventBus.fireEvent(new EmissionSourceListChangeEvent(scenarioContext.getActiveSituation().getSources(),
        scenarioContext.getActiveSituationCode()));
    eventBus.fireEvent(new BuildingListChangeEvent(scenarioContext.getActiveSituation().getBuildings(),
        scenarioContext.getActiveSituationCode()));

    if (placeController.getPlace() instanceof ThemeLaunchPlace) {
      placeController.goTo(new EmissionSourceListPlace(applicationContext.getActiveTheme()));
    }
    NotificationUtil.broadcastMessage(eventBus, M.messages().scenarioImportComplete(e.getValue()));
  }

  @EventHandler
  public void onRestoreScenarioContextCommand(final RestoreScenarioContextCommand c) {
    service.importScenario(c.getValue(), propertyMap -> {
      scenarioContext.setScenarioMetaData(Js.cast(propertyMap.get("metaData")));

      final ImportParcelSituation[] situations = Js.cast(propertyMap.get("situations"));
      for (final ImportParcelSituation situation : situations) {
        if (isRelevantForWnbExport(situation)) {
          scenarioContext.addSituation(0, converter.createSituationFromParcel(situation));
        }
      }

      final CalculationPointFeature[] calculationPoints = converter.getCalculationPoints(propertyMap.get("customPoints"));
      if (calculationPoints.length > 0) {
        eventBus.fireEvent(new AddCalculationPointsCommand(Arrays.asList(calculationPoints)));
      }

      eventBus.fireEvent(new RestoreScenarioContextCompletedEvent());
    });
  }

  @EventHandler
  public void onDeleteAllCalculationPointsCommand(final DeleteAllCalculationPointsCommand c) {
    scenarioContext.getCalculationPoints().clear();
  }

  @EventHandler
  public void onAddCalculationPointsCommand(final AddCalculationPointsCommand c) {
    c.silence();
    final List<CalculationPointFeature> addedCalculationPoints = scenarioContext.addCalculationPointsIgnoreOverlapping(c.getValue());
    if (addedCalculationPoints.size() < c.getValue().size()) {
      NotificationUtil.broadcastMessage(eventBus, M.messages().importCalculationPointDuplicatesMessage());
    }
    eventBus.fireEvent(new CalculationPointsAddedEvent(addedCalculationPoints));
  }

  @EventHandler
  public void onAddCalculationPointCommand(final AddCalculationPointCommand c) {
    CalculationPointUtil.addAndSetId(scenarioContext.getCalculationPoints(), c.getValue());
  }

  private boolean isRelevantForWnbExport(final ImportParcelSituation situation) {
    if (situation.getType() == SituationType.PROPOSED && !situation.getId().equals(printWnbContext.getProposedSituationId())) {
      return false;
    }
    return PrintWnbContext.PROJECT_CALCULATION_SITUATION_TYPES.contains(situation.getType());
  }

  private static boolean tryRemoveInlandDockingId(final EmissionSourceFeature dockingFeature, final List<EmissionSourceFeature> sources) {
    final String dockingId = dockingFeature.getGmlId();

    // List of labels of inlandShipping routing sources that have a dock connected to the given dockingFeature
    final List<String> inlandShippingSources = sources.stream()
        .filter(feature -> feature.getEmissionSourceType() == EmissionSourceType.SHIPPING_INLAND)
        .map(shipping -> (InlandShippingESFeature) shipping)
        .filter(shipping -> dockingId.equals(shipping.getMooringAId()) || dockingId.equals(shipping.getMooringBId()))
        .map(InlandShippingESFeature::getLabel)
        .collect(Collectors.toList());

    final String deleteConfirm = M.messages().esShipRoutingDeleteConfirm(String.join(", ", inlandShippingSources));
    if (!inlandShippingSources.isEmpty() && !Window.confirm(deleteConfirm)) {
      return false;
    }

    removeInlandMooringID(dockingId, sources);
    return true;
  }

  private static void removeInlandMooringID(final String dockingId, final List<EmissionSourceFeature> sources) {
    for (final EmissionSourceFeature feature : sources) {
      if (EmissionSourceType.SHIPPING_INLAND == feature.getEmissionSourceType()) {
        final InlandShippingESFeature shipping = (InlandShippingESFeature) feature;
        if (dockingId.equals(shipping.getMooringAId())) {
          shipping.setMooringAId(null);
        } else if (dockingId.equals(shipping.getMooringBId())) {
          shipping.setMooringBId(null);
        } else {
          // no-op
        }
      }
    }
  }

  private static boolean tryRemoveMaritimeDockingId(final EmissionSourceFeature dockingFeature, final List<EmissionSourceFeature> sources) {
    final String dockingId = dockingFeature.getGmlId();

    // List of labels of maritimeShipping routing sources that have a dock connected to the given dockingFeature
    final List<String> maritimeShippingSources = sources.stream()
        .filter(feature -> feature.getEmissionSourceType() == EmissionSourceType.SHIPPING_MARITIME_INLAND
            || feature.getEmissionSourceType() == EmissionSourceType.SHIPPING_MARITIME_MARITIME)
        .map(shipping -> (MaritimeShippingESFeature) shipping)
        .filter(shipping -> dockingId.equals(shipping.getMooringAId()) || dockingId.equals(shipping.getMooringBId()))
        .map(MaritimeShippingESFeature::getLabel)
        .collect(Collectors.toList());

    final String deleteConfirm = M.messages().esShipRoutingDeleteConfirm(String.join(", ", maritimeShippingSources));
    if (!maritimeShippingSources.isEmpty() && !Window.confirm(deleteConfirm)) {
      return false;
    }

    removeMaritimeMooringID(dockingId, sources);
    return true;
  }

  private static void removeMaritimeMooringID(final String dockingId, final List<EmissionSourceFeature> sources) {
    for (final EmissionSourceFeature feature : sources) {
      if (EmissionSourceType.SHIPPING_MARITIME_INLAND == feature.getEmissionSourceType()
          || EmissionSourceType.SHIPPING_MARITIME_MARITIME == feature.getEmissionSourceType()) {
        final MaritimeShippingESFeature shipping = (MaritimeShippingESFeature) feature;
        if (dockingId.equals(shipping.getMooringAId())) {
          shipping.setMooringAId(null);
        } else if (dockingId.equals(shipping.getMooringBId())) {
          shipping.setMooringBId(null);
        } else {
          // no-op
        }
      }
    }
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
