/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.context.dummy;

import nl.overheid.aerius.wui.application.domain.importer.ScenarioMetaData;

public class DummyScenarioMetaData {

  public static void dummy(ScenarioMetaData scenarioMetaData) {
    scenarioMetaData.setReference("RwZxdy5FXg48");
    scenarioMetaData.setCorporation("Boerderij ABC");
    scenarioMetaData.setProjectName("Plan Verbreezand (uitbreiding 4 stallen)");
    scenarioMetaData.setDescription("Vier extra stallen voor ruim 100 stuks rundvee, 50 struisvogels en 38.000 opfokhennen en -hanen");
    scenarioMetaData.setStreetAddress("Boerderijweg 34");
    scenarioMetaData.setPostcode("1130 BA");
    scenarioMetaData.setCity("Westerveen");
  }
}
