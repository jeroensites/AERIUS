/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.geo.layers.calculationpoint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Collection;
import ol.Feature;
import ol.OLFactory;
import ol.geom.Point;
import ol.layer.Layer;
import ol.layer.VectorLayerOptions;
import ol.source.Cluster;
import ol.source.ClusterOptions;
import ol.source.Vector;
import ol.source.VectorOptions;
import ol.style.Icon;
import ol.style.IconOptions;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.overheid.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.geo.icon.DynamicSvgIcon;
import nl.overheid.aerius.wui.application.ApplicationLimits;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;
import nl.overheid.aerius.wui.application.event.DeleteAllCalculationPointsEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointAddedEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointChangeEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.CalculationPointsAddedEvent;
import nl.overheid.aerius.wui.application.event.calculationpoint.RemoveCalculationPointEvent;
import nl.overheid.aerius.wui.application.geo.icons.LabelSvgIconBuilder;
import nl.overheid.aerius.wui.application.geo.layers.LabelFeature;
import nl.overheid.aerius.wui.application.geo.layers.LabelStyle;
import nl.overheid.aerius.wui.application.geo.layers.OptimizedCollection;
import nl.overheid.aerius.wui.application.i18n.ApplicationMessages;
import nl.overheid.aerius.wui.application.i18n.M;

public class CalculationPointMarkerLayer implements IsLayer<Layer> {
  private static final ApplicationMessages i18n = M.messages();
  private static final CalculationPointMarkerLayerEventBinder EVENT_BINDER = GWT.create(CalculationPointMarkerLayerEventBinder.class);
  private static final String MARKER_TEXT_COLOR = "black";

  interface CalculationPointMarkerLayerEventBinder extends EventBinder<CalculationPointMarkerLayer> {
  }

  // Minimum number of pixels between source labels
  private static final int CLUSTER_MINIMUM_PIXEL_DISTANCE = 60;

  // Resolution below which to switch to labeling individual sources
  private static final double CLUSTER_MINIMUM_RESOLUTION = 1.5;

  private static String selectedFeatureId = null;

  private static final LabelSvgIconBuilder LABEL_SVG_ICON_BUILDER = new LabelSvgIconBuilder();

  private final LayerInfo info;
  private final ol.layer.Vector layer;

  private final Map<String, LabelFeature> labelFeaturesById = new HashMap<>();

  private boolean showNameLabels = false;
  private Collection<Feature> olFeatures = new OptimizedCollection<>(new CalculationPointFeature[0]);

  private final MapConfigurationContext mapConfigurationContext;

  @Inject
  public CalculationPointMarkerLayer(@Assisted final int zIndex, final EventBus eventBus,
      final MapConfigurationContext mapConfigurationContext) {

    this.info = new LayerInfo();
    info.setName(CalculationPointMarkerLayer.class.getName());
    info.setTitle(i18n.calculationPointsLayerTitle());
    this.mapConfigurationContext = mapConfigurationContext;

    final VectorLayerOptions vectorLayerOptions = OLFactory.createOptions();
    vectorLayerOptions.setStyle((feature, v) -> getStyleFunction(feature, v));

    layer = new ol.layer.Vector(vectorLayerOptions);
    layer.setZIndex(zIndex);
    layer.setVisible(true);
    EVENT_BINDER.bindEventHandlers(this, eventBus);

    init(Collections.emptyList());
  }

  @EventHandler
  public void onDeleteAllCalculationPointsEvent(final DeleteAllCalculationPointsEvent e) {
    olFeatures.clear();
  }

  @EventHandler
  public void onCalculationPointsAddedEvent(final CalculationPointsAddedEvent e) {
    e.getValue().forEach(cp -> addLabelFeature(cp));
  }

  @EventHandler
  public void onCalculationPointAddedEvent(final CalculationPointAddedEvent e) {
    addLabelFeature(e.getValue());
  }

  @EventHandler
  public void onCalculationPointChangeEvent(final CalculationPointChangeEvent e) {
    if (olFeatures != null) {
      removeLabelFeature(e.getValue());
      addLabelFeature(e.getValue());
    }
  }

  @EventHandler
  public void onRemoveCalculationPointCommand(final RemoveCalculationPointEvent e) {
    removeLabelFeature(e.getValue());
  }

  private void addLabelFeature(final CalculationPointFeature cp) {
    final LabelFeature labelFeature = getOrCreateLabelFeature(cp);
    if (labelFeature != null) {
      olFeatures.push(labelFeature);
    }
  }

  private void removeLabelFeature(final CalculationPointFeature source) {
    final LabelFeature labelFeature = labelFeaturesById.remove(source.getId());
    if (labelFeature != null) {
      olFeatures.remove(labelFeature);
    }
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  private void init(final List<CalculationPointFeature> features) {
    // Clean the old source if any
    final Vector oldVectorSource = layer.getSource();
    if (oldVectorSource != null) {
      oldVectorSource.clear(true);
    }

    // Clustering is quite efficient, so if sources are clustered there is no real
    // reason to apply a limit, however when sources are unclustered there's a
    // noticeable slowdown starting from a couple thousand features.
    final LabelFeature[] array = features.stream()
        .map(source -> getOrCreateLabelFeature(source))
        .limit(ApplicationLimits.SOURCE_LAYER_DRAW_LIMIT)
        .toArray(len -> new LabelFeature[len]);

    olFeatures = new OptimizedCollection<>(array);

    final VectorOptions vectorSourceOptions = OLFactory.createOptions();
    vectorSourceOptions.setFeatures(olFeatures);
    final Vector vectorSource = new Vector(vectorSourceOptions);

    final ClusterOptions clusterOptions = new ClusterOptions();
    clusterOptions.setDistance(CLUSTER_MINIMUM_PIXEL_DISTANCE);
    clusterOptions.setSource(vectorSource);
    clusterOptions.setGeometryFunction(feature -> {
      if (feature.getId().equals(selectedFeatureId)) {
        return null;
      }
      return (Point) feature.getGeometry();
    });

    final Cluster cluster = new Cluster(clusterOptions);
    layer.setSource(cluster);
  }

  private LabelFeature getOrCreateLabelFeature(final CalculationPointFeature cp) {
    if (labelFeaturesById.get(cp.getId()) == null) {
      labelFeaturesById.put(cp.getId(), LabelFeature.create(cp.getId(), cp.getGeometry(), cp.getLabel(), LabelStyle.getLabelColor(cp), cp));
    }
    return labelFeaturesById.get(cp.getId());
  }

  private Style[] getStyleFunction(final Feature feature, final double resolution) {
    final LabelFeature[] features = feature.get("features");
    if (features.length == 0) {
      // This method can be called without any sources. So we just return null as
      // there is nothing to show.
      return null;
    }
    final boolean isClustered = features.length > 1 && resolution > CLUSTER_MINIMUM_RESOLUTION;

    final List<LabelSvgIconBuilder.Label> markers = new ArrayList<>();
    if (isClustered) {

      final LabelSvgIconBuilder.Label marker = new LabelSvgIconBuilder.Label(
          M.messages().calculationPointMarkerClusterLabel(features.length),
          LabelStyle.getLabelColor(features), MARKER_TEXT_COLOR);
      markers.add(marker);

    } else {
      for (final LabelFeature f : features) {
        final String label = f.getId().equals(selectedFeatureId) ? f.getId() : showNameLabels ? f.getLabel() : f.getId();
        final String correctedLabel = label.startsWith("-") ? label.substring(1) : label;

        final LabelSvgIconBuilder.Label marker = new LabelSvgIconBuilder.Label(
            correctedLabel, LabelStyle.getLabelColor(f), MARKER_TEXT_COLOR);
        markers.add(marker);
      }
    }

    final DynamicSvgIcon svg = LABEL_SVG_ICON_BUILDER.getSvgIcon(markers, false, LabelSvgIconBuilder.LabelStyle.CALCULATION_POINT);

    final IconOptions iconOptions = new IconOptions();
    iconOptions.setSrc(svg.getSrc());
    iconOptions.setAnchor(svg.getAnchorInPx());
    iconOptions.setAnchorXUnits("pixels");
    iconOptions.setAnchorYUnits("pixels");
    iconOptions.setScale(mapConfigurationContext.getIconScale());
    final Icon icon = new Icon(iconOptions);

    final StyleOptions styleOptions = new StyleOptions();
    styleOptions.setImage(icon);
    final Style style = new Style(styleOptions);

    return new Style[] {style};
  }
}
