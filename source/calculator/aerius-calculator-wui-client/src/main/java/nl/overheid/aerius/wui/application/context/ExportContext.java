/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.context;

import java.util.HashSet;
import java.util.Set;

import com.google.inject.Singleton;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportType;

@Singleton
public class ExportContext {

  private JobType jobType = JobType.CALCULATION;
  private ExportType exportType = ExportType.GML_SOURCES_ONLY;
  @JsProperty private final Set<String> includedResults = new HashSet<>();
  private String email = "";
  private boolean includeScenarioMetaData;

  public JobType getJobType() {
    return jobType;
  }

  public void setJobType(final JobType jobType) {
    this.jobType = jobType;
  }

  public ExportType getExportType() {
    return exportType;
  }

  public void setExportType(final ExportType exportType) {
    this.exportType = exportType;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public Set<String> getIncludingResults() {
    return includedResults;
  }

  public boolean isIncludeScenarioMetaData() {
    return jobType == JobType.REPORT || includeScenarioMetaData;
  }

  public void setIncludeScenarioMetaData(final boolean includeScenarioMetaData) {
    this.includeScenarioMetaData = includeScenarioMetaData;
  }
}
