/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.ui.wnb.pages.launch;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;

import nl.aerius.wui.vue.activity.VueView;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.wui.application.command.UserImportRequestedCommand;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceCreateFirstCommand;
import nl.overheid.aerius.wui.application.components.button.ButtonIcon;
import nl.overheid.aerius.wui.application.components.importer.ApplicationImportModalComponent;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.vue.BasicVueView;
import nl.overheid.aerius.wui.vue.DebugDirective;

@Component(components = {
    ApplicationImportModalComponent.class,
    ButtonIcon.class,
}, directives = {
    DebugDirective.class
})
public class WnbThemeLaunchView extends BasicVueView implements VueView<WnbThemeLaunchActivity> {
  @Prop EventBus eventBus;

  @Inject ApplicationContext applicationContext;

  @JsMethod
  public void onCreateSourceClick() {
    eventBus.fireEvent(new EmissionSourceCreateFirstCommand());
  }

  @JsMethod
  public void onImportFileClick() {
    eventBus.fireEvent(new UserImportRequestedCommand());
  }

  @Computed
  public String getQuickStartUrl() {
    return applicationContext.getConfiguration().getSettings().getSetting(SharedConstantsEnum.QUICK_START_URL);
  }
}
