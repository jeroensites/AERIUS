/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.v2.source.road.RoadSideBarrierType;
import nl.overheid.aerius.srm2.conversion.Sigma0Calculator;

/**
 * Client side implementation of ReductiveLodgingSystem props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class RoadSideBarrier {

  /**
   * Type of the barrier.
   */
  private String barrierType;
  /**
   * Height of the barrier.
   */
  private double height;
  /**
   * Distance from road to barrier.
   */
  private double distance;

  public static final @JsOverlay RoadSideBarrier create() {
    final RoadSideBarrier props = new RoadSideBarrier();
    props.setBarrierType(null);
    props.setHeight(Sigma0Calculator.MIN_BARRIER_HEIGHT);
    props.setDistance(0.0);
    return props;
  }

  public final @JsOverlay RoadSideBarrierType getBarrierType() {
    return barrierType.isEmpty() ? null : RoadSideBarrierType.valueOf(barrierType);
  }

  public final @JsOverlay void setBarrierType(final RoadSideBarrierType barrierType) {
    this.barrierType = barrierType == null ? "" : barrierType.name();
  }

  public final @JsOverlay double getHeight() {
    return height;
  }

  public final @JsOverlay void setHeight(final double height) {
    this.height = height;
  }

  public final @JsOverlay double getDistance() {
    return distance;
  }

  public final @JsOverlay void setDistance(final double distance) {
    this.distance = distance;
  }

}
