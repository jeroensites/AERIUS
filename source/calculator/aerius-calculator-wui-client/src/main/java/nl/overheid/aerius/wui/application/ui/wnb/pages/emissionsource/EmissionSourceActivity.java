/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.ui.wnb.pages.emissionsource;

import com.axellience.vuegwt.core.client.Vue;
import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditCancelEvent;
import nl.overheid.aerius.wui.application.command.source.EmissionSourceEditSaveEvent;
import nl.overheid.aerius.wui.application.components.map.MapComponentFactory;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.context.EmissionSourceEditorContext;
import nl.overheid.aerius.wui.application.place.EmissionSourceListPlace;
import nl.overheid.aerius.wui.application.ui.main.MainView.FlexView;
import nl.overheid.aerius.wui.application.ui.pages.ThemeDelegatedActivity;
import nl.overheid.aerius.wui.application.ui.pages.ThemeView;

/**
 * Activity for editing a single emission source object.
 */
public class EmissionSourceActivity extends BasicEventComponent implements ThemeDelegatedActivity, EmissionSourcePresenter {
  private static final EmissionSourceActivityEventBinder EVENT_BINDER = GWT.create(EmissionSourceActivityEventBinder.class);

  interface EmissionSourceActivityEventBinder extends EventBinder<EmissionSourceActivity> {}

  @Inject PlaceController placeController;
  @Inject ApplicationContext applicationContext;
  @Inject EmissionSourceEditorContext editorContext;

  private boolean userExit;
  private HandlerRegistration handlers;

  @Override
  public void onStart(final ThemeView view) {
    if (editorContext.isEditing()) {
      view.setDelegatedPresenter(this);
      view.setLeftView(EmissionSourceViewFactory.get(), FlexView.RIGHT);

      view.setMiddleView(EmissionSourceDetailViewFactory.get());
      view.setRightView(MapComponentFactory.get());
      view.attachMap(() -> view.rightComponent);
    } else {
      Vue.nextTick(() -> placeController.goTo(new EmissionSourceListPlace(applicationContext.getActiveTheme())));
    }
  }

  @Override
  public void onStop() {
    handlers.removeHandler();
    if (userExit) {
      return;
    }
  }

  @EventHandler(handles = { EmissionSourceEditSaveEvent.class, EmissionSourceEditCancelEvent.class })
  public void onEmissionSourceEditSaveEvent(final GenericEvent e) {
    userExit = true;
    placeController.goTo(new EmissionSourceListPlace(applicationContext.getActiveTheme()));
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    handlers = super.setEventBus(eventBus, this, EVENT_BINDER);
  }

}
