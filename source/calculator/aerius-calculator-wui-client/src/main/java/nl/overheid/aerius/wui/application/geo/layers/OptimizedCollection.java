/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers;

import ol.Collection;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

/**
 * Identical to ol.Collection, except it also supports initializing an entire
 * array via the constructor, which is more optimal.
 *
 * The normal Collection only supports .forEach(item -> col.push(item)) type of constructs,
 * which performs like a turd.
 *
 * https://github.com/openlayers/openlayers/blob/v6.9.0/src/ol/Collection.js#L110-L118
 */
@JsType(isNative = true, name = "ol.Collection", namespace = JsPackage.GLOBAL)
public class OptimizedCollection<T> extends Collection<T> {
  public OptimizedCollection(final T[] arr) {}
}
