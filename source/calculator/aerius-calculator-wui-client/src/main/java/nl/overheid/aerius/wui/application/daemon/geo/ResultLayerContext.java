/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.geo;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import nl.overheid.aerius.geo.domain.IsLayer;

@Singleton
public class ResultLayerContext {
  private final List<IsLayer<?>> resultLayers = new ArrayList<>();

  private IsLayer<?> resultLayer;
  private IsLayer<?> markersLayer;
  private IsLayer<?> habitatLayer;

  public void setResultLayer(final IsLayer<?> resultLayer) {
    this.resultLayer = resultLayer;
    addResultLayer(resultLayer);
  }

  public void setMarkersLayer(final IsLayer<?> markersLayer) {
    this.markersLayer = markersLayer;
    addResultLayer(markersLayer);
  }

  public void setHabitatLayer(final IsLayer<?> habitatLayer) {
    this.habitatLayer = habitatLayer;
    addResultLayer(habitatLayer);
  }

  public void clearResultLayers() {
    resultLayers.clear();
  }

  public void addResultLayer(final IsLayer<?> layer) {
    resultLayers.add(layer);
  }

  public IsLayer<?> getResultLayer() {
    return resultLayer;
  }

  public IsLayer<?> getMarkersLayer() {
    return markersLayer;
  }

  public IsLayer<?> getHabitatLayer() {
    return habitatLayer;
  }

  public List<IsLayer<?>> getResultLayers() {
    return new ArrayList<>(resultLayers);
  }
}
