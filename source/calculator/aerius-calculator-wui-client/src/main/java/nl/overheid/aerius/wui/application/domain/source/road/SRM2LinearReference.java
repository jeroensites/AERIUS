/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.domain.source.road;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.shared.domain.v2.source.road.RoadElevation;
import nl.overheid.aerius.wui.application.domain.source.base.LinearReference;

/**
 * Client side implementation of SRM2LinearReference props.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class SRM2LinearReference extends LinearReference {

  private Double tunnelFactor;
  private Boolean freeway;
  private String elevation;
  private int elevationHeight;
  private RoadSideBarrier barrierLeft;
  private RoadSideBarrier barrierRight;

  public static final @JsOverlay SRM2LinearReference create() {
    final SRM2LinearReference props = new SRM2LinearReference();
    props.setFromPosition(0.0);
    props.setToPosition(0.0);
    props.setTunnelFactor(1.0);
    props.setFreeway(false);
    props.setElevation(RoadElevation.NORMAL);
    props.setElevationHeight(0);
    props.setBarrierLeft(RoadSideBarrier.create());
    props.setBarrierRight(RoadSideBarrier.create());
    return props;
  }


  public final @JsOverlay Double getTunnelFactor() {
    return tunnelFactor;
  }

  public final @JsOverlay void setTunnelFactor(final Double tunnelFactor) {
    this.tunnelFactor = tunnelFactor;
  }

  public final @JsOverlay Boolean getFreeway() {
    return freeway;
  }

  public final @JsOverlay void setFreeway(final Boolean freeway) {
    this.freeway = freeway;
  }

  public final @JsOverlay RoadElevation getElevation() {
    return RoadElevation.valueOf(elevation);
  }

  public final @JsOverlay void setElevation(final RoadElevation elevation) {
    this.elevation = elevation.name();
  }

  public final @JsOverlay int getElevationHeight() {
    return elevationHeight;
  }

  public final @JsOverlay void setElevationHeight(final int elevationHeight) {
    this.elevationHeight = elevationHeight;
  }

  public final @JsOverlay RoadSideBarrier getBarrierLeft() {
    return barrierLeft;
  }

  public final @JsOverlay void setBarrierLeft(final RoadSideBarrier barrierLeft) {
    this.barrierLeft = barrierLeft;
  }

  public final @JsOverlay RoadSideBarrier getBarrierRight() {
    return barrierRight;
  }

  public final @JsOverlay void setBarrierRight(final RoadSideBarrier barrierRight) {
    this.barrierRight = barrierRight;
  }

}
