/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.util;

import java.util.stream.Stream;

import ol.Coordinate;
import ol.Extent;
import ol.geom.Circle;
import ol.geom.Geometry;
import ol.geom.LineString;
import ol.geom.Polygon;

import nl.overheid.aerius.geo.command.MapCenterChangeCommand;
import nl.overheid.aerius.geo.command.MapSetExtentCommand;
import nl.overheid.aerius.geo.shared.EPSG;
import nl.overheid.aerius.wui.application.domain.info.BBox;
import nl.overheid.aerius.wui.application.geo.util.hull.ConvexHullGrahamScan;
import nl.overheid.aerius.wui.application.geo.util.hull.Point;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.i18n.MessageFormatter;

/**
 * Utility class for geometric methods.
 */
public final class GeoUtil {

  private static final int SANELY_ROUNDING_DECIMALS = 3;

  private GeoUtil() {}

  public static OrientedEnvelope findSmallestSurroundingRectangleFlat(final Geometry geometry) {
    final Geometry clone = geometry.clone();
    final Polygon polygon = (Polygon) clone;

    final ConvexHullGrahamScan hullBuilder = new ConvexHullGrahamScan();

    final Coordinate[][] coordinates = polygon.getCoordinates();
    Stream.of(coordinates[0]).forEach(coordinate -> hullBuilder.addPoint(coordinate.getX(), coordinate.getY()));

    final Point[] hull = hullBuilder.getHull();
    final Coordinate[] olCoords = new Coordinate[hull.length];
    for (int i = 0; i < hull.length; i++) {
      olCoords[i] = new Coordinate(hull[i].x, hull[i].y);
    }
    final Polygon hullPolygon = new Polygon(new Coordinate[][] { olCoords });

    final Coordinate centroid = hullPolygon.getInteriorPoint().getCoordinates();
    final Coordinate[][] hullCoordinates = hullPolygon.getCoordinates();

    double minAngle = 0.0;
    double minArea = Double.MAX_VALUE;
    Extent smallestSurroundingRectangle = null;
    Coordinate nextCoordinate = null;
    Coordinate currentCoordinate = null;

    for (int i = 0; i < hullCoordinates[0].length - 1; i++) {
      currentCoordinate = hullCoordinates[0][i];
      nextCoordinate = hullCoordinates[0][i + 1];
      final double angle = -Math.atan2(nextCoordinate.getY() - currentCoordinate.getY(), nextCoordinate.getX() - currentCoordinate.getX());
      final Polygon polygonClone = (Polygon) hullPolygon.clone();
      polygonClone.rotate(angle, centroid);
      final Extent rect = polygonClone.getExtent();
      final double area = rect.getWidth() * rect.getHeight();
      if (area < minArea) {
        minArea = area;
        smallestSurroundingRectangle = rect;
        minAngle = angle;
      }
    }

    if (smallestSurroundingRectangle == null) {
      throw new RuntimeException("Not able to determine smallest surrounding rectangle for geometry: " + geometry);
    }

    final double width = smallestSurroundingRectangle.getWidth();
    final double length = smallestSurroundingRectangle.getHeight();

    return createOrientedEnvelope(Math.toDegrees(minAngle), width, length);
  }

  /**
   * @param orientation in degrees
   */
  private static OrientedEnvelope createOrientedEnvelope(final double orientation, final double width, final double length) {
    double roundedLength;
    double roundedWidth;
    double roundedOrientation;

    // Height of envelope = difference between the maximum and minimum y values
    // Width of envelope = difference between the maximum and minimum x values
    // Our definition of length = max of those 2 (and width = min of those 2)
    // If we have to switch, be sure to update angle accordingly (rotate another 90
    // degrees)
    if (length < width) {
      final double tempLength = length;
      roundedLength = sanelyRounded(width);
      roundedWidth = sanelyRounded(tempLength);
      roundedOrientation = sanelyOrientation(orientation + 90);
    } else {
      roundedLength = sanelyRounded(length);
      roundedWidth = sanelyRounded(width);
      roundedOrientation = sanelyOrientation(orientation);
    }

    final double finalOrientation = roundedOrientation;

    return new OrientedEnvelope(finalOrientation, roundedWidth, roundedLength, true);
  }

  private static double sanelyRounded(final double value) {
    final double precision = Math.pow(10, SANELY_ROUNDING_DECIMALS);
    return (int) (value * precision) / precision;
  }

  /**
   * Make sure orientation is within 0 and 180 degree.
   */
  private static double sanelyOrientation(final double orientation) {
    return sanelyRounded((180 + orientation) % 180);
  }

  public static OrientedEnvelope findCircleEnvelope(final Geometry geometry) {
    final Circle circle = (Circle) geometry.clone();
    final double diameter = circle.getRadius() * 2;

    return new OrientedEnvelope(0, diameter, diameter, false);
  }

  /**
   * Creates a new {@link MapCenterChangeCommand} from a geometry and using the
   * default max zoomlevel.
   *
   * @param geometry
   * @param espg
   * @return new command.
   */
  public static MapCenterChangeCommand createMapCenterZoomCommand(final Geometry geometry, final EPSG espg) {
    final Extent extent = geometry.getExtent();

    return new MapCenterChangeCommand(extent.getLowerLeftX() + extent.getWidth() / 2, extent.getLowerLeftY() + extent.getHeight() / 2,
        espg.getZoomLevel() - 1);
  }

  /**
   * Creates a new {@link MapSetExtentCommand} from geometry.
   *
   * @param geometry
   * @return new command.
   */
  public static MapSetExtentCommand createMapSetExtendCommand(final Geometry geometry) {
    final Extent extent = geometry.getExtent();
    final String wkt = "POLYGON(("
        + extent.getLowerLeftX() + " " + extent.getLowerLeftY() + ","
        + extent.getLowerLeftX() + " " + extent.getUpperRightY() + ","
        + extent.getUpperRightX() + " " + extent.getUpperRightY() + ","
        + extent.getUpperRightX() + " " + extent.getLowerLeftY() + ","
        + extent.getLowerLeftX() + " " + extent.getLowerLeftY()
        + "))";
    return new MapSetExtentCommand(wkt);
  }

  /**
   * Creates a new {@link MapSetExtentCommand} from a boundingbox.
   *
   * @param boundingBox
   * @return new command.
   */
  public static MapSetExtentCommand createMapSetExtendCommand(final BBox boundingBox) {
    final String wkt = "POLYGON(("
        + boundingBox.getMinX() + " " + boundingBox.getMinY() + ","
        + boundingBox.getMinX() + " " + boundingBox.getMaxY() + ","
        + boundingBox.getMaxX() + " " + boundingBox.getMaxY() + ","
        + boundingBox.getMaxX() + " " + boundingBox.getMinY() + ","
        + boundingBox.getMinX() + " " + boundingBox.getMinY()
        + "))";
    return new MapSetExtentCommand(wkt);
  }

  public static OrientedEnvelope determineOrientedEnvelope(final Geometry geometry) {
    if (GeoType.CIRCLE.is(geometry)) {
      return GeoUtil.findCircleEnvelope(geometry);
    } else if (GeoType.POLYGON.is(geometry)) {
      return GeoUtil.findSmallestSurroundingRectangleFlat(geometry);
    } else {
      return null;
    }
  }

  /**
   * Returns a specific statistic for a Geometry if available E.g. for lines the
   * length, and for polygons the area. Returns null when no statistic is available
   */
  public static String getGeometryStatistic(final Geometry geometry) {
    if (geometry == null) {
      return null;
    }

    if (GeoType.LINE_STRING.is(geometry)) {
      return M.messages().esLocationLineStringLength(MessageFormatter.formatDistanceWithUnit(((LineString) geometry).getLength()));
    } else if (GeoType.POLYGON.is(geometry)) {
      return M.messages().esLocationPolygonArea(MessageFormatter.formatSurfaceToHectareWithUnit(((Polygon) geometry).getArea()));
    } else {
      return null;
    }
  }
}
