/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.components.nav;

import java.util.Objects;

public class ButtonItem {

  private final String icon;
  private final Runnable action;

  public ButtonItem(final String icon, final Runnable action) {
    this.icon = icon;
    this.action = action;
  }

  public String getIcon() {
    return icon;
  }

  public Runnable getAction() {
    return action;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final ButtonItem that = (ButtonItem) o;
    return Objects.equals(icon, that.icon) && Objects.equals(action, that.action);
  }

  @Override
  public int hashCode() {
    return Objects.hash(icon, action);
  }

  @Override
  public String toString() {
    return "ButtonItem{" +
        "icon=" + icon +
        ", action=" + action +
        '}';
  }
}
