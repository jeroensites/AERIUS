/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.building;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import ol.Feature;
import ol.OLFactory;
import ol.geom.Point;
import ol.layer.Layer;
import ol.layer.VectorLayerOptions;
import ol.source.Vector;
import ol.style.Style;

import nl.overheid.aerius.geo.domain.IsLayer;
import nl.overheid.aerius.geo.domain.LayerInfo;
import nl.overheid.aerius.wui.application.command.building.BuildingModifyGeometryCommand;
import nl.overheid.aerius.wui.application.command.building.BuildingSelectFeatureCommand;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingAddCommand;
import nl.overheid.aerius.wui.application.command.building.TemporaryBuildingClearCommand;
import nl.overheid.aerius.wui.application.context.BuildingEditorContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.domain.source.BuildingFeature;
import nl.overheid.aerius.wui.application.event.building.BuildingAddedEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingDeletedEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingListChangeEvent;
import nl.overheid.aerius.wui.application.event.building.BuildingUpdatedEvent;
import nl.overheid.aerius.wui.application.event.situation.SituationSwitchEvent;

public abstract class BuildingGeometryLayer implements IsLayer<Layer> {
  private static final BuildingGeometryLayerEventBinder EVENT_BINDER = GWT.create(BuildingGeometryLayerEventBinder.class);

  interface BuildingGeometryLayerEventBinder extends EventBinder<BuildingGeometryLayer> {}

  protected static final Style[] NO_RENDERING = new Style[] {};

  private static final double GEOMETRY_MAX_RESOLUTION = 100.0;

  private final LayerInfo info;
  private final ol.layer.Vector layer;
  private final Map<String, Vector> buildings = new HashMap<>();

  @Inject private ScenarioContext scenarioContext;
  @Inject private BuildingEditorContext editorContext;
  protected String editableFeatureId = null;

  public BuildingGeometryLayer(final LayerInfo info, final EventBus eventBus, final int zIndex) {
    this.info = info;

    final VectorLayerOptions vectorLayerOptions = OLFactory.createOptions();
    vectorLayerOptions.setMaxResolution(GEOMETRY_MAX_RESOLUTION);
    vectorLayerOptions.setStyle((feature, v) -> getStyle(feature, v));

    layer = new ol.layer.Vector(vectorLayerOptions);
    layer.setZIndex(zIndex);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  protected abstract Style[] getStyle(Feature feat, double resolution);

  @EventHandler
  public void onTemporaryBuildingAddCommand(final TemporaryBuildingAddCommand c) {
    final Vector vector = getFeaturesVector(c.getSituationId());

    addFeature(vector, c.getValue());
  }

  @EventHandler
  public void onTemporaryBuildingClearCommand(final TemporaryBuildingClearCommand c) {
    final Vector vector = getFeaturesVector(c.getSituationId());
    final Feature feature = vector.getFeatureById(c.getValue().getId());

    if (feature != null) {
      vector.removeFeature(feature);
    }
  }

  @EventHandler
  public void onBuildingUpdatedEvent(final BuildingUpdatedEvent e) {
    final Vector vector = getFeaturesVector(e.getSituationId());
    final Feature feature = vector.getFeatureById(e.getValue().getId());

    if (feature != null) {
      vector.removeFeature(feature);
    }
    addFeature(vector, e.getValue());
  }

  @EventHandler
  public void onBuildingAddedEvent(final BuildingAddedEvent e) {
    final Vector vector = getFeaturesVector(e.getSituationId());

    addFeature(vector, e.getValue());
  }

  @EventHandler
  public void onBuildingDeletedEvent(final BuildingDeletedEvent e) {
    final Vector vector = getFeaturesVector(e.getSituationId());
    final Feature feature = vector.getFeatureById(e.getValue().getId());

    if (feature != null) {
      vector.removeFeature(feature);
    }
  }

  @EventHandler
  public void onBuildingListChangeEvent(final BuildingListChangeEvent e) {
    final Vector vector = getFeaturesVector(scenarioContext.getActiveSituationCode());

    vector.clear(true);
    scenarioContext.getActiveSituation().getBuildings()
        .forEach(v -> {
          addFeature(vector, v);
        });

    layer.setSource(vector);
  }

  @EventHandler
  public void onSituationSwitch(final SituationSwitchEvent e) {
    final Vector vector = getFeaturesVector(e.getValue());

    layer.setSource(vector);
  }

  @EventHandler
  public void onBuildingModifyGeometryCommand(final BuildingModifyGeometryCommand e) {
    if (e.getValue() == null) {
      editableFeatureId = null;
    } else {
      editableFeatureId = e.getValue().getId();
    }
    layer.getSource().changed();
  }

  @EventHandler(handles = { BuildingSelectFeatureCommand.class })
  public void refreshLayer() {
    layer.getSource().changed();
  }

  private void addFeature(final Vector vector, final BuildingFeature feature) {
    // Don't render point sources
    if (feature.getGeometry() instanceof Point) {
      return;
    }

    vector.addFeature(feature);
  }

  private Vector getFeaturesVector(final String situationCode) {
    Vector vector = buildings.get(situationCode);

    if (vector == null) {
      vector = new Vector();
      buildings.put(situationCode, vector);
    }
    return vector;
  }

  protected String getGeometryEditingFeatureId() {
    return editableFeatureId;
  }

  protected String getEditingFeatureId() {
    if (editorContext.getBuilding() == null) {
      return null;
    } else {
      return editorContext.getBuilding().getId();
    }
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public Optional<LayerInfo> getInfoOptional() {
    return Optional.of(info);
  }

  @Override
  public LayerInfo getInfo() {
    return info;
  }

}
