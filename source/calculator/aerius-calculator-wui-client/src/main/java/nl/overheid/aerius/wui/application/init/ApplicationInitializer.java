/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.init;

import java.util.Arrays;

import com.google.inject.Inject;

import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.future.AppAsyncCallback;
import nl.aerius.wui.init.AbstractInitializer;
import nl.aerius.wui.init.Initializer;
import nl.overheid.aerius.geo.proj4.Proj4Initializer;
import nl.overheid.aerius.geo.wui.MapLayoutPanel;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.service.v2.ContextServiceAsync;
import nl.overheid.aerius.wui.application.context.ApplicationContext;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.util.WKTUtil;
import nl.overheid.aerius.wui.util.ES6Util;

public class ApplicationInitializer extends AbstractInitializer implements Initializer {
  private static final String EPSG_29903 = "EPSG:29903";
  private static final String EPSG_28992 = "EPSG:28992";
  private static final String EPSG_27700 = "EPSG:27700";

  private static final String TM75_PROJ4_DEF = "+proj=tmerc +lat_0=53.5 +lon_0=-8 +k=1.000035 +x_0=200000 +y_0=250000 +ellps=mod_airy +towgs84=482.5,-130.6,564.6,-1.042,-0.214,-0.631,8.15 +units=m +no_defs";
  private static final String BNG_PROJ4_DEF = "+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy +towgs84=446.448,-125.157,542.06,0.15,0.247,0.842,-20.489 +units=m +no_defs";
  private static final String RDNEW_PROJ4_DEF = "+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=155000 +y_0=463000 +ellps=bessel +towgs84=565.417,50.3319,465.552,-0.398957,0.343988,-1.8774,4.0725 +units=m +no_defs";

  private static final int INIT_TARGET = 1;

  private final ContextServiceAsync contextService = ContextServiceAsync.Util.getInstance();

  @Inject ApplicationContext applicationContext;
  @Inject MapLayoutPanel map;
  @Inject UrlPlaceholderResolver urlPlaceholderResolver;

  @Inject Proj4Initializer proj4;

  public ApplicationInitializer() {
    super(INIT_TARGET);
  }

  @Override
  public void init() {
    ES6Util.register("DepositionGraphElement", "/components", "deposition-graph-component");

    // Register the RDNew (dutch), BNG (british), and TM75 (irish grid) defs with OL
    // via Proj4
    // TODO Provide via server
    proj4.defs(EPSG_27700, BNG_PROJ4_DEF);
    proj4.defs(EPSG_28992, RDNEW_PROJ4_DEF);
    proj4.defs(EPSG_29903, TM75_PROJ4_DEF);
    proj4.register();

    contextService.getContext(AppAsyncCallback.create(
        v -> initAppContext(v),
        e -> fail(M.messages().errorCouldNotLoadApplicationConfiguration(), e)));
  }

  private void initAppContext(final ApplicationConfiguration configuration) {
    GWTProd.log("AppContext:" + configuration);
    urlPlaceholderResolver.replaceGeoserverPlaceholders(configuration);
    applicationContext.setConfiguration(configuration);
    map.init(configuration.getReceptorGridSettings().getEpsg());
    WKTUtil.initReference(configuration.getReceptorGridSettings().getEpsg(), Arrays.asList(EPSG_29903, EPSG_28992, EPSG_27700));
    complete();
  }
}
