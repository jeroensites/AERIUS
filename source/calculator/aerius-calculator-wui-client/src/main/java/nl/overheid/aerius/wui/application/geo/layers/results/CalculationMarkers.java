/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.layers.results;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.gwt.resources.client.DataResource;

import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.resources.R;

/**
 * Describes the relation between a set of calculation markers and their
 * combination image.
 */
public final class CalculationMarkers {

  private static final Map<Set<CalculationMarker>, DataResource> combinations = new HashMap<>();
  private static final Map<ScenarioResultType, Map<DataResource, String>> legends = new HashMap<>();

  static {
    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.HIGHEST_MAX
    )), R.images().markerHighestMax());

    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.HIGHEST_MAX_INCREASE
    )), R.images().markerHighestMaxIncrease());

    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.MAX_CUMULATION
    )), R.images().markerMaxCumulation());

    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.MAX_DECREASE
    )), R.images().markerMaxDecrease());

    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.MAX_DECREASE,
        CalculationMarker.MAX_TOTAL_WITH_EFFECT
    )), R.images().markerMaxDecreaseMaxTotalWithEffect());

    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.MAX_INCREASE
    )), R.images().markerMaxIncrease());

    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.MAX_INCREASE,
        CalculationMarker.MAX_TOTAL_WITH_EFFECT
    )), R.images().markerMaxIncreaseMaxTotalWithEffect());

    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.MAX_SITUATION
    )), R.images().markerMaxSituation());

    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.MAX_SITUATION,
        CalculationMarker.MAX_CUMULATION
    )), R.images().markerMaxSituationMaxCumulation());

    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.MAX_TOTAL
    )), R.images().markerMaxTotal());

    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.MAX_TOTAL,
        CalculationMarker.MAX_CUMULATION
    )), R.images().markerMaxTotalMaxCumulation());

    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.MAX_TOTAL,
        CalculationMarker.MAX_SITUATION
    )), R.images().markerMaxTotalMaxSituation());

    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.MAX_TOTAL,
        CalculationMarker.MAX_SITUATION,
        CalculationMarker.MAX_CUMULATION
    )), R.images().markerMaxTotalMaxSituationMaxCumulation());

    combinations.put(new HashSet<>(Arrays.asList(
        CalculationMarker.MAX_TOTAL_WITH_EFFECT
    )), R.images().markerMaxTotalWithEffect());

    final Map<DataResource, String> projectCalculationLegend = new HashMap<>();
    projectCalculationLegend.put(R.images().markerMaxDecrease(), M.messages().markerMaxDecrease());
    projectCalculationLegend.put(R.images().markerMaxIncrease(), M.messages().markerMaxIncrease());
    projectCalculationLegend.put(R.images().markerMaxTotalWithEffect(), M.messages().markerMaxTotalWithEffect());
    legends.put(ScenarioResultType.PROJECT_CALCULATION, projectCalculationLegend);
  }

  private CalculationMarkers() {
  }

  public static DataResource getMarkerImage(final Set<CalculationMarker> markers) {
    return combinations.get(markers);
  }

  public static Map<DataResource, String> getMarkerLegend(final ScenarioResultType scenarioResultType) {
    return legends.get(scenarioResultType);
  }
}
