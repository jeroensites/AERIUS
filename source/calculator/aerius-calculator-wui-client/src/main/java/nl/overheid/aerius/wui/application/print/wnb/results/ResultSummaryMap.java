/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.print.wnb.results;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.DataResource;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.geo.command.LayerHiddenCommand;
import nl.overheid.aerius.geo.command.RenderSyncCommand;
import nl.overheid.aerius.geo.daemon.LayerContext;
import nl.overheid.aerius.geo.domain.legend.Legend;
import nl.overheid.aerius.geo.wui.OL3MapComponent;
import nl.overheid.aerius.geo.wui.layers.OL3Layer;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.wui.application.command.ChangeSituationResultKeyCommand;
import nl.overheid.aerius.wui.application.command.situation.SituationResultsZoomCommand;
import nl.overheid.aerius.wui.application.context.MapConfigurationContext;
import nl.overheid.aerius.wui.application.context.PrintWnbContext;
import nl.overheid.aerius.wui.application.context.ScenarioContext;
import nl.overheid.aerius.wui.application.context.SituationContext;
import nl.overheid.aerius.wui.application.context.calculation.CalculationContext;
import nl.overheid.aerius.wui.application.domain.result.SituationResultsKey;
import nl.overheid.aerius.wui.application.event.RestoreCalculationContextEvent;
import nl.overheid.aerius.wui.application.event.ResultsViewSelectionEvent;
import nl.overheid.aerius.wui.application.event.SituationResultsSelectedEvent;
import nl.overheid.aerius.wui.application.event.SituationResultsZoomEvent;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationMarkers;
import nl.overheid.aerius.wui.application.geo.layers.results.CalculationResultLayer;
import nl.overheid.aerius.wui.application.ui.air.legend.LayerLegendComponent;
import nl.overheid.aerius.wui.vue.BasicVueEventComponent;

@Component(components = {
    OL3MapComponent.class,
    LayerLegendComponent.class
})
public class ResultSummaryMap extends BasicVueEventComponent implements HasMounted, HasCreated {
  private static final String LEGEND_NAME = "calculator:wms_nature_areas_view";
  private static final ResultSummaryMapEventBinder EVENT_BINDER = GWT.create(ResultSummaryMapEventBinder.class);

  interface ResultSummaryMapEventBinder extends EventBinder<ResultSummaryMap> {}

  @Prop EventBus eventBus;

  @Data @JsProperty Map<DataResource, String> markerLegend = new HashMap<>();

  @Inject LayerContext layerContext;
  @Inject MapConfigurationContext mapConfigurationContext;
  @Inject PrintWnbContext printWnbContext;
  @Inject ScenarioContext scenarioContext;
  @Inject CalculationContext calculationContext;

  @Ref OL3MapComponent map;

  private SituationResultsKey proposedSituationResultsKey;
  private boolean zoomCommandExecuted;

  @Override
  public void created() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void mounted() {
    mapConfigurationContext.setIconScale(0.5);
    map.attach();
  }

  @Computed
  public Legend getLegend() {
    return layerContext.getLayerItems().stream()
        .map(layerItem -> layerItem.getLayer())
        .filter(layer -> layer instanceof OL3Layer)
        .map(ol3Layer -> (OL3Layer) ol3Layer)
        .filter(ol3Layer -> ol3Layer.getInfoOptional().isPresent())
        .map(ol3Layer -> ol3Layer.getInfo())
        .filter(layerInfo -> layerInfo.getName().equals(LEGEND_NAME))
        .findFirst()
        .map(layerInfo -> layerInfo.getLegend())
        .orElse(null);
  }

  @EventHandler
  public void onRestoreCalculationContextEvent(final RestoreCalculationContextEvent e) {
    // Select proposed situation results for WNB project calculation PDF
    proposedSituationResultsKey = new SituationResultsKey(
        SituationContext.handleFromSituation(scenarioContext.getActiveSituation()),
        printWnbContext.getScenarioResultType(),
        printWnbContext.getEmissionResultKey(),
        printWnbContext.getSummaryHexagonType());
    eventBus.fireEvent(new ChangeSituationResultKeyCommand(proposedSituationResultsKey));

    eventBus.fireEvent(new ResultsViewSelectionEvent(ResultView.MARKERS));

  }

  @EventHandler
  public void on(final SituationResultsSelectedEvent e) {
    if (e.getValue().equals(proposedSituationResultsKey)) {
      eventBus.fireEvent(new SituationResultsZoomCommand(proposedSituationResultsKey));
      markerLegend = CalculationMarkers.getMarkerLegend(printWnbContext.getScenarioResultType());
    }

    // Hide the calculation results layer every time, because selecting results also changes visibility of layers.
    layerContext.getLayerItems().stream()
        .filter(layerItem -> layerItem.getLayer().getInfoOptional()
            .map(info -> info.getName().equals(CalculationResultLayer.class.getCanonicalName()))
            .orElse(false))
        .forEach(calculationResultsLayerItem -> eventBus.fireEvent(new LayerHiddenCommand(calculationResultsLayerItem.getLayer())));
  }

  @EventHandler
  public void on(final SituationResultsZoomEvent e) {
    if (e.getValue().equals(proposedSituationResultsKey)) {
      zoomCommandExecuted = true;
      eventBus.fireEvent(new RenderSyncCommand(() -> printWnbContext.finish()));
    }
  }
}
