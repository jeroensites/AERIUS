/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.daemon.misc;

import com.google.web.bindery.event.shared.EventBus;

import elemental2.dom.DomGlobal;

import nl.aerius.wui.daemon.Daemon;
import nl.aerius.wui.dev.GWTProd;
import nl.aerius.wui.event.BasicEventComponent;
import nl.aerius.wui.util.NotificationUtil;
import nl.aerius.wui.util.SchedulerUtil;
import nl.overheid.aerius.wui.application.i18n.M;

public class CompatibilityDaemon extends BasicEventComponent implements Daemon {
  /**
   * A small delay before showing the unsupported browser notification
   */
  private static final int NOTIFICATION_DELAY = 500;

  private static final String EDGE = "Edge";
  private static final String EDGE_IOS = "EdgiOS";

  private static final String IOS = "iPhone OS";
  private static final String APPLE_WEB_KIT = "AppleWebKit";

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus);

    if (isUnsupportedBrowser()) {
      SchedulerUtil.delay(() -> NotificationUtil.broadcastWarning(eventBus, M.messages().applicationBrowserUnsupported()), NOTIFICATION_DELAY);
    }
  }

  private boolean isUnsupportedBrowser() {
    final String ua = DomGlobal.navigator.userAgent;
    boolean isUnsupported = false;
    try {
      if (isNonChromiumEdge(ua)
          || isAncientIE(ua)
          || isMobileSafari(ua)) {
        isUnsupported = true;
      }
    } catch (final Exception e) {
      // Because the user agent is a user submitted string, ignore any errors
      GWTProd.warn("Could not parse user agent: " + e.getMessage(), e);
    }

    return isUnsupported;
  }

  private boolean isAncientIE(final String ua) {
    return ua.indexOf("Trident") > -1 // Versions 8-11
        || ua.indexOf("MSIE") > -1; // Versions 6-10
  }

  private boolean isNonChromiumEdge(final String ua) {
    // Non-Chromium Edge
    return (ua.indexOf(EDGE) > -1 || ua.indexOf(EDGE_IOS) > -1)
        && computeMajorVersion(ua, EDGE) <= 44;
  }

  private boolean isMobileSafari(final String ua) {
    // Both iOS and AppleWebKit means mobile safari
    return ua.indexOf(IOS) > -1
        && ua.indexOf(APPLE_WEB_KIT) > -1;
  }

  private static int computeMajorVersion(final String ua, final String key) {
    final int begin = ua.indexOf(key);
    final int beginVersion = begin + key.length() + 1;
    final int endVersion = ua.indexOf(" ", beginVersion);

    final String version = ua.substring(beginVersion, endVersion);
    final String[] versionParts = version.split(".");
    try {
      return Integer.parseInt(versionParts[0]);
    } catch (final NumberFormatException nfe) {
      return -1;
    }
  }
}
