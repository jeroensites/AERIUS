/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.geo.icons;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import nl.overheid.aerius.geo.icon.DynamicSvgIcon;

public class LabelSvgIconBuilder {

  // Settings for the text in the label
  private static final String TEXT_STYLE = "font-size: 19px; font-weight: bold; font-family: 'Noto Sans TC', Arial, Sans-Serif; white-space: nowrap;";

  // Settings for the colored textBox
  private static final int TEXT_BOX_HEIGHT = 30;
  private static final int TEXT_BOX_HORIZONTAL_PADDING = 8;

  // Settings for the border surrounding and between labels
  private static final int LABEL_BORDER_WIDTH = 3;
  private static final int LABEL_BETWEEN_BORDER_WIDTH = 3;
  private static final String LABEL_COLOR = "#222222";

  // Settings for the outline if applicable (selected or moving)
  private static final double OUTLINE_WIDTH = 2.5;
  private static final double OUTLINE_OFFSET = 2;
  private static final String OUTLINE_COLOR = "#157cb1";

  // Constants for computing outlines in corners
  private static final double COS_45 = 0.7071;
  private static final double TAN_22_DOT_5 = 0.4142;

  private static final Map<String, DynamicSvgIcon> cache = new HashMap<>();

  public DynamicSvgIcon getSvgIcon(final List<Label> markers, final boolean hasOutline, final LabelStyle labelStyle) {

    // Should uniquely identify a marker, making sure the style is not re-rendered on panning / zooming
    final StringBuilder cacheKeyBuilder = new StringBuilder(hasOutline + "." + labelStyle.name());
    for (final Label marker : markers) {
      cacheKeyBuilder.append(".").append(marker.getLabelText()).append(".").append(marker.getBackgroundColor()).append(".").append(marker.getTextColor());
    }
    final String cacheKey = cacheKeyBuilder.toString();

    return cache.computeIfAbsent(cacheKey, key -> buildSvgIcon(markers, hasOutline, labelStyle));
  }

  private DynamicSvgIcon buildSvgIcon(final List<Label> markers, final boolean hasOutline, final LabelStyle labelStyle) {
    final StringBuilder svg = new StringBuilder();

    final List<Integer> textWidths = markers.stream().map(m ->
      // Compute the required size of the text label
      // The textBox width is at least getTextBoxMinWidth()
      Math.max(this.calculateStringWidth(m.labelText) + TEXT_BOX_HORIZONTAL_PADDING * 2, labelStyle.getTextBoxMinWidth())
    ).collect(Collectors.toList());
    final int totalTextWidths = textWidths.stream().mapToInt(i -> i + LABEL_BETWEEN_BORDER_WIDTH).sum() - LABEL_BETWEEN_BORDER_WIDTH;
    final int totalTextWidthsNoOffset = totalTextWidths - (2 * labelStyle.getTextBoxRadius());

    // Compute the boundaries of the image
    final double outerOffset = OUTLINE_OFFSET + OUTLINE_WIDTH + labelStyle.getLabelOffset();
    double viewBoxMaxX =  totalTextWidthsNoOffset / 2D + outerOffset;
    double viewBoxMinX = -viewBoxMaxX;
    final double viewBoxMaxY = labelStyle.getTextBoxNoOffset() / 2D + outerOffset;
    final double viewBoxMinY = -viewBoxMaxY;

    if (labelStyle.getPointerLocation() == PointerLocation.TOP_LEFT || labelStyle.getPointerLocation() == PointerLocation.BOTTOM_LEFT) {
      // Marker is larger on the left
      viewBoxMinX -= (outerOffset / TAN_22_DOT_5) + labelStyle.getTextBoxNoOffset() - outerOffset;
    }
    if (labelStyle.getPointerLocation() == PointerLocation.TOP_RIGHT || labelStyle.getPointerLocation() == PointerLocation.BOTTOM_RIGHT) {
      // Marker is larger on the right
      viewBoxMaxX += (outerOffset / TAN_22_DOT_5) + labelStyle.getTextBoxNoOffset() - outerOffset;
    }

    final double width = viewBoxMaxX - viewBoxMinX;
    final double height = viewBoxMaxY - viewBoxMinY;

    svg.append("<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"")
        .append(viewBoxMinX).append(" ").append(viewBoxMinY).append(" ")
        .append(viewBoxMaxX - viewBoxMinX).append(" ").append(viewBoxMaxY - viewBoxMinY)
        .append("\" width=\"").append(width)
        .append("\" height=\"").append(height)
        .append("\">");

    // If the source is selected or is moving, the label gets an outline with a white background
    if (hasOutline) {
      svg.append("<path d=\"");
      appendOffsetPath(svg, labelStyle, totalTextWidthsNoOffset, OUTLINE_OFFSET + OUTLINE_WIDTH + labelStyle.getLabelOffset());
      svg.append("\" fill=\"#ffffff\" />");

      svg.append("<path d=\"");
      appendOffsetPath(svg, labelStyle, totalTextWidthsNoOffset, OUTLINE_OFFSET + OUTLINE_WIDTH / 2.0 + labelStyle.getLabelOffset());
      svg.append("\" stroke=\"").append(OUTLINE_COLOR).append("\" fill=\"transparent\" stroke-width=\"").append(OUTLINE_WIDTH).append("\"");

      svg.append("/>");
    }

    // Draw the label itself
    svg.append("<path d=\"");
    appendOffsetPath(svg, labelStyle, totalTextWidthsNoOffset, labelStyle.getLabelOffset());
    svg.append("\" fill=\"" + LABEL_COLOR + "\" />");

    // Draw the textBoxes and label texts
    int i = 0;
    int offsetX = -totalTextWidths / 2;
    for (Label marker : markers) {

      final boolean leftRoundedCorners = i == 0; // The first element has rounded corners on the left
      final boolean rightRoundedCorners = i == markers.size() - 1; // The last element has rounded corners on the right

      final int textWidth = textWidths.get(i);
      drawTextBox(svg, offsetX, textWidth, marker.getBackgroundColor(), labelStyle, leftRoundedCorners, rightRoundedCorners);
      drawText(svg, offsetX, textWidth, marker.getLabelText(), marker.getTextColor());
      offsetX += textWidth + LABEL_BETWEEN_BORDER_WIDTH;

      i++;
    }

    svg.append("</svg>");

    // Determine the anchor point of the marker (which pixel points to the actual source).
    // Because the marker now can have an outline, the anchor is not always at 0,0 anymore
    double anchorPointX = (OUTLINE_OFFSET + OUTLINE_WIDTH) / TAN_22_DOT_5;
    double anchorPointY = OUTLINE_OFFSET + OUTLINE_WIDTH;

    // Bottom pointer has the anchor on the opposite side of the label
    if (labelStyle.getPointerLocation() == PointerLocation.TOP_RIGHT || labelStyle.getPointerLocation() == PointerLocation.BOTTOM_RIGHT) {
      anchorPointX = width - anchorPointX;
    }
    if (labelStyle.getPointerLocation() == PointerLocation.BOTTOM_LEFT || labelStyle.getPointerLocation() == PointerLocation.BOTTOM_RIGHT) {
      anchorPointY = height - anchorPointY;
    }

    return new DynamicSvgIcon(svg.toString(), new double[]{anchorPointX, anchorPointY});
  }

  /*
   * Generates a path with a certain offset around the marker
   */
  private void appendOffsetPath(final StringBuilder svg, final LabelStyle labelStyle, final double totalWidth, final double offset) {

    final double textBoxNoOffset = labelStyle.getTextBoxNoOffset();
    final double hWidth = totalWidth / 2D;
    final double hHeight = textBoxNoOffset / 2D;

    // start at the top right, before the rounded corner
    moveTo(svg, hWidth, -hHeight - offset);
    if (labelStyle.getPointerLocation() == PointerLocation.TOP_RIGHT) {

      // top right pointer
      lineTo(svg, hWidth + (offset / TAN_22_DOT_5) + textBoxNoOffset, -hHeight - offset);
      lineTo(svg, hWidth + (COS_45 * offset), hHeight + (COS_45 * offset));
      arcTo(svg, offset, hWidth, hHeight + offset);

    } else if (labelStyle.getPointerLocation() == PointerLocation.BOTTOM_RIGHT) {

      // bottom right pointer
      arcTo(svg, offset, hWidth + (COS_45 * offset), -hHeight - (COS_45 * offset));
      lineTo(svg, hWidth + (offset / TAN_22_DOT_5) + textBoxNoOffset, hHeight + offset);
    } else {
      arcTo(svg, offset, hWidth + offset, -hHeight);

      // bottom right corner
      lineTo(svg, hWidth + offset, hHeight);
      arcTo(svg, offset, hWidth, hHeight + offset);
    }

    lineTo(svg, -hWidth, hHeight + offset);

    if (labelStyle.getPointerLocation() == PointerLocation.BOTTOM_LEFT) {

      // bottom left pointer
      lineTo(svg, -hWidth - (offset / TAN_22_DOT_5) - textBoxNoOffset, hHeight + offset);
      lineTo(svg, -hWidth - (COS_45 * offset), -hHeight - (COS_45 * offset));
      arcTo(svg, offset, -hWidth, -hHeight - offset);

    } else if (labelStyle.getPointerLocation() == PointerLocation.TOP_LEFT) {

      // top left pointer
      arcTo(svg, offset, -hWidth - (COS_45 * offset), hHeight + (COS_45 * offset));
      lineTo(svg, -hWidth - (offset / TAN_22_DOT_5) - textBoxNoOffset, -hHeight - offset);
    } else {
      arcTo(svg, offset, -hWidth - offset, hHeight);

      // top left corner
      lineTo(svg, -hWidth - offset, -hHeight);
      arcTo(svg, offset, -hWidth, -hHeight - offset);
    }

    closePath(svg);
  }

  private static void closePath(final StringBuilder svg) {
    svg.append("Z");
  }

  private static void moveTo(final StringBuilder svg, final double x, final double y) {
    svg.append("M").append(x).append(",").append(y);
  }

  private static void lineTo(final StringBuilder svg, final double x, final double y) {
    svg.append("L").append(x).append(",").append(y);
  }

  private static void arcTo(final StringBuilder svg, final double radius, final double x, final double y) {
    svg.append("A").append(radius).append(" ").append(radius).append(",0,0,1,").append(x).append(",").append(y);
  }

  private void drawTextBox(final StringBuilder svg, final double offsetX, final int textBoxWidth, final String backgroundColor,
      final LabelStyle labelStyle, final boolean leftRoundedCorners, final boolean rightRoundedCorners) {
    final double offsetY = -labelStyle.getTextBoxNoOffset() / 2D;
    final double textBoxRadius = labelStyle.getTextBoxRadius();

    svg.append("<path d=\"");
    moveTo(svg, offsetX, offsetY);

    // Top left corner
    if (leftRoundedCorners) {
      arcTo(svg, textBoxRadius, offsetX + textBoxRadius, offsetY - textBoxRadius);
    } else {
      lineTo(svg, offsetX, offsetY - textBoxRadius);
    }
    lineTo(svg, offsetX + textBoxWidth - textBoxRadius, offsetY - textBoxRadius);

    // Top right corner
    if (rightRoundedCorners) {
      arcTo(svg, textBoxRadius, offsetX + textBoxWidth, offsetY);
    } else {
      lineTo(svg, offsetX + textBoxWidth, offsetY - textBoxRadius);
    }
    lineTo(svg, offsetX + textBoxWidth, offsetY - textBoxRadius * 2 + TEXT_BOX_HEIGHT);

    // Bottom right corner
    if (rightRoundedCorners) {
      arcTo(svg, textBoxRadius, offsetX + textBoxWidth - textBoxRadius, offsetY - textBoxRadius + TEXT_BOX_HEIGHT);
    } else {
      lineTo(svg, offsetX + textBoxWidth, offsetY - textBoxRadius + TEXT_BOX_HEIGHT);
    }
    lineTo(svg, offsetX + textBoxRadius, offsetY - textBoxRadius + TEXT_BOX_HEIGHT);

    // Bottom left corner
    if (leftRoundedCorners) {
      arcTo(svg, textBoxRadius, offsetX, offsetY - textBoxRadius * 2 + TEXT_BOX_HEIGHT);
    } else {
      lineTo(svg, offsetX, offsetY - textBoxRadius + TEXT_BOX_HEIGHT);
    }
    closePath(svg);
    svg.append("\" fill=\"").append(backgroundColor).append("\" />");
  }

  private static void drawText(final StringBuilder svg, final int xOffset, final int textBoxWidth, final String label, final String labelColor) {
    svg.append("<text");
    svg.append(" text-anchor=\"middle\" ");
    svg.append(" dominant-baseline=\"central\" ");
    svg.append(" style=\"").append(TEXT_STYLE).append("\"");
    svg.append(" x=\"").append(xOffset + textBoxWidth / 2.0).append("\"");
    svg.append(" y=\"").append(0).append("\" ");
    svg.append(" fill=\"").append(labelColor).append("\"");
    svg.append(">").append(label).append("</text>");
  }

  /*
   * In order to determine the width of a certain string precisely you have to add it to the DOM.
   * Otherwise the width will remain 0.
   */
  private int calculateStringWidth(final String text) {
    return calculateStringWidthInt(text, LabelSvgIconBuilder.TEXT_STYLE);
  }

  private static native int calculateStringWidthInt(final String text, final String textStyle) /*-{
      var svg = document.createElement('svg');
      var textElement = document.createElement("text");
      textElement.setAttribute("style", textStyle);
      textElement.innerText = text;
      svg.appendChild(textElement);
      document.body.appendChild(svg);
      var width = textElement.getBoundingClientRect().width;
      document.body.removeChild(svg);
      return width;
  }-*/;

  public static class Label {

    private static final String WHITE_COLOR = "#ffffff";

    private final String labelText;
    private final String backgroundColor;
    private final String textColor;

    public Label(final String labelText, final String backgroundColor) {
      this(labelText, backgroundColor, determineLabelColor(backgroundColor));
    }

    public Label(final String labelText, final String backgroundColor, final String textColor) {
      this.labelText = labelText;
      this.backgroundColor = backgroundColor;
      this.textColor = textColor;
    }

    public String getLabelText() {
      return labelText;
    }

    public String getBackgroundColor() {
      return backgroundColor;
    }

    public String getTextColor() {
      return textColor;
    }

    private static String determineLabelColor(final String backgroundColor) {
      if (Objects.equals(backgroundColor, WHITE_COLOR)) {
        return LABEL_COLOR;
      }
      return WHITE_COLOR;
    }
  }

  public enum PointerLocation {
    TOP_LEFT,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_RIGHT;
  }

  public enum LabelStyle {
    EMISSION_SOURCE(8, 40, PointerLocation.TOP_LEFT),
    BUILDING(8, 40, PointerLocation.BOTTOM_RIGHT),
    CALCULATION_POINT(15, 30, PointerLocation.TOP_RIGHT);

    private final int textBoxRadius;
    private final int textBoxMinWidth;
    private final PointerLocation pointerLocation;

    private final int textBoxNoOffset;
    private final int labelOffset;

    LabelStyle(final int textBoxRadius, final int textBoxMinWidth, final PointerLocation pointerLocation) {
      this.textBoxRadius = textBoxRadius;
      this.textBoxNoOffset = TEXT_BOX_HEIGHT - (2 * textBoxRadius);
      this.labelOffset = LABEL_BORDER_WIDTH + textBoxRadius;
      this.textBoxMinWidth = textBoxMinWidth;
      this.pointerLocation = pointerLocation;
    }

    public int getTextBoxRadius() {
      return textBoxRadius;
    }

    public int getTextBoxMinWidth() {
      return textBoxMinWidth;
    }

    public PointerLocation getPointerLocation() {
      return pointerLocation;
    }

    public int getTextBoxNoOffset() {
      return textBoxNoOffset;
    }

    public int getLabelOffset() {
      return labelOffset;
    }
  }

}
