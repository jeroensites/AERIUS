/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.place;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;

import nl.aerius.wui.place.PlaceTokenizer;
import nl.aerius.wui.place.TokenizerUtils;

public class PrintTokenizer implements PlaceTokenizer<PrintWnbPlace> {
  private final String prefix;
  private final Supplier<PrintWnbPlace> supplier;

  public PrintTokenizer(final Supplier<PrintWnbPlace> supplier, final String prefix) {
    this.supplier = supplier;
    this.prefix = prefix;
  }

  @Override
  public PrintWnbPlace getPlace(final String token) {
    final Map<String, String> parameters = TokenizerUtils.find(token);

    final PrintWnbPlace place = supplier.get();
    place.getTokens().putAll(parameters);

    return place;
  }

  @Override
  public final String getToken(final PrintWnbPlace place) {
    final Map<String, String> pairs = new LinkedHashMap<>();
    final Map<String, String> composites = new LinkedHashMap<>(place.getTokens());

    return TokenizerUtils.format(pairs, composites);
  }

  @Override
  public String getPrefix() {
    return prefix;
  }

  public static PlaceTokenizer<PrintWnbPlace> create(final Supplier<PrintWnbPlace> supplier, final String prefix) {
    return new PrintTokenizer(supplier, prefix);
  }
}
