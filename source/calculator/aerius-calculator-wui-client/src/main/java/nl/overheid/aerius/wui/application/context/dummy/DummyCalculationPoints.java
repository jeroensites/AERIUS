/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.application.context.dummy;

import java.util.ArrayList;
import java.util.List;

import ol.Coordinate;
import ol.geom.Point;

import nl.overheid.aerius.wui.application.domain.source.CalculationPointFeature;

public class DummyCalculationPoints {


  public static List<CalculationPointFeature> calculationPoints() {
    final ArrayList<CalculationPointFeature> calculationPointFeatures = new ArrayList<>();

    final CalculationPointFeature feature = new CalculationPointFeature();
    CalculationPointFeature.init(feature);
    feature.setId("1");
    feature.setGmlId("CP.1");
    feature.setLabel("Thuis");
    feature.setCustomPointId(1);
    feature.setGeometry(new Point(new Coordinate(150300, 450000)));
    calculationPointFeatures.add(feature);

    final CalculationPointFeature feature2 = new CalculationPointFeature();
    CalculationPointFeature.init(feature2);
    feature2.setId("2");
    feature2.setGmlId("CP.2");
    feature2.setLabel("Werk");
    feature2.setCustomPointId(2);
    feature2.setGeometry(new Point(new Coordinate(150800, 450000)));
    calculationPointFeatures.add(feature2);

    return calculationPointFeatures;
  }
}
