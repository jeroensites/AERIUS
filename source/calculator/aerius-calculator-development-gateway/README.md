# AERIUS Calculator service gateway module

This module contains the (Spring boot) webservice to start a zuul application for local development.
Zuul acts as an easy proxy to tie the different services and front-end parts together so you can go to `http://localhost:8080` and have everything working.
No hassle with different ports or cross-origin concerns, as long as everything is properly configured.

## Deployment

Build the module with maven `mvn clean install` from the parent folder.
This will build a runnable jar file.

Within the project this jar is included in a docker container, which in turn is deployed on the different environments.

## Development

The application is a pretty standard spring boot application.
As such, it can be either run through your favorite IDE as a java application by selecting the correct main class, or run maven with `mvn spring-boot:run -pl :aerius-calculator-development-gateway`.
Create your own  `config/application.yaml` file to override the default properties.

Alternatively, use the [Scripts](/scripts/) (linux only) to start your application.
