#!/usr/bin/env bash
set -e

# Change current directory to directory of script so it can be called from everywhere
SCRIPT_PATH=$(readlink -f "${0}")
SCRIPT_DIR=$(dirname "${SCRIPT_PATH}")
cd "${SCRIPT_DIR}"

# Optional: include search client sources in codeserver recompiler
# Create 'search-repo-dir.txt' and put the relative url to the
# search repository in there (relative to `source/`) in order
# to have the codeserver recompile those sources.
if [[ -f search-repo-dir.txt ]]; then
  searchDir=$(readlink -f "$(< search-repo-dir.txt)")
fi

cd ../..
echo "Starting in: $(pwd)"

mvn gwt:codeserver -pl :aerius-calculator-wui-client -am -Denv=dev \
  -Dgwt.style=DETAILED \
  -Dmaven.buildNumber.skip=true \
  -Dgwt.modules=nl.overheid.aerius.AeriusDebug \
  -Dgwt.additional.sources="${searchDir:-/tmp/this-does-not-exist/}"

