#
# Copyright the State of the Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.
#

openapi: 3.0.1
info:
  title: AERIUS connect service
  description: |
    ## Analyse
    Deze definitie bevat methodes waarmee AERIUS berekeningen gedaan kunnen worden, 
    en waarbij instellingen mogelijk zijn die niet mogelijk zijn met de standaard bereken methodes die bijvoorbeeld geschikt zijn voor vergunningverlening. 

    ## Gemeenschappelijke definitie
    Deze definitie is onderdeel van een verzameling van definities. 
    Het onderdeel [api-common.yaml](api-common.yaml) bevat de gemeenschappelijke methodes die niet gebonden zijn aan een specifieke toepassing.
    Dat onderdeel bevat onder andere de methodes en omschrijving hoe een api-key te bemachtigen die benodigd is voor de methodes binnen deze definitie. 
  termsOfService: https://www.aerius.nl/nl/producten/aerius-connect
  contact:
    name: AERIUS - Bij12 helpdesk
    url: https://www.bij12.nl/onderwerpen/stikstof-en-natura2000/helpdesk/
  license:
    name: Apache License, version 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0
  version: 7.0.0
servers:
  - url: /api/v7/
    description: Basis URL relatief ten opzichte van deze pagina
tags:
  - name: analysis
    description: Analyse gerelateerde methodes
paths:
  /analysis/wnb:
    post:
      summary: Analyse voor Wet Natuurbescherming
      description: |
        Deze methode kan gebruikt worden voor berekeningen in het kader van Wet Natuurbescherming.
        Bij deze methode is het mogelijk om met behulp van de opties de berekening te beinvloeden, zie WnbAnalysisOptions voor mogelijke opties.
        Door gebruik te maken van deze opties kunnen de resultaten verschillend zijn met de resultaten van een normale berekening in het kader van WNB.
        De default waardes die voor opties gelden zijn gelijk aan een normale berekening in het kader van WNB.

        Daarnaast zijn er onderdelen van de berekening die niet met opties te beinvloeden zijn, maar wel verschillend zijn tussen verschillende toepassingen (bijvoorbeeld WNB en RBL).
        Voor deze onderdelen wordt ook uitgegaan van de WNB manier van berekenen.
        
        Bij deze methode wordt uitgegaan van eigen rekenpunten.
        Deze rekenpunten kunnen worden opgegeven in aangeleverde bestanden (GML of RCP) bij de berekening, 
        of er kan gebruik gemaakt worden van een eerder vastgelegde receptorSet.
        
        Met deze methode is het niet mogelijk om een PDF geschikt voor vergunningverlening te verkrijgen.
      operationId: analyseWnb
      tags:
        - analysis
      security:
        - ApiKeyAuth: []
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/WnbAnalysisRequest'
      responses:
        200:
          $ref: 'api-common.yaml#/components/responses/CalculationStarted'
        400:
          $ref: 'api-common.yaml#/components/responses/ClientError'
        440:
          $ref: 'api-common.yaml#/components/responses/ValidationError'
components:
  securitySchemes:
    ApiKeyAuth:
      type: apiKey
      in: header
      name: api-key
  schemas:
    WnbAnalysisRequest:
      type: object
      required:
        - options
        - files
        - fileParts
      properties:
        options:
          $ref: '#/components/schemas/WnbAnalysisOptions'
        files:
          $ref: 'api-common.yaml#/components/schemas/UploadFiles'
        fileParts:
           $ref: 'api-common.yaml#/components/schemas/FileParts'
    WnbAnalysisOptions:
      allOf:
        - $ref: 'api-wnb.yaml#/components/schemas/WnbCalculationOptions'
        - description: Rekenopties voor analyse berekening voor WNB.
        - type: object
          required:
            - outputType
          properties:
            outputType:
              type: string
              enum: 
                - GML
                - CSV
              description: |
                Geeft aan wat voor type resultaat bestanden gebruikt moet worden.
                Ondersteunende waarden zijn:
                ___GML:___ AERIUS IMAER GML.
                ___CSV:___ Resultaten worden per sector en uitvoer type opgeleverd als CSV bestand. Dit bestand bevat geen brongegevens en is niet in te lezen in AERIUS.
            calculationPointsType:
              type: string
              default: CUSTOM_POINTS
              enum:
                - CUSTOM_POINTS
              description: | 
                Deze optie kan gebruikt worden om aan te geven welke rekenpunten doorgerekend moeten worden.
                De volgende opties worden ondersteund:
                ___CUSTOM_POINTS:___  Rekent met eigen rekenpunten.

                In het geval van CUSTOM_POINTS worden de rekenpunten uit de aangeleverde bestanden gebruikt, of worden de rekenpunten van een via '/receptorSets' opgevoerde receptor set gebruikt. 

                Is deze optie niet meegegeven, dan zal uit worden gegaan van CUSTOM_POINTS.
            withWNBMaxDistance:
              type: boolean
              default: true
              description: |
                Optie om aan te geven of bronemmissies moeten worden afgekapt volgens de 25km WNB beleidsregel.

                Is deze optie niet meegegeven, dan zullen de bronemissies worden afgekapt op 25km.
            stacking:
              type: boolean
              default: true
              description: |
                Optie om aan te geven dat stapelen gebruikt dient te worden.
                Puntbronnen waarvan alle eigenschappen behalve de emissies gelijk zijn kunnen gestapeld worden.
                Dat wil zeggen dat de bronnen samengevoegd worden door de emissies per stof op te tellen.

                Wanneer deze optie op 'true' staat of niet wordt meegegeven worden bronnen gestapeld.
                Wanneer deze optie op 'false' staat worden bronnen niet gestapeld.
            aggregate:
              type: boolean
              default: false
              description: |
                Bij berekeningen kan gebruik worden gemaakt van aggregatie.
                Afhankelijk van de afstand van receptor naar de bronnen kunnen bronnen worden geaggregeerd, ook al liggen deze niet exact op elkaar, en zijn niet alle eigenschappen gelijk.

                De afstand waarbij dit geldt is sector afhankelijk, waarbij de afstand vastligt in (de database van) AERIUS.

                Om bronnen te kunnen aggregeren gelden bepaalde regels, zoals een maximale uitstoothoogte en maximale warmteinhoud.
                Voor meer informatie zie de factsheets op de AERIUS website.

                Wanneer deze optie op 'true' staat worden bronnen geaggregeerd als dat volgens de regels kan.
                Wanneer deze optie op 'false' staat of niet wordt meegegeven worden bronnen niet geaggregeerd.
            roadOPS:
              type: string
              enum:
                - DEFAULT
                - OPS_ROAD
                - OPS_ALL
              default: DEFAULT
              description: |
                Met deze optie wordt wegverkeer niet met het SRM2 model maar met het OPS model doorgerekend.
                Er worden dan resultaten terug gegeven die buiten het modelbereik van SRM2 vallen.
                De volgende opties worden ondersteund:
                ___DEFAULT:___ Rekent met de normale rekeninstellingen / model.
                ___OPS_ROAD:___  Rekent voor bronnen die normaal met het SRM model worden doorgerekend vanaf 5 km afstand met het OPS model. Tot 5 km afstand wordt er voor deze bronnen niks berekend. Andere bronnen worden als normaal doorgerekend.
                ___OPS_ALL:___ Rekent alles uit met alleen OPS.
                
                Wanneer de optie niet wordt meegegeven wordt er met de optie DEFAULT gewerkt.
            meteoYear:
              type: string
              description: |
                Het veld geeft aan met welke meteojaar er gerekend moet worden. Bij het gebruik van een enkel jaar,
                volstaat het jaar, bijv "2013". Het gebruik van meerjarige meteo kan door het start en eind jaar
                op te geven met een streepje, bijv. "1995-2004". Er wordt een foutmelding teruggegeven als het
                opgegeven meteojaar niet bestaat.

                Wanneer deze optie niet wordt meegegeven wordt er met de standaard meteo gewerkt die AERIUS op dat moment hanteert.
            useReceptorHeight:
              type: boolean
              default: false
              description: |
                Receptoren kunnen (met behulp van een RCP bestand via de receptorSet methode) worden opgegeven met hoogtes.
                Is de waarde van deze optie true, dan worden de receptoren ook daadwerkelijk met die hoogtes doorgerekend.
                Mochten de hoogtes van receptoren niet beschikbaar zijn, dan zal een foutmelding volgen.
                Wordt een andere resultType dan CONCENTRATION gebruikt, dan zal een foutmelding volgen. 

                Wanneer deze optie op 'true' staat worden de receptoren met de hoogte waarde doorgerekend.
                Wanneer deze optie op 'false' staat of niet wordt meegegeven dan wordt de hoogte niet meegenomen in de berekening.
            validateAgainstSchema:
              type: boolean
              default: true
              description: |
                Met deze optie kan geconfigureerd worden of de validatie van IMAER GML bestanden d.m.v. de XSD uitgevoerd moet worden.
                Deze validatie niet uitvoeren vergroot de snelheid van de validatie stap, ten koste van enkele controles die uitgevoerd worden.
                Dit kan voor onduidelijke foutmeldingen in de daadwerkelijke berekeningen zorgen.

                Wanneer deze optie op 'true' staat of niet wordt meegegeven wordt er wel validatie tegen de XSD uitgevoerd.
                Wanneer deze optie op 'false' staat dan wordt deze validatie niet uitgevoerd.

            opsOptions:
              $ref: '#/components/schemas/OpsOptions'
              default: null
            outputOptions:
              $ref: '#/components/schemas/WnbAnalysisOutputOptions'
    WnbAnalysisOutputOptions:
      type: object
      description: Aanvullende rekenopties m.b.t. de resultaten
      properties:
        resultTypes:
          type: array
          description: |
            Geeft aan welke type resultaten moeten worden opgeleverd.
            Wordt deze optie niet meegegeven, dan wordt standaard alleen de waarde __DEPOSITION__ gebruikt.
          default:
            - DEPOSITION
          items:
            $ref: '#/components/schemas/ResultType'
        sectorOutput:
          type: boolean
          default: false
          description: |
            Optioneel veld die gebruikt kan worden in combinatie met outputType GML.
            Is de waarde van deze optie 'true', dan wordt per sector in de berekening een resultaat GML gegenereerd, naast de resultaat GML met totalen.
            Is de waarde van deze optie 'false' of wordt deze niet meegegeven, dan zal alleen de resultaat GML met totalen worden gegenereerd.
            
            In het geval van andere outputTypes wordt deze optie genegeerd.
    OpsOptions:
      type: object
      description: |
        Specifieke rekenopties voor OPS, die normaal gesproken in de control file (.ctr) worden weggeschreven. Deze waarden overschrijven de standaard
        waarden die in een normale AERIUS-berekening worden gebruikt. Bij het meegeven van de waarde `null` wordt de waarde niet overschreven.
      properties:
        rawInput:
          type: boolean
          default: false
          description: |
            Met deze optie kan het standaardgedrag rond het wegschrijven van bron (.brn) bestanden worden uitgeschakeld. Ook vind er geen validatie
            op de bron (.brn) bestanden plaats. Foutieve waarden leiden tot een foutmelding uit OPS.
        year:
          type: integer
          default: null
          description: |
            Control file (.ctr) waarde `YEAR`: rekenjaar (dat meegegeven wordt aan OPS, dus niet voor emissiefactoren en meteo).
        compCode:
          type: integer
          default: null
          description: |
            Control file (.ctr) waarde `COMPCODE`: stof code
        molWeight:
          type: double
          default: null
          description: |
            Control file (.ctr) waarde `ROUGHNESS`: moleculairgewicht
        phase:
          type: integer
          default: null
          description: |
            Control file (.ctr) waarde `PHASE`: fase
        loss:
          type: integer
          default: null
          description: |
            Control file (.ctr) waarde `LOSS`: depositie
        diffCoeff:
          type: string
          default: null
          description: |
            Control file (.ctr) waarde `DIFFCOEFF`: moleculaire differentie coëfficiënt
        washout:
          type: string
          default: null
          description: |
            Control file (.ctr) waarde `WASHOUT`: uitwassen
        convRate:
          type: string
          default: null
          description: |
            Control file (.ctr) waarde `CONVRATE`: omzetting coëfficiënt
        roughness:
          type: double
          default: null
          description: |
            Control file (.ctr) waarde `ROUGHNESS`: ruwheidslengte

    ResultType:
      type: string
      enum:
        - CONCENTRATION
        - DEPOSITION
        - DRY_DEPOSITION
        - WET_DEPOSITION
