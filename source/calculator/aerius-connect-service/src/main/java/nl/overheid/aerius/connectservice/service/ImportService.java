/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.controller.ImaerController;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.io.ImportableFile;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;

@Service
public class ImportService {

  private final ImaerController imaerController;
  private final LocaleService localeService;

  @Autowired
  public ImportService(final ImaerController imaerController, final LocaleService localeService) {
    this.imaerController = imaerController;
    this.localeService = localeService;
  }

  public List<ImportParcel> importFile(final MultipartFile filePart, final Set<ImportOption> options, final ImportProperties importProperties)
      throws AeriusException {
    final ImportableFile file = new MultipartImportableFile(filePart);
    return importFile(file, options, importProperties);
  }

  public List<ImportParcel> importFile(final ImportableFile importableFile, final Set<ImportOption> options, final ImportProperties importProperties)
      throws AeriusException {
    return imaerController.processFile(importableFile, options, importProperties, localeService.getLocale());
  }

}
