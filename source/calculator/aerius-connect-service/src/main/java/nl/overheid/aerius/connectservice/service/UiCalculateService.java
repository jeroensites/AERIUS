/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.calculation.NCACalculationOptionsUtil;
import nl.overheid.aerius.calculation.WNBCalculationOptionsUtil;
import nl.overheid.aerius.connectservice.domain.ExportOptions;
import nl.overheid.aerius.connectservice.domain.UiCalculationRequest;
import nl.overheid.aerius.db.calculator.CalculatorLimitsRepository;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.CalculatorLimits;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.validation.EmissionSourceCheckLimits;

@Service
public class UiCalculateService extends AbstractCalculateService<UiCalculationRequest> {
  private static final Logger LOGGER = LoggerFactory.getLogger(UiCalculateService.class);

  /**
   * Defines the default number of sources to determine if to run or small or large calculations queue.
   */
  private static final int DEFAULT_CALCULATOR_LARGE_CALCULATIONS_SPLIT = 100;

  private final UiSourcesService sourcesService;
  private final CalculatorLimitsRepository calculatorLimitsRepository;

  @Autowired
  UiCalculateService(final ExportTaskClientBean exportTaskclient, final JobRepositoryBean jobRepository,
      final ConnectCalculationPointSetsRepositoryBean pointSetsRepository, final ConstantRepositoryBean constantRepository,
      final ProxyFileService fileService, final UiSourcesService sourcesService, final CalculatorLimitsRepository calculatorLimitsRepository,
      final LocaleService localeService) {
    super(exportTaskclient, jobRepository, pointSetsRepository, constantRepository, fileService, localeService);
    this.sourcesService = sourcesService;
    this.calculatorLimitsRepository = calculatorLimitsRepository;
  }

  @Override
  public String calculate(final ConnectUser connectUser, final Scenario scenario, final UiCalculationRequest options) throws AeriusException {
    // Should not call this specific method for UI calculations
    throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
  }

  @Override
  protected JobType getJobType(final UiCalculationRequest options) {
    return Optional.ofNullable(options.getExportOptions())
        .map(ExportOptions::getJobType)
        .orElse(JobType.CALCULATION);
  }

  @Override
  protected Optional<String> getJobName(final UiCalculationRequest options) {
    return Optional.empty();
  }

  @Override
  protected boolean useCustomPoints(final UiCalculationRequest options) {
    // Either the calculation type is custom points,
    // in which case there should always be custom points supplied (checked in calling method).
    // Or just use/add the custom points that were supplied, if any.
    return explicitlyUseCustomPoints(options)
        || !options.getScenario().getCustomPointsList().isEmpty();
  }

  private static boolean explicitlyUseCustomPoints(final UiCalculationRequest request) {
    return request.getScenario().getOptions() != null
        && request.getExportOptions().getExportType() != ExportType.PAA
        && request.getScenario().getOptions().getCalculationType() == CalculationType.CUSTOM_POINTS;
  }

  @Override
  protected String getReceptorSetName(final UiCalculationRequest options) {
    return null;
  }

  @Override
  protected Optional<Integer> getOverrideYear(final UiCalculationRequest options) {
    return Optional.empty();
  }

  @Override
  protected void validateBeforeSending(final Scenario scenario, final UiCalculationRequest options) throws AeriusException {
    final CalculatorLimits limits = calculatorLimitsRepository.getCalculatorLimits();

    if (options.getExportOptions().getExportType() == ExportType.PAA &&
        scenario.getSituations().stream().noneMatch(scenarioSituation -> scenarioSituation.getType() == SituationType.PROPOSED)) {
      throw new AeriusException(AeriusExceptionReason.CALCULATION_PAA_PROPOSED_SITUATION_MISSING);
    }

    if (options.getExportOptions().getExportType() != ExportType.GML_SOURCES_ONLY) {
      final int numberOfSources = scenario.getSituations().stream().mapToInt(s -> s.getEmissionSourcesList().size()).sum();
      EmissionSourceCheckLimits.check(numberOfSources, limits);
    }

    for (final ScenarioSituation situation : scenario.getSituations()) {
      EmissionSourceCheckLimits.checkGeometries(situation.getEmissionSourcesList(), limits);
      sourcesService.validateSources(situation.getEmissionSourcesList());
    }
  }

  @Override
  protected void updateScenarioOptions(final Scenario scenario, final UiCalculationRequest options) throws AeriusException {
    scenario.setTheme(options.getTheme());
    switch (options.getTheme()) {
    case WNB:
      final CalculationSetOptions cso = WNBCalculationOptionsUtil.createWnbCalculationSetOptions();

      cso.setCalculationType(explicitlyUseCustomPoints(options) ? CalculationType.CUSTOM_POINTS : CalculationType.PERMIT);
      scenario.setOptions(cso);
      break;
    case NCA:
      final CalculationSetOptions csoNca = NCACalculationOptionsUtil.createCalculationSetOptions();

      csoNca.setCalculationType(explicitlyUseCustomPoints(options) ? CalculationType.CUSTOM_POINTS : CalculationType.PERMIT);
      scenario.setOptions(csoNca);
      break;
    case RBL:
    default:
      // Not yet implemented theme
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  @Override
  protected void setExportProperties(final ExportProperties exportProperties, final UiCalculationRequest options) {
    Optional.ofNullable(options.getExportOptions())
        .ifPresent(requestOptions -> {
          exportProperties.setExportType(requestOptions.getExportType());
          exportProperties.setEmailAddress(requestOptions.getEmail());
        });
  }

  @Override
  protected boolean shouldNotSendEmail(final UiCalculationRequest options) {
    return options.getExportOptions() == null;
  }

  @Override
  protected QueueEnum determineQueue(final UiCalculationRequest options) {
    final ExportType exportType = options.getExportOptions() == null ? ExportType.CALCULATION_UI : options.getExportOptions().getExportType();

    if (exportType != ExportType.CALCULATION_UI) {
      return exportType == ExportType.GML_SOURCES_ONLY ? QueueEnum.CONNECT_GML_EXPORT : QueueEnum.CONNECT_PAA_EXPORT;
    }
    final int size = options.getScenario().getSituations().stream().mapToInt(s -> s.getEmissionSourcesList().size()).sum();
    int configuredSize = -1;

    try {
      configuredSize = constantRepository.getInteger(ConstantsEnum.CALCULATOR_LARGE_CALCULATIONS_SPLIT);
    } catch (final AeriusException e) {
      LOGGER.error("Error determining CALCULATOR_LARGE_CALCULATIONS_SPLIT", e);
    }

    final int maxSize = configuredSize > 0 ? configuredSize : DEFAULT_CALCULATOR_LARGE_CALCULATIONS_SPLIT;
    return size > maxSize ? QueueEnum.CALCULATION_UI_LARGE : QueueEnum.CALCULATION_UI_SMALL;
  }

}
