/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.io.IOException;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.connectservice.api.UserApiDelegate;
import nl.overheid.aerius.connectservice.model.GenerateApiKey;
import nl.overheid.aerius.connectservice.service.ConstantService;
import nl.overheid.aerius.connectservice.service.LocaleService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.UserService;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.MailTo;
import nl.overheid.aerius.mail.MessageTaskClient;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.shared.Constants;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;

@Service
public class UserResource implements UserApiDelegate {

  private static final Pattern EMAIL_PATTERN = Pattern.compile(Constants.VALID_EMAIL_ADDRESS_REGEX, Pattern.CASE_INSENSITIVE);

  private final ResponseService responseService;
  private final UserService userService;
  private final TaskManagerClient taskManagerClient;
  private final LocaleService localeService;
  private final ConstantService constantService;

  @Autowired
  UserResource(final ResponseService responseService, final UserService userService, final TaskManagerClient taskManagerClient,
      final LocaleService localeService, final ConstantService constantService) {
    this.responseService = responseService;
    this.userService = userService;
    this.taskManagerClient = taskManagerClient;
    this.localeService = localeService;
    this.constantService = constantService;
  }

  @Override
  public ResponseEntity<Void> generateApiKey(final GenerateApiKey generateApiKey) {
    try {
      generateApiKey(generateApiKey.getEmail());
      return responseService.toOkResponse();
    } catch (final AeriusException | IOException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  private void generateApiKey(final String email) throws AeriusException, IOException {
    validateEmail(email);

    final String apiKey = userService.generateAPIKey(email);

    sendMail(email, apiKey);
  }

  /**
   * Validates the email address.
   * @param emailAddress email address
   * @throws AeriusException throws exception in case of validation errors
   */
  private void validateEmail(final String emailAddress) throws AeriusException {
    if (emailAddress == null || emailAddress.length() == 0 || !EMAIL_PATTERN.matcher(emailAddress.trim()).matches()) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_NO_VALID_EMAIL_SUPPLIED, emailAddress);
    }
  }

  private void sendMail(final String email, final String apiKey) throws IOException {
    final Locale locale = localeService.getLocale();
    final MailMessageData messageData = new MailMessageData(
        MessagesEnum.CONNECT_APIKEY_CONFIRM_SUBJECT, MessagesEnum.CONNECT_APIKEY_CONFIRM_BODY,
        locale, new MailTo(email));
    // Yeah, we could set this before generation, but I really don't care about the probable 1 second precision in this case.
    final Date creationDate = new Date();
    messageData.setReplacement(ReplacementToken.CONNECT_APIKEY, apiKey);
    // I would recommend refactoring CALC_CREATION_* stuff to CREATION_* stuff as this is, well, less okay. Something to do on the Master branch.
    messageData.setReplacement(ReplacementToken.CALC_CREATION_DATE, MessageTaskClient.getDefaultDateFormatted(creationDate, locale));
    messageData.setReplacement(ReplacementToken.CALC_CREATION_TIME, MessageTaskClient.getDefaultTimeFormatted(creationDate, locale));

    // Default urls for usage in e-mail
    messageData.setReplacement(ReplacementToken.MANUAL_URL, constantService.getValue(SharedConstantsEnum.MANUAL_URL));
    messageData.setReplacement(ReplacementToken.QUICK_START_URL, constantService.getValue(SharedConstantsEnum.QUICK_START_URL));
    messageData.setReplacement(ReplacementToken.BIJ12_HELPDESK_URL, constantService.getValue(SharedConstantsEnum.BIJ12_HELPDESK_URL));
    MessageTaskClient.startMessageTask(taskManagerClient, messageData);
  }

}
