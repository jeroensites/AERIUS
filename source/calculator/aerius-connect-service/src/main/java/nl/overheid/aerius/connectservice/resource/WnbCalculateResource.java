/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.connectservice.api.WnbApiDelegate;
import nl.overheid.aerius.connectservice.auth.AuthenticationService;
import nl.overheid.aerius.connectservice.model.CalculateResponse;
import nl.overheid.aerius.connectservice.model.UploadFile;
import nl.overheid.aerius.connectservice.model.WnbCalculationOptions;
import nl.overheid.aerius.connectservice.service.ObjectValidatorService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ScenarioService;
import nl.overheid.aerius.connectservice.service.WnbCalculateService;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.shared.domain.Theme;

/**
 * Service class for starting a WNB calculation.
 */
@Service
public class WnbCalculateResource extends CalculateResource<WnbCalculationOptions> implements WnbApiDelegate {

  @Autowired
  WnbCalculateResource(final AuthenticationService authenticationService, final ResponseService responseService,
      final ScenarioService scenarioService, final ObjectValidatorService validatorService, final WnbCalculateService calculateService) {
    super(authenticationService, responseService, scenarioService, validatorService, calculateService);
  }

  @Override
  public ResponseEntity<CalculateResponse> calculateWnb(final WnbCalculationOptions options, final List<UploadFile> files,
      final List<MultipartFile> fileParts) {
    return calculate(files, fileParts, Theme.WNB, Set.of(ImportOption.VALIDATE_AGAINST_SCHEMA), options);
  }
}
