/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.db.common.GeneralRepositoryBean;
import nl.overheid.aerius.db.common.results.ResultsSummaryRepository;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.exception.AeriusException;

@Service
public class UiResultsService {

  private final ResultsSummaryRepository resultsSummaryRepository;
  private final GeneralRepositoryBean generalRepository;

  @Autowired
  public UiResultsService(final ResultsSummaryRepository resultsSummaryRepository, final GeneralRepositoryBean generalRepository) {
    this.resultsSummaryRepository = resultsSummaryRepository;
    this.generalRepository = generalRepository;
  }

  public SituationResultsSummary determineSituationResultsSummary(final SituationCalculations situationCalculations,
      final ScenarioResultType resultType, final int jobId, final Integer calculationId, final SummaryHexagonType hexagonType,
      final EmissionResultKey emissionResultKey) throws AeriusException {
    SituationResultsSummary summary;
    if (hexagonType == SummaryHexagonType.CUSTOM_CALCULATION_POINTS) {
      summary = determineCustomCalculationPointResults(situationCalculations, resultType, calculationId);
    } else {
      summary = determineReceptorResults(situationCalculations, resultType, jobId, calculationId, hexagonType);
    }
    return summary;
  }

  private SituationResultsSummary determineReceptorResults(final SituationCalculations situationCalculations,
      final ScenarioResultType resultType, final int jobId, final Integer calculationId, final SummaryHexagonType hexagonType)
      throws AeriusException {
    return resultsSummaryRepository.determineReceptorResultsSummary(situationCalculations, resultType, jobId, calculationId, hexagonType,
        this::determineAssessmentArea, this::determineHabitatType);
  }

  private SituationResultsSummary determineCustomCalculationPointResults(final SituationCalculations situationCalculations,
      final ScenarioResultType resultType, final Integer calculationId) throws AeriusException {
    return resultsSummaryRepository.determineCustomCalculationPointResultsSummary(situationCalculations, resultType, calculationId);
  }

  private AssessmentArea determineAssessmentArea(final int assessmentAreaId) {
    final Optional<AssessmentArea> area = generalRepository.determineAssessmentArea(assessmentAreaId);
    return area.orElseGet(() -> {
      final AssessmentArea fakeArea = new AssessmentArea();
      fakeArea.setId(assessmentAreaId);
      fakeArea.setName("Gebied " + assessmentAreaId);
      return fakeArea;
    });
  }

  private HabitatType determineHabitatType(final int habitatTypeId) {
    final Optional<HabitatType> habitatType = generalRepository.determineHabitatType(habitatTypeId);
    return habitatType.orElseGet(() -> {
      final HabitatType fakeHabitatType = new HabitatType();
      fakeHabitatType.setId(habitatTypeId);
      fakeHabitatType.setName("Habitat " + habitatTypeId);
      return fakeHabitatType;
    });
  }

}
