/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.db.calculator.CalculationInfoRepositoryBean;
import nl.overheid.aerius.db.common.ReceptorInfoRepositoryBean;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.info.EmissionResultInfo;
import nl.overheid.aerius.shared.domain.info.ReceptorInfo;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

@Service
public class InfoService {
  private static final Logger LOG = LoggerFactory.getLogger(InfoService.class);

  private final ReceptorInfoRepositoryBean receptorInfoRepository;
  private final CalculationInfoRepositoryBean calculationInfoRepository;
  private final UiJobsService jobsService;
  private final LocaleService localeService;
  private final ReceptorUtil receptorUtil;

  @Autowired
  public InfoService(final ReceptorInfoRepositoryBean receptorInfoRepository, final CalculationInfoRepositoryBean calculationInfoRepository,
      final UiJobsService jobsService, final LocaleService localeService, final ReceptorUtil receptorUtil) {
    this.receptorInfoRepository = receptorInfoRepository;
    this.calculationInfoRepository = calculationInfoRepository;
    this.jobsService = jobsService;
    this.localeService = localeService;
    this.receptorUtil = receptorUtil;
  }

  public ReceptorInfo retrieveInfoSummary(final int receptorId, final int calculationYear, final String jobKey,
      final String[] situations) throws AeriusException {
    LOG.debug("Retrieving info summary for receptor: {}, calculationYear: {}, jobKey: {}, situations: {}",
        receptorId, calculationYear, jobKey, situations);
    final ReceptorInfo info = new ReceptorInfo();

    if (receptorId < 0) {
      return info;
    }

    final DBMessagesKey dbMessagesKey = new DBMessagesKey(ProductType.CALCULATOR, localeService.getLocale());

    info.setNaturaInfo(receptorInfoRepository.getNaturaAreaInfo(receptorId, dbMessagesKey));
    info.setHabitatTypeInfo(receptorInfoRepository.getReceptorHabitatAreaInfo(receptorId, dbMessagesKey));
    final Point receptorPoint = receptorUtil.getPointFromReceptorId(receptorId);
    info.setEmissionResultInfo(emissionResult(receptorPoint, receptorId, calculationYear, jobKey, situations));

    return info;
  }

  private EmissionResultInfo emissionResult(final Point receptorPoint, final int receptorId, final Integer calculationYear, final String jobKey,
      final String[] situations) throws AeriusException {
    final EmissionResultInfo erInfo = new EmissionResultInfo(calculationYear);
    addBackgroundInformation(erInfo, receptorPoint, receptorId, calculationYear);

    addJobInformation(erInfo, receptorId, jobKey, situations);
    return erInfo;
  }

  private void addBackgroundInformation(final EmissionResultInfo erInfo, final Point receptorPoint, final int receptorId,
      final Integer calculationYear) throws AeriusException {
    erInfo.setBackgroundEmissionResults(receptorInfoRepository.getBackgroundEmissionResult(receptorPoint, receptorId, calculationYear));
  }

  private void addJobInformation(final EmissionResultInfo erInfo, final int receptorId, final String jobKey,
      final String[] situations) throws AeriusException {
    if (jobKey != null) {
      final SituationCalculations situationCalculations = jobsService.getSituationCalculations(jobKey);

      for (final String situationId : situations) {
        final Optional<Integer> calculationId = situationCalculations.getCalculationId(situationId);

        if (!calculationId.isEmpty()) {
          addCalculationInformation(erInfo, receptorId, situationId, calculationId.get());
        }
      }
    }
  }

  private void addCalculationInformation(final EmissionResultInfo erInfo, final int receptorId, final String situationId, final int calculationId)
      throws AeriusException {
    final Map<EmissionResultKey, Double> emisssions = calculationInfoRepository.getEmissionResults(calculationId, receptorId);

    if (!emisssions.isEmpty()) {
      final EmissionResults newEmissionResults = new EmissionResults();
      for (final Entry<EmissionResultKey, Double> emission : emisssions.entrySet()) {
        newEmissionResults.add(emission.getKey(), emission.getValue());
      }
      erInfo.putEmissionResults(situationId, newEmissionResults);
    }
  }
}
