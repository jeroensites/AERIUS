/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.connectservice.api.UiApiDelegate;
import nl.overheid.aerius.connectservice.domain.FileType;
import nl.overheid.aerius.connectservice.domain.UIInfoMarkerRequest;
import nl.overheid.aerius.connectservice.domain.UiCalculationInfoResponse;
import nl.overheid.aerius.connectservice.domain.UiCalculationRequest;
import nl.overheid.aerius.connectservice.domain.UiRefreshEmissionsRequest;
import nl.overheid.aerius.connectservice.service.InfoService;
import nl.overheid.aerius.connectservice.service.ProxyFileService;
import nl.overheid.aerius.connectservice.service.ResponseService;
import nl.overheid.aerius.connectservice.service.ServerUsageService;
import nl.overheid.aerius.connectservice.service.UiCalculateService;
import nl.overheid.aerius.connectservice.service.UiJobsService;
import nl.overheid.aerius.connectservice.service.UiResultsService;
import nl.overheid.aerius.connectservice.service.UiSourcesService;
import nl.overheid.aerius.connectservice.service.UiValidateService;
import nl.overheid.aerius.shared.domain.ServerUsage;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculation;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.v2.geojson.FeatureCollection;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

import reactor.core.scheduler.Schedulers;

@Service
public class UiResource implements UiApiDelegate {

  private final ResponseService responseService;
  private final ProxyFileService proxyFileService;
  private final UiValidateService uiValidateService;
  private final UiCalculateService calculateService;
  private final UiJobsService jobsService;
  private final UiResultsService resultsService;
  private final UiSourcesService sourcesService;
  private final ServerUsageService serverUsageService;
  private final InfoService infoService;
  private final ObjectMapper objectMapper;

  @Autowired
  UiResource(final ResponseService responseService, final ProxyFileService proxyFileService, final UiValidateService uiValidateService,
      final UiCalculateService calculateService, final UiJobsService jobsService, final UiResultsService resultsService,
      final UiSourcesService sourcesService, final ServerUsageService serverUsageService, final InfoService infoService,
      final ObjectMapper objectMapper) {
    this.responseService = responseService;
    this.proxyFileService = proxyFileService;
    this.uiValidateService = uiValidateService;
    this.calculateService = calculateService;
    this.jobsService = jobsService;
    this.resultsService = resultsService;
    this.sourcesService = sourcesService;
    this.serverUsageService = serverUsageService;
    this.infoService = infoService;
    this.objectMapper = objectMapper;
  }

  @Override
  public ResponseEntity<Boolean> deleteFile(final String fileCode) {
    proxyFileService.deleteFile(fileCode);
    return responseService.toOkResponse(true);
  }

  @Override
  public ResponseEntity<String> importFile(final MultipartFile filePart, final String optImportProperties) {
    try {
      final String originalFilename = filePart.getOriginalFilename();
      final String fileCode = proxyFileService.writeRawFile(filePart);

      final ImportProperties importProperties = objectMapper.readValue(optImportProperties, ImportProperties.class);

      // Ensure a validation result with PENDING is available in the FileService
      proxyFileService.writePendingValidationFile(fileCode);

      // Start the validation on a different thread by subscribing on another scheduler.
      // This validation also splits the file into multiple ImportParcels if multiple situations are present
      uiValidateService.validateAndImportFileAsync(fileCode, originalFilename, importProperties)
          .subscribeOn(Schedulers.boundedElastic())
          .subscribe();

      // File is now (unvalidated) in the fileService. Response is the fileCode.
      return responseService.toOkResponse(fileCode);
    } catch (final IOException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> retrieveValidationResult(final String fileCode) {
    try {
      final InputStreamResource result = proxyFileService.retrieveFile(fileCode, FileType.VALIDATION, true, this::inputStream2Resource);

      return responseService.toOkResponse(result);
    } catch (final IOException | WebClientResponseException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> retrieveResult(final String fileCode) {
    final ResponseEntity<Object> fileResponse = getFileResponse(fileCode, FileType.IMPORTPARCEL);

    // Delete imported files from the fileserver.
    proxyFileService.deleteFile(fileCode);
    return fileResponse;
  }

  @Override
  public ResponseEntity<Object> retrieveRawFile(final String fileCode) {
    return getFileResponse(fileCode, FileType.RAW);
  }

  private ResponseEntity<Object> getFileResponse(final String fileCode, final FileType fileType) {
    try {
      final InputStreamResource result = proxyFileService.retrieveFile(fileCode, fileType, true, this::inputStream2Resource);

      return responseService.toOkResponse(result);
    } catch (final IOException | WebClientResponseException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  private InputStreamResource inputStream2Resource(final InputStream inputStream) {
    return new InputStreamResource(inputStream);
  }

  @Override
  public ResponseEntity<String> startCalculation(final String body) {
    try {
      final UiCalculationRequest request = objectMapper.readValue(body, UiCalculationRequest.class);
      sanityCheck(request);
      final Scenario scenario = request.getScenario();
      final String calculationKey = calculateService.calculate(scenario, request);
      return responseService.toOkResponse(calculationKey);
    } catch (final IOException | AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<String> cancelCalculation(final String jobKey) {
    try {
      final Optional<JobProgress> jobProgress = jobsService.getJob(jobKey);
      if (jobProgress.isEmpty()) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No job found with this key");
      } else {
        jobsService.cancelJob(jobKey);
        return responseService.toOkResponse(jobKey);
      }
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Void> deleteCalculation(final String jobKey) {
    try {
      jobsService.deleteJob(jobKey);
      return responseService.toOkResponse();
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> retrieveCalculationInfo(final String jobKey) {
    try {
      final Optional<JobProgress> jobProgress = jobsService.getJob(jobKey);
      if (jobProgress.isEmpty()) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No job found with this key");
      } else {
        final ServerUsage serverUsage = serverUsageService.getCurrentServerUsage();
        final List<SituationCalculation> situationCalculations = jobsService.getSituationCalculations(jobKey);
        return responseService.toOkResponse(new UiCalculationInfoResponse(jobProgress.get(), serverUsage, situationCalculations));
      }
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> retrieveSituationResultsSummary(final String jobKey, final String situationId, final String resultType,
      final String summaryHexagonType, final String optEmissionResultKey) {
    try {
      final SituationCalculations situationCalculations = jobsService.getSituationCalculations(jobKey);
      final Optional<Integer> calculationId = situationCalculations.getCalculationId(situationId);
      if (calculationId.isEmpty()) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No job/situation combination found with supplied values");
      } else {
        final int jobId = jobsService.getJobId(jobKey);
        final ScenarioResultType resultTypeType = Optional.ofNullable(resultType).map(ScenarioResultType::valueOf)
            .orElse(ScenarioResultType.SITUATION_RESULT);
        final SummaryHexagonType hexagonType = Optional.ofNullable(summaryHexagonType).map(SummaryHexagonType::valueOf)
            .orElse(SummaryHexagonType.RELEVANT_HEXAGONS);
        final EmissionResultKey emissionResultKey = Optional.ofNullable(optEmissionResultKey).map(EmissionResultKey::valueOf)
            .orElse(EmissionResultKey.NOXNH3_DEPOSITION);
        final SituationResultsSummary result = resultsService
            .determineSituationResultsSummary(situationCalculations, resultTypeType, jobId, calculationId.get(), hexagonType, emissionResultKey);
        return responseService.toOkResponse(result);
      }
    } catch (final AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> retrieveInfoSummary(final String body) {
    try {
      final UIInfoMarkerRequest infoMarker = objectMapper.readValue(body, UIInfoMarkerRequest.class);
      final Object result = infoService.retrieveInfoSummary(
          infoMarker.getReceptorId(),
          infoMarker.getCalculationYear(),
          infoMarker.getJobKey(),
          infoMarker.getSituations());
      return responseService.toOkResponse(result);
    } catch (final IOException | AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  private void sanityCheck(final UiCalculationRequest request) throws AeriusException {
    if (request == null) {
      // not sure if this can happen, but better check for it.
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    final Scenario scenario = request.getScenario();
    if (scenario == null || scenario.getSituations() == null || scenario.getSituations().isEmpty()) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_NO_SOURCES);
    }
    if (request.getTheme() == null) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_CALCULATIONTYPE_SUPPLIED_NOT_SUPPORTED);
    }
  }

  @Override
  public ResponseEntity<Object> refreshEmissions(final String body) {
    try {
      final UiRefreshEmissionsRequest request = objectMapper.readValue(body, UiRefreshEmissionsRequest.class);
      final FeatureCollection<EmissionSourceFeature> sources = request.getSources();
      sourcesService.validateSources(sources.getFeatures());
      sourcesService.refreshEmissions(sources.getFeatures(), request.getYear());
      return responseService.toOkResponse(sources);
    } catch (final IOException | AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

  @Override
  public ResponseEntity<Object> suggestWaterway(final String body) {
    try {
      final Geometry geometry = objectMapper.readValue(body, Geometry.class);
      final List<InlandWaterway> waterways = sourcesService.suggestInlandShippingWaterway(geometry);
      return responseService.toOkResponse(waterways);
    } catch (final IOException | AeriusException e) {
      throw responseService.toResponseStatusException(e);
    }
  }

}
