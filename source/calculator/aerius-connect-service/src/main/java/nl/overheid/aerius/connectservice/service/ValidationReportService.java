/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import nl.overheid.aerius.connectservice.domain.ImportParcelAggregate;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.OSUtils;

@Service
public class ValidationReportService {

  private static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  // Convenience local constant.
  private static final String SNL = OSUtils.LNL;
  private static final String DNL = SNL + SNL;

  private final MessagesService messagesService;

  public ValidationReportService(final MessagesService messagesService) {
    this.messagesService = messagesService;
  }

  public void report(final ImportParcelAggregate aggregate, final LocalDateTime generationTime, final OutputStream outputStream) throws IOException {
    try (final Writer writer = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8))) {
      formatTitleSection(writer, generationTime);
      formatExceptionText(writer, "fouten", aggregate.getExceptions());
      formatExceptionText(writer, "waarschuwingen", aggregate.getWarnings());
    }
  }

  private void formatTitleSection(final Writer writer, final LocalDateTime generationTime) throws IOException {
    writer.append("# AERIUS Validatie");
    writer.append(SNL);
    writer.append("Gegenereerd op: ");
    writer.append(generationTime.format(DATETIME_FORMATTER));
    writer.append('.');
    writer.append(SNL);
  }

  private void formatExceptionText(final Writer writer, final String title, final List<AeriusException> exceptions) throws IOException {
    if (!exceptions.isEmpty()) {
      writer.append(SNL);
      writer.append("## ");
      writer.append(title);
      writer.append(DNL);
      writer.append(exceptions.stream()
          .map(messagesService::getExceptionMessage)
          .collect(Collectors.joining(DNL)));
      writer.append(SNL);
    }
  }

}
