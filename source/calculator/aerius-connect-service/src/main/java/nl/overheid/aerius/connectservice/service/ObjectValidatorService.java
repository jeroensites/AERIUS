/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

/**
 * Helper service to handle validation of objects in multipart/form-data requests.
 * Shouldn't be needed, but current openapi-generator doesn't generate the @Valid annotation on other objects for multipart/form-data requests.
 */
@Service
public class ObjectValidatorService {

  private final Validator validator;

  public ObjectValidatorService(final Validator validator) {
    this.validator = validator;
  }

  public <C> void validateAll(final Collection<C> objects) {
    objects.forEach(this::validate);
  }

  public <C> void validate(final C object) {
    if (object != null) {
      final Set<ConstraintViolation<C>> violations = validator.validate(object);
      if (!violations.isEmpty()) {
        final String messages = violations.stream()
            .map(this::toMessage)
            .collect(Collectors.joining(". "));
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, messages);
      }
    }
  }

  private <C> String toMessage(final ConstraintViolation<C> violation) {
    return violation.getRootBeanClass().getSimpleName() + "." + violation.getPropertyPath() + ": " + violation.getMessage();
  }

}
