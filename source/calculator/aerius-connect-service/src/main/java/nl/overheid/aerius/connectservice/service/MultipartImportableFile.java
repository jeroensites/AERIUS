/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.io.ImportableFile;

/**
 *
 */
public class MultipartImportableFile implements ImportableFile {

  private final MultipartFile multipartFile;

  public MultipartImportableFile(final MultipartFile multipartFile) {
    this.multipartFile = multipartFile;
  }

  @Override
  public String getFileName() {
    return multipartFile.getOriginalFilename();
  }

  @Override
  public InputStream asInputStream() throws IOException {
    return multipartFile.getInputStream();
  }

  MultipartFile getMultipartFile() {
    return multipartFile;
  }

}
