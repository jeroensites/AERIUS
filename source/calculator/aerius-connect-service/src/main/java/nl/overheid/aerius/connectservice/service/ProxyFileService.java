/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.connectservice.config.FileServiceProperties;
import nl.overheid.aerius.connectservice.domain.FileType;
import nl.overheid.aerius.shared.InternalRequestMappings;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.validation.FileValidationStatus;
import nl.overheid.aerius.shared.domain.validation.ValidationStatus;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

import reactor.core.publisher.Mono;

@Service
public class ProxyFileService {

  private static final Logger LOG = LoggerFactory.getLogger(ProxyFileService.class);
  private static final Duration WEBCLIENT_TIMEOUT = Duration.ofMinutes(1);

  private final WebClient webClient;
  private final ObjectMapper objectMapper;

  @Autowired
  public ProxyFileService(final WebClient.Builder webClientBuilder, final ObjectMapper objectMapper, final FileServiceProperties properties) {
    this.webClient = webClientBuilder.baseUrl(properties.getBaseUrl()).build();
    this.objectMapper = objectMapper;
  }

  public String writeRawFile(final MultipartFile filePart) {
    return webClient.post()
        .uri(InternalRequestMappings.FILE_ADD)
        .contentType(MediaType.MULTIPART_FORM_DATA)
        .body(BodyInserters.fromMultipartData(fromFile(filePart)))
        .retrieve()
        .bodyToMono(String.class)
        .block(WEBCLIENT_TIMEOUT);
  }

  public String writeEmptyRawFile(final String filename) {
    return webClient.post()
        .uri(InternalRequestMappings.EMPTY_FILE_ADD)
        .body(BodyInserters.fromFormData("filename", filename))
        .retrieve()
        .bodyToMono(String.class)
        .block(WEBCLIENT_TIMEOUT);
  }

  private MultiValueMap<String, ?> fromFile(final MultipartFile filePart) {
    final MultipartBodyBuilder builder = new MultipartBodyBuilder();
    builder.part(RequestMappings.FILE, filePart.getResource());

    return builder.build();
  }

  public <T> T retrieveFile(final String fileCode, final FileType fileType, final boolean forPublicUse, final MapInputStreamFunction<T> function)
      throws IOException, WebClientResponseException {
    final String uri = getUriForFileType(fileCode, fileType, forPublicUse);
    final Mono<DataBuffer> dataBufferMono = webClient.get()
        .uri(uri)
        .retrieve()
        .onStatus(HttpStatus.NOT_FOUND::equals, clientResponse -> Mono.just(new ResponseStatusException(HttpStatus.NOT_FOUND)))
        .bodyToMono(DataBuffer.class);
    final DataBuffer dataBuffer = dataBufferMono.block(WEBCLIENT_TIMEOUT);

    if (dataBuffer == null) {
      throw new IOException("File not found or doesn't contain any data");
    }
    return function.apply(dataBuffer.asInputStream(true));
  }

  public String write(final Object object) {
    return write(object, false);
  }

  public String write(final Object object, final boolean publiclyAccessible) {
    return Mono.fromCallable(() -> objectMapper.writeValueAsBytes(object))
        .map(scenarioBytes -> new FilenameAwareByteArrayResource(scenarioBytes, "file"))
        .flatMap(resource -> {
          final BodyInserters.MultipartInserter body = BodyInserters.fromMultipartData("file", resource);
          body.with(InternalRequestMappings.FILE_PUBLIC_PARAMETER, publiclyAccessible);

          return webClient.post()
              .uri(InternalRequestMappings.FILE_ADD)
              .body(body)
              .retrieve()
              .bodyToMono(String.class)
              .onErrorMap(e -> new AeriusException(AeriusExceptionReason.INTERNAL_ERROR));
        })
        .block(WEBCLIENT_TIMEOUT);
  }

  public void write(final String fileCode, final byte[] fileContent, final FileType fileType) {
    final String uri = getUriForFileType(fileCode, fileType, false);
    final MultipartBodyBuilder builder = new MultipartBodyBuilder();
    final Resource resource = new ByteArrayResource(fileContent) {

      @Override
      public String getFilename() {
        // While it does not matter what the file name is, some value is required to generate a correct Multipart/Form-data request.
        return fileCode;
      }

    };
    builder.part("file", resource);

    webClient.post()
        .uri(uri)
        .contentType(MediaType.MULTIPART_FORM_DATA)
        .body(BodyInserters.fromMultipartData(builder.build()))
        .retrieve()
        .bodyToMono(Boolean.class)
        .block(WEBCLIENT_TIMEOUT);
  }

  public void writePendingValidationFile(final String fileCode) {
    final FileValidationStatus status = new FileValidationStatus(fileCode, ValidationStatus.PENDING, "", SituationType.UNKNOWN,
        List.of(), null);
    try {
      write(fileCode, objectMapper.writeValueAsBytes(status), FileType.VALIDATION);
    } catch (final IOException e) {
      LOG.error("Error while writing pending validation file", e);
    }
  }

  public void deleteFile(final String fileCode) {
    final String uri = getDeleteUri(fileCode);

    webClient.delete()
        .uri(uri)
        .retrieve()
        .onStatus(httpStatus -> HttpStatus.NOT_FOUND.equals(httpStatus),
            clientResponse -> Mono.empty())
        .bodyToMono(Void.class)
        .block(WEBCLIENT_TIMEOUT);
  }

  private String getUriForFileType(final String fileCode, final FileType fileType, final boolean forPublicUse) {
    switch (fileType) {
    case VALIDATION:
      return InternalRequestMappings.FILE_RETRIEVE_VALIDATION.replace(RequestMappings.FILE_CODE, fileCode);
    case IMPORTPARCEL:
      return InternalRequestMappings.FILE_RETRIEVE_IMPORTPARCEL.replace(RequestMappings.FILE_CODE, fileCode);
    case RAW:
      if (forPublicUse) {
        return InternalRequestMappings.FILE_RETRIEVE_RAW_PUBLIC.replace(RequestMappings.FILE_CODE, fileCode);
      }
      return InternalRequestMappings.FILE_RETRIEVE_RAW.replace(RequestMappings.FILE_CODE, fileCode);
    default:
      throw new IllegalArgumentException("FileType unsupported: " + fileType);
    }
  }

  private String getDeleteUri(final String fileCode) {
    return InternalRequestMappings.FILE_DELETE.replace(RequestMappings.FILE_CODE, fileCode);
  }

  public interface MapInputStreamFunction<T> {
    T apply(InputStream inputStream) throws IOException;
  }

  public static class FilenameAwareByteArrayResource extends ByteArrayResource {
    private final String filename;

    public FilenameAwareByteArrayResource(final byte[] bytes, final String filename) {
      super(bytes);
      this.filename = filename;
    }

    @Override
    public String getFilename() {
      return filename;
    }
  }
}
