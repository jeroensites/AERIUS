/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.io.IOException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.QueueEnum;

/**
 * Abstract base class for CalculateService implementations.
 * @param <T>
 */
abstract class AbstractCalculateService<T> implements CalculateService<T> {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractCalculateService.class);

  protected final ConstantRepositoryBean constantRepository;
  private final ExportTaskClientBean exportTaskclient;
  private final JobRepositoryBean jobRepository;
  private final ConnectCalculationPointSetsRepositoryBean pointSetsRepository;
  private final ProxyFileService fileService;
  private final LocaleService localeService;

  AbstractCalculateService(final ExportTaskClientBean exportTaskclient, final JobRepositoryBean jobRepository,
      final ConnectCalculationPointSetsRepositoryBean pointSetsRepository, final ConstantRepositoryBean constantRepository,
      final ProxyFileService fileService, final LocaleService localeService) {
    this.exportTaskclient = exportTaskclient;
    this.jobRepository = jobRepository;
    this.pointSetsRepository = pointSetsRepository;
    this.constantRepository = constantRepository;
    this.fileService = fileService;
    this.localeService = localeService;
  }

  public String calculate(final Scenario scenario, final T options) throws AeriusException {
    return calculate(Optional.empty(), scenario, options);
  }

  @Override
  public String calculate(final ConnectUser connectUser, final Scenario scenario, final T options) throws AeriusException {
    return calculate(Optional.of(connectUser), scenario, options);
  }

  private String calculate(final Optional<ConnectUser> connectUser, final Scenario scenario, final T options) throws AeriusException {
    final ExportProperties exportProperties = toExportProperties(connectUser, options);

    updateScenario(scenario, options);
    updateScenarioOptions(scenario, options);
    updateCustomPoints(scenario, connectUser, options);
    validate(scenario, options);

    // Write scenario to fileservice for use in the Print UI.
    if (exportProperties.getExportType() == ExportType.PAA) {
      final String fileIdentifier = fileService.write(scenario, true);
      exportProperties.setScenarioFileIdentifier(fileIdentifier);
    }

    final String jobKey = createJob(connectUser, options);
    sendTask(jobKey, exportProperties, scenario, options);
    return jobKey;
  }

  private String createJob(final Optional<ConnectUser> connectUser, final T options) throws AeriusException {
    final String jobKey;
    if (connectUser.isPresent()) {
      jobKey = jobRepository.createJob(connectUser.get(), getJobType(options), getJobName(options));
    } else {
      jobKey = jobRepository.createJob(getJobType(options));
    }
    return jobKey;
  }

  protected abstract JobType getJobType(T options);

  protected abstract Optional<String> getJobName(T options);

  private void updateCustomPoints(final Scenario scenario, final Optional<ConnectUser> connectUser, final T options) throws AeriusException {
    // Default for analysis: custom points
    if (useCustomPoints(options)) {
      final String receptorSetName = getReceptorSetName(options);

      if (connectUser.isPresent() && receptorSetName != null && !receptorSetName.isBlank()) {
        if (pointSetsRepository.getCalculationPointSetByName(connectUser.get(), receptorSetName) == null) {
          throw new AeriusException(AeriusExceptionReason.CONNECT_USER_CALCULATION_POINT_SET_DOES_NOT_EXIST, receptorSetName);
        }
        scenario.getCustomPointsList().clear();
        scenario.getCustomPointsList().addAll(pointSetsRepository.getCalculationPointsByName(connectUser.get(), receptorSetName));
      }
      if (scenario.getCustomPointsList().isEmpty()) {
        throw new AeriusException(AeriusExceptionReason.CONNECT_NO_CUSTOM_POINTS);
      }
    } else {
      scenario.getCustomPointsList().clear();
    }
  }

  protected abstract boolean useCustomPoints(final T options);

  protected abstract String getReceptorSetName(final T options);

  /**
   * Optional return the calculation year if the year is passed as parameter in the options.
   *
   * @param options options containing optional the calculation year
   * @return
   */
  protected abstract Optional<Integer> getOverrideYear(final T options);

  /**
   * Perform additional validations of the input options, in combination with the scenario.
   *
   * @param scenario scenario
   * @param options options to check
   * @throws AeriusException throws an exception if validation of options fails.
   */
  protected abstract void validateBeforeSending(final Scenario scenario, final T options) throws AeriusException;

  /**
   * Set the input options in the scenario.
   *
   * @param scenario scenario to set the options in
   * @param options input options to set in the scenario
   * @throws AeriusException
   */
  protected abstract void updateScenarioOptions(final Scenario scenario, final T options) throws AeriusException;

  protected void updateScenario(final Scenario scenario, final T options) {
    final Optional<Integer> overrideYear = getOverrideYear(options);
    if (overrideYear.isPresent()) {
      scenario.getSituations().forEach(situation -> situation.setYear(overrideYear.get()));
    }
  }

  private void validate(final Scenario scenario, final T options) throws AeriusException {
    for (final ScenarioSituation situation : scenario.getSituations()) {
      validateYear(situation);
    }
    validateBeforeSending(scenario, options);
  }

  private void validateYear(final ScenarioSituation situation) throws AeriusException {
    final int minYear = constantRepository.getInteger(ConstantsEnum.MIN_YEAR);
    final int maxYear = constantRepository.getInteger(ConstantsEnum.MAX_YEAR);
    final int situationYear = situation.getYear();
    if (situationYear < minYear || situationYear > maxYear) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_INCORRECT_CALCULATIONYEAR,
          String.valueOf(situationYear), String.valueOf(minYear), String.valueOf(maxYear));
    }
  }

  private ExportProperties toExportProperties(final Optional<ConnectUser> user, final T options) {
    final ExportProperties exportProperties = new ExportProperties();
    setExportProperties(exportProperties, options);
    // Always keep results for Connect.. initially. They will be cleaned when necessary by the JobCleanupWorker.
    exportProperties.getAdditionalOptions().add(ExportAdditionalOptions.TRACK_JOB_PROGRESS);
    exportProperties.setLocale(localeService.getLocale().getLanguage());
    if (shouldNotSendEmail(options)) {
      exportProperties.getAdditionalOptions().remove(ExportAdditionalOptions.EMAIL_USER);
    } else {
      user.ifPresent(connectUser -> exportProperties.setEmailAddress(connectUser.getEmailAddress()));
    }
    return exportProperties;
  }

  protected abstract void setExportProperties(final ExportProperties exportProperties, final T options);

  protected abstract boolean shouldNotSendEmail(T options);

  private void sendTask(final String jobKey, final ExportProperties exportProperties, final Scenario scenario, final T options)
      throws AeriusException {
    final QueueEnum queue = determineQueue(options);
    try {
      exportTaskclient.startExport(queue, exportProperties, scenario, jobKey);
    } catch (final IOException e) {
      LOG.error("Error when starting export", e);
      throw new AeriusException(AeriusExceptionReason.IO_EXCEPTION_UNKNOWN);
    }
  }

  protected QueueEnum determineQueue(final T options) {
    return QueueEnum.CONNECT_GML_EXPORT;
  }

}
