/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.ShippingRepositoryBean;
import nl.overheid.aerius.db.common.sector.SectorRepositoryBean;
import nl.overheid.aerius.emissions.CategoryBasedEmissionFactorSupplier;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.shared.emissions.EmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.EmissionsUpdater;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.GeometryCalculator;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.validation.CategoryBasedValidationHelper;
import nl.overheid.aerius.validation.ValidationHelper;
import nl.overheid.aerius.validation.ValidationVisitor;

@Service
public class UiSourcesService {

  private final PMF pmf;
  private final SectorCategories categories;
  private final ShippingRepositoryBean shippingRepository;
  private final GeometryCalculator geometryCalculator;

  @Autowired
  public UiSourcesService(final PMF pmf, final SectorRepositoryBean sectorRepository, final ShippingRepositoryBean shippingRepository,
      final GeometryCalculator geometryCalculator) throws AeriusException {
    this.pmf = pmf;
    // Cache the categories instead of retrieving them for every operation
    // Locale should not matter either, since (at the moment) it is only used for determining emissions.
    this.categories = sectorRepository.getSectorCategories(LocaleUtils.getDefaultLocale());
    this.shippingRepository = shippingRepository;
    this.geometryCalculator = geometryCalculator;
  }

  public void validateSource(final EmissionSourceFeature source) throws AeriusException {
    validateSources(List.of(source));
  }

  public void validateSources(final List<EmissionSourceFeature> sources) throws AeriusException {
    final List<AeriusException> errors = new ArrayList<>();
    final ValidationHelper validationHelper = new CategoryBasedValidationHelper(categories);
    final ValidationVisitor sourceValidator = new ValidationVisitor(errors, new ArrayList<>(), validationHelper);
    for (final EmissionSourceFeature source : sources) {
      source.accept(sourceValidator);
      // As soon as one error is found, throw that one.
      if (!errors.isEmpty()) {
        throw errors.get(0);
      }
    }
  }

  public void refreshEmissions(final List<EmissionSourceFeature> sources, final int year) throws AeriusException {
    final EmissionFactorSupplier supplier = new CategoryBasedEmissionFactorSupplier(categories, pmf, year);
    final EmissionsUpdater emissionsUpdater = new EmissionsUpdater(supplier, geometryCalculator);
    for (final EmissionSourceFeature source : sources) {
      emissionsUpdater.updateEmissions(source);
    }
  }

  public List<InlandWaterway> suggestInlandShippingWaterway(final Geometry geometry) throws AeriusException {
    return shippingRepository.suggestInlandShippingWaterway(geometry);
  }

}
