/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.connectservice.service.LocaleService;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Extra configuration for Spring MVC.
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

  @Autowired private ObjectMapper objectMapper;

  @Override
  public void extendMessageConverters(final List<HttpMessageConverter<?>> converters) {
    // Add multipart handler for (default) content type application/octet-stream
    final MultipartFormDataEndpointConverter converter = new MultipartFormDataEndpointConverter(objectMapper);
    final List<MediaType> supportedMediaTypes = new ArrayList<>();
    supportedMediaTypes.addAll(converter.getSupportedMediaTypes());
    supportedMediaTypes.add(MediaType.APPLICATION_OCTET_STREAM);
    converter.setSupportedMediaTypes(supportedMediaTypes);
    // Add it as last, to ensure it won't intervene with cases where it was already working.
    converters.add(converter);
  }

  @Bean
  public LocaleResolver localeResolver(final LocaleService localeService) {
    final AcceptHeaderLocaleResolver localeResolver = new AcceptHeaderLocaleResolver();
    final Locale defaultLocale = localeService.getDefaultLocale();
    localeResolver.setDefaultLocale(defaultLocale);
    LocaleContextHolder.setDefaultLocale(defaultLocale);
    localeResolver.setSupportedLocales(LocaleUtils.getLocales());
    return localeResolver;
  }

}
