/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.connectservice.model.JobStatus;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@Service
public class JobsService {

  private final JobRepositoryBean jobRepository;

  @Autowired
  public JobsService(final JobRepositoryBean jobRepository) {
    this.jobRepository = jobRepository;
  }

  public Optional<JobStatus> getJob(final ConnectUser user, final String jobKey) throws AeriusException {
    return jobRepository.getProgressForUserAndKey(user, jobKey).map(this::convert);
  }

  public List<JobStatus> getAllJobs(final ConnectUser user) throws AeriusException {
    return jobRepository.getProgressForUser(user).stream()
        .map(this::convert)
        .collect(Collectors.toList());
  }

  private JobStatus convert(final JobProgress progress) {
    return new JobStatus()
        .jobKey(progress.getKey())
        .name(progress.getName())
        .status(progress.getState().name())
        .hectareCalculated(progress.getHexagonCount())
        .startDateTime(convert(progress.getStartDateTime()))
        .endDateTime(convert(progress.getEndDateTime()))
        .resultUrl(progress.getResultUrl())
        .errorMessage(progress.getErrorMessage());
  }

  private OffsetDateTime convert(final Date date) {
    if (date == null) {
      return null;
    } else {
      return date.toInstant().atOffset(ZoneOffset.UTC);
    }
  }

  public void cancelJob(final ConnectUser user, final String jobKey) throws AeriusException {
    if (jobRepository.isJobFromUser(user, jobKey)) {
      jobRepository.cancelJob(jobKey);
    } else {
      throw new AeriusException(AeriusExceptionReason.CONNECT_USER_JOBKEY_DOES_NOT_EXIST, jobKey);
    }
  }

  public void deleteJob(final ConnectUser user, final String jobKey) throws AeriusException {
    if (jobRepository.isJobFromUser(user, jobKey)) {
      jobRepository.deleteJob(jobKey);
    } else {
      throw new AeriusException(AeriusExceptionReason.CONNECT_USER_JOBKEY_DOES_NOT_EXIST, jobKey);
    }
  }

}
