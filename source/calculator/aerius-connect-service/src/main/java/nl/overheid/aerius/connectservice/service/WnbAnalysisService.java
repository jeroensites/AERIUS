/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.connectservice.model.ResultType;
import nl.overheid.aerius.connectservice.model.WnbAnalysisOptions;
import nl.overheid.aerius.connectservice.model.WnbAnalysisOptions.CalculationPointsTypeEnum;
import nl.overheid.aerius.connectservice.model.WnbAnalysisOptions.OutputTypeEnum;
import nl.overheid.aerius.connectservice.model.WnbAnalysisOptions.RoadOPSEnum;
import nl.overheid.aerius.connectservice.model.WnbAnalysisOutputOptions;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.common.MeteoRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationRoadOPS;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

@Service
public class WnbAnalysisService extends AbstractCalculateService<WnbAnalysisOptions> {

  private final MeteoRepositoryBean meteoRepository;

  @Autowired
  WnbAnalysisService(final ExportTaskClientBean exportTaskclient, final JobRepositoryBean jobRepository,
      final ConnectCalculationPointSetsRepositoryBean pointSetsRepository, final ConstantRepositoryBean constantRepository,
      final ProxyFileService fileService, final MeteoRepositoryBean meteoRepository, final LocaleService localeService) {
    super(exportTaskclient, jobRepository, pointSetsRepository, constantRepository, fileService, localeService);
    this.meteoRepository = meteoRepository;
  }

  @Override
  public String calculate(final Scenario scenario, final WnbAnalysisOptions options) throws AeriusException {
    // Should not call this specific method for analysis
    throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
  }

  public String analyseWnb(final ConnectUser connectUser, final Scenario scenario, final WnbAnalysisOptions options)
      throws AeriusException {
    return calculate(connectUser, scenario, options);
  }

  @Override
  protected Optional<String> getJobName(final WnbAnalysisOptions options) {
    return Optional.ofNullable(options.getName());
  }

  @Override
  protected JobType getJobType(final WnbAnalysisOptions options) {
    return JobType.CALCULATION;
  }

  @Override
  protected boolean useCustomPoints(final WnbAnalysisOptions options) {
    // Default for analysis is using custom point
    return options.getCalculationPointsType() == null || options.getCalculationPointsType() == CalculationPointsTypeEnum.CUSTOM_POINTS;
  }

  @Override
  protected String getReceptorSetName(final WnbAnalysisOptions options) {
    return options.getReceptorSetName();
  }

  @Override
  protected Optional<Integer> getOverrideYear(final WnbAnalysisOptions options) {
    return Optional.ofNullable(options.getCalculationYear());
  }

  @Override
  protected void validateBeforeSending(final Scenario scenario, final WnbAnalysisOptions options) throws AeriusException {
    validateCustomPointHeights(scenario, options);
  }

  @Override
  protected void updateScenarioOptions(final Scenario scenario, final WnbAnalysisOptions options) throws AeriusException {
    final CalculationSetOptions scenarioOptions = scenario.getOptions();
    final CalculationType calculationType = useCustomPoints(options) ? CalculationType.CUSTOM_POINTS : CalculationType.PERMIT;
    scenarioOptions.setCalculationType(calculationType);
    scenarioOptions.setUseWnbMaxDistance(options.getWithWNBMaxDistance() == null || options.getWithWNBMaxDistance().booleanValue());
    scenarioOptions.setRoadOPS(toDomain(options.getRoadOPS()));
    scenarioOptions.setForceAggregation(Boolean.TRUE.equals(options.getAggregate()));
    scenarioOptions.setStacking(Boolean.TRUE.equals(options.getStacking()));
    scenarioOptions.setMeteo(toMeteo(options.getMeteoYear()));
    scenarioOptions.setUseReceptorHeights(Boolean.TRUE.equals(options.getUseReceptorHeight()));

    updateOPSOptions(scenarioOptions, options);
    updateOptionsSubstance(scenarioOptions, options.getOutputOptions());
  }

  @Override
  protected void setExportProperties(final ExportProperties exportProperties, final WnbAnalysisOptions options) {
    final ExportType exportType = determineExportType(options.getOutputType(), options.getOutputOptions());
    exportProperties.setName(options.getName());
    exportProperties.setExportType(exportType);
  }

  @Override
  protected boolean shouldNotSendEmail(final WnbAnalysisOptions options) {
    return !Boolean.TRUE.equals(options.getSendEmail());
  }

  private void updateOPSOptions(final CalculationSetOptions scenarioOptions, final WnbAnalysisOptions options) {
    if (options.getOpsOptions() != null) {
      scenarioOptions.setOpsOptions(new OPSOptions());
      scenarioOptions.getOpsOptions().setRawInput(Boolean.TRUE.equals(options.getOpsOptions().getRawInput()));
      scenarioOptions.getOpsOptions().setYear(options.getOpsOptions().getYear());
      scenarioOptions.getOpsOptions().setCompCode(options.getOpsOptions().getCompCode());
      scenarioOptions.getOpsOptions().setMolWeight(options.getOpsOptions().getMolWeight());
      scenarioOptions.getOpsOptions().setPhase(options.getOpsOptions().getPhase());
      scenarioOptions.getOpsOptions().setLoss(options.getOpsOptions().getLoss());
      scenarioOptions.getOpsOptions().setDiffCoeff(options.getOpsOptions().getDiffCoeff());
      scenarioOptions.getOpsOptions().setWashout(options.getOpsOptions().getWashout());
      scenarioOptions.getOpsOptions().setConvRate(options.getOpsOptions().getConvRate());
      scenarioOptions.getOpsOptions().setRoughness(options.getOpsOptions().getRoughness());
    }
  }

  private CalculationRoadOPS toDomain(final RoadOPSEnum connectValue) {
    CalculationRoadOPS domainValue = CalculationRoadOPS.DEFAULT;
    if (connectValue != null) {
      switch (connectValue) {
      case OPS_ROAD:
        domainValue = CalculationRoadOPS.OPS_ROAD;
        break;
      case OPS_ALL:
        domainValue = CalculationRoadOPS.OPS_ALL;
        break;
      default:
        domainValue = CalculationRoadOPS.DEFAULT;
        break;
      }
    }
    return domainValue;
  }

  private Meteo toMeteo(final String meteoYear) throws AeriusException {
    final Meteo meteo;
    // Only apply if it is a custom points calculation
    if (!StringUtils.isEmpty(meteoYear)) {
      final Map<String, Meteo> supportedMeteoOptions = meteoRepository.getMeteoDefinitions();
      meteo = supportedMeteoOptions.get(meteoYear);
      if (meteo == null) {
        throw new AeriusException(AeriusExceptionReason.CONNECT_INVALID_METEO, meteoYear);
      }
    } else {
      meteo = null;
    }
    return meteo;
  }

  private void updateOptionsSubstance(final CalculationSetOptions scenarioOptions, final WnbAnalysisOutputOptions outputOptions) {
    scenarioOptions.getSubstances().add(Substance.NH3);
    scenarioOptions.getSubstances().add(Substance.NOX);
    scenarioOptions.getSubstances().add(Substance.NO2);

    if (outputOptions != null && outputOptions.getResultTypes() != null) {
      for (final ResultType resultType : outputOptions.getResultTypes()) {
        scenarioOptions.getEmissionResultKeys().addAll(EmissionResultKey.getEmissionResultKeys(
            scenarioOptions.getSubstances(), toDomain(resultType)));
      }
    } else {
      scenarioOptions.getEmissionResultKeys().add(EmissionResultKey.NH3_DEPOSITION);
      scenarioOptions.getEmissionResultKeys().add(EmissionResultKey.NOX_DEPOSITION);
    }
  }

  private EmissionResultType toDomain(final ResultType connectValue) {
    EmissionResultType domainValue = null;
    if (connectValue != null) {
      switch (connectValue) {
      case CONCENTRATION:
        domainValue = EmissionResultType.CONCENTRATION;
        break;
      case DEPOSITION:
        domainValue = EmissionResultType.DEPOSITION;
        break;
      case DRY_DEPOSITION:
        domainValue = EmissionResultType.DRY_DEPOSITION;
        break;
      case WET_DEPOSITION:
        domainValue = EmissionResultType.WET_DEPOSITION;
        break;
      default:
        throw new IllegalArgumentException("Can't convert connect result type '" + connectValue + "' to a AERIUS result type");
      }
    }
    return domainValue;
  }

  private ExportType determineExportType(final OutputTypeEnum outputType, final WnbAnalysisOutputOptions outputOptions) {
    final ExportType exportType;
    if (outputType == OutputTypeEnum.CSV) {
      exportType = ExportType.CSV;
    } else {
      // Even though the user is required to supply the output type, we can use GML as default
      exportType = includeSectorResults(outputOptions)
          ? ExportType.GML_WITH_SECTORS_RESULTS
          : ExportType.GML_WITH_RESULTS;
    }
    return exportType;
  }

  private boolean includeSectorResults(final WnbAnalysisOutputOptions outputOptions) {
    return outputOptions != null
        && outputOptions.getSectorOutput() != null
        && outputOptions.getSectorOutput().booleanValue();
  }

  private void validateCustomPointHeights(final Scenario scenario, final WnbAnalysisOptions options) throws AeriusException {
    if (options.getUseReceptorHeight() != null && options.getUseReceptorHeight()) {
      validateResultTypesForHeightCalculation(options);
      validateCalculationPointsForHeightCalculation(scenario);
    }
  }

  private void validateResultTypesForHeightCalculation(final WnbAnalysisOptions options) throws AeriusException {
    for (final ResultType resultType : options.getOutputOptions().getResultTypes()) {
      if (resultType != ResultType.CONCENTRATION) {
        throw new AeriusException(AeriusExceptionReason.CONNECT_INVALID_OUTPUTTYPE, resultType.toString());
      }
    }
  }

  private void validateCalculationPointsForHeightCalculation(final Scenario scenario) throws AeriusException {
    if (scenario.getCustomPointsList().stream().anyMatch(this::hasNoHeight)) {
      throw new AeriusException(AeriusExceptionReason.CONNECT_MISSING_RECEPTOR_HEIGHT);
    }
  }

  private boolean hasNoHeight(final CalculationPointFeature pointFeature) {
    boolean noHeight = true;
    if (pointFeature.getProperties() instanceof CustomCalculationPoint) {
      final CustomCalculationPoint customPoint = (CustomCalculationPoint) pointFeature.getProperties();
      noHeight = customPoint.getHeight() == null;
    }
    return noHeight;
  }
}
