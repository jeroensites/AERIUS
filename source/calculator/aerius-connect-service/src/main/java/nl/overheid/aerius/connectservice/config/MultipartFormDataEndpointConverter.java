/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.config;

import java.lang.reflect.Type;

import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Converter to handle multipart/form-data request parts as json.
 *
 * This converter can be used to handle parts that supplied no content-type
 * (in which case the content-type will be assumed by Spring to be application/octet-stream).
 */
public class MultipartFormDataEndpointConverter extends MappingJackson2HttpMessageConverter {

  public MultipartFormDataEndpointConverter(final ObjectMapper objectMapper) {
      super(objectMapper);
  }

  @Override
  public boolean canRead(final Type type, final Class<?> contextClass, final MediaType mediaType) {
      // When a rest client(e.g. RestTemplate#getForObject) reads a request, 'RequestAttributes' can be null.
      final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
      if (requestAttributes == null) {
          return false;
      }
      final HandlerMethod handlerMethod = (HandlerMethod) requestAttributes
              .getAttribute(HandlerMapping.BEST_MATCHING_HANDLER_ATTRIBUTE, RequestAttributes.SCOPE_REQUEST);
      if (handlerMethod == null) {
          return false;
      }
      final RequestMapping requestMapping = handlerMethod.getMethodAnnotation(RequestMapping.class);
      if (requestMapping == null) {
          return false;
      }
      // This converter reads data only when the mapped controller method consumes just 'MediaType.MULTIPART_FORM_DATA_VALUE'.
      if (requestMapping.consumes().length != 1
              || !MediaType.MULTIPART_FORM_DATA_VALUE.equals(requestMapping.consumes()[0])) {
          return false;
      }
      return super.canRead(type, contextClass, mediaType);
  }

  @Override
  protected boolean canWrite(final MediaType mediaType) {
      // This converter is only be used for requests.
      return false;
  }
}