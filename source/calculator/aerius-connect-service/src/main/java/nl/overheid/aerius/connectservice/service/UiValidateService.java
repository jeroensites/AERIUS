/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import nl.overheid.aerius.connectservice.domain.FileType;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.io.ImportableFile;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.ops.OPSCustomCalculationPoint;
import nl.overheid.aerius.shared.domain.scenario.SituationStats;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.validation.FileValidationStatus;
import nl.overheid.aerius.shared.domain.validation.ValidationError;
import nl.overheid.aerius.shared.domain.validation.ValidationStatus;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

import reactor.core.publisher.Mono;

@Service
public class UiValidateService {

  private static final Logger LOG = LoggerFactory.getLogger(UiValidateService.class);

  private static final Set<ImportOption> DEFAULT_OPTIONS = EnumSet.of(
      ImportOption.USE_IMPORTED_LANDUSES,
      ImportOption.INCLUDE_CALCULATION_POINTS,
      ImportOption.INCLUDE_SOURCES,
      ImportOption.INCLUDE_NSL_MEASURES,
      ImportOption.INCLUDE_NSL_DISPERSION_LINES,
      ImportOption.INCLUDE_NSL_CORRECTIONS,
      ImportOption.USE_VALID_SECTORS,
      ImportOption.VALIDATE_SOURCES,
      ImportOption.VALIDATE_AGAINST_SCHEMA,
      ImportOption.VALIDATE_WITH_SCHEMATRON,
      ImportOption.INCLUDE_RESULTS);

  private final ProxyFileService fileService;
  private final ImportService importService;
  private final MessagesService messagesService;
  private final ObjectMapper objectMapper;

  @Autowired
  public UiValidateService(final ProxyFileService fileService, final ImportService importService, final MessagesService messagesService,
      final ObjectMapper objectMapper) {
    this.fileService = fileService;
    this.importService = importService;
    this.messagesService = messagesService;
    this.objectMapper = objectMapper;
  }

  public Mono<Void> validateAndImportFileAsync(final String fileCode, final String originalFilename, final ImportProperties importProperties) {
    final Locale locale = LocaleContextHolder.getLocale();
    return Mono.fromSupplier(() -> {
      // Different thread, so be sure to set the locale based on old thread
      LocaleContextHolder.setLocale(locale);
      validateAndImportFile(fileCode, originalFilename, importProperties);
      return null;
    });
  }

  private void validateAndImportFile(final String originalFileCode, final String originalFilename, final ImportProperties importProperties) {
    List<ImportParcel> parcels;

    try {
      parcels = getImportParcels(originalFileCode, originalFilename, importProperties);
    } catch (final IOException e) {
      parcels = onException(e);
    }

    final List<String> childFileCodes = new ArrayList<>();

    // Loop through every parcel except the first one
    parcels.stream().skip(1).forEach(parcel -> addChildFile(parcel, originalFilename, childFileCodes));

    // Update the folder of the first (parent) parcel
    final ImportParcel parcel = parcels.get(0);
    final FileValidationStatus parentStatus = makeFileValidationStatusForParcel(originalFileCode, parcel, childFileCodes);

    writeAndSaveResult(originalFileCode, parcel);
    writeAndSaveResultValidation(originalFileCode, parentStatus);
  }

  private void addChildFile(final ImportParcel parcel, final String filename, final List<String> childFileCodes) {
    final String fileCode = fileService.writeEmptyRawFile(filename);
    fileService.writePendingValidationFile(fileCode);

    childFileCodes.add(fileCode);

    final FileValidationStatus status = makeFileValidationStatusForParcel(fileCode, parcel, Collections.emptyList());

    writeAndSaveResult(fileCode, parcel);
    writeAndSaveResultValidation(fileCode, status);
  }

  private List<ImportParcel> getImportParcels(final String fileCode, final String originalFilename, final ImportProperties importProperties)
      throws IOException {
    final List<ImportParcel> importParcels = new ArrayList<>(fileService.retrieveFile(fileCode, FileType.RAW, false,
        inputStream -> {
          try (inputStream) {
            final ImportableFile importableFile = new FileServiceImportableFile(originalFilename, inputStream);
            // Use normal import to do the validation for us
            try {
              return importService.importFile(importableFile, DEFAULT_OPTIONS, importProperties);
            } catch (final AeriusException e) {
              return onException(e);
            }
          }
        }));
    importParcels.addAll(extractCalculationPointParcels(importParcels));
    importParcels.stream()
        .flatMap(parcel -> parcel.getCalculationPointsList().stream())
        .forEach(UiValidateService::toCorrectType);
    return importParcels;
  }

  private List<ImportParcel> onException(final Exception e) {
    LOG.error("Error while importing file", e);
    final AeriusException exception;
    if (e instanceof AeriusException) {
      exception = (AeriusException) e;
    } else {
      exception = new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }

    final ImportParcel errorParcel = new ImportParcel();
    errorParcel.getExceptions().add(exception);
    return List.of(errorParcel);
  }

  private void writeAndSaveResult(final String fileCode, final ImportParcel parcel) {
    // Clear warnings and exceptions
    parcel.getExceptions().clear();
    parcel.getWarnings().clear();

    try {
      final byte[] parcelJson = objectMapper.writeValueAsBytes(parcel);
      fileService.write(fileCode, parcelJson, FileType.IMPORTPARCEL);
    } catch (final JsonProcessingException e) {
      LOG.error("Error parsing parcels to JSON for fileCode {}", fileCode);
    }
  }

  private void writeAndSaveResultValidation(final String fileCode, final FileValidationStatus status) {
    try {
      updateValidationStatus(status);

      final byte[] bytes = objectMapper.writeValueAsBytes(status);
      fileService.write(fileCode, bytes, FileType.VALIDATION);
    } catch (final JsonProcessingException e) {
      LOG.error("Error parsing validation of parcel to JSON for fileCode {}", fileCode);
    }
  }

  private FileValidationStatus makeFileValidationStatusForParcel(final String fileCode, final ImportParcel parcel,
      final List<String> childFileCodes) {
    final SituationStats stats = new SituationStats();
    stats.setSources(parcel.getSituation().getSources().getFeatures().size());
    stats.setBuildings(parcel.getSituation().getBuildings().getFeatures().size());
    stats.setCalculationPoints(parcel.getCalculationPoints().getFeatures().size());

    final FileValidationStatus status = new FileValidationStatus(fileCode, ValidationStatus.PENDING, parcel.getSituation().getName(),
        parcel.getSituation().getType(), childFileCodes, stats);

    status.getErrors().addAll(parcel.getExceptions().stream()
        .map(this::mapAeriusExceptionToValidationError)
        .collect(Collectors.toList()));
    status.getWarnings().addAll(parcel.getWarnings().stream()
        .map(this::mapAeriusExceptionToValidationError)
        .collect(Collectors.toList()));

    return status;
  }

  private static void updateValidationStatus(final FileValidationStatus status) {
    if (!status.getErrors().isEmpty()) {
      status.setValidationStatus(ValidationStatus.INVALID);
    } else if (!status.getWarnings().isEmpty()) {
      status.setValidationStatus(ValidationStatus.VALID_WITH_WARNINGS);
    } else {
      status.setValidationStatus(ValidationStatus.VALID);
    }
  }

  private ValidationError mapAeriusExceptionToValidationError(final AeriusException exception) {
    return new ValidationError(exception.getReason().getErrorCode(), messagesService.getExceptionMessage(exception));
  }

  /**
   * Extract calculation points to new parcels
   */
  private List<ImportParcel> extractCalculationPointParcels(final List<ImportParcel> importParcels) {
    return importParcels.stream()
        .filter(UiValidateService::shouldExtractCalculationPoints)
        .map(parcel -> {
          final ImportParcel newParcel = createCalculationPointParcel(parcel);
          parcel.getCalculationPoints().getFeatures().clear();
          return newParcel;
        })
        .collect(Collectors.toList());
  }

  /**
   *  Only extract calculation points from a parcel if it also contains emission sources or buildings.
   */
  private static boolean shouldExtractCalculationPoints(final ImportParcel parcel) {
    return !parcel.getCalculationPointsList().isEmpty() &&
        (!parcel.getSituation().getEmissionSourcesList().isEmpty() || !parcel.getSituation().getBuildingsList().isEmpty());
  }

  /**
   * Create a new ImportParcel with just the calculation points and import status data from the input parcel.
   */
  private static ImportParcel createCalculationPointParcel(final ImportParcel importParcel) {
    final ImportParcel copy = new ImportParcel();
    copy.setType(importParcel.getType());
    copy.setDatabaseVersion(importParcel.getDatabaseVersion());
    copy.setVersion(importParcel.getVersion());
    copy.getCalculationPoints().setFeatures(new ArrayList<>(importParcel.getCalculationPointsList()));
    copy.getWarnings().addAll(importParcel.getWarnings());
    copy.getExceptions().addAll(importParcel.getExceptions());
    return copy;
  }

  private static void toCorrectType(final CalculationPointFeature feature) {
    if (feature.getProperties() instanceof OPSCustomCalculationPoint) {
      // OPSCustomCalculationPoint will be converted to JSON with type 'OPSCustomCalculationPoint' by jackson
      // Since this is unknown in the base class CalculationPoint, it won't work well when the points are send back for calculations/exports
      // 2 options:
      // - move OPSCustomCalculationPoint (or it's properties) to IMAER-java
      // - copy the properties to a type that is converted properly.
      // This is that second option.
      final OPSCustomCalculationPoint originalProperties = (OPSCustomCalculationPoint) feature.getProperties();
      final CustomCalculationPoint copiedProperties = new CustomCalculationPoint();
      copiedProperties.setGmlId(originalProperties.getGmlId());
      copiedProperties.setLabel(originalProperties.getLabel());
      copiedProperties.setDescription(originalProperties.getDescription());
      copiedProperties.setJurisdictionId(originalProperties.getJurisdictionId());
      copiedProperties.setCustomPointId(originalProperties.getCustomPointId());
      copiedProperties.setHeight(originalProperties.getHeight());
      feature.setProperties(copiedProperties);
    }
  }
}
