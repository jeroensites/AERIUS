/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.gml.GMLScenario;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.ZipFileMaker;

@Service
public class ExportService {

  private final ConstantRepositoryBean constantRepository;
  private final GMLWriter gmlWriter;

  public ExportService(final ConstantRepositoryBean constantRepository, final GMLWriter gmlWriter) {
    this.constantRepository = constantRepository;
    this.gmlWriter = gmlWriter;
  }

  public byte[] exportAsZip(final List<ImportParcel> importParcels) throws AeriusException, IOException {
    final File tempDir = Files.createTempDirectory("aerius_export_service").toFile();
    final List<File> exportFiles = new ArrayList<>();
    for (final ImportParcel parcel : importParcels) {
      final MetaDataInput metaDataInput = constructMetaData(parcel);
      final GMLScenario scenario = GMLScenario.Builder.create(parcel.getSituation().getName(), ensureSituationType(parcel))
          .sources(parcel.getSituation().getEmissionSourcesList())
          .buildings(parcel.getSituation().getBuildingsList())
          .calculationPoints(parcel.getCalculationPointsList())
          .nslDispersionLines(parcel.getSituation().getNslDispersionLinesList())
          .nslCorrections(parcel.getSituation().getNslCorrections())
          .nslMeasures(parcel.getSituation().getNslMeasuresList())
          .build();

      final File file = gmlWriter.writeToFile(tempDir, scenario, metaDataInput, importParcels.indexOf(parcel), Optional.empty(), Optional.empty());
      exportFiles.add(file);
    }

    return createZip(exportFiles, tempDir);
  }

  private SituationType ensureSituationType(final ImportParcel parcel) {
    return Optional.ofNullable(parcel.getSituation().getType()).orElse(SituationType.PROPOSED);
  }

  private MetaDataInput constructMetaData(final ImportParcel parcel) {
    final MetaDataInput metaDataInput = new MetaDataInput();
    metaDataInput.setScenarioMetaData(parcel.getImportedMetaData() == null ? new ScenarioMetaData() : parcel.getImportedMetaData());
    metaDataInput.setYear(parcel.getSituation().getYear());
    metaDataInput.setVersion(AeriusVersion.getVersionNumber());
    metaDataInput.setDatabaseVersion(constantRepository.getDatabaseVersion());
    metaDataInput.setOptions(new CalculationSetOptions());
    metaDataInput.setResultsIncluded(false);
    return metaDataInput;
  }

  private byte[] createZip(final List<File> files, final File baseDir) throws IOException {
    try (final ByteArrayOutputStream bstream = new ByteArrayOutputStream()) {
      ZipFileMaker.files2ZipStream(bstream, files, baseDir, true);
      return bstream.toByteArray();
    }
  }

}
