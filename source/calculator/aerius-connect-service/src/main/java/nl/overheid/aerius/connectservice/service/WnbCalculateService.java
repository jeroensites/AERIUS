/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.overheid.aerius.calculation.WNBCalculationOptionsUtil;
import nl.overheid.aerius.connectservice.model.WnbCalculationOptions;
import nl.overheid.aerius.connectservice.model.WnbCalculationOptions.CalculationPointsTypeEnum;
import nl.overheid.aerius.connectservice.model.WnbCalculationOptions.OutputTypeEnum;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.QueueEnum;

@Service
public class WnbCalculateService extends AbstractCalculateService<WnbCalculationOptions> {

  @Autowired
  WnbCalculateService(final ExportTaskClientBean exportTaskclient, final JobRepositoryBean jobRepository,
      final ConnectCalculationPointSetsRepositoryBean pointSetsRepository, final ConstantRepositoryBean constantRepository,
      final ProxyFileService fileService, final LocaleService localeService) {
    super(exportTaskclient, jobRepository, pointSetsRepository, constantRepository, fileService, localeService);
  }

  @Override
  public String calculate(final Scenario scenario, final WnbCalculationOptions options) throws AeriusException {
    // Should not call this specific method for wnb calculations
    throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
  }

  @Override
  protected JobType getJobType(final WnbCalculationOptions options) {
    return options.getOutputType() == OutputTypeEnum.PDF ? JobType.REPORT : JobType.CALCULATION;
  }

  @Override
  protected Optional<String> getJobName(final WnbCalculationOptions options) {
    return Optional.ofNullable(options.getName());
  }

  @Override
  protected boolean useCustomPoints(final WnbCalculationOptions options) {
    // Default for WNB calculation is not using custom points, but letting AERIUS determine receptors.
    return options.getCalculationPointsType() != null && options.getCalculationPointsType() == CalculationPointsTypeEnum.CUSTOM_POINTS;
  }

  @Override
  protected String getReceptorSetName(final WnbCalculationOptions options) {
    return options.getReceptorSetName();
  }

  @Override
  protected Optional<Integer> getOverrideYear(final WnbCalculationOptions options) {
    return Optional.ofNullable(options.getCalculationYear());
  }

  @Override
  protected void validateBeforeSending(final Scenario scenario, final WnbCalculationOptions options) throws AeriusException {
    if (options.getOutputType() == OutputTypeEnum.PDF &&
        scenario.getSituations().stream().noneMatch(scenarioSituation -> scenarioSituation.getType() == SituationType.PROPOSED)) {
      throw new AeriusException(AeriusExceptionReason.CALCULATION_PAA_PROPOSED_SITUATION_MISSING);
    }
  }

  @Override
  protected void updateScenarioOptions(final Scenario scenario, final WnbCalculationOptions options) throws AeriusException {
    final CalculationSetOptions cso = WNBCalculationOptionsUtil.createWnbCalculationSetOptions();

    cso.setCalculationType(useCustomPoints(options) ? CalculationType.CUSTOM_POINTS : CalculationType.PERMIT);
    scenario.setOptions(cso);
  }

  @Override
  protected void setExportProperties(final ExportProperties exportProperties, final WnbCalculationOptions options) {
    final ExportType exportType = options.getOutputType() == OutputTypeEnum.PDF ? ExportType.PAA : ExportType.GML_WITH_RESULTS;
    exportProperties.setName(options.getName());
    exportProperties.setExportType(exportType);
  }

  @Override
  protected boolean shouldNotSendEmail(final WnbCalculationOptions options) {
    return options.getSendEmail() != null && !options.getSendEmail();
  }

  @Override
  protected QueueEnum determineQueue(final WnbCalculationOptions options) {
    return options.getOutputType() == OutputTypeEnum.PDF ? QueueEnum.CONNECT_PAA_EXPORT : QueueEnum.CONNECT_GML_EXPORT;
  }

}
