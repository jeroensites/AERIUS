/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.connectservice.domain.ExportOptions;
import nl.overheid.aerius.connectservice.domain.UiCalculationRequest;
import nl.overheid.aerius.db.calculator.CalculatorLimitsRepository;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.CalculatorLimits;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.QueueEnum;

/**
 * Test class for {@link UiCalculateService}.
 */
@ExtendWith(MockitoExtension.class)
class UiCalculateServiceTest {

  private static final int MIN_YEAR = 1300;
  private static final int MAX_YEAR = 1500;
  private static final int TEST_YEAR = 1400;
  private static final String TEST_JOB_ID = "CalculateThis";
  private static final String TEST_EMAIL = "MyFakeEmail@example.org";

  @Mock ExportTaskClientBean exportTaskClient;
  @Mock JobRepositoryBean jobRepository;
  @Mock ConnectCalculationPointSetsRepositoryBean pointSetsRepository;
  @Mock ConstantRepositoryBean constantRepository;
  @Mock ProxyFileService fileService;
  @Mock UiSourcesService sourcesService;
  @Mock CalculatorLimitsRepository calculatorLimitsRepository;
  @Mock LocaleService localeService;

  @Captor ArgumentCaptor<ExportProperties> exportPropertiesCaptor;
  @Captor ArgumentCaptor<QueueEnum> queueNameCaptor;
  @Captor ArgumentCaptor<CalculationSetOptions> optionsCaptor;

  UiCalculateService calculateService;

  @BeforeEach
  void beforeEach() throws AeriusException {
    lenient().when(constantRepository.getInteger(ConstantsEnum.MIN_YEAR)).thenReturn(MIN_YEAR);
    lenient().when(constantRepository.getInteger(ConstantsEnum.MAX_YEAR)).thenReturn(MAX_YEAR);
    lenient().when(localeService.getLocale()).thenReturn(Locale.ENGLISH);
    calculateService = new UiCalculateService(exportTaskClient, jobRepository, pointSetsRepository, constantRepository, fileService, sourcesService,
        calculatorLimitsRepository, localeService);
    final CalculatorLimits calculatorLimits = new CalculatorLimits();

    calculatorLimits.setMaxSources(5000);
    lenient().when(calculatorLimitsRepository.getCalculatorLimits()).thenReturn(calculatorLimits);
  }

  @ParameterizedTest
  @MethodSource("provideTestCalculateParameters")
  void testCalculate(final int size, final QueueEnum expectedQueue) throws AeriusException, IOException {
    final Scenario scenario = mockScenario(size);
    final UiCalculationRequest options = mockOptions(scenario);

    when(jobRepository.createJob(any())).thenReturn(TEST_JOB_ID);

    final String jobId = calculateService.calculate(scenario, options);

    assertEquals(TEST_JOB_ID, jobId, "Job ID shouldn't be null");

    verify(jobRepository).createJob(JobType.CALCULATION);
    verify(exportTaskClient).startExport(queueNameCaptor.capture(), exportPropertiesCaptor.capture(), eq(scenario), eq(TEST_JOB_ID));

    verify(scenario.getSituations().get(0), never()).setYear(TEST_YEAR);

    final ExportProperties capturedProperties = exportPropertiesCaptor.getValue();
    assertEquals(ExportType.CALCULATION_UI, capturedProperties.getExportType(), "UI Calculation");
    assertNull(capturedProperties.getName(), "Should not have name");
    assertTrue(capturedProperties.getAdditionalOptions().contains(ExportAdditionalOptions.EMAIL_USER), "Should mail user");
    assertEquals(TEST_EMAIL, capturedProperties.getEmailAddress(), "Should contain user email address");

    assertEquals(expectedQueue, queueNameCaptor.getValue(), "Should send to " + expectedQueue + " calculation queue");
  }

  @Test
  void testLimitSourcesExceeded() throws AeriusException {
    final Scenario scenario = mockScenario(50_000);
    final UiCalculationRequest options = mockOptions(scenario);

    final AeriusException exception = assertThrows(AeriusException.class, () -> calculateService.calculate(scenario, options));

    assertEquals(AeriusExceptionReason.LIMIT_SOURCES_EXCEEDED, exception.getReason(), "Should have thrown LIMIT_SOURCES_EXCEEDED.");
    verify(jobRepository, never()).createJob(any());
    verify(jobRepository, never()).createJob(any(), any(), any());
  }

  @Test
  void testCalculateCustomPointsEmpty() throws AeriusException, IOException {
    final Scenario scenario = mockScenario(1);
    final CalculationSetOptions setOptions = mock(CalculationSetOptions.class);
    when(scenario.getOptions()).thenReturn(setOptions);
    when(setOptions.getCalculationType()).thenReturn(CalculationType.PERMIT);
    when(scenario.getCustomPointsList()).thenReturn(new ArrayList<>());
    final UiCalculationRequest request = mockOptions(scenario);

    when(jobRepository.createJob(any())).thenReturn(TEST_JOB_ID);

    final String jobId = calculateService.calculate(scenario, request);

    assertEquals(TEST_JOB_ID, jobId, "Job ID shouldn't be null");

    verify(jobRepository).createJob(JobType.CALCULATION);
    verify(exportTaskClient).startExport(queueNameCaptor.capture(), exportPropertiesCaptor.capture(), eq(scenario), eq(TEST_JOB_ID));
    verify(scenario).setOptions(optionsCaptor.capture());
    final CalculationSetOptions optionsWhenCalculating = optionsCaptor.getValue();
    assertNotEquals(setOptions, optionsWhenCalculating, "Should be a new options object set");
    assertEquals(CalculationType.PERMIT, optionsWhenCalculating.getCalculationType(), "Type should be set to permit.");
  }

  @Test
  void testCalculateCustomPointsNotEmpty() throws AeriusException, IOException {
    final Scenario scenario = mockScenario(1);
    final CalculationSetOptions setOptions = mock(CalculationSetOptions.class);
    when(scenario.getOptions()).thenReturn(setOptions);
    when(setOptions.getCalculationType()).thenReturn(CalculationType.PERMIT);
    when(scenario.getCustomPointsList()).thenReturn(List.of(mock(CalculationPointFeature.class)));
    final UiCalculationRequest request = mockOptions(scenario);

    when(jobRepository.createJob(any())).thenReturn(TEST_JOB_ID);

    final String jobId = calculateService.calculate(scenario, request);

    verify(jobRepository).createJob(JobType.CALCULATION);
    verify(exportTaskClient).startExport(queueNameCaptor.capture(), exportPropertiesCaptor.capture(), eq(scenario), eq(TEST_JOB_ID));
    verify(scenario).setOptions(optionsCaptor.capture());
    final CalculationSetOptions optionsWhenCalculating = optionsCaptor.getValue();
    assertNotEquals(setOptions, optionsWhenCalculating, "Should be a new options object set");
    assertEquals(CalculationType.PERMIT, optionsWhenCalculating.getCalculationType(), "Type should be set to permit.");

    assertEquals(TEST_JOB_ID, jobId, "Job ID shouldn't be null");

    assertEquals(1, scenario.getCustomPointsList().size(), "List shouldn't have been emptied");

    verify(jobRepository).createJob(JobType.CALCULATION);
  }

  @Test
  void testCalculateCustomPointsEmptyForCustomPointsCalculation() throws AeriusException, IOException {
    final Scenario scenario = mockScenario(1);
    final CalculationSetOptions setOptions = mock(CalculationSetOptions.class);
    when(scenario.getOptions()).thenReturn(setOptions);
    when(setOptions.getCalculationType()).thenReturn(CalculationType.CUSTOM_POINTS);
    when(scenario.getCustomPointsList()).thenReturn(new ArrayList<>());
    final UiCalculationRequest request = mockOptions(scenario);

    final AeriusException exception = assertThrows(AeriusException.class, () -> calculateService.calculate(scenario, request),
        "Should throw an error when trying to calculate CUSTOM_POINTS without custom points");

    assertEquals(AeriusExceptionReason.CONNECT_NO_CUSTOM_POINTS, exception.getReason(), "Reason returned");

    verify(jobRepository, never()).createJob(JobType.CALCULATION);
    verifyNoInteractions(exportTaskClient);
  }

  @Test
  void testCalculateCustomPointsNotEmptyForCustomPointsCalculation() throws AeriusException, IOException {
    final Scenario scenario = mockScenario(1);
    final CalculationSetOptions setOptions = mock(CalculationSetOptions.class);
    when(scenario.getOptions()).thenReturn(setOptions);
    when(setOptions.getCalculationType()).thenReturn(CalculationType.CUSTOM_POINTS);
    when(scenario.getCustomPointsList()).thenReturn(List.of(mock(CalculationPointFeature.class)));
    final UiCalculationRequest request = mockOptions(scenario);

    when(jobRepository.createJob(any())).thenReturn(TEST_JOB_ID);

    final String jobId = calculateService.calculate(scenario, request);

    verify(jobRepository).createJob(JobType.CALCULATION);
    verify(exportTaskClient).startExport(queueNameCaptor.capture(), exportPropertiesCaptor.capture(), eq(scenario), eq(TEST_JOB_ID));
    verify(scenario).setOptions(optionsCaptor.capture());
    final CalculationSetOptions optionsWhenCalculating = optionsCaptor.getValue();
    assertNotEquals(setOptions, optionsWhenCalculating, "Should be a new options object set");
    assertEquals(CalculationType.CUSTOM_POINTS, optionsWhenCalculating.getCalculationType(), "Type should be set to custom points.");

    assertEquals(TEST_JOB_ID, jobId, "Job ID shouldn't be null");

    assertEquals(1, scenario.getCustomPointsList().size(), "List shouldn't have been emptied");

    verify(jobRepository).createJob(JobType.CALCULATION);
  }

  private static Stream<Arguments> provideTestCalculateParameters() {
    return Stream.of(Arguments.of(10, QueueEnum.CALCULATION_UI_SMALL), Arguments.of(110, QueueEnum.CALCULATION_UI_LARGE));
  }

  private static UiCalculationRequest mockOptions(final Scenario scenario) {
    final UiCalculationRequest options = mock(UiCalculationRequest.class);
    final ExportOptions eo = new ExportOptions();
    eo.setExportType(ExportType.CALCULATION_UI);
    eo.setEmail(TEST_EMAIL);
    when(options.getTheme()).thenReturn(Theme.WNB);
    lenient().when(options.getScenario()).thenReturn(scenario);
    when(options.getExportOptions()).thenReturn(eo);
    return options;
  }

  private Scenario mockScenario(final int size) {
    final Scenario scenario = mock(Scenario.class);
    final ScenarioSituation situation = mock(ScenarioSituation.class);
    lenient().when(situation.getYear()).thenReturn(TEST_YEAR);
    final List<EmissionSourceFeature> sourcesList = mock(ArrayList.class);
    lenient().when(sourcesList.size()).thenReturn(size);
    lenient().when(sourcesList.iterator()).thenReturn(new ArrayList<EmissionSourceFeature>().iterator());
    lenient().when(scenario.getSituations()).thenReturn(List.of(situation));
    lenient().when(situation.getEmissionSourcesList()).thenReturn(sourcesList);

    return scenario;
  }
}
