/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.connectservice.model.RblCalculationOptions;
import nl.overheid.aerius.connectservice.model.RblCalculationOptions.NationalSrm2NetworkVersionEnum;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.ConstantRepositoryBean;
import nl.overheid.aerius.db.connect.ConnectCalculationPointSetsRepositoryBean;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.server.service.export.ExportTaskClientBean;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link RblCalculateService}.
 */
@ExtendWith(MockitoExtension.class)
class RblCalculateServiceTest {

  private static final int MIN_YEAR = 1300;
  private static final int MAX_YEAR = 1500;
  private static final int TEST_YEAR = 1400;
  private static final String TEST_JOB_ID = "CalculateThis";
  private static final String TEST_NAME = "StandardNaming";
  private static final String TEST_EMAIL = "MyFakeEmail@example.org";

  @Mock ExportTaskClientBean exportTaskClient;
  @Mock JobRepositoryBean jobRepository;
  @Mock ConnectCalculationPointSetsRepositoryBean pointSetsRepository;
  @Mock ConstantRepositoryBean constantRepository;
  @Mock ProxyFileService fileService;
  @Mock LocaleService localeService;

  @Mock ConnectUser connectUser;

  @Captor ArgumentCaptor<ExportProperties> exportPropertiesCaptor;

  RblCalculateService calculateService;

  @BeforeEach
  void beforeEach() throws AeriusException {
    lenient().when(connectUser.getEmailAddress()).thenReturn(TEST_EMAIL);
    lenient().when(constantRepository.getInteger(ConstantsEnum.MIN_YEAR)).thenReturn(MIN_YEAR);
    lenient().when(constantRepository.getInteger(ConstantsEnum.MAX_YEAR)).thenReturn(MAX_YEAR);
    lenient().when(fileService.write(any(), anyBoolean())).thenReturn("id");
    lenient().when(localeService.getLocale()).thenReturn(Locale.ENGLISH);
    calculateService = new RblCalculateService(exportTaskClient, jobRepository, pointSetsRepository, constantRepository, fileService, localeService);
  }

  @Test
  void testCalculate() throws AeriusException, IOException {
    final Scenario scenario = mockScenario();
    final RblCalculationOptions options = mock(RblCalculationOptions.class);
    when(options.getExtendedResults()).thenReturn(Boolean.TRUE);
    when(options.getName()).thenReturn(TEST_NAME);
    when(options.getCalculationYear()).thenReturn(TEST_YEAR);
    when(options.getNationalSrm2NetworkVersion()).thenReturn(NationalSrm2NetworkVersionEnum.NUMBER_2018);
    when(options.getSendEmail()).thenReturn(Boolean.TRUE);

    when(jobRepository.createJob(any(), any(), any())).thenReturn(TEST_JOB_ID);

    final String jobId = calculateService.calculate(connectUser, scenario, options);

    assertEquals(TEST_JOB_ID, jobId, "Job ID shouldn't be null");

    verify(jobRepository).createJob(connectUser, JobType.CALCULATION, Optional.of(TEST_NAME));
    verify(exportTaskClient).startExport(any(), exportPropertiesCaptor.capture(), eq(scenario), eq(TEST_JOB_ID));

    verify(scenario.getSituations().get(0)).setYear(TEST_YEAR);
    verify(scenario.getOptions()).setMonitorSrm2Year(2018);

    final ExportProperties capturedProperties = exportPropertiesCaptor.getValue();
    assertEquals(ExportType.CSV_EXTENDED, capturedProperties.getExportType(), "CSV export type");
    assertEquals(TEST_NAME, capturedProperties.getName(), "Should contain name");
    assertTrue(capturedProperties.getAdditionalOptions().contains(ExportAdditionalOptions.EMAIL_USER), "Should mail user");
    assertEquals(TEST_EMAIL, capturedProperties.getEmailAddress(), "Should contain user email address");
  }

  private Scenario mockScenario() {
    return mockScenario(TEST_YEAR);
  }

  private Scenario mockScenario(final int year) {
    final Scenario scenario = mock(Scenario.class);
    final ScenarioSituation situation = mock(ScenarioSituation.class);
    final CalculationSetOptions options = mock(CalculationSetOptions.class);
    final CalculationPointFeature mockPoint = mock(CalculationPointFeature.class);
    final List<CalculationPointFeature> customPointsList = List.of(mockPoint);

    when(situation.getYear()).thenReturn(year);
    when(scenario.getCustomPointsList()).thenReturn(customPointsList);
    when(scenario.getSituations()).thenReturn(List.of(situation));
    when(scenario.getOptions()).thenReturn(options);
    return scenario;
  }
}
