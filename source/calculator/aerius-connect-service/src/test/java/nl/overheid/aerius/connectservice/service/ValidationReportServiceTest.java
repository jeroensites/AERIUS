/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.overheid.aerius.connectservice.domain.ImportParcelAggregate;
import nl.overheid.aerius.shared.exception.AeriusException;

@ExtendWith(MockitoExtension.class)
class ValidationReportServiceTest {

  @Mock MessagesService messagesService;

  ValidationReportService reportService;

  @BeforeEach
  void beforeEach() {
    reportService = new ValidationReportService(messagesService);
  }

  @Test
  void testReport() throws IOException {
    final ImportParcelAggregate importResult = mock(ImportParcelAggregate.class);
    final LocalDateTime testDateTime = LocalDateTime.of(2021, 05, 26, 21, 54);
    final AeriusException error1 = mock(AeriusException.class);
    final AeriusException error2 = mock(AeriusException.class);

    final AeriusException warning1 = mock(AeriusException.class);
    final AeriusException warning2 = mock(AeriusException.class);

    final ArrayList<AeriusException> errors = new ArrayList<>(List.of(error1, error2));
    final ArrayList<AeriusException> warnings = new ArrayList<>(List.of(warning1, warning2));

    when(importResult.getExceptions()).thenReturn(errors);
    when(importResult.getWarnings()).thenReturn(warnings);
    when(messagesService.getExceptionMessage(error1)).thenReturn("Error message 1");
    when(messagesService.getExceptionMessage(error2)).thenReturn("Error message the \"second\"");
    when(messagesService.getExceptionMessage(warning1)).thenReturn("Some cute warning");
    when(messagesService.getExceptionMessage(warning2)).thenReturn("Another warning");

    String result;
    try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      reportService.report(importResult, testDateTime, outputStream);
      result = outputStream.toString(StandardCharsets.UTF_8);
    }

    assertEquals("# AERIUS Validatie\n"
        + "Gegenereerd op: 2021-05-26 21:54:00.\n"
        + "\n"
        + "## fouten\n"
        + "\n"
        + "Error message 1\n"
        + "\n"
        + "Error message the \"second\"\n"
        + "\n"
        + "## waarschuwingen\n"
        + "\n"
        + "Some cute warning\n"
        + "\n"
        + "Another warning\n",
        result);
  }

}
