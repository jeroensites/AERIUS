/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connectservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import nl.overheid.aerius.controller.ImaerController;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.io.ImportableFile;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;

@ExtendWith(MockitoExtension.class)
class ImportServiceTest {

  @Mock ImaerController imaerController;
  @Mock LocaleService localeService;

  @Mock ImportProperties importProperties;

  @Captor ArgumentCaptor<ImportableFile> importableFileCaptor;

  ImportService importService;

  @BeforeEach
  void beforeEach() {
    importService = new ImportService(imaerController, localeService);
  }

  @Test
  void testImportFile() throws AeriusException {
    final MultipartFile filePart = mock(MultipartFile.class);
    final Set<ImportOption> options = Set.of();
    final ImportParcel mockedResult = mock(ImportParcel.class);
    final List<ImportParcel> mockedResults = List.of(mockedResult);

    when(imaerController.processFile(importableFileCaptor.capture(), eq(options), eq(importProperties), isNull()))
        .thenReturn(mockedResults);

    final List<ImportParcel> result = importService.importFile(filePart, options, importProperties);

    assertEquals(mockedResults, result, "Returned should be the one from the controller");
    final ImportableFile capturedImportableFile = importableFileCaptor.getValue();
    assertTrue(capturedImportableFile instanceof MultipartImportableFile, "Should be our own wrapper");
    final MultipartImportableFile capturedMultipart = (MultipartImportableFile) capturedImportableFile;
    assertEquals(filePart, capturedMultipart.getMultipartFile(), "Original filepart should be used");
  }

}
