/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.js.file;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class SituationStats {
  private @JsProperty int sources;
  private @JsProperty int buildings;
  private @JsProperty int calculationPoints;

  public final @JsOverlay int getSources() {
    return sources;
  }

  public final @JsOverlay void setSources(final int sources) {
    this.sources = sources;
  }

  public final @JsOverlay int getBuildings() {
    return buildings;
  }

  public final @JsOverlay void setBuildings(final int buildings) {
    this.buildings = buildings;
  }

  public final @JsOverlay int getCalculationPoints() {
    return calculationPoints;
  }

  public final @JsOverlay void setCalculationPoints(final int calculationPoints) {
    this.calculationPoints = calculationPoints;
  }

}
