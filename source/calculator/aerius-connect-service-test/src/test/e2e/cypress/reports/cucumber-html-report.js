var reporter = require('cucumber-html-reporter');

var options = {
	theme: 'bootstrap',
	jsonDir: './cypress/reports/cucumber-json',
	output: './cypress/reports/cucumber-html/cucumber-htmlreport.html',
	reportSuiteAsScenarios: true,
	scenarioTimestamp: true,
	launchReport: true,
	metadata: {
	    "App Version":"beta",
	}
    };
reporter.generate(options);
