const cucumber = require('cypress-cucumber-preprocessor').default
const clipboardy = require('clipboardy');

module.exports = (on, config) => {
  on('file:preprocessor', cucumber())
  on('task', {
    getClipboard () {
        return clipboardy.readSync();
    }
});
}
