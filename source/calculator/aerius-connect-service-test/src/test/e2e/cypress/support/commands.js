import 'cypress-file-upload';

Cypress.Commands.add("form_request", (url, apiKey, requestType, formData) => {

    return cy
      .server()
      .route(requestType, url)
      .as("formRequest")
      .window()
      .then(win => {
        var xhr = new win.XMLHttpRequest();
        xhr.open(requestType, url);
        xhr.setRequestHeader('api-key',apiKey);
        xhr.send(formData);
      })
      .wait("@formRequest");
});
