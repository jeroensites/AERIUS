### Using Maven

#### Runs the unit tests in Docker container. Default max duration 30 minutes.
```bash
mvn test -DCYPRESS_BASE_URL_API=<> -DCYPRESS_API_KEY=<> [-DCYPRESS_MAX_TEST_DURATION_MINUTES=<>]
```

### Using Node.js
Execute the commands from the 'src/test/e2e' directory.

#### The project is set up as a Node.js project. Install modules and library based on the data from package.json
```bash
npm install
```

#### Run Cypress
```bash
npx cypress run --config baseUrl=<> --env API_KEY=<>
```

#### Show reports
```bash
node ./cypress/reports/cucumber-html-report.js
```
