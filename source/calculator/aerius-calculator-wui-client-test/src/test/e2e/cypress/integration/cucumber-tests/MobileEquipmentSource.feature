#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

@Manual
Feature: MobileEquipmentSource

    Scenario: Create new mobile equipment source - Agriculture
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Mobiele werktuigen' and sector 'Landbouw'
        And I fill location with coordinates 'POLYGON((108207.43 521706.71,108207.87 521660.31,108248.14 521663.37,108243.33 521711.53,108207.43 521706.71))'
        And I fill the mobile equipment description 'Mobile equiment' with stage class 'Stage-V, >= 2019 , >= 560  kW, diesel, SCR: ja', fuel usage '100' operation hours per year '100' adblue usage '5'
        Then blue ratio warning is visible
        When I save the data and validate the warning
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Mobiele werktuigen' and sector 'Landbouw' is correctly saved
        And location with coordinates 'X:108226,76 Y:521685,92' is correctly saved
        Then mobile equipment description 'Mobile equiment' with stage class 'Stage-V, >= 2019 , >= 560  kW, diesel, SCR: ja', fuel usage '100' operation hours per year '100' adblue usage '5' is correctly saved

    Scenario: Create new mobile equipment source - Construction, Industry and Mining
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Mobiele werktuigen' and sector 'Bouw, Industrie en Delfstoffenwinning'
        And I fill location with coordinates 'POLYGON((108207.43 521706.71,108207.87 521660.31,108248.14 521663.37,108243.33 521711.53,108207.43 521706.71))'
        And I fill the mobile equipment description 'Mobile equiment' with stage class 'alle werktuigen op benzine, 2takt', fuel usage '101' operation hours per year '' adblue usage ''
        Then blue ratio warning is not visible
        When I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Mobiele werktuigen' and sector 'Bouw, Industrie en Delfstoffenwinning' is correctly saved
        And location with coordinates 'X:108226,76 Y:521685,92' is correctly saved
        Then mobile equipment description 'Mobile equiment' with stage class 'alle werktuigen op benzine, 2takt', fuel usage '101' operation hours per year '' adblue usage '' is correctly saved

    Scenario: Create new mobile equipment source - Consumer mobile equipment
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Mobiele werktuigen' and sector 'Consumenten mobiele werktuigen'
        And I fill location with coordinates 'POLYGON((108207.43 521706.71,108207.87 521660.31,108248.14 521663.37,108243.33 521711.53,108207.43 521706.71))'
        And I fill the mobile equipment description 'Mobile equiment' with stage class 'Stage-II, 2002-2005, >= 560  kW, diesel, SCR: nee', fuel usage '100' operation hours per year '100' adblue usage ''
        Then blue ratio warning is not visible
        When I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Mobiele werktuigen' and sector 'Consumenten mobiele werktuigen' is correctly saved
        And location with coordinates 'X:108226,76 Y:521685,92' is correctly saved
        Then mobile equipment description 'Mobile equiment' with stage class 'Stage-II, 2002-2005, >= 560  kW, diesel, SCR: nee', fuel usage '100' operation hours per year '100' adblue usage '' is correctly saved
