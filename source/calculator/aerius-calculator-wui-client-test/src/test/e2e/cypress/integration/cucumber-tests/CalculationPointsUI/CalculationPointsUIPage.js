/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

class calculationPointsUIPage {

    static selectMenuCalculationPoint() {
        cy.get('[id="testId.NAV_REKENPUNTEN"]').click()
    }

    static createCalculationPointNameLocation(name, location) {        
        cy.get('[id="calculationLaunchButtonNewsource"]').click()
        calculationPointsUIPage.nameLocation(name, location)
    }

    static createCalculationPointNameLocationCorrectlySaved(name, location) {
        cy.get('[id="label"]').should('have.value', (name))
        cy.get('[id="wktGeometry"]').should('have.value', (location))
    }
    
    static addCalculationPointNameLocation(name, location) {
        cy.get('[id="sourceListButtonNewSource"]').click()
        calculationPointsUIPage.nameLocation(name, location)
    }

    static nameLocation(name, location) {
        cy.get('[id="label"]').type('{selectall}').type(name)
        cy.get('[id="wktGeometry"]').type('{selectall}').type(location)
    }

    static saveCalculationPoint() {
        cy.get('[id="buttonSave"]').click()
    }

    static cancelCalculationPoint() {
        cy.get('[id="buttonCancel"]').click()
    }

    static editCalculationPoint(name) {
        cy.get('.calculationpoints').contains(name).click()
        cy.get('[id="sourceListButtonEditSource"]').click()
    }

    static deleteCalculationPoint(name) {
        cy.get('.calculationpoints').contains(name).click()
        cy.get('[id="sourceListButtonDeleteSource"]').click()
    }

    static countCalculationPoint(count) {
        cy.get('.calculationpoints > div > div').should('have.length', count)
    }    

    static saveDisabledAndShowError(error) {
        cy.get(".errorContainer:first > div").should('have.text', error)
        cy.get('[id="buttonSave"]').should('have.class', 'prevented')
    }

    static clickImportCalculationPointsFromCalculationPointsStartPage(){
        cy.get('[id="calculationLaunchButtonImportfile"]').click()
    }

    static clickImportCalculationPointsFromCalculationPointsPage(){
        cy.get('[id="sourceListButtonImport"]').click()
    }
}

export default calculationPointsUIPage
