/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

class shippingSourcePage {

    static editMaritimeDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactor(shipDescription, shipCategory, shipsPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor) {
        cy.get('[id="collapsibleSubSources"] > .line > .title').click({ force: true })
        cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true })

        shippingSourcePage.maritimeDescriptionCategory(shipDescription, shipCategory)
        shippingSourcePage.visitsTimeUnit(shipsPerTimeUnit, timeUnit)
        shippingSourcePage.averageResidenceTimeShorePower(averageResidenceTime, shorePowerFactor)
    }

    static editMaritimeDescriptionCategoryShipPerTimeUnitTimeUnit(shipDescription, shipCategory, shipsPerTimeUnit, timeUnit) {
        cy.get('[id="collapsibleSubSources"] > .line > .title').click({ force: true })
        cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true })

        cy.get('[id="emissionSourceMaritimeStandardDescription"]').clear().type(shipDescription)
        cy.get('[id="shipCategory"]').select(shipCategory)
        cy.get('[id="maritimeMovementsInput"]').type('{selectall}').type(shipsPerTimeUnit)
        cy.get('[id="timeUnit"]').select(timeUnit)
    }

    static maritimeDescriptionCategory(shipDescription, shipCategory) {
        cy.get('[id="description"]').clear().type(shipDescription)
        cy.get('[id="shipCategory"]').select(shipCategory)
    }

    static visitsTimeUnit(shipsPerTimeUnit, timeUnit) {
        cy.get('[id="shipsPerTimeUnit"]').clear().type(shipsPerTimeUnit)
        cy.get('[id="timeUnit"]').select(timeUnit)
    }

    static averageResidenceTimeShorePower(averageResidenceTime, shorePowerFactor) {
        cy.get('[id="averageResidenceTimeInput"]').clear().type(averageResidenceTime)
        cy.get('[id="shorePowerFactorInput"]').clear().type(shorePowerFactor)
    }

    static maritimeDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactorCreated(rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor) {
        shippingSourcePage.maritimeDescriptionCategoryShipPerTimeUnitTimeUnitCreated(rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit)
        cy.get('[id=maritimeShippingAverageResidenceTime-' + rowNumber + ']').should('have.text', averageResidenceTime)
        cy.get('[id=maritimeShippingShorePowerFactor-' + rowNumber + ']').should('have.text', shorePowerFactor)
    }

    static maritimeDescriptionCategoryShipPerTimeUnitTimeUnitCreated(rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit) {
        shippingSourcePage.maritimeDescriptionCategoryShipPerTimeUnitTimeUnitCreated(rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit)
    }

    static maritimeDescriptionCategoryShipPerTimeUnitTimeUnitCreated(rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit) {
        //cy.get('[id=maritimeShippingCode-' + rowNumber + ']').should('have.text', shipCategory)
        cy.get('[id=maritimeShippingDescription-' + rowNumber + ']').should('have.text', description)
        cy.get('[id=maritimeShippingMovements-' + rowNumber + ']').should('have.text', shipPerTimeUnit)
        cy.get('[id=maritimeShippingTimeUnit-' + rowNumber + ']').should('have.text', timeUnit)
    }

    static editInlandDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactorPercentageLaden(shipDescription, shipCategory, shipsPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor, percentageLaden) {
        cy.get('[id="collapsibleSubSources"] > .line > .title').click({ force: true })
        cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true })

        shippingSourcePage.inlandDescriptionCategory(shipDescription, shipCategory)
        cy.get('[id="inlandVisitsInput"]').type('{selectall}').type(shipsPerTimeUnit)
        cy.get('[id="timeUnit"]').select(timeUnit)
        cy.get('[id="averageResidenceTimeInput"]').clear().type(averageResidenceTime)
        cy.get('[id="shippingShorePowerFactorInput"]').clear().type(shorePowerFactor)
        cy.get('[id="shippingPercentageLadenInput"]').clear().type(percentageLaden)
    }

    static editInlandDescriptionCategory(shipDescription, shipCategory) {
        cy.get('[id="collapsibleSubSources"] > .line > .title').click({ force: true })
        cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true })

        shippingSourcePage.inlandDescriptionCategory(shipDescription, shipCategory)
    }

    static inlandDescriptionCategory(shipDescription, shipCategory) {
        cy.get('[id="emissionSourceInlandStandardDescription"]').clear().type(shipDescription)
        cy.get('[id="shipCategory"]').select(shipCategory)
    }

    static editMovementTypeShipMovementsTimeUnitPercentageLaden(movementType, shipMovements, timeUnit, percentageLaden) {
        if (movementType == 'A to B') {
            cy.get('[id="shipMovements_A_TO_B"]').type('{selectall}').type(shipMovements)
            cy.get('[id="timeUnit"]').first().select(timeUnit)
            cy.get('[id="shipPercentageLaden_A_TO_B"]').type('{selectall}').type(percentageLaden)
        } else if (movementType == 'B to A') {
            cy.get('[id="shipMovements_B_TO_A"]').type('{selectall}').type(shipMovements)
            cy.get('[id="timeUnit"]').last().select(timeUnit, 1)
            cy.get('[id="shipPercentageLaden_B_TO_A"]').clear().type('{selectall}').type(percentageLaden)
        } else {
            cy.contains("movementType does not exist").should('not.exist')
        }
    }

    static editWaterwayDirection(waterway, direction) {
        cy.get('[id="collapsibleSubSources"] > .line > .title').click({ force: true })
        cy.get('[id="waterwayCategory"]').select(waterway)
        if (direction == 'Stroomopwaarts') {
            cy.get('[id="emissionSourceShipDirectionToggleDirectionUpstream"]').click();
        } else if (direction == 'Stroomafwaarts') {
            cy.get('[id="emissionSourceShipDirectionToggleDirectionDownstream"]').click();
        }
        cy.get('[id="collapsibleSubSources"] > .line > .title').click({ force: true })
    }

    static inlandDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactorPercentageLadenCreated(rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor, percentageLaden) {
        //cy.get('[id=inlandShippingCode-' + rowNumber + ']').should('have.text', shipCategory)
        cy.get('[id=inlandShippingDescription-' + rowNumber + ']').should('have.text', description)
        cy.get('[id=inlandShippingMovements-' + rowNumber + ']').should('have.text', shipPerTimeUnit)
        cy.get('[id=inlandShippingTimeUnit-' + rowNumber + ']').should('have.text', timeUnit)
        //inlandShippingResidenceTime
        //inlandShippingshorePowerFactor
        cy.get('[id=inlandShippingPercentageLaden-' + rowNumber + ']').should('have.text', percentageLaden)
    }

    static inlandDescriptionCategoryCreated(rowNumber, shipCategory) {
        //cy.get('[id=inlandShippingCode-' + rowNumber + ']').should('have.text', shipCategory)
    }

    static movementTypeDescriptionShipMovementsTimeUnitPercentageLadenCreated(rowNumber, movementType, description, shipMovements, timeUnit, percentageLaden) {
	    if (movementType == 'A to B') {
	       cy.get('[id=inlandShippingDescriptionAtoB-' + rowNumber + ']').should('have.text', description)
	       cy.get('[id=inlandShippingMovementsAtoB-' + rowNumber + ']').should('have.text', shipMovements)
	       cy.get('[id=inlandShippingTimeUnitAtoB-' + rowNumber + ']').should('have.text', timeUnit)
	       cy.get('[id=inlandShippingPercentageLadenAtoB-' + rowNumber + ']').should('have.text', percentageLaden)
	    } else if ('B to A') {
	       cy.get('[id=inlandShippingDescriptionBtoA-' + rowNumber + ']').should('have.text', description)
	       cy.get('[id=inlandShippingMovementsBtoA-' + rowNumber + ']').should('have.text', shipMovements)
           cy.get('[id=inlandShippingTimeUnitBtoA-' + rowNumber + ']').should('have.text', timeUnit)
           cy.get('[id=inlandShippingPercentageLadenBtoA-' + rowNumber + ']').should('have.text', percentageLaden)
	    }
    }

    static waterwayDirectionCreated(waterway, direction) {
        cy.get('[id="inlandShippingWaterwayCode"]').should('have.text', waterway)
        cy.get('[id="inlandShippingWaterwayDirectionType"]').should('have.text', direction)
    }

    static fillDockA(dockA) {
        cy.get('[id="mooringACategory"]').select((dockA), {force: true})
    }

    static fillDockB(dockB) {
        cy.get('[id="mooringBCategory"]').select((dockB), {force: true} )
    }

    static dockACorrectlySaved(dockA) {
        cy.get('[id="maritimeShippingMooringAId"]').should('have.text', dockA)
    }

    static dockBCorrectlySaved(dockB) {
        cy.get('[id="maritimeShippingMooringBId"]').should('have.text', dockB)
    }

    static fieldNotVisible(notVisibleField) {
        switch(notVisibleField) {
            case 'dock A':
                cy.get('[id="maritimeShippingMooringAId"]').should('not.exist')
            break;
            case 'dock B':
                cy.get('[id="maritimeShippingMooringBId"]').should('not.exist')
            break;
        }
    }

}
export default shippingSourcePage
