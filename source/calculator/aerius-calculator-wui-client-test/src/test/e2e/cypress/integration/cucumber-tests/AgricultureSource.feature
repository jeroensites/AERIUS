#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Agriculture source

    Scenario: Create new agriculture source without subsource validate warning - Lodging emission
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose date establishment of animal shelter '2021-02-01', heat content type 'Niet geforceerd', emission height '115' and heat content '99'
        And I save the data
        Then the subsource error is given 'Voeg ten minste 1 subbron toe.'
        Then the savesource error is given 'Er zijn foutmeldingen, los deze eerst op.'

    Scenario: Create new agriculture source - Manure storage
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Mestopslag'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Mestopslag' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characterstics with heat content type 'Niet geforceerd', emission height '15,0', heat content '20,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And emission fields with NOx '10,0' 'kg/j' and NH3 '20,0' 'kg/j' are correctly saved

    Scenario: Create new agriculture source - Farmland
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Landbouwgrond'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '0' and heat content '0'
        And I add a new farmland source
        And I fill the farmland category with 'Beweiding', NOX with value '9999' and NH3 with value '10000'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Landbouwgrond' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characterstics with heat content type 'Niet geforceerd', emission height '0,0', heat content '0,000' and diurnal variation type 'Meststoffen' is correctly saved
        And farmland category 'Beweiding' with NOX '9999' 'kg/j' and NH3 '10,0' 'ton/j' is correctly created

    Scenario: Create new agriculture source - Greenhouse horticulture
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Glastuinbouw'
        And I fill location with coordinates 'LINESTRING(200380.4 493929.51,192361.53 488279.85,211861.97 487186.37)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '0' and NH3 '0'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Glastuinbouw' is correctly saved
        And location with coordinates 'X:197214,83 Y:488007,7' is correctly saved
        And source characterstics with heat content type 'Niet geforceerd', emission height '15,0', heat content '20,000' and diurnal variation type 'Verwarming van Ruimten (Zonder Seizoenscorrectie)' is correctly saved
        And emission fields with NOx '0,0' 'kg/j' and NH3 '0,0' 'kg/j' are correctly saved

    Scenario: Create new agriculture source - Fireplaces, other
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Vuurhaarden, overig'
        And I fill location with coordinates 'POLYGON((173407.83 567557.35,166117.94 572842.51,166117.94 572842.51,171403.11 577580.94,173407.83 567557.35))'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '99999' and NH3 '88888'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Vuurhaarden, overig' is correctly saved
        And location with coordinates 'X:169450,23 Y:572569,15' is correctly saved
        And source characterstics with heat content type 'Niet geforceerd', emission height '15,0', heat content '20,000' and diurnal variation type 'Verwarming van Ruimten (Zonder Seizoenscorrectie)' is correctly saved
        And emission fields with NOx '100,0' 'ton/j' and NH3 '88,9' 'ton/j' are correctly saved

    Scenario: Create new agriculture source - Lodging emissions with RAV code
        Given I login with correct username and password
        And I create a new source from the start page
        When I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a stable system with RAV code 'E 1.5.1', number of animals '100' and BWL code 'BB 93.06.008'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Stalemissies' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And the stable system with RAV code 'E1.5.1', number of animals '100', factor '0,02', reduction '-' and emission '2,0' is correctly created

    Scenario: Create new agriculture source - Lodging emissions with RAV code and an additional rule
        Given I login with correct username and password
        And I create a new source from the start page
        When I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a stable system with RAV code 'E 1.5.1', number of animals '100' and BWL code 'BB 93.06.008'
        And I add an additional rule of type 'Additionele techniek' with code 'E 6.9.a' and BWL code 'BWL 2017.04'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Stalemissies' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And the stable system with RAV code 'E1.5.1', number of animals '100', factor '0,02', reduction '-' and emission '2,0' is correctly created
        And the additional rule of type 'Additionele techniek' with code 'E6.9.a', number of animals '100', factor '0,009', reduction '-' and emission '2,9' is correctly created

    Scenario: Create new agriculture source - Lodging emissions with RAV code and mulitple additional rules
        Given I login with correct username and password
        And I create a new source from the start page
        When I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a stable system with RAV code 'E 5.9.1.1.1', number of animals '100' and BWL code 'BWL 2009.02'
        And I add an additional rule of type 'Emissiereducerende techniek' with code 'E 5.13' and BWL code 'BWL 2005.01'
        And I add an additional rule of type 'Voer- en of managementmaatregel' with code 'PAS 2015.07-01'
        And I save the data
        When I select the source 'Bron 1'
        And the source 'Bron 1' with sector group 'Landbouw' and sector 'Stalemissies' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And the stable system with RAV code 'E5.9.1.1.1', number of animals '100', factor '0,034', reduction '-' and emission '3,4' is correctly created
        And the additional rule of type 'Emissiereducerende techniek' with code 'E5.13', number of animals '-', factor '-', reduction '70 %' and emission '1,0' is correctly created
        And the additional rule of type 'Voer- en of managementmaatregel' with code 'PAS2015.07-01', number of animals '-', factor '-', reduction '43 %' and emission '0,6' is correctly created

    Scenario: Cancel Lodging emissions with RAV code
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose date establishment of animal shelter '2021-02-01', heat content type 'Niet geforceerd', emission height '115' and heat content '99'
        And I add a stable system with RAV code 'E 1.5.1', number of animals '101' and BWL code 'BB 93.06.008'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Stalemissies' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characterstics with heat content type 'Niet geforceerd', emission height '115,0', heat content '99,000' and diurnal variation type 'Dierverblijven' is correctly saved
        When I choose to edit the source
        And I add a stable system with RAV code 'E 5.9.1.1.1', number of animals '99' and BWL code 'BWL 2009.02'
        When I cancel adding the stable system
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Stalemissies' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characterstics with heat content type 'Niet geforceerd', emission height '115,0', heat content '99,000' and diurnal variation type 'Dierverblijven' is correctly saved
        #And the stable system with RAV code 'E1.5.1', number of animals '101', factor '0,02', reduction '-' and emission '2,0' is correctly created
        And the RAV code row count has '1' rows

    Scenario: Edit existing agriculture source - Lodging emissions
       Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose date establishment of animal shelter '2021-02-01', heat content type 'Niet geforceerd', emission height '115' and heat content '99'
        And I add a stable system with RAV code 'E 1.5.1', number of animals '100' and BWL code 'BB 93.06.008'
        And I save the data
        When I select the source 'Bron 1'
        And I choose to edit the source
        And I select stable system 'E1.5.1'
        And I edit the stable system to RAV code 'E 1.5.1', number of animals '800' and BWL code 'BB 93.06.008'
        And I save the data
        When I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Stalemissies' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characterstics with heat content type 'Niet geforceerd', emission height '115,0', heat content '99,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the stable system with RAV code 'E1.5.1', number of animals '800', factor '0,02', reduction '-' and emission '16,0' is correctly created

    Scenario: Copy existing agriculture source - Lodging emissions
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose date establishment of animal shelter '2021-02-01', heat content type 'Niet geforceerd', emission height '115' and heat content '99'
        And I add a stable system with RAV code 'E 1.5.1', number of animals '100' and BWL code 'BB 93.06.008'
        And I add an additional rule of type 'Additionele techniek' with code 'E 6.9.a'
        And I save the data
        When I select the source 'Bron 1'
        And I copy the source 'Bron 1'
        When I select the source 'Bron 1 (1)'
        Then the source 'Bron 1 (1)' with sector group 'Landbouw' and sector 'Stalemissies' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And source characterstics with heat content type 'Niet geforceerd', emission height '115,0', heat content '99,000' and diurnal variation type 'Dierverblijven' is correctly saved
        And the additional rule of type 'Additionele techniek' with code 'E6.9.a', number of animals '100', factor '0,009', reduction '-' and emission '2,9' is correctly created

    Scenario: Delete existing agriculture source - Lodging emissions
       Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose date establishment of animal shelter '2021-02-01', heat content type 'Niet geforceerd', emission height '115' and heat content '99'
        And I add a stable system with RAV code 'E 1.5.1', number of animals '100' and BWL code 'BB 93.06.008'
        And I save the data
        When I select the source 'Bron 1'
        And I copy the source 'Bron 1'
        And I copy the source 'Bron 1 (1)'
        When I select the source 'Bron 1 (1)'
        Given I delete source 'Bron 1 (1)'
        Then the next sources exists 'Bron 1', 'Bron 1 (2)'
        And the next source does not exist 'Bron 1 (1)'

    Scenario: Delete existing stable system
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose date establishment of animal shelter '2021-02-01', heat content type 'Niet geforceerd', emission height '115' and heat content '99'
        And I add a stable system with RAV code 'E 1.5.1', number of animals '100' and BWL code 'BB 93.06.008'
        And I save the data
        When I select the source 'Bron 1'
        And I choose to edit the source
        And I select stable system 'E1.5.1'
        And I delete the stable system
        Then the RAV code is not visible

      Scenario: Copy existing stable system
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose date establishment of animal shelter '2021-02-01', heat content type 'Niet geforceerd', emission height '115' and heat content '99'
        And I add a stable system with RAV code 'E 1.5.1', number of animals '100' and BWL code 'BB 93.06.008'
        And I save the data
        When I select the source 'Bron 1'
        And I choose to edit the source
        And I select stable system 'E1.5.1'
        And I copy the stable system
        And I save the data
        Then the stable system with RAV code 'E1.5.1' is copied

    Scenario: Delete additional rule
        Given I login with correct username and password
        And I create a new source from the start page
        When I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a stable system with RAV code 'E 1.5.1', number of animals '100' and BWL code 'BB 93.06.008'
        And I add an additional rule of type 'Additionele techniek' with code 'E 6.9.a' and BWL code 'BWL 2017.04'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Stalemissies' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        And the stable system with RAV code 'E1.5.1', number of animals '100', factor '0,02', reduction '-' and emission '2,0' is correctly created
        And the additional rule of type 'Additionele techniek' with code 'E6.9.a', number of animals '100', factor '0,009', reduction '-' and emission '2,9' is correctly created
        When I choose to edit the source
        And I select stable system 'E1.5.1'
        And I delete the additional rule
        And I save the data
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Stalemissies' is correctly saved
        And location with coordinates 'X:172203,2 Y:599120,32' is correctly saved
        Then the additional rule of type 'Additionele techniek' with code 'E6.9.a' is deleted

     Scenario: Warning if combination of RAV code and additional rule is not aloud - Additional technique
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a stable system with RAV code 'E 1.5.1', number of animals '100' and BWL code 'BB 93.06.008'
        And I add an additional rule of type 'Additionele techniek' with code 'E 6.10.b' and BWL code 'BWL 2017.05'
        Then a warning is visible that the combination is not aloud

    Scenario: Warning if combination of RAV code and additional rule is not aloud - Emission reducing technique
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a stable system with RAV code 'E 1.5.1', number of animals '100' and BWL code 'BB 93.06.008'
        And I add an additional rule of type 'Emissiereducerende techniek' with code 'A 1.17' and BWL code 'BWL 2012.02'
        Then a warning is visible that the combination is not aloud

    Scenario: Warning if combination of RAV code and additional rule is not aloud - Fodder and management rule
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a stable system with RAV code 'E 1.5.1', number of animals '100' and BWL code 'BB 93.06.008'
        And I add an additional rule of type 'Voer- en of managementmaatregel' with code 'PAS 2015.07-01 Snijmaissilage'
        Then a warning is visible that the combination is not aloud

      Scenario: Warning if combination of RAV code and additional rule is not aloud - combination of additional rules
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Landbouw' and sector 'Stalemissies'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I add a stable system with RAV code 'E 1.5.1', number of animals '100' and BWL code 'BB 93.06.008'
        And I add an additional rule of type 'Additionele techniek' with code 'E 6.10.b' and BWL code 'BWL 2017.05'
        Then a warning is visible that the combination is not aloud
        And I add an additional rule of type 'Emissiereducerende techniek' with code 'A 1.17' and BWL code 'BWL 2012.02'
        Then a warning is visible that the combination is not aloud
        And I add an additional rule of type 'Voer- en of managementmaatregel' with code 'PAS 2015.07-01 Snijmaissilage'
        Then a warning is visible that the combination is not aloud
    
    Scenario: Create new agriculture source - Lodging emissions with custom specifications for cattle
        Given I login with correct username and password
        And I create a new source from the start page
        And I fill in the fields for sector group 'Landbouw' with sector 'Stalemissies'
        And I open subsource
        When I add a stable system and fill in the custom specification fields with the following data
        | description   | animalType    | factor | numberOfAnimals |
        | Rundvee       | Rundvee       | 5      | 100             |
        | Schapen       | Schapen       | 10     | 200             |
        | Geiten        | Geiten        | 15     | 300             |
        | Varkens       | Varkens       | 20     | 400             |
        | Kippen        | Kippen        | 25     | 500             |
        | Kalkoenen     | Kalkoenen     | 30     | 600             |
        | Paarden       | Paarden       | 35     | 600             |
        | Eenden        | Eenden        | 40     | 700             |
        | Pelsdieren    | Pelsdieren    | 45     | 800             |
        | Konijnen      | Konijnen      | 50     | 900             |
        | Parelhoenders | Parelhoenders | 55     | 1000            |
        | Struisvogels  | Struisvogels  | 60     | 2000            |
        | Overige       | Overige       | 65     | 3000            |
        And I save the data
        When I select the source 'Bron 1'
        Then the following stable systems with custom specification are created
        | description   | numberOfAnimals | factor |reduction | emission    |
        | Rundvee       | 100             | 5      |  -       | 500 kg/j    |
        | Schapen       | 200             | 10     |  -       | 2.000 kg/j  |
        | Geiten        | 300             | 15     |  -       | 4.500 kg/j  |
        | Varkens       | 400             | 20     |  -       | 8.000 kg/j  |
        | Kippen        | 500             | 25     |  -       | 12.500 kg/j |
        | Kalkoenen     | 600             | 30     |  -       | 18.000 kg/j |
        | Paarden       | 600             | 35     |  -       | 21.000 kg/j |
        | Eenden        | 700             | 40     |  -       | 28.000 kg/j |
        | Pelsdieren    | 800             | 45     |  -       | 36.000 kg/j |
        | Konijnen      | 900             | 50     |  -       | 45.000 kg/j |
        | Parelhoenders | 1000            | 55     |  -       | 55.000 kg/j |
        | Struisvogels  | 2000            | 60     |  -       | 120.000 kg/j|
        | Overige       | 3000            | 65     |  -       | 195.000 kg/j|
        When I choose to edit the source
        Then the stable systems contains the following icons and data in the list
        | icon                               | amount | description   | emission       |
        | icon-animal-cow                    | 100    | Rundvee       | 500,0 kg/j     |
        | icon-animal-sheep                  | 200    | Schapen       | 2.000,0 kg/j   |
        | icon-animal-goat                   | 300    | Geiten        | 4.500,0 kg/j   |
        | icon-animal-pig                    | 400    | Varkens       | 8.000,0 kg/j   |
        | icon-animal-chicken                | 500    | Kippen        | 12.500,0 kg/j  |
        | icon-animal-turkey                 | 600    | Kalkoenen     | 18.000,0 kg/j  |
        | icon-animal-horse                  | 600    | Paarden       | 21.000,0 kg/j  |
        | icon-animal-duck                   | 700    | Eenden        | 28.000,0 kg/j  |
        | icon-animal-mink                   | 800    | Pelsdieren    | 36.000,0 kg/j  |
        | icon-animal-rabbit                 | 900    | Konijnen      | 45.000,0 kg/j  |
        | icon-animal-con-animal-guinea-fowl | 1000   | Parelhoenders | 55.000,0 kg/j  |
        | icon-animal-ostrich                | 2000   | Struisvogels  | 120.000,0 kg/j |
        |                                    | 3000   | Overige       | 195.000,0 kg/j |

    Scenario: Edit agriculture source - Lodging emissions with custom specifications
        Given I login with correct username and password
        And I create a new source from the start page
        And I fill in the fields for sector group 'Landbouw' with sector 'Stalemissies'
        And I add a stable system
        And I choose stable system of type 'Eigen specificatie'
        And I fill in the fields with description 'Stalsysteem met kippen', animal type 'Kippen', factor '5' and number of animals '15'
        And I save the data
        When I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Stalemissies' is correctly saved
        Then the custom specification with description 'Stalsysteem met kippen', number of animals '15', factor '5', reduction '-' and emission '75' is created
        When I choose to edit the source
        And I select stable system 'Stalsysteem met kippen'
        And I fill in the fields with description 'Stalsysteem met konijnen', animal type 'Konijnen', factor '5' and number of animals '175'
        And I save the data
        Then the custom specification with description 'Stalsysteem met konijnen', number of animals '175', factor '5', reduction '-' and emission '875' is created

    Scenario: Import and copy existing agriculture source - Lodging emissions
        Given I login with correct username and password
        And I choose to import a source starting from the start page
        And I open the file dialog to import
        And I select file 'AERIUS_gml_landbouw_energie_bron.gml' to be imported
        When I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Landbouw' and sector 'Stalemissies' is correctly saved
        And location with coordinates 'X:136319,74 Y:577666,32' is correctly saved
        And source characterstics with heat content type 'Niet geforceerd', emission height '15,0', heat content '25,000' and diurnal variation type 'Dierverblijven' is correctly saved
        When I select the source 'Bron 2'
        Then the source 'Bron 2' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:52884,22 Y:430148,88' is correctly saved
        And source characterstics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And emission fields with NOx '1500,0' 'kg/j' and NH3 '1800,0' 'kg/j' are correctly saved
        When I copy the source 'Bron 2'
        And I select the source 'Bron 2 (1)'
        Then the source 'Bron 2 (1)' with sector group 'Energie' is correctly saved
        And location with coordinates 'X:52884,22 Y:430148,88' is correctly saved
        And source characterstics with heat content type 'Niet geforceerd', emission height '40,0', heat content '0,220' and diurnal variation type 'Standaard Profiel Industrie' is correctly saved
        And emission fields with NOx '1500' 'kg/j' and NH3 '1800' 'kg/j' are correctly saved
