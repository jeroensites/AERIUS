/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

class roadTransportationSourcePage {

    static chooseRoadTransportationElevation(roadElevationType, elevationHeight) {
        cy.get('[id="collapsibleSRM2Characteristics"]').click()
        cy.get('[id="roadElevationType"]').select((roadElevationType))
        cy.get('[id="elevationHeight"]').clear().type((elevationHeight))
    }

    static chooseRoadTransportationBarrierFromAToB(barrierType, barrierHeight, barrierDistance) {
        cy.get('[id="roadSideBarrierLeftType"]').select((barrierType))
        cy.get('[id="barrierLeftHeight"]').clear().type((barrierHeight))
        cy.get('[id="barrierLeftDistance"]').clear().type((barrierDistance))
    }

    static chooseRoadTransportationBarrierFromBToA(barrierType, barrierHeight, barrierDistance) {
        cy.get('[id="roadSideBarrierRightType"]').select((barrierType))
        cy.get('[id="barrierRightHeight"]').clear().type((barrierHeight))
        cy.get('[id="barrierRightDistance"]').clear().type((barrierDistance))
    }

    static chooseDirection(direction) {
        cy.get('[id="collapsibleSubSources"]').click()
        switch(direction){
            case 'Beide richtingen':
                cy.get('[id="emissionSourceRoadToggleDirectionBoth"]').click()
            break;
            case 'Van A naar B':
                cy.get('[id="emissionSourceRoadToggleDirectionAToB"]').click()
            break;
            case 'Van B naar A':
                cy.get('[id="emissionSourceRoadToggleDirectionBToA"]').click()
            break;
        }
    }

    static addEmissionSourceOfType(emissionSourceType) {
        cy.get('[id="sourceListButtonNewSource"]').click()
        switch(emissionSourceType){
            case 'Voorgeschreven factoren':
                cy.get('[id="tab-STANDARD"]').click()
            break;
            case 'Eigen specificatie':
                cy.get('[id="tab-CUSTOM"]').click()
            break;
        }
    }

    static chooseMaxSpeed(maxSpeed) {
        cy.get('[id="emissionSourceRoadMaxSpeed"]').select(maxSpeed)
    }

    static chooseTimeUnit(timeUnit) {
        cy.get('[id="timeUnit"]').select(timeUnit)
    }

    static chooseTrafficTypeNumberVehiclesAndStagnation(trafficType, numberOfVehicles, stagnationPercentage) {
        switch(trafficType){
            case 'Licht verkeer':
                cy.get('[id="numberOfVehiclesRow_LIGHT_TRAFFIC"]').clear().type(numberOfVehicles)
                cy.get('[id="EmissionSourceRoadStagnationRow_LIGHT_TRAFFIC"]').clear().type(stagnationPercentage)
            break;
            case 'Middelzwaar vrachtverk.':
                cy.get('[id="numberOfVehiclesRow_NORMAL_FREIGHT"]').clear().type(numberOfVehicles)
                cy.get('[id="EmissionSourceRoadStagnationRow_NORMAL_FREIGHT"]').clear().type(stagnationPercentage)
            break;
            case 'Zwaar vrachtverkeer':
                cy.get('[id="numberOfVehiclesRow_HEAVY_FREIGHT"]').clear().type(numberOfVehicles)
                cy.get('[id="EmissionSourceRoadStagnationRow_HEAVY_FREIGHT"]').clear().type(stagnationPercentage)
            break;
            case 'Busverkeer':
                cy.get('[id="numberOfVehiclesRow_AUTO_BUS"]').clear().type(numberOfVehicles)
                cy.get('[id="EmissionSourceRoadStagnationRow_AUTO_BUS"]').clear().type(stagnationPercentage)
        break;
        }
    }

    static sourceCharacteristicsCorrectlySaved(roadType, tunnelFactor, elevationType, elevationHeight) {
        cy.get('[id="srm2DetailRoadType"]').should('have.text',(roadType))
        cy.get('[id="srm2DetailTunnelFactor"]').should('have.text',(tunnelFactor))
        cy.get('[id="srm2DetailElevation"]').should('have.text',(elevationType))
        cy.get('[id="srm2DetailElevationHeight"]').should('have.text', (elevationHeight))
    }

    static barrierAToBCorrectlySaved(barrierType, barrierHeight, barrierDistance) {
        cy.get('[id="srm2DetailBarrierTypeLeft"]').should('have.text',(barrierType))
        cy.get('[id="srm2DetailBarrierHeightLeft"] > span:first').should('have.text',(barrierHeight))
        cy.get('[id="srm2DetailBarrierDistanceLeft"] > span:first').should('have.text',(barrierDistance))
    }

    static barrierBToACorrectlySaved(barrierType, barrierHeight, barrierDistance) {
        cy.get('[id="srm2DetailBarrierTypeRight"]').should('have.text',(barrierType))
        cy.get('[id="srm2DetailBarrierHeightRight"] > span:first').should('have.text',(barrierHeight))
        cy.get('[id="srm2DetailBarrierDistanceRight"] > span:first').should('have.text',(barrierDistance))
    }

    static directionCorrectlySaved(direction) {
        cy.get('[id="srm2DetailDrivingDirection"]').should('have.text',(direction))
    }

    static maxSpeedCorrectlySaved(maxSpeed) {
        cy.get('[id="srm2DetailRoadStandardVehicleTypeTitle"]').should('have.text',(maxSpeed))
    }

    static trafficTypeNumberVehiclesAndStagPercentageCorrectlySaved(trafficType, numberOfVehicles, stagnationPercentage) {
        switch(trafficType){
            case 'Licht verkeer':
                cy.get(':nth-child(2) > #srm2DetailRoadStandardVehicleTimeUnit').should('have.text',(numberOfVehicles))
                cy.get(':nth-child(2) > #srm2DetailRoadStandardVehicleStagnation').should('have.text',(stagnationPercentage))
            break;
            case 'Middelzwaar vrachtverk.':
            cy.get(':nth-child(3) > #srm2DetailRoadStandardVehicleTimeUnit').should('have.text',(numberOfVehicles))
            cy.get(':nth-child(3) > #srm2DetailRoadStandardVehicleStagnation').should('have.text',(stagnationPercentage))
            break;
            case 'Zwaar vrachtverkeer':
            cy.get(':nth-child(4) > #srm2DetailRoadStandardVehicleTimeUnit').should('have.text',(numberOfVehicles))
            cy.get(':nth-child(4) > #srm2DetailRoadStandardVehicleStagnation').should('have.text',(stagnationPercentage))
            break;
            case 'Busverkeer':
            cy.get(':nth-child(5) > #srm2DetailRoadStandardVehicleTimeUnit').should('have.text',(numberOfVehicles))
            cy.get(':nth-child(5) > #srm2DetailRoadStandardVehicleStagnation').should('have.text',(stagnationPercentage))
        break;
        }
    }

    static fillDescription(description) {
    	cy.get('[id="emissionSourceRoadCustomDescription"]').clear().type((description))
	}

	static chooseTimeUnitAndNumberOfVehicles(timeUnit, numberOfVehicles) {
	    cy.get('[id="timeUnit"]').select(timeUnit)
		cy.get('[id="numberOfVehiclesInput"]').type((numberOfVehicles))
	}
	
	static fillEmissionAndFraction(emission, fraction) {
	 switch(emission) {
            case 'NO2':
              cy.get('[id="emissionFactorInput_NO2"]').clear().type(fraction)
	          break;
	       case 'NOX':
	          cy.get('[id="emissionFactorInput_NOX"]').clear().type(fraction)
	          break;
	       case 'NH3':
	          cy.get('[id="emissionFactorInput_NH3"]').clear().type(fraction)
	          break;
	  }
	}
	
	/* This function does not start with a clear, only text input
	*  when we execute the clear the form / DOM is switchted so the
	*  input wil change	
	*/
	static fillEmissionAndFractionForEuroClass(emission, fraction) {
	 switch(emission) {
            case 'NO2':
              cy.get('[id="emissionFactorInput_NO2"]').type(fraction)
	          break;
	       case 'NOX':
	          cy.get('[id="emissionFactorInput_NOX"]').type(fraction)
	          break;
	       case 'NH3':
	          cy.get('[id="emissionFactorInput_NH3"]').type(fraction)
	          break;
	  }
	}
	
	static descriptionCorrectlySaved(description) {
		cy.get('[v-debug="srm2DetailRoadCustomDescription-0"]').should('have.text', description)
	}
	
	static timeUnitAndNumberOfVehiclesIsCorrectlySaved(timeUnit, numberOfVehicles) {
		cy.get('[v-debug="srm2DetailRoadCustomVehicles-0"]').should('have.text', (numberOfVehicles + " " +timeUnit))
	}
	
	static timeUnitAndNumberOfVehiclesForEuroClassIsCorrectlySaved(timeUnit, numberOfVehicles) {
		cy.get('[v-debug="srm2DetailRoadSpecificVehicles-0"]').should('have.text', (numberOfVehicles + " " +timeUnit))
	}
	
	static emissionAndFractionIsCorrectlySaved(emission, fraction) {
		 switch(emission) {
            case 'NO2':
              cy.get('[v-debug="srm2DetailRoadCustomEmissionValue-0-NO2"]').should('have.text', (fraction))
	          break;
	       case 'NOX':
	          cy.get('[v-debug="srm2DetailRoadCustomEmissionValue-0-NOX"]').should('have.text', (fraction))
	          break;
	       case 'NH3':
	          cy.get('[v-debug="srm2DetailRoadCustomEmissionValue-0-NH3"]').should('have.text', (fraction))
	          break;
	  }
	}
	
	static chooseEuroClassFromCustom(euroClass) {
		cy.get('[id="euroclass"]').select(euroClass)
		cy.wait(500) // form is switched wait a moment
    }
    
	static chooseEuroClassFromSpecific(euroClass) {
		cy.get('[id="onRoadMobileSourceCategories"]').select(euroClass)
		cy.wait(500) // form is switched wait a moment
    }

	static euroClassIsCorrectlySaved(euroClass) {
		cy.get('[id="srm2DetailRoadSpecificVehicleCode"]').should('have.text', (euroClass))
    }

}

export default roadTransportationSourcePage
