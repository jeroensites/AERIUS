/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import {And, Given, Then, When} from 'cypress-cucumber-preprocessor/steps'
import agricultureSourcePage from './AgricultureSourcePage'
import commonPage from '../../common/CommonPage'

Given('I login with correct username and password', () => {
    commonPage.visitAndLogin()
})

Then('I add a stable system with RAV code {string}, number of animals {string} and BWL code {string}', (ravCode, numberOfAnimals, bwlCode) => {
    agricultureSourcePage.addStableSystemWithRavCodeNumberOfAnimalsBwlCode(ravCode, numberOfAnimals, bwlCode)
})

Then('I edit the stable system to RAV code {string}, number of animals {string} and BWL code {string}', (ravCode, numberOfAnimals, bwlCode) => {
    agricultureSourcePage.editStableSystemWithRavCodeNumberOfAnimalsBwlCode(ravCode, numberOfAnimals, bwlCode)
})

Then('the RAV code is correctly created and visible in the UI', () => {
    agricultureSourcePage.ravCodeCreatedAndVisible()
})

Then ('the stable system with RAV code {string}, number of animals {string}, factor {string}, reduction {string} and emission {string} is correctly created', (ravCode, numberOfAnimals, factor, reduction, emission) => {
    agricultureSourcePage.stableSystemWithRavCodeCreated(ravCode, numberOfAnimals, factor, reduction, emission)
})

Then ('the additional rule of type {string} with code {string}, number of animals {string}, factor {string}, reduction {string} and emission {string} is correctly created', (typeOfAdditionalRule, addRuleCode, numberOfAnimals, factor, reduction, emission) => {
    agricultureSourcePage.additionalRuleCreated(typeOfAdditionalRule, addRuleCode, numberOfAnimals, factor, reduction, emission)
})

Then('the RAV code is not visible', () => {
    agricultureSourcePage.ravCodeNotVisible()
})

Then('the RAV code row count has {string} rows', (count) => {
    agricultureSourcePage.ravRowCount(count)
})

Then('I select stable system {string}', (nameStableSystem) => {
    agricultureSourcePage.selectStableSystemWithName(nameStableSystem)
})

Then('I delete the stable system', () => {
    agricultureSourcePage.deleteStableSystem()
})

Then('I the deleted stable system is not visible', () => {
    agricultureSourcePage.deletedStableSystemIsNotVisible()
})

Then('I copy the stable system', () => {
    agricultureSourcePage.copyStableSystem()
})

Then('the stable system with RAV code {string} is copied', (stableSystemWithRavCode) => {
    agricultureSourcePage.stableSystemIsCopied(stableSystemWithRavCode)
})

Then('I add an additional rule of type {string} with code {string}', (typeOfAdditionalRule, codeAdditionalRule) => {
    agricultureSourcePage.addAdditionalRuleWithCode(typeOfAdditionalRule, codeAdditionalRule)
})

Then('I add an additional rule of type {string} with code {string} and BWL code {string}', (typeOfAdditionalRule, codeAdditionalRule, bwlCode) => {
    agricultureSourcePage.addAdditionalRuleWithCodeAndBwlCode(typeOfAdditionalRule, codeAdditionalRule, bwlCode)
})

When('I add an invalid additional rule of type Additional technique', () => {
    agricultureSourcePage.addInvalidAdditionalRuleAdditionalTechnique()
})

When('I add an valid additional rule of type Additional technique', () => {
    agricultureSourcePage.addValidAdditionalRuleAdditionalTechnique()
})

When('I add an invalid additional rule of type Emission reducing technique', () => {
    agricultureSourcePage.addInvalidAdditionalRuleEmissionReducingTechnique()
})

When('I add an invalid additional rule of type Fodder and management rule', () => {
    agricultureSourcePage.addInvalidAdditionalRuleFodderManagementRule()
})

Then('a warning is visible that the combination is not aloud', () => {
    agricultureSourcePage.warningCombinationNotAloud()
})

When('I delete the additional rule', () => {
    agricultureSourcePage.deleteAdditionalRule()
})

Then('the additional rule of type {string} with code {string} is deleted', (addRuleType, addRuleCode) => {
    agricultureSourcePage.additionalRuleIsDeleted(addRuleType, addRuleCode)
})

When('I delete all the invalid combinations', () => {
    agricultureSourcePage.deleteAllInvalidCombinations()
})

Then('the warning is not visible that the combination is not aloud', () => {
    agricultureSourcePage.warningNotVisibleCombinationNotAloud()
})

Then('the RAV code and additional rule are correctly created and visible in the UI', () => {
    agricultureSourcePage.ravCodeAndadditionalRuleCreatedAndVisible()
})

Then('I add a stable system with a RAV code and add multiple additional rules', () => {
    agricultureSourcePage.addRAVCodeAndMultipleAdditionalRules()
})

Then('the RAV code and multiple additional rules are correctly created and visible in the UI', () => {
    agricultureSourcePage.ravCodeAndMultipleAdditionalRuleCreatedAndVisible()
})

When('I cancel adding the stable system', () => {
    agricultureSourcePage.cancelAddingStableSystem()
})

Then('I edit the fields of the source', () => {
    agricultureSourcePage.editFieldsOfSource()
})

Then('the source is modified and visible', () => {
    agricultureSourcePage.validateSourceModifiedAndVisible()
})

Then('I add a stable system with custom specifications', () => {
    agricultureSourcePage.addStableSystemWithCustomSpecification()
})

When('I edit the custom specifications', () => {
    agricultureSourcePage.editCustomSpecifications()
})

And ('I add a stable system', () => {
    agricultureSourcePage.addStableSystem()
})

And ('I choose stable system of type {string}', (typeStableSystem) => {
    agricultureSourcePage.chooseStableSystemOfType(typeStableSystem)
})

When ('I add a stable system and fill in the custom specification fields with the following data',(dataTable) => {
    agricultureSourcePage.fillInFieldsStableSystemCustomSpec(dataTable)
})

Then ('the following stable systems with custom specification are created',(dataTable) => {
    agricultureSourcePage.stableSystemCustomSpecCreated(dataTable)
})

Then ('the stable systems contains the following icons and data in the list',(dataTable) => {
    agricultureSourcePage.stableSystemContainsFollowingIconAndData(dataTable)
})

And ('I fill in the fields with description {string}, animal type {string}, factor {string} and number of animals {string}', (description, animalType, factor, numberOfAnimals) => {
    agricultureSourcePage.fillInFieldsSpecificStableSystemCustomSpec(description, animalType, factor, numberOfAnimals)
})

And ('the custom specification with description {string}, number of animals {string}, factor {string}, reduction {string} and emission {string} is created', (description, numberOfAnimals, factor, reduction, emission) => {
    agricultureSourcePage.specificStableSystemCustomSpec(description, numberOfAnimals, factor, reduction, emission)
})

And ('I add a new farmland source', () => {
    agricultureSourcePage.addNewFarmlandSource ()
})

And ('I fill the farmland category with {string}, NOX with value {string} and NH3 with value {string}', (category, NOX, NH3) => {
    agricultureSourcePage.fillInFieldsFarmlandSource (category, NOX, NH3)
})

And ('farmland category {string} with NOX {string} {string} and NH3 {string} {string} is correctly created', (category, NOX, NOXUnit, NH3, NH3Unit) => {
    agricultureSourcePage.farmlandSourceCorrectlySave (category, NOX, NOXUnit, NH3, NH3Unit)
})
