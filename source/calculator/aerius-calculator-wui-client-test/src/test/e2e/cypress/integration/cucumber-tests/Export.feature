#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Export

    Scenario: Export GML
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        When I choose to export through the menu
        And I choose the export option 'GML'
        And I choose to export situation 'Situatie 1' with calculation results is 'true'
        When I choose to add extra information for GML
        And I fill in the form with legal entity 'Corporation X', project name 'Project X', address 'Test street 123', postal code '1234 AB', place 'Testcity' and description 'xxx'
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export
        Then the following notification is displayed 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.'
        # And the following notification is displayed 'Het bestand is gereed. Download bestand.'

    Scenario: Export PDF
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I choose heat content type 'Niet geforceerd', emission height '15' and heat content '20'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        When I choose to export through the menu
        And I choose the export option 'PDF'
        When I fill in the form with legal entity 'Corporation X', project name 'Project X', address 'Test street 123', postal code '1234 AB', place 'Testcity' and description 'xxx'
        And I fill in the following email address 'test@aerius.nl'
        And I click on the button to export
        Then the following notification is displayed 'Uw export is succesvol gestart. U ontvangt een e-mail met downloadlink van de bestanden wanneer deze klaar is.'
        # And the following notification is displayed 'Het bestand is gereed. Download bestand.'

        # Scenario: Export GML with multiple sources (after importing GML with multiple sources)
        # Scenario: Export PDF with multiple sources (after importing GML with multiple sources)
