#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: GenericUI

    Scenario: Switch language to English
        Given I login with correct username and password
        And I switch language 'English'
        Then the current language should be 'English'
        And I switch language 'Nederlands'
        Then the current language should be 'Nederlands'

    #Item 121, Generic UI: tests to check generic user interface items, such as loging in, creating sources etcetera.
