#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: Road transportation

    Scenario: Create new road transportation source - freeways-standard
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Wegverkeer' and sector 'Snelwegen'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I choose road elevation type 'Normaal' and elevation height '0'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '0' and barrier distance '0'
        And I choose as road side barrier type from B to A 'Scherm', barrier height '0' and barrier distance '0'
        And I choose direction 'Beide richtingen'
        And I click to add a emission source of type 'Voorgeschreven factoren'
        And I choose maximum speed '100 km/uur'
        And I choose time unit 'p/uur'
        And I choose for traffic type 'Licht verkeer' the number of vehicles '0' and stagnation percentage '0'
        And I choose for traffic type 'Middelzwaar vrachtverk.' the number of vehicles '0' and stagnation percentage '0'
        And I choose for traffic type 'Zwaar vrachtverkeer' the number of vehicles '0' and stagnation percentage '0'
        And I choose for traffic type 'Busverkeer' the number of vehicles '0' and stagnation percentage '0'
        When I save the data and validate the warning
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Wegverkeer' and sector 'Snelwegen' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characterstics with road type 'Snelwegen', tunnel factor '1', elevation type 'Normaal' and elevation height '0' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '0,0 m' and barrier distance '0,0 m' is correctly saved
        And road side barrier type from B to A 'Scherm', barrier height '0,0 m' and barrier distance '0,0 m' is correctly saved
        And direction 'Beide richtingen' is correctly saved
        And maximum speed '100 km/uur' is correctly saved
        And traffic type 'Licht verkeer' with number of vehicles '0 p/uur' and stagnation percentage '0,0 %' is correctly saved
        And traffic type 'Midelzwaar vrachtverk.' with number of vehicles '0 p/uur' and stagnation percentage '0,0 %' is correctly saved
        And traffic type 'Zwaar vrachtverkeer' with number of vehicles '0 p/uur' and stagnation percentage '0,0 %' is correctly saved
        And traffic type 'Busverkeer' with number of vehicles '0 p/uur' and stagnation percentage '0,0 %' is correctly saved
        # emission

    Scenario: Create new road transportation source - non urban roads-standard
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Wegverkeer' and sector 'Buitenwegen'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I choose road elevation type 'Normaal' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '40'
        And I choose direction 'Van A naar B'
        And I click to add a emission source of type 'Voorgeschreven factoren'
        And I choose time unit 'p/etmaal'
        And I choose for traffic type 'Licht verkeer' the number of vehicles '100' and stagnation percentage '10'
        And I choose for traffic type 'Middelzwaar vrachtverk.' the number of vehicles '200' and stagnation percentage '20'
        And I choose for traffic type 'Zwaar vrachtverkeer' the number of vehicles '300' and stagnation percentage '30'
        And I choose for traffic type 'Busverkeer' the number of vehicles '400' and stagnation percentage '40'
        When I save the data and validate the warning
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Wegverkeer' and sector 'Buitenwegen' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characterstics with road type 'Buitenwegen', tunnel factor '1', elevation type 'Normaal' and elevation height '20' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van A naar B' is correctly saved
        And traffic type 'Licht verkeer' with number of vehicles '100 p/etmaal' and stagnation percentage '10,0 %' is correctly saved
        And traffic type 'Midelzwaar vrachtverk.' with number of vehicles '200 p/etmaal' and stagnation percentage '20,0 %' is correctly saved
        And traffic type 'Zwaar vrachtverkeer' with number of vehicles '300 p/etmaal' and stagnation percentage '30,0 %' is correctly saved
        And traffic type 'Busverkeer' with number of vehicles '400 p/etmaal' and stagnation percentage '40,0 %' is correctly saved
        # emission

    Scenario: Create new road transportation source - urban roads-standard
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Wegverkeer' and sector 'Binnen bebouwde kom'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I choose road elevation type 'Normaal' and elevation height '20'
        And I choose as road side barrier type from A to B 'Wal', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Scherm', barrier height '20' and barrier distance '40'
        And I choose direction 'Van B naar A'
        And I click to add a emission source of type 'Voorgeschreven factoren'
        And I choose time unit 'p/maand'
        And I choose for traffic type 'Licht verkeer' the number of vehicles '50000' and stagnation percentage '100'
        And I choose for traffic type 'Middelzwaar vrachtverk.' the number of vehicles '100000' and stagnation percentage '100'
        And I choose for traffic type 'Zwaar vrachtverkeer' the number of vehicles '150000' and stagnation percentage '100'
        And I choose for traffic type 'Busverkeer' the number of vehicles '200000' and stagnation percentage '100'
        When I save the data and validate the warning
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Wegverkeer' and sector 'Binnen bebouwde kom' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characterstics with road type 'Binnen bebouwde kom', tunnel factor '1', elevation type 'Normaal' and elevation height '20' is correctly saved
        And road side barrier type from A to B 'Wal', barrier height '10,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Scherm', barrier height '20,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van B naar A' is correctly saved
        And traffic type 'Licht verkeer' with number of vehicles '50000 p/maand' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Midelzwaar vrachtverk.' with number of vehicles '100000 p/maand' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Zwaar vrachtverkeer' with number of vehicles '150000 p/maand' and stagnation percentage '100,0 %' is correctly saved
        And traffic type 'Busverkeer' with number of vehicles '200000 p/maand' and stagnation percentage '100,0 %' is correctly saved
        # emission

    Scenario: Create new road transportation source - freeways-standard - custom specification
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Wegverkeer' and sector 'Snelwegen'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I choose road elevation type 'Normaal' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '40'
        And I choose direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I fill description with 'Eigen weg specificatie'
        And I choose time unit 'p/etmaal' with number of vehicles '100'
        # fields does not accept ,
        And I fill emission 'NO2' per vehicle '0.710'
        And I fill emission 'NOX' per vehicle '0.8101'
        And I fill emission 'NH3' per vehicle '0.91011'
        When I save the data and validate the warning
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Wegverkeer' and sector 'Snelwegen' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characterstics with road type 'Snelwegen', tunnel factor '1', elevation type 'Normaal' and elevation height '20' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van A naar B' is correctly saved
        And description 'Eigen weg specificatie' is correctly saved
        And time unit 'p/etmaal' with number of vehicles '100' is correctly saved
        # value is displayed with one digit
        And emission 'NO2' per vehicle '0,7 g/km' is correctly saved
        And emission 'NOX' per vehicle '0,8 g/km' is correctly saved
        And emission 'NH3' per vehicle '0,9 g/km' is correctly saved

    Scenario: Create new road transportation source - non urban roads-standard - custom specification
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Wegverkeer' and sector 'Buitenwegen'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I choose road elevation type 'Normaal' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '40'
        And I choose direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I fill description with 'Eigen weg specificatie'
        And I choose time unit 'p/etmaal' with number of vehicles '100'
        # fields does not accept ,
        And I fill emission 'NO2' per vehicle '0.710'
        And I fill emission 'NOX' per vehicle '0.8101'
        And I fill emission 'NH3' per vehicle '0.91011'
        When I save the data and validate the warning
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Wegverkeer' and sector 'Buitenwegen' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characterstics with road type 'Buitenwegen', tunnel factor '1', elevation type 'Normaal' and elevation height '20' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van A naar B' is correctly saved
        And description 'Eigen weg specificatie' is correctly saved
        And time unit 'p/etmaal' with number of vehicles '100' is correctly saved
        # value is displayed with one digit
        And emission 'NO2' per vehicle '0,7 g/km' is correctly saved
        And emission 'NOX' per vehicle '0,8 g/km' is correctly saved
        And emission 'NH3' per vehicle '0,9 g/km' is correctly saved
	
    Scenario: Create new road transportation source - urban roads-standard - custom specification
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Wegverkeer' and sector 'Binnen bebouwde kom'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I choose road elevation type 'Normaal' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '40'
        And I choose direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I fill description with 'Eigen weg specificatie'
        And I choose time unit 'p/etmaal' with number of vehicles '100'
        # fields does not accept ,
        And I fill emission 'NO2' per vehicle '0.710'
        And I fill emission 'NOX' per vehicle '0.8101'
        And I fill emission 'NH3' per vehicle '0.91011'
        When I save the data and validate the warning
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Wegverkeer' and sector 'Binnen bebouwde kom' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characterstics with road type 'Binnen bebouwde kom', tunnel factor '1', elevation type 'Normaal' and elevation height '20' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van A naar B' is correctly saved
        And description 'Eigen weg specificatie' is correctly saved
        And time unit 'p/etmaal' with number of vehicles '100' is correctly saved
        # value is displayed with one digit
        And emission 'NO2' per vehicle '0,7 g/km' is correctly saved
        And emission 'NOX' per vehicle '0,8 g/km' is correctly saved
        And emission 'NH3' per vehicle '0,9 g/km' is correctly saved

    Scenario: Create new road transportation source - non urban roads-standard - Euro class
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Wegverkeer' and sector 'Buitenwegen'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I choose road elevation type 'Normaal' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '40'
        And I choose direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I choose Euro class 'Bestelauto - LPG - Euro-4' from custom
        And I choose time unit 'p/etmaal' with number of vehicles '100'
        When I save the data and validate the warning
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Wegverkeer' and sector 'Buitenwegen' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characterstics with road type 'Buitenwegen', tunnel factor '1', elevation type 'Normaal' and elevation height '20' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van A naar B' is correctly saved
        # test on euro class code but we exspect the description
        And Euro class 'LBALEUR4' is correctly saved
        And time unit 'p/etmaal' with number of vehicles '100' for Euro class is correctly saved


    Scenario: Create new road transportation source - freeways-standard - Euro class modify emission value switch to custom
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Wegverkeer' and sector 'Snelwegen'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I choose road elevation type 'Normaal' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '40'
        And I choose direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I choose Euro class 'Bestelauto - LPG - Euro-4' from custom
        And I choose time unit 'p/etmaal' with number of vehicles '100'
        # enter any value it will switch the form from Euro to Custom.
        And I fill emission 'NO2' per vehicle '0' for Euro class
        # add a wait for DOM form will be switched to custom value
        And I wait '100'
        And I fill emission 'NO2' per vehicle '0.1101'
        And I fill emission 'NOX' per vehicle '0.2101'
        And I fill emission 'NH3' per vehicle '0.31011'
        And I fill description with 'Eigen weg specificatie'
        When I save the data and validate the warning
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Wegverkeer' and sector 'Snelwegen' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characterstics with road type 'Snelwegen', tunnel factor '1', elevation type 'Normaal' and elevation height '20' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van A naar B' is correctly saved
        And description 'Eigen weg specificatie' is correctly saved
        And time unit 'p/etmaal' with number of vehicles '100' is correctly saved
        # value is displayed with one digit
        And emission 'NO2' per vehicle '0,1 g/km' is correctly saved
        And emission 'NOX' per vehicle '0,2 g/km' is correctly saved
        And emission 'NH3' per vehicle '0,3 g/km' is correctly saved
    
    Scenario: Create new road transportation source - freeways-standard - Modify Euro class 'Other' switch to custom
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Wegverkeer' and sector 'Snelwegen'
        And I fill location with coordinates 'LINESTRING(136506.56 584497.6,145108.16 592669.12,159300.8 593959.36)'
        And I choose road elevation type 'Normaal' and elevation height '20'
        And I choose as road side barrier type from A to B 'Scherm', barrier height '10' and barrier distance '30'
        And I choose as road side barrier type from B to A 'Wal', barrier height '20' and barrier distance '40'
        And I choose direction 'Van A naar B'
        And I click to add a emission source of type 'Eigen specificatie'
        And I choose Euro class 'Bestelauto - LPG - Euro-4' from custom
        And I choose time unit 'p/etmaal' with number of vehicles '100'
        And I choose Euro class 'Trekker - diesel - zwaar - Euro-5 Step G - SCR - licht' from specific
        # select empty value will switch the form from Euro to Custom.
        And I choose Euro class 'Anders' from specific
        And I fill emission 'NO2' per vehicle '0.1101'
        And I fill emission 'NOX' per vehicle '0.2101'
        And I fill emission 'NH3' per vehicle '0.31011'
        And I fill description with 'Eigen weg specificatie'
        When I save the data and validate the warning
        And I select the source 'Verkeersnetwerk'
        And I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Wegverkeer' and sector 'Snelwegen' is correctly saved
        And location with coordinates 'X:146296,7 Y:592777,17' is correctly saved
        And source characterstics with road type 'Snelwegen', tunnel factor '1', elevation type 'Normaal' and elevation height '20' is correctly saved
        And road side barrier type from A to B 'Scherm', barrier height '10,0 m' and barrier distance '30,0 m' is correctly saved
        And road side barrier type from B to A 'Wal', barrier height '20,0 m' and barrier distance '40,0 m' is correctly saved
        And direction 'Van A naar B' is correctly saved
        And description 'Eigen weg specificatie' is correctly saved
        And time unit 'p/etmaal' with number of vehicles '100' is correctly saved
        # value is displayed with one digit
        And emission 'NO2' per vehicle '0,1 g/km' is correctly saved
        And emission 'NOX' per vehicle '0,2 g/km' is correctly saved
        And emission 'NH3' per vehicle '0,3 g/km' is correctly saved

    # time unit p/jaar
