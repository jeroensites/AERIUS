#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: CalculationPointsUI

    Scenario: Create scenario and add calculation point
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        And I select menu calculation point
        And I create a new calculation point 'Rekenpunt 1' with location 'POINT(172203.2 599120.32)'
        And I save the calculation point
        Then I edit the calculation point 'Rekenpunt 1'
        Then the calculation point 'Rekenpunt 1' with location 'POINT(172203.2 599120.32)' is correctly saved
        And I cancel the calculation point
        And I add a new calculation point 'Rekenpunt 2' with location 'POINT(172203 599120)'
        And I save the calculation point
        Then I edit the calculation point 'Rekenpunt 2'
        Then the calculation point 'Rekenpunt 2' with location 'POINT(172203 599120)' is correctly saved
        And I cancel the calculation point
        Then the amount of calculation points is '2'
        Then I delete the calculation point 'Rekenpunt 2'
        Then the amount of calculation points is '1'

    Scenario: Create scenario and add calculation point test invalid input
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        And I select menu calculation point
        And I create a new calculation point 'Rekenpunt 1' with location 'POINT(172203.2 599120.32'
        And I save the calculation point
        Then save is disabled and shows error 'Er zijn foutmeldingen, los deze eerst op.'
        And I cancel the calculation point
        And I create a new calculation point 'Rekenpunt 1' with location 'LINESTRING(106986.57 522346.38,106956.39 521501.52)'
        And I save the calculation point        
        Then save is disabled and shows error 'Er zijn foutmeldingen, los deze eerst op.'
        And I cancel the calculation point
        And I create a new calculation point 'Rekenpunt 1' with location 'POLYGON((106745.18 523130.9,106745.18 522648.12,107227.96 522859.34,106745.18 523130.9))'
        And I save the calculation point        
        Then save is disabled and shows error 'Er zijn foutmeldingen, los deze eerst op.'
        And I cancel the calculation point

    Scenario: Create scenario and import calculation points
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        And I select menu calculation point
        And I choose to import calculation points starting from the calculation points start page
        And I open the file dialog to import
        And I select file 'AERIUS_GML_24_CalculationPoints.gml' to be imported   
        Then the amount of calculation points is '24'

    Scenario: Create scenario and add one calculation point and add calculation points with import
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Energie'
        And I fill location with coordinates 'POINT(172203.2 599120.32)'
        And I fill the emission fields with NOx '10' and NH3 '20'
        And I save the data
        And I select menu calculation point
        And I create a new calculation point 'Rekenpunt 1' with location 'POINT(172203.2 599120.32)'
        And I save the calculation point
        And I choose to import calculation points starting from the calculation points page
        And I open the file dialog to import
        And I select file 'AERIUS_GML_24_CalculationPoints.gml' to be imported   
        Then the amount of calculation points is '25'
