/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given, When, Then, And } from 'cypress-cucumber-preprocessor/steps'
import shippingSourcePage from './ShippingSourcePage'
import commonPage from '../../common/CommonPage'

Given('I login with correct username and password', () => {
    commonPage.visitAndLogin()
})

Given('I fill the maritime shipping description {string} with type {string}, visits {string} time unit {string} and average residence time {string} hour and shorepower usage {string} procent', (description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor) => {
	shippingSourcePage.editMaritimeDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactor(description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor)
})

Given('I fill the maritime shipping description {string} with type {string}, visits {string} time unit {string}', (description, shipCategory, shipPerTimeUnit, timeUnit) => {
	shippingSourcePage.editMaritimeDescriptionCategoryShipPerTimeUnitTimeUnit(description, shipCategory, shipPerTimeUnit, timeUnit)
})

Given('I fill the inland shipping description {string} with type {string}, visits {string} time unit {string} and average residence time {string} hour and shorepower usage {string} procent laden {string} procent', (description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor, percentageLaden) => {
	shippingSourcePage.editInlandDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactorPercentageLaden(description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor, percentageLaden)
})

Given('I fill the inland shipping description {string} with type {string}', (description, shipCategory) => {
	shippingSourcePage.editInlandDescriptionCategory(description, shipCategory)
})

Given('I fill the movement {string} with ships movments {string} time unit {string} percentage laden {string}', (movementType, shipMovements, timeUnit, percentageLaden) => {
	shippingSourcePage.editMovementTypeShipMovementsTimeUnitPercentageLaden(movementType, shipMovements, timeUnit, percentageLaden)
})

Given('I fill waterway {string} and direction {string}', (waterway, direction) => {
	shippingSourcePage.editWaterwayDirection(waterway, direction)
})

Then('maritime ship row {string} description {string} type {string}, visits {string} time unit {string} and average residence time {string} hour and shorepower usage {string} procent is correctly created', (rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor) => {
	shippingSourcePage.maritimeDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactorCreated(rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor)
})

Then('maritime ship row {string} description {string} type {string}, visits {string} time unit {string} is correctly saved', (rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit) => {
	shippingSourcePage.maritimeDescriptionCategoryShipPerTimeUnitTimeUnitCreated(rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit)
})

Then('inland ship row {string} description {string} type {string}, visits {string} time unit {string} and average residence time {string} hour and shorepower usage {string} procent laden {string} procent is correctly saved', (rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor, percentageLaden) => {
	shippingSourcePage.inlandDescriptionCategoryShipPerTimeUnitTimeUnitAverageResidenceTimeShorePowerFactorPercentageLadenCreated(rowNumber, description, shipCategory, shipPerTimeUnit, timeUnit, averageResidenceTime, shorePowerFactor, percentageLaden)
})

Then('inland ship row {string} type {string} is correctly saved', (rowNumber, shipCategory) => {
	shippingSourcePage.inlandDescriptionCategoryCreated(rowNumber, shipCategory)
})

Then('movement row {string} direction {string} with description {string} ships movements {string} time unit {string} percentage laden {string} is correctly saved', (rowNumber, movementType, description, shipMovements, timeUnit, percentageLaden) => {
	shippingSourcePage.movementTypeDescriptionShipMovementsTimeUnitPercentageLadenCreated(rowNumber, movementType, description, shipMovements, timeUnit, percentageLaden)
})

Then('waterway {string} and direction {string} is correctly saved', (waterway, direction) => {
	shippingSourcePage.waterwayDirectionCreated(waterway, direction)
})

Then('I fill dock A with {string}', (dockA) => {
	shippingSourcePage.fillDockA(dockA)
})

Then('I fill dock B with {string}', (dockB) => {
	shippingSourcePage.fillDockB(dockB)
})

Then('dock A with value {string} is correctly saved', (dockA) => {
	shippingSourcePage.dockACorrectlySaved(dockA)
})

Then('dock B with value {string} is correctly saved', (dockB) => {
	shippingSourcePage.dockBCorrectlySaved(dockB)
})

Then ('field {string} is not visible', (notVisibleField) => {
    shippingSourcePage.fieldNotVisible(notVisibleField)
})
