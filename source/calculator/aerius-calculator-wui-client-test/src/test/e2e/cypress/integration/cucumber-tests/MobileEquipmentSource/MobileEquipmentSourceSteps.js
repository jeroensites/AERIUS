/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given, When, Then, And } from 'cypress-cucumber-preprocessor/steps'
import mobileEquipmentSourcePage from './MobileEquipmentSourcePage'
import commonPage from '../../common/CommonPage'

Given('I login with correct username and password', () => {
    commonPage.visitAndLogin()
})

Given ('I fill the mobile equipment description {string} with stage class {string}, fuel usage {string} operation hours per year {string} adblue usage {string}', (description, stageClass, fuelUsage, operationHours, adBlue)=> {
	mobileEquipmentSourcePage.inputDescriptionStageClassFuelUsageOperationHoursAdBlue(description, stageClass, fuelUsage, operationHours, adBlue)
})

Then ('mobile equipment description {string} with stage class {string}, fuel usage {string} operation hours per year {string} adblue usage {string} is correctly saved', (description, stageClass, fuelUsage, operationHours, adBlue)=> {
	mobileEquipmentSourcePage.descriptionStageClassFuelUsageOperationHoursAdBlueCreated(description, stageClass, fuelUsage, operationHours, adBlue)
})

Then ('blue ratio warning is visible', ()=> {
	mobileEquipmentSourcePage.blueRatioWarningisVisible()
})

Then ('blue ratio warning is not visible', ()=> {
	mobileEquipmentSourcePage.blueRatioWarningisNotVisible()
})
