/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given, When, Then, And } from 'cypress-cucumber-preprocessor/steps'
import exportPage from './ExportPage'
import commonPage from '../../common/CommonPage'

Given('I login with correct username and password', () => {
    commonPage.visitAndLogin()
})

Given('I choose to export through the menu', () => {
    exportPage.chooseExportThroughMenu()
})

And('I choose the export option {string}', (selectedExportOption) => {
    exportPage.exportOption(selectedExportOption)
})

And('I choose to add extra information for GML', () => {
    exportPage.chooseToAddExtraInformationGML()
})

And('I fill in the following email address {string}', (emailAddress) => {
    exportPage.fillInEmailAdress(emailAddress)
})

When('I fill in the form with legal entity {string}, project name {string}, address {string}, postal code {string}, place {string} and description {string}', (legalEntity, projectName, address, postalCode, place, description) => {
    exportPage.fillInFormPDF(legalEntity, projectName, address, postalCode, place, description)
})

And ('I choose to export situation {string} with calculation results is {string}', (situationName, calculationResult) => {
    exportPage.exportSituationCalculationResult(situationName, calculationResult)
})

And('I click on the button to export', () => {
    exportPage.clickOnExportButton()
})
