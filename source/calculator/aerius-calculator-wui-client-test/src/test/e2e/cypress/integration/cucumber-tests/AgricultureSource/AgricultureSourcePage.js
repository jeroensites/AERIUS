/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

// import { forEach } from "cypress/types/lodash"

class agricultureSourcePage {

    static addStableSystemWithRavCodeNumberOfAnimalsBwlCode(ravCode, numberOfAnimals, bwlCode) {
        //Stable systems, animals and numbers
        cy.get('[id="collapsibleSubSources"] > .line > .title').click({ force: true })
        cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true })
        //RAV code
        cy.get('[id="tab-STANDARD"]').click({ force: true })
        cy.get('[id="lodgingCodes"] > .item').type((ravCode))
        cy.get('.modal > :nth-child(1) > :nth-child(1) > .item').should('contain',(ravCode)).click()
        cy.get('[id="numberOfAnimals"]').type((numberOfAnimals))
        cy.get('[id="bwlCodeListBox"]').select(bwlCode)
    }

    static editStableSystemWithRavCodeNumberOfAnimalsBwlCode(ravCode, numberOfAnimals, bwlCode) {
        //RAV code
        // cy.get('[id="emissionSourceFarmlodgingToggleStandard"]').click({ force: true })
        cy.get('[id="lodgingCodes"] > .item').clear().type((ravCode))
        cy.get('.modal > :nth-child(1) > :nth-child(1) > .item').should('contain',(ravCode)).click()
        cy.get('[id="numberOfAnimals"]').clear().type((numberOfAnimals))
        cy.get('[id="bwlCodeListBox"]').select(bwlCode)
    }

    static ravCodeCreatedAndVisible(){
        //Validate Stable systems, animals and numbers
        cy.get('.detailRows > .detailRow > .ravCodeColumn').should('have.text','E1.5.1')
        cy.get('.detailRows > .detailRow > .amountColumn').should('have.text','100')
        cy.get('.detailRows > .detailRow > .factorColumn').should('have.text','0,02')
        cy.get('.detailRows > .detailRow > .reductionColumn').should('have.text','-')
        cy.get('.detailRows > .detailRow > .emissionColumn').should('have.text','2,0')
        cy.get('.detailDescription').should('have.text','E1.5.1 mestbandbatterij met geforceerde mestdroging; mestbandbatterij voor droge mest met geforceerde mestdroging  (Kippen; opfokhennen en hanen van legrassen; jonger dan 18 weken)')
    }

    static stableSystemWithRavCodeCreated(ravCode, numberOfAnimals, factor, reduction, emission){
        //Validate Stable systems, animals and numbers
        cy.get('[id="farmLodgingDetailStandardLodgingCode"]').should('have.text', ravCode)
        cy.get('[id="farmLodgingDetailStandardNumberOfAnimals"]').should('have.text', numberOfAnimals)
        cy.get('[id="farmLodgingDetailStandardEmissionFactor"]').should('include.text', factor) // have.text
        cy.get('[id="farmLodgingDetailStandardReduction"]').should('have.text', reduction)
        cy.get('[id="farmLodgingDetailStandardEmission"]').should('include.text', emission) // have.text
        cy.get('[id="farmLodgingDetailDescription"]').contains(ravCode)
    }

    static additionalRuleCreated(typeOfAdditionalRule, addRuleCode, numberOfAnimals, factor, reduction, emission){
        //Validate Stable systems, animals and numbers
        switch(typeOfAdditionalRule) {
            case 'Additionele techniek':
                cy.get('[id="farmLodgingDetailAdditionalLodgingCode"]').should('have.text', addRuleCode)
                cy.get('[id="farmLodgingDetailAdditionalNumberOfAnimals"]').should('have.text', numberOfAnimals)
                cy.get('[id="farmLodgingDetailAdditionalFactor"]').should('have.text', factor)
                cy.get('[id="farmLodgingDetailAdditionalReduction"]').should('have.text', reduction)
                cy.get('[id="farmLodgingDetailAdditionalEmission"]').should('have.text', emission)
            break;
            case 'Emissiereducerende techniek':
                cy.get('[id="farmLodgingDetailReductiveLodgingCode"]').should('have.text', addRuleCode)
                cy.get('[id="farmLodgingDetailReductiveAmount"]').should('have.text', numberOfAnimals)
                cy.get('[id="farmLodgingDetailReductiveFactor"]').should('have.text', factor)
                cy.get('[id="farmLodgingDetailReductiveReduction"]').should('have.text', reduction)
                cy.get('[id="farmLodgingDetailReductiveEmission"]').should('have.text', emission)
            break;
            case 'Voer- en of managementmaatregel':
                cy.get('[id="farmLodgingDetailFodderMeasureCode"]').should('have.text', addRuleCode)
                cy.get('[id="farmLodgingDetailFodderMeasureAmount"]').should('have.text', numberOfAnimals)
                cy.get('[id="farmLodgingDetailFodderMeasureFactor"]').should('have.text', factor)
                cy.get('[id="farmLodgingDetailFodderMeasureReduction"]').should('have.text', reduction)
                cy.get('[id="farmLodgingDetailFodderMeasureEmission"]').should('have.text', emission)
            break;
        }
    }

    static selectStableSystemWithName(nameStableSystem){
        cy.get('[id="subSourceListDescription"]').contains(nameStableSystem).click()
    }

    static deleteStableSystem(){
        cy.get('[id="sourceListButtonDeleteSource"] > .icon').click()
    }

    static ravCodeNotVisible(){
        //Validate Stable systems, animals and numbers
        cy.get('.detailRows > .detailRow > .ravCodeColumn').should('not.exist')
        cy.get('.detailRows > .detailRow > .amountColumn').should('not.exist')
        cy.get('.detailRows > .detailRow > .factorColumn').should('not.exist')
        cy.get('.detailRows > .detailRow > .reductionColumn').should('not.exist')
        cy.get('.detailRows > .detailRow > .emissionColumn').should('not.exist')
        cy.get('.detailDescription').should('not.exist')
    }
    
    static ravRowCount(count) {
        //Validate that we only have {count} rows
        cy.get('.detailRows >').should('have.length', count)
    }

    static copyStableSystem() {
        cy.get('[id="sourceListButtonCopySource"]').click()
    }

    static stableSystemIsCopied(stableSystemWithRavCode) {
        cy.get(':nth-child(1) > [data-v-e52b4cfc=""] > .detailRows > #farmLodgingDetailStandardRow-0 > #farmLodgingDetailStandardLodgingCode').contains (stableSystemWithRavCode)
        cy.get(':nth-child(2) > [data-v-e52b4cfc=""] > .detailRows > #farmLodgingDetailStandardRow-0 > #farmLodgingDetailStandardLodgingCode').contains (stableSystemWithRavCode)
    }

    static addAdditionalRuleWithCode(typeOfAdditionalRule, codeAdditionalRule) {
        //add an additional rule
        cy.get('[id="additionalSystemSelector"]').select((typeOfAdditionalRule))
        switch(typeOfAdditionalRule) {
            case 'Additionele techniek':
                cy.get('[id="additionalSystem"] > .item').type((codeAdditionalRule))
            break;
            case 'Emissiereducerende techniek':
                cy.get('[id="reductiveSystem"] > .item').type((codeAdditionalRule))
            break;
            case 'Voer- en of managementmaatregel':
                cy.get('[id="fodderMeasure"] > .item').type((codeAdditionalRule))
            break;
        }
        cy.get('.modal > :nth-child(1) > :nth-child(1) > .item').should('contain',(codeAdditionalRule)).click()
    }

    static addAdditionalRuleWithCodeAndBwlCode(typeOfAdditionalRule, codeAdditionalRule, bwlCode) {
        //add an additional rule
        cy.get('[id="additionalSystemSelector"]').select((typeOfAdditionalRule))
        switch(typeOfAdditionalRule) {
            case 'Additionele techniek':
                cy.get('[id="additionalSystem"] > .item').type((codeAdditionalRule))
                cy.get('.modal > :nth-child(1) > :nth-child(1) > .item').should('contain',(codeAdditionalRule)).click()
                cy.get('[id="additional-lodging-0"]').get('[id="bwlCodeListBox"]').should('contain',(bwlCode)).last().select(bwlCode)
            break;
            case 'Emissiereducerende techniek':
                cy.get('[id="reductiveSystem"] > .item').type((codeAdditionalRule))
                cy.get('.modal > :nth-child(1) > :nth-child(1) > .item').should('contain',(codeAdditionalRule)).click()
                cy.get('[id="reductive-lodging-0"]').get('[id="bwlCodeListBox"]').should('contain',(bwlCode)).last().select(bwlCode)
            break;
        }
    }

    static addInvalidAdditionalRuleAdditionalTechnique() {
        //add an additional rule that is not valid
        cy.get('[id="additionalSystemSelector"]').select('Additionele techniek')
        cy.get('[id="additionalSystem"] > .item').click().type('E 6.10.b biothermisch drogen van pluimveemest met chemisch luchtwassysteem 90% emissiereductie; geldt voor de huisvestingssystemen onder E 2.5, E 2.11, E 2.12, E 4.1 t/m E 4.3 en E 4.8 (Kippen; additionele technieken voor mestbewerking en mestopslag )')
        cy.get('.modal > :nth-child(1) > :nth-child(1) > .item').click()
    }

    static addValidAdditionalRuleAdditionalTechnique() {
        //add an additional rule that is not valid
        cy.get('[id="additionalSystemSelector"]').select('Additionele techniek')
        cy.get('[id="additional-lodging-1"] > [id="additionalSystem"] > .item').click().type('E 6.9.a biothermisch drogen van pluimveemest met chemisch luchtwassysteem 70% emissiereductie; geldt voor geldt voor de huisvestingssystemen onder E 1.5, E 1.8, E 5.8, E 5.9.1.1.3 en E 5.9.1.2.3 (Kippen; additionele technieken voor mestbewerking en mestopslag )')
        cy.get('.modal > :nth-child(1) > :nth-child(1) > .item').click()
    }

    static addInvalidAdditionalRuleEmissionReducingTechnique() {
        //add an additional rule that is not valid
        cy.get('[id="additionalSystemSelector"]').select('Emissiereducerende techniek')
        cy.get('[id="reductiveSystem"] > .item').click().type('A 1.17 mechanisch geventileerde stal met een chemisch luchtwassysteem (Rundvee; melk- en kalfkoeien ouder dan 2 jaar)')
        cy.get('.modal > :nth-child(1) > :nth-child(1) > .item').click()
    }

    static addInvalidAdditionalRuleFodderManagementRule() {
        //add an additional rule that is not valid
        cy.get('[id="additionalSystemSelector"]').select('Voer- en of managementmaatregel')
        cy.get('[id="fodderMeasure"] > .item').click().type('PAS 2015.07-01 Snijmaissilage')
        cy.get('.modal > :nth-child(1) > :nth-child(1) > .item').click()
    }

    static warningCombinationNotAloud(){
        cy.get('.suggestionWarning').should('have.text','Let op: de combinatie van deze systemen is niet toegestaan binnen de RAV systematiek.')
    }

    static deleteAdditionalRule() {
        //delete additional rule
        cy.get('[id="delete"]').click()
    }

    static additionalRuleIsDeleted(addRuleType, addRuleCode){
        switch(addRuleType) {
            case 'Additionele techniek':
                cy.get('[id="farmLodgingDetailStandardAdditionalLodgingCode"]').should('not.exist',(addRuleCode))
            break;
            case 'Emissiereducerende techniek':
                cy.get('[id="farmLodgingDetailStandardReductiveLodgingCode"]').should('not.exist',(addRuleCode))
            break;
            case 'Voer- en of managementmaatregel':
                cy.get('[id="farmLodgingDetailStandardFodderMeasureCode"]').should('not.exist',(addRuleCode))
            break;
        }

    }

    static deleteAllInvalidCombinations() {
        //delete invalid combinations
        cy.get('[id="additional-lodging-0"] > .tooltipContainer > .container > [id="delete"]').click()
        cy.get('[id="reductive-lodging-0"] > .tooltipContainer > .container > [id="delete"]').click()
        cy.get('[id="fodder-measure-0"] > .tooltipContainer > .container > [id="delete"]').click()
    }

    static warningNotVisibleCombinationNotAloud(){
        cy.get('.suggestionWarning').should('not.exist')
    }

    static ravCodeAndadditionalRuleCreatedAndVisible(){
        //Validate Stable systems, animals and numbers
        cy.get(':nth-child(1) > .ravCodeColumn').should('have.text','E1.5.1')
        cy.get(':nth-child(1) > .amountColumn').should('have.text','100')
        cy.get(':nth-child(1) > .factorColumn').should('have.text','0,02')
        cy.get(':nth-child(1) > .reductionColumn').should('have.text','-')
        cy.get(':nth-child(1) > .emissionColumn').should('have.text','2,0')
        cy.get('.detailDescription').should('have.text','E1.5.1 mestbandbatterij met geforceerde mestdroging; mestbandbatterij voor droge mest met geforceerde mestdroging  (Kippen; opfokhennen en hanen van legrassen; jonger dan 18 weken)')
        //Validate additional rules
        cy.get('.detailRows > :nth-child(2) > .ravCodeColumn').should('have.text','E6.9.a')
        cy.get('.detailRows > :nth-child(2) > .amountColumn').should('have.text','100')
        cy.get('.detailRows > :nth-child(2) > .factorColumn').should('have.text','0,009')
        cy.get('.detailRows > :nth-child(2) > .reductionColumn').should('have.text','-')
        cy.get('.detailRows > :nth-child(2) > .emissionColumn').should('have.text','2,9')
    }

    static addRAVCodeAndMultipleAdditionalRules() {
        //Stable systems, animals and numbers
        cy.get('[id="collapsibleSubSources"] > .line > .title').click()
        cy.get('[id="sourceListButtonNewSource"] > .icon').click()
        //RAV code
        cy.get('[id="tab-STANDARD"]').click()
        cy.get('[id="lodgingCodes"] > .item').type('E 5.9.1.1.1')
        cy.get('[title="E 5.9.1.1.1 uitbroeden eieren en opfokken vleeskuikens tot 13 dagen in stal en vervolghuisvesting in E 5.5 (grondhuisvesting met vloerverwarming en vloerkoeling) (Kippen; vleeskuikens)"]').click()
        cy.get('[id="numberOfAnimals"]').type('100')
        //add multiple additional rules
        cy.get('[id="additionalSystemSelector"]').select('Emissiereducerende techniek')
        cy.get('[id="reductiveSystem"] > .item').click().type('E 5.13 chemisch luchtwassysteem 70% emissiereductie (Kippen; vleeskuikens)')
        cy.get('.modal > :nth-child(1) > :nth-child(1) > .item').click()
        cy.get('[id="reductive-lodging-0"] > #bwlCodeListBox').select('BWL 2008.06')
        cy.get('[id="additionalSystemSelector"]').select('Voer- en of managementmaatregel')
        cy.get('[id="fodderMeasure"] > .item').click().type('PAS 2015.07-01 Snijmaissilage')
        cy.get('.modal > :nth-child(1) > :nth-child(1) > .item').click()
    }

    static cancelAddingStableSystem() {
        cy.get('[id="buttonCancel"]').click()
    }

    static ravCodeAndMultipleAdditionalRuleCreatedAndVisible(){
        //Validate Stable systems, animals and numbers
        cy.get(':nth-child(1) > .ravCodeColumn').should('have.text','E5.9.1.1.1')
        cy.get(':nth-child(1) > .amountColumn').should('have.text','100')
        cy.get(':nth-child(1) > .factorColumn').should('have.text','0,034')
        cy.get(':nth-child(1) > .reductionColumn').should('have.text','-')
        cy.get(':nth-child(1) > .emissionColumn').should('have.text','3,4')
        cy.get('.detailDescription').should('have.text','E5.9.1.1.1 uitbroeden eieren en opfokken vleeskuikens tot 13 dagen in stal en vervolghuisvesting in E 5.5 (grondhuisvesting met vloerverwarming en vloerkoeling) (Kippen; vleeskuikens)')
        //Validate additional rule 1
        cy.get('.detailRows > :nth-child(2) > .ravCodeColumn').should('have.text','E5.13')
        cy.get('.detailRows > :nth-child(2) > .amountColumn').should('have.text','-')
        cy.get('.detailRows > :nth-child(2) > .factorColumn').should('have.text','-')
        cy.get('.detailRows > :nth-child(2) > .reductionColumn').should('have.text','70%')
        cy.get('.detailRows > :nth-child(2) > .emissionColumn').should('have.text','1,0')
        //Validate additional rule 2
        cy.get('.detailRows > :nth-child(3) > .ravCodeColumn').should('have.text','PAS2015.07-01')
        cy.get('.detailRows > :nth-child(3) > .amountColumn').should('have.text','-')
        cy.get('.detailRows > :nth-child(3) > .factorColumn').should('have.text','-')
        cy.get('.detailRows > :nth-child(3) > .reductionColumn').should('have.text','43%')
        cy.get('.detailRows > :nth-child(3) > .emissionColumn').should('have.text','0,6')
    }

    static editFieldsOfSource() {
        //Sector
        cy.get('[id="label"]').clear().type('Bron 1x')
        //Location
        cy.get('[id="collapsibleLocation"]').click()
        cy.get('[id="location"]').clear().type('LINESTRING(200380.4 493929.51,192361.53 488279.85,211861.97 487186.37)')
        //Source characteristics
        cy.get('[id="collapsibleOPSCharacteristics"]').click()
        cy.get('[id="heatContentType"]').select('Geforceerd')
        cy.get('[id="emissionHeight"]').type('{selectall}').type('30')
        cy.get('[id="emissionTemperature"]').type('{selectall}').type('22,3')
        cy.get('[id="outflowDiameter"]').type('{selectall}').type('0,5')
    }

    static validateSourceModifiedAndVisible() {
        //Validate Sector
        cy.get('.title > h1').should('have.text','Bron 1x')
        //Validate specific sector group
        cy.get('[id="sourceListDetailSectorGroup"]').should('have.text','Landbouw')
        //Validate specific sector
        // cy.get('[id="sourceListDetailSector"]').should('have.text', 'Stalemissies') // TODO: AER3-942 aanzetten als bug opgelost is dat de sector in het Engels wordt weergegeven
        //Validate Location
        cy.get('[id="sourceListDetailLocation"]').should('have.text','X:197214,83 Y:488007,7')
        //Validate Source characteristics
        cy.get('[id="sourcelistDetailVentilation"]').should('have.text','Geforceerd')
        cy.get('[id="sourcelistDetailBuilding"]').should('have.text','Uit')
        cy.get('[id="sourcelistDetailEmissionHeight"]').should('have.text','30,0 m') //TODO
        // cy.get('[id="sourcelistDetailEmissionTemperature"]').should('have.text','22,3') //TODO
        // cy.get('[id="sourcelistDetailOutflowDiameter"]').should('have.text','0,5') //TODO
        cy.get('[id="sourcelistDetailDiurnalVariation"]').should('have.text','Continue Emissie')
    }

    static addStableSystem() {
        //Stable systems, animals and numbers
        cy.get('[id="collapsibleSubSources"] > .line > .title').click()
        cy.get('[id="sourceListButtonNewSource"] > .icon').click()
    }

    static chooseStableSystemOfType(typeStableSystem) {
        cy.get('[id="tab-CUSTOM"]').contains(typeStableSystem).click()
    }

    static addStableSystemWithCustomSpecification(description, animalType, factor, numberOfAnimals) {
        cy.get('[id="emissionSourceFarmlodgingCustomDescription"]').type((description))
        cy.get('[id="animalType"]').select((animalType))
        cy.get('[id="emissionFactorInput"]').type((factor))
        cy.get('[id="numberOfAnimalsInput"]').type(numberOfAnimals)
    }

    static fillInFieldsStableSystemCustomSpec(dataTable) {
        dataTable.hashes().forEach((element) => {
            cy.get('[id="sourceListButtonNewSource"] > .icon').click()
            cy.get('[id="tab-CUSTOM"]').contains('Eigen specificatie').click()
            cy.get('[id="emissionSourceFarmlodgingCustomDescription"]').type(element.description)
            cy.get('[id="animalType"]').select(element.animalType)
            cy.get('[id="emissionFactorInput"]').type(element.factor)
            cy.get('[id="numberOfAnimalsInput"]').type(element.numberOfAnimals)
        })
    }

    static stableSystemCustomSpecCreated(dataTable) {
        dataTable.hashes().forEach((element) => {
            cy.get('[id="farmLodgingCustomDescription"]').contains(element.description).should('have.text', (element.description))
            cy.get('[id="farmLodgingCustomNumberOfAnimals"]').contains(element.numberOfAnimals).should('have.text',(element.numberOfAnimals))
            cy.get('[id="farmLodgingCustomEmissionFactor"]').contains(element.factor).should('have.text',(element.factor))
            cy.get('[id="farmLodgingCustomReduction"]').contains(element.reduction).should('have.text',(element.reduction))
            cy.get('[id="farmLodgingCustomEmission"]').contains(element.emission).should('have.text',(element.emission))
        })
    }

    static stableSystemContainsFollowingIconAndData(dataTable) {
        dataTable.hashes().forEach((element) => {
            // cy.get('.iconColumn > .icon').contains(element.icon) //TODO
            // cy.get('[id="subSourceListAmount"]').contains(element.numberOfAnimals).should('have.text',(element.numberOfAnimals)) //TODO
            cy.get('[id="subSourceListDescription"]').contains(element.description).should('have.text',(element.description))
            cy.get('[id="subSourceListEmission"]').contains(element.emission).should('have.text',(element.emission))

        })
    }

    static fillInFieldsSpecificStableSystemCustomSpec(description, animalType, factor, numberOfAnimals) {
            cy.get('[id="emissionSourceFarmlodgingCustomDescription"]').type('{selectall}').type(description)
            cy.get('[id="animalType"]').select(animalType)
            cy.get('[id="emissionFactorInput"]').type('{selectall}').type(factor)
            cy.get('[id="numberOfAnimalsInput"]').type('{selectall}').type(numberOfAnimals)
    }

    static specificStableSystemCustomSpec(description, numberOfAnimals, factor, reduction, emission) {
        //Validate Stable systems, animals and numbers
        cy.get('[id="farmLodgingCustomDescription"]').should('have.text',(description))
        cy.get('[id="farmLodgingCustomNumberOfAnimals"]').should('have.text',(numberOfAnimals))
        cy.get('[id="farmLodgingCustomEmissionFactor"]').should('have.text',(factor))
        cy.get('[id="farmLodgingCustomReduction"]').should('have.text',(reduction))
        cy.get('[id="farmLodgingCustomEmission"]').should('have.text',(emission))
        //Validate total emissions
        // cy.get('[id="sourceListTotalNOX"]').should('have.text','x') //TODO AER3-913
        // cy.get('[id="sourceListTotalNH3"]').should('have.text','y') //TODO AER3-913
    }

    static addNewFarmlandSource() {
        cy.get('[id="collapsibleSubSources"]').click()
        cy.get('[id="sourceListButtonNewSource"]').click()
    }

    static fillInFieldsFarmlandSource(category, NOX, NH3) {
        cy.get('[id="emissionSourceFarmlandType"]').select((category),{force: true})
        cy.get('[id="FarmlandemissionInput_NOX"]').type((NOX),{force: true})
        cy.get('[id="FarmlandemissionInput_NH3"]').type((NH3),{force: true})
    }

    static farmlandSourceCorrectlySave(category, NOX, NOXUnit, NH3, NH3Unit) {
        cy.get('[id="farmlandDetailActivity-PASTURE"]').contains(category)
        cy.get(':nth-child(2) > [id="farmlandDetailEmission"]').should('have.text',(NOX))
        cy.get(':nth-child(2) > [id="farmlandDetailEmissionUnit"]').should('have.text',(NOXUnit))
        cy.get(':nth-child(3) > [id="farmlandDetailEmission"]').should('have.text',(NH3))
        cy.get(':nth-child(3) > [id="farmlandDetailEmissionUnit"]').should('have.text',(NH3Unit))
    }

}
export default agricultureSourcePage
