/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

class commonPage {

    static visitAndLogin() {
        this.username = Cypress.env('USERNAME')
        this.password = Cypress.env('PASSWORD')

        if ((this.username && this.password)) {
            cy.intercept('/application/v2/service/context').as('getContext')
            cy.visit("", {
                auth: {
                    "username": this.username,
                    "password": this.password
                }
            })
            cy.wait('@getContext')
        } else {
            cy.visit("")
        }
    }

    static createNewSourceFromTheStartPage(){
        cy.get('[id="calculationLaunchButtonNewsource"]').click()
    }

    static createNewSourceFromTheSourcePage(){
        cy.get('[id="sourceListButtonNewSource"]').click()
    }

    //Fill in fields for source with only a sector group:'Energy'/'Other'
    static fillInFieldsForSectorGroup(sectorGroup){
        //Sector
        cy.get('[id="label"]').clear().type('Bron 1')
        //Specific sector group
        cy.get('[id="sourceListDetailSectorGroup"]').select(sectorGroup)
        //Location
        cy.get('[id="wktGeometry"]').type('POINT(172203.2 599120.32)')
        //Source characteristics
        cy.get('[id="collapsibleOPSCharacteristics"]').click()
        cy.get('[id="heatContentType"]').select('Niet geforceerd')
        cy.get('[id="emissionHeight"]').type('15')
        cy.get('[id="emissionHeatContent"]').type('20')
        //Emissions
        cy.get('[id="collapsibleSubSources"]').click()
        cy.get('[id="emissionRow_NOX"]').type('200')
        cy.get('[id="emissionRow_NH3"]').type('300')
    }

    static selectSectorGroup(sourceName, sectorGroup){
        //name of source
        cy.get('[id="label"]').clear().type(sourceName)
        //Specific sector group
        cy.get('[id="sourceListDetailSectorGroup"]').select(sectorGroup)
    }

    static selectSectorGroupAndSector(sourceName, sectorGroup, sector){
        //name of source
        cy.get('[id="label"]').clear().type(sourceName)
        //Specific sector group
        cy.get('[id="sourceListDetailSectorGroup"]').select(sectorGroup)
        //Specific sector
        cy.get('[id="sourceListDetailSector"]').select(sector)
    }

    //Validate fields for source with name and  sector group
    static sourceNameWithSectorGroupCorrectlySaved (sourceName,sectorGroup) {
        //Validate name of source
        cy.get('[id="sourceListDetailTitle"]').should('have.text', (sourceName))
        //Validate specific sector group
        cy.get('[id="sourceListDetailSectorGroup"]').should('have.text', (sectorGroup))
    }

    //Validate fields for source with name, sector group and a sector
    static sourceNameWithSectorGroupAndSectorCorrectlySaved (sourceName,sectorGroup, sector) {
        //Validate name of source
        cy.get('[id="sourceListDetailTitle"]').should('have.text', (sourceName))
        //Validate specific sector group
        cy.get('[id="sourceListDetailSectorGroup"]').should('have.text', (sectorGroup))
        //Validate specific sector
        cy.get('[id="sourceListDetailSector"]').should('have.text', (sector))
    }

    static fillLocationWithCoordinates(locationCoordinates){
        cy.get('[id="wktGeometry"]').type((locationCoordinates))
    }

    static locationWithCoordinatesCorrectlySaved(locationCoordinates){
        cy.get('[id="sourceListDetailLocation"]').should('have.text',(locationCoordinates))
    }

    static lengthOfLocationCorrectlySaved(lengthOfLocation){
        cy.get('[id="sourceListDetailLocationStatistic"]').should('have.text',(lengthOfLocation))
    }

    static chooseSourceCharacteristicsStableEmission(establishmentDate, heatContentType, emissionHeight, heatContent) {
        //Source characteristics
        cy.get('[id="collapsibleOPSCharacteristics"]').click()
        cy.get('[id="established"]').type(establishmentDate)
        cy.get('[id="heatContentType"]').select((heatContentType))
        cy.get('[id="emissionHeight"]').type('{selectall}').type((emissionHeight))
        cy.get('[id="emissionHeatContent"]').type('{selectall}').type((heatContent))
    }

    static chooseSourceCharacteristics(heatContentType, emissionHeight, heatContent) {
        //Source characteristics
        cy.get('[id="collapsibleOPSCharacteristics"]').click()
        cy.get('[id="heatContentType"]').select((heatContentType))
        cy.get('[id="emissionHeight"]').type('{selectall}').type((emissionHeight))
        cy.get('[id="emissionHeatContent"]').type('{selectall}').type((heatContent))
    }

    static chooseSourceCharacteristicsWithDiurnalVariationType(heatContentType, emissionHeight, heatContent, diurnalVariationType) {
        //Source characteristics
        cy.get('[id="collapsibleOPSCharacteristics"]').click()
        cy.get('[id="heatContentType"]').select((heatContentType))
        cy.get('[id="emissionHeight"]').type('{selectall}').type((emissionHeight))
        cy.get('[id="emissionHeatContent"]').type('{selectall}').type((heatContent))
        cy.get('[id="diurnalVariationType"]').type('{selectall}').select((diurnalVariationType))
    }

    static sourceCharacteristicsCorrectlySaved(heatContentType, emissionHeight, heatContent, diurnalVariationType) {
        cy.get('[id="sourcelistDetailVentilation"]').should('have.text',(heatContentType))
        cy.get('[id="sourcelistDetailBuilding"]').should('have.text',('Uit'))
        cy.get('[id="sourcelistDetailEmissionHeight"] > span:first').should('have.text',(emissionHeight + ' m'))
        cy.get('[id="sourcelistDetailHeatContent"]').should('include.text', (heatContent + ' MW'))
        cy.get('[id="sourcelistDetailDiurnalVariation"]').should('have.text',(diurnalVariationType))
    }

    static fillEmissionFields(emissionNOx,emissionNH3) {
        cy.get('[id="collapsibleSubSources"]').click()
        cy.get('[id="emissionRow_NOX"]').type(emissionNOx)
        cy.get('[id="emissionRow_NH3"]').type(emissionNH3)
    }

    static emissionFieldsCorrectlySaved(emissionNOx, unitNOx, emissionNH3, unitNH3) {
        cy.get(':nth-child(2) > .detailRow > [id="genericDetailEmission"]').should('have.text',(emissionNOx))
        cy.get(':nth-child(2) > .detailRow > [id="genericDetailEmissionUnit"]').should('have.text',(unitNOx))
        cy.get(':nth-child(3) > .detailRow > [id="genericDetailEmission"]').should('have.text',(emissionNH3))
        cy.get(':nth-child(3) > .detailRow > [id="genericDetailEmissionUnit"]').should('have.text',(unitNH3))
    }

    //Fill in fields for source with a sector group and a sector
    static fillInFieldsForSectorGroupAndSector(sectorGroup,sector) {
        // //Sector
        cy.get('[id="label"]').clear().type('Bron 1')
        //Specific sector group
        cy.get('[id="sourceListDetailSectorGroup"]').select(sectorGroup)
        //Specific sector for agriculture
        cy.get('[id="sourceListDetailSector"]').select(sector)
        //Location
        cy.get('[id="wktGeometry"]').type('POINT(172203.2 599120.32)')
        //Source characteristics
        cy.get('[id="collapsibleOPSCharacteristics"]').click()
        cy.get('[id="heatContentType"]').select('Niet geforceerd')
        cy.get('[id="emissionHeight"]').type('15')
        cy.get('[id="emissionHeatContent"]').type('20')
    }

    //Validate fields for source with a sector group and a sector
    static sourceWithSectorGroupAndSectorCreated (sectorGroup, sector, sourceName) {
        //Validate Sector
        cy.get('[id="sourceListDetailTitle"]').should('have.text', (sourceName))
        //Validate specific sector group
        cy.get('[id="sourceListDetailSectorGroup"]').should('have.text', (sectorGroup))
        //Validate specific sector
        cy.get('[id="sourceListDetailSector"]').should('have.text', (sector))
        //Validate Location
        cy.get('[id="sourceListDetailLocation"]').should('have.text','X:172203,2 Y:599120,32')
        //Validate Source characteristics
        cy.get('[id="sourcelistDetailVentilation"]').should('have.text','Niet geforceerd')
        cy.get('[id="sourcelistDetailBuilding"]').should('have.text','Uit')
        cy.get('[id="sourcelistDetailEmissionHeight"]').should('have.text','15,0 m')
        cy.get('[id="sourcelistDetailHeatContent"]').should('have.text','20,000 MW')
        cy.get('[id="sourcelistDetailDiurnalVariation"]').should('have.text','Continue Emissie')
        //Validate total emissions
        // cy.get('[id="sourcelistTotalNOX"]').should('have.text','x') //TODO AER3-913
        // cy.get('[id="sourcelistTotalNH3"]').should('have.text','y') //TODO AER3-913
    }

    //Save the data for source
    static saveSource(){
        cy.get('[id="buttonSave"]').click()
    }

    //Save the data for source and acknowlegde the warning
    static saveSourceAndValidateWarning(){
        // click the save button
        cy.get('[id="buttonSave"]').click()
        // click the acknowledge button for the warning
        cy.get('[id="buttonSave"]').click()
    }

    //Select the source with a specific name
    static selectSource(sourceName) {
        cy.get('.sources').contains(sourceName).click()
    }

    //Click on edit source
    static editSource(){
        cy.get('[id="sourceListButtonEditSource"]').click()
    }

    //Copy source
    static copySource(){
        cy.get('[id="sourceListButtonCopySource"]').click()
    }

    //Delete selected source
    static deleteSelectedSource(sourceName){
        cy.get('.sources').contains(sourceName).click()
        cy.get('[id="sourceListButtonDeleteSource"]').click()
    }

     //The next source(s) exist(s)
     static sourcesExists(sourceName1, sourceName2){
        cy.get('.sources').contains(sourceName1)
        cy.get('.sources').contains(sourceName2)
    }

    //The next source(s) does not exist(s)
    static sourcesDoesNotExists(sourceName){
        cy.get('.sources').should('not.contain','sourceName')
    }

    //Click on button to import a source from start page
    static clickImportSourceFromStartPage(){
        cy.get('[id="calculationLaunchButtonImportfile"]').click()
    }

    //Open file dialog to import
    static openFileDialogToImport(){
        cy.get('[id="buttonOpenFileDialog"]').click()
    }

    //Select file to import
    static selectFileForImport(fileName){
        cy.get('[id="fileInput"]').attachFile((fileName))
        cy.get('[id="buttonOK"]').click()
    }

    //Validate noticication text
    static followingNotificationIsDisplayed (notificationText) {
        cy.get('.notification').contains(notificationText)
    }

	static wait(wait) {
		cy.wait(wait)
	}

    static openSubSource() {
        cy.get('[id="collapsibleSubSources"]').click()
    }

    static subsourceErrorTextExists(errorText) {
        cy.get(".errorContainer:first > div").should('have.text', errorText)
    }
    
    static saveSourceErrorTextExists(errorText) {
        cy.get(".errorContainer:last > div").should('have.text', errorText)
    }

    static followingWarningIsDisplayed(warningText) {
        cy.on('window:alert',(txt) => {
            expect(txt).to.contains(warningText);
         })
    }
}

export default commonPage
