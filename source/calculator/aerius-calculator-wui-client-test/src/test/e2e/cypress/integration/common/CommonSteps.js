/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import {And, Given, Then} from 'cypress-cucumber-preprocessor/steps'
import commonPage from './CommonPage'

And('I create a new source from the start page', () => {
    // on the launch page we start with new
    commonPage.createNewSourceFromTheStartPage()
    // on the source page we have to start with new
    commonPage.createNewSourceFromTheStartPage()
})

And('I create a new source from the source page', () => {
    commonPage.createNewSourceFromTheSourcePage()
})

Given ('I fill in the fields for sector group {string}', (sectorGroup) => {
    commonPage.fillInFieldsForSectorGroup(sectorGroup)
})

Given ('I name the source {string} and select sector group {string}', (sourceName,sectorGroup) => {
    commonPage.selectSectorGroup(sourceName, sectorGroup)
})

Given ('I name the source {string} and select sector group {string} and sector {string}', (sourceName,sectorGroup, sector) => {
    commonPage.selectSectorGroupAndSector(sourceName, sectorGroup, sector)
})

Then ('the source {string} with sector group {string} is correctly saved', (sourceName,sectorGroup) => {
    commonPage.sourceNameWithSectorGroupCorrectlySaved(sourceName, sectorGroup)
})

Then ('the source {string} with sector group {string} and sector {string} is correctly saved', (sourceName,sectorGroup, sector) => {
    commonPage.sourceNameWithSectorGroupAndSectorCorrectlySaved(sourceName, sectorGroup, sector)
})

Given('I fill location with coordinates {string}', (locationCoordinates) => {
    commonPage.fillLocationWithCoordinates(locationCoordinates)
})

Then('location with coordinates {string} is correctly saved', (locationCoordinates) => {
    commonPage.locationWithCoordinatesCorrectlySaved(locationCoordinates)
})

Then('length of location with value {string} is correctly saved', (lengthOfLocation) => {
    commonPage.lengthOfLocationCorrectlySaved(lengthOfLocation)
})

Given('I choose date establishment of animal shelter {string}, heat content type {string}, emission height {string} and heat content {string}', (establishmentDate, heatContentType, emissionHeight, heatContent) => {
    commonPage.chooseSourceCharacteristicsStableEmission(establishmentDate, heatContentType, emissionHeight, heatContent)
})

Given('I choose heat content type {string}, emission height {string} and heat content {string}', (heatContentType, emissionHeight, heatContent) => {
    commonPage.chooseSourceCharacteristics(heatContentType, emissionHeight, heatContent)
})

Given('I choose heat content type {string}, emission height {string}, heat content {string} and diurnal variation type {string}', (heatContentType, emissionHeight, heatContent, diurnalVariationType) => {
    commonPage.chooseSourceCharacteristicsWithDiurnalVariationType(heatContentType, emissionHeight, heatContent, diurnalVariationType)
})

Then('source characterstics with heat content type {string}, emission height {string}, heat content {string} and diurnal variation type {string} is correctly saved', (heatContentType, emissionHeight, heatContent, diurnalVariationType) => {
    commonPage.sourceCharacteristicsCorrectlySaved(heatContentType, emissionHeight, heatContent, diurnalVariationType)
})

Given('I fill the emission fields with NOx {string} and NH3 {string}', (emissionNOx, emissionNH3) => {
    commonPage.fillEmissionFields(emissionNOx,emissionNH3)
})

Then('emission fields with NOx {string} {string} and NH3 {string} {string} are correctly saved', (emissionNOx, unitNOx, emissionNH3, unitNH3) => {
    commonPage.emissionFieldsCorrectlySaved(emissionNOx, unitNOx, emissionNH3, unitNH3)
})

Given ('I fill in the fields for sector group {string} with sector {string}', (sectorGroup, sector) => {
    commonPage.fillInFieldsForSectorGroupAndSector(sectorGroup,sector)
})

Given ('the source with sector group {string} and sector {string} is created and visible for source {string}', (sectorGroup, sector, sourceName) => {
    commonPage.sourceWithSectorGroupAndSectorCreated(sectorGroup,sector,sourceName)
})

Given ('I save the data', () => {
    commonPage.saveSource()
})

Given ('I save the data and validate the warning', () => {
    commonPage.saveSourceAndValidateWarning();
})

Given ('I select the source {string}', (sourceName) => {
    commonPage.selectSource(sourceName)
})

Given ('I choose to edit the source', () =>{
    commonPage.editSource()
})

Given ('I copy the source {string}', (sourceName) =>{
    commonPage.copySource(sourceName)
})

Given ('I delete source {string}',(sourceName) =>{
    commonPage.deleteSelectedSource(sourceName)
})

Then ('the next sources exists {string}, {string}', (sourceName1, sourceName2) => {
    commonPage.sourcesExists(sourceName1, sourceName2)
})

Then ('the next source does not exist {string}', (sourceName) => {
    commonPage.sourcesDoesNotExists(sourceName)
})

Given ('I choose to import a source starting from the start page', () => {
    commonPage.clickImportSourceFromStartPage()
})

Given ('I open the file dialog to import', () => {
    commonPage.openFileDialogToImport()
})

Given ('I select file {string} to be imported', (fileName) => {
    commonPage.selectFileForImport(fileName)
})

And('the following notification is displayed {string}', (notificationText) => {
    commonPage.followingNotificationIsDisplayed(notificationText)
})

And('I wait {string}',(wait) => {
	commonPage.wait(Number(wait))
})

And('I open subsource', () => {
    commonPage.openSubSource()
})

Then ('the subsource error is given {string}', (errorText) => {
    commonPage.subsourceErrorTextExists(errorText)
})

Then ('the savesource error is given {string}', (errorText) => {
    commonPage.saveSourceErrorTextExists(errorText)
})

Then ('the following warning is displayed: {string}', (warningText) => {
    commonPage.followingWarningIsDisplayed(warningText)
})
