/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given, When, Then, And } from 'cypress-cucumber-preprocessor/steps'
import calculationPointsUIPage from './CalculationPointsUIPage'
import commonPage from '../../common/CommonPage'

Given('I login with correct username and password', () => {
    commonPage.visitAndLogin()
})

And('I select menu calculation point', () => {
    calculationPointsUIPage.selectMenuCalculationPoint()
})

And('I create a new calculation point {string} with location {string}', (name, location) => {
    calculationPointsUIPage.createCalculationPointNameLocation(name, location)
})

And('I add a new calculation point {string} with location {string}', (name, location) => {
    calculationPointsUIPage.addCalculationPointNameLocation(name, location)
})

And('I save the calculation point', () => {
    calculationPointsUIPage.saveCalculationPoint()
})

And('I cancel the calculation point', () => {
    calculationPointsUIPage.cancelCalculationPoint()
})

Then('I edit the calculation point {string}', (name) => {
    calculationPointsUIPage.editCalculationPoint(name)
})

Then('I delete the calculation point {string}', (name) => {
    calculationPointsUIPage.deleteCalculationPoint(name)
})

Then('the calculation point {string} with location {string} is correctly saved', (name, location) => {
    calculationPointsUIPage.createCalculationPointNameLocationCorrectlySaved(name, location)
})

Then('the amount of calculation points is {string}', (count) => {
    calculationPointsUIPage.countCalculationPoint(count)
})

Then('save is disabled and shows error {string}', (errorText) => {
    calculationPointsUIPage.saveDisabledAndShowError(errorText)
})

And('I choose to import calculation points starting from the calculation points start page', () => {
    calculationPointsUIPage.clickImportCalculationPointsFromCalculationPointsStartPage()
})

And('I choose to import calculation points starting from the calculation points page', () => {
    calculationPointsUIPage.clickImportCalculationPointsFromCalculationPointsPage()
})
