#
# Copyright the State of the Netherlands
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see http://www.gnu.org/licenses/.
#

Feature: ShippingSource

    Scenario: Create new shipping source - maritime - dock
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        And I fill the maritime shipping description 'Ship heading for docking' with type 'Passagiersschepen GT: 100-1599', visits '240' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        Then maritime ship row '0' description 'Ship heading for docking' type 'Passagiersschepen GT: 100-1599', visits '240' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12,0 %' procent is correctly created

    Scenario: Create new shipping source - maritime - inland route
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        And I fill the maritime shipping description 'Ship inland' with type 'Passagiersschepen GT: 100-1599', visits '242' time unit 'p/jaar'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        Then maritime ship row '0' description 'Ship inland' type 'Passagiersschepen GT: 100-1599', visits '242' time unit 'p/jaar' is correctly saved

    Scenario: Create new shipping source - maritime - maritime route
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Zeeroute'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        And I fill the maritime shipping description 'Ship from sea' with type 'Passagiersschepen GT: 100-1599', visits '244' time unit 'p/jaar'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Zeeroute' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        Then maritime ship row '0' description 'Ship from sea' type 'Passagiersschepen GT: 100-1599', visits '244' time unit 'p/jaar' is correctly saved

    Scenario: Create new shipping source - inland - dock
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Binnenvaart: Aanlegplaats'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        And I fill the inland shipping description 'Ship heading for docking' with type 'Motorvrachtschip - M1  (Spits)', visits '241' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent laden '14' procent
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Scheepvaart' and sector 'Binnenvaart: Aanlegplaats' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        Then inland ship row '0' description 'Ship heading for docking' type 'Motorvrachtschip - M1  (Spits)', visits '241' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent laden '14' procent is correctly saved

    Scenario: Create new shipping source - inland route
        Given I login with correct username and password
        And I create a new source from the start page
        And I name the source 'Bron 1' and select sector group 'Scheepvaart' and sector 'Binnenvaart: Vaarroute'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        And I fill the inland shipping description 'Ship heading for docking' with type 'Motorvrachtschip - M1  (Spits)'
        And I fill the movement 'A to B' with ships movments '12' time unit 'p/jaar' percentage laden '55'
        And I fill the movement 'B to A' with ships movments '12' time unit 'p/jaar' percentage laden '11'
        And I save the data
        Then I select the source 'Bron 1'
        Then the source 'Bron 1' with sector group 'Scheepvaart' and sector 'Binnenvaart: Vaarroute' is correctly saved
        Then waterway 'CEMT_VIb' and direction 'Irrelevant' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        Then inland ship row '0' type 'Motorvrachtschip - M1  (Spits)' is correctly saved
        Then movement row '0' direction 'A to B' with description 'Ship heading for docking' ships movements '12' time unit 'p/jaar' percentage laden '55,0 %' is correctly saved
        Then movement row '0' direction 'B to A' with description 'Ship heading for docking' ships movements '12' time unit 'p/jaar' percentage laden '11,0 %' is correctly saved

    Scenario: Linking maritime inland route to dock A
        Given I login with correct username and password
        # new source dock 1
        And I create a new source from the start page
        And I name the source 'Aanlegplaats 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(99719.42 497860.02)'
        And I fill the maritime shipping description 'Ship heading for docking 1' with type 'Passagiersschepen GT: 100-1599', visits '240' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent
        And I save the data
        # new source dock 2
        And I create a new source from the source page
        And I name the source 'Aanlegplaats 2' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(103444.54 497912.19)'
        And I fill the maritime shipping description 'Ship heading for docking 2' with type 'Passagiersschepen GT: 100-1599', visits '240' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent
        And I save the data
        # new source inland route
        And I create a new source from the source page
        And I name the source 'Binnengaats route' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        # linking dock A
        And I fill dock A with 'Aanlegplaats 1'
        And I fill the maritime shipping description 'Ship inland' with type 'Passagiersschepen GT: 100-1599', visits '242' time unit 'p/jaar'
        And I save the data
        Then I select the source 'Binnengaats route'
        Then the source 'Binnengaats route' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        And length of location with value 'Lengte: 3.725,21 m' is correctly saved
        Then dock A with value 'Aanlegplaats 1' is correctly saved
        And maritime ship row '0' description 'Ship inland' type 'Passagiersschepen GT: 100-1599', visits '242' time unit 'p/jaar' is correctly saved

    Scenario: Linking maritime inland route to dock B
        Given I login with correct username and password
        # new source dock 1
        And I create a new source from the start page
        And I name the source 'Aanlegplaats 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(99719.42 497860.02)'
        And I fill the maritime shipping description 'Ship heading for docking 1' with type 'Passagiersschepen GT: 100-1599', visits '240' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent
        And I save the data
        # new source dock 2
        And I create a new source from the source page
        And I name the source 'Aanlegplaats 2' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(103444.54 497912.19)'
        And I fill the maritime shipping description 'Ship heading for docking 2' with type 'Passagiersschepen GT: 100-1599', visits '240' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent
        And I save the data
        # new source inland route
        And I create a new source from the source page
        And I name the source 'Binnengaats route' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        # linking dock B
        And I fill dock B with 'Aanlegplaats 2'
        And I fill the maritime shipping description 'Ship inland' with type 'Passagiersschepen GT: 100-1599', visits '242' time unit 'p/jaar'
        And I save the data
        Then I select the source 'Binnengaats route'
        Then the source 'Binnengaats route' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        And length of location with value 'Lengte: 3.725,21 m' is correctly saved
        Then dock B with value 'Aanlegplaats 2' is correctly saved
        And maritime ship row '0' description 'Ship inland' type 'Passagiersschepen GT: 100-1599', visits '242' time unit 'p/jaar' is correctly saved
    
    Scenario: Linking maritime inland route to dock A and dock B
        Given I login with correct username and password
        # new source dock 1
        And I create a new source from the start page
        And I name the source 'Aanlegplaats 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(99719.42 497860.02)'
        And I fill the maritime shipping description 'Ship heading for docking 1' with type 'Passagiersschepen GT: 100-1599', visits '240' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent
        And I save the data
        # new source dock 2
        And I create a new source from the source page
        And I name the source 'Aanlegplaats 2' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(103444.54 497912.19)'
        And I fill the maritime shipping description 'Ship heading for docking 2' with type 'Passagiersschepen GT: 100-1599', visits '240' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent
        And I save the data
        # new source inland route
        And I create a new source from the source page
        And I name the source 'Binnengaats route' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        # linking dock A and B
        And I fill dock A with 'Aanlegplaats 1'
        And I fill dock B with 'Aanlegplaats 2'
        And I fill the maritime shipping description 'Ship inland' with type 'Passagiersschepen GT: 100-1599', visits '242' time unit 'p/jaar'
        And I save the data
        Then I select the source 'Binnengaats route'
        Then the source 'Binnengaats route' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        And length of location with value 'Lengte: 3.725,21 m' is correctly saved
        Then dock A with value 'Aanlegplaats 1' is correctly saved
        Then dock B with value 'Aanlegplaats 2' is correctly saved
        And maritime ship row '0' description 'Ship inland' type 'Passagiersschepen GT: 100-1599', visits '242' time unit 'p/jaar' is correctly saved

    Scenario: Delete a linked dock will delete the link between dock and route
        Given I login with correct username and password
        # new source dock 1
        And I create a new source from the start page
        And I name the source 'Aanlegplaats 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(99719.42 497860.02)'
        And I fill the maritime shipping description 'Ship heading for docking 1' with type 'Passagiersschepen GT: 100-1599', visits '240' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent
        And I save the data
        # new source dock 2
        And I create a new source from the source page
        And I name the source 'Aanlegplaats 2' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(103444.54 497912.19)'
        And I fill the maritime shipping description 'Ship heading for docking 2' with type 'Passagiersschepen GT: 100-1599', visits '240' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent
        And I save the data
        # new source inland route
        And I create a new source from the source page
        And I name the source 'Binnengaats route' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        # linking dock A and B        
        And I fill dock A with 'Aanlegplaats 1'
        And I fill dock B with 'Aanlegplaats 2'
        And I fill the maritime shipping description 'Ship inland' with type 'Passagiersschepen GT: 100-1599', visits '242' time unit 'p/jaar'
        And I save the data
        When I delete source 'Aanlegplaats 1'
        # Warning is displayed
        Then the following warning is displayed: 'Deze aanlegplaats is gekoppeld aan de volgende vaarroute(s): Binnengaats route. Door deze aanlegplaats te verwijderen, verdwijnt de koppeling.'
        When I select the source 'Binnengaats route'
        Then the source 'Binnengaats route' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        And length of location with value 'Lengte: 3.725,21 m' is correctly saved
        Then field 'dock A' is not visible
        Then dock B with value 'Aanlegplaats 2' is correctly saved
        And maritime ship row '0' description 'Ship inland' type 'Passagiersschepen GT: 100-1599', visits '242' time unit 'p/jaar' is correctly saved

    Scenario: Edit link between route and dock
        Given I login with correct username and password
        # new source dock 1
        And I create a new source from the start page
        And I name the source 'Aanlegplaats 1' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(99719.42 497860.02)'
        And I fill the maritime shipping description 'Ship heading for docking 1' with type 'Passagiersschepen GT: 100-1599', visits '240' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent
        And I save the data
        # new source dock 2
        And I create a new source from the source page
        And I name the source 'Aanlegplaats 2' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(103444.54 497912.19)'
        And I fill the maritime shipping description 'Ship heading for docking 2' with type 'Passagiersschepen GT: 100-1599', visits '240' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent
        And I save the data
        # new source dock 3
        And I create a new source from the source page
        And I name the source 'Aanlegplaats 3' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Aanlegplaats'
        And I fill location with coordinates 'POINT(103209.13 498378.92)'
        And I fill the maritime shipping description 'Ship heading for docking 3' with type 'Passagiersschepen GT: 100-1599', visits '240' time unit 'p/jaar' and average residence time '12' hour and shorepower usage '12' procent
        And I save the data
        # new source inland route
        And I create a new source from the source page
        And I name the source 'Binnengaats route' and select sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route'
        And I fill location with coordinates 'LINESTRING(99727.25 497872.33,100995.49 498063.89,101847.59 498090.31,102455.28 497997.83,103426.28 497905.36)'
        # linking dock A and B
        And I fill dock A with 'Aanlegplaats 1'
        And I fill dock B with 'Aanlegplaats 2'
        And I fill the maritime shipping description 'Ship inland' with type 'Passagiersschepen GT: 100-1599', visits '242' time unit 'p/jaar'
        And I save the data
        # linking dock A to another dock
        When I select the source 'Binnengaats route'
        And I choose to edit the source
        And I fill dock A with 'Aanlegplaats 3'
        And I fill dock B with 'Geen'
        And I save the data
        Then the source 'Binnengaats route' with sector group 'Scheepvaart' and sector 'Zeescheepvaart: Binnengaats route' is correctly saved
        And location with coordinates 'X:101575,19 Y:498081,86' is correctly saved
        And length of location with value 'Lengte: 3.725,21 m' is correctly saved
        Then dock A with value 'Aanlegplaats 3' is correctly saved
        Then field 'dock B' is not visible
        And maritime ship row '0' description 'Ship inland' type 'Passagiersschepen GT: 100-1599', visits '242' time unit 'p/jaar' is correctly saved
