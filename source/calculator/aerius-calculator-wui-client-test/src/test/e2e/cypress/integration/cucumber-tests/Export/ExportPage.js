/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

class exportPage {

    static chooseExportThroughMenu() {
        cy.get('[id="testId.NAV_EXPORTEREN"]').click()
    }

    static exportOption(selectedExportOption) {
        switch(selectedExportOption) {
        case 'GML':
            cy.get('[id="exportToggleGML"]').click()
            break;
        case 'PDF':
            cy.get('[id="exportTogglePDF"]').click()
            break;
        }
    }

    static chooseToAddExtraInformationGML() {
        cy.get('[id="includingExtraInfoToggle"]').get('[type="checkbox"]').check({force: true})
    }

    static fillInEmailAdress(emailAddress) {
        cy.get('[id="exportEmail"]').type(emailAddress)
    }

    static clickOnExportButton() {
        cy.intercept('/api/v7/ui/calculation/*').as('getCalculation') //Intercept the url that is triggered after clicking the export button
        cy.get('[id="buttonCalculate"]').click()
        cy.wait('@getCalculation') //Wait till that request has finished execution
    }

    static exportSituationCalculationResult(situationName, calculationResult) {
        cy.get('[type="checkbox"]').check({force: true})
    }

    static fillInFormPDF(legalEntity, projectName, address, postalCode, place, description) {
        cy.get('[id="scenarioMetaDataCorporation"]').type(legalEntity)
        cy.get('[id="scenarioMetaDataProjectName"]').type(projectName)
        cy.get('[id="scenarioMetaDataStreetAddress"]').type(address)
        cy.get('[id="scenarioMetaDataPostcode"]').type(postalCode)
        cy.get('[id="scenarioMetaDataCity"]').type(place)
        cy.get('[id="scenarioMetaDataDescription"]').type(description)
    }
}
export default exportPage
