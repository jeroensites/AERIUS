/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

class mobileEquipmentSourcePage {

	static inputDescriptionStageClassFuelUsageOperationHoursAdBlue(description, stageClass, fuelUsage, operationHours, adBlue) {
		cy.get('[id="collapsibleSubSources"] > .line > .title').click({ force: true })
		cy.get('[id="sourceListButtonNewSource"] > .icon').click({ force: true })

		cy.get('[id="emissionSourceOffRoadMobileStandardDescription"]').clear().type(description)
		cy.get('[id="offRoadMobileCategory"]').select(stageClass)

		if (fuelUsage.length > 0) {
			cy.get('[id="emissionSourceOffRoadMobileStandardLiterFuel"]').clear().type(fuelUsage)
		} else {
			cy.get('[id="emissionSourceOffRoadMobileStandardLiterFuel"]').should('not.exist');			
		}
		if (operationHours.length > 0) {
			cy.get('[id="emissionSourceOffRoadMobileStandardOperatingHours"]').clear().type(operationHours)
		} else {
			cy.get('[id="emissionSourceOffRoadMobileStandardOperatingHours"]').should('not.exist');
		}
		if (adBlue.length > 0) {
			cy.get('[id="emissionSourceOffRoadMobileStandardLiterAdBlue"]').clear().type(adBlue)
		} else {
			cy.get('[id="emissionSourceOffRoadMobileStandardLiterAdBlue"]').should('not.exist');
		}
	}
	
	static descriptionStageClassFuelUsageOperationHoursAdBlueCreated(description, stageClass, fuelUsage, operationHours, adBlue) {
		cy.get('[id="offroadDetailDescription-0"]').should('have.text', description)
	}
	
	static blueRatioWarningisVisible() {
		cy.get('.fuelAdBlueRatioWarning').should('exist');
	}
	
	static blueRatioWarningisNotVisible() {
		cy.get('.fuelAdBlueRatioWarning').should('not.exist');
	}
}
export default mobileEquipmentSourcePage
