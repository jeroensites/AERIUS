/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

import { Given, When, Then, And } from 'cypress-cucumber-preprocessor/steps'
import roadTransportationSourcePage from './RoadTransportationSourcePage'
import commonPage from '../../common/CommonPage'

Given('I login with correct username and password', () => {
    commonPage.visitAndLogin()
})

When ('I choose road elevation type {string} and elevation height {string}', (roadElevationType, elevationHeight) => {
    roadTransportationSourcePage.chooseRoadTransportationElevation(roadElevationType, elevationHeight)
})

When ('I choose as road side barrier type from A to B {string}, barrier height {string} and barrier distance {string}', (barrierType, barrierHeight, barrierDistance) => {
    roadTransportationSourcePage.chooseRoadTransportationBarrierFromAToB(barrierType, barrierHeight, barrierDistance)
})

When ('I choose as road side barrier type from B to A {string}, barrier height {string} and barrier distance {string}', (barrierType, barrierHeight, barrierDistance) => {
    roadTransportationSourcePage.chooseRoadTransportationBarrierFromBToA(barrierType, barrierHeight, barrierDistance)
})

And('I choose direction {string}', (direction) => {
    roadTransportationSourcePage.chooseDirection(direction)
})

And('I click to add a emission source of type {string}', (emissionSourceType) => {
    roadTransportationSourcePage.addEmissionSourceOfType(emissionSourceType)
})

And('I choose maximum speed {string}', (maxSpeed) => {
    roadTransportationSourcePage.chooseMaxSpeed(maxSpeed)
})

And('I choose time unit {string}', (timeUnit) => {
    roadTransportationSourcePage.chooseTimeUnit(timeUnit)
})

And('I choose for traffic type {string} the number of vehicles {string} and stagnation percentage {string}', (trafficType,numberOfVehicles, stagnationPercentage) => {
    roadTransportationSourcePage.chooseTrafficTypeNumberVehiclesAndStagnation(trafficType,numberOfVehicles, stagnationPercentage)
})

And('source characterstics with road type {string}, tunnel factor {string}, elevation type {string} and elevation height {string} is correctly saved', (roadType, tunnelFactor, elevationType, elevationHeight) => {
    roadTransportationSourcePage.sourceCharacteristicsCorrectlySaved(roadType, tunnelFactor, elevationType, elevationHeight)
})

And('road side barrier type from A to B {string}, barrier height {string} and barrier distance {string} is correctly saved', (barrierType, barrierHeight, barrierDistance) => {
    roadTransportationSourcePage.barrierAToBCorrectlySaved(barrierType, barrierHeight, barrierDistance)
})

And('road side barrier type from B to A {string}, barrier height {string} and barrier distance {string} is correctly saved', (barrierType, barrierHeight, barrierDistance) => {
    roadTransportationSourcePage.barrierBToACorrectlySaved(barrierType, barrierHeight, barrierDistance)
})

And('direction {string} is correctly saved', (direction) => {
    roadTransportationSourcePage.directionCorrectlySaved(direction)
})

And('maximum speed {string} is correctly saved', (maxSpeed) => {
    roadTransportationSourcePage.maxSpeedCorrectlySaved(maxSpeed)
})

And('traffic type {string} with number of vehicles {string} and stagnation percentage {string} is correctly saved', (trafficType, numberOfVehicles, stagnationPercentage) => {
    roadTransportationSourcePage.trafficTypeNumberVehiclesAndStagPercentageCorrectlySaved(trafficType, numberOfVehicles, stagnationPercentage)
})

And('I fill description with {string}', (description) => {
    roadTransportationSourcePage.fillDescription(description)
})

And('I choose time unit {string} with number of vehicles {string}', (timeUnit, numberOfVehicles) => {
    roadTransportationSourcePage.chooseTimeUnitAndNumberOfVehicles(timeUnit, numberOfVehicles)
})

And('I fill emission {string} per vehicle {string}', (emission, fraction) => {
    roadTransportationSourcePage.fillEmissionAndFraction(emission, fraction)
})

And('I fill emission {string} per vehicle {string} for Euro class', (emission, fraction) => {
    roadTransportationSourcePage.fillEmissionAndFractionForEuroClass(emission, fraction)
})

And('description {string} is correctly saved', (description) => {
    roadTransportationSourcePage.descriptionCorrectlySaved(description)
})

And('time unit {string} with number of vehicles {string} is correctly saved', (timeUnit, numberOfVehicles) => {
    roadTransportationSourcePage.timeUnitAndNumberOfVehiclesIsCorrectlySaved(timeUnit, numberOfVehicles)
})

And('time unit {string} with number of vehicles {string} is correctly saved', (timeUnit, numberOfVehicles) => {
    roadTransportationSourcePage.timeUnitAndNumberOfVehiclesForEuroClassIsCorrectlySaved(timeUnit, numberOfVehicles)
})

And('time unit {string} with number of vehicles {string} for Euro class is correctly saved', (timeUnit, numberOfVehicles) => {
    roadTransportationSourcePage.timeUnitAndNumberOfVehiclesForEuroClassIsCorrectlySaved(timeUnit, numberOfVehicles)
})

And('emission {string} per vehicle {string} is correctly saved', (emission, fraction) => {
    roadTransportationSourcePage.emissionAndFractionIsCorrectlySaved(emission, fraction)
})

And('I choose Euro class {string} from custom', (euroClass) => {
    roadTransportationSourcePage.chooseEuroClassFromCustom(euroClass)
})

And('I choose Euro class {string} from specific', (euroClass) => {
    roadTransportationSourcePage.chooseEuroClassFromSpecific(euroClass)
})

And('Euro class {string} is correctly saved', (euroClass) => {
    roadTransportationSourcePage.euroClassIsCorrectlySaved(euroClass)
})


