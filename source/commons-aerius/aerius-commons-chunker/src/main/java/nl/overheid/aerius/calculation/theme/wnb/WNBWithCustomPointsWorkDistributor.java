/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;

import java.sql.SQLException;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.domain.AssessmentAreaCompleteHandler;
import nl.overheid.aerius.calculation.domain.AssessmentAreaCompleteHandler.HasAssessmentAreaCompleteHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * WNB WorkDistributor that calculates both WNB receptors and Custom calculation points.
 * It first calculates the WNB receptors and then the custom points.
 */
class WNBWithCustomPointsWorkDistributor implements WorkDistributor<AeriusPoint>, HasAssessmentAreaCompleteHandler,
    CalculationResultHandler {

  private final WorkDistributor<AeriusPoint> wnbWorkDistributor;
  private final WorkDistributor<AeriusPoint> customPointsWorkDistributor;
  private boolean hasWnbWork;

  public WNBWithCustomPointsWorkDistributor(final WorkDistributor<AeriusPoint> wnbWorkDistributor,
      final WorkDistributor<AeriusPoint> customPointsWorkDistributor) {
    this.wnbWorkDistributor = wnbWorkDistributor;
    this.customPointsWorkDistributor = customPointsWorkDistributor;
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    wnbWorkDistributor.init(calculationJob);
    customPointsWorkDistributor.init(calculationJob);
  }

  @Override
  public boolean hasWork(final CalculatorOptions calculationOptions) throws AeriusException {
    synchronized (this) {
      hasWnbWork = wnbWorkDistributor.hasWork(calculationOptions);
      return hasWnbWork || customPointsWorkDistributor.hasWork(calculationOptions);
    }
  }

  @Override
  public WorkPacket<AeriusPoint> work() throws AeriusException {
    synchronized (this) {
      return hasWnbWork ? wnbWorkDistributor.work() : customPointsWorkDistributor.work();
    }
  }

  @Override
  public void addAssessmentAreaCompleteHandler(final AssessmentAreaCompleteHandler handler) {
    if (wnbWorkDistributor instanceof HasAssessmentAreaCompleteHandler) {
      ((HasAssessmentAreaCompleteHandler) wnbWorkDistributor).addAssessmentAreaCompleteHandler(handler);
    }
    if (customPointsWorkDistributor instanceof HasAssessmentAreaCompleteHandler) {
      ((HasAssessmentAreaCompleteHandler) customPointsWorkDistributor).addAssessmentAreaCompleteHandler(handler);
    }
  }

  @Override
  public void onFinish(final CalculationState state) {
    if (wnbWorkDistributor instanceof CalculationResultHandler) {
      ((CalculationResultHandler) wnbWorkDistributor).onFinish(state);
    }
    if (customPointsWorkDistributor instanceof CalculationResultHandler) {
      ((CalculationResultHandler) customPointsWorkDistributor).onFinish(state);
    }
  }

}
