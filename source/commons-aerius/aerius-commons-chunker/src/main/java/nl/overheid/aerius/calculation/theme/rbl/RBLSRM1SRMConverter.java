/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.geom.Geometry;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLMeasure;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM1RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm2.conversion.SRM1SRMConverter;
import nl.overheid.aerius.srm2.domain.SRM1RoadSegment;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Extends the {@link SRM1SRMConverter} to apply measures to SRM1 road emissions.
 */
public class RBLSRM1SRMConverter extends SRM1SRMConverter {

  private class MeasureGeometry {
    private final Geometry geometry;
    private final NSLMeasure measure;

    MeasureGeometry(final Geometry geometry, final NSLMeasure measure) {
      this.geometry = geometry;
      this.measure = measure;
    }
  }

  private static final double MEASURE_APPLY_THRESHOLD = 0.5;

  private final List<MeasureGeometry> measureGeometries = new ArrayList<>();

  RBLSRM1SRMConverter(final Calculation calculation) throws AeriusException {
    this.fillMeasureMap(calculation);
  }

  @Override
  protected List<SRM1RoadSegment> convert(final LineString geometry, final EmissionSource source,
      final List<Substance> substances) throws AeriusException {
    applyMeasures(geometry, source);
    return super.convert(geometry, source, substances);
  }

  private void fillMeasureMap(final Calculation calculation) throws AeriusException {
    for (final NSLMeasureFeature feature : calculation.getNslMeasures()) {
      final Geometry measureGeometry = GeometryUtil.getGeometry(feature.getGeometry());
      measureGeometries.add(new MeasureGeometry(measureGeometry, feature.getProperties()));
    }
  }

  private void applyMeasures(final LineString geometry, final EmissionSource source) throws AeriusException {
    if (!measureGeometries.isEmpty() && source instanceof SRM1RoadEmissionSource) {
      final SRM1RoadEmissionSource srm2Source = (SRM1RoadEmissionSource) source;
      final Geometry jtsSourceGeometry = GeometryUtil.getGeometry(geometry);
      for (final MeasureGeometry measureGeometry : measureGeometries) {
        if (shouldApplyMeasure(measureGeometry.geometry, jtsSourceGeometry)) {
          srm2Source.getSubSources().stream()
              .filter(StandardVehicles.class::isInstance)
              .map(StandardVehicles.class::cast)
              .forEach(subSource -> {
                subSource.getMeasures().addAll(measureGeometry.measure.getVehicleMeasures());
              });
        }
      }
    }
  }

  private boolean shouldApplyMeasure(final Geometry measureGeometry, final Geometry sourceGeometry) {
    return measureGeometry.intersection(sourceGeometry).getLength() > MEASURE_APPLY_THRESHOLD * sourceGeometry.getLength();
  }

}
