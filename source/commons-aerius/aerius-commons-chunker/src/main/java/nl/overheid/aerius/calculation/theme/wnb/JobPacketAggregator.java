/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import nl.overheid.aerius.calculation.aggregation.AggregationDistanceProfilePicker;
import nl.overheid.aerius.calculation.aggregation.EngineSourceAggregator;
import nl.overheid.aerius.calculation.aggregation.GridEmissionSourceDigestor;
import nl.overheid.aerius.calculation.aggregation.GridEngineSourceAggregatable;
import nl.overheid.aerius.calculation.aggregation.SourcesStore;
import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.conversion.EngineSourceExpander;
import nl.overheid.aerius.calculation.conversion.JobPackets;
import nl.overheid.aerius.calculation.conversion.JobPackets.JobPacketBuilder;
import nl.overheid.aerius.calculation.conversion.SourceConversionHelper;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.WorkPacket.GroupedSourcesPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.calculation.grid.GridZoomLevel;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Wraps the {@link EngineSourceAggregator} to provide aggregated sources in {@link JobPacket} for a specific receptor cell.
 */
public class JobPacketAggregator {
  private final EngineSourceAggregator<EngineSource, Substance> aggregator;
  private final GridUtil gridUtil;
  private final PMF pmf;

  private final Map<Integer, SourcesStore> sourcesStore = new HashMap<>();
  private final SectorCategories sectorCategories;
  private boolean forceAggregation;
  private JobPackets expandedSourceJobPackets;
  private Map<Integer, Integer> calculationYears;

  public JobPacketAggregator(final PMF pmf, final GridUtil gridUtil) throws SQLException {
    this.pmf = pmf;
    this.gridUtil = gridUtil;
    aggregator = new EngineSourceAggregator<>(new GridEngineSourceAggregatable(gridUtil.getGridSettings().getSrid()));
    sectorCategories = SectorRepository.getSectorCategories(pmf, LocaleUtils.getDefaultLocale());
  }

  /**
   * Digest all sources and potentially store them in the {@link CalculationJob} to be used by later on.
   */
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    final List<Substance> substances = calculationJob.getSubstances();
    final GridEmissionSourceDigestor sourceDigestor = new GridEmissionSourceDigestor(gridUtil, aggregator, new AggregationDistanceProfilePicker());

    forceAggregation = calculationJob.getCalculationSetOptions().isForceAggregation();
    if (forceAggregation) {
      calculationYears = calculationJob.getScenarioCalculations().getCalculationYears();
      for (final Calculation job : calculationJob.getScenarioCalculations().getCalculations()) {
        final CalculationSetOptions cso = calculationJob.getCalculationSetOptions();
        final SourceConversionHelper conversionHelper = new SourceConversionHelper(pmf, sectorCategories, job.getYear());
        final List<GroupedSourcesPacket> expanded;

        try (Connection con = pmf.getConnection()) {
          final SourceConverter sourceConverter = new WNBSourceConverter(con, calculationJob.getProvider(), job.getSituation());
          expanded = EngineSourceExpander.toEngineSourcesByGroup(con, sourceConverter, conversionHelper, job.getSituation(),
              substances, cso.isStacking());
        }
        sourcesStore.put(job.getCalculationId(), sourceDigestor.digest(cso, substances, expanded));
      }
    } else {
      expandedSourceJobPackets = new JobPacketBuilder(pmf, sectorCategories).build(calculationJob,
          (con, situation) -> sourceConverter(con, calculationJob.getProvider(), situation));
    }
  }

  private static WNBSourceConverter sourceConverter(final Connection con, final CalculationEngineProvider engineProvider,
      final ScenarioSituation situation) throws SQLException {
    return new WNBSourceConverter(con, engineProvider, situation);
  }

  /**
   * Returns the sources for the given cell id. Depending on the configuration the sources are aggregated/stacked or not.
   *
   * @param receptorCellId cell id of the receptor in the grid
   * @return package of sources.
   */
  public List<JobPacket> getSourcesFor(final Integer receptorCellId) {
    return forceAggregation ? getAggregatedSourcesFor(receptorCellId) : expandedSourceJobPackets.getJobPackets();
  }

  /**
   *  Returns the sources aggregated.
   */
  private List<JobPacket> getAggregatedSourcesFor(final Integer receptorCellId) {
    final List<JobPacket> jobPackets = new ArrayList<>();

    for (final Entry<Integer, SourcesStore> entry : sourcesStore.entrySet()) {
      jobPackets.add(
          new JobPacket(entry.getKey(), calculationYears.get(entry.getKey()),
              entry.getValue().getSourcesFor(receptorCellId)
                  .entrySet().stream()
                  .map(e -> new GroupedSourcesPacket(e.getKey(), e.getValue())).collect(Collectors.toList())));
    }
    return jobPackets;
  }

  /**
   * Returns all cell id's of cell that contain sources.
   *
   * @param calculationId
   * @param zoomLevel
   * @return
   */
  public Iterable<Integer> getSourceGridIds(final Integer calculationId, final GridZoomLevel zoomLevel) {
    return sourcesStore.get(calculationId).getSourceGridIds(zoomLevel);
  }
}
