/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.aerius.adms.conversion.ADMSBuildingTracker;
import nl.aerius.adms.conversion.EmissionSource2ADMSConverter;
import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.domain.WorkPacket.GroupedSourcesPacketMap;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * ADMS specific conversion of {@link EmissionSource} objects to ADMS source objects.
 */
class ADMSSourceConverter implements SourceConverter<ADMSBuildingTracker> {

  private final EmissionSource2ADMSConverter converter;
  private final Integer groupId;
  private final Map<String, EngineSource> collectedBuildings = new HashMap<>();

  public ADMSSourceConverter(final Integer groupId) {
    this.groupId = groupId;
    converter = new EmissionSource2ADMSConverter();
  }

  @Override
  public boolean convert(final Connection con, final GroupedSourcesPacketMap map, final EmissionSource originalSource, final Geometry geometry,
      final List<Substance> substances, final ADMSBuildingTracker buildingTracker) throws AeriusException {
    addBuilding(map, buildingTracker, originalSource.getCharacteristics().getBuildingId());
    return map.addAll(groupId, CalculationEngine.ADMS, List.of(converter.convert(originalSource, geometry, substances)));
  }

  private void addBuilding(final GroupedSourcesPacketMap map, final ADMSBuildingTracker buildingTracker, final String buildingId)
      throws AeriusException {
    if (buildingId != null && !buildingId.isBlank() && !collectedBuildings.containsKey(buildingId)) {
      final EngineSource building = buildingTracker.getBuilding(buildingId);

      if (building != null) {
        collectedBuildings.put(buildingId, building);
      }
    }
  }

  @Override
  public ADMSBuildingTracker createBuildingTracker(final List<BuildingFeature> buildings) throws AeriusException {
    return new ADMSBuildingTracker(buildings);
  }

  @Override
  public void postProcess(final GroupedSourcesPacketMap map) {
    if (!collectedBuildings.isEmpty()) {
      map.addAll(groupId, CalculationEngine.ADMS, collectedBuildings.values());
    }
  }
}
