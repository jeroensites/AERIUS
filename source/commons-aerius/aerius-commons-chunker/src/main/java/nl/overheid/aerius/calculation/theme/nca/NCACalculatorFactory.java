/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.NCACalculationOptionsUtil;
import nl.overheid.aerius.calculation.TotalResultDBHandler;
import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.CalculationTaskFactory;
import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.base.CalculatorThemeFactory;
import nl.overheid.aerius.calculation.base.IntermediateResultHandler;
import nl.overheid.aerius.calculation.base.SimpleCalculationResultQueue;
import nl.overheid.aerius.calculation.base.WorkBySectorHandlerImpl;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.base.WorkHandler;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.natura2k.Natura2KReceptorLoader;
import nl.overheid.aerius.calculation.natura2k.Natura2KResultsReadyHandler;
import nl.overheid.aerius.calculation.natura2k.Natura2kReceptorStore;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * {@link CalculatorThemeFactory} for NCA calculations.
 */
public class NCACalculatorFactory implements CalculatorThemeFactory {

  private static final Logger LOGGER = LoggerFactory.getLogger(NCACalculatorFactory.class);

  private static final CalculationTaskFactory CALCULATION_TASK_FACTORY = new NCACalculationTaskFactory();

  private final PMF pmf;
  private final ReceptorUtil receptorUtil;
  private final GridSettings gridSettings;
  private final Natura2kReceptorStore n2kReceptorStore;

  public NCACalculatorFactory(final PMF pmf) throws SQLException, AeriusException {
    this.pmf = pmf;
    final ReceptorGridSettings rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(pmf);

    receptorUtil = new ReceptorUtil(rgs);
    gridSettings = new GridSettings(rgs);
    n2kReceptorStore = loadNatura2kReceptorStore(pmf, receptorUtil);
  }

  /**
   * Load Natura2000 area data into Natura2kReceptorStore.
   *
   * @param pmf
   * @param receptorUtil
   * @return
   * @throws SQLException
   * @throws AeriusException
   */
  private Natura2kReceptorStore loadNatura2kReceptorStore(final PMF pmf, final ReceptorUtil receptorUtil) throws SQLException, AeriusException {
    final Natura2KReceptorLoader receptorLoader = new Natura2KReceptorLoader(receptorUtil, gridSettings);
    try (Connection con = pmf.getConnection()) {
      return receptorLoader.fillReceptorStore(con);
    }
  }

  @Override
  public void prepareOptions(final ScenarioCalculations scenarioCalculations) throws AeriusException {
    final CalculationSetOptions options = scenarioCalculations.getOptions();

    if (options.getCalculationType() == CalculationType.PERMIT) {
      scenarioCalculations.getScenario().setOptions(NCACalculationOptionsUtil.createCalculationSetOptions());
    }
  }

  @Override
  public CalculationEngineProvider getProvider(final CalculationJob calculationJob) {
    return sectorId -> CalculationEngine.ADMS;
  }

  @Override
  public List<IntermediateResultHandler> getIntermediateResultHandlers(final CalculationJob calculationJob)
      throws AeriusException {
    return List.of(new Natura2KResultsReadyHandler(pmf, calculationJob));
  }

  @Override
  public List<CalculationResultHandler> getCalculationResultHandler(final CalculationJob calculationJob, final ExportType exportType,
      final boolean uiCalculation, final Date exportDate) throws AeriusException {
    return List.of(new TotalResultDBHandler(pmf, calculationJob));
  }

  @Override
  public CalculationResultQueue createCalculationResultQueue(final CalculationSetOptions calculationSetOptions) {
    return new SimpleCalculationResultQueue();
  }

  @Override
  public WorkHandler<AeriusPoint> createWorkHandler(final CalculationJob calculationJob, final CalculationTaskHandler workHandler)
      throws AeriusException {
    return new WorkBySectorHandlerImpl(calculationJob.getProvider(), CALCULATION_TASK_FACTORY, workHandler);
  }

  @Override
  public WorkDistributor<AeriusPoint> createWorkDistributor(final CalculationJob calculationJob) throws AeriusException, SQLException {
    final CalculationType ct = calculationJob.getCalculationSetOptions().getCalculationType();

    if (ct == CalculationType.PERMIT) {
      return new NCANatura2kWorkDistributor(pmf, n2kReceptorStore, receptorUtil);
    } else {
      LOGGER.error("Unsupported calculation type for NCA calculation:'{}'", ct);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

}
