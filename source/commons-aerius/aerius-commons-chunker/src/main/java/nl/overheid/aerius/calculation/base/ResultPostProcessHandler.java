/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.util.List;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

/**
 * Class implementing this interface can update results retrieved from the workers before the result is handled. This can be
 * used in case results need a correction factor.
 */
public interface ResultPostProcessHandler {

  /**
   * Post process the given sector results.
   * @param calculationId The calculation the results belong to
   * @param results sector specific results
   */
  void postProcessSectorResults(int calculationId, List<AeriusResultPoint> results);
}
