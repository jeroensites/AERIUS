/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import nl.overheid.aerius.building.BuildingTracker;
import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.domain.WorkPacket.GroupedSourcesPacketMap;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm2.conversion.SRMSourceConverter;

class RBLSourceConverter implements SourceConverter<BuildingTracker> {

  private final SRMSourceConverter converter;
  private final Integer groupId;

  RBLSourceConverter(final Integer sectorId, final Calculation calculation) throws AeriusException {
    this.groupId = sectorId;
    this.converter = new SRMSourceConverter(new RBLSRM1SRMConverter(calculation));
  }

  @Override
  public boolean convert(final Connection con, final GroupedSourcesPacketMap map, final EmissionSource originalSource, final Geometry geometry,
      final List<Substance> substances, final BuildingTracker buildingTracker) throws AeriusException {
    final Collection<EngineSource> expanded = new ArrayList<>();
    final boolean converted = converter.convert(expanded, originalSource, geometry, substances);

    if (converted) {
      map.addAll(groupId, CalculationEngine.ASRM2, expanded);
    }
    return converted;
  }

  @Override
  public BuildingTracker createBuildingTracker(final List<BuildingFeature> buildings) {
    // RBL doesn't support buildings therefor no building tracker is available.
    return null;
  }
}
