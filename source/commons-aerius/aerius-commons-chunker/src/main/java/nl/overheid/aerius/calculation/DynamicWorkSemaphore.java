/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor;
import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor.RabbitMQWorkerObserver;
import nl.overheid.aerius.calculation.base.WorkSemaphore;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * {@link WorkSemaphore} implementation that scales with more or less capacity depending on the load on the queue. For this queue the worker type
 * queue to the taskmanager is monitored. If the queue is empty (no ready messages) then it might be possible to handle more tasks.
 *
 */
class DynamicWorkSemaphore implements WorkSemaphore {

  private static final Logger LOGGER = LoggerFactory.getLogger(DynamicWorkSemaphore.class);

  /**
   * Minimal number of threads available to run when there are workers available.
   */
  private static final int MINIMAL_THREADS = 2;

  private final RabbitMQWorkerMonitor rabbitMQQueueMonitor;
  private final WorkerType workerType;

  private final Map<String, DynamicWorkQueueSemaphore> map = new HashMap<>();

  public DynamicWorkSemaphore(final RabbitMQWorkerMonitor rabbitMQQueueMonitor, final WorkerType workerType) {
    this.rabbitMQQueueMonitor = rabbitMQQueueMonitor;
    this.workerType = workerType;
  }

  @Override
  public void drain() {
    map.forEach((k, v) -> v.drain());
    map.forEach((k, v) -> rabbitMQQueueMonitor.removeObserver(v));
    map.clear();
  }

  @Override
  public void acquire(final String queue) throws InterruptedException {
    getMonitor(queue).acquire();
  }

  @Override
  public void release(final String queue) {
    getMonitor(queue).release();
  }

  /**
   * @return Number of available permits that can be acquired. Method for testing only
   */
  int availablePermits(final String queue) {
    synchronized (this) {
      return getMonitor(queue).semaphore.availablePermits();
    }
  }

  private DynamicWorkQueueSemaphore getMonitor(final String queueName) {
    return map.computeIfAbsent(queueName, q -> addObserver(new DynamicWorkQueueSemaphore(workerType, queueName)));
  }

  private DynamicWorkQueueSemaphore addObserver(final DynamicWorkQueueSemaphore monitor) {
    rabbitMQQueueMonitor.addObserver(monitor);
    return monitor;
  }

  /**
   * Specific {@link WorkSemaphore} implementation for a specific queue.
   */
  private static class DynamicWorkQueueSemaphore implements RabbitMQWorkerObserver {
    /**
     * The semaphore used to acquire and release slots.
     */
    private final Semaphore semaphore;
    /**
     * Keeps track of the number of tasks that have acquired a slot.
     */
    private final AtomicInteger numberOfTasksRunning = new AtomicInteger();
    /**
     * Keeps track of the number of slots that should be available.
     */
    private int dynamicSize;
    /**
     * Keeps track of the last known chunker utilisation.
     */
    private int chunkerWorkerQueueUtilisation;
    /**
     * Keeps track of the last known number of calculation engine workers available.
     */
    private int calculationEngineWorkerQueueSize;

    /**
     * Name of the calculation engine worker queue name, e.g. ops
     */
    private final String calculationEngineWorkerQueueName;
    /**
     * Name of the chunker worker queue.
     */
    private final String chunkerWorkerQueueName;

    public DynamicWorkQueueSemaphore(final WorkerType chunkerWorkerType, final String calculationEngineWorkerQueueName) {
      this.chunkerWorkerQueueName = chunkerWorkerType.type().getWorkerQueueName();
      this.calculationEngineWorkerQueueName = calculationEngineWorkerQueueName;
      semaphore = new Semaphore(0);
    }

    @Override
    public void updateWorkers(final String workerQueueName, final int size, final int utilisation) {
      synchronized (this) {
        if (calculationEngineWorkerQueueName.equals(workerQueueName) && calculationEngineWorkerQueueSize != size) {
          calculationEngineWorkerQueueSize = size;
          //
        } else if (chunkerWorkerQueueName.equals(workerQueueName) && chunkerWorkerQueueUtilisation != utilisation) {
          chunkerWorkerQueueUtilisation = utilisation;
        } else {
          // No relevant queue and no data changed.
          return;
        }
        final int oldDynamicSize = dynamicSize;

        dynamicSize = calculationEngineWorkerQueueSize == 0 ? 0
            : Math.max(MINIMAL_THREADS, (int) Math.ceil((double) calculationEngineWorkerQueueSize / Math.max(1, chunkerWorkerQueueUtilisation)));
        LOGGER.debug("Dynamic Size {} - modelWorkerQueueSize:{}, chunkerWorkerQueueUtilization:{}, old: {}, new: {}", calculationEngineWorkerQueueName,
            calculationEngineWorkerQueueSize, chunkerWorkerQueueUtilisation, oldDynamicSize, dynamicSize);
        if (oldDynamicSize < dynamicSize) {
          semaphore.release(dynamicSize - oldDynamicSize);
        } else if (oldDynamicSize > dynamicSize) {
          semaphore.tryAcquire(oldDynamicSize - dynamicSize);
        } // else size remains the same, nothing to do.
      }
    }

    public void acquire() throws InterruptedException {
      semaphore.acquire();
      final int newNumberOfTasksRunning = numberOfTasksRunning.incrementAndGet();
      LOGGER.trace("Acquire: tasks:#{}", newNumberOfTasksRunning);
    }

    /**
     * Releases a slot, but only if there are fewer tasks running then the dynamic size.
     */
    public void release() {
      synchronized (this) {
        final int newNumberOfTasksRunning = numberOfTasksRunning.decrementAndGet();

        LOGGER.trace("Release: tasks:#{}, dynamicSize: #{}", newNumberOfTasksRunning, dynamicSize);
        if (dynamicSize > numberOfTasksRunning.get()) {
          semaphore.release();
        }
      }
    }

    /**
     * Draining will release all tasks still waiting on acquire.
     */
    public void drain() {
      synchronized (this) {
        LOGGER.trace("Drain all tasks({}) remaining", semaphore.getQueueLength());
        while (semaphore.hasQueuedThreads()) {
          semaphore.release();
        }
        dynamicSize = 0;
      }
    }
  }
}
