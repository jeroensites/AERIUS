/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;


import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;

/**
 * Data class storing results for all groups in a single point
 */
class GroupResultPoint extends AeriusPoint {

  private static final long serialVersionUID = 1L;

  /**
   * Map<Group Id, EmissionResults>
   */
  private final Map<CalculationEngine, Map<Integer, EmissionResults>> groupResults = new HashMap<>();
  private int size;

  /**
   * Creates a new point for the given point location
   * @param ap point location
   */
  public GroupResultPoint(final AeriusPoint ap) {
    super(ap.getId(), ap.getPointType(), ap.getX(), ap.getY(), ap.getHeight());
  }

  public AeriusResultPoint getTotalsResultPoint() {
    final AeriusResultPoint arp = new AeriusResultPoint(getId(), getPointType(), getX(), getY());
    for (final Entry<CalculationEngine, Map<Integer, EmissionResults>> engineResult : groupResults.entrySet()) {
      for (final Entry<Integer, EmissionResults> groupResult : engineResult.getValue().entrySet()) {
        for (final Entry<EmissionResultKey, Double> emission : groupResult.getValue().entrySet()) {
          final Double value = emission.getValue();

          if (!Double.isNaN(value)) {
            arp.setEmissionResult(emission.getKey(), arp.getEmissionResult(emission.getKey()) + value);
          }
        }
      }
    }
    return arp;
  }

  /**
   * Adds the results for the given group. The caller should guarantee the results are actually for the location of this point
   * @param calculationEngine
   * @param groupId group Id the results are for
   * @param emissionResults emission results for group
   */
  public void put(final CalculationEngine calculationEngine,  final int groupId, final EmissionResults emissionResults) {
    synchronized (this) {
      groupResults.computeIfAbsent(calculationEngine, ce -> new HashMap<>()).put(groupId, emissionResults);
      size++;
    }
  }

  /**
   * Returns the number of groups results are put in this object.
   * @return number of groups results are put in this object.
   */
  public int size() {
    return size;
  }

  @Override
  public String toString() {
    return "GroupResultPoint [groupResults=" + groupResults + "]";
  }
}
