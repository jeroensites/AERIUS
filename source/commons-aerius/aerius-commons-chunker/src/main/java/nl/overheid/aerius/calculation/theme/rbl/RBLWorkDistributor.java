/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.conversion.EngineSourceExpander;
import nl.overheid.aerius.calculation.conversion.SourceConversionHelper;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.GroupedSourcesPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.io.PreCalculationUtil;
import nl.overheid.aerius.util.FeaturePointToEnginePointUtil;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Work distributor to create source/calculation point sets for NSL calculations.
 */
public class RBLWorkDistributor implements WorkDistributor<AeriusPoint> {

  private static final Integer STORAGE_SECTOR_ID = Sector.SECTOR_DEFAULT.getSectorId();

  private final PMF pmf;
  private final MonitorSRM2NetworksStore srm2SourcesStore;
  private final SectorCategories sectorCategories;

  private List<AeriusPoint> points;
  private final List<JobPacket> jobPackets = new ArrayList<>();

  public RBLWorkDistributor(final PMF pmf, final MonitorSRM2NetworksStore srm2SourcesStore) throws SQLException {
    this.pmf = pmf;
    this.srm2SourcesStore = srm2SourcesStore;
    sectorCategories = SectorRepository.getSectorCategories(pmf, LocaleUtils.getDefaultLocale());
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    points = FeaturePointToEnginePointUtil.convert(calculationJob.getScenarioCalculations().getCalculationPoints());
    // TODO: copy calculation points to calculation so we can assign per situation?
    if (calculationJob.getScenarioCalculations().getCalculations().size() == 1) {
      final Calculation calculation = calculationJob.getScenarioCalculations().getCalculations().get(0);

      PreCalculationUtil.assignDispersionLines(calculation.getSources(), points,
          calculation.getNslDispersionLines());
    }

    final List<Substance> substances = calculationJob.getSubstances();
    final List<EngineSource> monitorSrm2Sources = determineMonitorSrm2Sources(calculationJob);

    initSourcePackets(calculationJob, substances, monitorSrm2Sources);
  }

  /**
   * Creates the source packages. Since these won't change during calculation it can be done upfront.
   *
   * @param calculationJob
   * @param substances
   * @param monitorSrm2Sources
   * @throws SQLException
   * @throws AeriusException
   */
  private void initSourcePackets(final CalculationJob calculationJob, final List<Substance> substances, final List<EngineSource> monitorSrm2Sources)
      throws SQLException, AeriusException {
    for (final Calculation job : calculationJob.getScenarioCalculations().getCalculations()) {
      final List<EngineSource> sources;

      try (Connection con = pmf.getConnection()) {
        final SourceConversionHelper conversionHelper = new SourceConversionHelper(pmf, sectorCategories, job.getYear());

        sources = EngineSourceExpander.toEngineSources(con, new RBLSourceConverter(STORAGE_SECTOR_ID, job), conversionHelper,
            job.getSituation(), substances, true);
      }
      sources.addAll(monitorSrm2Sources);
      jobPackets.add(new JobPacket(job.getCalculationId(), job.getYear(),
          List.of(new GroupedSourcesPacket(STORAGE_SECTOR_ID, CalculationEngine.ASRM2, sources))));
    }
  }

  /**
   * Add optional monitor srm2 soures when requested and available.
   *
   * @param calculationJob
   * @return
   * @throws AeriusException
   */
  private List<EngineSource> determineMonitorSrm2Sources(final CalculationJob calculationJob) throws AeriusException {
    final List<EngineSource> extraSources = new ArrayList<>();

    if (calculationJob.getCalculationSetOptions().isIncludeMonitorSrm2Network()) {
      extraSources.addAll(
          srm2SourcesStore.get(calculationJob.getCalculationSetOptions().getMonitorSrm2Year(), calculationJob.getSubstances()));
    }
    return extraSources;
  }

  @Override
  public boolean hasWork(final CalculatorOptions calculationOptions) throws AeriusException {
    return !points.isEmpty();
  }

  @Override
  public WorkPacket<AeriusPoint> work() {
    final WorkPacket<AeriusPoint> workPacket = new WorkPacket<>(new ArrayList<>(points), jobPackets);

    // Clear the points to indicate there is not other work.
    // This implementation depends on WorkSplitterCalculationTaskHandler to split the tasks.
    // Once WorkSplitterCalculationTaskHandler is being removed this implementation should handle splitting in reasonable sized packets.
    points = List.of();
    return workPacket;
  }

}
