/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.natura2k;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.conversion.JobPackets;
import nl.overheid.aerius.calculation.conversion.JobPackets.JobPacketBuilder;
import nl.overheid.aerius.calculation.conversion.JobPackets.SourceConverterSupplier;
import nl.overheid.aerius.calculation.domain.AssessmentAreaCompleteHandler;
import nl.overheid.aerius.calculation.domain.AssessmentAreaCompleteHandler.HasAssessmentAreaCompleteHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Abstract base class to determine the receptors to calculate. Receptors are from nature areas.
 * It sorts the nature areas based on the distance to the sources.
 */
public abstract class AbstractNatura2KWorkDistributor implements WorkDistributor<AeriusPoint>, HasAssessmentAreaCompleteHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractNatura2KWorkDistributor.class);

  protected final List<AssessmentAreaCompleteHandler> assessmentAreaCompleteHandlers = new ArrayList<>();
  protected final Natura2kReceptorStore n2kReceptorStore;
  private final JobPacketBuilder jobPacketBuilder;

  private Iterator<Entry<Integer, Integer>> listN2KIdIterator;
  private final Set<Integer> receptorsDone = new HashSet<>();
  /***
   * If not all receptors could be put in a work packet the remaining set of receptors is kept in this list.
   */
  private List<AeriusPoint> receptorsToDo = List.of();
  /**
   * The set of receptors to return in the next work set.
   */
  private List<AeriusPoint> nextReceptorsToDo = List.of();
  private JobPackets jobPackets;

  private int sourcesSize;

  protected AbstractNatura2KWorkDistributor(final PMF pmf, final Natura2kReceptorStore n2kReceptorStore, final SectorCategories sectorCategories)
      throws SQLException {
    this.n2kReceptorStore = n2kReceptorStore;
    jobPacketBuilder = new JobPacketBuilder(pmf, sectorCategories);
  }

  /**
   * @param assessmentAreaCompleteHandler Optional handler to send a signal a natura area is finished calculating
   */
  @Override
  public void addAssessmentAreaCompleteHandler(final AssessmentAreaCompleteHandler assessmentAreaCompleteHandler) {
    assessmentAreaCompleteHandlers.add(assessmentAreaCompleteHandler);
  }

  protected void init(final CalculationJob calculationJob, final List<Entry<Integer, Integer>> listn2k) throws SQLException, AeriusException {
    listN2KIdIterator = listn2k.iterator();
    jobPackets = jobPacketBuilder.build(calculationJob, createSourceConverterSupplier(calculationJob));
    sourcesSize = jobPackets.getSourcesCount();
  }

  /**
   * Creates a new  SourceConverterSupplie to be used in the {@link JobPacketBuilder}.
   * @param calculationJob
   * @return
   */
  protected abstract SourceConverterSupplier createSourceConverterSupplier(final CalculationJob calculationJob);

  @Override
  public boolean hasWork(final CalculatorOptions calculationOptions) throws AeriusException {
    synchronized (this) {
      if (!nextReceptorsToDo.isEmpty()) {
        return true; // if nextReceptorsToDo not yet processed do nothing yet.
      }
      if (listN2KIdIterator.hasNext() || !receptorsToDo.isEmpty()) {
        nextReceptorsToDo = collectReceptors(calculationOptions);
        return !nextReceptorsToDo.isEmpty();
      } else {
        return false;
      }
    }
  }

  @Override
  public WorkPacket<AeriusPoint> work() throws AeriusException {
    synchronized (this) {
      if (nextReceptorsToDo.isEmpty()) {
        return null;
      }
      final WorkPacket<AeriusPoint> packet = new WorkPacket<>(nextReceptorsToDo, jobPackets.getJobPackets());

      nextReceptorsToDo = List.of();
      return packet;
    }
  }

  private List<AeriusPoint> collectReceptors(final CalculatorOptions calculationOptions) throws AeriusException {
    int nrOfReceptorsToAdd = nrOfPoints(calculationOptions, sourcesSize);
    final List<AeriusPoint> pointsToCalculate = new ArrayList<>(nrOfReceptorsToAdd);

    while (nrOfReceptorsToAdd > 0) {
      if (receptorsToDo.isEmpty()) {
        if (listN2KIdIterator.hasNext()) {
          // Get all receptors of a single nature area.
          receptorsToDo = n2kReceptorStore.getReceptors(listN2KIdIterator.next().getKey());
        } else {
          // No more points to calculate, break out of the while loop
          break;
        }
      }
      final int size = receptorsToDo.size();
      if (size > nrOfReceptorsToAdd) {
        // If more receptors available than needed only add the amount needed.
        final int added = addReceptors(pointsToCalculate, nrOfReceptorsToAdd);
        receptorsToDo = receptorsToDo.subList(nrOfReceptorsToAdd, size);
        nrOfReceptorsToAdd -= added;
      } else {
        // If less or same points as needed add all points.
        nrOfReceptorsToAdd -= addReceptors(pointsToCalculate, size);
        receptorsToDo = List.of();
      }
    }
    LOGGER.debug("Calculate {} receptors for {} sources", pointsToCalculate.size(), sourcesSize);
    return pointsToCalculate;
  }

  private int addReceptors(final List<AeriusPoint> pointsToCalculate, final int maxAmountToAdd) throws AeriusException {
    int added = 0;
    for (int i = 0; i < maxAmountToAdd; i++) {
      final AeriusPoint aeriusPoint = receptorsToDo.get(i);

      if (checkPoint(aeriusPoint) && receptorsDone.add(aeriusPoint.getId())) {
        added++;
        pointsToCalculate.add(aeriusPoint);
      }
    }
    return added;
  }

  /**
   * Additional method to determine if a point needs to be added to be calculated.
   * For example if the receptor is to far away it should not be added
   * @param aeriusPoint point to check
   * @return true if point should be calculated
   * @throws AeriusException
   */
  protected boolean checkPoint(final AeriusPoint aeriusPoint) throws AeriusException {
    return true;
  }
}
