/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;

import java.sql.SQLException;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.conversion.JobPackets.SourceConverterSupplier;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.natura2k.AbstractNatura2KWorkDistributor;
import nl.overheid.aerius.calculation.natura2k.Natura2kReceptorStore;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.function.AeriusFunction;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * {@link WorkDistributor} implementation for calculating nature area receptors.
 * It can be used to either calculate receptors for the Wet Natuur Bescherming (Wnb) calculation or
 * to calculate all receptors in natura areas within a specific distance or all available receptors.
 */
class WNBDistanceWorkDistributor extends AbstractNatura2KWorkDistributor implements CalculationResultHandler {

  // Max distance + some safety margin. The margin is not relevant for the end results as the actual distance is calculated in OPS.
  private static final int WNB_MAX_DISTANCE_M = 25_500;

  private AeriusFunction<AeriusPoint, Boolean> checkPointFunction;
  private List<Entry<Integer, Integer>> listn2k;

  public WNBDistanceWorkDistributor(final PMF pmf, final Natura2kReceptorStore n2kReceptorStore, final SectorCategories sectorCategories)
      throws SQLException {
    super(pmf, n2kReceptorStore, sectorCategories);
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    final CalculationSetOptions options = calculationJob.getCalculationSetOptions();
    // Collect all areas that are at least 25km within the sources, other areas are ignored.
    final Stream<Entry<Integer, Integer>> stream = n2kReceptorStore.order(calculationJob.getTree()).stream();
    final Stream<Entry<Integer, Integer>> to2Collect;

    if (hasMaxDistance(options)) {
      final double maxDistance = options.isUseWNBMaxDistance() ? WNB_MAX_DISTANCE_M : options.getCalculateMaximumRange();
      // filter points that are beyond the max distance
      checkPointFunction = p -> calculationJob.getTree().findShortestDistance(p) <= maxDistance;
      // filter areas that are beyond the max distance
      to2Collect = stream.filter(e -> e.getValue() <= maxDistance);
    } else {
      // If no max distance points should always be accepted.
      checkPointFunction = p -> true;
      to2Collect = stream;
    }
    listn2k = to2Collect.collect(Collectors.toList());
    init(calculationJob, listn2k);
  }

  @Override
  protected SourceConverterSupplier createSourceConverterSupplier(final CalculationJob calculationJob) {
    return (con, situation) -> new WNBSourceConverter(con, calculationJob.getProvider(), situation);
  }

  private boolean hasMaxDistance(final CalculationSetOptions options) {
    return options.isUseWNBMaxDistance() || (options.isMaximumRangeRelevant() && options.getCalculateMaximumRange() > 0);
  }

  @Override
  protected boolean checkPoint(final AeriusPoint aeriusPoint) throws AeriusException {
    return checkPointFunction.apply(aeriusPoint);
  }

  @Override
  public void onFinish(final CalculationState state) {
    if (state == CalculationState.COMPLETED) {
      // If the calculation is finished call the assessment complete handler on all calculated assessment areas.
      // This is not optimal, because it could be done once an assessment area is complete.
      // But we don't track which assessment area is complete (and don't want to query the database on it).
      listn2k.forEach(e -> assessmentAreaCompleteHandlers.forEach(h -> h.onAssessmentAreaComplete(e.getKey())));
    }
  }
}
