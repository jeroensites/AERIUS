/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.SQLException;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Wraps the {@link WorkHandler} and iterates the {@link JobCalculation}'s over the work handlers.
 */
public class CalculationJobWorkHandler<P extends AeriusPoint> {

  private final WorkHandler<P> workHandler;
  private final CalculationJob calculationJob;
  private final WorkDistributor<P> workDistributor;
  private final CalculationResultQueue queue;

  public CalculationJobWorkHandler(final CalculationJob calculationJob, final WorkDistributor<P> workDistributor, final WorkHandler<P> workHandler,
      final CalculationResultQueue queue) {
    this.calculationJob = calculationJob;
    this.workDistributor = workDistributor;
    this.workHandler = workHandler;
    this.queue = queue;
  }

  /**
   * Perform all calculations until all work is done.
   *
   * @throws AeriusException
   * @throws SQLException
   * @throws InterruptedException
   * @throws TaskCancelledException
   */
  public void work() throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
    workDistributor.init(calculationJob);
    boolean first = true;

    while (workDistributor.hasWork(calculationJob.getCalculatorOptions())) {
      final WorkPacket<P> work = workDistributor.work();

      if (work == null) {
        continue; // hasWork lied!! it had no work...try again.
      }
      if (first) {
        queue.init(work.getJobPackets());
        first = false;
      }
      for (final JobPacket packet : work.getJobPackets()) {
        workHandler.work(calculationJob, packet, work.getPoints());
      }
    }
  }
}
