/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

/**
 * Store emission calculation results organized by point and with each point the result per group.
 */
class GroupResults {
  /**
   * Map<Receptor Id, GroupResultPoint>
   */
  private final Map<Integer, GroupResultPoint> results = new ConcurrentHashMap<>();
  /**
   * Expected number of results.
   */
  private final int expectedCount;

  /**
   * Creates a results storage expecting the given groups to be stored results for
   * @param expectedCount Expected number of results
   */
  public GroupResults(final int expectedCount) {
    this.expectedCount = expectedCount;
  }

  /**
   * Returns the calculation point with given id if all groups are calculated or null of not exists.
   *
   * @param id id of the calculation point
   * @return result if available and all results for the point are present else it returns null
   */
  public GroupResultPoint get(final Integer id) {
    final GroupResultPoint srp = results.get(id);

    return srp == null || srp.size() != expectedCount ? null : srp;
  }

  /**
   * Puts the result for the given result point.
   *
   * @param calculationEngine calculation engine the results are calculated with
   * @param groupId group id to relate the results with
   * @param resultPoint point and results to get emission results from
   * @return
   */
  public GroupResultPoint put(final CalculationEngine calculationEngine, final int groupId, final AeriusResultPoint resultPoint) {
    synchronized (this) {
      final GroupResultPoint srp = results.computeIfAbsent(resultPoint.getId(), id -> new GroupResultPoint((resultPoint)));

      srp.put(calculationEngine, groupId, resultPoint.getEmissionResults());
      return srp.size() == expectedCount ? srp : null;
    }
  }

  @Override
  public String toString() {
    return "GroupResults [expectedCount=" + expectedCount + ", results=" + results.size() + "]";
  }
}
