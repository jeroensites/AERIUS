/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

/**
 * Interface for the queue that receives the calculated results.
 */
public interface CalculationResultQueue {

  void init(List<JobPacket> packets);

  void clear();

  void put(CalculationEngine calculationEngine, Map<Integer, List<AeriusResultPoint>> results, int calculationId);

  ArrayList<AeriusResultPoint> pollTotalResults(int calculationId);

}
