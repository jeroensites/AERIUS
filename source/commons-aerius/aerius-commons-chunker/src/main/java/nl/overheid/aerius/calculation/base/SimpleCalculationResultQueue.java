/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

/**
 * Queue to collect calculated results before storing them in a persistent storage place.
 */
public class SimpleCalculationResultQueue implements CalculationResultQueue {

  private final Map<Integer, List<AeriusResultPoint>> completeResults = new HashMap<>();

  @Override
  public void init(final List<JobPacket> packets) {
    for (final JobPacket packet : packets) {
      completeResults.put(packet.getCalculationId(), new ArrayList<>());
    }
  }

  @Override
  public void clear() {
    completeResults.clear();
  }

  @Override
  public void put(final CalculationEngine calculationEngine, final Map<Integer, List<AeriusResultPoint>> results, final int calculationId) {
    synchronized (this) {
      results.forEach((k, v) -> completeResults.get(calculationId).addAll(v));
    }
  }

  @Override
  public ArrayList<AeriusResultPoint> pollTotalResults(final int calculationId) {
    final ArrayList<AeriusResultPoint> results = new ArrayList<AeriusResultPoint>();
    final List<AeriusResultPoint> queue = completeResults.get(calculationId);

    synchronized (this) {
      if (queue != null) {
        results.addAll(queue);
        queue.clear();
      }
    }
    return results;
  }

}
