/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Queue;

import nl.overheid.aerius.calculation.base.IncludeResultsFilter;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

/**
 * Queue to collected results from multiple calculations. When the multiple calculations are done for the same receptor set this queue keeps track if
 * the results for all sectors are available for a specific receptor. If that is the case the result is stored in the completed queue. The calculating
 * process can then pull these results from the queue.
 */
final class WNBCalculationResultQueue implements CalculationResultQueue {

  private final IncludeResultsFilter filter;

  /**
   * Map<Calculation Id, SectorResults>
   */
  private final Map<Integer, GroupResults> situationResults = new HashMap<>();
  /**
   * Map<Calculation Id, Queue<SectorsResultPoint>>
   */
  private final Map<Integer, Queue<AeriusResultPoint>> completeResults = new HashMap<>();

  public WNBCalculationResultQueue(final IncludeResultsFilter filter) {
    this.filter = filter;
  }

  /**
   * Initializes the queue with all calculations and sectors per calculation.
   */
  @Override
  public void init(final List<JobPacket> packets) {
    for (final JobPacket packet : packets) {
      addCalculation(packet.getCalculationId(), packet.getSourcesPackets().size());
    }
  }

  /**
   * Adds a calculation for the set of sectors that will be calculated.
   *
   * @param calculationId id of the calculation
   * @param sectors set of the sectors that will be calculated
   */
  void addCalculation(final int calculationId, final int groupCount) {
    situationResults.put(calculationId, new GroupResults(groupCount));
    completeResults.put(calculationId, new ArrayDeque<>());
  }

  /**
   * Clears all data. Only use when in case of canceling of job.
   */
  @Override
  public void clear() {
    situationResults.clear();
    completeResults.clear();
  }

  /**
   * Returns true if there results in the complete queue.
   *
   * @param calculationId the calculation id to check
   * @return true if results are present
   */
  public boolean hasResults(final int calculationId) {
    synchronized (this) {
      final Queue<AeriusResultPoint> queue = completeResults.get(calculationId);
      return queue != null && !queue.isEmpty();
    }
  }

  /**
   * Polls the completed queue for total results. If no results are present null is returned.
   *
   * @param calculationId the calculation to get the results for
   * @return null if no results else a result object
   */
  @Override
  public ArrayList<AeriusResultPoint> pollTotalResults(final int calculationId) {
    final ArrayList<AeriusResultPoint> results = new ArrayList<>();

    synchronized (this) {
      final Queue<AeriusResultPoint> queue = completeResults.get(calculationId);

      while (queue != null && !queue.isEmpty()) {
        results.add(queue.poll());
      }
    }
    return results;
  }

  /**
   * Puts the calculation results by calculation id and sector id into the queue and calculates for each point if all results are present, and if so
   * places the result on the completed queue.
   *
   * @param results calculation result
   * @param calculationId id of the calculation the results are from
   * @param sectorId id of the sector the results are from
   */
  @Override
  public void put(final CalculationEngine calculationEngine, final Map<Integer, List<AeriusResultPoint>> results, final int calculationId) {
    final GroupResults sr = situationResults.get(calculationId);
    Objects.requireNonNull(sr, "CalculationResultQueue not initialized for calculationId:" + calculationId);

    synchronized (this) {
      for (final Entry<Integer, List<AeriusResultPoint>> entry : results.entrySet()) {
        for (final AeriusResultPoint result : entry.getValue()) {
          putInCompleteIfDone(calculationId, sr.put(calculationEngine, entry.getKey(), result));
        }
      }
    }
  }

  private void putInCompleteIfDone(final int calculationId, final GroupResultPoint grp) {
    if (grp != null) {
      final AeriusResultPoint totalsResultPoint = grp.getTotalsResultPoint();

      if (filter.includeResult(totalsResultPoint)) {
        completeResults.get(calculationId).add(totalsResultPoint);
      }
    }
  }
}
