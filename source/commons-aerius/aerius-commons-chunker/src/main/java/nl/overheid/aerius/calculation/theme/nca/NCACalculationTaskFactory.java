/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.io.ADMSSourceFileType;
import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.calculation.base.CalculationTaskFactory;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * NCA {@link CalculationTaskFactory}. Creates the ADMS input data object.
 */
class NCACalculationTaskFactory implements CalculationTaskFactory {
  private static final Logger LOGGER = LoggerFactory.getLogger(NCACalculationTaskFactory.class);

  @Override
  public <E extends EngineSource, R extends AeriusPoint, T extends EngineInputData<E, R>> CalculationTask<E, R, T> createTask(
      final CalculationEngine calculationEngine, final CalculationJob calculationJob) throws AeriusException {
    if (calculationEngine == CalculationEngine.ADMS) {
      final ADMSInputData input = new ADMSInputData();

      input.setLicense(calculationJob.getAdmsLicense());
      input.setFileType(ADMSSourceFileType.UPL);
      return new CalculationTask<E, R, T>(WorkerType.ADMS, (T) input);
    } else {
      LOGGER.error("Calculation engine {} not supported for NCA theme.", calculationEngine);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }
}
