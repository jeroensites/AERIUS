/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.CsvOutputHandler;
import nl.overheid.aerius.calculation.SectorAndTotalResultDBHandler;
import nl.overheid.aerius.calculation.TotalResultDBHandler;
import nl.overheid.aerius.calculation.TotalResultDBUnsafeHandler;
import nl.overheid.aerius.calculation.WNBCalculationOptionsUtil;
import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.base.CalculatorThemeFactory;
import nl.overheid.aerius.calculation.base.IncludeResultsFilter;
import nl.overheid.aerius.calculation.base.IntermediateResultHandler;
import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.base.ResultPostProcessHandler;
import nl.overheid.aerius.calculation.base.WorkBySectorHandlerImpl;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.base.WorkHandler;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.DefaultCalculationEngineProvider;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.OPSCalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.calculation.natura2k.Natura2KReceptorLoader;
import nl.overheid.aerius.calculation.natura2k.Natura2KResultsReadyHandler;
import nl.overheid.aerius.calculation.natura2k.Natura2kReceptorStore;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.shared.domain.calculation.CalculationRoadOPS;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;
import nl.overheid.aerius.util.FeaturePointToEnginePointUtil;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * {@link CalculatorThemeFactory} implementation for PAS Wet-NB calculations.
 */
public class WNBCalculatorFactory implements CalculatorThemeFactory {

  private static final Logger LOGGER = LoggerFactory.getLogger(WNBCalculatorFactory.class);

  private static final OPSCalculationEngineProvider OPS_CALCULATION_ENGINE_PROVIDER = new OPSCalculationEngineProvider();
  private static final IncludeResultsFilter NO_FILTER = resultPoint -> true;

  private final PMF pmf;
  private final GridUtil gridUtil;
  private final GridSettings gridSettings;
  private final SectorCategories sectorCategories;
  private final Natura2kGridPointStore n2kPointStore;
  private final Natura2kReceptorStore n2kReceptorStore;
  private final WNBCalculationTaskFactory calculationTaskFactory;
  private final WNBRelevantResultsFilter wnbIncludeResultsFilter;
  private final DefaultCalculationEngineProvider defaultCalculationEngineProvider;

  /**
   * Recalculated the receptor id given the x-y coordinates in case a point is a receptor point.
   *
   * @param results list of result points.
   */
  private final ResultPostProcessHandler receptorPointResultsHandler;

  public WNBCalculatorFactory(final PMF pmf) throws SQLException, AeriusException {
    this.pmf = pmf;
    final ReceptorGridSettings rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(pmf);
    final ReceptorUtil receptorUtil = new ReceptorUtil(rgs);

    sectorCategories = SectorRepository.getSectorCategories(pmf, LocaleUtils.getDefaultLocale());
    gridSettings = new GridSettings(rgs);
    gridUtil = new GridUtil(gridSettings);
    n2kPointStore = loadNatura2kGridPointStore(pmf, receptorUtil);
    n2kReceptorStore = loadNatura2kReceptorStore(pmf, receptorUtil);
    calculationTaskFactory = new WNBCalculationTaskFactory(rgs);
    wnbIncludeResultsFilter = new WNBRelevantResultsFilter();
    receptorPointResultsHandler = (calculationId, results) -> {
      for (final AeriusResultPoint arp : results) {
        if (arp.getPointType() == AeriusPointType.RECEPTOR) {
          // This looks stupid, but an x-y coordinate for exact center of hexagon has a resolution that is bigger then 1 meter
          // While i.e. OPS has resolution of 1 meter, it means the x-y coordinates back from ops have lost detail information.
          // Therefore first calculate id and then set the exact x-y coordinate for the id.
          arp.setCoordinates(receptorUtil.getPointFromReceptorId(receptorUtil.getReceptorIdFromPoint(arp)).getCoordinates());
        }
      }
    };
    try (Connection con = pmf.getConnection()) {
      defaultCalculationEngineProvider = new DefaultCalculationEngineProvider(SectorRepository.getSectorProperties(con));
    }
  }

  /**
   * Load Natura2000 area data into Natura2kReceptorStore.
   *
   * @param pmf
   * @param receptorUtil
   * @return
   * @throws SQLException
   * @throws AeriusException
   */
  private Natura2kReceptorStore loadNatura2kReceptorStore(final PMF pmf, final ReceptorUtil receptorUtil) throws SQLException, AeriusException {
    final Natura2KReceptorLoader receptorLoader = new Natura2KReceptorLoader(receptorUtil, gridSettings);
    try (Connection con = pmf.getConnection()) {
      return receptorLoader.fillReceptorStore(con);
    }
  }

  /**
   * Load Natura2000 area data into Natura2kGridPointStore.
   *
   * @param pmf
   * @param receptorUtil
   * @return
   * @throws SQLException
   * @deprecated gridpointstore is for deprecated aggregation.
   */
  @Deprecated
  private Natura2kGridPointStore loadNatura2kGridPointStore(final PMF pmf, final ReceptorUtil receptorUtil) throws SQLException {
    final Natura2KReceptorLoader receptorLoader = new Natura2KReceptorLoader(receptorUtil, gridSettings);
    try (Connection con = pmf.getConnection()) {
      return receptorLoader.fillPointStore(con);
    }
  }

  @Override
  public void prepareOptions(final ScenarioCalculations scenarioCalculations) {
    final CalculationSetOptions options = scenarioCalculations.getOptions();

    if (options.getCalculationType() == CalculationType.PERMIT) {
      scenarioCalculations.getScenario().setOptions(WNBCalculationOptionsUtil.createWnbCalculationSetOptions());
    }
  }

  @Override
  public CalculationEngineProvider getProvider(final CalculationJob calculationJob) {
    return calculationJob.getCalculationSetOptions().getRoadOPS() == CalculationRoadOPS.DEFAULT
        ? defaultCalculationEngineProvider
        : OPS_CALCULATION_ENGINE_PROVIDER;
  }

  @Override
  public List<CalculationResultHandler> getCalculationResultHandler(final CalculationJob calculationJob, final ExportType exportType,
      final boolean uiCalculation, final Date exportDate) {
    final List<CalculationResultHandler> finishResultHandler = new ArrayList<>();
    finishResultHandler.add(createResultStorageHandler(calculationJob, exportType, uiCalculation));
    if (CalculationType.PERMIT != calculationJob.getCalculationSetOptions().getCalculationType()) {
      // Only delete low results if not WNB calculation.
      finishResultHandler.add(createNonInteractiveFinishResultHandler(calculationJob));
    }
    return finishResultHandler;
  }

  /**
   * Sets the {@link CalculationResultHandler} on the calculator.
   * @param uiOutput if true the output is intended for displaying the results in a browser.
   * @param sectorOuput if true results by sector should be stored.
   * @param csvOutput if true the output should be stored in a file instead in the database.
   * @param exportType
   * @return this object
   */
  private CalculationResultHandler createResultStorageHandler(final CalculationJob calculationJob, final ExportType exportType,
      final boolean uiOutput) {
    final CalculationResultHandler handler;

    if (exportType == ExportType.CSV) {
      LOGGER.trace("Add CsvOutputHandler");
      handler = new CsvOutputHandler(pmf, calculationJob);
      calculationJob.setUnitsAccumulator();
    } else if (exportType == ExportType.GML_WITH_SECTORS_RESULTS) {
      LOGGER.trace("Add SectorAndTotalResultDBHandler");
      handler = new SectorAndTotalResultDBHandler(pmf, calculationJob);
    } else if (uiOutput) {
      LOGGER.trace("Add TotalResultDBUnsafeHandler");
      handler = new TotalResultDBUnsafeHandler(pmf, calculationJob);
    } else {
      LOGGER.trace("Add TotalResultDBHandler");
      handler = new TotalResultDBHandler(pmf, calculationJob);
    }
    return handler;
  }

  private CalculationResultHandler createNonInteractiveFinishResultHandler(final CalculationJob calculationJob) {
    return new CalculationResultHandler() {
      @Override
      public void onFinish(final CalculationState state) {
        WNBUtil.deleteLowResults(pmf, calculationJob);
      }
    };
  }

  @Override
  public void prepareResultHandler(final ResultHandler resultHandler, final CalculationJob calculationJob) {
    resultHandler.addResultUpdateHandler(receptorPointResultsHandler);
    addNettingPostProcessorIfNeeded(resultHandler, calculationJob);
  }

  @Override
  public WorkHandler<AeriusPoint> createWorkHandler(final CalculationJob calculationJob, final CalculationTaskHandler remoteWorkHandler)
      throws AeriusException {
    final CalculationTaskHandler accumulatorHandler =
        calculationJob.isUnitsAccumulator() ? new UnitAccumulatorTaskHandler(calculationJob, remoteWorkHandler) : remoteWorkHandler;

    return new WorkBySectorHandlerImpl(calculationJob.getProvider(), calculationTaskFactory, accumulatorHandler);
  }

  private void addNettingPostProcessorIfNeeded(final ResultHandler resultHandler, final CalculationJob calculationJob) {
    if (NettingResultPostProcessHandler.canApply(calculationJob.getScenarioCalculations())) {
      resultHandler.addResultUpdateHandler(new NettingResultPostProcessHandler(calculationJob.getScenarioCalculations()));
    }
  }

  /**
   * Returns the filter if results below a threshold should be ignored.
   * @return
   */
  private IncludeResultsFilter getIncludeResultFilter(final CalculationSetOptions calculationSetOptions) {
    return CalculationType.PERMIT == calculationSetOptions.getCalculationType() ? wnbIncludeResultsFilter : NO_FILTER;
  }

  @Override
  public List<IntermediateResultHandler> getIntermediateResultHandlers(final CalculationJob calculationJob)
      throws AeriusException {
    return List.of(new Natura2KResultsReadyHandler(pmf, calculationJob));
  }

  @Override
  public CalculationResultQueue createCalculationResultQueue(final CalculationSetOptions calculationSetOptions) {
    return new WNBCalculationResultQueue(getIncludeResultFilter(calculationSetOptions));
  }

  @Override
  public WorkDistributor<AeriusPoint> createWorkDistributor(final CalculationJob calculationJob) throws AeriusException, SQLException {
    final WorkDistributor<AeriusPoint> wd;
    final CalculationType ct = calculationJob.getCalculationSetOptions().getCalculationType();

    switch (ct) {
    case CUSTOM_POINTS:
      wd = createCustomPointWorkDistributor(calculationJob);
      break;
    case PERMIT:
      wd = createWNBWorkDistributor(calculationJob);
      break;
    case NATURE_AREA:
      if (calculationJob.getCalculationSetOptions().isForceAggregation()) {
        wd = new WNBAggregatedWorkDistributor(pmf, gridUtil, n2kPointStore);
      } else {
        wd = new WNBDistanceWorkDistributor(pmf, n2kReceptorStore, sectorCategories);
      }
      break;
    case RADIUS:
    default:
      LOGGER.error("Unsupported calculation type:'{}'", ct);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    return wd;
  }

  private WorkDistributor<AeriusPoint> createCustomPointWorkDistributor(final CalculationJob calculationJob) throws SQLException {
    final List<AeriusPoint> points = FeaturePointToEnginePointUtil.convert(calculationJob.getScenarioCalculations().getCalculationPoints());

    return new CustomPointsWorkDistributor(pmf, gridUtil, points);
  }

  private WorkDistributor<AeriusPoint> createWNBWorkDistributor(final CalculationJob calculationJob) throws SQLException {
    final WNBDistanceWorkDistributor wnbWorkDistributor = new WNBDistanceWorkDistributor(pmf, n2kReceptorStore, sectorCategories);

    return calculationJob.getScenarioCalculations().getCalculationPoints().isEmpty() ?
        wnbWorkDistributor : new WNBWithCustomPointsWorkDistributor(wnbWorkDistributor, createCustomPointWorkDistributor(calculationJob));
  }
}
