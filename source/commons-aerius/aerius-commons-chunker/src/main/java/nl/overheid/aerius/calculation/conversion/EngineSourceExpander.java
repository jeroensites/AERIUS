/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.domain.WorkPacket.GroupedSourcesPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.GroupedSourcesPacketMap;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.validation.ValidationVisitor;

/**
 * Expands {@link EmissionSourceFeature} objects to {@link EngineSource} objects.
 */
public final class EngineSourceExpander {

  private EngineSourceExpander() {
    // util class
  }

  /**
   * Converts all {@link EmissionSourceFeature} geometries to engine sources.
   *
   * @param con Connection to use for queries.
   * @param sourceConverter converter class doing the actual conversion of the source
   * @param sourceConversionHelper
   * @param situation The situation containing emission sources to convert to engine sources.
   * @param substances substances to convert
   * @param stacking Switch to control the stacking of sources
   * @return The sources expanded to engine sources.
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException throws AeriusException in case of other problems
   */
  public static List<EngineSource> toEngineSources(final Connection con, final SourceConverter sourceConverter,
      final SourceConversionHelper sourceConversionHelper, final ScenarioSituation situation, final List<Substance> substances,
      final boolean stacking) throws SQLException, AeriusException {
    return toEngineSourcesByGroup(con, sourceConverter, sourceConversionHelper, situation, substances, stacking)
        .stream().flatMap(p -> p.getSources().stream()).collect(Collectors.toList());
  }

  /**
   * Converts all {@link EmissionSourceFeature} geometries to engine sources and return them by sector ID.
   *
   * @param con Connection to use for queries.
   * @param sourceConverter converter class doing the actual conversion of the source
   * @param sourceConversionHelper
   * @param situation The situation containing emission sources to convert to engine sources.
   * @param substances substances to convert
   * @param stacking Switch to control the stacking of sources
   * @return The sources expanded to engine sources by sector ID.
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException throws AeriusException in case of other problems
   */
  public static List<GroupedSourcesPacket> toEngineSourcesByGroup(final Connection con, final SourceConverter sourceConverter,
      final SourceConversionHelper sourceConversionHelper, final ScenarioSituation situation, final List<Substance> substances,
      final boolean stacking) throws SQLException, AeriusException {
    validateSources(sourceConverter, sourceConversionHelper, situation.getEmissionSourcesList());
    sourceConversionHelper.enforceEmissions(situation.getEmissionSourcesList());

    final GroupedSourcesPacketMap map = nonStacking(con, sourceConverter, sourceConversionHelper, situation, substances);

    return stacking ? stacking(map) : map.toGroupedSourcesPacket();
  }

  /**
   * Validates all sources and throws an exception if one of the sources doesn't validate.
   */
  private static void validateSources(final SourceConverter sourceConverter, final SourceConversionHelper sourceConversionHelper,
      final List<EmissionSourceFeature> sources) throws AeriusException {
    final List<AeriusException> errors = new ArrayList<>();
    final ValidationVisitor sourceValidator = new ValidationVisitor(errors, new ArrayList<>(), sourceConversionHelper.getValidationHelper());
    for (final EmissionSourceFeature emissionSource : sources) {
      sourceConverter.validate(emissionSource);
      emissionSource.accept(sourceValidator);
    }
    if (!errors.isEmpty()) {
      throw errors.get(0);
    }
  }

  private static GroupedSourcesPacketMap nonStacking(final Connection con, final SourceConverter sourceConverter,
      final SourceConversionHelper sourceConversionHelper, final ScenarioSituation situation, final List<Substance> substances)
      throws SQLException, AeriusException {
    final ExpanderVisitor visitor = new ExpanderVisitor(con, sourceConverter, substances, sourceConversionHelper, situation.getBuildingsList());

    for (final EmissionSourceFeature emissionSource : situation.getEmissionSourcesList()) {
      emissionSource.accept(visitor);
    }
    sourceConverter.postProcess(visitor.getExpandedSources());
    return visitor.getExpandedSources();
  }

  /**
   * Stacks sources with the same characteristics.
   */
  private static List<GroupedSourcesPacket> stacking(final GroupedSourcesPacketMap map) {
    return map.getMap().entrySet().stream()
        .map(e -> new GroupedSourcesPacket(e.getKey().getId(), e.getKey().getCalculationEngine(), SourceStacker.stack(e.getValue())))
        .collect(Collectors.toList());
  }
}
