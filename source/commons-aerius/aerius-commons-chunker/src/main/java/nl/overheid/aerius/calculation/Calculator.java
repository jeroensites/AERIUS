/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.Instant;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationJobWorkHandler;
import nl.overheid.aerius.calculation.base.WorkTracker;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * Calculator object to run the calculation. The Calculator is prepared with the {@link CalculatorBuilder} class via the
 * {@link CalculatorBuildDirector}. This class contains the running process that controls the calculation.
 */
public final class Calculator {

  private static final Logger LOGGER = LoggerFactory.getLogger(Calculator.class);

  private static final long RESULT_HANDLER_SLEEP_TIME_SECONDS = 1;

  private final PMF pmf;
  private final ScheduledExecutorService executor;
  private final CalculationJob calculationJob;
  private final WorkTracker tracker;
  private final IntermediateHandlerProcessor intermediateProcessor;
  private final TotalResultsProcessor resultsProcessor;
  private final CalculationJobWorkHandler<AeriusPoint> workHandler;

  Calculator(final PMF pmf, final ScheduledExecutorService executor, final CalculationJob calculationJob,
      final CalculationJobWorkHandler<AeriusPoint> workHandler, final TotalResultsProcessor resultsProcessor,
      final IntermediateHandlerProcessor intermediateProcessor, final WorkTracker tracker) {
    this.pmf = pmf;
    this.executor = executor;
    this.calculationJob = calculationJob;
    this.workHandler = workHandler;
    this.resultsProcessor = resultsProcessor;
    this.intermediateProcessor = intermediateProcessor;
    this.tracker = tracker;
  }

  /**
   * Starts a calculation and blocks until it is finished.
   *
   * @param calculationJob the job to calculate
   * @param calculationPoints optional set of points, used when providing a custom set of points to calculate
   * @param handler handler that processes the returned results.
   * @throws Exception
   */
  public CalculationJob calculate() throws Exception {
    final ScheduledFuture<?> resultHandlerFuture =
        executor.scheduleWithFixedDelay(resultsProcessor, RESULT_HANDLER_SLEEP_TIME_SECONDS, RESULT_HANDLER_SLEEP_TIME_SECONDS, TimeUnit.SECONDS);

    try {
      tracker.start();
      updateCalculationState(tracker.getState());
      // Send the work to workers.
      workHandler.work();
      // Wait for all results to be received from the workers.
      waitForWorkerCompletion();
      // Wait for all processing threads to complete (i.e. store results, intermediate handlers).
      waitForProcessorsCompletion(resultHandlerFuture);
    } catch (final TaskCancelledException e) {
      LOGGER.error("Calculation TaskCancelledException", e);
      tracker.cancel(e.getCause());
      throw e.getCause();
    } catch (final InterruptedException e) {
      LOGGER.error("Calculation interrupted:", e);
      tracker.cancel(e);
      Thread.currentThread().interrupt();
      throw e;
    } catch (final Exception e) {
      LOGGER.error("Calculation cancelled:", e);
      tracker.cancel(e);
      throw e;
    } finally {
      // This marks the calculation finished in the database.
      updateCalculationState(tracker.getState());
      LOGGER.info("Calculation finished with state '{}' in {} seconds.", tracker.getState(),
          Instant.now().minusSeconds(calculationJob.getStartTime().getEpochSecond()).getEpochSecond());
    }
    return calculationJob;
  }

  private void waitForProcessorsCompletion(final ScheduledFuture<?> resultHandlerFuture) throws Exception {
    // Make sure all data put in the queues is processed.
    // For example store all results in the database.
    cancelAndWaitForFuture(resultHandlerFuture);
    resultsProcessor.onFinish(tracker.getState());

    // Complete the intermediate processor if present.
    // For WNB this means calculate statistics for all assessment areas.
    if (intermediateProcessor != null) {
      intermediateProcessor.onFinish(tracker.getState());
    }
  }

  private static void cancelAndWaitForFuture(final Future<?> future) throws InterruptedException {
    future.cancel(false);
    try {
      future.get();
    } catch (final CancellationException e) {
      LOGGER.trace("Future cancelled, as expected", e);
    } catch (final ExecutionException e) {
      LOGGER.error("Future threw an exception, which is unexpected", e);
    }
  }

  /**
   * Waits for all results to be returned by the workers.
   *
   * @throws Exception
   */
  private void waitForWorkerCompletion() throws Exception {
    final CalculationState state = tracker.waitForCompletion();

    if (state == CalculationState.CANCELLED) {
      throw tracker.getException();
    }
  }

  private void updateCalculationState(final CalculationState state) throws SQLException {
    try (final Connection con = pmf.getConnection()) {
      for (final Calculation calculation : calculationJob.getCalculations()) {
        CalculationRepository.updateCalculationState(con, calculation.getCalculationId(), state);
      }
      calculationJob.setState(state);
    }
  }
}
