/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.srm2.conversion.SRMSourceConverter;

class MonitorSRM2NetworksStore {

  private static final Logger LOG = LoggerFactory.getLogger(MonitorSRM2NetworksStore.class);

  private final SRMSourceConverter sourceConverter = new SRMSourceConverter();
  private final Map<Integer, List<EmissionSourceFeature>> srm2NetworksByYear = new HashMap<>();

  public void put(final int year, final List<EmissionSourceFeature> srm2Network) {
    srm2NetworksByYear.put(year, srm2Network);
  }

  public List<EngineSource> get(final int year, final List<Substance> substances) throws AeriusException {
    if (srm2NetworksByYear.containsKey(year)) {
      final List<EngineSource> results = new ArrayList<>();
      sourceConverter.convert(results, srm2NetworksByYear.get(year), substances);
      return results;
    }

    LOG.error("No Monitor SRM2 network for year {}", year);
    throw new AeriusException(ImaerExceptionReason.SRM_NO_MONITOR_SRM2_DATA_FOR_YEAR, String.valueOf(year));
  }

}
