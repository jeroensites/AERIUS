/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.SQLException;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.calculation.domain.WorkPacket.GroupedSourcesPacket;
import nl.overheid.aerius.calculation.domain.WorkPacket.JobPacket;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * {@link WorkHandler} implementation that splits the work into smaller tasks grouped by by sector.
 */
public class WorkBySectorHandlerImpl implements WorkHandler<AeriusPoint> {

  private final CalculationTaskFactory taskFactory;
  private final CalculationTaskHandler calculationTaskHandler;
  private final CalculationEngineProvider provider;

  public WorkBySectorHandlerImpl(final CalculationEngineProvider provider, final CalculationTaskFactory taskFactory,
      final CalculationTaskHandler calculationTaskHandler) {
    this.provider = provider;
    this.taskFactory = taskFactory;
    this.calculationTaskHandler = calculationTaskHandler;
  }

  /**
   * Do work for a given calculation and given set of receptors.
   */
  @Override
  public void work(final CalculationJob calculationJob, final JobPacket jobPacket, final Collection<AeriusPoint> calculationPoints)
      throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
    final Map<CalculationEngine, CalculationTask> taskMap = new EnumMap<>(CalculationEngine.class);

    for (final GroupedSourcesPacket entry : jobPacket.getSourcesPackets()) {
      final CalculationTask<EngineSource, AeriusPoint, EngineInputData<EngineSource, AeriusPoint>> task =
          computeIfAbsent(taskMap, entry.getKey().getCalculationEngine(), calculationJob);

      task.setSources(entry.getKey().getId(), entry.getSources());
    }
    final CalculationSetOptions cso = calculationJob.getCalculationSetOptions();

    for (final Entry<CalculationEngine, CalculationTask> taskEntry : taskMap.entrySet()) {
      final CalculationTask task = taskEntry.getValue();
      final WorkKey workKey = new WorkKey(task.getWorkerType().type().getWorkerQueueName(), calculationJob.getJobIdentifier(),
          jobPacket.getCalculationId(), taskEntry.getKey());

      task.setOptions(cso.getSubstances(), cso.getEmissionResultKeys(), jobPacket.getYear(), cso.getMeteo());
      taskFactory.setTaskOptions(task, calculationJob);
      calculationTaskHandler.work(workKey, task, calculationPoints);
    }
  }

  private CalculationTask<EngineSource, AeriusPoint, EngineInputData<EngineSource, AeriusPoint>> computeIfAbsent(
      final Map<CalculationEngine, CalculationTask> taskMap, final CalculationEngine calculationEngine, final CalculationJob calculationJob)
          throws AeriusException {
    final CalculationTask<EngineSource, AeriusPoint, EngineInputData<EngineSource, AeriusPoint>> task;
    // No real computeIfAbsent because createTask throws a checked Exception. bleh.
    if (taskMap.containsKey(calculationEngine)) {
      task = taskMap.get(calculationEngine);
    } else {
      task = taskFactory.createTask(calculationEngine, calculationJob);
      taskMap.put(calculationEngine, task);
    }
    return task;
  }
}
