/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Interface to handle separate logic needed for calculation for different themes.
 */
public interface CalculatorThemeFactory {

  /**
   * Called on initialization of a calculation and can be used to force certain options.
   *
   * @param scenarioCalculations scenario to being calculated
   * @throws AeriusException
   */
  void prepareOptions(ScenarioCalculations scenarioCalculations) throws AeriusException;

  /**
   * Returns the {@link CalculationEngineProvider} related to this theme.
   */
  CalculationEngineProvider getProvider(CalculationJob calculationJob);

  /**
   * Returns a list of intermediateResultHandlers. The default implementation returns an empty list.
   *
   * @param calculationJob
   * @return
   * @throws AeriusException
   */
  default List<IntermediateResultHandler> getIntermediateResultHandlers(final CalculationJob calculationJob) throws AeriusException {
    return List.of();
  }

  /**
   * Returns additional handlers for theme specific actions. Should return an empty list if no handlers available to return.
   *
   * @param calculationJob
   * @param exportType
   * @param uiCalculation if this calculation is for the ui or background task
   * @param exportDate the moment the export started
   * @throws AeriusException
   */
  List<CalculationResultHandler> getCalculationResultHandler(CalculationJob calculationJob, ExportType exportType,
      boolean uiCalculation, Date exportDate) throws AeriusException;

  /**
   * @param calculationSetOptions calculation options
   * @return a new queue that collects the calculated results.
   */
  CalculationResultQueue createCalculationResultQueue(CalculationSetOptions calculationSetOptions);

  /**
   * Option to add additional processing to the result handler. The default does nothing.
   *
   * @param resultHandler Result handler to use
   * @param calculationJob job containing calculation specific details.
   */
  default void prepareResultHandler(final ResultHandler resultHandler, final CalculationJob calculationJob) {
  }

  /**
   * Creates a theme specific work handler that can be used to process the input before it is send to be calculated.
   *
   * @param calculationJob data object with input data
   * @param workHandler handler to which the processed calculation should be passed on
   * @return a theme specific work handler
   * @throws AeriusException
   */
  WorkHandler<AeriusPoint> createWorkHandler(CalculationJob calculationJob, CalculationTaskHandler workHandler) throws AeriusException;

  /**
   * Creates a theme specific {@link WorkDistributor} object.
   */
  WorkDistributor<AeriusPoint> createWorkDistributor(CalculationJob calculationJob)
      throws AeriusException, SQLException;
}
