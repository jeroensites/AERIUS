/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.emissions.CategoryBasedEmissionFactorSupplier;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.emissions.EmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.EmissionsUpdater;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.GeometryCalculator;
import nl.overheid.aerius.srm.io.LegacyNSLImportReader;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.GeometryCalculatorImpl;

class MonitorSRM2Loader {

  private static final Logger LOG = LoggerFactory.getLogger(MonitorSRM2Loader.class);
  private static final Pattern SRM2_FILE_PATTERN = Pattern.compile("srm2_(\\d+).csv");

  private static final String MONITOR_SRM2_VERSION = "2020";

  private final GeometryCalculator geometryCalculator = new GeometryCalculatorImpl();
  private final SectorCategories categories;

  MonitorSRM2Loader(final SectorCategories categories) {
    this.categories = categories;
  }

  MonitorSRM2NetworksStore loadMonitorSRM2Data(final File directory) {
    LOG.info("Loading Monitor SRM2 network data");
    final File versionDirectory = new File(directory, MONITOR_SRM2_VERSION);
    LOG.info("Scanning {}", versionDirectory.getAbsolutePath());
    final MonitorSRM2NetworksStore store = new MonitorSRM2NetworksStore();
    try {
      final List<MonitorSRM2File> srm2Files = findFiles(versionDirectory, SRM2_FILE_PATTERN);

      for (final MonitorSRM2File srm2File : srm2Files) {
        LOG.info("Loading {} for Monitor SRM2 data", srm2File.file.getName());
        final LegacyNSLImportReader reader = new LegacyNSLImportReader();
        final List<EmissionSourceFeature> network = readObjects(reader, srm2File.file);

        // PMF is only used for shipping, so should be OK to not supply it here.
        final EmissionFactorSupplier emissionFactorSupplier = new CategoryBasedEmissionFactorSupplier(categories, null, srm2File.year);
        final EmissionsUpdater emissionsUpdater = new EmissionsUpdater(emissionFactorSupplier, geometryCalculator);
        emissionsUpdater.updateEmissions(network);

        store.put(srm2File.year, network);
      }
    } catch (final AeriusException | IOException exception) {
      throw new RuntimeException("Error while reading Monitor SRM2 files.", exception);
    }
    return store;
  }

  private List<EmissionSourceFeature> readObjects(final LegacyNSLImportReader reader, final File file) throws IOException, AeriusException {
    try (final InputStream is = new FileInputStream(file)) {
      final ImportParcel importResult = new ImportParcel();
      reader.read(file.getName(), is, categories, null, importResult);
      final List<AeriusException> exceptions = importResult.getExceptions();

      if (!exceptions.isEmpty()) {
        throw exceptions.get(0);
      }
      return importResult.getSituation().getEmissionSourcesList();
    }
  }

  private List<MonitorSRM2File> findFiles(final File directory, final Pattern pattern) throws FileNotFoundException {
    return FileUtil.getFilteredFiles(directory, pattern, (dir, name, matcher) -> {
      final MonitorSRM2File filename = new MonitorSRM2File();

      filename.file = new File(dir, name);
      filename.year = Integer.parseInt(matcher.group(1));
      return filename;
    });
  }

  private static class MonitorSRM2File {
    File file;
    int year;
  }

}
