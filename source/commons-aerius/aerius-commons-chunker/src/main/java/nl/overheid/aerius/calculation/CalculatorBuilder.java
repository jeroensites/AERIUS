/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor;
import nl.overheid.aerius.calculation.base.CalculationJobWorkHandler;
import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.base.CalculatorThemeFactory;
import nl.overheid.aerius.calculation.base.IntermediateResultHandler;
import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.base.WorkHandler;
import nl.overheid.aerius.calculation.base.WorkTracker;
import nl.overheid.aerius.calculation.domain.AssessmentAreaCompleteHandler;
import nl.overheid.aerius.calculation.domain.AssessmentAreaCompleteHandler.HasAssessmentAreaCompleteHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Creates a Calculator object on which all handlers can be added. When done with the call to {@link #build(RabbitMQQueueMonitor, TaskManagerClient)}
 * the Calculator object is completed and ready to run.
 */
final class CalculatorBuilder {

  private static final Logger LOGGER = LoggerFactory.getLogger(CalculatorBuilder.class);

  private final PMF pmf;
  private final ScheduledExecutorService executor;
  private final CalculatorThemeFactory themeFactory;
  private final CalculationJob calculationJob;
  private final MultipleCalculationResultHandler calculationResultHandler = new MultipleCalculationResultHandler();

  private IntermediateHandlerProcessor intermediateHandlerProcessor;

  public CalculatorBuilder(final PMF pmf, final ScheduledExecutorService executor, final CalculatorThemeFactory themeFactory,
      final CalculationJob calculationJob) {
    this.pmf = pmf;
    this.executor = executor;
    this.themeFactory = themeFactory;
    this.calculationJob = calculationJob;
  }

  /**
   * Sets the {@link CalculationResultHandler} on the calculator.
   * @param exportType
   * @param uiOutput if true the output is intended for displaying the results in a browser.
   * @param exportDate The moment of the export.
   * @return this object
   * @throws AeriusException
   */
  public CalculatorBuilder setCalculationResultHandler(final ExportType exportType, final boolean uiOutput, final Date exportDate)
      throws AeriusException {
    themeFactory.getCalculationResultHandler(calculationJob, exportType, uiOutput, exportDate).forEach(calculationResultHandler::addHandler);
    return this;
  }

  /**
   * Set the progress tracking of the calculation in the database. Used in combination with a database Job.
   * @return this object
   */
  public CalculatorBuilder setTrackJobHandler() throws AeriusException {
    LOGGER.trace("Add TrackJobProgressHandler");
    final TrackJobProgressHandler trackJobProgressHandler = new TrackJobProgressHandler(pmf, calculationJob);
    calculationResultHandler.addHandler(trackJobProgressHandler);
    trackJobProgressHandler.init();
    return this;
  }

  /**
   * Sets the intermediate result handler to send results when they arrive. Used when showing results in the browser.
   * @param resultCallback callback to send intermediate results to if not null
   * @return this object
   */
  public CalculatorBuilder setIntermediateHandler(final WorkerIntermediateResultSender resultCallback) throws AeriusException {
    if (resultCallback != null) {
      intermediateHandlerProcessor = new IntermediateHandlerProcessor(resultCallback);
      themeFactory.getIntermediateResultHandlers(calculationJob).forEach(
          handler -> {
            if (handler instanceof CalculationResultHandler) {
              calculationResultHandler.addHandler((CalculationResultHandler) handler);
            }
            intermediateHandlerProcessor.add(handler);
          });
    }
    return this;
  }

  /**
   * Builds the calculator object and stitches everything together.
   * @param rabbitMQWorkerMonitor
   * @param taskManagerClient
   * @param workerType
   * @return new Calculator object
   */
  public Calculator build(final RabbitMQWorkerMonitor rabbitMQWorkerMonitor, final TaskManagerClient taskManagerClient, final WorkerType workerType)
      throws AeriusException, SQLException {
    LOGGER.trace("Build the Calculator object");

    final DynamicWorkSemaphore workLocker = new DynamicWorkSemaphore(rabbitMQWorkerMonitor, workerType);
    final WorkTracker tracker = new WorkTrackerImpl();
    final WorkMonitorImpl workMonitor = new WorkMonitorImpl(tracker, workLocker);
    calculationResultHandler.addHandler(workMonitor);
    final CalculationResultQueue queue = themeFactory.createCalculationResultQueue(calculationJob.getCalculationSetOptions());

    LOGGER.trace("Create Handlers");
    final ResultHandler resultHandler = new ResultHandlerImpl(workMonitor, queue, calculationResultHandler);
    final CalculationTaskHandler remoteWorkHandler =
        new RemoteCalculationTaskHandler(taskManagerClient, resultHandler, calculationJob.getQueueName());
    final CalculationTaskHandler workSplitHandler = wrapByWorkSplitHandler(remoteWorkHandler);
    themeFactory.prepareResultHandler(resultHandler, calculationJob);

    LOGGER.trace("Create WorkHandler");
    final WorkHandler<AeriusPoint> workHandler = themeFactory.createWorkHandler(calculationJob, workSplitHandler);
    LOGGER.trace("Create WorkDistributor");
    final List<IntermediateResultHandler> intermediateHandlers =
        intermediateHandlerProcessor == null ? List.of() : intermediateHandlerProcessor.getIntermediateHandlers();
    final WorkDistributor<AeriusPoint> workDistributor = themeFactory.createWorkDistributor(calculationJob);
    if (workDistributor instanceof CalculationResultHandler) {
      calculationResultHandler.addHandler((CalculationResultHandler) workDistributor);
    }
    addAssessmentAreaCompleteHandlers(intermediateHandlers, workDistributor);
    final CalculationJobWorkHandler<AeriusPoint> workJobHandler = new CalculationJobWorkHandler<>(calculationJob, workDistributor, workHandler, queue);
    final TotalResultsProcessor resultsProcessor = new TotalResultsProcessor(queue, calculationResultHandler, calculationJob, tracker);

    return new Calculator(pmf, executor, calculationJob, workJobHandler, resultsProcessor, intermediateHandlerProcessor, tracker);
  }

  /**
   * Determines if the {@link WorkSplitterCalculationTaskHandler} should be added. This is needed when the {@link WorkDistributor}
   * doesn't split the calculation points into reasonable sized tasks.
   *
   * @param calculationTaskHandler the CalculationTaskHandler to wrap.
   * @return a wrapped CalculationTaskHandler or the CalculationTaskHandler itself
   */
  private CalculationTaskHandler wrapByWorkSplitHandler(final CalculationTaskHandler calculationTaskHandler) {
    final CalculationTaskHandler taskSplitHandler;

    if (calculationJob.getCalculationSetOptions().getCalculationType() == CalculationType.PERMIT) {
      taskSplitHandler = calculationTaskHandler;
    } else {
      final CalculatorOptions calculatorOptions = calculationJob.getCalculatorOptions();
      taskSplitHandler = new WorkSplitterCalculationTaskHandler(calculationTaskHandler,
          calculatorOptions.getMaxCalculationEngineUnits(), calculatorOptions.getMinReceptor());
    }
    return taskSplitHandler;
  }

  private void addAssessmentAreaCompleteHandlers(final List<IntermediateResultHandler> intermediateResultHandler,
      final WorkDistributor<AeriusPoint> wd) {
    if (wd instanceof HasAssessmentAreaCompleteHandler) {
      intermediateResultHandler.stream()
          .filter(AssessmentAreaCompleteHandler.class::isInstance)
          .map(AssessmentAreaCompleteHandler.class::cast)
          .forEach(h -> ((HasAssessmentAreaCompleteHandler) wd).addAssessmentAreaCompleteHandler(h));
    }
  }
}
