/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.domain.WorkPacket;
import nl.overheid.aerius.calculation.grid.GridPointStore;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.wnb.ReceptorRepository;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Work distributor to calculate a custom list of points.
 */
class CustomPointsWorkDistributor implements WorkDistributor<AeriusPoint> {
  private final PMF pmf;
  private final GridUtil gridUtil;
  private final List<AeriusPoint> calculationPoints;

  private final JobPacketAggregator aggregator;

  private Iterator<Entry<Integer, Collection<AeriusPoint>>> pointStoreIterator;

  public CustomPointsWorkDistributor(final PMF pmf, final GridUtil gridUtil, final List<AeriusPoint> calculationPoints) throws SQLException {
    this.pmf = pmf;
    this.gridUtil = gridUtil;
    this.calculationPoints = calculationPoints;
    aggregator = new JobPacketAggregator(pmf, gridUtil);
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    final GridPointStore pointStore = new GridPointStore(gridUtil.getGridSettings());

    fillPoints(pointStore, calculationPoints);
    setOptionalTerrainData(pointStore);
    pointStoreIterator = pointStore.entrySet().iterator();
    aggregator.init(calculationJob);
  }

  @Override
  public boolean hasWork(final CalculatorOptions calculationOptions) throws AeriusException {
    return pointStoreIterator.hasNext();
  }

  @Override
  public WorkPacket<AeriusPoint> work() {
    final Entry<Integer, Collection<AeriusPoint>> next = pointStoreIterator.next();

    return new WorkPacket<AeriusPoint>(next.getValue(), aggregator.getSourcesFor(next.getKey()));
  }

  private void setOptionalTerrainData(final GridPointStore pointStore) throws SQLException {
    for (final Entry<Integer, Collection<AeriusPoint>> entry : pointStore.entrySet()) {
      ReceptorRepository.setTerrainData(pmf, entry.getValue());
    }
  }

  /**
   * Put calculationPoints in store receptors.
   * @param pointStore
   * @param calculationPoints
   */
  private void fillPoints(final GridPointStore pointStore, final List<AeriusPoint> calculationPoints) {
    for (final AeriusPoint point : calculationPoints) {
      final AeriusPoint pointToAdd;
      if (!(point instanceof OPSReceptor)) {
        pointToAdd = new OPSReceptor(point);
      } else {
        pointToAdd = point;
      }
      pointStore.add(pointToAdd);
    }
  }
}
