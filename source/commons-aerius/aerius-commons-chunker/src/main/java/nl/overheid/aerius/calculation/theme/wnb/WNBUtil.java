/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationRepository;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;

/**
 * Util class for calculating to the PAS.
 */
public final class WNBUtil {

  private static final Logger LOG = LoggerFactory.getLogger(WNBUtil.class);

  private WNBUtil() {
    // util
  }

  /**
   * Delete result below the threshold value if the calculation is a WNB calculation.
   * @param pmf database manager
   * @param calculatorJob job to delete results for.
   */
  public static void deleteLowResults(final PMF pmf, final CalculationJob calculatorJob) {
    if (calculatorJob.getCalculationSetOptions().getCalculationType() == CalculationType.PERMIT) {
      try (final Connection con = pmf.getConnection()) {
        for (final Calculation calculation : calculatorJob.getScenarioCalculations().getCalculations()) {
          CalculationRepository.deleteLowCalculationResults(con, calculation.getCalculationId());
        }
      } catch (final SQLException e) {
        // suppress error
        LOG.error("deleteLowResults failed", e);
      }
    }
  }
}
