/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.natura2k;

import java.io.Serializable;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import nl.overheid.aerius.calculation.base.IntermediateResultHandler;
import nl.overheid.aerius.calculation.domain.AssessmentAreaCompleteHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.JobRepositoryBean;
import nl.overheid.aerius.db.common.results.ResultsSummaryRepository;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * {@link IntermediateResultHandler} to send id's of completed assessment areas via as intermediate result.
 */
public class Natura2KResultsReadyHandler implements AssessmentAreaCompleteHandler, IntermediateResultHandler {

  private final Queue<Integer> queue = new ConcurrentLinkedQueue<>();
  private final ResultsSummaryRepository summaryRepository;
  private final JobRepositoryBean jobRepository;
  private final CalculationJob calculationJob;

  public Natura2KResultsReadyHandler(final PMF pmf, final CalculationJob calculationJob) {
    this.summaryRepository = new ResultsSummaryRepository(pmf);
    this.jobRepository = new JobRepositoryBean(pmf);
    this.calculationJob = calculationJob;
  }

  @Override
  public Serializable get() throws AeriusException {
    final Integer areaId;
    // synchronize only poll the area id and not synchronize the querying of the summary because querying is expensive and would block adding new ids
    synchronized (queue) {
      areaId = queue.isEmpty() ? null : queue.poll();
    }
    return areaId == null ? null : queryCalculationSummary(areaId);
  }

  private synchronized Serializable queryCalculationSummary(final Integer areaId) throws AeriusException {
    final int jobId = jobRepository.getJobId(calculationJob.getJobIdentifier().getCorrelationId());
    final SituationCalculations situationCalculations = jobRepository.getSituationCalculations(calculationJob.getJobIdentifier().getCorrelationId());
    summaryRepository.insertResultsSummaryForArea(jobId, situationCalculations, areaId);

    // Return value doesn't matter, as long as it's not null
    return true;
  }

  @Override
  public void onAssessmentAreaComplete(final int assessmentId) {
    synchronized (queue) {
      queue.add(assessmentId);
    }
  }
}
