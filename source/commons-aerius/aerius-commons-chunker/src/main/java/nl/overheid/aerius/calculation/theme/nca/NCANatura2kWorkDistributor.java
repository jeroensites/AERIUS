/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.nca;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.conversion.JobPackets.SourceConverterSupplier;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.natura2k.AbstractNatura2KWorkDistributor;
import nl.overheid.aerius.calculation.natura2k.Natura2kReceptorStore;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.function.AeriusFunction;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * Distributor for NCA theme.
 */
class NCANatura2kWorkDistributor extends AbstractNatura2KWorkDistributor implements CalculationResultHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(NCANatura2kWorkDistributor.class);

  private static final Integer STORAGE_SECTOR_ID = 1;
  private static final ADMSSourceConverter SOURCE_CONVERTER = new ADMSSourceConverter(STORAGE_SECTOR_ID);
  private static final SourceConverterSupplier SOURCE_CONVERTER_SUPPLIER = (con, situation) -> SOURCE_CONVERTER;
  private static final double MAX_DISTANCE_M = 15_000;

  private AeriusFunction<AeriusPoint, Boolean> checkPointFunction;
  private List<Entry<Integer, Integer>> listn2k;

  public NCANatura2kWorkDistributor(final PMF pmf, final Natura2kReceptorStore n2kReceptorStore, final ReceptorUtil receptorUtil) throws SQLException {
    super(pmf, n2kReceptorStore, SectorRepository.getSectorCategories(pmf, Locale.ENGLISH));
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    final CalculationType calculationType = calculationJob.getCalculationSetOptions().getCalculationType();

    if (calculationType != CalculationType.PERMIT) {
      LOGGER.error("Unsupported calculation type for NCA calculation:'{}'", calculationType);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    // filter points that are beyond the max distance
    checkPointFunction = p -> calculationJob.getTree().findShortestDistance(p) <= MAX_DISTANCE_M;

    listn2k = n2kReceptorStore.order(calculationJob.getTree()).stream()
        .filter(e -> e.getValue() <= MAX_DISTANCE_M)
        .collect(Collectors.toList());
    init(calculationJob, listn2k);
  }

  @Override
  protected SourceConverterSupplier createSourceConverterSupplier(final CalculationJob calculationJob) {
    return SOURCE_CONVERTER_SUPPLIER;
  }

  @Override
  protected boolean checkPoint(final AeriusPoint aeriusPoint) throws AeriusException {
    return checkPointFunction.apply(aeriusPoint);
  }

  @Override
  public void onFinish(final CalculationState state) {
    if (state == CalculationState.COMPLETED) {
      // If the calculation is finished call the assessment complete handler on all calculated assessment areas.
      // This is not optimal, because it could be done once an assessment area is complete.
      // But we don't track which assessment area is complete (and don't want to query the database on it).
      listn2k.forEach(e -> assessmentAreaCompleteHandlers.forEach(h -> h.onAssessmentAreaComplete(e.getKey())));
    }
  }
}
