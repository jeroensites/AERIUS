/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor;
import nl.overheid.aerius.calculation.base.CalculatorThemeFactory;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.theme.nca.NCACalculatorFactory;
import nl.overheid.aerius.calculation.theme.rbl.RBLCalculatorFactory;
import nl.overheid.aerius.calculation.theme.wnb.WNBCalculatorFactory;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Director class that constructs a complete calculator object. This class should be instantiated only once preferable.
 *
 * Director constructs a calculator object that is completely initialized. Then the calculator can be started which is a blocking process.
 */
public class CalculatorBuildDirector {

  private static final Logger LOGGER = LoggerFactory.getLogger(CalculatorBuildDirector.class);

  private final ScheduledExecutorService executor;
  private final PMF pmf;
  private final RabbitMQWorkerMonitor rabbitMQWorkerMonitor;
  private final WorkerType workerType;
  private final TaskManagerClient taskManagerClient;
  private final CalculatorThemeFactory rblCalculatorThemeFactory;
  private final CalculatorThemeFactory wnbCalculatorThemeFactory;
  private final NCACalculatorFactory ncaCalculatorThemeFactory;

  public CalculatorBuildDirector(final PMF pmf, final BrokerConnectionFactory factory, final int nrOfWorkers, final WorkerType workerType)
      throws SQLException, IOException, AeriusException {
    this(pmf, factory, nrOfWorkers, workerType, null);
  }

  public CalculatorBuildDirector(final PMF pmf, final BrokerConnectionFactory factory, final int nrOfWorkers, final WorkerType workerType,
      final File nslMonitorSrm2Directory) throws SQLException, IOException, AeriusException {
    this(pmf, factory, nrOfWorkers, new RabbitMQWorkerMonitor(factory), workerType, nslMonitorSrm2Directory);
  }

  /**
   * Constructor to be used for testing only!!! It doesn't start the QueueMonitor if test is true.
   *
   * @param pmf
   * @param factory
   * @param test if true runs in test mode.
   * @throws SQLException
   * @throws AeriusException
   */
  public CalculatorBuildDirector(final PMF pmf, final BrokerConnectionFactory factory, final int nrOfWorkers,
      final RabbitMQWorkerMonitor rabbitMQWorkerMonitor, final WorkerType workerType, final File nslMonitorSrm2Directory)
          throws SQLException, IOException, AeriusException {
    this.pmf = pmf;
    rblCalculatorThemeFactory = new RBLCalculatorFactory(pmf, nslMonitorSrm2Directory);
    wnbCalculatorThemeFactory = new WNBCalculatorFactory(pmf);
    ncaCalculatorThemeFactory = new NCACalculatorFactory(pmf);
    taskManagerClient = new TaskManagerClient(factory);
    this.rabbitMQWorkerMonitor = rabbitMQWorkerMonitor;
    this.workerType = workerType;
    // Each process uses 2 scheduled threads. Therefore start scheduler with 2 times number of processes.
    executor = Executors.newScheduledThreadPool(nrOfWorkers * 2);
    rabbitMQWorkerMonitor.start();
  }

  public void shutdown() {
    try {
      rabbitMQWorkerMonitor.shutdown();
    } finally {
      executor.shutdown();
    }
  }

  /**
   * Constructs a {@link Calculator} object.
   * @param inputData
   * @param jobIdentifier
   * @param intermediateResultCallback
   * @return
   * @throws AeriusException
   * @throws SQLException
   */
  public Calculator construct(final CalculationInputData inputData, final JobIdentifier jobIdentifier,
      final WorkerIntermediateResultSender intermediateResultCallback) throws AeriusException, SQLException {
    final CalculatorBuilder builder = init(inputData, jobIdentifier)
        .setCalculationResultHandler(inputData.getExportType(), intermediateResultCallback != null, inputData.getCreationDate())
        .setIntermediateHandler(intermediateResultCallback);

    if (inputData.isTrackJobProcess()) {
      builder.setTrackJobHandler();
    }
    return builder.build(rabbitMQWorkerMonitor, taskManagerClient, workerType);
  }

  /**
   * Initializes a CalculationJob, inserts the calculation in the database.
   */
  private CalculatorBuilder init(final CalculationInputData inputData, final JobIdentifier jobIdentifier) throws AeriusException {
    LOGGER.info("Init calculation.");
    final CalculatorThemeFactory themeFactory = getCalculatorThemeFactory(inputData.getScenario().getTheme());
    final Scenario scenario = inputData.getScenario();
    final ScenarioCalculations scenarioCalculations = new ScenarioCalculationsImpl(scenario);

    themeFactory.prepareOptions(scenarioCalculations);
    final CalculationJob calculationJob;

    try (final Connection con = pmf.getConnection()) {
      final CalculatorOptions calculatorOptions = getCalculatorOptions(con, inputData.getExportType() == ExportType.CALCULATION_UI);
      calculationJob = InitCalculation.initCalculation(con, scenarioCalculations, inputData.getName(), inputData.getQueueName(), calculatorOptions,
          !inputData.isCsvOutput(), inputData.getAdmsLicense(), jobIdentifier);
    } catch (final SQLException e) {
      LOGGER.error("Error while trying to init calculation", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    calculationJob.setProvider(themeFactory.getProvider(calculationJob));

    return new CalculatorBuilder(pmf, executor, themeFactory, calculationJob);
  }

  protected CalculatorOptions getCalculatorOptions(final Connection con, final boolean calculatorOptionsUI) throws SQLException {
    return calculatorOptionsUI ? CalculatorOptionsFactory.initUICalculatorOptions(con) : CalculatorOptionsFactory.initWorkerCalculationOptions(con);
  }

  protected CalculatorThemeFactory getCalculatorThemeFactory(final Theme theme) {
    switch (theme) {
    case NCA:
      return ncaCalculatorThemeFactory;
    case RBL:
      return rblCalculatorThemeFactory;
    case WNB:
    default:
      return wnbCalculatorThemeFactory;
    }
  }
}
