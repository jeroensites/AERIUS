/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.calculation.base.IntermediateResultHandler;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Processes all added {@link IntermediateResultHandler}s in the {@link #run()} method. The {@link #run()} method runs until the status running is
 * false. This status is set when {@link #onFinish(CalculationState)} is calculated. In that case it keeps running until all handlers report they
 * have send all results.
 */
class IntermediateHandlerProcessor implements Runnable {
  private static final Logger LOG = LoggerFactory.getLogger(IntermediateHandlerProcessor.class);

  private final List<IntermediateResultHandler> intermediateHandlers = new ArrayList<>();
  private final WorkerIntermediateResultSender sender;

  private Exception exception;

  public IntermediateHandlerProcessor(final WorkerIntermediateResultSender sender) {
    this.sender = sender;
  }

  public void add(final IntermediateResultHandler handler) {
    intermediateHandlers.add(handler);
  }

  public List<IntermediateResultHandler> getIntermediateHandlers() {
    return intermediateHandlers;
  }

  @Override
  public void run() {
    try {
      sendResultsOnHandlers();
    } catch (final IOException | AeriusException e) {
      LOG.error("IntermediateHandlerProcessor exception", e);
      exception = e;
    }
  }

  private boolean sendResultsOnHandlers() throws IOException, AeriusException {
    boolean hasSendResults = false;
    for (final IntermediateResultHandler handler : intermediateHandlers) {
      Serializable value;
      value = handler.get();
      if (value != null) {
        if (value instanceof Boolean) {
          hasSendResults = hasSendResults || Boolean.TRUE.equals(value);
        } else {
          sender.sendIntermediateResult(value);
          hasSendResults = true;
        }
      }
    }
    return hasSendResults;
  }

  public void onFinish(final CalculationState state) throws Exception {
    if (state == CalculationState.COMPLETED) {
      if (exception != null) {
        throw exception;
      }
      while (sendResultsOnHandlers()) {}
    }
  }
}
