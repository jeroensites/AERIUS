/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.base.ResultPostProcessHandler;
import nl.overheid.aerius.calculation.base.WorkMonitor;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Implementation of {@link ResultHandler}.
 */
class ResultHandlerImpl implements ResultHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(ResultHandlerImpl.class);

  private final WorkMonitor monitor;
  private final CalculationResultQueue queue;
  private final CalculationResultHandler handler;
  private final List<ResultPostProcessHandler> resultUpdateHandlers = new ArrayList<>();

  public ResultHandlerImpl(final WorkMonitor monitor, final CalculationResultQueue queue, final CalculationResultHandler handler) {
    this.monitor = monitor;
    this.queue = queue;
    this.handler = handler;
  }

  @Override
  public void addResultUpdateHandler(final ResultPostProcessHandler resultUpdateHandler) {
    this.resultUpdateHandlers.add(resultUpdateHandler);
  }

  @Override
  public String registerTask(final WorkKey workKey) throws InterruptedException, TaskCancelledException {
    return monitor.registerTask(workKey);
  }

  @Override
  public void onFailure(final String taskId, final Exception exception) {
    synchronized (this) {
      monitor.taskFailed(taskId, exception);
      queue.clear();
    }
  }

  @Override
  public void onResult(final String taskId, final CalculationEngine calculationEngine, final Map<Integer, List<AeriusResultPoint>> results) {
    if (!monitor.isCancelled()) {
      try {
        final WorkKey workKey = monitor.getWorkKey(taskId);
        final int calculationId = workKey.getCalculationId();

        for (final Entry<Integer, List<AeriusResultPoint>> entry : results.entrySet()) {
          resultUpdateHandlers.forEach(handler -> handler.postProcessSectorResults(calculationId, entry.getValue()));
        }
        handleSectorResults(calculationId, calculationEngine, results);
        // unregister task if all work on the task is done
        monitor.unRegisterTask(taskId);
      } catch (final AeriusException | RuntimeException e) {
        LOGGER.error("[taskid: {}] onResult failed:", taskId, e);
        onFailure(taskId, e);
      }
    }
  }

  private void handleSectorResults(final int calculationId, final CalculationEngine calculationEngine, final Map<Integer, List<AeriusResultPoint>> results)
      throws AeriusException {
    queue.put(calculationEngine, results, calculationId);
    handler.onSectorResults(results, calculationId, calculationEngine);
  }
}
