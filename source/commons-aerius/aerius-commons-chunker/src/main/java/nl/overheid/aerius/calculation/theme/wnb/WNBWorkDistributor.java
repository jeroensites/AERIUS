/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;

import java.sql.SQLException;
import java.util.List;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.conversion.JobPackets.SourceConverterSupplier;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.natura2k.AbstractNatura2KWorkDistributor;
import nl.overheid.aerius.calculation.natura2k.Natura2kReceptorStore;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.wnb.WnbChunkerRepository;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * {@link WorkDistributor} implementation for the Wet Natuur Bescherming (Wnb) calculation.
 * Given the sources it collects the receptors in Natura2000 areas needed to be calculated.
 * This process runs until based on the results it is determined all relevant receptors have been calculated.
 */
class WNBWorkDistributor extends AbstractNatura2KWorkDistributor {

  private final PMF pmf;
  private final WnbChunkerRepository rr;

  private ChunkState chunkState;

  public WNBWorkDistributor(final PMF pmf, final Natura2kReceptorStore n2kReceptorStore, final SectorCategories sectorCategories)
      throws SQLException {
    super(pmf, n2kReceptorStore, sectorCategories);
    this.pmf = pmf;
    rr = getNBWetRepository();
  }

  @Override
  public void init(final CalculationJob calculationJob) throws SQLException, AeriusException {
    final List<Entry<Integer, Integer>> listn2k = n2kReceptorStore.order(calculationJob.getTree());
    chunkState = new ChunkState(pmf, calculationJob, listn2k, rr);
    assessmentAreaCompleteHandlers.forEach(chunkState::addAreaCompleteHandler);
    initializeChunkStateWithDistances(listn2k, chunkState);
    init(calculationJob, listn2k);
  }

  @Override
  protected SourceConverterSupplier createSourceConverterSupplier(final CalculationJob calculationJob) {
    return (con, situation) -> new WNBSourceConverter(con, calculationJob.getProvider(), situation);
  }

  private void initializeChunkStateWithDistances(final List<Entry<Integer, Integer>> listn2k, final ChunkState chunkState) {
    for (final Entry<Integer, Integer> entry : listn2k) {
      final Integer n2kId = entry.getKey();
      chunkState.addN2KArea(n2kId, n2kReceptorStore.getReceptors(n2kId).size(), entry.getValue());
    }
  }

  @Override
  public boolean hasWork(final CalculatorOptions calculationOptions) throws AeriusException {
    return super.hasWork(calculationOptions) && chunkState.resultsPending();
  }

  /**
   * Returns the wrapper around the repository objects. Override this method in tests to stub the database.
   *
   * @return wrapper object
   */
  protected WnbChunkerRepository getNBWetRepository() {
    return new WnbChunkerRepository();
  }
}
