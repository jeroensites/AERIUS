/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.aggregation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import nl.overheid.aerius.calculation.grid.GridZoomLevel;

class GridLevelStore<E> implements LevelStore<GridZoomLevel, E> {
  /**
   * Represents the objects per cell on all levels on a given grid.
   *
   * @param <E> The object to contain in each cell.
   */
  public final class GridMap extends AutoFillingHashMap<GridZoomLevel, ZoomLevelMap> {
    private static final long serialVersionUID = 2561782710057038872L;

    @Override
    protected ZoomLevelMap getEmptyObject() {
      return new ZoomLevelMap();
    }
  }

  /**
   * Represents the objects per cell on single level on a given grid.
   *
   * @param <E> The object to contain in each cell.
   */
  public final class ZoomLevelMap extends AutoFillingHashMap<Integer, Collection<E>> {
    private static final long serialVersionUID = 2561782710057038872L;

    @Override
    protected Collection<E> getEmptyObject() {
      return new ArrayList<>();
    }
  }

  private final GridMap gridMap = new GridMap();

  @Override
  public void add(final GridZoomLevel level, final int cell, final E elem) {
    gridMap.get(level).get(cell).add(elem);
  }

  @Override
  public void addAll(final GridZoomLevel level, final int cell, final Collection<E> col) {
    gridMap.get(level).get(cell).addAll(col);
  }

  @Override
  public Map<Integer, Collection<E>> getLevel(final GridZoomLevel level) {
    return gridMap.get(level);
  }
}
