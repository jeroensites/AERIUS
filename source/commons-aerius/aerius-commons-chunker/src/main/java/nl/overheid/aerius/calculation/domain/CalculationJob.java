/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.io.File;
import java.time.Instant;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Data object with a state and data information of a single calculation.
 */
public class CalculationJob {

  private final JobIdentifier jobIdentifier;
  private final String name;
  private final ScenarioCalculations scenarioCalculations;
  private final String queueName;

  private CalculationState state = CalculationState.UNDEFINED;
  private CalculationEngineProvider provider;
  private CalculatorOptions calculatorOptions;
  private EmissionSourceListSTRTree tree;
  private UnitsPerSectorPerEngineAccumulator unitsAccumulator;
  // Location where output results are temporary stored to be send to the user
  private File outputDirectory;
  // Optional ADMS license
  private byte[] admsLicense;
  // Initialize start time when object created.
  private final Instant startTime = Instant.now();

  public CalculationJob(final JobIdentifier jobIdentifier, final String name, final ScenarioCalculations scenarioCalculations,
      final String queueName) {
    this.jobIdentifier = jobIdentifier;
    this.name = name;
    this.scenarioCalculations = scenarioCalculations;
    this.queueName = queueName;
  }

  public Instant getStartTime() {
    return startTime;
  }

  public String getName() {
    return name;
  }

  public ScenarioCalculations getScenarioCalculations() {
    return scenarioCalculations;
  }

  public List<Calculation> getCalculations() {
    return getScenarioCalculations().getCalculations();
  }

  public String getQueueName() {
    return queueName;
  }

  public JobIdentifier getJobIdentifier() {
    return jobIdentifier;
  }

  public CalculationState getState() {
    return state;
  }

  public void setState(final CalculationState state) {
    this.state = state;
  }

  public CalculationEngineProvider getProvider() {
    return provider;
  }

  public void setProvider(final CalculationEngineProvider provider) {
    this.provider = provider;
  }

  public CalculationSetOptions getCalculationSetOptions() {
    return getScenarioCalculations().getOptions();
  }

  public CalculatorOptions getCalculatorOptions() {
    return calculatorOptions;
  }

  public void setCalculatorOptions(final CalculatorOptions calculatorOptions) {
    this.calculatorOptions = calculatorOptions;
  }

  public List<Substance> getSubstances() {
    return getCalculationSetOptions().getSubstances();
  }

  public File getOutputDirectory() {
    return outputDirectory;
  }

  public void setOutputDirectory(final File outputDirectory) {
    this.outputDirectory = outputDirectory;
  }

  /**
   * Returns the tree of all sources. The tree is lazily created so it's only created when the tree is required.
   * Sources from {@link SituationType#NETTING} are not included as they are not used to determine calculation points, but only to net the results of
   * the proposed situations.
   *
   * @return tree of all sources.
   * @throws AeriusException
   */
  public synchronized EmissionSourceListSTRTree getTree() throws AeriusException {
    if (tree == null) {
      // If only Netting than we need the sources of Netting, otherwise netting will be filtered out in determining the calculation points.
      final List<ScenarioSituation> situations = scenarioCalculations.getScenario().getSituations();
      final boolean onlyNetting = situations.stream().allMatch(s -> s.getType() == SituationType.NETTING);
      final Function<ScenarioSituation, Boolean> filter = onlyNetting ? (s -> true) : s -> (s.getType() != SituationType.NETTING);
      final List<List<EmissionSourceFeature>> emissionSources = situations.stream()
          .filter(filter::apply).map(ScenarioSituation::getEmissionSourcesList).collect(Collectors.toList());

      tree = EmissionSourceListSTRTree.buildTree(emissionSources);
    }
    return tree;
  }

  public UnitsPerSectorPerEngineAccumulator getUnitsAccumulator() {
    return unitsAccumulator;
  }

  /**
   * @return true if has an unitsAccumulator
   */
  public boolean isUnitsAccumulator() {
    return unitsAccumulator != null;
  }

  /**
   * Call to enable units accumulator.
   */
  public void setUnitsAccumulator() {
    unitsAccumulator = new UnitsPerSectorPerEngineAccumulator();
  }

  /**
   * @return Optional License for CERC ADMS tool. Will be passed to ADMS worker.
   */
  public byte[] getAdmsLicense() {
    return admsLicense;
  }

  public void setAdmsLicense(final byte[] admsLicense) {
    this.admsLicense = admsLicense;
  }
}
