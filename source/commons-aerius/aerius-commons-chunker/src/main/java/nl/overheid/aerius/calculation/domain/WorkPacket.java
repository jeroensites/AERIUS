/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * A complete package of work to be send to the workers.
 * @param <P>
 */
public class WorkPacket<P extends AeriusPoint> {

  /**
   * Key to uniquely identify a list of emission/engine sources.
   * The integer key in can be the sector code of the emission source.
   */
  public static class GroupedSourcesKey {
    private final Integer id;
    private final CalculationEngine calculationEngine;

    public GroupedSourcesKey(final Integer id, final CalculationEngine calculationEngine) {
      this.id = id;
      this.calculationEngine = calculationEngine;
    }

    public Integer getId() {
      return id;
    }

    public CalculationEngine getCalculationEngine() {
      return calculationEngine;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((calculationEngine == null) ? 0 : calculationEngine.hashCode());
      result = prime * result + ((id == null) ? 0 : id.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null || getClass() != obj.getClass()) {
        return false;
      }
      final GroupedSourcesKey other = (GroupedSourcesKey) obj;
      if (calculationEngine != other.calculationEngine
          || (id == null && other.id != null)
          || (id != null && !id.equals(other.id))) {
        return false;
      }
      return true;
    }

    @Override
    public String toString() {
      return "GroupedSourcesKey [" + id + ", " + calculationEngine + "]";
    }
  }

  /**
   * Map to store collections of sources by the unique group key.
   */
  public static class GroupedSourcesPacketMap {

    private final Map<GroupedSourcesKey, Collection<EngineSource>> map = new HashMap<>();

    /**
     * Adds all sources grouped by the group key and calculation engine.
     *
     * @param groupKey key to identify this group of sources
     * @param engine calculation engine
     * @param sources sources to add
     */
    public boolean addAll(final Integer groupKey, final CalculationEngine engine, final Collection<EngineSource> sources) {
      final GroupedSourcesKey key = new GroupedSourcesKey(groupKey, engine);

      return map.computeIfAbsent(key, k -> new ArrayList<>()).addAll(sources);
    }

    public Map<GroupedSourcesKey, Collection<EngineSource>> getMap() {
      return map;
    }

    public List<GroupedSourcesPacket> toGroupedSourcesPacket() {
      return map.entrySet().stream().map(e -> new GroupedSourcesPacket(e.getKey(), e.getValue())).collect(Collectors.toList());
    }
  }

  /**
   * A packet of sources and the key identifying the sources.
   */
  public static class GroupedSourcesPacket {
    private final GroupedSourcesKey key;
    private final Collection<EngineSource> sources;

    public GroupedSourcesPacket(final Integer key, final CalculationEngine calculationEngine, final Collection<EngineSource> sources) {
      this(new GroupedSourcesKey(key, calculationEngine), sources);
    }

    public GroupedSourcesPacket(final GroupedSourcesKey key, final Collection<EngineSource> sources) {
      this.key = key;
      this.sources = sources;
    }

    public GroupedSourcesKey getKey() {
      return key;
    }

    public Collection<EngineSource> getSources() {
      return sources;
    }
  }

  /**
   * A packet of sources. Multiple situations are put into this packet.
   */
  public static class JobPacket {
    private final Integer calculationId;
    private final int year;
    private final List<GroupedSourcesPacket> sourcesPackets;

    public JobPacket(final Integer calculationId, final int year, final List<GroupedSourcesPacket> sourcesPackets) {
      this.calculationId = calculationId;
      this.year = year;
      this.sourcesPackets = sourcesPackets;
    }

    public Integer getCalculationId() {
      return calculationId;
    }

    public int getYear() {
      return year;
    }

    public List<GroupedSourcesPacket> getSourcesPackets() {
      return sourcesPackets;
    }
  }

  private final Collection<P> points;
  private final List<JobPacket> jobPackets;

  /**
   * Constructor.
   *
   * @param points calculation points (receptors) to calculate.
   * @param jobPackets packet of sources to calculate.
   */
  public WorkPacket(final Collection<P> points, final List<JobPacket> jobPackets) {
    this.points = points;
    this.jobPackets = jobPackets;
  }

  public Collection<P> getPoints() {
    return points;
  }

  public List<JobPacket> getJobPackets() {
    return jobPackets;
  }
}
