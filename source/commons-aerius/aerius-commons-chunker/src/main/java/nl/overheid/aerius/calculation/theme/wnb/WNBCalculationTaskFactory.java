/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;

import java.util.Collection;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.calculation.base.CalculationTaskFactory;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.ops.OPSVersion;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm2.domain.SRMInputData;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Implementation of {@link CalculationTaskFactory} specific for PAS.
 */
public class WNBCalculationTaskFactory implements CalculationTaskFactory {
  private static final Logger LOGGER = LoggerFactory.getLogger(WNBCalculationTaskFactory.class);

  private final int surfaceZoomLevel1;

  public WNBCalculationTaskFactory(final ReceptorGridSettings rgs) {
    surfaceZoomLevel1 = rgs.getZoomLevel1().getSurfaceLevel1();
  }

  @Override
  public <E extends EngineSource, R extends AeriusPoint, T extends EngineInputData<E, R>> CalculationTask<E, R, T> createTask(
      final CalculationEngine calculationEngine, final CalculationJob calculationJob) throws AeriusException {
    final CalculationTask<E, R, T> calculationTask;
    switch (calculationEngine) {
    case ASRM2:
      calculationTask = new CalculationTask<E, R, T>(WorkerType.ASRM, (T) new SRMInputData(Theme.WNB));
      break;
    case OPS:
      calculationTask = new CalculationTask<E, R, T>(WorkerType.OPS, (T) createOPSInputData(calculationJob));
      break;
    default:
      LOGGER.error("Calculation engine {} not supported for WNB theme.", calculationEngine);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    return calculationTask;
  }

  @Override
  public <E extends EngineSource, R extends AeriusPoint, T extends EngineInputData<E, R>> void setTaskOptions(final CalculationTask<E, R, T> task,
      final CalculationJob calculationJob) {
    if (task.getTaskInput() instanceof OPSInputData) {
      final OPSInputData opsInputTask = (OPSInputData) task.getTaskInput();

      opsInputTask.setOpsOptions(calculationJob.getCalculationSetOptions().getOpsOptions());
      for (final Entry<Integer, Collection<OPSSource>> entry : opsInputTask.getEmissionSources().entrySet()) {
        if (calculationJob.getProvider().getCalculationEngine(entry.getKey()) == CalculationEngine.ASRM2) {
          opsInputTask.addMinDistanceGroupId(entry.getKey());
        }
      }
    }
  }

  private OPSInputData createOPSInputData(final CalculationJob calculationJob) {
    final OPSInputData opsInputData = new OPSInputData(OPSVersion.VERSION, surfaceZoomLevel1);

    opsInputData.setMaxDistance(calculationJob.getCalculationSetOptions().isUseWNBMaxDistance());
    return opsInputData;
  }
}
