/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.EngineSource;


/**
 * Stores workload data of a CalculationJob grouped by EmissionSource-Sector and CalculationEngine to be used.
 */
public class UnitsPerSectorPerEngineAccumulator {

  private final Map<Integer, Map<String, Long>> unitsBySectorByEngine = new HashMap<>();

  /**
   * Incorporates the submitted data for a Sector in the Collection.
   * @param sectorId
   * @param sources
   * @param numCalcPoints
   */
  public void add(final int sectorId, final Collection<EngineSource> sources, final int numCalcPoints) {
    if (!sources.isEmpty()) {
      final String engineName = sources.iterator().next() instanceof OPSSource ? CalculationEngine.OPS.name() : CalculationEngine.ASRM2.name();
      final Long unitCount = sources.stream().filter(EngineEmissionSource.class::isInstance)
          .mapToLong(source -> ((EngineEmissionSource) source).getEmissions().size()).sum() * numCalcPoints;
      updateMap(sectorId, engineName, unitCount);
    }
  }

  /**
   * @param sectorId
   * @param engineName
   * @param unitCount
   */
  private void updateMap(final Integer sectorId, final String engineName, final Long unitCount) {
    final Map<String, Long> subMap;
    if (unitsBySectorByEngine.containsKey(sectorId)) {
      subMap = unitsBySectorByEngine.get(sectorId);
    } else {
      subMap = new HashMap<>();
      unitsBySectorByEngine.put(sectorId, subMap);
    }
    if (subMap.containsKey(engineName)) {
      final Long oldCount = subMap.get(engineName);
      subMap.put(engineName, oldCount + unitCount);
    } else {
      subMap.put(engineName, unitCount);
    }
  }

  /**
   * @param sector
   * @param calculationEngine
   * @return the accumulated value if any, 0L otherwise.
   */
  public Long getUnits(final int sectorId, final CalculationEngine calculationEngine) {
    Long result = 0L;
    final Map<String, Long> sectorMap = unitsBySectorByEngine.get(sectorId);
    if (sectorMap != null) {
      final Long potentialResult = sectorMap.get(calculationEngine.name());
      if (potentialResult != null) {
        result = potentialResult;
      }
    }
    return result;
  }
}
