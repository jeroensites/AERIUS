/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.io.Serializable;

import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Handler interface to return intermediate results during calculation.
 */
public interface IntermediateResultHandler {
  /**
   * This methods gets the intermediate results. By contract when this method is called it should return new or updated results if any
   * or should return null if no new or updated results are available.
   *
   * If the method returns a boolean it means it should not send the data. If the boolean is true it means there is more to do,
   * else it will interpret the same as if it returned null. That is it will stop.
   * @return intermediate results or null if no new intermediate results are available.
   * @throws AeriusException
   */
  Serializable get() throws AeriusException;
}
