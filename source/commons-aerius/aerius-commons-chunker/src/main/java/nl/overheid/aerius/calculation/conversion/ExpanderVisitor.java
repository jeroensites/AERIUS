/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.building.BuildingTracker;
import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.domain.WorkPacket.GroupedSourcesPacketMap;
import nl.overheid.aerius.function.AeriusConsumer;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.geojson.IsFeature;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceVisitor;
import nl.overheid.aerius.shared.domain.v2.source.FarmLodgingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.FarmlandEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.InlandShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MooringInlandShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MooringMaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM1RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.offroad.CustomOffRoadMobileSource;
import nl.overheid.aerius.shared.domain.v2.source.offroad.OffRoadMobileSource;
import nl.overheid.aerius.shared.domain.v2.source.offroad.StandardOffRoadMobileSource;
import nl.overheid.aerius.shared.domain.v2.source.plan.Plan;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Expands {@link EmissionSource} objects into point objects using the visitor pattern.
 *
 * <p>This visitor uses a {@link SourceCollector} to pass the converted results and therefore only traverses the sources lists.
 * The collecting via the interface design pattern is used because sources can be handled differently for different themes.
 */
class ExpanderVisitor<T extends BuildingTracker> implements EmissionSourceVisitor<Void> {

  private final Connection con;
  private final List<Substance> substances;
  private final GroupedSourcesPacketMap expandedSources = new GroupedSourcesPacketMap();
  private final SourceConverter<T> sourceConverter;
  private final InlandShipExpander shipSourceExpander;
  private final MaritimeShipExpander maritimeShipExpander;
  private final SourceConversionHelper sourceConversionHelper;
  private final T buildingTracker;

  /**
   * Constructor.
   *
   * @param con Connection to use for queries.
   * @param substances emissions substances to convert
   * @param sourceConversionHelper
   * @param sources The emission sources to convert to engine sources.
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException
   */
  public ExpanderVisitor(final Connection con, final SourceConverter<T> sourceConverter, final List<Substance> substances,
      final SourceConversionHelper sourceConversionHelper, final List<BuildingFeature> buildings) throws SQLException, AeriusException {
    this.con = con;
    this.sourceConverter = sourceConverter;
    this.substances = substances;
    this.sourceConversionHelper = sourceConversionHelper;
    this.buildingTracker = sourceConverter.createBuildingTracker(buildings);
    shipSourceExpander = new InlandShipExpander(con, this.sourceConverter, sourceConversionHelper);
    maritimeShipExpander = new MaritimeShipExpander(con, this.sourceConverter, sourceConversionHelper);
  }

  public GroupedSourcesPacketMap getExpandedSources() {
    return expandedSources;
  }

  @Override
  public Void visit(final FarmLodgingEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances, buildingTracker);
    return null;
  }

  @Override
  public Void visit(final FarmlandEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances, buildingTracker);
    return null;
  }

  @Override
  public Void visit(final GenericEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances, buildingTracker);
    return null;
  }

  @Override
  public Void visit(final MooringInlandShippingEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    return wrap(emissionSource, expanded -> shipSourceExpander.inlandMooringToPoints(expanded, emissionSource, feature.getGeometry(), substances));
  }

  @Override
  public Void visit(final InlandShippingEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    return
        wrap(emissionSource, expanded -> shipSourceExpander.inlandRouteToPoints(expanded, emissionSource, feature.getGeometry(), substances));
  }

  @Override
  public Void visit(final MooringMaritimeShippingEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    return wrap(emissionSource,
        expanded -> maritimeShipExpander.maritimeMooringShipsToPoints(expanded, emissionSource, feature.getGeometry(), substances));
  }

  @Override
  public Void visit(final MaritimeShippingEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    if (feature.getGeometry().type() == GeometryType.LINESTRING) {
      wrap(emissionSource,
          expanded -> maritimeShipExpander.maritimeRouteToPoints(expanded, emissionSource, feature.getGeometry(), substances));
    } else {
      sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances, buildingTracker);
    }
    return null;
  }

  private Void wrap(final EmissionSource emissionSource, final AeriusConsumer<List<EngineSource>> consumer) throws AeriusException {
    final List<EngineSource> expanded = new ArrayList<>();

    consumer.apply(expanded);
    expandedSources.addAll(emissionSource.getSectorId(), CalculationEngine.OPS, expanded);
    return null;
  }

  /**
   * Converts a {@link OffRoadMobileEmissionValues} object to points object.
   * The default and custom categories are split, where custom categories will result in a
   * emission source each, due to the differences in OPS characteristics.
   *
   * @param emissionSource EmissionSource to convert
   * @throws AeriusException throws AeriusException in case of other problems
   */
  @Override
  public Void visit(final OffRoadMobileEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    //split the source: customs get their own source (own characteristics).
    final List<StandardOffRoadMobileSource> defaultVehicles = new ArrayList<>();
    final List<CustomOffRoadMobileSource> customVehicles = new ArrayList<>();
    for (final OffRoadMobileSource vehicle : emissionSource.getSubSources()) {
      if (vehicle instanceof CustomOffRoadMobileSource) {
        customVehicles.add((CustomOffRoadMobileSource) vehicle);
      } else if (vehicle instanceof StandardOffRoadMobileSource) {
        defaultVehicles.add((StandardOffRoadMobileSource) vehicle);
      }
    }
    final OPSSourceCharacteristics sectorCharacteristics = sourceConversionHelper.getCharacteristicsSupplier()
        .getSectorCharacteristics(emissionSource.getSectorId());
    //handle the 'custom' vehicles
    for (final CustomOffRoadMobileSource vehicle : customVehicles) {
      final OffRoadMobileEmissionSource customVehicleSource = new OffRoadMobileEmissionSource();
      customVehicleSource.getSubSources().add(vehicle);
      customVehicleSource.setEmissions(sourceConversionHelper.getSubSourceEmissionsCalculator().calculateOffRoadEmissions(customVehicleSource));
      final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
      sectorCharacteristics.copyTo(characteristics);
      if (vehicle.getCharacteristics() instanceof OPSSourceCharacteristics) {
        final OPSSourceCharacteristics vehicleCharacteristics = (OPSSourceCharacteristics) vehicle.getCharacteristics();
        characteristics.setEmissionHeight(vehicleCharacteristics.getEmissionHeight());
        characteristics.setSpread(vehicleCharacteristics.getSpread());
        characteristics.setHeatContent(vehicleCharacteristics.getHeatContent());
      }
      customVehicleSource.setSectorId(emissionSource.getSectorId());
      customVehicleSource.setCharacteristics(characteristics);

      //add to the expanded list. Source can be all geometries.
      sourceConverter.convert(con, expandedSources, customVehicleSource, feature.getGeometry(), substances, buildingTracker);
    }
    //handle the 'default' vehicles.
    final OffRoadMobileEmissionSource defaultVehiclesSource = new OffRoadMobileEmissionSource();
    defaultVehiclesSource.setSectorId(emissionSource.getSectorId());
    defaultVehiclesSource.setCharacteristics(sectorCharacteristics);
    defaultVehiclesSource.getSubSources().addAll(defaultVehicles);
    defaultVehiclesSource.setEmissions(sourceConversionHelper.getSubSourceEmissionsCalculator().calculateOffRoadEmissions(defaultVehiclesSource));
    //add to the expanded list. Source can be all geometries.
    sourceConverter.convert(con, expandedSources, defaultVehiclesSource, feature.getGeometry(), substances, buildingTracker);
    return null;
  }

  /**
   * Converts a {@link PlanEmissionValues} object to points object.
   * The categories are split, where each categories will result in a
   * emission source each, due to the differences in OPS characteristics.
   *
   * @param emissionSource EmissionSource to convert
   * @throws AeriusException throws AeriusException in case of other problems
   */
  @Override
  public Void visit(final PlanEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    //split the source: each plan type get their own source (own characteristics based on sector).
    for (final Plan plan : emissionSource.getSubSources()) {
      final GenericEmissionSource planEmissionSource = new GenericEmissionSource();
      final Map<Substance, Double> emissions = sourceConversionHelper.getSubSourceEmissionsCalculator().calculatePlanEmissions(plan);
      planEmissionSource.setSectorId(emissionSource.getSectorId());
      planEmissionSource.setEmissions(emissions);
      planEmissionSource.setCharacteristics(sourceConversionHelper.getCharacteristicsSupplier().getPlanCharacteristics(plan));
      //add to the expanded list. Source can be all geometries.
      sourceConverter.convert(con, expandedSources, planEmissionSource, feature.getGeometry(), substances, buildingTracker);
    }
    return null;
  }

  @Override
  public Void visit(final SRM1RoadEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    emissionSource.setCharacteristics(sourceConversionHelper.getCharacteristicsSupplier().getSectorCharacteristics(emissionSource.getSectorId()));
    sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances, buildingTracker);
    return null;
  }

  @Override
  public Void visit(final SRM2RoadEmissionSource emissionSource, final IsFeature feature) throws AeriusException {
    emissionSource.setCharacteristics(sourceConversionHelper.getCharacteristicsSupplier().getSectorCharacteristics(emissionSource.getSectorId()));
    sourceConverter.convert(con, expandedSources, emissionSource, feature.getGeometry(), substances, buildingTracker);
    return null;
  }

}
