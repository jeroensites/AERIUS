/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.rbl;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.exception.AeriusException;

class MonitorSRM2NetworksStoreTest {

  @Test
  void testYearAvailable() throws AeriusException {
    final MonitorSRM2NetworksStore store = new MonitorSRM2NetworksStore();
    store.put(2020, Collections.emptyList());

    assertTrue(store.get(2020, Collections.emptyList()).isEmpty(), "Stored network should be the same");
  }

  @Test
  void testYearUnavailable() {
    assertThrows(AeriusException.class, () -> {
      final MonitorSRM2NetworksStore store = new MonitorSRM2NetworksStore();
      store.put(2020, Collections.emptyList());
      store.get(2022, Collections.emptyList());
    });
  }

}
