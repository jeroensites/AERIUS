/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;

/**
 * Test class for {@link GroupResultPoint}.
 */
class SectorsResultPointTest {

  @Test
  void testGetTotalsResultPoint() {
    final AeriusPoint ap = new AeriusPoint(1, AeriusPointType.POINT, 100, 200);
    final GroupResultPoint srp = new GroupResultPoint(ap);
    final EmissionResults er1 = new EmissionResults();
    er1.put(EmissionResultKey.NH3_DEPOSITION, 200.0);
    srp.put(CalculationEngine.OPS, 1, er1);
    final EmissionResults er2 = new EmissionResults();
    er2.put(EmissionResultKey.NH3_DEPOSITION, 300.0);
    srp.put(CalculationEngine.OPS, 2, er2);
    final EmissionResults er3 = new EmissionResults();
    er2.put(EmissionResultKey.NH3_CONCENTRATION, 300.0);
    srp.put(CalculationEngine.OPS, 3, er3);
    assertEquals(500, srp.getTotalsResultPoint().getEmissionResult(EmissionResultKey.NH3_DEPOSITION), 0.0001, "Total should be sum");
  }
}
