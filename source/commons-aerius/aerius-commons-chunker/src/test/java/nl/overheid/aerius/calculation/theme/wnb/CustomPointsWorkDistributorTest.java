/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.CalculationTestBase;
import nl.overheid.aerius.calculation.base.CalculationJobWorkHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FeaturePointToEnginePointUtil;

/**
 * Test class for {@link CustomPointsWorkDistributor}.
 */
class CustomPointsWorkDistributorTest extends CalculationTestBase {

  @Test
  void testCustomPointsDistributor() throws SQLException, IOException, InterruptedException, AeriusException, TaskCancelledException {
    final CalculationJob calculationJob = getExampleCalculationJob();
    calculationJob.setProvider(defaultCalculationEngineProvider);
    final List<AeriusPoint> points = FeaturePointToEnginePointUtil.convert(calculationJob.getScenarioCalculations().getCalculationPoints());
    final AtomicInteger counter = new AtomicInteger();
    final CustomPointsWorkDistributor distributor =
        new CustomPointsWorkDistributor(getCalcPMF(), new GridUtil(new GridSettings(RECEPTOR_GRID_SETTINGS)), points);
    final CalculationJobWorkHandler<AeriusPoint> handler = new CalculationJobWorkHandler<>(calculationJob, distributor,
        (calculation, jobPacket, calculationPointStore) -> counter.addAndGet(calculationPointStore.size()),
        new WNBCalculationResultQueue(new WNBRelevantResultsFilter()));

    handler.work();
    assertEquals(points.size(), counter.get(), "Number of points in should be same as passed to handler");
  }
}
