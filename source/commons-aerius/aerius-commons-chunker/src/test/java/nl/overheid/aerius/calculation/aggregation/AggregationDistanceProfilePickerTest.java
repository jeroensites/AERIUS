/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.aggregation;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link AggregationDistanceProfilePicker}.
 */
class AggregationDistanceProfilePickerTest {

  private static final int TEST_ALWAYS_AGGREGATED_SECTOR = 1700;
  private static final int TEST_DEFAULT_AGGREGATION_DISABLED_SECTOR = 4110;

  private final AggregationDistanceProfilePicker picker = new AggregationDistanceProfilePicker();

  @Test
  void testAlwaysAggregatedSector() {
    assertProfile(TEST_ALWAYS_AGGREGATED_SECTOR, false, AggregationDistanceProfile.INDUSTRY);
  }

  @Test
  void testAlwaysAggregatedSectorForceAggregation() {
    assertProfile(TEST_ALWAYS_AGGREGATED_SECTOR, true, AggregationDistanceProfile.INDUSTRY);
  }

  @Test
  void testDefaultAggregationDisabledSector() {
    assertProfile(TEST_DEFAULT_AGGREGATION_DISABLED_SECTOR, false, AggregationDistanceProfile.DISABLED);
  }

  @Test
  void testDefaultAggregationDisabledSectorForceAggregation() {
    assertProfile(TEST_DEFAULT_AGGREGATION_DISABLED_SECTOR, true, AggregationDistanceProfile.STABLES);
  }

  private void assertProfile(final int sectorId, final boolean forceAggregation, final AggregationDistanceProfile expectedProfile) {
    assertSame(expectedProfile, picker.getAggregationProfile(sectorId, forceAggregation), "Unexpected profile encountered");
  }

}
