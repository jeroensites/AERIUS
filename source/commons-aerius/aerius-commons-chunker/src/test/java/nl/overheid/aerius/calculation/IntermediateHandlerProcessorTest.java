/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.Serializable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;

/**
 * Test class for {@link IntermediateHandlerProcessor}.
 */
class IntermediateHandlerProcessorTest {

  private WorkerIntermediateResultSender sender;
  private Serializable callbackResult;
  private IntermediateHandlerProcessor ihp;
  private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
  private ScheduledFuture<?> future;

  @BeforeEach
  void before() {
    sender = (result) -> callbackResult = result;
    ihp = new IntermediateHandlerProcessor(sender);
  }

  @AfterEach
  void after() throws InterruptedException {
    if (future != null && !future.isCancelled()) {
      future.cancel(true);
    }
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testSendResults() throws Exception {
    final String resultString = "AERIUS";
    final Semaphore semaphore = new Semaphore(0);
    final AtomicBoolean send = new AtomicBoolean(false);
    ihp.add(() -> {
      if (send.get()) { // in second call release semaphore
        semaphore.release();
        return null;
      } else {
        send.set(true); // in first call mark as result returned.
        return resultString;
      }
    });
    future = executor.scheduleWithFixedDelay(ihp, 0, 10, TimeUnit.MILLISECONDS);
    // wait for release. This happens in second call to get, first get sets callback.
    // this is in second call so we know for sure results has been processed in first call.
    semaphore.acquire();
    ihp.onFinish(CalculationState.CANCELLED);
    future.cancel(false);
    assertEquals(resultString, callbackResult, "Intermediate result should have been send");
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testSendResultsOnFinish() throws Exception {
    final String resultString1 = "AERIUS_1";
    final String resultString2 = "AERIUS_2";
    final AtomicInteger send = new AtomicInteger(0);
    final Semaphore semaphore = new Semaphore(0);
    ihp.add(() -> {
      try {
        if (send.get() == 1) { // when running finished wait for #onFinish to be processed
          semaphore.release();
          return resultString2;
        } else if (send.get() == 2) { // this means it was run after running completed.
          return null;
        } else {
          return resultString1;
        }} finally {
          send.incrementAndGet();
        }
    });
    future = executor.scheduleWithFixedDelay(ihp, 0, 10, TimeUnit.MILLISECONDS);
    ihp.onFinish(CalculationState.COMPLETED);
    semaphore.acquire();
    future.cancel(false);
    assertEquals(resultString2, callbackResult, "Intermediate result should have been send in onFinish");
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testFinish() throws Exception {
    future = executor.scheduleWithFixedDelay(ihp, 0, 10, TimeUnit.MILLISECONDS);
    assertFalse(future.isCancelled(), "Futre should be alive");
    ihp.onFinish(CalculationState.COMPLETED);
    future.cancel(false);
    assertTrue(future.isCancelled(), "Futre should be dead");
  }
}
