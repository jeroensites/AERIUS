/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.theme.wnb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * The class <code>CalculationResultQueueTest</code> contains tests for the
 * class {@link WNBCalculationResultQueue}.
 */
class WNBCalculationResultQueueTest {

  private static final int POINT_ID_1 = 4361388;
  private static final int POINT_X_1 = 130000;
  private static final int POINT_Y_1 = 450000;
  private static final int POINT_ID_2 = 90998;
  private static final int POINT_X_2 = 150000;
  private static final int POINT_Y_2 = 300000;
  private static final int CALCULATION_ID_1 = 1;
  private static final int CALCULATION_ID_2 = 2;
  private static final int SECTOR_ID_1 = 30;
  private static final int SECTOR_ID_2 = 40;

  private WNBCalculationResultQueue queue;

  @BeforeEach
  void before() {
    queue = new WNBCalculationResultQueue(new WNBRelevantResultsFilter());
  }

  /**
   * Run the boolean hasResults(int) method test
   */
  @Test
  void testHasResultsEmpty() {
    assertFalse(queue.hasResults(CALCULATION_ID_1), "No results on empty");
    queue.addCalculation(CALCULATION_ID_1, 0);
    assertFalse(queue.hasResults(CALCULATION_ID_1), "Still no results on empty");
  }

  @Test
  void testPutNotInitialized() {
    assertThrows(NullPointerException.class, () -> {
      queue.put(CalculationEngine.OPS, new HashMap<>(), CALCULATION_ID_1);
      fail("Should throw a NullPointerException.");
    });
  }

  /**
   * Run the void put(CalculationResult, int, int) method test
   */
  @Test
  void testPut() {
    init();
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_1), CALCULATION_ID_1);
    assertFalse(queue.hasResults(CALCULATION_ID_1), "Should not have results with 1 sector for calculation 1");
    assertFalse(queue.hasResults(CALCULATION_ID_2), "Should not have results with 1 sector for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_2), CALCULATION_ID_1);
    assertTrue(queue.hasResults(CALCULATION_ID_1), "Should have results for calculation 1");
    assertFalse(queue.hasResults(CALCULATION_ID_2), "Should not have results for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_1), CALCULATION_ID_2);
    assertTrue(queue.hasResults(CALCULATION_ID_1), "Should have results for calculation 1");
    assertFalse(queue.hasResults(CALCULATION_ID_2), "Should not have results for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_2), CALCULATION_ID_2);
    assertTrue(queue.hasResults(CALCULATION_ID_1), "Should have results for calculation 1");
    assertTrue(queue.hasResults(CALCULATION_ID_2), "Should have results for calculation 2");
  }

  /**
   * Run the void put(CalculationResult, int, int) method test
   */
  @Test
  void testPollCompleteResults() {
    init();
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_1), CALCULATION_ID_1);
    assertTrue(queue.pollTotalResults(CALCULATION_ID_1).isEmpty(), "Should return empty with 1 sector for calculation 1");
    assertTrue(queue.pollTotalResults(CALCULATION_ID_2).isEmpty(), "Should return empty with 1 sector for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_2), CALCULATION_ID_1);
    assertEquals(2, queue.pollTotalResults(CALCULATION_ID_1).size(), "Should return results for calculation 1");
    assertTrue(queue.pollTotalResults(CALCULATION_ID_2).isEmpty(), "Should return empty with no results for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_1), CALCULATION_ID_2);
    assertTrue(queue.pollTotalResults(CALCULATION_ID_1).isEmpty(), "Should return empty for calculation 1");
    assertTrue(queue.pollTotalResults(CALCULATION_ID_2).isEmpty(), "Should return empty with not all results for calculation 2");
    queue.put(CalculationEngine.OPS, createResults(SECTOR_ID_2), CALCULATION_ID_2);
    assertTrue(queue.pollTotalResults(CALCULATION_ID_1).isEmpty(), "Should return empty for calculation 1");
    assertEquals(2, queue.pollTotalResults(CALCULATION_ID_2).size(), "Should return results for calculation 2");
  }

  private WNBCalculationResultQueue init() {
    queue.addCalculation(CALCULATION_ID_1, 2);
    queue.addCalculation(CALCULATION_ID_2, 2);
    return queue;
  }

  private Map<Integer, List<AeriusResultPoint>> createResults(final Integer sectorId) {
    final Map<Integer, List<AeriusResultPoint>> map = new HashMap<>();
    final List<AeriusResultPoint> results = new ArrayList<>();
    results.add(new AeriusResultPoint(POINT_ID_1, AeriusPointType.RECEPTOR, POINT_X_1, POINT_Y_1));
    results.add(new AeriusResultPoint(POINT_ID_2, AeriusPointType.RECEPTOR, POINT_X_2, POINT_Y_2));
    for (final AeriusResultPoint rp : results) {
      rp.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, new Random().nextDouble());
    }
    map.put(sectorId, results);
    return map;
  }
}
