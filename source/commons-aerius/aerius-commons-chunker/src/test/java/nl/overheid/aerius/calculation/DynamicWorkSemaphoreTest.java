/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor;
import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor.RabbitMQWorkerObserver;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Test class for {@link DynamicWorkSemaphore}.
 */
class DynamicWorkSemaphoreTest {

  private static final String QUEUE_NAME = "test";
  private static final String CHUNKER_QUEUE_NAME = WorkerType.CONNECT.type().getWorkerQueueName();

  private RabbitMQWorkerMonitor monitor;

  private DynamicWorkSemaphore semaphore;
  private RabbitMQWorkerObserver observer;

  @BeforeEach
  void before() {
    monitor = Mockito.mock(RabbitMQWorkerMonitor.class);
    semaphore = new DynamicWorkSemaphore(monitor, WorkerType.CONNECT);
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        observer = invocation.getArgument(0, RabbitMQWorkerObserver.class);
        return null;
      }
    }).when(monitor).addObserver(any());
    // Init the mock observer
    semaphore.availablePermits(QUEUE_NAME);
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testAcquireAndRelease() throws InterruptedException {
    assertEquals(0, semaphore.availablePermits(QUEUE_NAME), "The semaphore should be exhausted");
    semaphore.release(QUEUE_NAME);
    assertEquals(1, semaphore.availablePermits(QUEUE_NAME), "There should be 1 slot available");
    semaphore.acquire(QUEUE_NAME);
    assertEquals(0, semaphore.availablePermits(QUEUE_NAME), "The semaphore should be exhausted again");
    assertNotNull(observer, "After release we should get new aquire, and have a observer.");
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testAddingNewSlot() throws InterruptedException {
    assertEquals(0, semaphore.availablePermits(QUEUE_NAME), "The semaphore should be exhausted");
    // This should release a new slot
    updateWorkerQueueSize(1);
    assertEquals(2, semaphore.availablePermits(QUEUE_NAME), "There should be 2 slots available");
    semaphore.acquire(QUEUE_NAME);
    semaphore.acquire(QUEUE_NAME);
    assertEquals(0, semaphore.availablePermits(QUEUE_NAME), "The semaphore should be exhausted again");
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testChangesInWorkersUtilisationSlots() throws InterruptedException {
    updateWorkerQueueSize(2);
    assertEquals(2, semaphore.availablePermits(QUEUE_NAME), "There should be a 2 slots available");
    updateWorkerQueueSize(0);
    assertEquals(0, semaphore.availablePermits(QUEUE_NAME), "The semaphore should be exhausted");
    updateWorkerQueueSize(1);
    assertEquals(2, semaphore.availablePermits(QUEUE_NAME), "There should only be 2 slots available");
  }

  @Test
  @Timeout(value = 1_000, unit = TimeUnit.MILLISECONDS)
  void testChangesInChunkerWorkers() throws InterruptedException {
    updateWorkerQueueSize(10);
    assertEquals(10, semaphore.availablePermits(QUEUE_NAME), "There should be a 10 slots available");
    updateChunkerQueueUtilisation(2);
    assertEquals(5, semaphore.availablePermits(QUEUE_NAME), "There should be a 5 slots available");
    updateChunkerQueueUtilisation(1);
    assertEquals(10, semaphore.availablePermits(QUEUE_NAME), "There should be a 10 slots available");
  }

  private void updateWorkerQueueSize(final int size) {
    observer.updateWorkers(QUEUE_NAME, size, 0);
  }

  private void updateChunkerQueueUtilisation(final int utilisation) {
    observer.updateWorkers( CHUNKER_QUEUE_NAME, 0, utilisation);
  }
}
