/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.DefaultCalculationEngineProvider;
import nl.overheid.aerius.calculation.theme.wnb.WNBSourceConverter;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.emissions.CategoryBasedEmissionFactorSupplier;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.MooringMaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.emissions.EmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.EmissionsUpdater;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.GeometryCalculator;
import nl.overheid.aerius.test.TestDomain;
import nl.overheid.aerius.util.GeometryCalculatorImpl;

/**
 * Unit test for {@link EngineSourceExpander}
 */
class EngineSourceExpanderTest extends BaseDBTest {

  private static final List<Substance> KEY_NH3 = List.of(Substance.NH3);
  private static final List<Substance> KEY_NOX = List.of(Substance.NOX);
  private static final List<Substance> KEY_NOX_NH3 = List.of(Substance.NOX, Substance.NH3);

  private static SourceConverter geometryExpander;
  private static TestDomain testDomain;

  @BeforeAll
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    testDomain = new TestDomain(getCalcPMF());
    try (Connection con = getCalcPMF().getConnection()) {
      geometryExpander = new WNBSourceConverter(getCalcPMF().getConnection(),
          new DefaultCalculationEngineProvider(SectorRepository.getSectorProperties(con)), null);
    }
  }

  @Test
  void testMobileSourceToPoints() throws SQLException, AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setGeometry(new Point(4, 5));
    final OffRoadMobileEmissionSource emissionSource = new OffRoadMobileEmissionSource();
    feature.setProperties(emissionSource);

    testDomain.getOffRoadMobileEmissionSource(emissionSource);

    final List<EmissionSourceFeature> sources = new ArrayList<>();
    sources.add(feature);
    final List<EngineSource> pointSources = expand(sources, KEY_NOX, true);
    assertEquals(2, pointSources.size(), "Number of point sources");
    boolean foundCustom = false;
    for (final EngineSource ps : pointSources) {
      final OPSSource es = (OPSSource) ps;
      if (es.getEmissions().get(Substance.NOX) == 101010.0) {
        assertFalse(foundCustom, "Should only find it once");
        foundCustom = true;
        assertEquals(10, es.getEmissionHeight(), 0.001, "Emission height custom source");
        assertEquals(5, es.getSpread(), 0.001, "Spread custom source");
        assertEquals(20, es.getHeatContent(), 0.001, "Heat content custom source");
      } else {
        assertEquals(0, es.getEmissionHeight(), 0.001, "Emission height non-custom source");
        assertEquals(0, es.getSpread(), 0.001, "Spread custom non-source");
        assertEquals(0, es.getHeatContent(), 0.001, "Heat content non-custom source");
      }
    }
    assertTrue(foundCustom, "Should have found the custom source");
  }

  @Test
  void testMobileSourceOPSDefaults() throws SQLException, AeriusException {
    final Sector sector = testDomain.getCategories().getSectorById(3220);
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setGeometry(new Point(4, 5));
    final OffRoadMobileEmissionSource emissionSource = new OffRoadMobileEmissionSource();
    feature.setProperties(emissionSource);
    emissionSource.setSectorId(sector.getSectorId());
    emissionSource.setCharacteristics(sector.getDefaultCharacteristics());

    testDomain.getOffRoadMobileEmissionSource(emissionSource);

    final List<EmissionSourceFeature> sourceList = new ArrayList<>();
    sourceList.add(feature);
    final List<EngineSource> pointSources = expand(sourceList, KEY_NOX, true);
    assertEquals(2, pointSources.size(), "Number of point sources");
    boolean foundCustom = false;
    for (final EngineSource ps : pointSources) {
      final OPSSource es = (OPSSource) ps;
      if (es.getEmissions().get(Substance.NOX) == 101010.0) {
        assertFalse(foundCustom, "Should only find it once");
        foundCustom = true;
        assertEquals(10, es.getEmissionHeight(), 0.001, "Emission height custom source");
        assertEquals(5, es.getSpread(), 0.001, "Spread custom source");
        assertEquals(20, es.getHeatContent(), 0.001, "Heat content custom source");
        assertEquals(sector.getDefaultCharacteristics().getParticleSizeDistribution(), es.getParticleSizeDistribution(), 0.001,
            "Particle size distribution");
      } else {
        assertEquals(sector.getDefaultCharacteristics().getEmissionHeight(), es.getEmissionHeight(), 0.001, "Emission height non-custom source");
        assertEquals(sector.getDefaultCharacteristics().getSpread(), es.getSpread(), 0.001, "Spread custom non-source");
        assertEquals(sector.getDefaultCharacteristics().getHeatContent(), es.getHeatContent(), 0.001, "Heat content non-custom source");
        assertEquals(sector.getDefaultCharacteristics().getParticleSizeDistribution(), es.getParticleSizeDistribution(), 0.001,
            "Particle size distribution");
      }
    }
    assertTrue(foundCustom, "Should have found the custom source");
  }

  @Test
  void testStackingOfOffRoadMobileSource() throws SQLException, AeriusException {
    assertEquals(2, createAndExpandOffRoadMobileSources(true).size(), "Number of point sources");
  }

  @Test
  void testNonStackingOffRoadMobileSource() throws SQLException, AeriusException {
    assertEquals(4, createAndExpandOffRoadMobileSources(false).size(), "Number of point sources");
  }

  @Test
  @Disabled("Currently no plan categories in the database, hence disabled.")
  void testPlanToPoints() throws SQLException, AeriusException {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setGeometry(new Point(4, 5));
    final PlanEmissionSource emissionSource = testDomain.getPlanEmissionSource();
    feature.setProperties(emissionSource);
    assertEquals(2, emissionSource.getSubSources().size(), "Should be 2 plan emission values");

    final List<EmissionSourceFeature> sources = new ArrayList<>();
    sources.add(feature);
    final List<EngineSource> pointSources = expand(sources, KEY_NOX_NH3, true);
    updateEmissions(sources);
    assertEquals(1, pointSources.size(), "Number of point sources");
    double emissionNH3 = 0.0;
    double emissionNOx = 0.0;
    for (final EngineSource es : pointSources) {
      emissionNH3 += ((EngineEmissionSource) es).getEmissions().getOrDefault(Substance.NH3, 0.0);
      emissionNOx += ((EngineEmissionSource) es).getEmissions().getOrDefault(Substance.NOX, 0.0);
    }
    assertEquals(emissionSource.getEmissions().getOrDefault(Substance.NH3, 0.0), emissionNH3, 0.001, "Emission NH3");
    assertEquals(emissionSource.getEmissions().getOrDefault(Substance.NOX, 0.0), emissionNOx, 0.001, "Emission NOx");
  }

  @Test
  void testGuardAgainstInvalidSector() {
    final AeriusException exception = assertThrows(AeriusException.class, () -> {
      final EmissionSourceFeature feature = new EmissionSourceFeature();
      feature.setId("1");
      feature.setGeometry(GenericSourceExpanderTest.getExampleLineWKTGeometry());
      final MooringMaritimeShippingEmissionSource emissionSource = new MooringMaritimeShippingEmissionSource();
      final MooringMaritimeShippingEmissionSource source = testDomain.getMooringMaritimeShipEmissionSource(emissionSource);
      feature.setProperties(source);

      // Set the sector to have a none ops calculation engine
      source.setSectorId(RoadType.FREEWAY.getSectorId());
      expand(List.of(feature), KEY_NOX_NH3, true);
    });
    assertEquals(1016, exception.getReason().getErrorCode(), "Should throw with expected reason code");
  }

  private List<EngineSource> createAndExpandOffRoadMobileSources(final boolean stacking) throws SQLException, AeriusException {
    final Sector sector = testDomain.getCategories().getSectorById(3220);
    final List<EmissionSourceFeature> sourceList = createStackableOffRoadSourceList(sector);
    return expand(sourceList, KEY_NOX, stacking);
  }

  private List<EngineSource> expand(final List<EmissionSourceFeature> sourceList, final List<Substance> substances, final boolean stacking)
      throws SQLException, AeriusException {
    final SourceConversionHelper conversionHelper = new SourceConversionHelper(getCalcPMF(), testDomain.getCategories(), TestDomain.YEAR);
    final ScenarioSituation situation = new ScenarioSituation();
    situation.getEmissionSourcesList().addAll(sourceList);
    return EngineSourceExpander.toEngineSources(getCalcConnection(), geometryExpander, conversionHelper, situation, substances, stacking);
  }

  private List<EmissionSourceFeature> createStackableOffRoadSourceList(final Sector sector) {
    final EmissionSourceFeature emissionSource1 = createOffRoadEmissionSource(sector, 1);
    final EmissionSourceFeature emissionSource2 = createOffRoadEmissionSource(sector, 2);
    final List<EmissionSourceFeature> sourceList = new ArrayList<>();
    sourceList.add(emissionSource1);
    sourceList.add(emissionSource2);
    return sourceList;
  }

  private EmissionSourceFeature createOffRoadEmissionSource(final Sector sector, final int id) {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setGeometry(new Point(4, 5));
    final OffRoadMobileEmissionSource emissionSource = new OffRoadMobileEmissionSource();
    feature.setProperties(emissionSource);
    emissionSource.setSectorId(sector.getSectorId());
    testDomain.getOffRoadMobileEmissionSource(emissionSource);
    return feature;
  }

  private void updateEmissions(final List<EmissionSourceFeature> sources) throws AeriusException {
    final EmissionFactorSupplier emissionFactorSupplier = new CategoryBasedEmissionFactorSupplier(testDomain.getCategories(), getCalcPMF(),
        TestDomain.YEAR);
    final GeometryCalculator geometryCalculator = new GeometryCalculatorImpl();
    final EmissionsUpdater updater = new EmissionsUpdater(emissionFactorSupplier, geometryCalculator);
    updater.updateEmissions(sources);
  }
}
