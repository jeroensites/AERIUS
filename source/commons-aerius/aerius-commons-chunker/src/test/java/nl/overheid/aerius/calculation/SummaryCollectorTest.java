/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link SummaryCollector}.
 */
class SummaryCollectorTest extends CalculationTestBase {

  private static final String FILENAME = "filename";
  private CalculationJob calculationJob;
  private SummaryCollector collector;

  @TempDir
  File tempDir;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp();
    calculationJob = getExampleCalculationJob();
    calculationJob.setUnitsAccumulator();
    collector = new SummaryCollector(calculationJob);
  }

  @Test
  void testWritingSummaryFile() throws IOException {
    final Calculation jobCalculation = calculationJob.getCalculations().get(0);
    final int calculationId = jobCalculation.getCalculationId();

    collector.addSummary(calculationId, EmissionResultType.CONCENTRATION, FILENAME);
    calculationJob.getScenarioCalculations().getScenario().getSituations().get(0).getEmissionSourcesList()
        .stream()
        .map(e -> e.getProperties().getSectorId())
        .distinct()
        .forEach(sectorId -> {
          for (int i = 0; i < 100; i++) {
            try {
              collector.addResult(calculationId, sectorId, EmissionResultKey.NH3_CONCENTRATION, 10.0);
              collector.addResult(calculationId, sectorId, EmissionResultKey.NOX_CONCENTRATION, 5.0);
            } catch (final AeriusException e) {
              throw new RuntimeException(e);
            }
          }
        });
    collector.writeSummary(tempDir.getAbsolutePath(), calculationJob.getName());
    final File summaryFile = new File(tempDir, calculationJob.getName() + ".summary");
    assertTrue(summaryFile.exists(), "Summary file should exist");
    final List<String> lines = Files.readAllLines(summaryFile.toPath());
    assertEquals(7, lines.size(), "Summary file should have expected nr. of files");
    // Because lines are created based on Map variables the order can be arbitrary so we try to find the line with the nox value.
    final int noxRow = lines.get(1).contains("nox") ? 1 : 2;
    assertEquals("concentration;1800;nox;5.0;5.0;500.0;5.0;50;50000.0;0;0;" + FILENAME, lines.get(noxRow),
        "Content of a line should be as expected");
  }
}
