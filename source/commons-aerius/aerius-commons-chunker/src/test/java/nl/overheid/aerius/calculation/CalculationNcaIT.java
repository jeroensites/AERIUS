/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.theme.nca.NCAMockConnection;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.SRM2RoadEmissionSource;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.test.TestDomain;

/**
 * Integration test to do a complete calculation NCA.
 */
class CalculationNcaIT extends CalculationITBase {

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp((dsv, rdf) -> new NCAMockConnection(dsv, rdf), WorkerType.ADMS);
  }

  @Disabled("Disabled test for now as it needs additional testing.")
  @Test
  @Timeout(value = 20, unit = TimeUnit.SECONDS)
  void testRunSingle() throws Exception {
    assertTestRun(new NcaTestInputBuilder().createInputData());
  }

  private static class NcaTestInputBuilder extends TestInputBuilder {

    public NcaTestInputBuilder() {
      super(Theme.NCA);
    }

    @Override
    protected CalculationInputData createInputData() {
      final CalculationInputData inputData = super.createInputData();
      inputData.setExportType(ExportType.CSV);
      return inputData;
    }

    @Override
    protected void setScenario(final Scenario scenario) {
      super.setScenario(scenario);
      addPoints(scenario);
    }

    private void addPoints(final Scenario scenario) {
      final Random random = new Random();
      for (int i = 0; i < 100; i++) {
        final CalculationPointFeature point = new CalculationPointFeature();
        point.setGeometry(new Point(TestDomain.XCOORD_1 + random.nextInt(2000), TestDomain.YCOORD_1 + random.nextInt(2000)));
        final CalculationPoint calculationPoint = new CustomCalculationPoint();

        point.setProperties(calculationPoint);
        point.setId(String.valueOf(i));
        calculationPoint.setGmlId(point.getId());
        scenario.getCustomPoints().getFeatures().add(point);
      }
    }

    @Override
    protected void setSources(final List<EmissionSourceFeature> sources, final double emissionFactor) {
      final LineString geometryRoad = new LineString();
      geometryRoad.setCoordinates(new double[][] {{TestDomain.XCOORD_1, TestDomain.YCOORD_1}, {TestDomain.XCOORD_1 + 100, TestDomain.YCOORD_1 + 100}});
      final SRM2RoadEmissionSource emissionSource = new SRM2RoadEmissionSource();
      emissionSource.getEmissions().put(Substance.NOX, 10.0 * emissionFactor);
      sources.add(TestDomain.getSource("1", geometryRoad, "SomeRoad", emissionSource));
    }

    @Override
    protected void setCalculationSetOptions(final CalculationSetOptions options) {
      options.setCalculationType(CalculationType.PERMIT);
      options.getSubstances().add(Substance.NOX);
      options.getEmissionResultKeys().add(EmissionResultKey.NOX_CONCENTRATION);
    }
  }
}
