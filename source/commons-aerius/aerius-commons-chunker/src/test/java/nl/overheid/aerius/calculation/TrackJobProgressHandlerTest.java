/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.Transaction;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.connect.ConnectUserRepository;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link TrackJobProgressHandler}.
 */
class TrackJobProgressHandlerTest extends CalculationTestBase {

  private static final String TEST_EMAIL = "aerius@example.com";
  private static final Date BEFORE_ALL_TIME = new Date();

  private String jobKey;
  private TrackJobProgressHandler handler;
  private int calculationId;
  private Connection connection;
  private Transaction transaction;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp();
    connection = getCalcConnection();
    transaction = new Transaction(connection);
    final ConnectUser user = createUser(connection);
    jobKey = JobRepository.createJob(connection, user, JobType.CALCULATION);
    handler = initHandler(new JobIdentifier(jobKey));
  }

  @AfterEach
  void after() throws Exception {
    transaction.rollback();
    super.tearDown();
  }

  @Test
  void testInit() throws SQLException {
    assertTrue(handler.isJobFound(), "Job should be found");
    assertFalse(StringUtils.isEmpty(jobKey), "jobKey should be set:" + jobKey);
    final JobProgress progress = JobRepository.getProgress(connection, jobKey);
    assertTrue(progress.getCreationDateTime().after(BEFORE_ALL_TIME), "CreationDateTime should be later than the start time of this test");
  }

  @Test
  void testOnTotalResults() throws AeriusException, SQLException {
    handler.onTotalResults(IntermediateResultHandlerUtil.createResults(), calculationId);
    final JobProgress progress = JobRepository.getProgress(connection, jobKey);
    assertEquals(3, progress.getHexagonCount(), "Hexagon count should be matching created results");
  }

  private TrackJobProgressHandler initHandler(final JobIdentifier jobIdentifier) throws AeriusException, SQLException {
    final TrackJobProgressHandler handler = new TrackJobProgressHandler(getCalcPMF(), getExampleCalculationJob(jobIdentifier));
    handler.init();

    return handler;
  }

  private ConnectUser createUser(final Connection connection) throws SQLException, AeriusException {
    final ConnectUser createUser = new ConnectUser();
    createUser.setApiKey(TEST_EMAIL);
    createUser.setEmailAddress(TEST_EMAIL);
    createUser.setEnabled(true);

    try {
      ConnectUserRepository.createUser(connection, createUser);
    } catch (final AeriusException e) {
      if (e.getReason() != AeriusExceptionReason.USER_EMAIL_ADDRESS_ALREADY_EXISTS) {
        throw e;
      }
    }
    return ConnectUserRepository.getUserByEmailAddress(connection, TEST_EMAIL);
  }
}
