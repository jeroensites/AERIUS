/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationInfoRepository;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.test.TestDomain;

/**
 * Integration test to do a complete calculation WNB. The calculation of OPS is mocked.
 */
class CalculationWnbIT extends CalculationITBase {

  private static final int GENERIC_SECTOR = 1800;
  private Consumer<EngineInputData<?, ?>> inputAsserter = input -> {
  };

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    super.setUp((dsv, rdf) -> new WNBMockConnection(dsv, rdf, input -> inputAsserter.accept(input)), WorkerType.OPS, WorkerType.ASRM);
  }

  @Test
  @Timeout(value = 20_000, unit = TimeUnit.MILLISECONDS)
  void testRunSingle() throws Exception {
    final AtomicBoolean minDistanceCheck = new AtomicBoolean();
    inputAsserter = input -> {
      if (input instanceof OPSInputData) {
        final OPSInputData opsInput = (OPSInputData) input;
        minDistanceCheck.set(opsInput.minDistanceContains(3111) && opsInput.minDistanceContains(3112)
            && !opsInput.minDistanceContains(GENERIC_SECTOR));
      }
    };
    assertTestRun(new WnbTestInputBuilder().createInputData());
    assertTrue(minDistanceCheck.get(), "Should have set both sectors assigned as minDistance");
  }

  @Test
  void testRunSingleWithCustomPointsCombined() throws Exception {
    final int numberOfCustomPoints = 100;
    final WnbTestInputBuilder builder = new WnbTestInputBuilder() {

      @Override
      protected void setScenario(final Scenario scenario) {
        super.setScenario(scenario);
        addPoints(scenario);
      }

      private void addPoints(final Scenario scenario) {
        final Random random = new Random();
        for (int i = 0; i < numberOfCustomPoints; i++) {
          final CalculationPointFeature point = new CalculationPointFeature();
          point.setGeometry(new Point(TestDomain.XCOORD_1 + random.nextInt(2000), TestDomain.YCOORD_1 + random.nextInt(2000)));
          final CustomCalculationPoint calculationPoint = new CustomCalculationPoint();

          point.setProperties(calculationPoint);
          calculationPoint.setCustomPointId(i);
          point.setId(String.valueOf(i));
          calculationPoint.setGmlId(point.getId());
          scenario.getCustomPoints().getFeatures().add(point);
        }
      }
    };
    assertTestRun(builder.createInputData());
    assertResults(1);
    for (final Calculation calculation : calculationJob.getCalculations()) {
      final PartialCalculationResult results = CalculationInfoRepository.getCalculationResults(getCalcConnection(), calculation.getCalculationId());
      final long numberOfCustomPointResults =
          results.getResults().stream().map(CalculationPointFeature::getProperties).filter(CustomCalculationPoint.class::isInstance).count();

      assertEquals(numberOfCustomPoints, numberOfCustomPointResults, "Should have same number calculated as input");
      assertNotEquals(results.getResults().size(), numberOfCustomPoints, "Should not have also receptor points");
    }
  }

  @Test
  void testRunComparison() throws Exception {
    run(new WnbComparisonTestInputBuilder().createInputData());
    assertResults(2);
  }

  @Test
  void testSectorResults() throws Exception {
    final CalculationInputData inputData = new WnbTestInputBuilder() {
      @Override
      protected void setSources(final java.util.List<EmissionSourceFeature> sources, final double emissionFactor) {
        sources.addAll(createPointSourceList(1.0));
      };
    }.createInputData();

    inputData.setExportType(ExportType.GML_WITH_SECTORS_RESULTS);
    run(inputData);

    // Get the sector results for default sector
    final int id = calculationJob.getCalculations().get(0).getCalculationId();
    final List<CalculationPointFeature> receptorPoints = getReceptors(getCalcPMF(), id, inputData.getScenario(), Sector.SECTOR_DEFAULT.getSectorId());

    assertNotNull(receptorPoints, "Sector result retrieved");
    assertFalse(receptorPoints.isEmpty(), "Calculations should not be empty");

    for (final CalculationPointFeature point : receptorPoints) {
      assertFalse(point.getProperties().getResults().isEmpty(), "Sector emission should not be empty");
    }
  }

  @Override
  protected void assertResults(final int nrOfCalculationsExpected) throws Exception {
    super.assertResults(nrOfCalculationsExpected);
    for (final Calculation calculation : calculationJob.getCalculations()) {
      assertFalse(CalculationInfoRepository.getCalculationResults(getCalcConnection(), calculation.getCalculationId()).getResults().isEmpty(),
          "Should have calculation results per receptor in the database");
    }
  }

  private static List<CalculationPointFeature> getReceptors(final PMF pmf, final Integer calculationId,
      final Scenario scenario, final Integer sectorId) throws SQLException {
    final List<CalculationPointFeature> receptors = new ArrayList<>();
    if (calculationId != null && calculationId > 0) {
      //no need for validating calculation, if not available no results will be found anyway.
      //(or should we take that as an exception?)
      try (final Connection connection = pmf.getConnection()) {
        final PartialCalculationResult result = CalculationInfoRepository.getCalculationSectorResults(connection, calculationId, sectorId);
        receptors.addAll(result.getResults());
      }
    }
    return receptors;
  }

  private static class WnbTestInputBuilder extends TestInputBuilder {

    public WnbTestInputBuilder() {
      super(Theme.WNB);
    }

    @Override
    protected void setSources(final List<EmissionSourceFeature> sources, final double emissionFactor) {
      final LineString geometry = new LineString();
      geometry.setCoordinates(new double[][] {{TestDomain.XCOORD_1, TestDomain.YCOORD_1}, {TestDomain.XCOORD_1 + 100, TestDomain.YCOORD_1}});

      sources.addAll(List.of(createSource(emissionFactor, geometry, 3111), createSource(emissionFactor, geometry, 3112),
          createSource(emissionFactor, geometry, GENERIC_SECTOR)));
    }

    @Override
    protected void setCalculationSetOptions(final CalculationSetOptions options) {
      options.setCalculationType(CalculationType.PERMIT);
    }

    protected static List<EmissionSourceFeature> createPointSourceList(final double emissionFactor) {
      final Point geometry = new Point(TestDomain.XCOORD_1, TestDomain.YCOORD_1);

      return List.of(createSource(emissionFactor, geometry, GENERIC_SECTOR));
    }

    private static EmissionSourceFeature createSource(final double emissionFactor, final Geometry geometry, final int sectorId) {
      final GenericEmissionSource emissionSource = new GenericEmissionSource();
      emissionSource.setSectorId(sectorId);
      emissionSource.getEmissions().put(Substance.NH3, 10.0 * emissionFactor);
      emissionSource.getEmissions().put(Substance.NOX, 20.0 * emissionFactor);
      return TestDomain.getSource("1", geometry, "SomeSource", emissionSource);
    }
  }

  private static class WnbComparisonTestInputBuilder extends WnbTestInputBuilder {

    @Override
    protected void setScenario(final Scenario scenario) {
      final ScenarioSituation situationOne = new ScenarioSituation();
      situationOne.getEmissionSourcesList().clear();
      situationOne.getEmissionSourcesList().addAll(createPointSourceList(1.0));
      situationOne.setType(SituationType.REFERENCE);
      situationOne.setYear(TestDomain.YEAR);
      final ScenarioSituation situationTwo = new ScenarioSituation();
      situationTwo.getEmissionSourcesList().clear();
      situationTwo.getEmissionSourcesList().addAll(createPointSourceList(2.0));
      situationTwo.setType(SituationType.PROPOSED);
      situationTwo.setYear(TestDomain.YEAR);
      scenario.getSituations().add(situationOne);
      scenario.getSituations().add(situationTwo);
    }
  }
}
