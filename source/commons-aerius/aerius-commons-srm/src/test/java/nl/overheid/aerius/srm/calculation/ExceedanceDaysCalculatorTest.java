/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ExceedanceDaysCalculator}.
 */
class ExceedanceDaysCalculatorTest {

  @Test
  void testHockeystick() {
    assertEquals(6d, ExceedanceDaysCalculator.calculate(0), 1E-3, "Number of excess days");
    assertEquals(6d, ExceedanceDaysCalculator.calculate(1), 1E-3, "Number of excess days");
    assertEquals(15.71d, ExceedanceDaysCalculator.calculate(25), 1E-3, "Number of excess days");
    assertEquals(352.36d, ExceedanceDaysCalculator.calculate(100), 1E-3, "Number of excess days");

    // Test the boundary values.
    assertEquals(6.03d, ExceedanceDaysCalculator.calculate(16), 1E-3, "Number of excess days");
    assertEquals(35d, ExceedanceDaysCalculator.calculate(31.2), 1E-3, "Number of excess days");

    assertEquals(365d, ExceedanceDaysCalculator.calculate(200), 1E-3, "Maximum number of excess days");
  }
}
