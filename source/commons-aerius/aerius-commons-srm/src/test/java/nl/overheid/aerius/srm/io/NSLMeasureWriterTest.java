/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.base.EmissionReduction;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLMeasure;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicleMeasure;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;

/**
 * Test class for {@link NSLMeasureWriter}.
 */
class NSLMeasureWriterTest {

  @Test
  void testNormal() throws IOException {
    final NSLMeasureWriter resultWriter = new NSLMeasureWriter(4);

    final File outputFile = new File(Files.createTempDirectory("NSLMeasureWriterTest").toFile(), "normalTestFile.csv");

    final List<NSLMeasureFeature> measures = new ArrayList<>();
    measures.add(testCase(87));

    resultWriter.write(outputFile, measures);

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_nsl_measures.csv"));

    assertEquals(expected, result, "Result for normal NSL points export");
  }

  @Test
  void testWriteSeparateActions() throws IOException {
    final NSLMeasureWriter resultWriter = new NSLMeasureWriter(4);

    final File outputFile = new File(Files.createTempDirectory("NSLMeasureWriterTest").toFile(), "extendedTestFile.csv");

    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeHeader(writer);
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeRow(writer, testCase(25));
    }
    try (Writer writer = new PrintWriter(
        Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
      resultWriter.writeRow(writer, testCase(600));
    }

    final String result = AssertCSV.getFileContent(outputFile);
    final String expected = AssertCSV.getFileContent(AssertCSV.getFile("aerius_nsl_measures_separate_writing.csv"));

    assertEquals(expected, result, "Result for separate writing NSL points export");
  }

  private static NSLMeasureFeature testCase(final int id) {
    final NSLMeasureFeature feature = new NSLMeasureFeature();
    final NSLMeasure measure = new NSLMeasure();
    feature.setProperties(measure);
    measure.setGmlId("PREFIX." + id);
    measure.setLabel("maatregel " + id);
    measure.setDescription("maatregelen voor gebieden");

    final StandardVehicleMeasure vehicleMeasure = new StandardVehicleMeasure();
    vehicleMeasure.setVehicleType(VehicleType.LIGHT_TRAFFIC);
    vehicleMeasure.setRoadSpeedType(RoadSpeedType.URBAN_TRAFFIC_NORMAL);
    final EmissionReduction emissionReduction1 = new EmissionReduction();
    emissionReduction1.setSubstance(Substance.PM10);
    emissionReduction1.setFactor(id * 1.123456789);
    vehicleMeasure.addEmissionReduction(emissionReduction1);
    final EmissionReduction emissionReduction2 = new EmissionReduction();
    emissionReduction2.setSubstance(Substance.NOX);
    emissionReduction2.setFactor(id * 5.0);
    vehicleMeasure.addEmissionReduction(emissionReduction2);
    measure.getVehicleMeasures().add(vehicleMeasure);

    final StandardVehicleMeasure vehicleMeasure2 = new StandardVehicleMeasure();
    vehicleMeasure2.setVehicleType(VehicleType.NORMAL_FREIGHT);
    vehicleMeasure2.setRoadSpeedType(RoadSpeedType.URBAN_TRAFFIC_NORMAL);
    final EmissionReduction emissionReduction3 = new EmissionReduction();
    emissionReduction3.setSubstance(Substance.PM10);
    emissionReduction3.setFactor(id * 0.8);
    vehicleMeasure2.addEmissionReduction(emissionReduction3);
    measure.getVehicleMeasures().add(vehicleMeasure2);

    final Polygon polygon = new Polygon();
    polygon.setCoordinates(new double[][][] {{
      {(id + 20.0), (id + 10.0)},
      {(id + 20.0), (id + 30.0)},
      {(id + 40.0), (id + 30.0)},
      {(id + 20.0), (id + 10.0)}
    }});
    feature.setGeometry(polygon);

    if (id % 2 == 1) {
      measure.setJurisdictionId(4);
    }

    return feature;
  }

}
