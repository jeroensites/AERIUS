/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.base.EmissionReduction;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLMeasure;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicleMeasure;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link NSLMeasureFileReader}.
 */
class NSLMeasureFileReaderTest {

  private static final String SPECIFIC_TEST_FILE = "measure_example.csv";

  @Test
  void testSpecificCase() throws Exception {
    final NSLMeasureFileReader reader = new NSLMeasureFileReader();
    final File file = new File(getClass().getResource(SPECIFIC_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(isr)) {

      final String header = bufferedReader.readLine();
      final AeriusException parsingHeaderException = reader.parseHeader(header);

      assertNull(parsingHeaderException, "Header should be correctly parsed");

      final LineReaderResult<NSLMeasureFeature> result = reader.readObjects(bufferedReader);

      if (!result.getExceptions().isEmpty()) {
        throw result.getExceptions().get(0);
      }
      final List<NSLMeasureFeature> measures = result.getObjects();
      validateResults(measures);
    }
  }

  @Test
  void testSpecificCaseWithMainImporter() throws Exception {
    final LegacyNSLImportReader reader = new LegacyNSLImportReader();
    final File file = new File(getClass().getResource(SPECIFIC_TEST_FILE).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {

      final SectorCategories categories = Mockito.mock(SectorCategories.class);
      final Substance substance = Substance.NOX;
      final ImportParcel importResult = new ImportParcel();

      reader.read("some name", inputStream, categories, substance, importResult);

      if (!importResult.getExceptions().isEmpty()) {
        throw importResult.getExceptions().get(0);
      }
      final List<NSLMeasureFeature> measures = importResult.getSituation().getNslMeasuresList();
      validateResults(measures);
    }
  }

  private void validateResults(final List<NSLMeasureFeature> measures) {
    assertEquals(1, measures.size(), "Count nr. of measures");
    final NSLMeasureFeature feature = measures.get(0);
    final NSLMeasure measure = feature.getProperties();
    assertEquals("PREFIX.5", measure.getGmlId(), "measure id");
    assertNotNull(measure.getJurisdictionId(), "jurisdiction id");
    assertEquals(7, measure.getJurisdictionId().intValue(), "jurisdiction id");
    assertEquals("doe maar tekst", measure.getLabel(), "label");
    assertEquals("doe maar beschrijving", measure.getDescription(), "description");
    assertNotNull(feature.getGeometry(), "geometry");
    assertTrue(feature.getGeometry() instanceof Polygon, "geometry type");
    assertArrayEquals(new double[][][] {{
        {119687.74, 495299.28},
        {124727.74, 495353.04},
        {124647.1, 491509.2},
        {117470.14, 493296.72},
        {119687.74, 495299.28}
    }}, feature.getGeometry().getCoordinates(),
        "geometry");

    assertEquals(3, measure.getVehicleMeasures().size(), "Count nr. of vehicle measures");
    for (final StandardVehicleMeasure vehicleMeasure : measure.getVehicleMeasures()) {
      if (vehicleMeasure.getVehicleType() == VehicleType.NORMAL_FREIGHT
          && vehicleMeasure.getRoadSpeedType() == RoadSpeedType.URBAN_TRAFFIC_STAGNATING) {
        assertEquals(2, vehicleMeasure.getEmissionReductions().size(), "Count nr. of emission reductions");
        for (final EmissionReduction reduction : vehicleMeasure.getEmissionReductions()) {
          if (reduction.getSubstance() == Substance.NOX) {
            assertEquals(0.7, reduction.getFactor(), 1E-10, "factor substance NOx");
          } else if (reduction.getSubstance() == Substance.PM10) {
            assertEquals(0.6, reduction.getFactor(), 1E-10, "factor substance PM10");
          } else {
            fail("Unexpected substance: " + reduction.getSubstance());
          }
        }
      } else {
        assertEquals(1, vehicleMeasure.getEmissionReductions().size(), "Count nr. of emission reductions");
        final EmissionReduction reduction = vehicleMeasure.getEmissionReductions().get(0);
        assertEquals(Substance.NOX, reduction.getSubstance(), "substance");
        if (vehicleMeasure.getVehicleType() == VehicleType.LIGHT_TRAFFIC) {
          assertEquals(0.5, reduction.getFactor(), 1E-10, "factor light traffic");
        } else if (vehicleMeasure.getVehicleType() == VehicleType.NORMAL_FREIGHT) {
          assertEquals(0.8, reduction.getFactor(), 1E-10, "factor normal freight");
        } else {
          fail("Unexpected vehicle type: " + vehicleMeasure.getVehicleType());
        }
        assertEquals(RoadSpeedType.URBAN_TRAFFIC_FREE_FLOW, vehicleMeasure.getRoadSpeedType(), "speed type");
      }
    }
  }

}
