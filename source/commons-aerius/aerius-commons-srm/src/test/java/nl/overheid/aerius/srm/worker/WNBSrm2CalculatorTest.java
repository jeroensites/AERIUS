/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.srm.AbstractSRMTestBase;
import nl.overheid.aerius.srm.io.NSLResult;
import nl.overheid.aerius.srm2.nsl.SRM2CalculatorTest;
import nl.overheid.aerius.util.OSUtils;

/**
 * Test class for {@link WNBSrm2Calculator}
 */
class WNBSrm2CalculatorTest extends AbstractSRMTestBase<WNBResult> {

  private static final double ROUDING_RESOLUTION = 0.001;
  private static final String RELATIVE_RESOURCES_PATH = "../../srm2/wnb/";
  private static final int YEAR = 2020;
  private static final Substance[] SUBSTANCES = new Substance[] {Substance.NOX, Substance.NO2, Substance.NH3};
  private static final EnumSet<EmissionResultKey> EMISSION_RESULT_KEYS =
      EnumSet.of(EmissionResultKey.NOX_DEPOSITION, EmissionResultKey.NO2_CONCENTRATION, EmissionResultKey.NOX_CONCENTRATION,
          EmissionResultKey.NH3_DEPOSITION);

  static List<Object[]> data() throws FileNotFoundException {
    return data(new File(SRM2CalculatorTest.class.getResource(RELATIVE_RESOURCES_PATH).getFile()), RELATIVE_RESOURCES_PATH,
        "segment", "receptors", "results");
  }

  @Override
  protected LineReaderResult<WNBResult> readResultFile(final InputStream is) throws IOException {
    final WNBReferenceResultsReader reader = new WNBReferenceResultsReader();
    return reader.readObjects(is);
  }

  @ParameterizedTest
  @MethodSource("data")
  void testCalculate(final String name, final String srcFile, final String receptorFile, final String resultFile) throws Throwable {
    logger.info("Run test '{}'", name);
    init(name, srcFile, receptorFile, resultFile, null, null);
    assertCalculate(Theme.WNB, YEAR, SUBSTANCES, EMISSION_RESULT_KEYS);
  }

  @Override
  protected void assertSectorResults(final Map<Integer, List<AeriusResultPoint>> results) {
    for (final Entry<Integer, List<AeriusResultPoint>> entry : results.entrySet()) {
      for (int i = 0; i < entry.getValue().size(); i++) {
        final AeriusResultPoint result = entry.getValue().get(i);
        final String ourId = result.getGmlId() + "_" + entry.getKey();

        assertEquals(expectedResults.get(ourId).getSrm2NOxDeposition(), result.getEmissionResult(EmissionResultKey.NOX_DEPOSITION),
            ROUDING_RESOLUTION, "NOx deposition id " + entry.getKey() + " for " + result.getId());
        assertEquals(expectedResults.get(ourId).getSrm2NH3Deposition(), result.getEmissionResult(EmissionResultKey.NH3_DEPOSITION),
            ROUDING_RESOLUTION, "NH3 depotision id " + entry.getKey() + " for " + result.getId());
      }
    }
  }

  @Override
  protected void writeSectorResultsToFile(final Map<Integer, List<AeriusResultPoint>> sectorResults) throws IOException {
    final List<String> content = new ArrayList<>();
    content.add("calculation_point_id;sector_id;nox_deposition;nh3_deposition");
    for (final Entry<Integer, List<AeriusResultPoint>> entry : sectorResults.entrySet()) {
      content.addAll(
          entry.getValue().stream().map(r -> formatResult(r, entry.getKey()))
              .collect(Collectors.toList()));
    }
    final File file = new File(getClass().getResource(RELATIVE_RESOURCES_PATH).getFile(),
        "Rekenresultaten_" + name + ".csv");

    logger.info("Write results to {}", file);

    try (final Writer writer = Files.newBufferedWriter(file.toPath())) {
      for (final String string : content) {
        writer.write(string);
        writer.write(OSUtils.NL);
      }
    }
  }

  private String formatResult(final AeriusResultPoint point, final int sectorId) {
    return String.format("%s;%d;%.6f;%.6f",
        point.getGmlId(), sectorId,
        point.getEmissionResult(EmissionResultKey.NOX_DEPOSITION),
        point.getEmissionResult(EmissionResultKey.NH3_DEPOSITION));
  }

  @Override
  protected int assertResult(final AeriusResultPoint rp) {
    final NSLResult er = expectedResults.get(rp.getGmlId());

    assertNotNull(er, "Expect to have result for id: " + rp.getId());
    assertEquals((int) Math.round(er.getPoint().getX()), rp.getRoundedX(), "X");
    assertEquals((int) Math.round(er.getPoint().getY()), rp.getRoundedY(), "Y");
    int failed = 0;
    failed += assertEqualsLog("NOX Deposition", er, er.getSrm2ConcentrationNOx(), rp.getEmissionResult(EmissionResultKey.NOX_DEPOSITION),
        ROUDING_RESOLUTION);
    return failed;
  }

}
