/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ExceedanceHoursCalculator}.
 */
class ExceedanceHoursCalculatorTest {

  @Test
  void testCalculateLast() {
    final double cjNO2 = 18.1330771446228;
    final double expected = 73.2034927463532;
    assertEquals(expected, ExceedanceHoursCalculator.calculate19(cjNO2), 1E-13, "Expected concentration day 19");
  }

  @Test
  void testCalculateDays() {
    final double[] cjNO2 = IntStream.range(1, 20).mapToDouble(i -> 50.0 + i * 2.0).toArray();
    final int[] expected = {0, 1, 1, 2, 2, 3, 4, 5, 6, 7, 8, 10, 11, 14, 16, 18, 19, 19, 19,};

    for (int i = 0; i < cjNO2.length; i++) {
      assertEquals(expected[i], (int) ExceedanceHoursCalculator.calculate(cjNO2[i]), "Expected days");
    }
  }
}
