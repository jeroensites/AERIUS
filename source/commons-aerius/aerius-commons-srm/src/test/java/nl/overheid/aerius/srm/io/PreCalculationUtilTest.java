/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.vividsolutions.jts.geom.Geometry;

import nl.overheid.aerius.shared.domain.nsl.NSLCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLDispersionLineFeature;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Test class for {@link PreCalculationUtil}.
 */
class PreCalculationUtilTest {

  @Test
  void testSetDispersionLineGeometry() throws AeriusException {
    final NSLDispersionLineFeature dispersionLine = new NSLDispersionLineFeature();
    final EmissionSourceFeature emissionSource = new EmissionSourceFeature();
    final LineString lineString = new LineString();
    lineString.setCoordinates(new double[][] {{300, 300}, {2000, 300}});
    emissionSource.setGeometry(lineString);
    final NSLCalculationPoint calculationPoint = new NSLCalculationPoint(0, "10", 1000, 1000);

    PreCalculationUtil.setDispersionLineGeometry(dispersionLine, emissionSource, calculationPoint);
    final Geometry geometry = GeometryUtil.getGeometry(dispersionLine.getGeometry());
    assertEquals(700, (int) geometry.getLength(), "Distance incorrectly calculated:");
    assertEquals("LINESTRING (1000 300, 1000 1000)", geometry.toText(), "Coordinate incorrectly calculated:");
  }
}
