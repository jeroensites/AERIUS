/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.util.OSUtils;

/**
 * Test class for {@link SubReceptorDisplacementsConstant}.
 */
class SubReceptorDisplacementsConstantTest {

  static final HexagonZoomLevel ZOOM_LEVEL_1 = new HexagonZoomLevel(1, 10000);

  @Test
  void testGetValue() {
    final SubReceptorDisplacementsConstant constant = new SubReceptorDisplacementsConstant(2, ZOOM_LEVEL_1);
    assertEquals(getExpectedResult2Rings(), constant.getValue(), "Result with 2 rings");
  }

  private String getExpectedResult2Rings() {
    final StringBuilder builder = new StringBuilder();
    builder.append("constant double2 SUB_RECEPTOR_DISPLACEMENTS[19] = {").append(OSUtils.LNL);
    builder.append("(double2)(0.0, 0.0),").append(OSUtils.LNL);

    builder.append("(double2)(-31.020161970069985, -53.72849659117709),").append(OSUtils.LNL);
    builder.append("(double2)(0.0, -53.72849659117709),").append(OSUtils.LNL);
    builder.append("(double2)(31.020161970069985, -53.72849659117709),").append(OSUtils.LNL);

    builder.append("(double2)(-46.53024295510498, -26.864248295588546),").append(OSUtils.LNL);
    builder.append("(double2)(-15.510080985034993, -26.864248295588546),").append(OSUtils.LNL);
    builder.append("(double2)(15.510080985034993, -26.864248295588546),").append(OSUtils.LNL);
    builder.append("(double2)(46.53024295510498, -26.864248295588546),").append(OSUtils.LNL);

    builder.append("(double2)(-62.04032394013997, 0.0),").append(OSUtils.LNL);
    builder.append("(double2)(-31.020161970069985, 0.0),").append(OSUtils.LNL);
    builder.append("(double2)(31.020161970069985, 0.0),").append(OSUtils.LNL);
    builder.append("(double2)(62.04032394013997, 0.0),").append(OSUtils.LNL);

    builder.append("(double2)(-46.53024295510498, 26.864248295588546),").append(OSUtils.LNL);
    builder.append("(double2)(-15.510080985034993, 26.864248295588546),").append(OSUtils.LNL);
    builder.append("(double2)(15.510080985034993, 26.864248295588546),").append(OSUtils.LNL);
    builder.append("(double2)(46.53024295510498, 26.864248295588546),").append(OSUtils.LNL);

    builder.append("(double2)(-31.020161970069985, 53.72849659117709),").append(OSUtils.LNL);
    builder.append("(double2)(0.0, 53.72849659117709),").append(OSUtils.LNL);
    builder.append("(double2)(31.020161970069985, 53.72849659117709)};");

    return builder.toString();
  }
}
