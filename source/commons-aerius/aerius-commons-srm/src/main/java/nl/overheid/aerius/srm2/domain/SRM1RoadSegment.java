/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.domain;

import java.util.Objects;

import nl.overheid.aerius.shared.domain.EngineEmissionSource;
import nl.overheid.aerius.shared.domain.EngineSource;

/**
 * {@link EngineSource} object for SRM1 road data.
 */
public class SRM1RoadSegment extends EngineEmissionSource {

  private static final long serialVersionUID = 1L;

  private String wktGeometry;
  private String segmentId;

  public SRM1RoadSegment() {
  }

  public SRM1RoadSegment(final String segmentId, final String wktGeometry) {
    this.segmentId = segmentId;
    this.wktGeometry = wktGeometry;
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((segmentId == null) ? 0 : segmentId.hashCode());
    return prime * result + ((wktGeometry == null) ? 0 : wktGeometry.hashCode());
  }

  @Override
  public boolean equals(final Object obj) {
    return this == obj
        || (obj != null && getClass() == obj.getClass()
        && segmentId == ((SRM1RoadSegment) obj).segmentId
        && Objects.equals(wktGeometry, ((SRM1RoadSegment) obj).wktGeometry));
  }

  public String getWKTGeometry() {
    return wktGeometry;
  }

  public void setWKTGeometry(final String wktGeometry) {
    this.wktGeometry = wktGeometry;
  }

  public String getSegmentId() {
    return segmentId;
  }

  public void setSegmentId(final String segmentId) {
    this.segmentId = segmentId;
  }

  @Override
  public String toString() {
    return "SRM1RoadSegment [geometry=" + wktGeometry + ", segmentId=" + segmentId + "]";
  }
}
