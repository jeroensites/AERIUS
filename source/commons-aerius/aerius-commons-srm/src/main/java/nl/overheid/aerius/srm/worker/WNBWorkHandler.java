/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.opencl.DefaultContext;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.PreSRMData;
import nl.overheid.aerius.srm.config.SRMWorkerConfiguration;
import nl.overheid.aerius.srm2.calculation.SRM2Calculator;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;
import nl.overheid.aerius.srm2.domain.SRMInputData;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.Worker;

/**
 * SRM WorkHandler for the {@link Theme#WNB}.
 */
class WNBWorkHandler implements Worker<SRMInputData<SRM2RoadSegment>, CalculationResult> {
  private static final Logger LOGGER = LoggerFactory.getLogger(WNBWorkHandler.class);

  private final SRM2CalculationOptions calculationOptions = SRMConstants.WNB_SRM2_CALCULATION_OPTIONS;
  private final SRM2Calculator srm2Calculator;

  public WNBWorkHandler(final SRMWorkerConfiguration config, final PreSRMData preSRMData) {
    srm2Calculator = new SRM2Calculator(new DefaultContext(true, config.isForceCpu()), preSRMData, config.getConnectionTimeout(),
            calculationOptions) {
      @Override
      protected AeriusResultPoint newResultPoint(final AeriusPoint point) {
        return new AeriusResultPoint(point);
      }
    };
  }

  @Override
  public CalculationResult run(final SRMInputData<SRM2RoadSegment> input, final WorkerIntermediateResultSender resultSender,
      final JobIdentifier jobIdentifier) throws AeriusException {
    checkAndSetMeteo(input);

    LOGGER.debug("Start calculation run for profile WNB");
    final WNBSrm2Calculator calculator =
        new WNBSrm2Calculator(srm2Calculator, input.getSubstances(), input.getYear(), input.getMeteo(), input.getEmissionResultKeys(), input.getReceptors());

    return calculator.calculate(input.getEmissionSources());
  }

  private void checkAndSetMeteo(final SRMInputData<SRM2RoadSegment> input) throws AeriusException {
    final Meteo meteo = input.getMeteo();
    if (meteo == null) {
      input.setMeteo(calculationOptions.getMeteoForYear(input.getYear()));
    } else if (meteo.isMultiYear() && !meteo.equals(calculationOptions.getPrognoseBaseMeteo())) {
      // If a custom Meteo is specified, check if the prognose of PreSRM is based on the same years.
      throw new AeriusException(AeriusExceptionReason.SRM_UNSUPPORTED_METEO, calculationOptions.getPrognoseBaseMeteo().toString(), meteo.toString());
    }

  }
}
