/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl;

import com.jogamp.opencl.CLMemory.Mem;

/**
 * A usage hint for a buffer by OpenCL kernels.
 */
public enum MemoryUsage {
  /**
   * The buffer will only be read by kernels.
   */
  READ_ONLY(Mem.READ_ONLY),

  /**
   * The buffer will receive both reads and writes from kernels.
   */
  READ_WRITE(Mem.READ_WRITE),
  /**
   * The buffer will only be written to by kernels.
   */
  WRITE_ONLY(Mem.WRITE_ONLY);

  private final Mem clValue;

  MemoryUsage(final Mem clValue) {
    this.clValue = clValue;
  }

  public Mem getClValue() {
    return clValue;
  }
}
