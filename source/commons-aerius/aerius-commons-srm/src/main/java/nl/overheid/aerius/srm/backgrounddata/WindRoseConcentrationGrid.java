/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.srm.SRMConstants;

/**
 * Grid object storing the {@link WindRoseConcentration} data.
 */
public class WindRoseConcentrationGrid extends PointGrid<WindRoseConcentration> {

  private static final WindRoseConcentration NO_DATA_POINT = new WindRoseConcentration(0, 0) {
    @Override
    public double getGCN(final Substance substance) {
      return SRMConstants.NO_DATA;
    }

    @Override
    public double getCHwn(final Substance substance) {
      return SRMConstants.NO_DATA;
    }

    @Override
    public double getBackground(final Substance substance) {
      return SRMConstants.NO_DATA;
    }

    @Override
    public double getO3(final int sector) {
      return SRMConstants.NO_DATA;
    }
  };

  public WindRoseConcentrationGrid(final int columns, final int rows, final int diameter, final int offsetX, final int offsetY) {
    super(columns, rows, diameter, offsetX, offsetY, NO_DATA_POINT);
  }

  @Override
  protected WindRoseConcentration[] createGrid(final int columns, final int rows) {
    return new WindRoseConcentration[columns * rows];
  }
}
