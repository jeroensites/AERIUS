/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.calculation;

import java.util.stream.IntStream;

/**
 * Calculates the NO2 exceedance hours or:
 * De grenswaarde voor de uurgemiddelde concentratie stikstofdioxide is 200 μg/m3.
 * Deze grenswaarde mag maximaal 18 maal per jaar worden overschreden.
 */
public final class ExceedanceHoursCalculator {

  private static final double LIMIT = 200.0;
  private static final double[] K = {45.1, 42.4, 41, 39.6, 38.7, 38.5, 38.1, 37.8, 37.7, 37.7, 37.8, 37.9, 37.9, 37.9, 37.6, 37.6, 37.4, 37.4, 37.3};
  private static final double[] M = {2.88, 2.72, 2.58, 2.51, 2.45, 2.38, 2.33, 2.29, 2.25, 2.2, 2.17, 2.13, 2.1, 2.08, 2.06, 2.04, 2.02, 2.0, 1.98};
  private static final double[] NO2_LIMIT = IntStream.range(0, K.length).mapToDouble(i -> (LIMIT - K[i]) / M[i]).toArray();
  private static final int IDX_LAST = K.length - 1;

  private ExceedanceHoursCalculator() {
    // Util class
  }

  /**
   * Calculates the NO2 exceedance hour value for day 19.
   *
   * @param cjNO2 Yearly NO2
   * @return calculated value
   */
  public static double calculate19(final double cjNO2) {
    return K[IDX_LAST] + M[IDX_LAST] * cjNO2;
  }

  /**
   * Calculates the number times the hourly average NO2 exceeds 200 μg/m3
   *
   * @param cjNO2 Yearly NO2
   * @return Number of times the hourly average NO2 exceeds 200 μg/m3
   */
  public static double calculate(final double cjNO2) {
    if (Double.isNaN(cjNO2)) {
      return Double.NaN;
    }

    int idx = 0;
    while (idx < K.length && NO2_LIMIT[idx] < cjNO2) {
      idx++;
    }
    return idx;
  }
}
