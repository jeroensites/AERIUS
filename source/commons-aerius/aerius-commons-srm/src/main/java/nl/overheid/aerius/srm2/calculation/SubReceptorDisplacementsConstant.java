/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.opencl.Constant;
import nl.overheid.aerius.receptor.SubReceptorUtil;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.util.OSUtils;

/**
 * Global constant containing all subreceptor displacements.
 *
 * Will end up as a double2 array with name SUB_RECEPTOR_DISPLACEMENTS.
 */
class SubReceptorDisplacementsConstant implements Constant {

  private static final int BUILDER_CAPACITY = 20000;

  private final ArrayList<AeriusPoint> displacements;

  SubReceptorDisplacementsConstant(final int subReceptorRings, final HexagonZoomLevel zoomLevel1) {
    final SubReceptorUtil<AeriusPoint> subReceptorUtil = new DisplacementSubReceptorUtil(zoomLevel1);
    displacements = subReceptorUtil.determineSubPoints(new AeriusPoint(0, 0), subReceptorRings);
  }

  int getNumberOfSubReceptors() {
    return displacements.size();
  }

  @Override
  public String getValue() {
    final StringBuilder builder = new StringBuilder(BUILDER_CAPACITY);
    builder.append("constant double2 SUB_RECEPTOR_DISPLACEMENTS[");
    builder.append(displacements.size());
    builder.append("] = {");
    builder.append(OSUtils.LNL);
    final List<String> pointParts = new ArrayList<String>();
    for (final AeriusPoint point : displacements) {
      pointParts.add("(double2)(" + point.getX() + ", " + point.getY() + ")");
    }
    builder.append(StringUtils.join(pointParts, "," + OSUtils.LNL));
    builder.append("};");
    return builder.toString();
  }

  private static class DisplacementSubReceptorUtil extends SubReceptorUtil<AeriusPoint> {

    public DisplacementSubReceptorUtil(final HexagonZoomLevel zoomLevel1) {
      super(zoomLevel1);
    }

    @Override
    public AeriusPoint newPoint(final AeriusPoint original) {
      return new AeriusPoint(AeriusPointType.POINT);
    }
  }

}
