/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.base.EmissionReduction;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLMeasure;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicleMeasure;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.util.gml.GMLIdUtil;

/**
 * Reader for NSL measure files.
 *
 * Available columns:
 * <pre>
 * measure_id;jurisdiction_id;label;substance;factor;vehicle_type;speed_type;geometry
 * </pre>
 */
class NSLMeasureFileReader extends AbstractNSLFileReader<NSLMeasureFeature> {

  // @formatter:off
  private enum MeasureColumns implements Columns {
    MEASURE_ID,
    JURISDICTION_ID,
    LABEL,
    SUBSTANCE,
    FACTOR,
    VEHICLE_TYPE,
    SPEED_PROFILE,
    DESCRIPTION,
    GEOMETRY;
    // @formatter:on

    static MeasureColumns safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }
  }

  private final Map<String, NSLMeasureFeature> measureIdCache = new HashMap<>();

  @Override
  protected NSLMeasureFeature parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final NSLMeasureFeature measure;
    final String measureId = getGmlId(MeasureColumns.MEASURE_ID, GMLIdUtil.MEASURE_PREFIX, warnings);

    boolean existingMeasure = false;
    if (measureIdCache.containsKey(measureId)) {
      measure = measureIdCache.get(measureId);
      validateWithExistingMeasure(measure);
      existingMeasure = true;
    } else {
      measure = constructMeasure(measureId);
      measureIdCache.put(measureId, measure);
    }

    final StandardVehicleMeasure vehicleMeasure = determineVehicleMeasure(measure);
    vehicleMeasure.addEmissionReduction(determineReduction(measureId));

    return existingMeasure ? null : measure;
  }

  private NSLMeasureFeature constructMeasure(final String measureId) throws AeriusException {
    final NSLMeasureFeature feature = new NSLMeasureFeature();
    final NSLMeasure measure = new NSLMeasure();
    measure.setGmlId(measureId);
    measure.setJurisdictionId(getInt(MeasureColumns.JURISDICTION_ID));
    measure.setLabel(getString(MeasureColumns.LABEL));
    measure.setDescription(getString(MeasureColumns.DESCRIPTION));
    feature.setProperties(measure);
    feature.setGeometry((Polygon) getGeometry(MeasureColumns.GEOMETRY, geometry -> geometry.type() != GeometryType.POLYGON));
    return feature;
  }

  private StandardVehicleMeasure determineVehicleMeasure(final NSLMeasureFeature feature) throws AeriusException {
    final NSLMeasure measure = feature.getProperties();
    final VehicleType vehicleType = getEnumValue(VehicleType.class, MeasureColumns.VEHICLE_TYPE, measure.getGmlId());
    final RoadSpeedType speedProfile = getEnumValue(RoadSpeedType.class, MeasureColumns.SPEED_PROFILE, measure.getGmlId());
    return measure.getVehicleMeasures().stream()
        .filter(vehicleMeasure -> vehicleMeasure.getVehicleType() == vehicleType && vehicleMeasure.getRoadSpeedType() == speedProfile)
        .findFirst()
        .orElseGet(() -> createNewVehicleMeasure(measure, vehicleType, speedProfile));
  }

  private StandardVehicleMeasure createNewVehicleMeasure(final NSLMeasure measure, final VehicleType vehicleType, final RoadSpeedType speedProfile) {
    final StandardVehicleMeasure vehicleMeasure = new StandardVehicleMeasure();
    vehicleMeasure.setVehicleType(vehicleType);
    vehicleMeasure.setRoadSpeedType(speedProfile);
    measure.getVehicleMeasures().add(vehicleMeasure);
    return vehicleMeasure;
  }

  private EmissionReduction determineReduction(final String id) throws AeriusException {
    final EmissionReduction reduction = new EmissionReduction();
    reduction.setSubstance(getEnumValue(Substance.class, MeasureColumns.SUBSTANCE, id));
    reduction.setFactor(getDouble(MeasureColumns.FACTOR));
    return reduction;
  }

  private void validateWithExistingMeasure(final NSLMeasureFeature existingMeasureFeature) throws AeriusException {
    final NSLMeasure existingMeasure = existingMeasureFeature.getProperties();
    if (existingMeasure.getJurisdictionId() != getInt(MeasureColumns.JURISDICTION_ID)
        || !StringUtils.equals(existingMeasure.getLabel(), getString(MeasureColumns.LABEL))
        || !StringUtils.equals(existingMeasure.getDescription(), getString(MeasureColumns.DESCRIPTION))
        || !GeometryUtil.getGeometry(existingMeasureFeature.getGeometry()).equalsTopo(GeometryUtil.getGeometry(getString(MeasureColumns.GEOMETRY)))) {
      throw new AeriusException(ImaerExceptionReason.SRM_MEASURE_RECORDS_DID_NOT_MATCH, existingMeasure.getGmlId());
    }
  }

  @Override
  Columns[] expectedColumns() {
    return MeasureColumns.values();
  }

  @Override
  Columns safeColumnOf(final String value) {
    return MeasureColumns.safeValueOf(value);
  }

}
