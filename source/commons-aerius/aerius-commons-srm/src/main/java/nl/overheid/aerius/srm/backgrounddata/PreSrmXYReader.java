/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import nl.overheid.aerius.io.AbstractLineColumnReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.OSUtils;

/**
 * Base class for reading pre-processed pre-srm data. This file verifies the header and provides access to reading X and Y coordinates.
 * X and Y coordinates are RDNew.
 *
 * @param <T> the data type the data is stored in.
 */
public abstract class PreSrmXYReader<T> extends AbstractLineColumnReader<T> {

  public enum YearPattern {
    YEAR, PROGNOSE, YEAR_PROGNOSE
  }

  private static final String X_COLUMN = "x-coordinate";
  private static final int X_COLUMN_INDEX = 0;
  private static final String Y_COLUMN = "y-coordinate";
  private static final int Y_COLUMN_INDEX = 1;
  private static final int HEADER_ROWS = 4;
  private static final String METEO_PROGNOSE = "prognose";
  private static final String METEO_YEAR = "year";

  private final String preSrmVersion;
  private final YearPattern yearPattern;
  private final int year;
  private int minX = Integer.MAX_VALUE;
  // this is the x coordinate next to minX. It's used to determine the diameter of a grid cell
  private int minX2 = Integer.MAX_VALUE;
  private int maxX;
  private int minY = Integer.MAX_VALUE;
  private int maxY;

  /**
   * Initializes reader.
   *
   * @param preSrmVersion expected pre-srm version the data is generated with
   * @param yearPattern
   * @param year expected year the data is generated for or -1 if to ignore the year
   */
  public PreSrmXYReader(final String preSrmVersion, final YearPattern yearPattern, final int year) {
    this.preSrmVersion = preSrmVersion;
    this.yearPattern = yearPattern;
    this.year = year;

    if (yearPattern != YearPattern.PROGNOSE && year < 0) {
      throw new IllegalArgumentException("When passing non prognose only year should be set. Year passed:" + year);
    }
  }

  /**
   * Reads the file and returns the results.
   *
   * @param inputStream containing the pre-srm input file content
   * @return results line read results
   * @throws IOException general read problem
   */
  @Override
  public LineReaderResult<T> readObjects(final InputStream inputStream) throws IOException {
    try (InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8.name())) {
      return super.readObjects(reader, HEADER_ROWS);
    }
  }

  @Override
  protected String skipHeaderRow(final BufferedReader reader, final int headerRowCount) throws IOException, AeriusException {
    final String header = super.skipHeaderRow(reader, headerRowCount);
    verifyHeader(header);
    return header;
  }

  /**
   * Verifies if the header contains the expected data.
   *
   * @param header the read header
   * @throws IOException when invalid data in file
   */
  private void verifyHeader(final String header) throws IOException {
    final String[] headerRows = header.split(OSUtils.NL);
    final String foundPreSrmVersion = valueFromHeader(headerRows, 0);
    if (!preSrmVersion.equals(foundPreSrmVersion)) {
      throw new IOException(
          "The presrm version in the file '" + foundPreSrmVersion + "', doesn't match with the version expected by the worker: " + preSrmVersion);
    }
    validateYearPattern(valueFromHeader(headerRows, 1));
    validateMeteoPattern(valueFromHeader(headerRows, 2));
  }

  private void validateYearPattern(final String yearString) throws IOException {
    if (yearPattern == YearPattern.YEAR_PROGNOSE || yearPattern == YearPattern.YEAR) {
      validateYear(yearString);
    }
  }

  private void validateMeteoPattern(final String meteoType) throws IOException {
    if (yearPattern == YearPattern.PROGNOSE || yearPattern == YearPattern.YEAR_PROGNOSE) {
      if (!METEO_PROGNOSE.equals(meteoType)) {
        throw new IOException("Expected meteo info in file to be 'prognose'");
      }
    } else {
      if (!METEO_YEAR.equals(meteoType)) {
        throw new IOException("Expected meteo info in file to be 'year'");
      }
    }
  }

  private void validateYear(final String yearString) throws IOException {
    final int foundYear = Integer.parseInt(yearString);

    if (year > 0 && year != foundYear) {
      throw new IOException("The year version in the file" + foundYear + ", doesn't match with year in the configuration " + year);
    }
  }

  private String valueFromHeader(final String[] header, final int idx) {
    return header.length > idx && header[idx].lastIndexOf(':') > -1 ? header[idx].substring(header[idx].lastIndexOf(':') + 1).trim() : "";
  }

  protected int readX() {
    final int x = getInt(X_COLUMN, X_COLUMN_INDEX);

    if (x < minX) {
      minX2 = minX;
    } else if (x > minX && x < minX2) {
      minX2 = x;
    }
    minX = Math.min(minX, x);
    maxX = Math.max(maxX, x);
    return x;
  }

  protected int readY() {
    final int y = getInt(Y_COLUMN, Y_COLUMN_INDEX);

    minY = Math.min(minY, y);
    maxY = Math.max(maxY, y);
    return y;
  }

  public int getDiameter() {
    return minX2 - minX;
  }

  public int getColumns() {
    return 1 + ((maxX - minX) / getDiameter());
  }

  public int getRows() {
    return 1 + ((maxY - minY) / getDiameter());
  }

  public int getOffsetX() {
    return minX - (getDiameter() / 2);
  }

  public int getOffsetY() {
    return minY - (getDiameter() / 2);
  }
}
