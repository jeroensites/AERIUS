/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.domain;

import java.util.List;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLCorrection;

/**
 * Data object containing all information for a srm2 calculation.
 */
public class SRMInputData<E extends EngineSource> extends EngineInputData<E, AeriusPoint> {

  private static final long serialVersionUID = 2L;

  private List<NSLCorrection> corrections;

  /**
   * Constructor.
   *
   * @param theme Calculation theme this data should be calculated with
   */
  public SRMInputData(final Theme theme) {
    super(theme);
  }

  @Override
  public String toString() {
    return "SRMInputData:" + super.toString();
  }

  /**
   * @return the corrections
   */
  public List<NSLCorrection> getCorrections() {
    return corrections;
  }

  /**
   * @param corrections the corrections to set
   */
  public void setCorrections(final List<NSLCorrection> corrections) {
    this.corrections = corrections;
  }
}
