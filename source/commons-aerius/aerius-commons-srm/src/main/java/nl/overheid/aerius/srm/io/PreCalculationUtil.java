/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.operation.distance.DistanceOp;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.nsl.NSLCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLDispersionLine;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLDispersionLineFeature;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Util for specific specific calculation preparations.
 */
public final class PreCalculationUtil {

  private PreCalculationUtil() {
    // util class
  }

  /**
   * Copies the dispersion line objects to their respective calculation point object
   * because this is how the SRM calculation expects the dispersion lines.
   *
   * It also calculates the geometry and length (distance) of the dispersion line.
   *
   * @param sources sources to calculate the distance to the dispersion line to.
   * @param points calculation points to put the dispersion lines on.
   * @param dispersionLines the dispersion lines
   * @throws AeriusException
   */
  public static void assignDispersionLines(final List<EmissionSourceFeature> sources, final List<AeriusPoint> points,
      final List<NSLDispersionLineFeature> dispersionLines) throws AeriusException {
    final Map<String, NSLCalculationPoint> nslPoints = points.stream()
        .filter(NSLCalculationPoint.class::isInstance)
        .map(NSLCalculationPoint.class::cast)
        .collect(Collectors.toMap(NSLCalculationPoint::getGmlId, point -> point));
    final Map<String, EmissionSourceFeature> roadMap = new HashMap<>();

    roadToMap(sources, roadMap);
    if (!nslPoints.isEmpty()) {
      for (final NSLDispersionLineFeature dispersionLine : dispersionLines) {
        assignAndCalculateGeometry(nslPoints, roadMap, dispersionLine);
      }
    }
  }

  private static void roadToMap(final List<EmissionSourceFeature> sources, final Map<String, EmissionSourceFeature> map) {
    for (final EmissionSourceFeature es : sources) {
      map.put(es.getProperties().getGmlId(), es);
    }
  }

  /**
   * Adds the dispersion line to the calculation point and calculates the geometry/distance of the dispersion line.
   *
   * @param nslPoints
   * @param roadMap
   * @param dispersionLine
   * @throws AeriusException
   */
  private static void assignAndCalculateGeometry(final Map<String, NSLCalculationPoint> nslPoints, final Map<String, EmissionSourceFeature> roadMap,
      final NSLDispersionLineFeature dispersionLineFeature) throws AeriusException {
    final NSLDispersionLine dispersionLine = dispersionLineFeature.getProperties();
    final NSLCalculationPoint nslCalculationPoint = nslPoints.get(dispersionLine.getCalculationPointGmlId());

    if (nslCalculationPoint != null) {
      setDispersionLineGeometry(dispersionLineFeature, roadMap.get(dispersionLine.getRoadGmlId()), nslCalculationPoint);
      nslCalculationPoint.getDispersionLines().add(dispersionLine);
    }
  }

  /**
   * Calculates and sets the geometry and length (distance) of the dispersion line.
   * It calculates the point on the emission source geometry closest to the calculation point.
   *
   * @param dispersionLine dispersion line to set the values on.
   * @param emissionSource source with geometry starts from.
   * @param calculationPoint point the dispersion line ends
   * @throws AeriusException
   */
  static void setDispersionLineGeometry(final NSLDispersionLineFeature dispersionLine, final EmissionSourceFeature emissionSource,
      final NSLCalculationPoint calculationPoint) throws AeriusException {
    if (emissionSource != null) {

      final Geometry sourceGeometry = GeometryUtil.getGeometry(emissionSource.getGeometry());
      final Coordinate[] nearestPoints = DistanceOp.nearestPoints(sourceGeometry, GeometryUtil.getGeometry(calculationPoint));
      final LineString lineString = new LineString();
      final double[][] coordinates = new double[nearestPoints.length][];
      for (int i = 0; i < nearestPoints.length; i++) {
        coordinates[i] = new double[] {nearestPoints[i].x, nearestPoints[i].y};
      }
      lineString.setCoordinates(coordinates);
      dispersionLine.setGeometry(lineString);
    }
  }
}
