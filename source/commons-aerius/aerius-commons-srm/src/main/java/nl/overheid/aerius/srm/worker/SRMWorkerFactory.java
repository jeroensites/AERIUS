/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.config.SRMWorkerConfiguration;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerFactory;

/**
 * Worker Factory for SRM Calculations. Depending on the {@link Theme} passed as input the specific profile calculator is run.
 */
public class SRMWorkerFactory implements WorkerFactory<SRMWorkerConfiguration> {

  private static List<SRM2CalculationOptions> options =
      Arrays.asList(SRMConstants.NSL_SRM2_CALCULATION_OPTIONS, SRMConstants.WNB_SRM2_CALCULATION_OPTIONS);

  @Override
  public SRMWorkerConfiguration createConfiguration(final Properties properties) {
    return new SRMWorkerConfiguration(properties, options);
  }

  @Override
  public <S extends Serializable, T extends Serializable> Worker<S,T> createWorkerHandler(final SRMWorkerConfiguration config,
      final BrokerConnectionFactory factory) throws IOException {
    return (Worker<S, T>) new SRMWorkerHandler(config);
  }

  @Override
  public WorkerType getWorkerType() {
    return WorkerType.ASRM;
  }
}
