/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

import java.nio.DoubleBuffer;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLKernel;

import nl.overheid.aerius.opencl.Connection;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.WindRoseConcentration;
import nl.overheid.aerius.srm.backgrounddata.WindRoseSpeed;

/**
 * Classes implementing this interface handle the substance specific implementation.
 */
interface SubstanceCalculationContext {

  /**
   * Returns the name of the calculation kernel name aka open cl calculation function.
   *
   * @return calculation kernel name
   */
  String getCalculationKernelName();

  /**
   * Returns the name of the summary kernel name aka open cl summary function.
   *
   * @return summary kernel name
   */
  String getSumKernelName();

  /**
   * Returns the number of double values in the worker/result data object.
   *
   * @return number of double values on worker/result
   */
  int getWorkSize();

  /**
   * Returns the number of additional bytes in a single work item used by the specific substance implementation.
   *
   * @return additional number of bytes used
   */
  long getAdditionalBatchSizeInBytes();

  /**
   * Initialize the receptor buffer and additional receptor related buffers.
   *
   * @param connection
   *          OpenCL connection
   * @param calculationKernel
   *          calculation kernel
   * @param sumKernel
   *          summary kernel
   * @param batchSize
   * @param receptorPositionBuffer
   * @param receptorDepositionVelocityBuffer
   * @param receptorWindBuffer
   */
  void initReceptorBuffers(Connection connection, CLKernel calculationKernel, CLKernel sumKernel, int batchSize,
      CLBuffer<DoubleBuffer> receptorPositionBuffer, CLBuffer<DoubleBuffer> receptorDepositionVelocityBuffer,
      CLBuffer<DoubleBuffer> receptorWindBuffer);

  /**
   * Prepares a buffer given a wind rose. This method is called for each receptor point. The data to add is stored in
   * the windrose and is dependent on the substance to calculate. This means the windrose contains substance specific
   * data. There is no information on which receptor this data is for, the implementing method should append the data to
   * the specific buffer.
   *
   * @param windrose
   */
  void prepareBatchWindRoseBuffers(WindRoseConcentration windrose);

  /**
   * Called before a batch is calculated. Use this method to reset the buffers initialized in
   * {@link #prepareBatchWindRoseBuffers(WindRoseSpeed)}.
   *
   * @param connection
   *          OpenCL connection
   */
  void prepareBatch(Connection connection);

  /**
   * Perform the actual calculation in this method.
   *
   * @param connection
   *          OpenCL connection
   * @param calculationKernel
   *          calculation kernel
   * @param sumKernel
   *          summary kernel
   * @param workSize
   * @param actualBatchSize
   */
  void runBatch(Connection connection, CLKernel calculationKernel, CLKernel sumKernel, int workSize, int actualBatchSize);

  /**
   * Stored results from the buffer in the result point. The index points to the position in the buffer.
   *
   * @param aeriusResultPoint
   * @param doubleBuffer
   * @param index
   */
  void downloadBatchResults(AeriusResultPoint aeriusResultPoint, DoubleBuffer doubleBuffer, int index);

  default void setData(final AeriusResultPoint aeriusResultPoint, final EmissionResultKey key, final double value) {
    aeriusResultPoint.setEmissionResult(key, value < 0.0 ? SRMConstants.NO_DATA : value);
  }
}
