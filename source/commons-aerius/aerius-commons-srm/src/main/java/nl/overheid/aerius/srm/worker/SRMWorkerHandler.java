/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm.backgrounddata.PreSRMDataBuilder;
import nl.overheid.aerius.srm.config.SRMWorkerConfiguration;
import nl.overheid.aerius.srm2.domain.SRMInputData;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.Worker;

/**
 * WorkHandler for SRM calculations that directs the calculation to the profile specific implementation.
 */
class SRMWorkerHandler implements Worker<SRMInputData<?>, CalculationResult> {
  private static final Logger LOGGER = LoggerFactory.getLogger(SRMWorkerHandler.class);

  private final NSLWorkerHandler nslWorkerHandler;
  private final WNBWorkHandler pasWorkerHandler;

  SRMWorkerHandler(final SRMWorkerConfiguration config) throws IOException {
    final PreSRMDataBuilder builder = new PreSRMDataBuilder();

    if (config.hasThemeConfig(Theme.WNB)) {
      pasWorkerHandler = new WNBWorkHandler(config, builder.build(config.getThemeConfiguration(Theme.WNB)));
    } else {
      pasWorkerHandler = null;
    }
    if (config.hasThemeConfig(Theme.RBL)) {
      nslWorkerHandler = new NSLWorkerHandler(config, builder.build(config.getThemeConfiguration(Theme.RBL)));
    } else {
      nslWorkerHandler = null;
    }
  }

  @Override
  public CalculationResult run(final SRMInputData input, final WorkerIntermediateResultSender resultSender, final JobIdentifier jobIdentifier)
      throws Exception {
    final Theme profile = input.getTheme();

    if (profile == Theme.RBL && nslWorkerHandler != null) {
      return nslWorkerHandler.run(input, resultSender, jobIdentifier);
    } else if (profile == Theme.WNB && pasWorkerHandler != null) {
      return pasWorkerHandler.run(input, resultSender, jobIdentifier);
    } else {
      LOGGER.error("Unsupported or unconfigured profile passed to srm2 worker: {}", profile);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }
}
