/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.conversion;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.GeometryType;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;

/**
 * Converter class to convert EmissionSource related objects to SRM1 and/or SRM2 road segment objects.
 */
public class EmissionSourceSRMConverter {

  private final List<AbstractSRMConverter<? extends EngineSource>> converters = new ArrayList<>();

  public EmissionSourceSRMConverter() {
    this(null);
  }

  public EmissionSourceSRMConverter(final AbstractSRMConverter<? extends EngineSource> firstConverter) {
    if (firstConverter != null) {
      this.converters.add(firstConverter);
    }
    this.converters.add(new SRM2SegmentSRMConverter());
    this.converters.add(new GenericSegmentSRMConverter());
  }

  public List<EngineSource> convert(final List<EmissionSourceFeature> emissionSources, final List<Substance> substances) throws AeriusException {
    final List<EngineSource> engineSources = new ArrayList<>();

    if (emissionSources != null) {
      for (final EmissionSourceFeature emissionSource : emissionSources) {
        engineSources.addAll(convert(emissionSource, substances));
      }
    }
    return engineSources;
  }

  public List<EngineSource> convert(final EmissionSourceFeature emissionSource, final List<Substance> substances) throws AeriusException {
    final List<EngineSource> converted = new ArrayList<>();

    converted.addAll(convert(emissionSource.getGeometry(), emissionSource.getProperties(), substances));

    return converted;
  }

  /**
   * Copies the {@link EmissionSource} object to {@link SRM2RoadSegment} objects for the given emission. It
   * splits lines into segments with only start and an end point.
   *
   * @param geometry road geometry
   * @param source {@link EmissionSource} object to convert to {@link SRM2RoadSegment} objects
   * @param keys keys to get emission for
   * @return list of road segments
   * @throws AeriusException Thrown if the geometry is not a line.
   */
  public final List<EngineSource> convert(final Geometry geometry, final EmissionSource source, final List<Substance> substances)
      throws AeriusException {
    if (geometry.type() != GeometryType.LINESTRING || !(geometry instanceof LineString)) {
      throw new AeriusException(AeriusExceptionReason.ROAD_GEOMETRY_NOT_ALLOWED);
    }
    final List<EngineSource> result = new ArrayList<>();

    for (final AbstractSRMConverter<? extends EngineSource> converter : converters) {
      if (converter.canConvert(source)) {
        result.addAll(converter.convert((LineString) geometry, source, substances));
        break;
      }
    }
    return result;
  }

}
