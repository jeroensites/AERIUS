/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.domain;

import java.io.Serializable;
import java.util.Set;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.meteo.Meteo;

/**
 * SRM2 calculation options that determine how a SRM2 calculation should be performed.
 */
public class SRM2CalculationOptions implements Serializable {

  private static final long serialVersionUID = 1L;

  public enum Option {
    /**
     * When set loads all background concentration data.
     */
    ALL_BACKGROUND,
    /**
     * When set enable sub receptor calculation for receptors close to a source point.
     */
    CALCULATE_SUBRECEPTORS,
    /**
     * When set calculates deposition.
     */
    CALCULATE_DEPOSITION,
    /**
     * When set calculates srm1 sources with the srm1 algorithm.
     */
    CALCULATE_SRM1,
  }

  private final Theme theme;

  private final String preSRMVersion;
  private final String aeriusPreSRMVersion;
  private final Set<Option> options;
  private final int prognoseYear;
  private final Meteo prognoseBaseMeteo;

  protected SRM2CalculationOptions(final Theme theme, final String preSRMVersion, final String aeriusVersion, final int prognoseYear,
      final Meteo prognoseBaseMeteo, final Set<Option> options) {
    this.theme = theme;
    this.preSRMVersion = preSRMVersion;
    this.aeriusPreSRMVersion = preSRMVersion + '-' + aeriusVersion;
    this.prognoseYear = prognoseYear;
    this.prognoseBaseMeteo = prognoseBaseMeteo;
    this.options = options;
  }

  public Theme getTheme() {
    return theme;
  }

  public String getAeriusPreSRMVersion() {
    return aeriusPreSRMVersion;
  }

  public String getPreSRMVersion() {
    return preSRMVersion;
  }

  public boolean isAllBackground() {
    return options.contains(Option.ALL_BACKGROUND) || isCalculateSRM1();
  }

  public boolean isCalculateDeposition() {
    return options.contains(Option.CALCULATE_DEPOSITION);
  }

  public boolean isSubReceptorCalculation() {
    return options.contains(Option.CALCULATE_SUBRECEPTORS);
  }

  public boolean isCalculateSRM1() {
    return options.contains(Option.CALCULATE_SRM1);
  }

  public int getPrognoseYear() {
    return prognoseYear;
  }

  public Meteo getPrognoseBaseMeteo() {
    return prognoseBaseMeteo;
  }

  /**
   * Returns the yearly meteo if available, prognose meteo otherwise. This is determined as follows:
   * If the requested year is beyond the current prognoseYear, return the prognose meteo.
   * The prognoseYear indicates up until which year yearly meteo is available.
   *
   * @param year year to request meteo for
   * @return Meteo
   */
  public Meteo getMeteoForYear(final int year) {
    if (year > prognoseYear) {
      return prognoseBaseMeteo;
    } else {
      return new Meteo(year);
    }
  }
}
