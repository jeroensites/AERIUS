/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import nl.overheid.aerius.shared.domain.v2.nsl.NSLCorrection;

/**
 * Flattens a list of corrections ({@link NSLCorrection}) and writes it in a csv file.
 */
public final class NSLCorrectionWriter extends AbstractNSLWriter<NSLCorrectionWriter.Data> {

  protected static class Data {

    final int scale;
    final NSLCorrection correction;

    Data(final int scale, final NSLCorrection correction) {
      this.scale = scale;
      this.correction = correction;
    }

  }

  private static class Column extends GenericColumn<Data> {

    public Column(final String header, final Function<Data, String> function) {
      super(header, function);
    }

  }

  private static class CorrectionColumn extends Column {

    public CorrectionColumn(final String header, final Function<NSLCorrection, String> function) {
      super(header, data -> function.apply(data.correction));
    }

  }

  private static final Column[] COLUMNS = new Column[] {
      new CorrectionColumn("label", correction -> correction.getLabel()),
      new CorrectionColumn("jurisdiction_id", correction -> correction.getJurisdictionId() == null
          ? ""
          : String.valueOf(correction.getJurisdictionId())),
      new CorrectionColumn("substance", correction -> correction.getSubstance().getName()),
      new Column("value", data -> roundedValue(data.scale, data.correction.getValue())),
      new CorrectionColumn("calculation_point_id", correction -> correction.getCalculationPointGmlId()),
      new CorrectionColumn("description", correction -> correction.getDescription()),
  };

  private static final List<GenericColumn<Data>> DEFAULT_COLUMNS = Collections.unmodifiableList(Arrays.asList(COLUMNS));

  private final int scale;

  public NSLCorrectionWriter(final int scale) {
    this.scale = scale;
  }

  @Override
  List<GenericColumn<Data>> columns() {
    return DEFAULT_COLUMNS;
  }

  /**
   * write row of data to the writer (does not close the writer);
   */
  public void writeRow(final Writer writer, final NSLCorrection correction) throws IOException {
    final Data data = new Data(scale, correction);
    super.writeRow(writer, data);
  }

  public void write(final File outputFile, final List<NSLCorrection> corrections) throws IOException {
    try (final Writer writer = Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8)) {
      writeHeader(writer);
      for (final NSLCorrection correction : corrections) {
        writeRow(writer, correction);
      }
    }
  }

}
