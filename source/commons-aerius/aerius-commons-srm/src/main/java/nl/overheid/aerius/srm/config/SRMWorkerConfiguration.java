/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.config;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.worker.WorkerConfiguration;

/**
 * SRM Worker configuration options.
 */
public class SRMWorkerConfiguration extends WorkerConfiguration {

  private static final String SRM_WORKER_PREFIX = "srm";

  public static class SRMProfileConfiguration extends WorkerConfiguration {
    private static final String SRM_WORKER_SRM_ROOT = "root";
    private static final String SRM_WORKER_PRESRM_LU_DIRECTORY_PREFIX = "presrm-lu-";
    private static final String SRM_WORKER_AERIUS_PRESRM_DIRECTORY_PREFIX = "aerius-presrm-";
    private static final String SRM2_WORKER_DEPOSITION_VELOCITY_DIRECTORY = "depositionvelocity";
    private static final String SRM_WORKER_WIND_VELDEN_DIRECTORY_PREFIX = "windvelden-";
    private final SRM2CalculationOptions options;

    public SRMProfileConfiguration(final Properties properties, final SRM2CalculationOptions opt) {
      super(properties, SRM_WORKER_PREFIX + '.' + themeConfigPath(opt));
      this.options = opt;
    }

    private static String themeConfigPath(final SRM2CalculationOptions opt) {
      final Theme theme = opt.getTheme();
      // For backward compatibility keep configuration parameter at nsl.
      return theme == Theme.RBL ? "nsl" : theme.getKey();
    }

    @Override
    protected List<String> getValidationErrors() {
      final List<String> reasons = new ArrayList<>();

      if (validateDirectoryProperty(SRM_WORKER_SRM_ROOT, reasons, false)) {
        if (!getAeriusPreSRMDirectory().isDirectory()) {
          reasons.add("Missing AERIUS PreSrm directory:" + getAeriusPreSRMDirectory());
        }
        if (!getLandUseDirectory().isDirectory()) {
          reasons.add("Missing PreSrm directory:" + getLandUseDirectory());
        }
        if (options.isCalculateSRM1()) {
          if (!getWindVeldenDirectory().isDirectory()) {
            reasons.add("Missing windvelden directory:" + getWindVeldenDirectory());
          }
        }
        if (options.isCalculateDeposition()) {
          if (!getDepositionVelocityFile().exists()) {
            reasons.add("Missing deposition velocity file:" + getDepositionVelocityFile());
          }
        }
      }
      return reasons;
    }

    public SRM2CalculationOptions getOptions() {
      return options;
    }

    public File getAeriusPreSRMDirectory() {
      final String srmRoot = getProperty(SRM_WORKER_SRM_ROOT);

      return srmRoot == null ? null : new File(srmRoot, SRM_WORKER_AERIUS_PRESRM_DIRECTORY_PREFIX + options.getAeriusPreSRMVersion());
    }

    public File getLandUseDirectory() {
      final String srmRoot = getProperty(SRM_WORKER_SRM_ROOT);
      return srmRoot == null ? null : new File(srmRoot, SRM_WORKER_PRESRM_LU_DIRECTORY_PREFIX + options.getPreSRMVersion());
    }

    public File getDepositionVelocityFile() {
      final String srmRoot = getProperty(SRM_WORKER_SRM_ROOT);

      return srmRoot == null ? null : new File(new File(srmRoot, SRM2_WORKER_DEPOSITION_VELOCITY_DIRECTORY), SRMConstants.DEPOSITION_VELOCITY_FILE);
    }

    public File getWindVeldenDirectory() {
      final String srmRoot = getProperty(SRM_WORKER_SRM_ROOT);

      return srmRoot == null ? null : new File(srmRoot, SRM_WORKER_WIND_VELDEN_DIRECTORY_PREFIX + SRMConstants.WINDVELDEN_VERSION);
    }

    public boolean isConfigured() {
      return getAeriusPreSRMDirectory() != null;
    }
  }

  private static final String SRM2_WORKER_CONNECTION_TIMEOUT = "connection_timeout";
  private static final String SRM2_WORKER_FORCE_CPU = "force_cpu";

  private final Map<Theme, SRMProfileConfiguration> themeConfig = new EnumMap<>(Theme.class);

  public SRMWorkerConfiguration(final Properties properties, final List<SRM2CalculationOptions> options) {
    super(properties, SRM_WORKER_PREFIX);
    options.forEach(opt -> themeConfig.put(opt.getTheme(), new SRMProfileConfiguration(properties, opt)));
  }

  public SRMProfileConfiguration getThemeConfiguration(final Theme theme) {
    return themeConfig.get(theme);
  }

  public boolean hasThemeConfig(final Theme theme) {
    return themeConfig.containsKey(theme) && themeConfig.get(theme).isConfigured();
  }

  @Override
  protected List<String> getValidationErrors() {
    if (getProcesses() > 0) {
      return themeConfig.entrySet().stream().filter(e -> hasThemeConfig(e.getKey()))
          .flatMap(e -> e.getValue().getValidationErrors().stream()).collect(Collectors.toList());
    } else {
      return Collections.emptyList();
    }
  }

  public boolean isForceCpu() {
    return getPropertyBooleanSafe(SRM2_WORKER_FORCE_CPU);
  }

  public long getConnectionTimeout() {
    return getPropertyIntSafe(SRM2_WORKER_CONNECTION_TIMEOUT);
  }
}
