/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.srm.backgrounddata.LandUseData.LandUseType;

/**
 * Data object containing the PRE-SRM calculated data.
 */
public class PreSRMData {
  private static final Logger LOGGER = LoggerFactory.getLogger(PreSRMData.class);

  private final Map<Integer, HasGridValue<WindRoseConcentration>> windRoseConcentrationYearGrid = new HashMap<>();
  private final Map<Integer, HasGridValue<WindRoseConcentration>> windRoseConcentrationPrognoseGrid = new HashMap<>();
  private final Map<Integer, WindRoseSpeedGrid> windRoseSpeedYearGrid = new HashMap<>();
  private WindRoseSpeedGrid windRoseSpeedPrognoseGrid;
  private final Map<Integer, WindSpeedGrid> windSpeedYearGrid = new HashMap<>();
  private WindSpeedGrid windSpeedPrognoseGrid;
  private LandUseData landUseData;
  private Map<Substance, DepositionVelocityMap> depositionVelocityMaps = new EnumMap<>(Substance.class);

  public PreSRMData() {
  }

  void addWindSpeedGrid(final int year, final WindSpeedGrid table) {
    windSpeedYearGrid.put(year, table);
  }

  void addWindRoseSpeedTable(final int year, final WindRoseSpeedGrid table) {
    windRoseSpeedYearGrid.put(year, table);
  }

  public WindSpeedGrid getWindSpeedGrid(final Meteo meteo) throws AeriusException {
    if (meteo.isSingleYear()) {
      final int meteoYear = meteo.getStartYear();
      if (windSpeedYearGrid.containsKey(meteoYear)) {
        return windSpeedYearGrid.get(meteoYear);
      }
      LOGGER.error("No windspeed table for year {}", meteoYear);
    } else {
      return windSpeedPrognoseGrid;
    }
    throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
  }

  public WindRoseSpeedGrid getWindRoseSpeedGrid(final Meteo meteo) throws AeriusException {
    if (meteo.isSingleYear()) {
      final int meteoYear = meteo.getStartYear();
      if (windRoseSpeedYearGrid.containsKey(meteoYear)) {
        return windRoseSpeedYearGrid.get(meteoYear);
      }
      LOGGER.error("No windspeed rose table for year {}", meteoYear);
    } else {
      return windRoseSpeedPrognoseGrid;
    }
    throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
  }

  void setWindSpeedPrognoseGrid(final WindSpeedGrid windSpeedPrognoseGrid) {
    this.windSpeedPrognoseGrid = windSpeedPrognoseGrid;
  }

  void setWindRoseSpeedPrognoseGrid(final WindRoseSpeedGrid windRoseSpeedPrognoseGrid) {
    this.windRoseSpeedPrognoseGrid = windRoseSpeedPrognoseGrid;
  }

  void addWindRoseConcentrationGrid(final int year, final HasGridValue<WindRoseConcentration> grid) {
    windRoseConcentrationYearGrid.put(year, grid);
  }

  void addWindRoseConcentrationPrognoseGrid(final int year, final HasGridValue<WindRoseConcentration> grid) {
    windRoseConcentrationPrognoseGrid.put(year, grid);
  }

  public HasGridValue<WindRoseConcentration> getWindRoseConcentrationGrid(final Meteo meteo, final int year) throws AeriusException {
    final Map<Integer, HasGridValue<WindRoseConcentration>> grid;
    final int gridYear;

    if (meteo.isSingleYear()) {
      gridYear = meteo.getStartYear();
      grid = windRoseConcentrationYearGrid;
    } else {
      grid = windRoseConcentrationPrognoseGrid;
      gridYear = year;
    }
    final HasGridValue<WindRoseConcentration> yearGrid = grid.get(gridYear);

    if (yearGrid == null) {
      final String simpleName = meteo.getClass().getSimpleName();
      LOGGER.error("No windspeed rose concentration for {} year {}", simpleName, gridYear);
      throw new AeriusException(AeriusExceptionReason.SRM2_NO_PRESRM_DATA_FOR_YEAR, year);
    }
    return yearGrid;
  }

  public MapData getLandUse(final LandUseType type) {
    return landUseData.getMapData(type);
  }

  /**
   * @param x The x coordinate of the source.
   * @param y The y coordinate of the source.
   * @return the landuse value at the given location
   *
   * {@link LandUseData#getLandUseValueSource(double, double)}.
   */
  public double getLandUseValueSource(final double x, final double y) {
    return landUseData.getLandUseValueSource(x, y);
  }

  void setLandUseData(final LandUseData landUseData) {
    this.landUseData = landUseData;
  }

  public DepositionVelocityMap getDepositionVelocityMap(final Substance substance) {
    return depositionVelocityMaps.get(substance);
  }

  void setDepositionVelocityMaps(final Map<Substance, DepositionVelocityMap> depositionVelocityMaps) {
    this.depositionVelocityMaps = depositionVelocityMaps;
  }
}
