/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm2.calculation.SRM2Calculator;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;

/**
 * Calculates WetNB SRM2.
 */
class WNBSrm2Calculator {

  private static final EnumSet<EmissionResultKey> TOTAL_NOX_CONCENTRATION =
      EnumSet.of(EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NOX_DEPOSITION);

  private final SRM2Calculator srm2Calculator;
  private final List<Substance> substances;
  private final int year;
  private final Meteo meteo;
  private final EnumSet<EmissionResultKey> erks;
  private final Collection<AeriusPoint> receptors;
  private final boolean noNOxConcentration;

  public WNBSrm2Calculator(final SRM2Calculator srm2Calculator, final List<Substance> substances, final int year,
      final Meteo meteo, final EnumSet<EmissionResultKey> erks, final Collection<AeriusPoint> receptors) {
    this.srm2Calculator = srm2Calculator;
    this.substances = substances;
    this.year = year;
    this.meteo = meteo;
    this.erks = erks;
    noNOxConcentration = !erks.contains(EmissionResultKey.NOX_CONCENTRATION);
    this.receptors = receptors;
  }

  public CalculationResult calculate(final Map<Integer, Collection<SRM2RoadSegment>> sources) throws AeriusException {
    if (sources.size() > 1 && erks.contains(EmissionResultKey.NOX_DEPOSITION)) {
      return calculateWithRecalculate(sources);
    } else {
      return calculatePlain(sources, erks);
    }
  }

  /**
   * Calculates SRM2 outputs only for sectors specific. This is when there is only 1 sector in the input
   * or can be used to calculate the sector specific results for multiple sector inputs.
   *
   * @param sources sources to calculate
   * @param erks EmissionResultKeys to calculate for.
   * @return calculation result
   * @throws AeriusException
   */
  private CalculationResult calculatePlain(final Map<Integer, Collection<SRM2RoadSegment>> sources, final EnumSet<EmissionResultKey> erks)
      throws AeriusException {
    final CalculationResult calculationResult = new CalculationResult(CalculationEngine.ASRM2);

    for (final Entry<Integer, Collection<SRM2RoadSegment>> source : sources.entrySet()) {
      calculationResult.put(source.getKey(), srm2Calculator.calculate(year, meteo, substances, erks, source.getValue(), receptors));
    }
    return calculationResult;
  }

  /**
   * Calculates multiple sector SRM2 input. NOx Deposition is calculated is recalculated by multiplying the factor of sector specific
   * NOX concentration divided by the total (all sectors) NOX concentration.
   *
   * @param sources sources to calculate
   * @return calculation result
   * @throws AeriusException
   */
  private CalculationResult calculateWithRecalculate(final Map<Integer, Collection<SRM2RoadSegment>> sources) throws AeriusException {
    final EnumSet<EmissionResultKey> recalculateErks = EnumSet.copyOf(erks);
    recalculateErks.add(EmissionResultKey.NOX_CONCENTRATION); // make sure nox concentration is calculated.
    recalculateErks.remove(EmissionResultKey.NOX_DEPOSITION);
    final CalculationResult calculationResult = calculatePlain(sources, recalculateErks);
    final Map<Integer, AeriusResultPoint> totalResults = srm2Calculator
        .calculate(year, meteo, substances, TOTAL_NOX_CONCENTRATION, toTotalSources(sources), receptors)
        .parallelStream()
        .collect(Collectors.toMap(AeriusResultPoint::getId, Function.identity()));

    reCalculateDepositions(calculationResult, totalResults);
    return calculationResult;
  }

  private List<SRM2RoadSegment> toTotalSources(final Map<Integer, Collection<SRM2RoadSegment>> sources) {
    return sources.entrySet().stream().flatMap(e -> e.getValue().stream()).collect(Collectors.toList());
  }

  private void reCalculateDepositions(final CalculationResult calculationResult, final Map<Integer, AeriusResultPoint> totalResults) {
    for (final Entry<Integer, List<AeriusResultPoint>> sectorResult : calculationResult.getResults().entrySet()) {
      for (int i = 0; i < sectorResult.getValue().size(); i++) {
        final AeriusResultPoint sectorResultPoint = sectorResult.getValue().get(i);
        final AeriusResultPoint totalRPoint = totalResults.get(sectorResultPoint.getId());
        final double totalConcentration = totalRPoint.getEmissionResult(EmissionResultKey.NOX_CONCENTRATION);
        final double sectorDeposition;

        if (Double.isNaN(sectorResultPoint.getEmissionResult(EmissionResultKey.NOX_CONCENTRATION))) {
          sectorDeposition = Double.NaN;
        } else if (totalConcentration > 0.0) {
          sectorDeposition = totalRPoint.getEmissionResult(EmissionResultKey.NOX_DEPOSITION) *
              (sectorResultPoint.getEmissionResult(EmissionResultKey.NOX_CONCENTRATION) / totalConcentration);
        } else {
          sectorDeposition = 0.0;
        }
        sectorResultPoint.setEmissionResult(EmissionResultKey.NOX_DEPOSITION, sectorDeposition);
        if (noNOxConcentration) {
          sectorResultPoint.unSetEmissionResult(EmissionResultKey.NOX_CONCENTRATION);
        }
      }
    }
  }
}
