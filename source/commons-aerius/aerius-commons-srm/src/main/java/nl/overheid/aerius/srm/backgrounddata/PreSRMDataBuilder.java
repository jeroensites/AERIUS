/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.config.SRMWorkerConfiguration.SRMProfileConfiguration;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;

/**
 * Builder class to load all pre-srm related data.
 * This includes:
 * <ul>
 * <li>Pre-processed Wind Rose factors and speeds</li>
 * <li>Pre-processed Wind Rose concentration O3 and NH3 background for specific years</li>
 * <li>Land use</li>
 * <li>Deposition velocity data</li>
 * </ul>
 */
public class PreSRMDataBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(PreSRMDataBuilder.class);

  public PreSRMData build(final SRMProfileConfiguration config) throws IOException {
    final File preSRMDirectory = config.getAeriusPreSRMDirectory().getAbsoluteFile();
    final SRM2CalculationOptions options = config.getOptions();
    final PreSRMData data = new PreSRMData();

    loadWindRoseSpeedData(config, preSRMDirectory, options, data);
    loadWindSpeedData(config.getWindVeldenDirectory(), options, data);
    loadConcentrationData(config, preSRMDirectory, data);
    loadLandUseData(data, config.getLandUseDirectory().getAbsoluteFile());
    if (options.isCalculateDeposition()) {
      loadDepositionVelocities(data, config.getDepositionVelocityFile());
    }
    return data;
  }

  private void loadWindRoseSpeedData(final SRMProfileConfiguration config, final File preSRMDirectory, final SRM2CalculationOptions options,
      final PreSRMData data) throws FileNotFoundException, IOException {
    final WindRoseSpeedDataBuilder windRoseSpeedBuilder = new WindRoseSpeedDataBuilder(config.getOptions());

    windRoseSpeedBuilder.loadWindRoseSpeedPrognose(data, preSRMDirectory);
    windRoseSpeedBuilder.loadWindRoseByYear(data, preSRMDirectory);
  }

  private void loadWindSpeedData(final File windDirectory, final SRM2CalculationOptions options, final PreSRMData data)
      throws FileNotFoundException, IOException {
    if (options.isCalculateSRM1()) {
      final WindSpeedDataBuilder builder = new WindSpeedDataBuilder();

      builder.loadWindSpeedPrognose(data, windDirectory);
      builder.loadWindSpeedByYear(data, windDirectory);
    }
  }

  private void loadConcentrationData(final SRMProfileConfiguration config, final File preSRMDirectory, final PreSRMData data) throws IOException {
    final WindRoseConcentrationBuilder windRoseConBuilder = new WindRoseConcentrationBuilder();

    windRoseConBuilder.loadWindRoseConcentration(data, preSRMDirectory, config.getOptions());
  }

  private void loadLandUseData(final PreSRMData data, final File landUseDirectory) throws IOException {
    LOG.info("Loading landUse data from: {}.", landUseDirectory);
    data.setLandUseData(LandUseDataBuilder.loadZ0(landUseDirectory));
  }

  private void loadDepositionVelocities(final PreSRMData data, final File depositionvelocityFilepath) throws IOException {
    LOG.info("Loading deposition velocity data from {}.", depositionvelocityFilepath);
    try (final InputStream is = new FileInputStream(depositionvelocityFilepath)) {
      data.setDepositionVelocityMaps(DepositionVelocityReader.read(is, SRMConstants.HEX_HOR, SRMConstants.getReceptorUtil()));
    }
  }
}
