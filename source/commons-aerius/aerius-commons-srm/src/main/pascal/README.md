This tool generates the preprocessed pre-srm files for the AERIUS srm2 worker.
pre-srm is used to generate wind (air), O3, NO2, PM10, PM24 NH3 background
values per 1km area for a given coordinate set.

This tool is written in free pascal and can be build with the open source pascal
ide Lazarus.

To build the tool and interface files for pre-srm are needed. The files included
are from the latest pre-srm version supported. With a new version of pre-srm it
needs to be checked if there are any changes in the interface.

To run the tool the pre_srm.dll (or pres_srm_64.dll) and depac.dll is needed.
Place the dll files from the specific pre-srm version in the directoy of the
srm2_preprocess.exe 

To generate the files the configuration needs to be set in a .ini file. 
In the brach directory a set of ini files as well as windows batch scripts
are available to generate the needed files. Because generation only uses
1 cpu core there are multiple scripts that can be started concurently to
speed up generation. Adapt the scripts to get the best performance.

# Files
The files generated are for:
1) Concentration
2) Wind factor
3) Schiphol correction factor
4) Ijmond correction factor

For each of those files there are 2 type of files generated
1) Ending `-prognose`: Uses 10-year meteo
2) Ending `-year`: Use specific year meteo
The specfic meteo is only used for years in the past.
This means when a new year is passed for that year the configuration
and batch need to be updated to generate that new year.

# Steps for generating files
1) Install Lazarus
2) Download and Intall the needed pre-srm version
3) Copy the pre-srm .pas files from the pres-srm Delphi directory to this pascal directory.
4) Copy the pre-srm .dll files from the pres-srm installation directoy to this pascal directory.
5) Start lazarus and compile the executable (Ctrl-F9)
6) Set the environment variable PRESRM_DATAPATH to the presrm data directory
  (should end with slash /)
7) Check and update if the batch and ini files need to be updated for a new year
8) Run the batch scripts
