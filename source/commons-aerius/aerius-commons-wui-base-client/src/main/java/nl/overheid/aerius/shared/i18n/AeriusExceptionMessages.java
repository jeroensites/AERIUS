/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

import com.google.gwt.i18n.client.ConstantsWithLookup;

/**
 * Interface for error codes representing errors occurred on the server and
 * passed to the client.
 */
public interface AeriusExceptionMessages extends ConstantsWithLookup {

  /**
   * Internal error, try later again. Recoverable error. Possible cause
   * temporary unavailability of a service.
   */
  String e666();

  /**
   * Internal error, contact help desk. When this message is shown an error
   * occurred on the server that was very likely caused by a bug.
   */
  String e667();

  /**
   * Internal error, contact help desk. When this message is shown an error
   * occurred on the server we are missing a required configuration in the database for this product.
   */
  String e668();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CALCULATION_NO_SOURCES}.
   */
  String e1001();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#SOURCE_VALIDATION_FAILED}.
   */
  String e1002();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#LIMIT_SOURCES_EXCEEDED}.
   */
  String e1003();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#LIMIT_LINE_LENGTH_EXCEEDED}.
   */
  String e1004();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#LIMIT_POLYGON_SURFACE_EXCEEDED}.
   */
  String e1005();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#INLAND_SHIPPING_SHIP_TYPE_NOT_ALLOWED}.
   */
  String e1006();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#SHIPPING_ROUTE_GEOMETRY_NOT_ALLOWED}.
   */
  String e1007();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#GEOCODER_ERROR}.
   */
  String e1008();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CALCULATION_TO_COMPLEX}.
   */
  String e1009();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#ROAD_GEOMETRY_NOT_ALLOWED}.
   */
  String e1010();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CALCULATION_PAA_PROPOSED_SITUATION_MISSING}.
   */
  String e1011();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#LIMIT_LINE_LENGTH_ZERO}.
   */
  String e1012();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#LIMIT_POLYGON_SURFACE_ZERO}.
   */
  String e1013();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#INLAND_SHIPPING_WATERWAY_INCONCLUSIVE}.
   */
  String e1014();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#INLAND_SHIPPING_WATERWAY_NO_DIRECTION}.
   */
  String e1015();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#SHIPPING_INVALID_SECTOR}.
   */
  String e1016();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CALCULATION_DUPLICATE_POINT_IDS}.
   */
  String e1017();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#HOURS_EXCEEDING_HOURS_IN_YEAR}.
   */
  String e1020();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#CUSTOM_DIURNAL_VARIATION_TYPE_UNKNOWN}.
   */
  String e1023();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#CUSTOM_DIURNAL_VARIATION_INVALID_COUNT}.
   */
  String e1024();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#CUSTOM_DIURNAL_VARIATION_INVALID_SUM}.
   */
  String e1025();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#MOBILE_SOURCE_MISSING_LITER_FUEL}.
   */
  String e1026();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#MOBILE_SOURCE_MISSING_OPERATING_HOURS}.
   */
  String e1027();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#MOBILE_SOURCE_MISSING_LITER_ADBLUE}.
   */
  String e1028();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#MOBILE_SOURCE_HIGH_ADBLUE_FUEL_RATIO}.
   */
  String e1029();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_FILE_NOT_SUPPLIED}.
   */
  String e5001();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_FILE_COULD_NOT_BE_READ}.
   */
  String e5002();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#FAULTY_REQUEST}.
   */
  String e5003();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_FILE_UNSUPPORTED}.
   */
  String e5004();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_FILE_TYPE_NOT_ALLOWED}.
   */
  String e5005();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_REQUIRED_ID_MISSING}.
   */
  String e5006();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_DUPLICATE_ENTRY}.
   */
  String e5007();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_NO_SOURCES_PRESENT}.
   */
  String e5008();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_NO_RESULTS_PRESENT}.
   */
  String e5009();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_NO_CALCULATION_POINTS_PRESENT}.
   */
  String e5010();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_CALCULATION_POINTS_PRESENT}.
   */
  String e5013();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_NO_VALID_RECEIVED_DATE}.
   */
  String e5011();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORTED_FILE_NOT_FOUND}.
   */
  String e5012();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IO_EXCEPTION_UNKNOWN}.
   */
  String e5050();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IO_EXCEPTION_NUMBER_FORMAT}.
   */
  String e5051();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IO_EXCEPTION_NOT_ENOUGH_FIELDS}.
   */
  String e5052();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#BRN_WITHOUT_SUBSTANCE}.
   */
  String e5101();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#BRN_SUBSTANCE_NOT_SUPPORTED}
   */
  String e5102();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_VALIDATION_FAILED}.
   */
  String e5201();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_GEOMETRY_INVALID}.
   */
  String e5202();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_ENCODING_INCORRECT}.
   */
  String e5203();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_GEOMETRY_INTERSECTS}.
   */
  String e5204();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_GEOMETRY_NOT_PERMITTED}.
   */
  String e5205();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_GEOMETRY_UNKNOWN}.
   */
  String e5206();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_RAV_CODE}.
   */
  String e5207();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_MOBILE_SOURCE_CODE}.
   */
  String e5208();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_SHIP_CODE}.
   */
  String e5209();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_PLAN_CODE}.
   */
  String e5210();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_GENERIC_PARSE_ERROR}.
   */
  String e5211();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_PARSE_ERROR}.
   */
  String e5212();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_VERSION_NOT_SUPPORTED}.
   */
  String e5213();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_CREATION_FAILED}.
   */
  String e5214();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GEOMETRY_INVALID}.
   */
  String e5215();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_PAS_MEASURE_CODE}.
   */
  String e5216();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_INVALID_PAS_MEASURE_CATEGORY}.
   */
  String e5217();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_INVALID_CATEGORY_MATCH}.
   */
  String e5218();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_INVALID_ROAD_CATEGORY_MATCH}.
   */
  String e5219();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_ID_NOT_UNIQUE}.
   */
  String e5220();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_ROAD_CATEGORY}.
   */
  String e5221();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_METADATA_EMPTY}.
   */
  String e5222();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_VERSION_NOT_LATEST}.
   */
  String e5223();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_SOURCE_NO_EMISSION}.
   */
  String e5224();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_SRS_NAME_UNSUPPORTED}.
   */
  String e5225();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM2_SOURCE_NO_VEHICLES}.
   */
  String e5226();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_WATERWAY_CODE}.
   */
  String e5227();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_INVALID_SHIP_FOR_WATERWAY}.
   */
  String e5228();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#INLAND_WATERWAY_NOT_SET}.
   */
  String e5229();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM2_SOURCE_NEGATIVE_VEHICLES}.
   */
  String e5230();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_ROAD_SEGMENT_POSITION_NOT_FRACTION}.
   */
  String e5231();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_UNKNOWN_FARMLAND_ACTIVITY_CODE}.
   */
  String e5232();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_MOBILE_SOURCE_IDLE_NOT_SET}.
   */
  String e5233();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#GML_OFF_ROAD_CATEGORY_CONVERTED}.
   */
  String e5234();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#PAA_VALIDATION_FAILED}.
   */
  String e5301();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#PAA_EXPORT_TIMEOUT}.
   */
  String e5302();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#ZIP_WITHOUT_USABLE_FILES}.
   */
  String e5401();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#ZIP_TOO_MANY_USABLE_FILES}.
   */
  String e5402();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#COHESION_DUPLICATE_SOURCE_IDS}.
   */
  String e5501();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#COHESION_DUPLICATE_POINT_IDS}.
   */
  String e5502();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#COHESION_DUPLICATE_MEASURE_IDS}.
   */
  String e5503();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#COHESION_DUPLICATE_DISPERSION_LINES}.
   */
  String e5504();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#COHESION_REFERENCE_DISPERSION_LINE_MISSING_ROAD}.
   */
  String e5511();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#COHESION_REFERENCE_DISPERSION_LINE_MISSING_POINT}.
   */
  String e5512();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#COHESION_REFERENCE_CORRECTION_MISSING_POINT}.
   */
  String e5513();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#COHESION_DISPERSION_LINE_NOT_PERPENDICULAR}.
   */
  String e5514();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#COHESION_ROAD_MISSING_DISPERSION_LINE}.
   */
  String e5515();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#COHESION_REFERENCE_MISSING_BUILDING}.
   */
  String e5521();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#OPS_INTERNAL_EXCEPTION}.
   */
  String e6101();

  /**
   *
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#OPS_INPUT_VALIDATION}.
   */
  String e6102();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM2IO_EXCEPTION_NO_ROAD_PROPERTIES}.
   */
  String e6201();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM2_TO_MANY_ROAD_SEGMENTS}.
   */
  String e6202();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM2_NO_PRESRM_DATA_FOR_YEAR}.
   */
  String e6203();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM2_MISSING_COLUMN_HEADER}.
   */
  String e6204();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM2_INCORRECT_EXPECTED_VALUE}.
   */
  String e6205();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM2_INCORRECT_WKT_VALUE}.
   */
  String e6206();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#NSL_LEGACY_FILESUPPORT_WILL_BE_REMOVED}.
   */
  String e6207();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM2_SOURCE_TUNNEL_FACTOR_ZERO}.
   */
  String e6209();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM_LEGACY_INVALID_ROAD_TYPE}.
   */
  String e6211();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM_LEGACY_INVALID_SPEED_TYPE}.
   */
  String e6212();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM_LEGACY_INVALID_TREE_FACTOR}.
   */
  String e6213();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM_NO_MONITOR_SRM2_DATA_FOR_YEAR}.
   */
  String e6214();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#SRM_MEASURE_RECORDS_DID_NOT_MATCH}.
   */
  String e6215();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CSV_INCORRECT_ENUM_VALUE}.
   */
  String e6216();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CSV_ID_ADJUSTED}.
   */
  String e6217();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#SRM_UNSUPPORTED_METEO}.
   */
  String e6218();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#ADMS_INTERNAL_EXCEPTION}.
   */
  String e6301();

  /**
   *
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#ADMS_INPUT_VALIDATION}.
   */
  String e6302();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#REQUEST_ALREADY_EXISTS}.
   */
  String e20001();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#PERMIT_UNKNOWN}.
   */
  String e20002();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#PERMIT_ALREADY_UPDATED}.
   */
  String e20004();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#PERMIT_DOES_NOT_FIT}.
   */
  String e20005();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#PERMIT_DELETE_ACTIVE_NO_AUTHORIZATION}.
   */
  String e20006();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#PERMIT_DEQUEUE_NO_AUTHORIZATION}.
   */
  String e20007();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#PERMIT_ENQUEUE_NO_AUTHORIZATION}.
   */
  String e20008();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#MELDING_DOES_NOT_FIT}.
   */
  String e20011();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#MELDING_ABOVE_PERMIT_THRESHOLD}.
   */
  String e20013();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#MELDING_NOT_VIA_CALCULATOR}.
   */
  String e20016();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#MELDING_ATTACHMENTS_FOR_AUTHORIZATION_TO_BIG}.
   */
  String e20017();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#PRIORITY_PROJECT_UNKNOWN}.
   */
  String e20018();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#PRIORITY_SUBPROJECT_UNKNOWN}.
   */
  String e20019();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#MELDING_UNKNOWN}.
   */
  String e20020();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#PRIORITY_PROJECT_DOES_NOT_FIT}.
   */
  String e20022();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#REQUEST_INVALID_REFERENCE}.
   */
  String e20023();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#PRIORITY_SUBPROJECT_DOES_NOT_FIT}.
   */
  String e20024();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#PRIORITY_PROJECT_NON_ALLOWED_SECTOR}.
   */
  String e20025();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_SINGLE_SITUATION_REQUIRED}.
   */
  String e20026();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#IMPORT_DIFFERENTIAL_CALCULATION_WITH_RESULTS_NOT_ALLOWED}.
   */
  String e20027();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#AUTHORIZATION_ERROR}.
   */
  String e40001();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_ALREADY_EXISTS}.
   */
  String e40002();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_DOES_NOT_EXIST}.
   */
  String e40003();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_CANNOT_BE_DELETED}.
   */
  String e40004();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_EMAIL_ADDRESS_ALREADY_EXISTS}.
   */
  String e40005();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_API_KEY_ALREADY_EXISTS}.
   */
  String e40006();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_INVALID_API_KEY}.
   */
  String e40007();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_API_KEY_GENERATION_DISABLED}.
   */
  String e40008();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_MAX_CONCURRENT_JOB_LIMIT_REACHED}.
   */
  String e40009();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_ACCOUNT_DISABLED}.
   */
  String e40010();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_PRIORITY_PROJECT_UTILISATION_EXPORT_NOT_ALLOWED}.
   */
  String e40011();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#USER_PERIOD_JOB_RATE_LIMIT_REACHED}.
   */
  String e40012();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_VALID_EMAIL_SUPPLIED}.
   */
  String e50001();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_INCORRECT_CALCULATIONYEAR}.
   */
  String e50002();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_CALCULATIONTYPE_SUPPLIED}.
   */
  String e50003();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_SUBSTANCE_SUPPLIED}.
   */
  String e50004();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_INVALID_CALCULATION_RANGE}.
   */
  String e50005();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_INVALID_TEMPPROJECT_RANGE}.
   */
  String e50006();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_SOURCES}.
   */
  String e50007();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_SITUATION_NO_PROPOSED}.
   */
  String e50008();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_CALCULATIONTYPE_SUPPLIED_NOT_SUPPORTED}.
   */
  String e50009();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_REPORT_PERMIT_DEMAND_COMPARISON_NOT_SUPPORTED}.
   */
  String e50010();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_UNKNOWN_SUBSTANCE_SUPPLIED}.
   */
  String e50011();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_USER_JOBKEY_DOES_NOT_EXIST}.
   */
  String e50012();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_JOB_CANCELLED}.
   */
  String e50013();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_RECEPTORS_IN_PARAMETERS}.
   */
  String e50014();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_USER_CALCULATION_POINT_SET_ALREADY_EXISTS}.
   */
  String e50015();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_USER_CALCULATION_POINT_SET_DOES_NOT_EXIST}.
   */
  String e50016();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_UNSUPPORTED_DATATYPE_IN_OPERATION}.
   */
  String e50017();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_UNSUPPORTED_PAS_OPTIONS}.
   */
  String e50018();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_VALIDATION_SKIPPED_WARNING}.
   */
  String e50019();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_INVALID_OUTPUTTYPE}.
   */
  String e50020();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_INVALID_METEO}.
   */
  String e50021();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_MISSING_RECEPTOR_HEIGHT}.
   */
  String e50022();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_UNEXPECTED_RECEPTOR_HEIGHT}.
   */
  String e50023();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_CUSTOM_POINTS}.
   */
  String e50024();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#CONNECT_NO_SITUATION_TYPE_DETECTED}.
   */
  String e50025();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#SCENARIO_API_CONNECTION_ERROR}.
   */
  String e60001();

  /**
   * See {@link nl.overheid.aerius.shared.exception.AeriusExceptionReason#GENERAL_UNSUPPORTED_PAS_OPTIONS}.
   */
  String e90001();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#SCHEMATRON_ASSERT}.
   */
  String e99000();

  /**
   * See {@link nl.overheid.aerius.shared.exception.ImaerExceptionReason#SCHEMATRON_REPORT}.
   */
  String e99001();

}
