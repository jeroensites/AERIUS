/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.exception.ImaerExceptionReason;

/**
 * Test class for interface {@link AeriusExceptionMessages}.
 */
class AeriusExceptionMessagesTest {

  /**
   * Test to check if the reasons from {@link nl.overheid.aerius.shared.exception.AeriusException} has methods on the
   * interface of {@link AeriusExceptionMessages}
   */
  @Test
  void testReasonsMatch() {
    for (final Method method : AeriusExceptionMessages.class.getDeclaredMethods()) {
      final String methodName = method.getName().substring(1);
      boolean present = false;
      for (final Reason reason : allReasons()) {
        if (Integer.toString(reason.getErrorCode()).equals(methodName)) {
          present = true;
          continue;
        }
      }
      assertTrue(present, "Interface method " + method.getName() + " not present in " + AeriusExceptionReason.class.getCanonicalName());
    }
  }

  /**
   * Test to check if the reasons from {@link nl.overheid.aerius.shared.exception.AeriusException} has methods on the
   * interface of {@link AeriusExceptionMessages}
   */
  @Test
  void testMethodMatch() {
    for (final Reason reason : allReasons()) {
      boolean present = false;
      for (final Method method : AeriusExceptionMessages.class.getDeclaredMethods()) {
        final String methodName = method.getName().substring(1);
        if (Integer.toString(reason.getErrorCode()).equals(methodName)) {
          present = true;
          continue;
        }
      }
      assertTrue(present, "Reason " + reason.getErrorCode() + " not present in interface " + AeriusExceptionMessages.class.getCanonicalName());
    }
  }

  private List<Reason> allReasons() {
    return Stream.concat(Stream.of(ImaerExceptionReason.values()), Stream.of(AeriusExceptionReason.values())).collect(Collectors.toList());
  }
}
