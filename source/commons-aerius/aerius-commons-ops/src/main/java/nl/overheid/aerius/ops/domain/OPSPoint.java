/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;

/**
 * Simple point with x and y coordinate.
 */
public class OPSPoint extends SridPoint {

  private static final long serialVersionUID = 68023908055973260L;

  OPSPoint() {
    super();
  }

  @Override
  @Min(value = OPSLimits.X_COORDINATE_MINIMUM)
  @Max(value = OPSLimits.X_COORDINATE_MAXIMUM)
  public double getX() {
    return super.getX();
  }

  @Override
  @Min(value = OPSLimits.Y_COORDINATE_MINIMUM)
  @Max(value = OPSLimits.Y_COORDINATE_MAXIMUM)
  public double getY() {
    return super.getY();
  }

}
