/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.conversion;

import java.math.BigDecimal;
import java.math.RoundingMode;

import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.io.BrnConstants;
import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;

/**
 * Converts the properties of an OPSSource to input for the .brn file.
 * Contains the AERIUS specific logic.
 */
public class AeriusOPSSourceProperties implements OPSSourceProperties {

  /**
   * if category would be lower 0, set it to 1, otherwise ops will log error.
   */
  private static final int MIN_CAT = 1;

  @Override
  public Object getX(final SridPoint p, final boolean epsg4326) {
    return epsg4326 ? p.getX() : p.getRoundedX();
  }

  @Override
  public Object getY(final SridPoint p, final boolean epsg4326) {
    return epsg4326 ? p.getY() : p.getRoundedY();
  }

  @Override
  public double getHeatContent(final OPSSource s) {
    if (useOutflow(s)) {
      return useBuilding(s) ? 0d : BrnConstants.HEAT_CONTENT_NOT_APPLICABLE;
    } else {
      return  s.getHeatContent() < 0 ? BrnConstants.HEAT_CONTENT_NOT_APPLICABLE : s.getHeatContent();
    }
  }

  @Override
  public double getEmissionHeight(final OPSSource s) {
    if (useBuilding(s)) {
      return Math.min(OPSLimits.SCOPE_BUILDING_HEIGHT_MAXIMUM, s.getEmissionHeight());
    } else {
      return s.getEmissionHeight();
    }
  }

  @Override
  public double getSpread(final OPSSource s) {
    // The spread cannot be higher than the height of the building
    // (prior to OPS 4.6.2.1 this used to be a warning, but will now cause a crash if not handled properly).
    //      /
    // ____/
    // |  |\
    // |  | \
    // |  |
    // |  |
    // |  |
    // This chimney has a height (in characters) of 5 and a spread of 2. A higher spread than 5 would force it to hit the ground.
    // If we use a building the maximum allowed EmissionHeight is 13, we have to check if we have to correct the EmissionHeight with
    // getEmissionHeight(d)
    return Math.min(getEmissionHeight(s),
        new BigDecimal(String.valueOf(s.getSpread())).setScale(1, RoundingMode.HALF_UP).doubleValue());
  }

  @Override
  public double getOutflowDiameter(final OPSSource s) {
    if (useOutflow(s)) {
      return useBuilding(s) ? adjustValueToMinMax(s.getOutflowDiameter(), OPSLimits.SCOPE_BUILDING_OUTFLOW_DIAMETER_MINIMUM,
          OPSLimits.SCOPE_BUILDING_OUTFLOW_DIAMETER_MAXIMUM) : s.getOutflowDiameter();
    } else {
      return BrnConstants.OUTFLOW_DIAMETER_NOT_APPLICABLE;
    }
  }

  @Override
  public double getOutflowVelocity(final OPSSource s) {
    if (useOutflow(s) && s.getOutflowDirection() != null && s.getOutflowVelocity() > BrnConstants.OUTFLOW_VELOCITY_NOT_APPLICABLE) {
      final double outflowValue;
      if (useBuilding(s)) {
        outflowValue = adjustValueToMinMax(s.getOutflowVelocity(), OPSLimits.SCOPE_BUILDING_OUTFLOW_VELOCITY_MINIMUM,
            OPSLimits.SCOPE_BUILDING_OUTFLOW_VELOCITY_MAXIMUM);
      } else {
        outflowValue = s.getOutflowVelocity();
      }
      return (s.getOutflowDirection() == OPSSource.OPSSourceOutflowDirection.HORIZONTAL ? -1 : 1) * outflowValue;
    }
    return BrnConstants.OUTFLOW_VELOCITY_NOT_APPLICABLE;
  }

  @Override
  public double getEmissionTemperature(final OPSSource s) {
    if (useOutflow(s)) {
      return useBuilding(s) ? BrnConstants.EMISSION_TEMPERATURE_NOT_APPLICABLE : s.getEmissionTemperature();
    } else {
      return BrnConstants.EMISSION_TEMPERATURE_NOT_APPLICABLE;
    }
  }

  @Override
  public int getCat(final OPSSource s) {
    return Math.max(MIN_CAT, s.getCat());
  }

  @Override
  public double getBuildingLength(final OPSSource s) {
    return useBuildingValueMinMax(s.getBuildingLength(),
        OPSLimits.SCOPE_BUILDING_LENGTH_MINIMUM,
        OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM,
        BrnConstants.BUILDING_LENGTH_NOT_APPLICABLE);
  }

  @Override
  public double getBuildingWidth(final OPSSource s) {
    return useBuildingValueMinMax(s.getBuildingWidth(),
        OPSLimits.SCOPE_BUILDING_WIDTH_MINIMUM,
        OPSLimits.SCOPE_BUILDING_WIDTH_MAXIMUM,
        BrnConstants.BUILDING_WIDTH_NOT_APPLICABLE);
  }

  @Override
  public double getBuildingHeight(final OPSSource s) {
    return useBuildingValueMinMax(s.getBuildingHeight(),
        OPSLimits.SCOPE_BUILDING_HEIGHT_MINIMUM,
        OPSLimits.SCOPE_BUILDING_HEIGHT_MAXIMUM,
        BrnConstants.BUILDING_HEIGHT_NOT_APPLICABLE);
  }

  /**
   * If heat content equals HEAT_CONTENT_NOT_APPLICABLE we use outflow.
   *
   * @param s
   * @return
   */
  private static boolean useOutflow(final OPSSource s) {
    return Double.compare(BrnConstants.HEAT_CONTENT_NOT_APPLICABLE, s.getHeatContent()) == 0;
  }

  private static double useBuildingValueMinMax(final double value, final double minValue, final double maxValue, final double notapplicableValue) {
    if (value > 0) {
      return adjustValueToMinMax(value, minValue, maxValue);
    } else {
      return notapplicableValue;
    }
  }

  private static double adjustValueToMinMax(final double value, final double minValue, final double maxValue) {
    return Math.max(minValue, Math.min(maxValue, value));
  }

  private static boolean useBuilding(final OPSSource s) {
    return s.getBuildingWidth() > 0 || s.getBuildingLength() > 0 || s.getBuildingHeight() > 0;
  }

}
