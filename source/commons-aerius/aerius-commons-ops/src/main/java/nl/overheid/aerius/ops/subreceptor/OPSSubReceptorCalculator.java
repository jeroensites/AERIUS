/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.subreceptor;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
public abstract class OPSSubReceptorCalculator extends AbstractSubReceptorCalculator<OPSSource> {

  private static final String PRECISION_FORMAT = "0.###E0";

  private static final double RECALCULATE_DISTANCE = 20.0;
  private static final double MIN_SOURCE_RECEPTOR_DISTANCE = 20.0;
  private static final double MAX_DIAMETER = 100.0;

  private static final int SUB_RECEPTOR_RINGS = 11;

  private final Collection<OPSReceptor> receptors;

  public OPSSubReceptorCalculator(final Collection<OPSReceptor> receptors, final int hexagonSurfaceLevel1) {
    super(SUB_RECEPTOR_RINGS, new HexagonZoomLevel(1, hexagonSurfaceLevel1));
    this.receptors = receptors;
  }

  @Override
  protected boolean shouldRecalculate(final OPSSource source, final AeriusResultPoint point) {
    return source.getDiameter() < MAX_DIAMETER
        && point.distance(source.getPoint()) < RECALCULATE_DISTANCE + source.getDiameter() / 2.0;
  }

  @Override
  protected boolean shouldIncludeResult(final OPSSource source, final AeriusResultPoint point) {
    return point.distance(source.getPoint()) > MIN_SOURCE_RECEPTOR_DISTANCE;
  }

  @Override
  protected OPSReceptor getOriginalPoint(final AeriusResultPoint point) {
    for (final OPSReceptor receptor : receptors) {
      if (receptor.equals(point)) {
        return receptor;
      }
    }
    return null;
  }

  @Override
  protected List<AeriusResultPoint> recalculate(final OPSSource inHexagonSource, final ArrayList<OPSReceptor> subPoints)
      throws IOException, AeriusException {
    final List<OPSSource> sources = new ArrayList<>();
    sources.add(inHexagonSource);

    return recalculate(sources, subPoints);
  }

  protected abstract List<AeriusResultPoint> recalculate(List<OPSSource> sources, ArrayList<OPSReceptor> receptors)
      throws IOException, AeriusException;

  @Override
  protected double roundToProperPrecision(final double value) {
    final NumberFormat formatter = new DecimalFormat(PRECISION_FORMAT, DecimalFormatSymbols.getInstance(Locale.ENGLISH));
    return Double.parseDouble(formatter.format(value));
  }

}
