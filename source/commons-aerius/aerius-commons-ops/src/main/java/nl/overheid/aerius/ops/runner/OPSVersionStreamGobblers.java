/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.runner;

import nl.overheid.aerius.exec.StreamGobbler;
import nl.overheid.aerius.ops.OPSVersion;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;

/**
 * StreamGobblers to check the version OPS returns.
 */
public class OPSVersionStreamGobblers extends OPSStreamGobblers {

  private OPSInvalidVersionException versionException = new OPSInvalidVersionException("[no version detected]");

  @Override
  public StreamGobbler outputStreamGobbler(final String type, final String parentId) {
    return new StreamGobbler(type, parentId, this::matchVersion);
  }

  private Boolean matchVersion(final String line) {
    try {
      if (OPSVersion.validateOPSVersion(line)) {
        versionException = null;
        return false;
      }
    } catch (final OPSInvalidVersionException e) {
      versionException = e;
    }
    return true;
  }

  public OPSInvalidVersionException getVersionException() {
    return versionException;
  }
}
