/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import java.util.Collection;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;

/**
 * Helper class to calculate how expensive a calculation is.
 */
class OPSTimeLogger {

  private static final Logger LOG = LoggerFactory.getLogger(OPSTimeLogger.class);

  private static final double TWO_DIGITS_RESOLUTION = 100;

  public void logDuration(final long startTime, final OPSInputData inputData) {
    if (LOG.isDebugEnabled()) {
      final int opsunits = countOPSUnits(inputData);
      final long delta = System.nanoTime() - startTime;
      LOG.debug("Total execution time for {} ops units: {}ms. Average: {}ms per ops unit.", opsunits, TimeUnit.NANOSECONDS.toMillis(delta),
          TimeUnit.NANOSECONDS.toMillis((long) (delta * TWO_DIGITS_RESOLUTION / opsunits)) / TWO_DIGITS_RESOLUTION);
    }
  }

  /**
   * Count the total number of sources * total number of receptors.
   *
   * @param data input data
   * @return total sources count * receptors
   */
  private int countOPSUnits(final OPSInputData data) {
    int opsUnits = 0;

    for (final Entry<Integer, Collection<OPSSource>> entry : data.getEmissionSources().entrySet()) {
      for (final OPSSource opsSource : entry.getValue()) {
        opsUnits += opsSource.getSubstances().size();
      }
    }
    return opsUnits * data.getReceptors().size();
  }
}
