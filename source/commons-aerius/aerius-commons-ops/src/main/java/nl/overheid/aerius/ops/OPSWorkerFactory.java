/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.exception.OPSRunDirectoryExistsException;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerFactory;

/**
 * Worker implementation for OPS workers. This worker can be used to run multiple OPS calls concurrently.
 */
public class OPSWorkerFactory implements WorkerFactory<OPSWorkerConfiguration> {

  private static final Logger LOG = LoggerFactory.getLogger(OPSWorkerFactory.class);

  @Override
  public OPSWorkerConfiguration createConfiguration(final Properties properties) {
    return new OPSWorkerConfiguration(properties);
  }

  @Override
  public Worker<OPSInputData, CalculationResult> createWorkerHandler(final OPSWorkerConfiguration config, final BrokerConnectionFactory factory)
      throws IOException, InterruptedException, OPSInvalidVersionException {
    return new Worker<OPSInputData, CalculationResult>() {
      private final RunOPS runOPS = createRunOps(config);

      @Override
      public CalculationResult run(final OPSInputData input, final WorkerIntermediateResultSender resultSender, final JobIdentifier correlation)
          throws Exception {
        while (true) {
          try {
            return runOPS.run(input);
          } catch (final OPSRunDirectoryExistsException e) {
            LOG.warn("Directory existed, trying again.");
          }
        }
      }
    };
  }

  public RunOPS createRunOps(final OPSWorkerConfiguration workerConfiguration) throws IOException, InterruptedException, OPSInvalidVersionException {
    return new RunOPS(createOPSConfiguration(workerConfiguration));
  }

  public final OPSConfiguration createOPSConfiguration(final OPSWorkerConfiguration config) {
    final OPSConfiguration opsConfig = new OPSConfiguration();
    opsConfig.setOpsRoot(config.getOPSRoot());
    opsConfig.setRunFilesDirectory(config.getOPSRunFilesDirectory());
    opsConfig.setKeepGeneratedFiles(config.isOPSKeepGeneratedFiles());
    opsConfig.setSettingsMeteoFile(config.getSettingsMeteoFile());
    opsConfig.setSettingsLandUse(config.getSettingsLandUse());
    return opsConfig;
  }

  @Override
  public WorkerType getWorkerType() {
    return WorkerType.OPS;
  }
}
