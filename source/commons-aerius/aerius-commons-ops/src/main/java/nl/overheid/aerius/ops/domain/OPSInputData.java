/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;

/**
 * Data class to combine all data needed for a single OPS run.
 */
public class OPSInputData extends EngineInputData<OPSSource, OPSReceptor> {

  private static final long serialVersionUID = 2L;

  private final String opsVersion;
  private final int hexagonSurfaceLevel1;
  private OPSOptions opsOptions;
  /**
   * If set it will pass the max distance option to OPS. This will limit calculation of source/receptors to 25km.
   */
  private boolean maxDistance;
  /**
   * If set it will pass the min distance option to OPS. This will not calculate of source/receptors within 5km.
   */
  private final Set<Integer> minDistanceSet = new HashSet<>();

  public OPSInputData(final String opsVersion, final int hexagonSurfaceLevel1) {
    super(Theme.WNB); // At this moment only WNB is supported with OPS
    this.opsVersion = opsVersion;
    this.hexagonSurfaceLevel1 = hexagonSurfaceLevel1;
  }

  public String getOpsVersion() {
    return opsVersion;
  }

  public boolean isMaxDistance() {
    return maxDistance;
  }

  public void setMaxDistance(final boolean maxDistance) {
    this.maxDistance = maxDistance;
  }

  public void addMinDistanceGroupId(final Integer groupId) {
    minDistanceSet.add(groupId);
  }

  public boolean minDistanceContains(final Integer groupId) {
    return minDistanceSet.contains(groupId);
  }

  @Override
  @NotNull
  @Size(min = 1)
  @Valid
  public Collection<OPSReceptor> getReceptors() {
    return super.getReceptors();
  }

  public int getHexagonSurfaceLevel1() {
    return hexagonSurfaceLevel1;
  }

  public OPSOptions getOpsOptions() {
    return opsOptions;
  }

  public void setOpsOptions(final OPSOptions opsOptions) {
    this.opsOptions = opsOptions;
  }

}
