/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.util.OSUtils;

/**
 * Class that contains the used OPS version number and contains checks to determine if the binary used is version expected.
 */
public final class OPSVersion {

  public static final String VERSION = "5.0.1.3";

  public static final String OPS_VERSION_RELEASE_LINUX = "OPS-version: L-5.0.1.3 ; Release date: 11 nov 2021";
  public static final String OPS_VERSION_RELEASE_WINDOWS = "OPS-version: W-5.0.1.3 ; Release date: 11 nov 2021";

  private OPSVersion() {
    // Util class.
  }

  /**
   * Validates if the given OPS version matches the expected version by this product. If it doesn't match it throws an
   * {@link OPSInvalidVersionException}.
   *
   * @param opsVersion version to check
   * @throws OPSInvalidVersionException in case OPS version doesn't match
   */
  public static void validateVersion(final String opsVersion) throws OPSInvalidVersionException {
    if (!VERSION.equals(opsVersion)) {
      throw new OPSInvalidVersionException("Data received with version number '" + opsVersion +"', expecting: '" + VERSION + "'.");
    }
  }

  /**
   * Validates if the version returned by OPS matches the expected version information pattern.
   * If it doesn't match it throws an {@link OPSInvalidVersionException}.
   *
   * @param opsOutput ops output to match.
   * @return true if version matches
   * @throws OPSInvalidVersionException if it doesn't match else returns null.
   */
  public static boolean validateOPSVersion(final String opsOutput) throws OPSInvalidVersionException {
    final String trimmedOpsOutput = opsOutput.trim();
    final String patternToMatch = OSUtils.isWindows() ? OPS_VERSION_RELEASE_WINDOWS : OPS_VERSION_RELEASE_LINUX;

    if (patternToMatch.equals(trimmedOpsOutput)) {
      return true;
    } else {
      throw new OPSInvalidVersionException("Invalid OPS version configured. Expected " + patternToMatch + ", but found: " +  trimmedOpsOutput);
    }
  }
}
