/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.subreceptor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.receptor.SubReceptorUtil;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
abstract class AbstractSubReceptorCalculator<T extends EngineSource> {
  private static final Logger LOG = LoggerFactory.getLogger(AbstractSubReceptorCalculator.class);

  private final int subReceptorRings;
  private final HexagonZoomLevel zoomLevel1;

  /**
   * @param subReceptorRings The number of rings to use within the hexagon for the sub receptor calculation.
   */
  public AbstractSubReceptorCalculator(final int subReceptorRings, final HexagonZoomLevel zoomLevel1) {
    this.subReceptorRings = subReceptorRings;
    this.zoomLevel1 = zoomLevel1;
  }

  public void recalculateInHexagonSources(final Collection<T> sources, final List<AeriusResultPoint> singleSubstanceResults,
      final Substance substance) throws IOException, AeriusException {
    for (final AeriusResultPoint resultPoint : singleSubstanceResults) {
      //for each point, determine if there are sources nearby.
      //if so, recalculate it.
      for (final T source : sources) {
        if (shouldRecalculate(source, resultPoint) && isValidSource(source, substance)) {
          recalculateInHexagonSource(source, resultPoint, substance);
        }
      }
    }
  }

  private void recalculateInHexagonSource(final T inHexagonSource, final AeriusResultPoint point, final Substance substance)
      throws IOException, AeriusException {
    final OPSReceptor original = getOriginalPoint(point);
    final SubReceptorUtil<OPSReceptor> subReceptorUtil = new SubReceptorUtil<OPSReceptor>(zoomLevel1) {
      @Override
      public OPSReceptor newPoint(final OPSReceptor original) {
        return new OPSReceptor(original);
      }
    };
    final ArrayList<OPSReceptor> subPoints = subReceptorUtil.determineSubPoints(original, subReceptorRings);
    if (LOG.isTraceEnabled()) {
      LOG.trace("Recalculate in hexagon source:{} with {} sub sources.", inHexagonSource, subPoints.size());
    }
    final List<AeriusResultPoint> subResults = recalculate(inHexagonSource, subPoints);
    //replace the results
    for (final Entry<EmissionResultKey, Double> originalResultEntry : point.getEmissionResults().entrySet()) {
      if (originalResultEntry.getKey().getSubstance() == substance) {
        double midPointResult = 0.0;
        double totalResult = 0.0;
        int pointsUsed = 0;
        for (final AeriusResultPoint subResult : subResults) {
          if (shouldIncludeResult(inHexagonSource, subResult)) {
            totalResult += subResult.getEmissionResult(originalResultEntry.getKey());
            pointsUsed++;
          }
          if (subResult.equals(original)) {
            midPointResult = subResult.getEmissionResult(originalResultEntry.getKey());
          }
        }
        //result should be the original value - the value of the point with this source (to obtain the results of all the other sources in the
        // calculation) + the average of the sub calculation.
        final double newValue = roundToProperPrecision(originalResultEntry.getValue() - midPointResult + totalResult / pointsUsed);
        if (LOG.isTraceEnabled()) {
          LOG.trace("Original value {} source only recalulated to {} and total subpoints results {} averaged to {} resulting in new value {}",
              originalResultEntry.getValue(), midPointResult, totalResult, totalResult / pointsUsed, newValue);
        }
        originalResultEntry.setValue(newValue);
      }
    }
  }

  protected abstract double roundToProperPrecision(double value);

  /**
   * Determine if the source should be recalculated for a specific point.
   * @param source The source to determine for.
   * @param point The point to determine for.
   * @return True if the combination should be recalculated, false if not.
   */
  protected abstract boolean shouldRecalculate(T source, AeriusResultPoint point);

  /**
   * Determine if the results for the source should be included for a specific point.
   * @param source The source to determine for.
   * @param point The point to determine for.
   * @return True if the result for the combination should be added, false if not.
   */
  protected abstract boolean shouldIncludeResult(T source, AeriusResultPoint point);

  /**
   * @param point The result point to determine the original point for.
   * @return The original point, including terrain properties and the like.
   */
  protected abstract OPSReceptor getOriginalPoint(AeriusResultPoint point);

  /**
   * @param inHexagonSource The source to recalculate.
   * @param subPoints The points to recalculate for, a list of subpoint of the original point.
   * @return The results for those points.
   * @throws IOException In case of errors reading/writing files.
   * @throws AeriusException In case of general errors calculating.
   */
  protected abstract List<AeriusResultPoint> recalculate(T inHexagonSource, ArrayList<OPSReceptor> subPoints) throws IOException, AeriusException;

  /**
   * Returns true if source has any emission for the input.
   * @param source to check
   * @param substance substance to check
   * @return true if any emission for any source, else false
   */
  protected abstract boolean isValidSource(T source, Substance substance);

}
