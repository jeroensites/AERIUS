/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

public final class SubstanceUtil {

  /**
   * List of substances OPS can calculate results for.
   */
  private static final EnumSet<Substance> SUPPORTED_SUBSTANCES = EnumSet.of(Substance.NH3, Substance.NOX, Substance.PM10);

  private SubstanceUtil() {
    // Util class
  }

  /**
   * @param substances
   * @return
   */
  public static List<Substance> supportedSubstances(final List<Substance> substances) {
    final List<Substance> supportedSubstances = new ArrayList<>();
    for (final Substance substance : substances) {
      if (SUPPORTED_SUBSTANCES.contains(substance)) {
        supportedSubstances.add(substance);
      }
    }
    return supportedSubstances;
  }

  /**
   * Returns true if has any emission for the input.
   * @param sources to check
   * @param substance substance to check
   * @return true if any emission for any source, else false
   */
  public static boolean hasEmissionForSubstance(final Collection<OPSSource> sources, final Substance substance) {
    if (SUPPORTED_SUBSTANCES.contains(substance)) {
      for (final OPSSource source : sources) {
        if (hasEmissionForSubstance(source, substance)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Returns true if has any emission for the input.
   * @param source to check
   * @param substance substance to check
   * @return true if any emission for the source, else false
   */
  public static boolean hasEmissionForSubstance(final OPSSource source, final Substance substance) {
    return SUPPORTED_SUBSTANCES.contains(substance) && source.getEmission(substance) > 0.0;
  }

  /**
   *
   * @param substances
   * @param emissionResultKeys
   * @return
   */
  public static Map<Substance, EnumSet<EmissionResultKey>> emissionResultKeys(final List<Substance> substances,
      final Set<EmissionResultKey> emissionResultKeys) {
    final Map<Substance, EnumSet<EmissionResultKey>> map = new EnumMap<>(Substance.class);

    substances.forEach(s -> map.put(s, erkForSubstance(s, emissionResultKeys)));
    return map;

  }

  private static EnumSet<EmissionResultKey> erkForSubstance(final Substance substance, final Set<EmissionResultKey> emissionResultKeys) {
    final EnumSet<EmissionResultKey> keysForSubstance = EnumSet.noneOf(EmissionResultKey.class);

    for (final EmissionResultKey key : emissionResultKeys) {
      if (substance == key.getSubstance()) {
        keysForSubstance.add(key);
      }
      if (substance == Substance.PM10 && key.getSubstance() == Substance.PM25) {
        keysForSubstance.add(key);
      }
    }
    return keysForSubstance;
  }
}
