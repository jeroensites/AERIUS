/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;

/**
 * Convenience object to keep track of a source and the (optional) building that is related to it.
 */
public class SourceBuildingCombination {

  private final EmissionSourceFeature source;
  private BuildingFeature building;

  public SourceBuildingCombination(final EmissionSourceFeature source) {
    this.source = source;
  }

  public BuildingFeature getBuilding() {
    return building;
  }

  public void setBuilding(final BuildingFeature building) {
    this.building = building;
  }

  public EmissionSourceFeature getSource() {
    return source;
  }

}
