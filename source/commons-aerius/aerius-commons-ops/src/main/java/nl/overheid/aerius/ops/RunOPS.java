/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.OPSFileWriter;
import nl.overheid.aerius.ops.runner.OPSSingleSubstanceRunner;
import nl.overheid.aerius.ops.subreceptor.OPSSubReceptorCalculator;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Handles a full runs of OPS. Includes writing configuration files,
 * instrumenting OPS and reading output from OPS output files. This class can
 * be reused to run multiple runs. In the constructor the location of OPS is
 * initialized and each run is started via {@link #run(int, OPSInputData)}.
 * This class is thread safe.
 */
class RunOPS {

  private static final String OPS_BIN = "/bin";

  private static final OPSTimeLogger OPS_LOGGER = new OPSTimeLogger();
  private static final OPSResultCollector COLLECTOR = new OPSResultCollector();

  private final Validator validator;
  private final OPSSingleSubstanceRunner runner;

  /**
   * Initializes the class with OPS location information.
   *
   * @param configuration OPS configuration
   * @param keepGeneratedFiles Boolean to determine if any generated files should remain or should be deleted once done.
   * @throws OPSInvalidVersionException
   * @throws InterruptedException
   * @throws IOException
   */
  public RunOPS(final OPSConfiguration configuration) throws IOException, InterruptedException, OPSInvalidVersionException {
    final File opsRoot = configuration.getOPSRoot();
    if (opsRoot == null || configuration.getRunFilesDirectory() == null) {
      throw new IllegalArgumentException("No argument allowed to be null or empty");
    }
    runner = new OPSSingleSubstanceRunner(new OPSFileWriter(configuration), new File(opsRoot, OPS_BIN), configuration.isKeepGeneratedFiles());
    final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
    validator = validatorFactory.getValidator();
    runner.versionCheck();
  }

  public CalculationResult run(final OPSInputData inputData) throws IOException, AeriusException, OPSInvalidVersionException {
    final long startTime = System.nanoTime();

    // Don't validate raw ops files
    if (inputData.getOpsOptions() == null || !inputData.getOpsOptions().isRawInput()) {
      validateInput(inputData);
    }

    final CalculationResult result = new CalculationResult(CalculationEngine.OPS);

    for (final Entry<Integer, Collection<OPSSource>> entry : inputData.getEmissionSources().entrySet()) {
      result.put(entry.getKey(), runSector(inputData, entry.getKey(), entry.getValue()));
    }
    OPS_LOGGER.logDuration(startTime, inputData);
    return result;
  }

  /**
   * Perform a OPS run for each substance present in the input data.
   *
   * @param inputData all input values used in a OPS calculation
   * @param groupId The id for this set of sources
   * @param sources sources to calculate
   * @return the result of an OPS calculation
   * @throws IOException IOException
   * @throws AeriusException AeriusException
   * @throws OPSInvalidVersionException
   */
  private List<AeriusResultPoint> runSector(final OPSInputData inputData, final Integer groupId, final Collection<OPSSource> sources)
      throws IOException, AeriusException, OPSInvalidVersionException {
    final List<Substance> supportedSubstances = SubstanceUtil.supportedSubstances(inputData.getSubstances());
    final Map<Substance, EnumSet<EmissionResultKey>> erkMap =
        SubstanceUtil.emissionResultKeys(supportedSubstances, inputData.getEmissionResultKeys());

    final List<AeriusResultPoint> result = COLLECTOR.mergeResults(runOne(inputData, groupId, sources, supportedSubstances, erkMap));
    return result;
  }

  private Map<Substance, List<AeriusResultPoint>> runOne(final OPSInputData inputData, final Integer groupId, final Collection<OPSSource> sources,
      final List<Substance> supportedSubstances, final Map<Substance, EnumSet<EmissionResultKey>> erkMap) throws IOException, AeriusException {
    final Map<Substance, List<AeriusResultPoint>> results = new EnumMap<>(Substance.class);
    final boolean minDistance = inputData.minDistanceContains(groupId);

    for (final Substance substance : supportedSubstances) {
      final EnumSet<EmissionResultKey> erks = erkMap.get(substance);

      if (SubstanceUtil.hasEmissionForSubstance(sources, substance)) {
        final List<AeriusResultPoint> singleSubstanceResults =
            runner.runSingleSubstance(sources, inputData.getReceptors(), substance, inputData.getYear(), inputData.getMeteo(), erks,
                inputData.getOpsOptions(), inputData.isMaxDistance(), minDistance);
        final OPSSubReceptorCalculator subReceptorCalculator =
            createSubReceptorCalculator(inputData, substance, inputData.getYear(), erks, minDistance);
        subReceptorCalculator.recalculateInHexagonSources(sources, singleSubstanceResults, substance);
        results.put(substance, singleSubstanceResults);
      } else {
        results.put(substance, emptyResultsForSubstance(inputData.getReceptors(), erks));
      }
    }
    return results;
  }

  private OPSSubReceptorCalculator createSubReceptorCalculator(final OPSInputData inputData, final Substance substance, final int year,
      final Set<EmissionResultKey> erks, final boolean minDistance) {
    return new OPSSubReceptorCalculator(inputData.getReceptors(), inputData.getHexagonSurfaceLevel1()) {

      @Override
      protected List<AeriusResultPoint> recalculate(final List<OPSSource> sources, final ArrayList<OPSReceptor> receptors)
          throws IOException, AeriusException {
        return runner.runSingleSubstance(sources, receptors, substance, year, inputData.getMeteo(), erks, inputData.getOpsOptions(),
            inputData.isMaxDistance(), minDistance);
      }

      @Override
      protected boolean isValidSource(final OPSSource source, final Substance substance) {
        return SubstanceUtil.hasEmissionForSubstance(source, substance);
      }

    };
  }

  private List<AeriusResultPoint> emptyResultsForSubstance(final Collection<OPSReceptor> receptors, final Set<EmissionResultKey> erks) {
    final List<AeriusResultPoint> emptyResults = new ArrayList<>();
    for (final OPSReceptor opsr : receptors) {
      final AeriusResultPoint rp = new AeriusResultPoint(opsr.getId(), opsr.getPointType(), Math.round(opsr.getX()), Math.round(opsr.getY()));
      for (final EmissionResultKey erk : erks) {
        rp.setEmissionResult(erk, 0.0);
      }
      emptyResults.add(rp);
    }
    return emptyResults;
  }

  private void validateInput(final OPSInputData input) throws AeriusException, OPSInvalidVersionException {
    final List<String> errorMessages = new ArrayList<>();
    OPSVersion.validateVersion(input.getOpsVersion());
    aggregateError(validator.validate(input), errorMessages);
    for (final Entry<Integer, Collection<OPSSource>> entry : input.getEmissionSources().entrySet()) {
      for (final OPSSource source : entry.getValue()) {
        aggregateError(validator.validate(source), errorMessages);
      }
    }
    if (!errorMessages.isEmpty()) {
      throw new AeriusException(AeriusExceptionReason.OPS_INPUT_VALIDATION, String.join(", ", errorMessages));
    }
  }

  private <C> void aggregateError(final Set<ConstraintViolation<C>> sViolations, final List<String> errorMessages) {
    if (!sViolations.isEmpty()) {
      for (final ConstraintViolation<C> violation : sViolations) {
        errorMessages.add(violation.getPropertyPath() + ": " + violation.getMessage());
      }
    }
  }
}
