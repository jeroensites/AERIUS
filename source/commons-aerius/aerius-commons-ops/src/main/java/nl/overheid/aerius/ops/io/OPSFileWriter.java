/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;

/**
 * Writes all input files for a single OPS run into a temporary directory.
 */
public class OPSFileWriter {

  private final OPSConfiguration configuration;

  /**
   * Create an OPSFileWriter to generate OPS files with.
   *
   * @param configuration global ops options.
   * @param opsRoot Directory to the OPS installation directory.
   */
  public OPSFileWriter(final OPSConfiguration configuration) {
    this.configuration = configuration;
  }

  /**
   * Create and write all configuration files to disk.
   *
   * @param runId The run ID to use. Will be used as sub-directory of the base directory to write all files to on disk.
   * @param data The data to use.
   * @param substance The substance to write the files for.
   * @param year The year to write the files for.
   * @param meteoFile Custom meteoFile to include in the file
   * @throws IOException In case writing a file fails.
   */
  public void writeFiles(final String runId, final Collection<OPSSource> sources, final Collection<OPSReceptor> receptors, final Substance substance,
      final int year, final Meteo meteo, final OPSOptions opsOptions) throws IOException {
    final File runDirectory = getRunDirectory(runId);
    if (!runDirectory.exists() && !runDirectory.mkdirs()) {
      throw new IOException("Could not create directory " + runDirectory);
    }
    BrnFileWriter.writeFile(runDirectory, sources, substance, opsOptions);
    RcpFileWriter.writeFile(runDirectory, receptors);
    final String meteoFile = ControlFileWriter.toOpsMeteoFile(meteo);
    ControlFileWriter.writeFile(configuration, runDirectory, runId, substance, year, meteoFile, opsOptions);
  }

  /**
   * @param runId The run ID to get the location for.
   * @return the 'physical' location of the directory the files will be written to.
   */
  public File getRunDirectory(final String runId) {
    return new File(configuration.getRunFilesDirectory(), runId);
  }

  /**
   * @param runId The run ID to get the location for.
   * @return the 'virtual' location of the settings file to be used by OPS.
   */
  public String getSettingsLocation(final String runId) {
    return ControlFileWriter.controlFileLocation(getRunDirectory(runId));
  }
}
