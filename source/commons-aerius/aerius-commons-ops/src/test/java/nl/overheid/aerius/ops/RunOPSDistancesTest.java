/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Unit test to check the -mindist and -maxdist OPS options.
 */
class RunOPSDistancesTest extends BaseRunOPSTest {

  private static final String ROOT = "runops_distances/";
  private static final String BRN_RESOURCE = ROOT + "emissions.brn";
  private static final String RCP_RESOURCE = ROOT + "receptor.rcp";

  private boolean withMaxDistance;
  private boolean withMinDistance;

  static List<Object[]> data() throws FileNotFoundException {
    return Stream.concat(resultsByKey(EmissionResultKey.NH3_DEPOSITION), resultsByKey(EmissionResultKey.NOX_DEPOSITION)).collect(Collectors.toList());
  }

  private static Stream<Object[]> resultsByKey(final EmissionResultKey key) {
    return Arrays.asList(
        new Object[] {key, "all", false, false}, // Should return data for all distances
        new Object[] {key, "only_min", false, true}, // Should return 0 for data within 5 km
        new Object[] {key, "only_max", true, false}, // Should return 0 for data beyond 25 km
        new Object[] {key, "both", true, true} // Should return 0 for both data within 5 km and beyond 25km
        ).stream();
  }

  /**
   * @param key emission result key to calculate
   * @param expected name of the expected results directory
   * @param withMaxDistance if max distance filter should be enabled.
   * @param withMinDistance if min distance filter should be enabled.
   */
  @ParameterizedTest
  @MethodSource("data")
  void testRunOPS(final EmissionResultKey key,  final String expected, final boolean withMaxDistance, final boolean withMinDistance)
      throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    this.withMaxDistance = withMaxDistance;
    this.withMinDistance = withMinDistance;
    assertRunOPS(new ResultDataFileReader(), key, ROOT + key.getSubstance().getName().toLowerCase() + "/" + expected + "/result.tab", false, true);
  }

  @Override
  protected OPSInputData getInputTestData(final EmissionResultKey key) throws IOException {
    final OPSInputData data =  OPSTestUtil.getInputTestData(key, 2030, getClass(), BRN_RESOURCE, RCP_RESOURCE);

    data.setMaxDistance(withMaxDistance);
    if (withMinDistance) {
      data.getEmissionSources().forEach((k, v) -> data.addMinDistanceGroupId(k));
    }
    return data;
  }
}
