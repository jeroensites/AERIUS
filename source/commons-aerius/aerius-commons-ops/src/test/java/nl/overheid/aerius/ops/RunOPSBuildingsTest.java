/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FileUtil;

/**
 *  Test class for RunOPS on several different substances.
 *  All tests will be ignored when OPS is not installed in the right location
 *  (see worker.properties file in src/test/resources), they are just won't test anything.
 */
class RunOPSBuildingsTest extends BaseRunOPSTest {

  private static final String BASE_TESTS = "runopsbuilding/";
  private static final String RECEPTORS_FILE = BASE_TESTS + "EW-profile.rcp";
  private static final int YEAR = 2018;
  private static final int EXPECTED_DIFF_RESULTS = 3;

  private String brnFile;

  static List<Object[]> data() throws FileNotFoundException {
    return FileUtil.getFilesWithExtension(new File(RunOPSBuildingsTest.class.getResource(BASE_TESTS).getFile()),
        (f, n) -> n.endsWith("brn")).stream().map(f -> new Object[] { f }).collect(Collectors.toList());
  }

  @ParameterizedTest
  @MethodSource("data")
  void testRunOPS(final File file) throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    final String parent = new File(file.getParent()).getName() + '/';
    brnFile = BASE_TESTS + parent + file.getName();
    final String referenceResultFile = BASE_TESTS + parent + "result.tab";
    final ResultDataFileReader reader = new ResultDataFileReader();
    final List<AssertionError> errors = assertRunOPS(reader, EmissionResultKey.NOX_CONCENTRATION, referenceResultFile, false, false);

    // The test runs an sub receptor calculator for some receptors and therefore returns a different result as a plain ops run for those receptors.
    // This assert check tests if the number of different results is the same as we expected given these sub receptors calculations should only
    // be different.
    assertEquals(EXPECTED_DIFF_RESULTS, errors.size(), "Different number of non matching calculation results as expected");
  }

  @Override
  protected OPSInputData getInputTestData(final EmissionResultKey key) throws IOException {
    return OPSTestUtil.getInputTestData(key, YEAR, getClass(), brnFile, RECEPTORS_FILE);
  }

}
