/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.ops.util.ResultDataFileReaderHelper;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class to check if sources near by receptors are correctly calculated with OPS.
 */
class RunOPSSubreceptorsTest extends BaseRunOPSTest {
  private static final Logger LOG = LoggerFactory.getLogger(RunOPSSubreceptorsTest.class);

  private static final String RESOURCES_DIRECTORY = "subreceptors/";
  private static final String RECEPTOR_FILE = RESOURCES_DIRECTORY + "receptors.rcp";
  private static final String EMISSION_SOURCE_FILE = "/emissions.brn";
  private static final String EXPECTED_RESULT_FILE = "/" + ResultDataFileReader.FILE_NAME;

  static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> objects = new ArrayList<>();

    objects.add(getParameters("hogebron_NOx"));
    objects.add(getParameters("hogebron_verplaatst_NOx"));
    objects.add(getParameters("lagebron_NOx"));
    objects.add(getParameters("lijnbron_NOx"));
    objects.add(getParameters("lijnbron_kort_NOx"));

    return objects;
  }

  private static Object[] getParameters(final String folder) {
    return new Object[] {RESOURCES_DIRECTORY + folder + EMISSION_SOURCE_FILE, RESOURCES_DIRECTORY + folder + EXPECTED_RESULT_FILE};
  }

  @ParameterizedTest
  @MethodSource("data")
  void testRunOPSWithNearbySources(final String emissionFile, final String resultFile)
      throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    LOG.debug("Run test on {} with resultfile {}", emissionFile, resultFile);
    final EmissionResultKey erk = EmissionResultKey.NOX_DEPOSITION;
    final ResultDataFileReader resultReader = new ResultDataFileReader();
    final File resultDirectory = config.getRunFilesDirectory();
    final int numberOfFilesBeforeRun = resultDirectory.listFiles().length;
    final OPSInputData inputData = getInputTestData(erk);

    inputData.setReceptors(OPSTestUtil.readRcpFile(getClass(), RECEPTOR_FILE).getObjects());
    inputData.setEmissionSources(1, OPSTestUtil.readBrnFile(Substance.NOX, getClass(), emissionFile).getObjects());

    setSources(inputData, emissionFile, Substance.NOX);
    setReceptors(inputData, RECEPTOR_FILE);
    final CalculationResult result = runOPS.run(inputData);
    final String message = "RunOPS for small receptor-source distances.";
    ResultDataFileReaderHelper.assertResults(RunOPSTest.class, message, resultReader, resultFile, erk, result.getResults().get(1), true);
    assertEquals(numberOfFilesBeforeRun, resultDirectory.listFiles().length, "Number of files/directories in" + resultDirectory);
  }
}
