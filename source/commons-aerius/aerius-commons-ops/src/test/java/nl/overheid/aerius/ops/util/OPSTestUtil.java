/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.util;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.EnumSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.io.AbstractLineReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.OPSVersion;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.io.BrnFileReader;
import nl.overheid.aerius.ops.io.RcpFileReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.util.OSUtils;

/**
 * Util class used by OPS unit tests.
 */
public final class OPSTestUtil {

  private static final Logger LOG = LoggerFactory.getLogger(OPSTestUtil.class);

  public static final String BRN_RESOURCE = "io/source/emission_2.brn";
  public static final int ZOOM_LEVEL_1_SURFACE = 10000;

  /**
   * Util class with only static methods.
   */
  private OPSTestUtil() {
  }

  public static OPSInputData getInputTestData(final EmissionResultKey key, final int year) throws IOException {
    return getInputTestData(key, year, OPSTestUtil.class, "../" + BRN_RESOURCE, "../io/receptor/receptor_0.rcp");
  }

  public static OPSInputData getInputTestData(final EmissionResultKey key, final int year, final Class<?> clazz, final String brnResource,
      final String rcpResource) throws IOException {
    final OPSInputData data = new OPSInputData(OPSVersion.VERSION, OPSTestUtil.ZOOM_LEVEL_1_SURFACE);
    //In test if key is pm25 use pm10, because there is not such thing as pm25 emissions. only pm10.
    final Substance substance = key.getSubstance() == Substance.PM25 ? Substance.PM10 : key.getSubstance();
    final LineReaderResult<OPSSource> brnSources = OPSTestUtil.readBrnFile(substance, clazz, brnResource);

    if (!brnSources.getExceptions().isEmpty()) {
      brnSources.getExceptions().forEach(e -> LOG.error("Error reading brn input file:{}", brnResource, e));
      fail("Errors reading brn input file: " + brnResource);
    }
    final ArrayList<OPSSource> es = new ArrayList<>(brnSources.getObjects());
    final LineReaderResult<OPSReceptor> readRcpFile = OPSTestUtil.readRcpFile(clazz, rcpResource);
    if (!readRcpFile.getExceptions().isEmpty()) {
      readRcpFile.getExceptions().forEach(e -> LOG.error("Error reading receptor file:{}", rcpResource, e));
      fail("Errors reading receptor file: " + rcpResource);
    }
    final ArrayList<OPSReceptor> receptors = new ArrayList<>(readRcpFile.getObjects());

    data.setYear(year);
    data.setEmissionSources(1, es);
    data.setSubstances(substance.hatch());
    data.setEmissionResultKeys(EnumSet.copyOf(key.hatch()));
    data.setReceptors(receptors);
    return data;
  }

  public static LineReaderResult<OPSReceptor> readRcpFile(final Class<?> clazz, final String resourceName) throws IOException {
    final RcpFileReader reader = new RcpFileReader();

    reader.setReadTerrainData(true);
    return readFile(reader, clazz, resourceName);
  }

  public static LineReaderResult<OPSSource> readBrnFile(final BrnFileReader reader, final Class<?> clazz, final String resourceName)
      throws IOException {
    return readFile(reader, clazz, resourceName);
  }

  public static LineReaderResult<OPSSource> readBrnFile(final Substance substance, final Class<?> clazz, final String resourceName)
      throws IOException {
    return readFile(new BrnFileReader(substance), clazz, resourceName);
  }

  public static <L> LineReaderResult<L> readFile(final AbstractLineReader<L> reader, final Class<?> clazz, final String resourceName)
      throws IOException {
    try (InputStream inputStream = getFileInputStream(clazz, resourceName)) {
      return reader.readObjects(inputStream);
    }
  }

  public static String readFile(final Class<?> clazz, final String resourceName) throws IOException {
    return OPSTestUtil.readFile(getFile(clazz, resourceName));
  }

  public static String readFile(final File file) throws IOException {
    return String.join(OSUtils.NL, Files.readAllLines(file.toPath())) + OSUtils.NL;
  }

  private static InputStream getFileInputStream(final Class<?> clazz, final String resourceName) throws FileNotFoundException {
    return new FileInputStream(getFile(clazz, resourceName));
  }

  private static File getFile(final Class<?> clazz, final String resourceName) throws FileNotFoundException {
    final URL resource = clazz.getResource(resourceName);
    if (resource == null) {
      throw new FileNotFoundException("Resource with name: '" + resourceName + "' not found relative to class:" + clazz.getPackage());
    }
    return new File(resource.getFile());
  }


}
