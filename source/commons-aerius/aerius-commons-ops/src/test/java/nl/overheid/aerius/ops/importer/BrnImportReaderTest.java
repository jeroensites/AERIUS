/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.importer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.io.ImportReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Test class for {@link BrnImportReader}.
 */
class BrnImportReaderTest {

  private static final String FILENAME = "BrnImportReader.brn";
  private final ImportReader ir = new BrnImportReader(true);

  private InputStream inputStream;
  private final SectorCategories categories = new SectorCategories();

  @BeforeEach
  void before() {
    inputStream = getClass().getResourceAsStream(FILENAME);
    final ArrayList<DiurnalVariationSpecification> diurnalVariations = new ArrayList<>();
    categories.setDiurnalVariations(diurnalVariations);
  }

  @AfterEach
  void after() throws IOException {
    inputStream.close();
  }

  @Test
  void detectTest() {
    assertTrue(ir.detect("sources.brn"), "Detect extension correcty");
    assertTrue(ir.detect("sources.BRN"), "Detect extension case insane");
    assertFalse(ir.detect("sources"), "Not detect other extensions");
  }

  @Test
  void testBRNWithoutSubstance() throws IOException, SQLException {
    final AeriusException e = assertThrows(AeriusException.class, () -> ir.read(FILENAME, inputStream, categories, null, new ImportParcel()));

    assertEquals(AeriusExceptionReason.BRN_WITHOUT_SUBSTANCE, e.getReason(), "Exception reason BRN without substance");
  }

  @Test
  void testBRNUnsupportedSubstance() throws IOException, SQLException {
    final AeriusException e = assertThrows(AeriusException.class,
        () -> ir.read(FILENAME, inputStream, categories, Substance.NOXNH3, new ImportParcel()));

    assertEquals(AeriusExceptionReason.BRN_SUBSTANCE_NOT_SUPPORTED, e.getReason(), "Exception reason BRN unsupported substance");
  }

  @Test
  void testBRNWithErrors() throws IOException, AeriusException {
    final ImportParcel result = new ImportParcel();
    ir.read(FILENAME, inputStream, categories, Substance.NH3, result);
    assertEquals(6, result.getSituation().getEmissionSourcesList().size(), "Should have read # rows correctly");
    assertEquals(1, result.getExceptions().size(), "Should have read # errors");
  }
}
