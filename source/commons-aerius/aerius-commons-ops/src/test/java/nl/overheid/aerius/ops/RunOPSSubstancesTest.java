/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *  Test class for RunOPS on several different substances.
 *  All tests will be ignored when OPS is not installed in the right location
 *  (see worker.properties file in src/test/resources), they are just won't test anything.
 */
class RunOPSSubstancesTest extends BaseRunOPSTest {

  private static final String EXPECTED_NOX = "runopsNOx/" + ResultDataFileReader.FILE_NAME;
  private static final String EXPECTED_NH3 = "runopsNH3/" + ResultDataFileReader.FILE_NAME;
  //  private static final String EXPECTED_PM10 = "runopsPM10/" + ResultDataFileReader.FILE_NAME;
  private static final String EXPECTED_TERRAIN_NOX = "runopsTerrainNOx/" + ResultDataFileReader.FILE_NAME;
  private static final String EXPECTED_TERRAIN_NH3 = "runopsTerrainNH3/" + ResultDataFileReader.FILE_NAME;
  //  private static final String EXPECTED_TERRAIN_PM10 = "runopsTerrainPM10/" + ResultDataFileReader.FILE_NAME;

  static List<Object[]> data() throws FileNotFoundException {
    return Arrays.asList(
        //without terrain stuff tests:
        new Object[] {EmissionResultKey.NOX_DEPOSITION, EXPECTED_NOX, false},
        new Object[] {EmissionResultKey.NOX_CONCENTRATION, EXPECTED_NOX, false},
        new Object[] {EmissionResultKey.NOX_DRY_DEPOSITION, EXPECTED_NOX, false},
        new Object[] {EmissionResultKey.NOX_WET_DEPOSITION, EXPECTED_NOX, false},

        new Object[] {EmissionResultKey.NH3_DEPOSITION, EXPECTED_NH3, false},
        new Object[] {EmissionResultKey.NH3_CONCENTRATION, EXPECTED_NH3, false},
        new Object[] {EmissionResultKey.NH3_DRY_DEPOSITION, EXPECTED_NH3, false},
        new Object[] {EmissionResultKey.NH3_WET_DEPOSITION, EXPECTED_NH3, false},

        //with terrain stuff tests:
        new Object[] {EmissionResultKey.NOX_DEPOSITION, EXPECTED_TERRAIN_NOX, true},
        new Object[] {EmissionResultKey.NOX_CONCENTRATION, EXPECTED_TERRAIN_NOX, true},
        new Object[] {EmissionResultKey.NOX_DRY_DEPOSITION, EXPECTED_TERRAIN_NOX, true},
        new Object[] {EmissionResultKey.NOX_WET_DEPOSITION, EXPECTED_TERRAIN_NOX, true},

        new Object[] {EmissionResultKey.NH3_DEPOSITION, EXPECTED_TERRAIN_NH3, true},
        new Object[] {EmissionResultKey.NH3_CONCENTRATION, EXPECTED_TERRAIN_NH3, true},
        new Object[] {EmissionResultKey.NH3_DRY_DEPOSITION, EXPECTED_TERRAIN_NH3, true},
        new Object[] {EmissionResultKey.NH3_WET_DEPOSITION, EXPECTED_TERRAIN_NH3, true}
        );
  }

  @ParameterizedTest
  @MethodSource("data")
  void testRunOPS(final EmissionResultKey key, final String expected, final boolean withTerrain)
      throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    final ResultDataFileReader reader = new ResultDataFileReader();
    assertRunOPS(reader, key, expected, withTerrain, true);
  }
}
