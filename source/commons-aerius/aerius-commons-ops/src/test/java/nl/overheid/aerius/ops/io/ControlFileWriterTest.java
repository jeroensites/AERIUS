/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.util.OSUtils;

/**
 * Test class for {@link ControlFileWriter}.
 */
class ControlFileWriterTest {

  private static final String SETTINGS10 = "settings_10.ctr";
  private static final String LINUX_SETTINGS10 = "linux_settings_10.ctr";
  private static final String RUN_ID10 = "RAY310";

  private static final String SETTINGS11 = "settings_11.ctr";
  private static final String LINUX_SETTINGS11 = "linux_settings_11.ctr";
  private static final String RUN_ID11 = "AB305";

  private static final String SETTINGS17 = "settings_17.ctr";
  private static final String LINUX_SETTINGS17 = "linux_settings_17.ctr";
  private static final String RUN_ID17 = "14AK31";

  private static final String SETTINGS17_CUSTOM_METEO = "settings_17_custom_meteo.ctr";
  private static final String LINUX_SETTINGS17_CUSTOM_METEO = "linux_settings_17_custom_meteo.ctr";
  private static final String CUSTOM_METEO_FILE = "the_meteo_file";

  private static final String SETTINGS17_OPS_OPTIONS = "settings_17_ops_options.ctr";
  private static final String LINUX_SETTINGS17_OPS_OPTIONS = "linux_settings_17_ops_options.ctr";

  private static final File OPS_ROOT = new File(OSUtils.isWindows() ? "C:\\OPS\\" : "/opt/aerius/ops/ops_current_version/");
  private static final File OPS_RUN = new File(OSUtils.isWindows() ? "C:\\OPS\\run" : "//tmp/ops/");

  private final OPSConfiguration cfo = new OPSConfiguration();
  private OPSOptions opsOptions = null;

  @TempDir
  File tempDir;

  ControlFileWriterTest() {
    cfo.setOpsRoot(OPS_ROOT);
  }

  @Test
  void writeFile10Test() throws IOException {
    assertControlFile(RUN_ID10, Substance.PM10, SETTINGS10, LINUX_SETTINGS10);
  }

  @Test
  void writeFile11Test() throws IOException {
    assertControlFile(RUN_ID11, Substance.NOX, SETTINGS11, LINUX_SETTINGS11);
  }

  @Test
  void writeFile17Test() throws IOException {
    assertControlFile(RUN_ID17, Substance.NH3, SETTINGS17, LINUX_SETTINGS17);
  }

  @Test
  void writeCustomMeteoTest() throws IOException {
    cfo.setSettingsMeteoFile(CUSTOM_METEO_FILE);
    assertControlFile(RUN_ID17, Substance.NH3, SETTINGS17_CUSTOM_METEO, LINUX_SETTINGS17_CUSTOM_METEO);
  }

  @Test
  void writeEmptyOpsOptionsTest() throws IOException {
    opsOptions = new OPSOptions();
    assertControlFile(RUN_ID17, Substance.NH3, SETTINGS17, LINUX_SETTINGS17);
  }

  @Test
  void writeCustomOpsOptionsTest() throws IOException {
    opsOptions = new OPSOptions();
    opsOptions.setYear(1994);
    opsOptions.setCompCode(2);
    opsOptions.setMolWeight(3.3);
    opsOptions.setPhase(4);
    opsOptions.setLoss(9);
    opsOptions.setDiffCoeff("5.2");
    opsOptions.setWashout("7");
    opsOptions.setConvRate("Some conv rate");
    opsOptions.setRoughness(234.2);
    assertControlFile(RUN_ID17, Substance.NH3, SETTINGS17_OPS_OPTIONS, LINUX_SETTINGS17_OPS_OPTIONS);
  }

  @Test
  void testToOpsMeteoFile() throws IOException {
    assertNull(ControlFileWriter.toOpsMeteoFile(null), "Absent meteo");
    assertEquals("a005105c.*", ControlFileWriter.toOpsMeteoFile(new Meteo(2005)), "Single year meteo");
    assertEquals("m005114c.*", ControlFileWriter.toOpsMeteoFile(new Meteo(2005, 2014)), "Multi year meteo");
  }

  private void assertControlFile(final String runId, final Substance substance, final String refFileWin, final String refFileLinux) throws IOException {
    ControlFileWriter.writeFile(cfo, tempDir, runId, substance, 2010, null, opsOptions);
    //If linux, we can't really compare these files: either we make them equal and OPS won't work properly
    //or we make OPS work and the test will fail (reference doesn't match the output).
    final String referenceFile = OSUtils.isWindows() ? refFileWin : refFileLinux;

    final String output = getOutputFile(tempDir, runId);
    final String reference = OPSTestUtil.readFile(getClass(), referenceFile);

    assertEquals(reference, output, "WriteFile Test Substance " + substance);
  }

  private String getOutputFile(final File path, final String runId) throws IOException {
    final String content = OPSTestUtil.readFile(new File(path, ControlFileWriter.getControlFileName()));
    return content.replace(path.toString(), OPS_RUN.toString() + File.separator + runId);
  }
}
