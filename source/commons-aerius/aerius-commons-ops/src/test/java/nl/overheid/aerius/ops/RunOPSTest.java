/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.RcpFileReader;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.OPSOptions;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 *  Test class for RunOPS.
 *  All tests will still work without OPS installed in the right location
 *  (see worker.properties file in src/test/resources), they are just won't test anything.
 */
class RunOPSTest extends BaseRunOPSTest {

  @Test
  void testRunFailedOPS() throws IOException, InterruptedException, OPSInvalidVersionException {
    final OPSInputData data = getInputTestData(EmissionResultKey.NOX_DEPOSITION);
    // Diurnal variation 9 does not work (arbitrarily chosen value).
    data.getEmissionSources().get(1).iterator().next().setDiurnalVariation(9);
    assertThrows(AeriusException.class, () -> runOPS.run(data), "Should throw an exception for invalid diurnal variation.");
  }

  /**
   * Test if OPS triggers an error if to many input objects are passed to OPS. This test only runs when a new version
   * of OPS is used, because the test can take up-to 10 minutes and only tests the limits of OPS.
   *
   * If a new version of OPS is installed this test should be run at least once before updating the OPS version number here
   * to be sure the test still runs as expected or possible if any input can be handled this test can be removed.
   * @throws OPSInvalidVersionException
   */
  @Test
  void runStackOverflowOPSTest() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    if (!config.getOPSRoot().toString().contains("5.0.1.3")) {
      final OPSInputData data = getInputTestData(EmissionResultKey.NH3_DEPOSITION);
      try (InputStream inputStream = getClass().getResourceAsStream("stackoverflow/" + "receptors.rcp")) {
        final LineReaderResult<OPSReceptor> receptors = new RcpFileReader().readObjects(inputStream);
        data.setReceptors(receptors.getObjects());
      }
      try {
        runOPS.run(data);
        fail("The OPS stackoverflow unit test didn't throw an exception, so this new version seems improved!");
      } catch (final AeriusException e) {
        assertSame(AeriusExceptionReason.OPS_INTERNAL_EXCEPTION, e.getReason(), "Reason should be ops internal exception");
      }
      fail("Stackoverflow test ran with expected failure. This means you need to update the ops version number in this test.");
    }
  }

  @Test
  void testFileRemainsAfterFailedOPS() throws IOException, InterruptedException, OPSInvalidVersionException {
    final File resultDirectory = config.getRunFilesDirectory();
    final int numberOfFilesBeforeRun = resultDirectory.listFiles().length;
    testRunFailedOPS();
    assertEquals(numberOfFilesBeforeRun + 1, resultDirectory.listFiles().length, "Number of files/directories in" + resultDirectory);
  }

  @Test
  void testRunOPSMultipleTimesAtSameTime() throws Throwable {
    final EmissionResultKey key = EmissionResultKey.NH3_DEPOSITION;
    final OPSInputData data = getInputTestData(key);
    final Set<Callable<Boolean>> tasks = new HashSet<>();
    final int numberOfTasks = 10;
    for (int i = 0; i < numberOfTasks; i++) {
      tasks.add(new OPSRunCallable(runOPS, data));
    }
    final ExecutorService service = Executors.newFixedThreadPool(10);
    final List<Future<Boolean>> results = service.invokeAll(tasks);
    int validResults = 0;
    try {
      for (final Future<Boolean> result : results) {

        // wait for all results to be in.
        if (result.get()) {
          validResults++;
        }
      }
      assertEquals(numberOfTasks, validResults, "Right results");
    } catch (final ExecutionException e) {
      throw e.getCause();
    } finally {
      service.shutdownNow();
    }
  }

  @Test
  void testRunOPSInvalidDataNH3() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertOPSInvalidData(EmissionResultKey.NH3_DEPOSITION);
  }

  @Test
  void testRunOPSInvalidDataNOX() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertOPSInvalidData(EmissionResultKey.NOX_DEPOSITION);
  }

  @Disabled("Disable pm10 calculations because the results are not representative.")
  @Test
  void testRunOPSInvalidDataPM10() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertOPSInvalidData(EmissionResultKey.PM10_CONCENTRATION);
  }

  private void assertOPSInvalidData(final EmissionResultKey key)
      throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    OPSInputData data = new OPSInputData(OPSVersion.VERSION, OPSTestUtil.ZOOM_LEVEL_1_SURFACE);

    // Empty list emission sources does not fail, just returns receptors with 0.0 deposition.

    data = getInputTestData(key);
    adjustAndTestValue(data, "setReceptors", null,
        Collection.class, data, true, "Null receptors");

    data = getInputTestData(key);
    adjustAndTestValue(data, "setReceptors", new ArrayList<OPSReceptor>(),
        Collection.class, data, true, "Empty list receptors");
  }

  @Test
  void testRunOPSReceptorData() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    final EmissionResultKey nh3ConKey = EmissionResultKey.NH3_CONCENTRATION;

    OPSInputData data = getInputTestData(nh3ConKey);
    // ID can be higher then the maximum value of integer, so no point testing min - 1, max + 1
    OPSReceptor receptor = data.getReceptors().iterator().next();
    List<AeriusResultPoint> results = adjustAndTestValue(receptor, "setId", OPSLimits.RECEPTOR_ID_MINIMUM,
        Integer.TYPE, data, false, "Receptor id minimum");
    assertNotNull(results, "Should be a result object");
    assertEquals(data.getReceptors().size(), results.size(), "Returned list size");
    assertEquals(OPSLimits.RECEPTOR_ID_MINIMUM, results.get(0).getId(), "Returned ID");

    data = getInputTestData(nh3ConKey);
    receptor = data.getReceptors().iterator().next();
    results = adjustAndTestValue(receptor, "setId", OPSLimits.RECEPTOR_ID_MAXIMUM,
        Integer.TYPE, data, false, "Receptor id maximum");
    assertNotNull(results, "Should be a result object");
    assertEquals(data.getReceptors().size(), results.size(), "Returned list size");
    assertEquals(OPSLimits.RECEPTOR_ID_MAXIMUM, results.get(0).getId(), "Returned ID");

    data = getInputTestData(nh3ConKey);
    receptor = data.getReceptors().iterator().next();
    assertMinAndMax(receptor, "setX",
        OPSLimits.X_COORDINATE_MINIMUM, OPSLimits.X_COORDINATE_MAXIMUM, Double.TYPE, data,
        "X-coordinate");

    data = getInputTestData(nh3ConKey);
    receptor = data.getReceptors().iterator().next();
    assertMinAndMax(receptor, "setY",
        OPSLimits.Y_COORDINATE_MINIMUM, OPSLimits.Y_COORDINATE_MAXIMUM, Double.TYPE, data,
        "Y-coordinate");
  }

  @Test
  void testRunOPSSourceNH3Data() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertOPSTestSourceData(EmissionResultKey.NH3_DEPOSITION);
  }

  @Test
  void testRunOPSSourceNOxData() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertOPSTestSourceData(EmissionResultKey.NOX_DEPOSITION);
  }

  @Disabled("Disable pm10 calculations because the results are not representative.")
  @Test
  void testRunOPSSourcePM10Data() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertOPSTestSourceData(EmissionResultKey.PM10_CONCENTRATION);
  }

  private void assertOPSTestSourceData(final EmissionResultKey key)
      throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    OPSInputData data = null;

    //id has no limit: default used is 0 in OPS.

    data = getInputTestData(key);
    assertMinAndMax(data.getEmissionSources().get(1).iterator().next().getPoint(), "setX",
        OPSLimits.X_COORDINATE_MINIMUM, OPSLimits.X_COORDINATE_MAXIMUM, Double.TYPE, data, "X-coordinate");

    data = getInputTestData(key);
    assertMinAndMax(data.getEmissionSources().get(1).iterator().next().getPoint(), "setY",
        OPSLimits.Y_COORDINATE_MINIMUM, OPSLimits.Y_COORDINATE_MAXIMUM, Double.TYPE, data, "Y-coordinate");

    data = getInputTestData(key);
    // only test maximum for setHeatContent
    assertMax(data.getEmissionSources().get(1).iterator().next(), "setHeatContent", OPSLimits.SOURCE_HEAT_CONTENT_MAXIMUM, Double.TYPE, data,
        "Heat content");

    data = getInputTestData(key);
    assertMinAndMax(data.getEmissionSources().get(1).iterator().next(), "setEmissionHeight",
        OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM, OPSLimits.SOURCE_EMISSION_HEIGHT_MAXIMUM, Double.TYPE, data,
        "Height");

    data = getInputTestData(key);
    assertMinAndMax(data.getEmissionSources().get(1).iterator().next(), "setDiameter",
        OPSLimits.SOURCE_DIAMETER_MINIMUM, OPSLimits.SOURCE_DIAMETER_MAXIMUM, Integer.TYPE, data,
        "Diameter");

    data = getInputTestData(key);
    assertMinAndMax(data.getEmissionSources().get(1).iterator().next(), "setSpread",
        OPSLimits.SOURCE_SPREAD_MINIMUM, OPSLimits.SOURCE_SPREAD_MAXIMUM, Double.TYPE, data,
        "Spread");

    data = getInputTestData(key);
    //difference between NH3 and others due to 'hack' in OPS.
    if (key == EmissionResultKey.NH3_DEPOSITION) {
      assertMinAndMax(data.getEmissionSources().get(1).iterator().next(), "setDiurnalVariation",
          OPSLimits.SOURCE_DIURNAL_VARIATION_MINIMUM,
          OPSLimits.SOURCE_DIURNAL_VARIATION_MAXIMUM,
          Integer.TYPE, data,
          "Diurnal Variation");
    } else {
      //Can't really test maximum if it's not NH3. It is 3 instead of 5 as 4 and 5 are NH3-only. Using 4 or 5 results in AeriusException.
      assertMin(data.getEmissionSources().get(1).iterator().next(), "setDiurnalVariation",
          OPSLimits.SOURCE_DIURNAL_VARIATION_MINIMUM,
          Integer.TYPE, data,
          "Diurnal Variation");
    }

    data = getInputTestData(key);
    assertMinAndMax(data.getEmissionSources().get(1).iterator().next(), "setArea",
        OPSLimits.SOURCE_AREA_MINIMUM, OPSLimits.SOURCE_AREA_MAXIMUM, Integer.TYPE, data,
        "Area");

    // Test not done for substance PM10 because minimum value 0 is invalid for PM10.
    if (key != EmissionResultKey.PM10_CONCENTRATION) {
      data = getInputTestData(key);
      assertMinAndMax(data.getEmissionSources().get(1).iterator().next(), "setParticleSizeDistribution",
          OPSLimits.SOURCE_PARTICLE_SIZE_DISTRIBUTION_MINIMUM + 0, OPSLimits.SOURCE_PARTICLE_SIZE_DISTRIBUTION_MAXIMUM,
          Integer.TYPE, data,
          "Particle Size Distribution");
    }
  }

  @Test
  void testRunOPSExpectedResults() throws IOException, AeriusException, OPSInvalidVersionException {
    final OPSInputData data = OPSTestUtil.getInputTestData(EmissionResultKey.NOX_DEPOSITION, 2019);

    final EnumSet<EmissionResultKey> resultKeysToReturn = EnumSet.of(EmissionResultKey.NOX_CONCENTRATION);
    data.setEmissionResultKeys(resultKeysToReturn);
    CalculationResult result = runOPS.run(data);
    assertValidateResults(result, EnumSet.of(EmissionResultKey.NOX_CONCENTRATION));

    resultKeysToReturn.add(EmissionResultKey.NOX_DEPOSITION);
    result = runOPS.run(data);
    assertValidateResults(result, EnumSet.of(EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NOX_DEPOSITION,
        EmissionResultKey.NOXNH3_DEPOSITION));

    resultKeysToReturn.add(EmissionResultKey.NH3_DEPOSITION);
    result = runOPS.run(data);
    //no NH3 stuff, we aren't calculating that one...
    assertValidateResults(result, EnumSet.of(EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NOX_DEPOSITION,
        EmissionResultKey.NOXNH3_DEPOSITION));

    data.getSubstances().add(Substance.NH3);
    result = runOPS.run(data);
    //now we are calculating NH3 as well, so the run should return results (even though no sources actually have emissions for it)
    assertValidateResults(result, EnumSet.of(EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NOX_DEPOSITION,
        EmissionResultKey.NOXNH3_DEPOSITION, EmissionResultKey.NH3_DEPOSITION));

    //    Disable pm10 calculations because the results are not representative.
    //    data.setSubstances(Substance.PM10.hatch());
    //    resultKeysToReturn.clear();
    //    resultKeysToReturn.add(EmissionResultKey.PM25_CONCENTRATION);
    //    result = runOPS.run(data);
    //    //no sources with the substance, but there will be results (even though it's all 0.0)...
    //    assertValidateResults(result, EnumSet.of(EmissionResultKey.PM25_CONCENTRATION));
  }

  @Test
  void testRunOPSCustomOpsOptions() throws IOException, AeriusException, OPSInvalidVersionException {
    final OPSInputData data = OPSTestUtil.getInputTestData(EmissionResultKey.NOX_DEPOSITION, 2019);
    final OPSOptions opsOptions = new OPSOptions();
    opsOptions.setYear(1994);
    opsOptions.setCompCode(3);
    opsOptions.setMolWeight(3.3);
    opsOptions.setPhase(1);
    opsOptions.setLoss(1);
    opsOptions.setDiffCoeff("0.2");
    opsOptions.setWashout("1");
    opsOptions.setConvRate("");
    opsOptions.setRoughness(2.2);
    data.setOpsOptions(opsOptions);

    final EnumSet<EmissionResultKey> resultKeysToReturn = EnumSet.of(EmissionResultKey.NOX_CONCENTRATION);
    data.setEmissionResultKeys(resultKeysToReturn);
    final CalculationResult result = runOPS.run(data);
    assertValidateResults(result, EnumSet.of(EmissionResultKey.NOX_CONCENTRATION));
  }

  @Test
  void testRunOPSCustomOpsOptionsIncorrect() throws IOException, AeriusException, OPSInvalidVersionException {
    final OPSInputData data = OPSTestUtil.getInputTestData(EmissionResultKey.NOX_DEPOSITION, 2019);
    final OPSOptions opsOptions = new OPSOptions();
    opsOptions.setPhase(2);
    data.setOpsOptions(opsOptions);

    final EnumSet<EmissionResultKey> resultKeysToReturn = EnumSet.of(EmissionResultKey.NOX_CONCENTRATION);
    data.setEmissionResultKeys(resultKeysToReturn);
    assertThrows(AeriusException.class, () -> runOPS.run(data));
  }

  private void assertValidateResults(final CalculationResult result, final Set<EmissionResultKey> resultKeysExpected) {
    final List<AeriusResultPoint> resultPoints = result.getResults().get(1);

    assertFalse(resultPoints.isEmpty(), "Should have results to check");
    for (final AeriusResultPoint resultPoint : resultPoints) {
      for (final EmissionResultKey key : EmissionResultKey.values()) {
        if (resultKeysExpected.contains(key)) {
          assertTrue(resultPoint.getEmissionResults().hasResult(key), "Should have some results for " + key);
        } else {
          assertFalse(resultPoint.getEmissionResults().hasResult(key), "Shouldn't have any results for " + key);
        }
      }
    }
  }

  private void assertMinAndMax(final Object adjustValueOn, final String methodNameToAdjustValue,
      final int minimumValue, final int maximumValue, final Class<?> valueType, final OPSInputData data, final String extraInfo)
      throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertMin(adjustValueOn, methodNameToAdjustValue, minimumValue, valueType, data, extraInfo);

    assertMax(adjustValueOn, methodNameToAdjustValue, maximumValue, valueType, data, extraInfo);
  }

  private void assertMin(final Object adjustValueOn, final String methodNameToAdjustValue,
      final int minimumValue, final Class<?> valueType, final OPSInputData data, final String extraInfo)
      throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    adjustAndTestValue(adjustValueOn, methodNameToAdjustValue, minimumValue - 1, valueType, data, true, extraInfo + " minimum");
    adjustAndTestValue(adjustValueOn, methodNameToAdjustValue, minimumValue, valueType, data, false, extraInfo + " minimum");
  }

  private void assertMax(final Object adjustValueOn, final String methodNameToAdjustValue,
      final int maximumValue, final Class<?> valueType, final OPSInputData data, final String extraInfo)
      throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    adjustAndTestValue(adjustValueOn, methodNameToAdjustValue, maximumValue, valueType, data, false, extraInfo + " maximum");
    adjustAndTestValue(adjustValueOn, methodNameToAdjustValue, maximumValue + 1, valueType, data, true, extraInfo + " maximum");
  }

  private void adjustValue(final Object adjustValueOn, final String methodNameToAdjustValue, final Object value, final Class<?> valueType) {
    final Class<? extends Object> cls = adjustValueOn.getClass();
    try {
      final Method method = cls.getMethod(methodNameToAdjustValue, valueType);
      method.invoke(adjustValueOn, value);
    } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      fail("Error while executing method. " + e.getMessage());
    }
  }

  private List<AeriusResultPoint> adjustAndTestValue(final Object adjustValueOn, final String methodNameToAdjustValue, final Object value,
      final Class<?> valueType, final OPSInputData data, final boolean shouldFail, final String extraInfo)
      throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    adjustValue(adjustValueOn, methodNameToAdjustValue, value, valueType);

    List<AeriusResultPoint> results = null;
    if (shouldFail) {
      runOPSExpectInvalidInputException(data, extraInfo);
    } else {
      results = runOPS.run(data).getResults().get(1);
      assertFalse(results.isEmpty(), "Returned result shouldn't be empty. Test: " + extraInfo);
    }
    return results;
  }

  private void runOPSExpectInvalidInputException(final OPSInputData data, final String testDescription) throws IOException,
      InterruptedException, AeriusException, OPSInvalidVersionException {
    try {
      runOPS.run(data);
      fail("Input should have been qualified as invalid. Test was: " + testDescription);
    } catch (final AeriusException e) {
      assertSame(AeriusExceptionReason.OPS_INPUT_VALIDATION, e.getReason(), "Check on correct reason");
      assertFalse(e.getArgs()[0].isEmpty(), "Violations in exception shouldn't be empty.");
    }
  }

  class OPSRunCallable implements Callable<Boolean> {

    private final RunOPS ops;
    private final OPSInputData data;

    public OPSRunCallable(final RunOPS ops, final OPSInputData data) {
      this.ops = ops;
      this.data = data;
    }

    @Override
    public Boolean call() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
      return !ops.run(data).getResults().isEmpty();
    }
  }
}
