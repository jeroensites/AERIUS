/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.BrnFileReaderV0;
import nl.overheid.aerius.ops.io.RcpFileReader;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.ops.util.ResultDataFileReaderHelper;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.worker.PropertiesUtil;

/**
 * Base class for running OPS tests.
 */
public abstract class BaseRunOPSTest {

  private static final Logger LOG = LoggerFactory.getLogger(BaseRunOPSTest.class);

  protected static final int YEAR = 2014;

  protected RunOPS runOPS;
  protected OPSConfiguration config;

  private boolean keepGeneratedFiles;

  @BeforeEach
  public void setUp() throws Exception {
    config = getConfig();
    keepGeneratedFiles = true; //set to true before assume, if assume returns false tearDown() will not be run.
    assertTrue(checkOPSAvailability(), "OPS not installed.");
    config.getRunFilesDirectory().mkdirs();
    runOPS = new RunOPS(config);
  }

  @AfterEach
  public void tearDown() throws Exception {
    if (!keepGeneratedFiles) {
      for (final File file : config.getRunFilesDirectory().listFiles()) {
        try {
          UUID.fromString(file.getName());
          FileUtil.removeDirectoryRecursively(file.toPath());
        } catch (final IllegalArgumentException e) {
          // If argument for UUID.fromString does not follow UUID rules, it'll throw an IllegalArgumentException
          // When that's the case, we don't want to delete (we didn't generate it).
          // Should make results where runFilesPath is defined as C:\\ less dramatic.
        }
      }
    }
  }

  protected OPSConfiguration getConfig() throws IOException {
    final OPSWorkerFactory factory = new OPSWorkerFactory();
    return factory.createOPSConfiguration(factory.createConfiguration(PropertiesUtil.getFromTestPropertiesFile("ops")));
  }

  private boolean checkOPSAvailability() {
    final File opsRoot = config.getOPSRoot();
    final boolean available = opsRoot != null && opsRoot.exists();
    if (!available) {
      LOG.warn("Could not find OPS root directory ({}). {}", opsRoot, Thread.currentThread().getStackTrace()[2]);
    }
    return available;
  }

  protected List<AssertionError> assertRunOPS(final ResultDataFileReader reader, final EmissionResultKey erk, final String referenceFile,
      final boolean withTerrain, final boolean failOnResultDiff)
          throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    final File resultDirectory = config.getRunFilesDirectory();
    final int numberOfFilesBeforeRun = resultDirectory.listFiles().length;
    final CalculationResult result = runOPS.run(withTerrain ? getTerrainInputTestData(erk) : getInputTestData(erk));
    final String message = "RunOPS: Input " + erk + ". reference file: " + referenceFile;
    final List<AssertionError> errors =
        ResultDataFileReaderHelper.assertResults(getClass(), message, reader, referenceFile, erk, result.getResults().get(1), failOnResultDiff);

    assertEquals(numberOfFilesBeforeRun, resultDirectory.listFiles().length, "Number of files/directories in" + resultDirectory);
    return errors;
  }

  protected OPSInputData getInputTestData(final EmissionResultKey key) throws IOException {
    return OPSTestUtil.getInputTestData(key, YEAR);
  }

  private OPSInputData getTerrainInputTestData(final EmissionResultKey key) throws IOException {
    return OPSTestUtil.getInputTestData(key, YEAR, getClass(), OPSTestUtil.BRN_RESOURCE, "io/receptor/receptor_terrain.rcp");
  }

  protected void setSources(final OPSInputData inputData, final String sourcesFile, final Substance substance)
      throws IOException, FileNotFoundException {
    try (InputStream inputStream = getFileInputStream(sourcesFile)) {
      final BrnFileReaderV0 emissionFileReader = new BrnFileReaderV0(substance);
      final LineReaderResult<OPSSource> results = emissionFileReader.readObjects(inputStream);
      inputData.setEmissionSources(1, new ArrayList<>(results.getObjects()));
    }
  }

  protected void setReceptors(final OPSInputData inputData, final String receptorFile) throws IOException, FileNotFoundException {
    try (InputStream inputStream = getFileInputStream(receptorFile)) {
      final RcpFileReader opsReceptorFileReader = new RcpFileReader();
      opsReceptorFileReader.setReadTerrainData(true);
      final LineReaderResult<OPSReceptor> receptors = opsReceptorFileReader.readObjects(inputStream);

      assertNotNull(receptors, "Check if has receptors.");
      inputData.setReceptors(new ArrayList<>(receptors.getObjects()));
    }
  }

  protected InputStream getFileInputStream(final String fileName) throws FileNotFoundException {
    final File file = new File(getClass().getResource(fileName).getFile());

    return new FileInputStream(file);
  }

}
