/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.shared.exception;

import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Enum with a list of errors that can be returned by the server via a {@link AeriusException}. Each reason should state in the JavaDoc the
 * arguments to be passed. It is not enforced at compile time if this matches, therefore be careful and test it.
 */
public enum AeriusExceptionReason implements Reason {

  // Internal errors codes < 1000, these will be interpreted as such. USE WISELY!

  /**
   * Unspecified internal server error.
   */
  INTERNAL_ERROR(666),
  /**
   * Database error. Can be incorrect query or missing database connection.
   */
  SQL_ERROR(667),
  /**
   * Database content error. For worker to test required database constants.
   */
  SQL_ERROR_CONSTANTS_MISSING(668),

  // Generic calculator errors start at 1000
  /**
   * Trying to start a calculation but there are no sources to calculate.
   *
   * @param 0 name of the project
   */
  CALCULATION_NO_SOURCES(1001),
  /**
   * Number of sources exceeds maximum.
   *
   * @param 0 maximum number of allowed sources
   * @param 1 name of the source
   */
  LIMIT_SOURCES_EXCEEDED(1003),
  /**
   * Length of a line exceeds maximum length.
   *
   * @param 0 name of the source
   * @param 1 allowed maximum line length
   * @param 2 length of source
   */
  LIMIT_LINE_LENGTH_EXCEEDED(1004),
  /**
   * Surface of a polygon exceeds maximum surface.
   *
   * @param 0 name of the source
   * @param 1 allowed maximum surface
   * @param 2 surface of source
   */
  LIMIT_POLYGON_SURFACE_EXCEEDED(1005),

  /**
   * Geocoder call failed.
   */
  GEOCODER_ERROR(1008),
  /**
   * Calculation to complex for calculator (to many engine sources to calculate).
   *
   * @param 0 name of the project
   */
  CALCULATION_TO_COMPLEX(1009),
  /**
   * Roads are only allowed to be LineStrings, or networks containing LineStrings.
   */
  ROAD_GEOMETRY_NOT_ALLOWED(1010),
  /**
   * Proposed situation missing for PAA output type
   */
  CALCULATION_PAA_PROPOSED_SITUATION_MISSING(1011),
  /**
   * The calculation contained (custom) points with duplicate IDs.
   */
  CALCULATION_DUPLICATE_POINT_IDS(1017),

  // Import errors
  /**
   * The POST doesn't contain an file to import.
   */
  IMPORT_FILE_NOT_SUPPLIED(5001),
  /**
   * The file could not be read.
   */
  IMPORT_FILE_COULD_NOT_BE_READ(5002),
  /**
   * The request to the server contains invalid values in the POST and could not be processed.
   */
  FAULTY_REQUEST(5003),
  /**
   * The file to be imported is not supported.
   *
   * @param 0 filename
   */
  IMPORT_FILE_UNSUPPORTED(5004),
  /**
   * The file with extension to be imported is not allowed. The import functionality that throws this would like the user to know that only specific
   * extensions/filetypes are allowed..
   */
  IMPORT_FILE_TYPE_NOT_ALLOWED(5005),
  /**
   * Uploaded file is missing the required ID.
   */
  IMPORT_REQUIRED_ID_MISSING(5006),
  /**
   * Uploaded entry is a duplicate and cannot be imported.
   */
  IMPORT_DUPLICATE_ENTRY(5007),
  /**
   * Uploaded file should contain sources and has none.
   */
  IMPORT_NO_SOURCES_PRESENT(5008),
  /**
   * Uploaded file should contain results and has none.
   *
   * @param 0 reference
   */
  IMPORT_NO_RESULTS_PRESENT(5009),
  /**
   * Uploaded file should contain calculation points and has none.
   */
  IMPORT_NO_CALCULATION_POINTS_PRESENT(5010),
  /**
   * The supplied received date is not valid.
   */
  IMPORT_NO_VALID_RECEIVED_DATE(5011),
  /**
   * The imported file could not be found.
   * This could be because something changed on the server, or because importing didn't succeed in the first place.
   */
  IMPORTED_FILE_NOT_FOUND(5012),

  /**
   * Used when actual reason for the exception when reading a row in a file is unknown.
   *
   * @param 0 line number
   */
  IO_EXCEPTION_UNKNOWN(5050),
  /**
   * Used when a number is incorrectly formatted in the file.
   *
   * @param 0 line number
   * @param 1 column name
   * @param 2 column content
   */
  IO_EXCEPTION_NUMBER_FORMAT(5051),
  /**
   * Used when a row does not have enough fields.
   *
   * @param 0 line number
   */
  IO_EXCEPTION_NOT_ENOUGH_FIELDS(5052),

  // Import BRN file errors.
  /**
   * A .BRN file is supplied without a substance (required).
   */
  BRN_WITHOUT_SUBSTANCE(5101),
  /**
   * A substance is supplied that is not supported.
   *
   * @param 0 substance given as input
   * @param 1 list of allowed substances.
   */
  BRN_SUBSTANCE_NOT_SUPPORTED(5102),

  // Import PAA file errors.
  /**
   * The validation for importing a PDF failed (incorrect metadata in PDF).
   */
  PAA_VALIDATION_FAILED(5301),

  /**
   * The timeout for exporting the PDF has been reached.
   */
  PAA_EXPORT_TIMEOUT(5302),

  // Import ZIP file errors.

  /**
   * ZIP archive file does not contain any useable files.
   */
  ZIP_WITHOUT_USABLE_FILES(5401),
  /**
   * ZIP archive file contains to many useable files.
   */
  ZIP_TOO_MANY_USABLE_FILES(5402),

  // OPS file and calculation errors.

  /**
   * OPS run failed with error.
   *
   * @param 0 ops message
   */
  OPS_INTERNAL_EXCEPTION(6101),
  /**
   * OPS was fed with input that is outside the expected scope of ops
   *
   * @param 0 the validation errors
   */
  OPS_INPUT_VALIDATION(6102),

  // Standard file parsing errors.

  // SRM2 file and calculation errors.
  /**
   * Could not find road properties for given road type.
   *
   * @param 0 line number
   * @param 1 column content
   * @param 2 found road parameters
   */
  SRM2IO_EXCEPTION_NO_ROAD_PROPERTIES(6201),
  /**
   * The current hardware can't calculate the given number of sources.
   *
   * @param 0 number of input sources
   */
  SRM2_TO_MANY_ROAD_SEGMENTS(6202),
  /**
   * There is no pre-processed meteo and background data from presrm for the given year.
   *
   * @param 0 year to be calculated
   */
  SRM2_NO_PRESRM_DATA_FOR_YEAR(6203),
  /**
   * Could not find expected column header(s) in the specified file.
   *
   * @param 0 column(s) that were expected
   */
  SRM2_MISSING_COLUMN_HEADER(6204),
  /**
   * Column contained incorrect (empty or unparseable) value in the specified file.
   *
   * @param 0 line number
   * @param 1 column for which a value was expected
   */
  SRM2_INCORRECT_EXPECTED_VALUE(6205),
  /**
   * Could not parse the WKT value in the specified file as a valid geometry.
   *
   * @param 0 line number
   * @param 1 value that couldn't be parsed as a valid geometry.
   */
  SRM2_INCORRECT_WKT_VALUE(6206),
  /**
   * Warning NSL Legacy file support is not supported and will be remove in future.
   *
   */
  NSL_LEGACY_FILESUPPORT_WILL_BE_REMOVED(6207),

  /**
   * Record contains an Enum value that could not be parsed correctly.
   *
   * @param 0 Line number of the offending record
   * @param 1 ID of the offending record
   * @param 2 String that was supposed to be an enum value
   * @param 3 Column name that contains the enum value.
   */
  CSV_INCORRECT_ENUM_VALUE(6216),
  /**
   * File contained an ID that did not match requirements and was adjusted.
   */
  CSV_ID_ADJUSTED(6217),
  /**
   * Unsupported meteo option for SRM. Triggered when the multiple year meteo requested by the calculation
   * does not match with the multiple year meteo used to generate the underlying version of PreSRM
   *
   * @param 0 Years on which the generated PreSRM is based
   * @param 1 Years requested by the calculation
   */
  SRM_UNSUPPORTED_METEO(6218),

  // ADMS file and calculation errors
  /**
   * ADMS run failed with error.
   *
   * @param 0 ADMS message
   */
  ADMS_INTERNAL_EXCEPTION(6301),
  /**
   * ADMS was fed with input that is outside the expected scope of ADMS
   *
   * @param 0 the validation errors
   */
  ADMS_INPUT_VALIDATION(6302),

  // Register Errors starts at 20000

  /**
   * Request already exists in the (register) database.
   *
   * @param 0 AERIUS reference of the scenario
   */
  REQUEST_ALREADY_EXISTS(20001),
  /**
   * Scenario does not exist in the (register) database.
   *
   * @param 0 dossier id of the scenario
   */
  PERMIT_UNKNOWN(20002),
  /**
   * Permit is already updated and we're trying to update over an older version.
   *
   * @param 0 dossier id
   */
  PERMIT_ALREADY_UPDATED(20004),
  /**
   * Attempting to assign a permit for which there is no development room.
   *
   * @param dossier id
   */
  PERMIT_DOES_NOT_FIT(20005),
  /**
   * No authorization to delete an active permit.
   *
   * @param dossier id
   */
  PERMIT_DELETE_ACTIVE_NO_AUTHORIZATION(20006),
  /**
   * No authorization to dequeue a permit.
   *
   * @param dossier id
   */
  PERMIT_DEQUEUE_NO_AUTHORIZATION(20007),
  /**
   * No authorization to enqueue a permit.
   *
   * @param dossier id
   */
  PERMIT_ENQUEUE_NO_AUTHORIZATION(20008),
  /**
   * Melding does not fit the required space.
   */
  MELDING_DOES_NOT_FIT(20011),
  /**
   * Melding is not actually a melding (above permit threshold).
   */
  MELDING_ABOVE_PERMIT_THRESHOLD(20013),
  /**
   * User accesses melding page directly. should go through calculator.
   */
  MELDING_NOT_VIA_CALCULATOR(20016),
  /**
   * User accesses melding page directly. should go through calculator.
   */
  MELDING_ATTACHMENTS_FOR_AUTHORIZATION_TO_BIG(20017),
  /**
   * Priority project does not exist in the (register) database.
   *
   * @param 0 dossier id of the priority project
   */
  PRIORITY_PROJECT_UNKNOWN(20018),
  /**
   * Priority subproject does not exist in the (register) database.
   *
   * @param 0 reference of the priority subproject
   */
  PRIORITY_SUBPROJECT_UNKNOWN(20019),
  /**
   * Melding does not exist in the (register) database.
   *
   * @param 0 reference of the melding
   */
  MELDING_UNKNOWN(20020),
  /**
   * Priority Project reservation doesn't fit.
   */
  PRIORITY_PROJECT_DOES_NOT_FIT(20022),
  /**
   * Reference is not a valid AERIUS reference.
   *
   * @param 0 The invalid reference.
   */
  REQUEST_INVALID_REFERENCE(20023),
  /**
   * Priority Sub Project doesn't fit.
   */
  PRIORITY_SUBPROJECT_DOES_NOT_FIT(20024),
  /**
   * A non-allowed sector has been found in the IMAER file whilst uploading a priority project (any type).
   */
  PRIORITY_PROJECT_NON_ALLOWED_SECTOR(20025),
  /**
   * Used when an imported file did not contain a single GML.
   */
  IMPORT_SINGLE_SITUATION_REQUIRED(20026),
  /**
   * Used when an imported file contains a diferential calculation with results.
   */
  IMPORT_DIFFERENTIAL_CALCULATION_WITH_RESULTS_NOT_ALLOWED(20027),

  // Authorization & Authentication and User management errors (across applications).

  /**
   * Authorization error. Does not have the required permissions.
   */
  AUTHORIZATION_ERROR(40001),

  /**
   * User already exists in the (register) database.
   *
   * @param 0 username
   */
  USER_ALREADY_EXISTS(40002),

  /**
   * User does not exist in the (register) database.
   *
   * @param 0 username
   */
  USER_DOES_NOT_EXIST(40003),

  /**
   * User can not be deleted from (register) database (foreign key violation).
   */
  USER_CANNOT_BE_DELETED(40004),

  /**
   * An email address is already registered in the user list.
   *
   * @param 0 email address
   */
  USER_EMAIL_ADDRESS_ALREADY_EXISTS(40005),
  /**
   * An API key is already registered in the user list.
   */
  USER_API_KEY_ALREADY_EXISTS(40006),
  /**
   * The API key doesn't belong to an user.
   */
  USER_INVALID_API_KEY(40007),
  /**
   * API key generation is disabled.
   */
  USER_API_KEY_GENERATION_DISABLED(40008),
  /**
   * The user reached his max concurrent jobs.
   */
  USER_MAX_CONCURRENT_JOB_LIMIT_REACHED(40009),
  /**
   * The user account is disabled.
   */
  USER_ACCOUNT_DISABLED(40010),
  /**
   * The user does not have the permission to export the priority project
   */
  USER_PRIORITY_PROJECT_UTILISATION_EXPORT_NOT_ALLOWED(40011),
  /**
   * The user reached the job rate limit within a period.
   */
  USER_PERIOD_JOB_RATE_LIMIT_REACHED(40012),

  // Connect Application errors start at 50000

  /**
   * The email is not supplied.
   *
   * @param 0 given email address
   */
  CONNECT_NO_VALID_EMAIL_SUPPLIED(50001),
  /**
   * The calculation year is incorrect.
   *
   * @oaram 0 given year
   */
  CONNECT_INCORRECT_CALCULATIONYEAR(50002),
  /**
   * The calculation type is not supplied.
   */
  CONNECT_NO_CALCULATIONTYPE_SUPPLIED(50003),
  /**
   * The calculation substance is not supplied.
   */
  CONNECT_NO_SUBSTANCE_SUPPLIED(50004),
  /**
   * The calculation distance range is invalid.
   *
   * @param 0 invalid given calculation distance
   */
  CONNECT_INVALID_CALCULATION_RANGE(50005),
  /**
   * The calculation temp years range is invalid.
   *
   * @param 0 invalid given temp year
   * @param 1 minimum years
   * @param 2 maximum years
   */
  CONNECT_INVALID_TEMPPROJECT_RANGE(50006),
  /**
   * No input sources specified.
   */
  CONNECT_NO_SOURCES(50007),
  /**
   * A Connect report API call is made without supplying a proposed situation.
   */
  CONNECT_SITUATION_NO_PROPOSED(50008),
  /**
   * The calculation type is temporary not supported.
   */
  CONNECT_CALCULATIONTYPE_SUPPLIED_NOT_SUPPORTED(50009),
  /**
   * The calculation type is temporary not supported.
   */
  CONNECT_REPORT_PERMIT_DEMAND_COMPARISON_NOT_SUPPORTED(50010),
  /**
   * An unsupported substance or typo in the substance was passed.
   */
  CONNECT_UNKNOWN_SUBSTANCE_SUPPLIED(50011),
  /**
   * The user does not have a job with the the supplied jobkey
   */
  CONNECT_USER_JOBKEY_DOES_NOT_EXIST(50012),
  /**
   * cancel the connect calculation job
   */
  CONNECT_JOB_CANCELLED(50013),
  /**
   * The passed parameter did not any receptors.
   *
   */
  CONNECT_NO_RECEPTORS_IN_PARAMETERS(50014),
  /**
   * UserCalculationPointSet already exists in the database.
   *
   * @param 0 setname
   */
  CONNECT_USER_CALCULATION_POINT_SET_ALREADY_EXISTS(50015),
  /**
   * UserCalculationPointSet does not exist in the database.
   *
   * @param 0 setname
   */
  CONNECT_USER_CALCULATION_POINT_SET_DOES_NOT_EXIST(50016),
  /**
   * An unsupported DataType was chosen for an operation.
   *
   */
  CONNECT_UNSUPPORTED_DATATYPE_IN_OPERATION(50017),
  /**
   * CalculationOptions were found that are no longer supported after the PAS stopped.
   *
   * @param 0 name of unsupported field.
   * @param 1 Statement about the consequences.
   */
  CONNECT_UNSUPPORTED_PAS_OPTIONS(50018),
  /**
   * Supplied GML file is larger than threshold XSD validation is skipped
   *
   */
  CONNECT_VALIDATION_SKIPPED_WARNING(50019),
  /**
   * The outputType is not supported for the current operation.
   *
   * @param 0 invalid outputType choice.
   */
  CONNECT_INVALID_OUTPUTTYPE(50020),
  /**
   * The supplied meteo year is not supported.
   * @param 0 invalid meteo year.
   *
   */
  CONNECT_INVALID_METEO(50021),

  /**
   * The receptorfile does not contain a height column
   * while the option is set to expect receptor heights.
   */
  CONNECT_MISSING_RECEPTOR_HEIGHT(50022),

  /**
   * The receptorfile contains a height column
   * while option to expect receptor heights is not set.
   */
  CONNECT_UNEXPECTED_RECEPTOR_HEIGHT(50023),

  /**
   * No custom points specified, either through receptorSetName or through a supplied file.
   */
  CONNECT_NO_CUSTOM_POINTS(50024),

  /**
   * Could not detect situation type.
   * @param 0 the file name for which no situation type could be detected.
   */
  CONNECT_NO_SITUATION_TYPE_DETECTED(50025),

  // Scenario Application-specific errors start at 60000

  /**
   * An I/O error has occurred while trying to communicate with the API.
   *
   * @param 0 detailed error message.
   */
  SCENARIO_API_CONNECTION_ERROR(60001),

  // General errors start at 90000

  /**
   * CalculationOptions were found that are no longer supported after the PAS stopped.
   *
   * @param 0 name of unsupported field.
   * @param 1 Statement about the consequences.
   */
  GENERAL_UNSUPPORTED_PAS_OPTIONS(90001);

  private final int errorCode;

  AeriusExceptionReason(final int errorCode) {
    this.errorCode = errorCode;
  }

  /**
   * @param errorCode The error code to resolve.
   * @return The reason object for the given error code, or null if the error code was unknown.
   */
  public static Reason fromErrorCode(final int errorCode) {
    for (final AeriusExceptionReason reason : AeriusExceptionReason.values()) {
      if (reason.getErrorCode() == errorCode) {
        return reason;
      }
    }

    return ImaerExceptionReason.fromErrorCode(errorCode);
  }

  @Override
  public int getErrorCode() {
    return errorCode;
  }
}
