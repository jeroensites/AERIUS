/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.config;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;

/**
 * Data class with all application dynamic configuration.
 */
public class ApplicationConfiguration implements Serializable {
  private static final long serialVersionUID = 1L;

  private @JsProperty Map<Theme, AppThemeConfiguration> contexts = new HashMap<>();
  private Settings settings = new Settings();
  private ReceptorGridSettings receptorGridSettings = null;
  private SectorCategories sectorCategories = null;


  /**
   * @return the contexts
   */
  public Map<Theme, AppThemeConfiguration> getContexts() {
    return contexts;
  }

  public AppThemeConfiguration getAppThemeConfiguration(final Theme theme) {
    return contexts.get(theme);
  }

  /**
   * @return the settings
   */
  public Settings getSettings() {
    return settings;
  }

  /**
   * private to ensure no formatter will add final
   */
  @SuppressWarnings("unused")
  private void setSettings(final Settings settings) {
    this.settings = settings;
  }

  /**
   * @param contexts the contexts to set
   */
  public void setContexts(final Map<Theme, AppThemeConfiguration> contexts) {
    this.contexts = contexts;
  }

  /**
   * @return the receptorGridSettings
   */
  public ReceptorGridSettings getReceptorGridSettings() {
    return receptorGridSettings;
  }

  /**
   * @param receptorGridSettings the receptorGridSettings to set
   */
  public void setReceptorGridSettings(final ReceptorGridSettings receptorGridSettings) {
    this.receptorGridSettings = receptorGridSettings;
  }

  /**
   * @return the sectorCategories
   */
  public SectorCategories getSectorCategories() {
    return sectorCategories;
  }

  /**
   * @param sectorCategories the sectorCategories to set
   */
  public void setSectorCategories(final SectorCategories sectorCategories) {
    this.sectorCategories = sectorCategories;
  }

}
