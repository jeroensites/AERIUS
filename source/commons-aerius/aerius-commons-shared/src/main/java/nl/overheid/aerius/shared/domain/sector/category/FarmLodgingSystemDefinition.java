/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

/**
 * A farm lodging system definition (a.k.a. BWL-number).<br>
 * Each farm lodging type ({@link FarmLodgingCategory}) has one or more system definitions.
 */
public class FarmLodgingSystemDefinition extends AbstractCategory {

  private static final long serialVersionUID = -2245472003033932829L;

  public FarmLodgingSystemDefinition() {
    // No-op.
  }

  public FarmLodgingSystemDefinition(final int id, final String code, final String name, final String description) {
    super(id, code, name, description);
  }

  public FarmLodgingSystemDefinition copy() {
    return new FarmLodgingSystemDefinition(getId(), getCode(), getName(), getDescription());
  }

}
