/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;

/**
 * Container for all RoadEmissionCategory objects.
 *
 * The algorithm assumes if there is a category with strict speed there always is a category with the same speed but non strict.
 */
public class RoadEmissionCategories implements Serializable {

  private static final long serialVersionUID = 2766326230085459481L;

  private @JsProperty Set<RoadEmissionCategory> categoriesNonStrict = new TreeSet<>();
  private @JsProperty Set<RoadEmissionCategory> categoriesStrict = new TreeSet<>();

  /**
   * Adds the given road emission category.
   * @param category The road emission category to add.
   * @return True if the category has been added, false if it already existed.
   */
  public boolean add(final RoadEmissionCategory category) {
    if (category.isStrictEnforcement()) {
      return categoriesStrict.add(category);
    } else {
      return categoriesNonStrict.add(category);
    }
  }

  /**
   * @param roadType The road type for which to list the speed categories.
   * @param roadSpeedType The road speed type for which to list the speed categories.
   * @return The set of speed categories for the given road type, or null if the road type is not valid.
   * A single speed category of 0 indicates that no maximum speed is applicable for the road type.
   */
  public TreeSet<Integer> getMaximumSpeedCategories(final RoadType roadType, final RoadSpeedType roadSpeedType) {
    Set<RoadEmissionCategory> matchingCategories = categoriesNonStrict.stream()
        .filter(category -> category.getRoadType() == roadType)
        .filter(category -> category.getSpeedType() == null || roadSpeedType == null || category.getSpeedType() == roadSpeedType)
        .collect(Collectors.toSet());
    if (matchingCategories.stream().anyMatch(RoadEmissionCategory::isSpeedRelevant)) {
      matchingCategories = matchingCategories.stream()
          .filter(RoadEmissionCategory::isSpeedRelevant)
          .collect(Collectors.toSet());
    }
    final Set<Integer> maxSpeeds = matchingCategories.stream()
        .map(RoadEmissionCategory::getMaximumSpeed)
        .collect(Collectors.toSet());
    return new TreeSet<>(maxSpeeds);
  }

  /**
   * Finds the road category that has higher or equal speed and same other properties.
   *
   * @param roadType road type
   * @param vehicleType vehicle type
   * @param strictEnforcement true if strict enforcement
   * @param maxSpeed speed
   * @param speedType Speed type of non freeway
   * @return road category or null if no category found.
   */
  public RoadEmissionCategory findClosestCategory(final RoadType roadType, final VehicleType vehicleType, final Boolean strictEnforced,
      final Integer maximumSpeed, final RoadSpeedType speedType) {
    final int maximumSpeedUnBoxed = maximumSpeed == null ? 0 : maximumSpeed;
    return findClosestCategoryUnboxed(roadType, vehicleType, Boolean.TRUE.equals(strictEnforced), maximumSpeedUnBoxed, speedType);
  }

  private RoadEmissionCategory findClosestCategoryUnboxed(final RoadType roadType, final VehicleType vehicleType, final boolean strictEnforced,
      final int maximumSpeed, final RoadSpeedType speedType) {
    final RoadEmissionCategory cat = new RoadEmissionCategory(roadType, vehicleType, strictEnforced, maximumSpeed, speedType);
    final RoadEmissionCategory ceiling = getCeiling(cat);
    final RoadEmissionCategory candidate = ceiling == null ? getFloor(cat) : ceiling;

    return strictEnforced ? findClosestIfStrict(maximumSpeed, candidate,
        findClosestCategoryUnboxed(roadType, vehicleType, false, maximumSpeed, speedType)) : candidate;
  }

  private RoadEmissionCategory getCeiling(final RoadEmissionCategory cat) {
    return matchEqualsType(cat, ((TreeSet<RoadEmissionCategory>) (cat.isStrictEnforcement() ? categoriesStrict : categoriesNonStrict)).ceiling(cat));
  }

  private RoadEmissionCategory getFloor(final RoadEmissionCategory cat) {
    return matchEqualsType(cat, ((TreeSet<RoadEmissionCategory>) (cat.isStrictEnforcement() ? categoriesStrict : categoriesNonStrict)).floor(cat));
  }

  private RoadEmissionCategory matchEqualsType(final RoadEmissionCategory cat1, final RoadEmissionCategory cat2) {
    return cat1.equalsType(cat2) ? cat2 : null;
  }

  /**
   * Finds the closest category if looking for a strict enforced speed category.
   * @param speed given speed
   * @param nonStrictCat the category found using strict enforcement.
   * @param strictCat the category found using strict enforcement.
   * @return
   */
  private RoadEmissionCategory findClosestIfStrict(final int speed, final RoadEmissionCategory strictCat, final RoadEmissionCategory nonStrictCat) {
    final RoadEmissionCategory result;
    if (strictCat == null) {
      result = nonStrictCat;
    } else if (strictCat.getMaximumSpeed() == speed) {
      result = strictCat;
    } else {
      result = findClosestOther(speed, strictCat, nonStrictCat);
    }
    return result;
  }

  /**
   * Find the closest if the speed doesn't matches any speed in categories list.
   * @param speed given speed
   * @param nonStrictCat the category found using strict enforcement.
   * @param strictCat the category found using strict enforcement.
   * @return
   */
  private RoadEmissionCategory findClosestOther(final int speed, final RoadEmissionCategory strictCat, final RoadEmissionCategory nonStrictCat) {
    final boolean nsG = speed >= nonStrictCat.getMaximumSpeed();
    final boolean sG = speed >= strictCat.getMaximumSpeed();
    final RoadEmissionCategory result;
    if (nsG && sG) {
      // If both categories below speed take non-strict but only if it has a higher speed as the strict.
      result = nonStrictCat.getMaximumSpeed() > strictCat.getMaximumSpeed() ? nonStrictCat : strictCat;
    } else if (nsG) {
      // If not strict higher speed, but strict is below given speed take not strict.
      result = nonStrictCat;
    } else if (sG) {
      // If strict higher speed, but not strict is below given speed take strict.
      result = strictCat;
    } else {
      // If speed is below both categories take the lowest (i.e. the one closest to the speed).
      result = strictCat.getMaximumSpeed() > nonStrictCat.getMaximumSpeed() ? nonStrictCat : strictCat;
    }
    return result;
  }

  /**
   * Get the road category exactly matching the given the road parameters.
   * @param roadType The road type of the road emission category.
   * @param vehicleType The standard vehicle type of the road emission category.
   * @param strictEnforced The enforcement category of the road emission category.
   * @param maximumSpeed The maximum speed category of the road emission category.
   * @return The road emission category for the given combination, or null if the combination is not valid.
   */
  public RoadEmissionCategory getRoadEmissionCategory(final RoadType roadType, final VehicleType vehicleType, final boolean strictEnforced,
      final int maximumSpeed, final RoadSpeedType speedType) {
    return getRoadEmissionCategory(new RoadEmissionCategory(roadType, vehicleType, strictEnforced, maximumSpeed, speedType));
  }

  /**
   * Get the exact road category for the list of categories.
   * @param category category to look up.
   * @return null if not equals matching road category
   */
  public RoadEmissionCategory getRoadEmissionCategory(final RoadEmissionCategory category) {
    final RoadEmissionCategory found =
        ((TreeSet<RoadEmissionCategory>) (category.isStrictEnforcement() ? categoriesStrict : categoriesNonStrict)).floor(category);
    return found != null && found.equals(category) ? found : null;
  }
}
