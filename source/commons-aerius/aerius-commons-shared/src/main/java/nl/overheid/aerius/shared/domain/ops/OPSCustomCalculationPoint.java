/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.ops;

import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;

public class OPSCustomCalculationPoint extends CustomCalculationPoint {

  private static final long serialVersionUID = 1L;

  /**
   * z0 - Average roughness(m).
   */
  private Double averageRoughness;

  /**
   * landuse - The dominant land use (enumerated value, 1-9).
   */
  private LandUse landUse;

  /**
   * Distribution of land uses (percentages).
   */
  private int[] landUses;

  public Double getAverageRoughness() {
    return averageRoughness;
  }

  public void setAverageRoughness(final Double averageRoughness) {
    this.averageRoughness = averageRoughness;
  }

  public LandUse getLandUse() {
    return landUse;
  }

  public void setLandUse(final LandUse landUse) {
    this.landUse = landUse;
  }

  public int[] getLandUses() {
    return landUses;
  }

  public void setLandUses(final int[] landUses) {
    this.landUses = landUses;
  }

  public boolean hasNoTerrainData() {
    return averageRoughness == null || landUse == null;
  }

  public boolean hasTerrainData() {
    return !hasNoTerrainData();
  }

  public boolean hasNoHeight() {
    return getHeight() == null;
  }

  public boolean hasHeight() {
    return !hasNoHeight();
  }

}
