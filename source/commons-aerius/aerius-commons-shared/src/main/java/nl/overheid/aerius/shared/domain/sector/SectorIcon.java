/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector;



/**
 * Enum class representing the sectors for which an image is available.
 */
public enum SectorIcon {
  ABROAD,
  AVIATION,
  AVIATION_AERODROME(3640, "993300"),
  AVIATION_TAKE_OFF(3610, "993300"),
  AVIATION_TAXI(3630, "993300"),
  AVIATION_TOUCH_DOWN(3620, "993300"),
  BACKGROUND_DEPOSITION,
  CONSTRUCTION,
  CONSUMERS(8200, "F47A0F"),
  ENERGY(2100, "0F789B"),
  ENERGY_POWER_PLANT,
  ENERGY_PRODUCTION_DISTRIBUTION,
  AGRICULTURE(4600, "427F08"),
  FARMLAND(4150, "427F08"),
  FARMLAND_FERTILIZER(4200, "427F08"),
  FARMLAND_MANURE(4140, "427F08"),
  FARMLAND_ORGANIC_PROCESSES(4400, "427F08"),
  FARMLAND_PASTURE(4130, "427F08"),
  FARM_LODGE(4110, "427F08"),
  FARM_MANURE_STORAGE(4120, "427F08"),
  FARM_OTHER,
  GREENHOUSE(4320, "427F08"),
  HDO,
  INDUSTRY(1800, "6B15CB"),
  INDUSTRY_BASE_MATERIAL(1500, "6B15CB"),
  INDUSTRY_BUILDING_MATERIALS(1400, "6B15CB"),
  INDUSTRY_CHEMICAL_INDUSTRY(1300, "6B15CB"),
  INDUSTRY_FOOD_INDUSTRY(1100, "6B15CB"),
  INDUSTRY_METAL_INDUSTRY(1700, "6B15CB"),
  MEASUREMENT_CORRECTION,
  MINING(3230, "CB181D"),
  OFFICES_SHOPS(8640, "F47A0F"),
  OTHER(9999, "084594"),
  PLAN(9000, "AE017E"),
  RAIL_EMPLACEMENT(3710, "AAA406"),
  RAIL_TRANSPORT(3720, "AAA406"),
  RECREATION(8210, "F47A0F"),
  ROAD_CONSTRUCTION_INDUSTRY(3220, "CB181D"),
  ROAD_FARM(3210, "CB181D"),
  ROAD_CONSUMER(3530, "CB181D"),
  ROAD_FREEWAY(3111, "CB181D"),
  ROAD_OTHER,
  ROAD_NON_URBAN(3112, "CB181D"),
  ROAD_URBAN(3113, "CB181D"),
  ROAD_UNDERLYING_NETWORK,
  SHIPPING,
  SHIPPING_DOCK(7510, "084594"),
  SHIPPING_FISHING,
  SHIPPING_INLAND(7620, "084594"),
  SHIPPING_INLAND_DOCK(7610, "084594"),
  SHIPPING_MARITIME_MOORING(7520, "084594"),
  SHIPPING_MARITIME_NCP(7530, "084594"),
  SHIPPING_RECREATIONAL,
  TRAFFIC_AND_TRANSPORT,
  WASTE_MANAGEMENT(1050, "6B15CB"),

  // Other deposition types:

  /**
   * The part of the deposition decline that is used for economic development (half the decline).
   */
  RETURNED_DEPOSITION_SPACE,
  /**
   * The part of the deposition decline that is used for economic development for Limburg (Limburg has it's own policy).
   */
  RETURNED_DEPOSITION_SPACE_LIMBURG,
  /**
   * Deposition space addition
   */
  DEPOSITION_SPACE_ADDITION;

  private final int sectorCode;
  private final String color;

  private SectorIcon() {
    this(0, "");
  }

  private SectorIcon(final int sectorCode, final String color) {
    this.sectorCode = sectorCode;
    this.color = color;
  }

  /**
   * @return the color
   */
  public String getColor() {
    return color;
  }

  /**
   * @return the sectorCode
   */
  public int getSectorCode() {
    return sectorCode;
  }

  /**
   * Safely converts a string representation of the sector Image to this enum.
   * Used to convert values from the database into this enum. It returns OTHER
   * if the input is null or invalid.
   *
   * @param value value to convert
   * @return SectorIcon or OTHER if null or invalid input
   */
  public static SectorIcon safeValueOf(final String value) {
    if (value == null) {
      return OTHER;
    }
    try {
      return valueOf(value.toUpperCase());
    } catch (final IllegalArgumentException e) {
      return OTHER;
    }
  }

  /**
   * Returns the sector icon for the given sector.
   *
   * @param sector sector
   * @return sector icon or null
   */
  public static SectorIcon valueOf(final Sector sector) {
    return valueOf(sector.getSectorId());
  }

  /**
   * Returns the sector icon for the given sector id.
   *
   * @param sectorId sectorId
   * @return sector icon or null
   */
  public static SectorIcon valueOf(final int sectorId) {
    for(final SectorIcon icon : values()) {
      if (icon.sectorCode == sectorId) {
        return icon;
      }
    }
    return OTHER;
  }

  public static String getIconStyle(SectorIcon icon) {
    return "icon-" + icon.toString().toLowerCase().replace("_", "-");
  }
}
