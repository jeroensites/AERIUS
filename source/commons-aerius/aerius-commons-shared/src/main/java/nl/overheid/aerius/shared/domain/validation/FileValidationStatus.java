/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.validation;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.shared.domain.scenario.SituationStats;
import nl.overheid.aerius.shared.domain.scenario.SituationType;

public class FileValidationStatus {
  private final String fileCode;
  private ValidationStatus status;
  private final List<ValidationError> errors;
  private final List<ValidationError> warnings;
  private final String situationName;
  private final SituationType situationType;
  private final List<String> childFileCodes;
  private final SituationStats situationStats;

  public FileValidationStatus(final String fileCode, final ValidationStatus status, final String situationName, final SituationType situationType, final List<String> childFileCodes, final SituationStats situationStats) {
    this.fileCode = fileCode;
    this.status = status;
    this.errors = new ArrayList<>();
    this.warnings = new ArrayList<>();
    this.situationName = situationName;
    this.situationType = situationType;
    this.childFileCodes = childFileCodes;
    this.situationStats = situationStats;
  }

  public final String getFileCode() {
    return fileCode;
  }

  public final ValidationStatus getValidationStatus() {
    return status;
  }

  public final void setValidationStatus(final ValidationStatus status) {
    this.status = status;
  }

  public final List<ValidationError> getErrors() {
    return errors;
  }

  public final List<ValidationError> getWarnings() {
    return warnings;
  }

  public final String getSituationName() {
    return situationName;
  }

  public final SituationType getSituationType() {
    return situationType;
  }

  public final List<String> getChildFileCodes() {
    return childFileCodes;
  }

  public SituationStats getSituationStats() {
    return situationStats;
  }

}
