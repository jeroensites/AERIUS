/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.List;

import jsinterop.annotations.JsProperty;

/**
 * Holds lists with domain objects that are relevant for farm lodging types (RAV including stacking).<br>
 * This object is owned by the context.
 */
public class FarmLodgingCategories implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * List of all farm lodging types (systems). This is the RAV code list.
   */
  private @JsProperty List<FarmLodgingCategory> farmLodgingCategories;

  /**
   * List of all farm additional lodging systems. These are RAV codes that generate extra emissions
   * in a lodging and can be stacked on top of regular farm lodging types.
   */
  private @JsProperty List<FarmAdditionalLodgingSystemCategory> farmAdditionalLodgingSystemCategories;

  /**
   * List of all farm reductive lodging systems. These are RAV codes that reduce emissions
   * in a lodging and can be stacked on top of regular farm lodging types.
   */
  private @JsProperty List<FarmReductiveLodgingSystemCategory> farmReductiveLodgingSystemCategories;

  /**
   * List of all farm lodging fodder measures. These are measures that reduce emissions
   * in a lodging and can be stacked on top of regular farm lodging types.
   */
  private @JsProperty List<FarmLodgingFodderMeasureCategory> farmLodgingFodderMeasureCategories;

  public List<FarmLodgingCategory> getFarmLodgingSystemCategories() {
    return farmLodgingCategories;
  }

  public void setFarmLodgingSystemCategories(final List<FarmLodgingCategory> farmLodgingCategories) {
    this.farmLodgingCategories = farmLodgingCategories;
  }

  public List<FarmAdditionalLodgingSystemCategory> getFarmAdditionalLodgingSystemCategories() {
    return farmAdditionalLodgingSystemCategories;
  }

  public void setFarmAdditionalLodgingSystemCategories(final List<FarmAdditionalLodgingSystemCategory> farmAdditionalLodgingSystemCategories) {
    this.farmAdditionalLodgingSystemCategories = farmAdditionalLodgingSystemCategories;
  }

  public List<FarmReductiveLodgingSystemCategory> getFarmReductiveLodgingSystemCategories() {
    return farmReductiveLodgingSystemCategories;
  }

  public void setFarmReductiveLodgingSystemCategories(final List<FarmReductiveLodgingSystemCategory> farmReductiveLodgingSystemCategories) {
    this.farmReductiveLodgingSystemCategories = farmReductiveLodgingSystemCategories;
  }

  public List<FarmLodgingFodderMeasureCategory> getFarmLodgingFodderMeasureCategories() {
    return farmLodgingFodderMeasureCategories;
  }

  public void setFarmLodgingFodderMeasureCategories(final List<FarmLodgingFodderMeasureCategory> farmLodgingFodderMeasureCategories) {
    this.farmLodgingFodderMeasureCategories = farmLodgingFodderMeasureCategories;
  }

  /**
   * @param code The farm lodging category code to determine the FarmLodgingCategory object for.
   * @return The right FarmLodgingCategory object, or null if no match could be made based on the code.
   */
  public FarmLodgingCategory determineFarmLodgingCategoryByCode(final String code) {
    return AbstractCategory.determineByCode(farmLodgingCategories, code);
  }

  /**
   * @param code The lodging system category code to determine the FarmAdditionalLodgingSystemCategory object for.
   * @return The right FarmAdditionalLodgingSystemCategory object, or null if no match could be made based on the code.
   */
  public FarmAdditionalLodgingSystemCategory determineAdditionalLodgingSystemByCode(final String code) {
    return AbstractCategory.determineByCode(farmAdditionalLodgingSystemCategories, code);
  }

  /**
   * @param code The lodging system category code to determine the FarmReductiveLodgingSystemCategory object for.
   * @return The right FarmReductiveLodgingSystemCategory object, or null if no match could be made based on the code.
   */
  public FarmReductiveLodgingSystemCategory determineReductiveLodgingSystemByCode(final String code) {
    return AbstractCategory.determineByCode(farmReductiveLodgingSystemCategories, code);
  }

  /**
   * @param code The lodging fodder measure code to determine the FarmLodgingFodderMeasureCategory object for.
   * @return The right FarmLodgingFodderMeasureCategory object, or null if no match could be made based on the code.
   */
  public FarmLodgingFodderMeasureCategory determineLodgingFodderMeasureByCode(final String code) {
    return AbstractCategory.determineByCode(farmLodgingFodderMeasureCategories, code);
  }
}
