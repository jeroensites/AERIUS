/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector;

/**
 * Enum to guide the application to bind a sector to the an implementation. For
 * example to guide the user interface to use specific emission input fields for
 * for a new source.
 */
public enum EmissionCalculationMethod {
  /**
   * Farm Lodge emission values.
   */
  FARM_LODGE,
  /**
   * Farmland emission values.
   */
  FARMLAND,
  /**
   * Generic for all others.
   */
  GENERIC,
  /**
   * Mobile off road emission values.
   */
  OFFROAD_MOBILE,
  /**
   * Plan emission values.
   */
  PLAN,
  /**
   * Road emission values.
   */
  ROAD,
  /**
   * Docked inland shipping emission values.
   */
  SHIPPING_INLAND_DOCKED,
  /**
   * Route for inland shipping route emission values.
   */
  SHIPPING_INLAND,
  /**
   * Docked maritime shipping emission values.
   */
  SHIPPING_MARITIME_DOCKED,
  /**
   * Inland route for maritime shipping emission values.
   */
  SHIPPING_MARITIME_INLAND,
  /**
   * Maritime route for maritime shipping emission values.
   */
  SHIPPING_MARITIME_MARITIME;

  /**
   * Safely converts a string representation of the sector emission type to this
   * enum.  Used to convert values from the database into this enum. It returns
   * the GENERIC value for null input or input that fails.
   *
   * @param value value to convert
   * @return SectorEmissionType or GENERIC if no valid input
   */
  public static EmissionCalculationMethod safeValueOf(final String value) {
    if (value == null) {
      return GENERIC;
    }
    try {
      return valueOf(value.toUpperCase());
    } catch (final IllegalArgumentException e) {
      return GENERIC;
    }
  }
}
