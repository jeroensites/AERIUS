/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.geo;

import java.io.Serializable;
import java.util.Locale;

/**
 * The different types of area ((multi)polygon) geometries we can find in the database.
 */
public enum AreaType implements Serializable {
  NATURA2000_AREA,
  NATURA2000_DIRECTIVE_AREA,
  HABITAT_TYPE,
  HABITAT_AREA,
  PROVINCE_AREA,
  MUNICIPALITY_AREA,
  TOWN_AREA,
  ZIP_CODE_AREA;

  /**
   * Returns the database object name.
   * @param plural if plural append 's'
   * @return database table name
   */
  public String toDatabaseObjectName() {
    if (this == HABITAT_TYPE) {
      return null; // "habitat_type(s)" not yet supported: needs intersection
    } else {
      return this.toString().toLowerCase(Locale.ROOT);
    }
  }

  public String toDatabasePluralObjectName() {
    if (this == HABITAT_TYPE) {
      return null; // "habitat_type(s)" not yet supported: needs intersection
    } else {
      return this.toString().toLowerCase(Locale.ROOT) + "s";
    }
  }

  /**
   * Returns the enum matching the databaseObjectName.
   * @param databaseObjectName name from the database
   * @return enum representing database object name
   */
  public static AreaType fromDatabaseObjectName(final String databaseObjectName) {
    try {
      if (databaseObjectName == null) {
        return null;
      } else {
        final String dONUpper = databaseObjectName.toUpperCase(Locale.ROOT);
        return valueOf(dONUpper.charAt(dONUpper.length() - 1) == 'S'
            ? dONUpper.substring(0, dONUpper.length() - 1) : dONUpper);
      }
    } catch (final IllegalArgumentException e) {
      return null;
    }
  }

}
