/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

import static jsinterop.annotations.JsPackage.GLOBAL;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsType;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasName;

/**
 * An assessment area, like a N2000 area.
 */
@JsType(namespace = GLOBAL, name = "Object", isNative = true)
public class AssessmentArea implements Comparable<AssessmentArea>, HasId, HasName {

  private int assessmentAreaId;
  private String name;
  private BBox bounds;

  @Override
  public final @JsOverlay int getId() {
    return assessmentAreaId;
  }

  @Override
  public final @JsOverlay void setId(final int id) {
    this.assessmentAreaId = id;
  }

  @Override
  public final @JsOverlay String getName() {
    return name;
  }

  @Override
  public final @JsOverlay void setName(final String name) {
    this.name = name;
  }

  public final @JsOverlay BBox getBounds() {
    return bounds;
  }

  public final @JsOverlay void setBounds(final BBox bounds) {
    this.bounds = bounds;
  }

  @Override
  public final @JsOverlay int compareTo(final AssessmentArea o) {
    return name.compareTo(o.getName());
  }

  @Override
  public final @JsOverlay int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + assessmentAreaId;
    return result;
  }

  @Override
  public final @JsOverlay boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() && this.assessmentAreaId == ((AssessmentArea) obj).assessmentAreaId;
  }

  @Override
  public final @JsOverlay String toString() {
    return "AssessmentArea [assessmentAreaId=" + assessmentAreaId + ", name=" + name + ", bounds=" + bounds + "]";
  }
}
