/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.result.range;

import java.util.List;
import java.util.function.Function;

import nl.overheid.aerius.geo.domain.legend.ColorLabelsLegend;
import nl.overheid.aerius.geo.domain.legend.LegendType;


/**
 * Legend that is extracted from a color range
 */
public class ColorRangesLegend extends ColorLabelsLegend {
  private static final long serialVersionUID = 1L;

  public ColorRangesLegend() {}

  public ColorRangesLegend(final List<ColorRange> colorRanges, final LegendType icon) {
    super(getLabels(colorRanges, v -> v.toString()), getColors(colorRanges), icon, null);
  }

  public ColorRangesLegend(final List<ColorRange> colorRanges, final Function<Double, String> labelDeducer, final LegendType icon, final String unit) {
    super(getLabels(colorRanges, labelDeducer), getColors(colorRanges), icon, unit);
  }

  private static String[] getLabels(final List<ColorRange> colorRanges, final Function<Double, String> labelDeducer) {
    final String[] labels = new String[colorRanges.size()];
    for (int i = 0; i < colorRanges.size(); i++) {
      if (colorRanges.get(i).getLabel() != null) {
        labels[i] = colorRanges.get(i).getLabel();
      } else {
        // Otherwise try to deduce the label from the range
        labels[i] = deduceLabel(colorRanges, labelDeducer, i);
      }
    }
    return labels;
  }

  private static String deduceLabel(final List<ColorRange> colorRanges, final Function<Double, String> labelDeducer, final int index) {
    if (index != colorRanges.size() - 1) {
      if (Double.isInfinite(-colorRanges.get(index).getLowerValue())) {
        return "< " + labelDeducer.apply(colorRanges.get(index + 1).getLowerValue());
      } else {
        return labelDeducer.apply(colorRanges.get(index).getLowerValue()) + " - " + labelDeducer.apply(colorRanges.get(index + 1).getLowerValue());
      }
    } else {
      return "> " + labelDeducer.apply(colorRanges.get(index).getLowerValue());
    }
  }

  private static String[] getColors(final List<ColorRange> colorRanges) {
    return colorRanges.stream().map(ColorRange::getColor).toArray(String[]::new);
  }

}
