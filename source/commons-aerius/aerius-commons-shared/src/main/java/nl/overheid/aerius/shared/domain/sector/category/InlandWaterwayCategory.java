/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.WaterwayDirection;

/**
 * Category class for Inland Waterways.
 */
public class InlandWaterwayCategory extends AbstractCategory {

  private static final long serialVersionUID = 1L;

  private @JsProperty List<WaterwayDirection> directions = new ArrayList<>();

  public List<WaterwayDirection> getDirections() {
    return directions;
  }

  public void setDirections(final List<WaterwayDirection> directions) {
    this.directions = directions;
  }

  /**
   * Returns true if the direction on which ships move is relevant.
   * @return true if direction is relevant
   */
  public boolean isDirectionRelevant() {
    return directions.size() > 1;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    return (directions == null ? 1 : (prime * directions.hashCode())) + super.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    final boolean isClass = obj != null && getClass() == obj.getClass();
    return isClass && Objects.equals(directions, ((InlandWaterwayCategory) obj).getDirections()) && super.equals(obj);
  }

  @Override
  public String toString() {
    return super.toString() + ", directions=" + directions + "]";
  }

}
