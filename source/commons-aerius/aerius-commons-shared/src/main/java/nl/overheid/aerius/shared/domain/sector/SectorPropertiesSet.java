/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.shared.domain.sector;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import jsinterop.annotations.JsProperty;

/**
 * Set class for {@link SectorProperties}.
 */
public class SectorPropertiesSet implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * Maps sector id to properties.
   */
  @JsProperty private Map<Integer, SectorProperties> properties = new HashMap<>();

  public SectorProperties get(final Integer sectorId) {
    final SectorProperties sectorProperties = properties.get(sectorId);

    return sectorProperties == null ? SectorProperties.SECTOR_UNDEFINED : sectorProperties;
  }

  /**
   * @param sectorId
   * @param sectorProperties
   */
  public void put(final int sectorId, final SectorProperties sectorProperties) {
    properties.put(sectorId, sectorProperties);
  }
}
