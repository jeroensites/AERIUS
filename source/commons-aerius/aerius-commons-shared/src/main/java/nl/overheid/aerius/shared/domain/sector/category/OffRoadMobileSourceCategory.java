/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.util.HashMap;
import java.util.Map;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * Off-Road Mobile Source Category class. For each mobile source per substance the
 * different emission factors are available in the object.
 */
public class OffRoadMobileSourceCategory extends AbstractEmissionCategory {

  private static final long serialVersionUID = 2L;

  private Double maxAdBlueFuelRatio;

  private @JsProperty Map<Substance, Double> emissionFactorsPerLiterFuel = new HashMap<>();
  private @JsProperty Map<Substance, Double> emissionFactorsPerOperatingHour = new HashMap<>();
  private @JsProperty Map<Substance, Double> emissionFactorsPerLiterAdBlue = new HashMap<>();

  public Double getMaxAdBlueFuelRatio() {
    return maxAdBlueFuelRatio;
  }

  public void setMaxAdBlueFuelRatio(final Double maxAdBlueFuelRatio) {
    this.maxAdBlueFuelRatio = maxAdBlueFuelRatio;
  }

  /**
   * emission factor per liter fuel (kg/l)
   */
  public double getEmissionFactorPerLiterFuel(final Substance substance) {
    return emissionFactorsPerLiterFuel.containsKey(substance) ? emissionFactorsPerLiterFuel.get(substance) : 0;
  }

  public void setEmissionFactorPerLiterFuel(final Substance substance, final double emissionFactor) {
    emissionFactorsPerLiterFuel.put(substance, Double.valueOf(emissionFactor));
  }

  /**
   * emission factor per operating hour (kg/h)
   */
  public double getEmissionFactorPerOperatingHour(final Substance substance) {
    return emissionFactorsPerOperatingHour.containsKey(substance) ? emissionFactorsPerOperatingHour.get(substance) : 0;
  }

  public void setEmissionFactorPerOperatingHour(final Substance substance, final double emissionFactor) {
    emissionFactorsPerOperatingHour.put(substance, Double.valueOf(emissionFactor));
  }

  /**
   * emission factor per liter AdBlue (kg/l)
   */
  public double getEmissionFactorPerLiterAdBlue(final Substance substance) {
    return emissionFactorsPerLiterAdBlue.containsKey(substance) ? emissionFactorsPerLiterAdBlue.get(substance) : 0;
  }

  public void setEmissionFactorPerLiterAdBlue(final Substance substance, final double emissionFactor) {
    emissionFactorsPerLiterAdBlue.put(substance, Double.valueOf(emissionFactor));
  }

  public boolean expectsLiterFuel() {
    return expects(emissionFactorsPerLiterFuel);
  }

  public boolean expectsOperatingHours() {
    return expects(emissionFactorsPerOperatingHour);
  }

  public boolean expectsLiterAdBlue() {
    return expects(emissionFactorsPerLiterAdBlue);
  }

  private boolean expects(final Map<Substance, Double> emissionFactors) {
    return !emissionFactors.isEmpty() && emissionFactors.values().stream().anyMatch(value -> Double.compare(0, value) != 0);
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() && getId() == ((OffRoadMobileSourceCategory) obj).getId();
  }

  @Override
  public int hashCode() {
    return getId();
  }

  @Override
  public String toString() {
    return super.toString() + ", emissionFactorsPerLiterFuel=" + emissionFactorsPerLiterFuel
        + ", emissionFactorsPerOperatingHour=" + emissionFactorsPerOperatingHour
        + ", emissionFactorsPerLiterAdBlue=" + emissionFactorsPerLiterAdBlue
        + ", maxAdBlueFuelRatio=" + maxAdBlueFuelRatio;
  }
}
