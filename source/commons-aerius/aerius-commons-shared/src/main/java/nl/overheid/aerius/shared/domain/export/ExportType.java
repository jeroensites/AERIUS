/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.export;

/**
 * Type of data to export.
 */
public enum ExportType {
  /**
   * Perform a calculation for showing results in the user interface. No documents will be generated.
   */
  CALCULATION_UI,
  /**
   * CSV file export.
   */
  CSV,
  /**
   * CSV file export where results file is extended, only supported with theme RBL.
   */
  CSV_EXTENDED,
  /**
   * Output a GML file with only sources.
   */
  GML_SOURCES_ONLY,
  /**
   * Output a GML file with sources and available results.
   */
  GML_WITH_RESULTS,
  /**
   * Output a GML file with sources and available results, and separate GML file for used sectors.
   */
  GML_WITH_SECTORS_RESULTS,
  /**
   * Output a OPS input file.
   */
  OPS,
  /**
   * Output a Permit Application Appendix file (as PDF).
   */
  PAA;
}
