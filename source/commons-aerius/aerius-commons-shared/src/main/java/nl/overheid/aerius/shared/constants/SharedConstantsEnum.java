/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.constants;

/**
 * Global constants stored in the database and shared among the client and
 * server application. Constants NOT used in the client should be placed
 * in {@link nl.overheid.aerius.enums.ConstantsEnum}.
 *
 * <p>These can be accessed via the context's
 * {@link nl.overheid.aerius.shared.domain.context.Context#getSetting(SharedConstantsEnum)} method.</p>
 *
 * <p>The values of the entries are stored in the database tables <code>public.constants</code>
 * and <code>system.constants</code>.</p>
 */
public enum SharedConstantsEnum {
  /**
   * ID for jira collector
   */
  COLLECT_COLLECTOR_ID,
  /**
   * show feedback link for jira
   */
  COLLECT_FEEDBACK,
  /**
   * Manual url.
   */
  MANUAL_URL,
  /**
   * Quick start url
   */
  QUICK_START_URL,
  /**
   * URL to factsheet giving more information about OPS characteristics calculations.
   */
  OPS_FACTSHEET_URL,
  /**
   * URL to Bij12 helpdesk
   */
  BIJ12_HELPDESK_URL,
  /**
   * URL to details about methods and data used in the current release.
   */
  RELEASE_DETAILS_URL,
  /**
   * Database version as defined in system
   */
  DATABASE_VERSION,
  /**
   * AERIUS version as defined in the manifest file
   */
  AERIUS_VERSION,
  /**
   * Polling time for system info message.
   * Used for the polling time for the request.
   */
  SYSTEM_INFO_POLLING_TIME,
  /**
   * Calculation boundary as rendered on the map
   * Contains the geometry as WKT string
   */
  CALCULATOR_BOUNDARY,
  /**
   * Internal connect url, used during PDF generation to reach connect using fully qualified url.
   */
  CONNECT_INTERNAL_URL,
  /**
   * Internal geoserver url, used during PDF generation to reach geoserver using fully qualified url.
   */
  GEOSERVER_INTERNAL_URL,
  /**
   * Search endpoint url
   */
  SEARCH_ENDPOINT_URL,
  /**
   * Tetris endpoint url
   */
  TETRIS_ENDPOINT_URL,
  /**
   * Tetris origin point (String in a double colon separated x:y)
   */
  TETRIS_ORIGIN_POINT,
  /**
   * Denotes on which side of the road cars drive.
   * Not used in the calculation but only in the visualization.
   */
  DRIVING_SIDE;

}
