/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import jsinterop.annotations.JsProperty;

import nl.aerius.search.domain.SearchCapability;
import nl.aerius.search.domain.SearchRegion;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.shared.domain.result.range.ColorRange;
import nl.overheid.aerius.shared.domain.result.range.ColorRangeType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.EmissionCalculationMethod;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.SectorPropertiesSet;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;

/**
 * Base class for Application configuration.
 */
public class AppThemeConfiguration implements Serializable {

  private static final long serialVersionUID = 4L;

  private Theme theme;

  // Specific layers
  private @JsProperty List<LayerProps> baseLayers = new ArrayList<>();

  // Specific layers
  private @JsProperty Map<String, LayerProps> layers = new LinkedHashMap<>();

  private @JsProperty Map<ColorRangeType, List<ColorRange>> colorRanges = new HashMap<>();

  /**
   * Maps sector id to the way it should be viewed/edited in the user interface.
   */
  private @JsProperty Map<Integer, EmissionCalculationMethod> emisionSourceUIViewType;

  private @JsProperty EmissionResultValueDisplaySettings emissionResultValueDisplaySettings;

  private String defaultMeteoYear;
  private @JsProperty List<String> meteoYears;
  private String calculationYearDefault;
  private @JsProperty List<String> calculationYears;

  private CharacteristicsType characteristicsType;
  private @JsProperty List<EmissionResultKey> emissionResultKeys;
  private @JsProperty List<Substance> substances;
  private @JsProperty List<Substance> emissionSubstancesRoad;
  private @JsProperty List<Substance> importSubstances;
  private CalculationType defaultCalculationType;
  private @JsProperty List<CalculationType> calculationTypes;
  private @JsProperty SectorPropertiesSet sectorPropertiesSet;
  private @JsProperty Map<SituationType, Integer> maxNumberOfSituationsPerType;
  private @JsProperty List<ResultView> resultViews;
  private int maxNumberOfSituations;
  private int maxNumberOfFilesSituationImport;
  private @JsProperty List<SituationType> availableSituationTypes;
  private @JsProperty List<SectorGroup> sectorGroups;
  private SearchRegion searchRegion;
  private @JsProperty List<SearchCapability> searchCapabilities;

  protected AppThemeConfiguration() {
    // Needed for GWT-RPC
  }

  public AppThemeConfiguration(final Theme theme) {
    this.theme = theme;
  }

  /**
   *
   * @return the list of allowed sectors
   */
  public List<SectorGroup> getSectorGroups() {
    return sectorGroups;
  }

  public void setSectorGroups(final List<SectorGroup> sectorGroups) {
    this.sectorGroups = sectorGroups;
  }

  /**
   * @return the theme
   */
  public Theme getTheme() {
    return theme;
  }

  public CharacteristicsType getCharacteristicsType() {
    return characteristicsType;
  }

  public void setCharacteristicsType(final CharacteristicsType characteristicsType) {
    this.characteristicsType = characteristicsType;
  }

  /**
   * @return the baseLayers
   */
  public List<LayerProps> getBaseLayers() {
    return baseLayers;
  }

  /**
   * @param baseLayers the baseLayers to set
   */
  public void setBaseLayers(final List<LayerProps> baseLayers) {
    this.baseLayers = baseLayers;
  }

  @SuppressWarnings("unchecked")
  public <T extends LayerProps> T getLayer(final Enum<?> layerType) {
    return (T) layers.get(layerType);
  }

  /**
   * @return the layers
   */
  public List<LayerProps> getLayers() {
    return new ArrayList<>(layers.values());
  }

  /**
   * private setter to ensure the layers field can never be final
   */
  @SuppressWarnings("unused")
  private void setLayers(final Map<String, LayerProps> layers) {
    this.layers = layers;
  }

  public void setLayer(final String id, final LayerProps layerProps) {
    layers.put(id, layerProps);
  }

  public Map<ColorRangeType, List<ColorRange>> getColorRanges() {
    return colorRanges;
  }

  public void setColorRanges(final Map<ColorRangeType, List<ColorRange>> colorRanges) {
    this.colorRanges = colorRanges;
  }

  public List<ColorRange> getColorRange(final ColorRangeType colorRangeType) {
    return this.colorRanges.getOrDefault(colorRangeType, Collections.emptyList());
  }

  public EmissionResultValueDisplaySettings getEmissionResultValueDisplaySettings() {
    return emissionResultValueDisplaySettings;
  }

  public void setEmissionResultValueDisplaySettings(final EmissionResultValueDisplaySettings emissionResultValueDisplaySettings) {
    this.emissionResultValueDisplaySettings = emissionResultValueDisplaySettings;
  }

  /**
   * @return the emisionSourceUIViewType
   */
  public Map<Integer, EmissionCalculationMethod> getEmisionSourceUIViewType() {
    return emisionSourceUIViewType;
  }

  /**
   * @param emisionSourceUIViewType the emisionSourceUIViewType to set
   */
  public void setEmisionSourceUIViewType(final Map<Integer, EmissionCalculationMethod> emisionSourceUIViewType) {
    this.emisionSourceUIViewType = emisionSourceUIViewType;
  }

  /**
   * @return the calculationYearDefault
   */
  public String getCalculationYearDefault() {
    return calculationYearDefault;
  }

  /**
   * @return the calculationYears
   */
  public List<String> getCalculationYears() {
    return calculationYears;
  }

  /**
   * @param calculationYearDefault the calculationYearDefault to set
   */
  public void setCalculationYearDefault(final String calculationYearDefault) {
    this.calculationYearDefault = calculationYearDefault;
  }

  /**
   * @param calculationYears the calculationYears to set
   */
  public void setCalculationYears(final List<String> calculationYears) {
    this.calculationYears = calculationYears;
  }

  /**
   * @return the calculationYears
   */
  public List<String> getMeteoYears() {
    return meteoYears;
  }

  /**
   * @param defaultMeteoYear the default MeteoYear
   */
  public void setDefaultMeteoYear(final String defaultMeteoYear) {
    this.defaultMeteoYear = defaultMeteoYear;
  }

  /**
   * @return the default MeteoYear
   */
  public String getDefaultMeteoYear() {
    return defaultMeteoYear;
  }

  /**
   * @param calculationYears the calculationYears to set
   */
  public void setMeteoYears(final List<String> meteoYears) {
    this.meteoYears = meteoYears;
  }

  /**
   * @return the emission result keys that are available in the theme
   */
  public List<EmissionResultKey> getEmissionResultKeys() {
    return emissionResultKeys;
  }

  public void setEmissionResultKeys(final List<EmissionResultKey> emissionResultKeys) {
    this.emissionResultKeys = emissionResultKeys;
  }

  /**
   * @return the emission substances that can be used in the theme
   */
  public List<Substance> getSubstances() {
    return substances;
  }

  public void setSubstances(final List<Substance> substances) {
    this.substances = substances;
  }

  /**
   * @return supported emission substances for road sources.
   */
  public List<Substance> getEmissionSubstancesRoad() {
    return emissionSubstancesRoad;
  }

  public void setEmissionSubstancesRoad(final List<Substance> emissionSubstancesRoad) {
    this.emissionSubstancesRoad = emissionSubstancesRoad;
  }

  /**
   * @return the substances that can be imported in the theme
   */
  public List<Substance> getImportSubstances() {
    return importSubstances;
  }

  public void setImportSubstances(final List<Substance> importSubstances) {
    this.importSubstances = importSubstances;
  }

  /**
   * @return the calculation method that can be used in the theme
   */
  public List<CalculationType> getCalculationTypes() {
    return calculationTypes;
  }

  public void setCalculationTypes(final List<CalculationType> calculationTypes) {
    this.calculationTypes = calculationTypes;
  }

  /**
   * @Return the default calculation method in the theme
   */
  public CalculationType getDefaultCalculationType() {
    return defaultCalculationType;
  }

  public void setDefaultCalculationType(final CalculationType calculationType) {
    this.defaultCalculationType = calculationType;
  }

  /**
   * @Return the SectorPropertiesSet in the theme
   */
  public SectorPropertiesSet getSectorPropertiesSet() {
    return sectorPropertiesSet;
  }

  public void setSectorPropertiesSet(final SectorPropertiesSet sectorPropertiesSet) {
    this.sectorPropertiesSet = sectorPropertiesSet;
  }

  /**
   * searchCapabilities
   *
   * @Return the max amount of situations per SituationType in the theme
   */
  public Map<SituationType, Integer> getMaxNumberOfSituationsPerType() {
    return maxNumberOfSituationsPerType;
  }

  public void setMaxNumberOfSituationsPerType(final Map<SituationType, Integer> maxNumberOfSituationsPerType) {
    this.maxNumberOfSituationsPerType = maxNumberOfSituationsPerType;
  }

  public int getMaxNumberOfSituations() {
    return maxNumberOfSituations;
  }

  public void setMaxNumberOfSituations(final int maxNumberOfSituations) {
    this.maxNumberOfSituations = maxNumberOfSituations;
  }

  public int getMaxNumberOfFilesSituationImport() {
    return maxNumberOfFilesSituationImport;
  }

  public void setMaxNumberOfFilesSituationImport(final int maxNumberOfFilesSituationImport) {
    this.maxNumberOfFilesSituationImport = maxNumberOfFilesSituationImport;
  }

  public List<ResultView> getResultViews() {
    return resultViews;
  }

  public void setResultViews(final List<ResultView> resultViews) {
    this.resultViews = resultViews;
  }

  public List<SituationType> getAvailableSituationTypes() {
    return availableSituationTypes;
  }

  public void setAvailableSituationTypes(final List<SituationType> availableSituationTypes) {
    this.availableSituationTypes = availableSituationTypes;
  }

  public SearchRegion getSearchRegion() {
    return searchRegion;
  }

  public void setSearchRegion(final SearchRegion searchRegion) {
    this.searchRegion = searchRegion;
  }

  public List<SearchCapability> getSearchCapabilities() {
    return searchCapabilities;
  }

  public void setSearchCapabilities(final List<SearchCapability> searchCapabilities) {
    this.searchCapabilities = searchCapabilities;
  }
}
