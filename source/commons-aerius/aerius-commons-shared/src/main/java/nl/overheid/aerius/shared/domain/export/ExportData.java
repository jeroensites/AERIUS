/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.export;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;

import nl.overheid.aerius.shared.Constants;

/**
 * Data class containing generic export properties.
 */
public abstract class ExportData implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * Options to perform additional actions during the process.
   */
  public enum ExportAdditionalOptions {
    /**
     * In export workers e-mail the users the exported file.
     */
    EMAIL_USER,
    /**
     * Remove results with the job is finished.
     */
    REMOVE_RESULTS,
    /**
     * In export workers return the total content of the export file via the queue.
     */
    RETURN_FILE,
    /**
     * If set the file(s) are submitted to the file services
     */
    SUBMIT_FILE_TO_SERVICE,
    /**
     * If set the progress of the job is stored in the database.
     */
    TRACK_JOB_PROGRESS,
  }

  private String emailAddress;
  private String locale = Constants.DEFAULT_LOCALE;
  private Date creationDate = new Date();
  private HashSet<ExportAdditionalOptions> additionalOptions = new HashSet<>();
  /**
   * URL to the file service. Should not end with slash!
   */
  private String fileServiceUrl;
  private String scenarioFileIdentifier;

  public ExportData() {
    // add default option - may be removed if not needed
    additionalOptions.add(ExportAdditionalOptions.EMAIL_USER);
  }

  /**
   * @return The email address to use when mailing.
   */
  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(final String emailAddress) {
    this.emailAddress = emailAddress;
  }

  /**
   * @return The locale to use for i18n purposes.
   */
  public String getLocale() {
    return locale;
  }

  public void setLocale(final String locale) {
    this.locale = locale;
  }

  public String getFileServiceUrl() {
    return fileServiceUrl;
  }

  public void setFileServiceUrl(final String fileServiceUrl) {
    this.fileServiceUrl = fileServiceUrl;
  }

  /**
   * @return The creation date of the export (or actually the date the export was initialized).
   */
  public Date getCreationDate() {
    return new Date(creationDate.getTime());
  }

  public void setCreationDate(final Date creationDate) {
    this.creationDate = creationDate;
  }

  public HashSet<ExportAdditionalOptions> getAdditionalOptions() {
    return additionalOptions;
  }

  public void setAdditionalOptions(final HashSet<ExportAdditionalOptions> additionalOptions) {
    this.additionalOptions = new HashSet<>(additionalOptions);
  }

  public final boolean isEmailUser() {
    return additionalOptions.contains(ExportAdditionalOptions.EMAIL_USER);
  }

  public boolean isRemoveResults() {
    return additionalOptions.contains(ExportAdditionalOptions.REMOVE_RESULTS);
  }

  public final boolean isReturnFile() {
    return additionalOptions.contains(ExportAdditionalOptions.RETURN_FILE);
  }

  public final boolean isSubmitFileToService() {
    return additionalOptions.contains(ExportAdditionalOptions.SUBMIT_FILE_TO_SERVICE);
  }

  public boolean isTrackJobProcess() {
    return additionalOptions.contains(ExportAdditionalOptions.TRACK_JOB_PROGRESS);
  }

  public String getScenarioFileIdentifier() {
    return scenarioFileIdentifier;
  }

  public void setScenarioFileIdentifier(final String scenarioFileIdentifier) {
    this.scenarioFileIdentifier = scenarioFileIdentifier;
  }

  @Override
  public String toString() {
    return "ExportData [emailAddress=" + emailAddress + ", locale=" + locale + ", creationDate=" + creationDate + ", additionalOptions="
        + additionalOptions + "]";
  }

}
