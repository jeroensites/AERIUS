/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import jsinterop.annotations.JsProperty;

/**
 * Data object for a farm lodging fodder measure, including its reduction factors.<br>
 * The object is published in the RAV (Regeling Ammoniak en Veehouderij) appendix 3:<br>
 * http://wetten.overheid.nl/BWBR0013629/geldigheidsdatum_11-06-2015#Bijlage3 <br>
 * <br>
 * A fodder measure has an id, code, name and description.<br>
 * It reduces emissions in a lodging and can be stacked on top of regular {@link FarmLodgingCategory}.
 */
public class FarmLodgingFodderMeasureCategory extends AbstractEmissionCategory {

  private static final long serialVersionUID = 1L;

  public static class AmmoniaProportion implements Serializable {

    private static final long serialVersionUID = 4481617666781910735L;

    /**
     * The proportion of the total ammonia emissions originating from the floor as a fraction
     */
    private double proportionFloor;

    /**
     * The proportion of the total ammonia emissions originating from the manure pit as a fraction
     */
    private double proportionCellar;

    public AmmoniaProportion() {
      // Needed for GWT serialization.
    }

    public AmmoniaProportion(final double proportionFloor, final double proportionCellar) {
      this.proportionFloor = proportionFloor;
      this.proportionCellar = proportionCellar;
    }

    public double getProportionFloor() {
      return proportionFloor;
    }

    public double getProportionCellar() {
      return proportionCellar;
    }
  }

  /**
   * Reduction factor for the emissions originating from the floor as a fraction
   */
  private double reductionFactorFloor;

  /**
   * Reduction factor for the emissions originating from the manure pit as a fraction
   */
  private double reductionFactorCellar;

  /**
   * Reduction factor for the emissions originating from both the floor and the manure pit as a fraction,
   * only use when there is only one measure.
   */
  private double reductionFactorTotal;

  /**
   * The proportions between ammonia emissions originating from the floor and the manure pit
   * per animal category. When there's no proportion for an animal category, then this
   * measure should not be used on the lodging type.
   */
  private @JsProperty Map<FarmAnimalCategory, AmmoniaProportion> ammoniaProportions = new HashMap<>();

  /**
   * Default public constructor.
   */
  public FarmLodgingFodderMeasureCategory() {
    // No-op.
  }

  public FarmLodgingFodderMeasureCategory(final int id, final String code, final String name, final String description,
      final double reductionFactorFloor, final double reductionFactorCellar, final double reductionFactorTotal) {
    super(id, code, name, description);
    this.reductionFactorFloor = reductionFactorFloor;
    this.reductionFactorCellar = reductionFactorCellar;
    this.reductionFactorTotal = reductionFactorTotal;
  }

  public double getReductionFactorFloor() {
    return reductionFactorFloor;
  }

  public void setReductionFactorFloor(final double reductionFactorFloor) {
    this.reductionFactorFloor = reductionFactorFloor;
  }

  public double getReductionFactorCellar() {
    return reductionFactorCellar;
  }

  public void setReductionFactorCellar(final double reductionFactorCellar) {
    this.reductionFactorCellar = reductionFactorCellar;
  }

  public double getReductionFactorTotal() {
    return reductionFactorTotal;
  }

  public void setReductionFactorTotal(final double reductionFactorTotal) {
    this.reductionFactorTotal = reductionFactorTotal;
  }

  public Map<FarmAnimalCategory, AmmoniaProportion> getAmmoniaProportions() {
    return ammoniaProportions;
  }

  public void setAmmoniaProportions(final Map<FarmAnimalCategory, AmmoniaProportion> ammoniaProportions) {
    this.ammoniaProportions = ammoniaProportions;
  }

  public void addAmmoniaProportion(final FarmAnimalCategory farmAnimalCategory, final double proportionFloor, final double proportionCellar) {
    ammoniaProportions.put(farmAnimalCategory, new AmmoniaProportion(proportionFloor, proportionCellar));
  }

  /**
   * Whether the fodder measure can be applied to given farm lodging system (animal category).
   */
  public boolean canApplyToFarmLodgingCategory(final FarmLodgingCategory farmLodgingCategory) {
    return ammoniaProportions.containsKey(farmLodgingCategory.getAnimalCategory());
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("ammoniaProportions=[");
    final int initialLength = sb.length();
    for (final Entry<FarmAnimalCategory, AmmoniaProportion> entry : ammoniaProportions.entrySet()) {
      if (sb.length() != initialLength) {
        sb.append("; ");
      }
      sb.append(entry.getKey().getCode());
      sb.append("=");
      sb.append(entry.getValue().getProportionFloor());
      sb.append(":");
      sb.append(entry.getValue().getProportionCellar());
    }
    sb.append("], ");

    return super.toString()
        + ", reductionFactorFloor=" + reductionFactorFloor
        + ", reductionFactorCellar=" + reductionFactorCellar
        + ", " + sb.toString()
        + "]";
  }

}
