/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.export;

import java.io.Serializable;
import java.util.Arrays;

import nl.overheid.aerius.shared.domain.DownloadInfo;

/**
 *
 */
public class ExportedData implements Serializable {

  private static final long serialVersionUID = 1L;

  private DownloadInfo downloadInfo;
  private String fileName;
  private byte[] fileContent;
  private int filesInZip;
  private String fileCode;

  public DownloadInfo getDownloadInfo() {
    return downloadInfo;
  }

  public void setDownloadInfo(final DownloadInfo downloadInfo) {
    this.downloadInfo = downloadInfo;
  }

  public String getFileCode() {
    return fileCode;
  }

  public void setFileCode(final String fileCode) {
    this.fileCode = fileCode;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(final String fileName) {
    this.fileName = fileName;
  }

  public byte[] getFileContent() {
    return fileContent;
  }

  public void setFileContent(final byte[] fileContent) {
    this.fileContent = fileContent;
  }

  public int getFilesInZip() {
    return filesInZip;
  }

  public void setFilesInZip(final int filesInZip) {
    this.filesInZip = filesInZip;
  }

  @Override
  public String toString() {
    return "ExportedData [downloadInfo=" + downloadInfo + ", fileName=" + fileName + ", fileContent=" + Arrays.toString(fileContent) + ", filesInZip="
        + filesInZip + "]";
  }
}
