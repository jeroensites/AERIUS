/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;

/**
 * Categories related to plans. A plan category consists of a unit and a factor
 * for the specific unit or no unit. Plans have no year specific factors.
 */
public class PlanCategory extends AbstractEmissionCategory {

  public enum CategoryUnit {
    HECTARE, GIGA_JOULE, MEGA_WATT_HOURS, COUNT,
    // No unit means the emission factor is 1.
    NO_UNIT, SQUARE_METERS, TONNES;
  }

  private static final long serialVersionUID = 2L;

  private OPSSourceCharacteristics characteristics;
  private CategoryUnit categoryUnit;
  private @JsProperty Map<Substance, Double> emissionFactors = new HashMap<>();

  public CategoryUnit getCategoryUnit() {
    return categoryUnit;
  }

  public OPSSourceCharacteristics getCharacteristics() {
    return characteristics;
  }

  public double getEmissionFactor(final EmissionValueKey key) {
    return getEmissionFactor(key.getSubstance());
  }

  public double getEmissionFactor(final Substance substance) {
    return emissionFactors.containsKey(substance) ? emissionFactors.get(substance) : 0;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() ? getId() == ((PlanCategory) obj).getId() : false;
  }

  public ArrayList<Substance> getKeys() {
    return new ArrayList<Substance>(emissionFactors.keySet());
  }

  @Override
  public int hashCode() {
    return getId();
  }

  public void setCategoryUnit(final CategoryUnit categoryUnit) {
    this.categoryUnit = categoryUnit;
  }

  public void setCharacteristics(final OPSSourceCharacteristics characteristics) {
    this.characteristics = characteristics;
  }

  /**
   * @param substance the Substance to set the emission factor for.
   * @param emissionFactor the emission factor to set.
   */
  public void setEmissionFactor(final Substance substance, final Double emissionFactor) {
    this.emissionFactors.put(substance, emissionFactor);
  }

  @Override
  public String toString() {
    return super.toString() + ", characteristics=" + characteristics + ", categoryUnit=" + categoryUnit;
  }
}
