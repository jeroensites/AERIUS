/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

/**
 * A farm field category.
 */
public class FarmlandCategory extends AbstractEmissionCategory {

  private static final long serialVersionUID = 3L;

  private int sectorCode;

  public FarmlandCategory() {
    // No-op.
  }

  public FarmlandCategory(final int id, final String code, final String name, final String description) {
    super(id, code, name, description);
  }

  public int getSectorCode() {
    return this.sectorCode;
  }

  public void setSectorCode(final int i) {
    this.sectorCode = i;
  }

}
