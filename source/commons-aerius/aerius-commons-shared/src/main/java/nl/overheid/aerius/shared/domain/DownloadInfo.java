/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

import java.io.Serializable;

/**
 * Data object to pass download url and information if it was possible to send an e-mail.
 */
public class DownloadInfo implements Serializable {

  private static final long serialVersionUID = -608162841765345976L;

  private String url;
  private String emailAddress;
  private boolean emailSuccess;

  public String getEmailAddress() {
    return emailAddress;
  }

  public String getUrl() {
    return url;
  }

  public boolean isEmailSuccess() {
    return emailSuccess;
  }

  public void setEmailAddress(final String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public void setEmailSuccess(final boolean emailSuccess) {
    this.emailSuccess = emailSuccess;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  @Override
  public String toString() {
    return "DownloadInfo [url=" + url + ", emailAddress=" + emailAddress + ", emailSuccess=" + emailSuccess + "]";
  }
}
