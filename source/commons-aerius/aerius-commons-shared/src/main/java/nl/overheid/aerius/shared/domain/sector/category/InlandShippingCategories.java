/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import jsinterop.annotations.JsProperty;

/**
 *
 */
public class InlandShippingCategories implements Serializable {

  private static final long serialVersionUID = 2L;

  private @JsProperty List<InlandShippingCategory> shipCategories = new ArrayList<>();

  private @JsProperty List<InlandWaterwayCategory> waterwayCategories = new ArrayList<>();

  private @JsProperty Map<InlandShippingCategory, List<InlandWaterwayCategory>> shipsToWaterways = new HashMap<>();

  public List<InlandShippingCategory> getShipCategories() {
    return shipCategories;
  }

  public void setShipCategories(final List<InlandShippingCategory> inlandShippingCategories) {
    this.shipCategories = inlandShippingCategories;
  }

  public List<InlandWaterwayCategory> getWaterwayCategories() {
    return waterwayCategories;
  }

  public void setWaterwayCategories(final List<InlandWaterwayCategory> waterwayCategories) {
    this.waterwayCategories = waterwayCategories;
  }

  /**
   * private setter to ensure the shipsToWaterways field can never be final
   */
  private void setShipsToWaterways(final Map<InlandShippingCategory, List<InlandWaterwayCategory>> shipsToWaterways) {
    this.shipsToWaterways = shipsToWaterways;
  }

  public InlandShippingCategory getShipCategoryByCode(final String code) {
    return AbstractCategory.determineByCode(shipCategories, code);
  }

  public InlandShippingCategory getShipCategoryById(final int id) {
    return AbstractCategory.determineById(shipCategories, id);
  }

  public InlandWaterwayCategory getWaterwayCategoryByCode(final String type) {
    return AbstractCategory.determineByCode(waterwayCategories, type);
  }

  public InlandWaterwayCategory getWaterwayCategoryById(final int id) {
    return AbstractCategory.determineById(waterwayCategories, id);
  }

  /**
   * Adds a shipping category to be a valid category for the given waterway category.
   * @param shipCategory shipping category to add
   * @param waterwayCategory waterway to relate shipping category to.
   */
  public void addWaterwayForShip(final InlandShippingCategory shipCategory, final InlandWaterwayCategory waterwayCategory) {
    if (!shipsToWaterways.containsKey(shipCategory)) {
      shipsToWaterways.put(shipCategory, new ArrayList<InlandWaterwayCategory>());
    }
    shipsToWaterways.get(shipCategory).add(waterwayCategory);
  }

  /**
   * Returns true if the given ship category is valid on the given waterway category.
   * @param shipCategory ship category to check
   * @param waterwayCategory waterway category to check
   * @return true if valid combination
   */
  public boolean isValidCombination(final InlandShippingCategory shipCategory, final InlandWaterwayCategory waterwayCategory) {
    return shipsToWaterways.containsKey(shipCategory) && shipsToWaterways.get(shipCategory).contains(waterwayCategory);
  }

  /**
   * Returns true if the given ship category code is valid on the given waterway category code.
   * @param shipCategoryCode ship category code to check
   * @param waterwayCategoryCode waterway category code to check
   * @return true if valid combination
   */
  public boolean isValidCombinationByCodes(final String shipCategoryCode, final String waterwayCategoryCode) {
    final Optional<InlandShippingCategory> shipCategory = Optional.ofNullable(getShipCategoryByCode(shipCategoryCode));
    final Optional<InlandWaterwayCategory> waterwayCategory = Optional.ofNullable(getWaterwayCategoryByCode(waterwayCategoryCode));
    return shipCategory.isPresent() && waterwayCategory.isPresent() && isValidCombination(shipCategory.get(), waterwayCategory.get());
  }

}
