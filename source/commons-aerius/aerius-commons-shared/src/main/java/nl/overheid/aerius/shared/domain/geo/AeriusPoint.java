/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.geo;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.HasId;

/**
 * Base class for all AERIUS points on the map. A point can represent a receptor, meaning on the hexagon grid or a free/normal point.
 */
public class AeriusPoint extends SridPoint implements Serializable, HasId {

  private static final long serialVersionUID = 3L;

  /**
   * Identifies how this point should be interpreted.
   */
  public enum AeriusPointType {
    /**
     * Hexagon of 100 m2, point is at the center of the hexagon.
     */
    RECEPTOR,
    /**
     * Normal point.
     */
    POINT;
  }

  private int id;
  private String gmlId;
  private AeriusPointType pointType;
  private String label;
  private Double height;

  private transient String gml;

  // Needed by GWT.
  public AeriusPoint() {
    pointType = AeriusPointType.RECEPTOR;
  }

  /**
   * Initializes this AeriusPoint with the given ID.
   *
   * @param id the ID
   */
  public AeriusPoint(final int id) {
    this(id, AeriusPointType.RECEPTOR);
  }

  public AeriusPoint(final AeriusPointType pointType) {
    this.pointType = pointType;
  }

  public AeriusPoint(final int id, final AeriusPointType pointType) {
    this.id = id;
    this.pointType = pointType;
  }

  /**
   * Initializes this AeriusPoint with the given location.
   *
   * @param x x coordinate
   * @param y y coordinate
   */
  public AeriusPoint(final double x, final double y) {
    this(0, x, y);
  }

  /**
   * Initializes this AeriusPoint with the given location.
   *
   * @param x x coordinate
   * @param y y coordinate
   * @param id the ID
   */
  protected AeriusPoint(final int id, final double x, final double y) {
    this(id, AeriusPointType.RECEPTOR, x, y);
  }

  /**
   * @param id
   * @param pointType
   * @param x
   * @param y
   */
  public AeriusPoint(final int id, final AeriusPointType pointType, final double x, final double y) {
    this(id, pointType, x, y, null);
  }

  /**
   * @param id
   * @param pointType
   * @param x
   * @param y
   * @param height
   */
  public AeriusPoint(final int id, final AeriusPointType pointType, final double x, final double y, final Double height) {
    super(x, y);
    this.id = id;
    this.pointType = pointType;
    this.height = height;
  }

  public AeriusPoint copy() {
    return copy(new AeriusPoint());
  }

  protected AeriusPoint copy(final AeriusPoint copy) {
    copy.id = id;
    copy.pointType = pointType;
    copy.label = label;
    copy.setX(getX());
    copy.setY(getY());
    copy.setHeight(getHeight());
    return copy;
  }

  /**
   * Objects are equal if same type and same id or if obj is a point and the super is equal.
   */
  @Override
  public boolean equals(final Object obj) {
    return obj instanceof AeriusPoint ? pointType == ((AeriusPoint) obj).getPointType() && id == ((AeriusPoint) obj).getId() : super.equals(obj);
  }

  @Override
  public int hashCode() {
    return id + 31 * pointType.hashCode();
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  public String getGmlId() {
    return gmlId;
  }

  public void setGmlId(final String gmlId) {
    this.gmlId = gmlId;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(final String label) {
    this.label = label;
  }

  public void setPointType(final AeriusPointType pointType) {
    this.pointType = pointType;
  }

  public AeriusPointType getPointType() {
    return pointType;
  }

  public String getGml() {
    return gml;
  }

  public void setGml(final String gml) {
    this.gml = gml;
  }

  public Double getHeight() {
    return height;
  }

  public void setHeight(final Double height) {
    this.height = height;
  }

  @Override
  public String toString() {
    return "AeriusPoint [id=" + id + "(" + pointType + "), gmlId=" + gmlId + ", " + super.toString() + "]";
  }
}
