/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.shared.domain.scenario.SituationType;

public enum ScenarioResultType {
  SITUATION_RESULT,
  PROJECT_CALCULATION,
  MAX_TEMPORARY_CONTRIBUTION,
  MAX_TEMPORARY_EFFECT;

  public static List<ScenarioResultType> getResultTypesForSituationType(final SituationType situationType) {
    final List<ScenarioResultType> scenarioResultTypes = new ArrayList<ScenarioResultType>();

    if (situationType != SituationType.UNKNOWN) {
      scenarioResultTypes.add(ScenarioResultType.SITUATION_RESULT);
    }
    if (situationType == SituationType.PROPOSED){
      scenarioResultTypes.add(ScenarioResultType.PROJECT_CALCULATION);
    }
    if (situationType == SituationType.TEMPORARY) {
      scenarioResultTypes.add(ScenarioResultType.MAX_TEMPORARY_CONTRIBUTION);
      scenarioResultTypes.add(ScenarioResultType.MAX_TEMPORARY_EFFECT);
    }

    return scenarioResultTypes;
  }

}
