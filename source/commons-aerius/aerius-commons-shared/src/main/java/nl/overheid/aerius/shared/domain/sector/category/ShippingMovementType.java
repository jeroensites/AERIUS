/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;

/**
 * {@link ShippingMovementType} containing the different types of movement for shipping.
 */
public enum ShippingMovementType implements Serializable {
  /**
   * Ship is docked.
   */
  DOCK,
  /**
   * Ship is traveling inland/in port.
   */
  INLAND,
  /**
   * Ship is traveling on sea.
   */
  MARITIME;

  /**
   * Returns the name in lower case.
   *
   * @return name in lower case.
   */
  @Override
  public String toString() {
    return name().toLowerCase();
  }
}
