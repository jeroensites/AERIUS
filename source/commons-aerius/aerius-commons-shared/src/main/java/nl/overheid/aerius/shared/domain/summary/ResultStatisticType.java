/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.summary;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ResultStatisticType {

  /**
   * The summed cartographic surface calculated in m^2.
   */
  SUM_CARTOGRAPHIC_SURFACE("sumCartographicSurface"),
  /**
   * The summed cartographic surface with an increase in deposition calculated in m^2.
   */
  SUM_CARTOGRAPHIC_SURFACE_INCREASE("sumCartographicSurfaceIncrease"),
  /**
   * The summed cartographic surface with an decrease in deposition calculated in m^2.
   */
  SUM_CARTOGRAPHIC_SURFACE_DECREASE("sumCartographicSurfaceDecrease"),
  /**
   * The number of calculation points.
   */
  COUNT_CALCULATION_POINTS("countCalculationPoints"),
  /**
   * The number of calculation points with an increase in deposition.
   */
  COUNT_CALCULATION_POINTS_INCREASE("countCalculationPointsIncrease"),
  /**
   * The number of calculation points with an increase in deposition.
   */
  COUNT_CALCULATION_POINTS_DECREASE("countCalculationPointsDecrease"),
  /**
   * The contribution of the calculation.
   * In case of deposition, this is in mol/ha/y.
   */
  MAX_CONTRIBUTION("maxContribution"),
  /**
   * The total result of the contribution of the calculation and the background value.
   * In case of deposition, this is in mol/ha/y.
   */
  MAX_TOTAL("maxTotal"),
  /**
   * The largest increase in deposition for the project calculation
   */
  MAX_INCREASE("maxIncrease"),
  /**
   * The largest decrease in deposition for the project calculation
   */
  MAX_DECREASE("maxDecrease"),
  /**
   * Highest temporary contribution of the temporary situations.
   */
  MAX_TEMP_CONTRIBUTION("maxTempContribution"),
  /**
   * Highest temporary increase of deposition of the temporary situations.
   */
  MAX_TEMP_INCREASE("maxTempIncrease");

  private final String jsonKey;

  private ResultStatisticType(final String jsonKey) {
    this.jsonKey = jsonKey;
  }

  @JsonValue
  public String getJsonKey() {
    return jsonKey;
  }

  public static List<ResultStatisticType> getValuesForResult(final ScenarioResultType resultType,
      final SummaryHexagonType hexagonType) {
    if (hexagonType == SummaryHexagonType.CUSTOM_CALCULATION_POINTS) {
      return getValuesForScenarioResultTypeCustomPoints(resultType);
    }
    return getValuesForScenarioResultTypeHexagons(resultType);
  }

  private static List<ResultStatisticType> getValuesForScenarioResultTypeHexagons(final ScenarioResultType resultType) {
    final List<ResultStatisticType> resultStatisticTypes = new ArrayList<>();
    resultStatisticTypes.add(SUM_CARTOGRAPHIC_SURFACE);
    resultStatisticTypes.add(MAX_TOTAL);

    switch (resultType) {
      case SITUATION_RESULT:
        resultStatisticTypes.add(MAX_CONTRIBUTION);
        break;
      case PROJECT_CALCULATION:
        resultStatisticTypes.add(SUM_CARTOGRAPHIC_SURFACE_INCREASE);
        resultStatisticTypes.add(MAX_INCREASE);
        resultStatisticTypes.add(SUM_CARTOGRAPHIC_SURFACE_DECREASE);
        resultStatisticTypes.add(MAX_DECREASE);
        break;
      case MAX_TEMPORARY_CONTRIBUTION:
        resultStatisticTypes.add(MAX_TEMP_CONTRIBUTION);
        break;
      case MAX_TEMPORARY_EFFECT:
        resultStatisticTypes.add(MAX_TEMP_INCREASE);
        break;
    }

    return resultStatisticTypes;
  }

  private static List<ResultStatisticType> getValuesForScenarioResultTypeCustomPoints(final ScenarioResultType resultType) {
    final List<ResultStatisticType> resultStatisticTypes = new ArrayList<>();
    resultStatisticTypes.add(COUNT_CALCULATION_POINTS);

    switch (resultType) {
      case SITUATION_RESULT:
        resultStatisticTypes.add(MAX_CONTRIBUTION);
        break;
      case PROJECT_CALCULATION:
        resultStatisticTypes.add(COUNT_CALCULATION_POINTS_INCREASE);
        resultStatisticTypes.add(COUNT_CALCULATION_POINTS_DECREASE);
        resultStatisticTypes.add(MAX_INCREASE);
        resultStatisticTypes.add(MAX_DECREASE);
        break;
      case MAX_TEMPORARY_CONTRIBUTION:
        resultStatisticTypes.add(MAX_TEMP_CONTRIBUTION);
        break;
      case MAX_TEMPORARY_EFFECT:
        resultStatisticTypes.add(MAX_TEMP_INCREASE);
        break;
    }

    return resultStatisticTypes;
  }

  /**
   * Returns the ResultStatisticType associated by the jsonKey
   */
  public static ResultStatisticType getValueByJsonKey(final String jsonKey) {
    for (final ResultStatisticType value : values()) {
      if (value.getJsonKey().equals(jsonKey)) {
        return value;
      }
    }
    return null;
  }

}
