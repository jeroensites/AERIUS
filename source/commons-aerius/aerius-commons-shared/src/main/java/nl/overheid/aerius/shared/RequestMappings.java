/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared;

public final class RequestMappings {
  private static final String SLASH = "/";

  public static final long MAX_LIST_SIZE_SANITY = 1000;

  public static final String AERIUS_SYSTEM_INFO_UPDATE = "/aerius-system-info-update";
  public static final String AERIUS_SYSTEM_INFO_UPDATE_MESSAGE = AERIUS_SYSTEM_INFO_UPDATE + "?message";

  public static final String CALCULATION_CODE_BARE = "calculationCode";
  public static final String CALCULATION_CODE = "{" + CALCULATION_CODE_BARE + "}";
  public static final String SITUATION_CODE_BARE = "situationCode";
  public static final String SITUATION_CODE = "{" + SITUATION_CODE_BARE + "}";
  public static final String RESULT_TYPE_BARE = "resultType";
  public static final String RESULT_TYPE = "{" + RESULT_TYPE_BARE + "}";
  public static final String IMPORT_CODE_BARE = "importCode";
  public static final String IMPORT_CODE = "{" + IMPORT_CODE_BARE + "}";
  public static final String EXPORT_CODE_BARE = "exportCode";
  public static final String EXPORT_CODE = "{" + EXPORT_CODE_BARE + "}";
  public static final String FILE_CODE_BARE = "fileCode";
  public static final String FILE_CODE = "{" + FILE_CODE_BARE + "}";
  public static final String FILE_CODES_BARE = "fileCodes";
  public static final String FILE_CODES = "{" + FILE_CODES_BARE + "}";
  public static final String LINKED_FILE_TYPE = "{linkedFileType}";
  public static final String FILE = "file";

  /** CONNECT PARAMS **/
  private static final String CONNECT_API_VERSION = "v7";
  private static final String UI = "ui";
  private static final String IMPORT = "import";
  private static final String CALCULATION = "calculation";
  private static final String CANCEL = "cancel";
  private static final String INFO = "info";
  private static final String SHIPPING = "shipping";
  private static final String SOURCE = "source";
  private static final String SUGGEST_INLAND_SHIPPING_WATERWAY = "suggestwaterway";
  private static final String REFRESH_EMISSIONS = "refreshEmissions";
  private static final String LINKED_FILE_TYPE_VALIDATION_RESULT = "validationresult";
  private static final String LINKED_FILE_TYPE_IMPORT_PARCEL = "result";

  /** CONNECT **/
  public static final String CONNECT_VERSION = SLASH + CONNECT_API_VERSION + SLASH;

  public static final String CONNECT_UI_IMPORT = CONNECT_VERSION + UI + SLASH + IMPORT;

  private static final String CONNECT_UI_RETRIEVE_LINKED_FILE = CONNECT_UI_IMPORT + SLASH + FILE_CODE + SLASH;
  public static final String CONNECT_UI_RETRIEVE_VALIDATION_RESULT = CONNECT_UI_RETRIEVE_LINKED_FILE + LINKED_FILE_TYPE_VALIDATION_RESULT;
  public static final String CONNECT_UI_RETRIEVE_IMPORT_PARCEL = CONNECT_UI_RETRIEVE_LINKED_FILE + LINKED_FILE_TYPE_IMPORT_PARCEL;

  public static final String CONNECT_UI_DELETE_FILE = CONNECT_UI_IMPORT + SLASH + FILE_CODE;

  public static final String CONNECT_UI_FILE = CONNECT_VERSION + UI + SLASH + FILE;
  public static final String CONNECT_UI_RETRIEVE_RAW_FILE = CONNECT_UI_FILE + SLASH + FILE_CODE;

  public static final String CONNECT_UI_CALCULATE = CONNECT_VERSION + UI + SLASH + CALCULATION;
  public static final String CONNECT_UI_CALCULATE_INFO = CONNECT_VERSION + UI + SLASH + CALCULATION + SLASH + CALCULATION_CODE;

  public static final String CONNECT_UI_CALCULATE_DELETE = CONNECT_VERSION + UI + SLASH + CALCULATION + SLASH + CALCULATION_CODE;
  public static final String CONNECT_UI_CALCULATE_CANCEL = CONNECT_VERSION + UI + SLASH + CALCULATION + SLASH + CALCULATION_CODE + SLASH + CANCEL;

  public static final String CONNECT_UI_RECEPTOR_INFO = CONNECT_VERSION + UI + SLASH + INFO;

  public static final String CONNECT_UI_CALCULATION_SUMMARY = CONNECT_VERSION + UI + SLASH + CALCULATION + SLASH + CALCULATION_CODE
      + SLASH + SITUATION_CODE + SLASH + RESULT_TYPE;

  public static final String CONNECT_UI_REFRESH_EMISSIONS = CONNECT_VERSION + UI + SLASH + SOURCE
      + SLASH + REFRESH_EMISSIONS;
  public static final String CONNECT_UI_SUGGEST_INLAND_SHIPPING_WATERWAY = CONNECT_VERSION + UI + SLASH + SHIPPING
      + SLASH + SUGGEST_INLAND_SHIPPING_WATERWAY;

  public static final String CONNECT_HEXAGON_TYPE_PARAM = "summaryHexagonType";

  /** WEBSERVER **/
  public static final String PRINT_WNB = "print-wnb";

  private RequestMappings() {}
}
