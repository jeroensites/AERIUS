/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import jsinterop.annotations.JsProperty;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;

/**
 * Data object for Road Emission factor.
 *
 * NOTE: does not extend {@link AbstractEmissionCategory} as it does not have an ID or code at the moment.
 */
public class RoadEmissionCategory implements Serializable, Comparable<RoadEmissionCategory> {

  private static final long serialVersionUID = 4L;

  // if the speed is 0, it's irrelevant.
  private int maximumSpeed;
  private RoadType roadType;
  private VehicleType vehicleType;
  private boolean strictEnforcement;
  private RoadSpeedType speedType;

  /**
   * Emission factor in g/24h/km
   */
  private @JsProperty Map<EmissionValueKey, Double> emissionFactors = new HashMap<>();
  private @JsProperty Map<EmissionValueKey, Double> stagnatedEmissionFactors = new HashMap<>();

  public RoadEmissionCategory(final RoadType roadType, final VehicleType vehicleType, final boolean strictEnforcement, final int maximumSpeed,
      final RoadSpeedType speedType) {
    this.roadType = roadType;
    this.vehicleType = vehicleType;
    this.strictEnforcement = strictEnforcement;
    this.maximumSpeed = maximumSpeed;
    this.speedType = speedType;
    // In the earlier design SpeedRoadType was not specified and when read via IMAER it gets the value null or set via the UI.
    // Therefore if speedType is null and it's an urban road it's set to value used in the PAS: RoadSpeedType.E.
    if (speedType == null && roadType == RoadType.URBAN_ROAD) {
      this.speedType = RoadSpeedType.URBAN_TRAFFIC_FREE_FLOW;
      // If it's null and non urban, use national road when maximum speed is available as indication of SRM2,
      // and use non urban traffic (RoadSpeedType.B) when there is no maximum speed
    } else if (speedType == null && roadType == RoadType.NON_URBAN_ROAD) {
      this.speedType = maximumSpeed > 0 ? RoadSpeedType.NATIONAL_ROAD : RoadSpeedType.NON_URBAN_TRAFFIC;
      // And in the case of freeways never use the speed type as the distinction is done solely based on maxim
    } else if (roadType == RoadType.FREEWAY) {
      this.speedType = null;
    }
  }

  protected RoadEmissionCategory() {
    // Needed for GWT.
  }

  @Override
  public int compareTo(final RoadEmissionCategory other) {
    return compareTo(
        compareTo(roadType, other.getRoadType()),
        compareTo(speedType, other.getSpeedType()),
        compareTo(vehicleType, other.getVehicleType()),
        //Ensuring categories that are not strict enforced get sorted first
        Boolean.compare(other.isStrictEnforcement(), strictEnforcement),
        Integer.compare(maximumSpeed, other.getMaximumSpeed()));
  }

  private <E extends Enum<E>> int compareTo(final E a, final E b) {
    return a == null ? (b == null ? 0 : -1) : (b == null ? 1 : a.compareTo(b));
  }

  private int compareTo(final int... compares) {
    for (final int cmp : compares) {
      if (cmp != 0) {
        return cmp;
      }
    }
    return 0;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && getClass() == obj.getClass() && compareTo((RoadEmissionCategory) obj) == 0;
  }

  /**
   * Returns true if same road and vehicle type.
   * @param rec road category to compare
   * @return true if same road and vehicle type
   */
  public boolean equalsType(final RoadEmissionCategory rec) {
    return rec != null
        && compareTo(roadType, rec.getRoadType()) == 0
        && compareTo(speedType, rec.getSpeedType()) == 0
        && compareTo(vehicleType, rec.getVehicleType()) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(roadType, vehicleType, maximumSpeed, strictEnforcement, speedType);
  }

  @Min(value = 0)
  public int getMaximumSpeed() {
    return maximumSpeed;
  }

  public RoadType getRoadType() {
    return roadType;
  }

  public RoadSpeedType getSpeedType() {
    return speedType;
  }

  @NotNull
  public VehicleType getVehicleType() {
    return vehicleType;
  }

  public boolean isStrictEnforcement() {
    return strictEnforcement;
  }

  public double getEmissionFactor(final EmissionValueKey evk) {
    return emissionFactors.containsKey(evk) ? emissionFactors.get(evk) : 0;
  }

  public double getStagnatedEmissionFactor(final EmissionValueKey evk) {
    return stagnatedEmissionFactors.containsKey(evk) ? stagnatedEmissionFactors.get(evk) : 0;
  }

  public ArrayList<EmissionValueKey> getEmissionValueKeys() {
    return new ArrayList<EmissionValueKey>(emissionFactors.keySet());
  }

  public void setEmissionFactor(final EmissionValueKey evk, final double emissionFactor) {
    emissionFactors.put(evk, emissionFactor);
  }

  public void setStagnatedEmissionFactor(final EmissionValueKey evk, final double stagnatedEmissionFactor) {
    stagnatedEmissionFactors.put(evk, stagnatedEmissionFactor);
  }

  public boolean isSpeedRelevant() {
    return maximumSpeed > 0;
  }

  @Override
  public String toString() {
    return "RoadEmissionCategory [" + roadType + (strictEnforcement ? "(strict)" : "") + (speedType == null ? "" : ("(" + speedType + ")"))
        + ",max:" + maximumSpeed + ":" + vehicleType + "]";
  }
}
