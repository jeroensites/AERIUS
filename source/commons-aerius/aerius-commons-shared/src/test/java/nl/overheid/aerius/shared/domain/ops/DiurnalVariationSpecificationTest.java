/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.ops;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link DiurnalVariationSpecification}.
 */
class DiurnalVariationSpecificationTest {

  private static final String CODE = "code";

  @Test
  void testEquals() {
    final DiurnalVariationSpecification d1 = new DiurnalVariationSpecification();
    final DiurnalVariationSpecification d2 = new DiurnalVariationSpecification();
    assertNotEquals(d1, new Object(), "Equals not same other object");
    d1.setCode(CODE);
    assertNotEquals(d1, d2, "Not equals Code");
    d2.setCode(CODE);
    assertEquals(d1, d2, "Equals Code");
    d1.setId(1);
    assertNotEquals(d1, d2, "Not equals id");
    d2.setId(1);
    assertEquals(d1, d2, "Equals id");
    d1.setName("Name");
    assertEquals(d1, d2, "Equals even if name different");
    d1.setDescription("Description");
    assertEquals(d1, d2, "Equals even if description different");
  }
}
