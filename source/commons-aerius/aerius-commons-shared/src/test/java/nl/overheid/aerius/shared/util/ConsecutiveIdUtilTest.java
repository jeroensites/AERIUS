/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class ConsecutiveIdUtilTest {

  @Test
  void testRemoval() {
    final List<String> testList = new ArrayList<>();

    final ConsecutiveIdUtil ciu = new ConsecutiveIdUtil(i -> "ES." + (i + 1), testList::get, testList::size);

    testList.add("ES.1");
    testList.add("ES.2");
    testList.add("ES.2123123");

    testList.remove(1);

    final ConsecutiveIdUtil.IndexIdPair idToAdd1 = ciu.generate();
    testList.add(idToAdd1.getIndex(), idToAdd1.getId());

    final ConsecutiveIdUtil.IndexIdPair idToAdd2 = ciu.generate();
    testList.add(idToAdd2.getIndex(), idToAdd2.getId());

    assertEquals(4, testList.size());
  }

  @Test
  void testConsecutiveIds() {
    final List<String> testList = new ArrayList<>();
    final ConsecutiveIdUtil ciu = new ConsecutiveIdUtil(i -> "ES." + (i + 1), testList::get, testList::size);

    // Test add to empty list
    ConsecutiveIdUtil.IndexIdPair idToAdd = ciu.generate();
    assertEquals("ES.1", idToAdd.getId(), "");
    assertEquals(0, idToAdd.getIndex(), "");
    testList.add(idToAdd.getIndex(), idToAdd.getId());
    // End result
    assertEquals("[ES.1]", testList.toString(), "");

    // Add some test elements
    testList.add("ES.2");
    testList.add("ES.3");
    testList.add("ES.4");
    // End result
    assertEquals("[ES.1, ES.2, ES.3, ES.4]", testList.toString(), "");

    // Test add to end of list
    idToAdd = ciu.generate();
    assertEquals("ES.5", idToAdd.getId(), "");
    assertEquals(4, idToAdd.getIndex(), "");
    testList.add(idToAdd.getIndex(), idToAdd.getId());
    // End result
    assertEquals("[ES.1, ES.2, ES.3, ES.4, ES.5]", testList.toString(), "");

    // Create gaps
    testList.remove(2); // Remove ES.3
    testList.remove(0); // Remove ES.1
    // End result
    assertEquals("[ES.2, ES.4, ES.5]", testList.toString(), "");

    // Test add in gap
    idToAdd = ciu.generate();
    assertEquals("ES.1", idToAdd.getId(), "");
    assertEquals(0, idToAdd.getIndex(), "");
    testList.add(idToAdd.getIndex(), idToAdd.getId());
    // Test add in gap
    idToAdd = ciu.generate();
    assertEquals("ES.3", idToAdd.getId(), "");
    assertEquals(2, idToAdd.getIndex(), "");
    testList.add(idToAdd.getIndex(), idToAdd.getId());
    // Test add to end
    idToAdd = ciu.generate();
    assertEquals("ES.6", idToAdd.getId(), "");
    assertEquals(5, idToAdd.getIndex(), "");
    testList.add(idToAdd.getIndex(), idToAdd.getId());

    // End result
    assertEquals("[ES.1, ES.2, ES.3, ES.4, ES.5, ES.6]", testList.toString(), "");
  }
}
