/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link FarmLodgingCategory}.
 */
class FarmLodgingCategoriesTest {

  private static final double EPSILON = 1E-5;

  private static int lastId;

  @Test
  void testDetermineByCode() {
    final FarmLodgingCategories cats = createFixture();
    assertEquals(2.1, cats.determineFarmLodgingCategoryByCode("B2.1.100.1").getEmissionFactor(), EPSILON, "B2.1.100.1");
    assertEquals(1.1, cats.determineAdditionalLodgingSystemByCode("E1.1").getEmissionFactor(), EPSILON, "E1.1");
    assertEquals(0.1, cats.determineReductiveLodgingSystemByCode("E1.1.0").getReductionFactor(), EPSILON, "E1.1.0");
    assertEquals(0.4, cats.determineLodgingFodderMeasureByCode("PAS 2015.03-01").getReductionFactorCellar(), EPSILON, "PAS 2015.03-01");
    assertNull(cats.determineFarmLodgingCategoryByCode("abc"), "Category by code should not be null");
  }

  @Test
  void testConstrainedReductionFactor() {
    final FarmLodgingCategory traditional = createFarmLodgingCategory(null, 5.0, null, null);
    final FarmLodgingCategory c1 = createFarmLodgingCategory(null, 3.0, null, traditional);
    final FarmLodgingCategory c2 = createFarmLodgingCategory(null, 1.5, null, traditional);
    final FarmLodgingCategory c3 = createFarmLodgingCategory(null, 1.0, null, traditional);

    assertNull(traditional.getTraditionalFarmLodgingCategory(), "TraditionalFarmLodgingCategory should be null");
    assertNotNull(c1.getTraditionalFarmLodgingCategory(), "C1 TraditionalFarmLodgingCategory should not be null");

    assertEquals(0.0, traditional.getReductionFactor(), EPSILON, "Reductionfactor traditional");
    assertEquals(0.4, c1.getReductionFactor(), EPSILON, "Reductionfactor c1");
    assertEquals(0.7, c2.getReductionFactor(), EPSILON, "Reductionfactor c2");
    assertEquals(0.8, c3.getReductionFactor(), EPSILON, "Reductionfactor c3");

    assertFalse(traditional.shouldConstrainReductionFactor(), "Traditional should not constrain reduction factor");
    assertFalse(c1.shouldConstrainReductionFactor(), "C1 should not constrain reduction factor");
    assertFalse(c2.shouldConstrainReductionFactor(), "C2 should not constrain reduction factor");
    assertTrue(c3.shouldConstrainReductionFactor(), "C3 should constrain reduction factor");

    assertEquals(5.0, traditional.getConstrainedEmissionFactor(), EPSILON, "ConstrainedEmissionFactor traditional");
    assertEquals(3.0, c1.getConstrainedEmissionFactor(), EPSILON, "ConstrainedEmissionFactor c1");
    assertEquals(1.5, c2.getConstrainedEmissionFactor(), EPSILON, "ConstrainedEmissionFactor c2");
    assertEquals(1.5, c3.getConstrainedEmissionFactor(), EPSILON, "ConstrainedEmissionFactor c3");
  }

  @Test
  void testCanStackCategory() {
    final FarmAdditionalLodgingSystemCategory a1 = createFarmAdditionalLodgingSystemCategory(null, 5.0);
    final FarmAdditionalLodgingSystemCategory a2 = createFarmAdditionalLodgingSystemCategory(null, 6.0);
    final FarmReductiveLodgingSystemCategory r1 = createFarmReductiveLodgingSystemCategory(null, 35.0);
    final FarmReductiveLodgingSystemCategory r2 = createFarmReductiveLodgingSystemCategory(null, 25.0);
    final FarmLodgingCategory c1 = createFarmLodgingCategory(null, 5.0, null, null);
    c1.getFarmAdditionalLodgingSystemCategories().add(a1);
    c1.getFarmReductiveLodgingSystemCategories().add(r1);

    assertTrue(c1.canStackAdditionalLodgingSystemCategory(a1), "Valid additional sytem");
    assertFalse(c1.canStackAdditionalLodgingSystemCategory(a2), "Not a valid additional sytem");
    assertFalse(c1.canStackAdditionalLodgingSystemCategory(null), "Null not a valid additional sytem");
    assertTrue(c1.canStackReductiveLodgingSystemCategory(r1), "Valid reductive sytem");
    assertFalse(c1.canStackReductiveLodgingSystemCategory(r2), "Not a valid reductive sytem");
    assertFalse(c1.canStackAdditionalLodgingSystemCategory(null), "Null not a valid reductive sytem");
  }

  @Test
  void testFodderCategories() {
    final FarmLodgingCategories cats = createFixture();
    final FarmLodgingCategory cat = cats.determineFarmLodgingCategoryByCode("A1.2");
    final FarmLodgingFodderMeasureCategory fod = cats.determineLodgingFodderMeasureByCode("PAS 2015.03-01");
    assertFalse(fod.canApplyToFarmLodgingCategory(cat), "Measure can't be applied to A1");
    fod.addAmmoniaProportion(cat.getAnimalCategory(), 0.3, 0.7);
    assertTrue(fod.canApplyToFarmLodgingCategory(cat), "Measure can now be applied to A1");
  }

  private static FarmLodgingCategories createFixture() {
    final FarmLodgingCategories cats = new FarmLodgingCategories();
    cats.setFarmLodgingSystemCategories(createFarmLodgingCategories());
    cats.setFarmAdditionalLodgingSystemCategories(createFarmAdditionalLodgingSystemCategories());
    cats.setFarmReductiveLodgingSystemCategories(createFarmReductiveLodgingSystemCategories());
    cats.setFarmLodgingFodderMeasureCategories(createFarmLodgingFodderMeasureCategories());
    return cats;
  }

  private static ArrayList<FarmLodgingCategory> createFarmLodgingCategories() {
    final ArrayList<FarmLodgingCategory> list = new ArrayList<>();
    list.add(createFarmLodgingCategory("A1.2", 3.0, "A1", null));
    list.add(createFarmLodgingCategory("B2.1.100.1", 2.1, "B2", null));
    list.add(createFarmLodgingCategory("D2.3.4", 0.7, "D2", null));
    return list;
  }

  private static ArrayList<FarmAdditionalLodgingSystemCategory> createFarmAdditionalLodgingSystemCategories() {
    final ArrayList<FarmAdditionalLodgingSystemCategory> list = new ArrayList<>();
    list.add(createFarmAdditionalLodgingSystemCategory("E1.1", 1.1));
    list.add(createFarmAdditionalLodgingSystemCategory("E2.1.0", 2.2));
    return list;
  }

  private static ArrayList<FarmReductiveLodgingSystemCategory> createFarmReductiveLodgingSystemCategories() {
    final ArrayList<FarmReductiveLodgingSystemCategory> list = new ArrayList<>();
    list.add(createFarmReductiveLodgingSystemCategory("B2.1", 0.25));
    list.add(createFarmReductiveLodgingSystemCategory("E1.1.0", 0.10));
    return list;
  }

  private static ArrayList<FarmLodgingFodderMeasureCategory> createFarmLodgingFodderMeasureCategories() {
    final ArrayList<FarmLodgingFodderMeasureCategory> list = new ArrayList<>();
    list.add(createFarmLodgingFodderMeasureCategory("PAS 2015.03-01", 0.16, 0.40, 0.35));
    list.add(createFarmLodgingFodderMeasureCategory("PAS 2015.04-01", 0.10, 0.10, 0.1));
    return list;
  }

  private static FarmLodgingCategory createFarmLodgingCategory(final String code, final double ef, final String animalCategory,
      final FarmLodgingCategory traditional) {
    return new FarmLodgingCategory(lastId++, code, code, "", ef, new FarmAnimalCategory(lastId++, animalCategory, animalCategory, ""),
        traditional, false);
  }

  private static FarmAdditionalLodgingSystemCategory createFarmAdditionalLodgingSystemCategory(final String code, final double ef) {
    return new FarmAdditionalLodgingSystemCategory(lastId++, code, code, "", ef, false);
  }

  private static FarmReductiveLodgingSystemCategory createFarmReductiveLodgingSystemCategory(final String code, final double ef) {
    return new FarmReductiveLodgingSystemCategory(lastId++, code, code, "", ef, false);
  }

  private static FarmLodgingFodderMeasureCategory createFarmLodgingFodderMeasureCategory(final String code, final double efFloor,
      final double efCellar, final double efTotal) {
    final FarmLodgingFodderMeasureCategory cat = new FarmLodgingFodderMeasureCategory(lastId++, code, code, "", efFloor, efCellar, efTotal);
    cat.addAmmoniaProportion(new FarmAnimalCategory(lastId++, "D1.1", "D1.1", ""), 0.1, 0.9);
    cat.addAmmoniaProportion(new FarmAnimalCategory(lastId++, "D3", "D3", ""), 0.3, 0.7);
    return cat;
  }
}
