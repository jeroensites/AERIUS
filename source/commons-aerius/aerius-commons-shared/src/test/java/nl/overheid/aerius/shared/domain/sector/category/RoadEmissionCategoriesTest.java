/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.TreeSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;

/**
 * Test class for {@link RoadEmissionCategories}.
 */
class RoadEmissionCategoriesTest {
  private static final RoadEmissionCategory AB_100_NOT_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.AUTO_BUS, false, 100, null);
  private static final String ONE_CATEGORY_ALWAYS = "With 1 category, this category should always be returned.";
  private static final RoadEmissionCategory URBAN_NOT_STRICT_C = new RoadEmissionCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 0,
      RoadSpeedType.URBAN_TRAFFIC_NORMAL);
  private static final RoadEmissionCategory URBAN_NOT_STRICT_E = new RoadEmissionCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 0,
      RoadSpeedType.URBAN_TRAFFIC_FREE_FLOW);

  private static final RoadEmissionCategory NON_URBAN_ROAD_LT = new RoadEmissionCategory(RoadType.NON_URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 0,
      null);
  private static final RoadEmissionCategory NATIONAL_ROAD_LT_80 = new RoadEmissionCategory(RoadType.NON_URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false,
      80, null);
  private static final RoadEmissionCategory NATIONAL_ROAD_LT_100 = new RoadEmissionCategory(RoadType.NON_URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false,
      100, null);

  private static final RoadEmissionCategory LT_50_NOT_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 50, null);
  private static final RoadEmissionCategory LT_80_NOT_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 80, null);
  private static final RoadEmissionCategory LT_80_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 80, null);
  private static final RoadEmissionCategory LT_100_NOT_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 100,
      null);
  private static final RoadEmissionCategory LT_120_NOT_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 120,
      null);
  private static final RoadEmissionCategory LT_120_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 120, null);
  private static final RoadEmissionCategory LT_130_NOT_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 130,
      null);
  private static final RoadEmissionCategory LT_130_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 130, null);

  private RoadEmissionCategories categories;

  @BeforeEach
  void before() {
    categories = createData();
  }

  @Test
  void testSpeedCategoriesSrm2() {
    final RoadSpeedType srm2RoadSpeedType = null;
    final TreeSet<Integer> speeds = categories.getMaximumSpeedCategories(RoadType.FREEWAY, srm2RoadSpeedType);

    assertNotNull(speeds, "No speed categories found.");
    assertEquals(5, speeds.size(), "Unexpected number of speed categories.");

    for (final int speed : new int[] {50, 80, 100, 120, 130}) {
      assertEquals(Integer.valueOf(speed), speeds.pollFirst(), "Unexpected speed category for " + speed);
    }

    final TreeSet<Integer> nonUrbanSpeeds = categories.getMaximumSpeedCategories(RoadType.NON_URBAN_ROAD, srm2RoadSpeedType);

    assertNotNull(nonUrbanSpeeds, "No speed categories found.");
    assertEquals(2, nonUrbanSpeeds.size(), "Unexpected number of speed categories.");

    for (final int speed : new int[] {80, 100}) {
      assertEquals(Integer.valueOf(speed), nonUrbanSpeeds.pollFirst(), "Unexpected speed category for " + speed);
    }

    final TreeSet<Integer> urbanSpeeds = categories.getMaximumSpeedCategories(RoadType.URBAN_ROAD, srm2RoadSpeedType);

    assertNotNull(urbanSpeeds, "No speed categories found.");
    assertEquals(1, urbanSpeeds.size(), "Unexpected number of speed categories.");

    for (final int speed : new int[] {0}) {
      assertEquals(Integer.valueOf(speed), urbanSpeeds.pollFirst(), "Unexpected speed category for " + speed);
    }
  }

  @Test
  void testSpeedCategoriesSrm1() {
    final TreeSet<Integer> speeds = categories.getMaximumSpeedCategories(RoadType.FREEWAY, RoadSpeedType.NON_URBAN_TRAFFIC);

    assertNotNull(speeds, "No speed categories found.");
    assertEquals(5, speeds.size(), "Unexpected number of speed categories.");

    for (final int speed : new int[] {50, 80, 100, 120, 130}) {
      assertEquals(Integer.valueOf(speed), speeds.pollFirst(), "Unexpected speed category for " + speed);
    }

    final TreeSet<Integer> nonUrbanSpeeds = categories.getMaximumSpeedCategories(RoadType.NON_URBAN_ROAD, RoadSpeedType.NON_URBAN_TRAFFIC);

    assertNotNull(nonUrbanSpeeds, "No speed categories found.");
    assertEquals(1, nonUrbanSpeeds.size(), "Unexpected number of speed categories.");

    for (final int speed : new int[] {0}) {
      assertEquals(Integer.valueOf(speed), nonUrbanSpeeds.pollFirst(), "Unexpected speed category for " + speed);
    }

    final TreeSet<Integer> urbanSpeeds = categories.getMaximumSpeedCategories(RoadType.URBAN_ROAD, RoadSpeedType.URBAN_TRAFFIC_NORMAL);

    assertNotNull(urbanSpeeds, "No speed categories found.");
    assertEquals(1, urbanSpeeds.size(), "Unexpected number of speed categories.");

    for (final int speed : new int[] {0}) {
      assertEquals(Integer.valueOf(speed), urbanSpeeds.pollFirst(), "Unexpected speed category for " + speed);
    }
  }

  /**
   * Tests input that is below the lowest speed category.
   */
  @Test
  void testBelowCategory() {
    assertEquals(LT_50_NOT_STRICT, categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 30, null),
        "Lower strict should return strict lowest category");
    assertEquals(LT_50_NOT_STRICT, categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 30, null),
        "Lower not strict should return not strict lowest category");
  }

  /**
   * Tests input that matches speed category.
   */
  @Test
  void testMatchCategory() {
    assertEquals(LT_100_NOT_STRICT, categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 100, null),
        "Match exact speed not strict");
    assertEquals(LT_80_STRICT, categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 80, null),
        "Match exact speed strict");
  }

  @Test
  void testNoExactSpeedCategory() {
    assertEquals(LT_100_NOT_STRICT, categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 90, null),
        "No exact match not strict");
    assertEquals(LT_120_STRICT, categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 110, null),
        "No exact match strict");
    assertEquals(LT_130_NOT_STRICT, categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 130, null),
        "No exact match not strict higher");
  }

  @Test
  void testNoExactButLowerNonStictSpeedCategory() {
    assertEquals(LT_100_NOT_STRICT, categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 90, null),
        "No exact match not strict");
  }

  @Test
  void testSpeedAbove() {
    assertEquals(LT_130_NOT_STRICT, categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 160, null),
        "Find not strict but higher while looking for strict");
    assertEquals(LT_130_NOT_STRICT, categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 160, null),
        "Find not strict but higher while looking for strict");
    categories.add(LT_130_STRICT);
    assertEquals(LT_130_STRICT, categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 160, null),
        "Find strict when speed above lowest category below");
  }

  @Test
  void testFindClosestCategoryWithOne() {
    assertEquals(URBAN_NOT_STRICT_C,
        categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, true, 30, RoadSpeedType.URBAN_TRAFFIC_NORMAL),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT_C,
        categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 30, RoadSpeedType.URBAN_TRAFFIC_NORMAL),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT_C,
        categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 90, RoadSpeedType.URBAN_TRAFFIC_NORMAL),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT_C,
        categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, true, 120, RoadSpeedType.URBAN_TRAFFIC_NORMAL),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT_C,
        categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, true, 135, RoadSpeedType.URBAN_TRAFFIC_NORMAL),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT_C,
        categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, true, 160, RoadSpeedType.URBAN_TRAFFIC_NORMAL),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT_E,
        categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 160, RoadSpeedType.URBAN_TRAFFIC_FREE_FLOW),
        ONE_CATEGORY_ALWAYS);
    assertEquals(URBAN_NOT_STRICT_E, categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 160, null),
        ONE_CATEGORY_ALWAYS);
  }

  @Test
  void testFindNothing() {
    assertNull(categories.findClosestCategory(RoadType.NON_URBAN_ROAD,
        VehicleType.LIGHT_TRAFFIC, true, 30, RoadSpeedType.URBAN_TRAFFIC_FREE_FLOW), "Non urban road should not return a category");
  }

  @Test
  void testStrictInOnlyNonStrict() {
    assertEquals(AB_100_NOT_STRICT, categories.findClosestCategory(RoadType.FREEWAY, VehicleType.AUTO_BUS, true, 90, null),
        "In only non strict return closest if given is strict");
  }

  @Test
  void testGetCategoryNonUrban() {
    assertEquals(NON_URBAN_ROAD_LT, categories.findClosestCategory(RoadType.NON_URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 0, null),
        "Non urban without maximum speed should be retrievable");
    assertEquals(NATIONAL_ROAD_LT_80, categories.findClosestCategory(RoadType.NON_URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 20, null),
        "Non urban without maximum speed should be retrievable");
    assertEquals(NATIONAL_ROAD_LT_100, categories.findClosestCategory(RoadType.NON_URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 120, null),
        "Non urban without maximum speed should be retrievable");
  }

  private RoadEmissionCategories createData() {
    final RoadEmissionCategories categories = new RoadEmissionCategories();

    categories.add(URBAN_NOT_STRICT_C);
    categories.add(URBAN_NOT_STRICT_E);
    categories.add(new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.AUTO_BUS, false, 80, null));
    categories.add(AB_100_NOT_STRICT);
    categories.add(LT_50_NOT_STRICT);
    categories.add(LT_80_STRICT);
    categories.add(LT_80_NOT_STRICT);
    categories.add(LT_100_NOT_STRICT);
    categories.add(LT_120_NOT_STRICT);
    categories.add(LT_120_STRICT);
    categories.add(LT_130_NOT_STRICT);
    categories.add(NON_URBAN_ROAD_LT);
    categories.add(NATIONAL_ROAD_LT_80);
    categories.add(NATIONAL_ROAD_LT_100);
    return categories;
  }
}
