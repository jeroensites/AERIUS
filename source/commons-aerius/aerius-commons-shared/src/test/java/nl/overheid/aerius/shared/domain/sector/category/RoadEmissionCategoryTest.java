/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;

/**
 * Test class for {@link RoadEmissionCategory}.
 */
class RoadEmissionCategoryTest {

  @Test
  void testEquals() {
    final RoadEmissionCategory rec = new RoadEmissionCategory();
    assertNotEquals(rec, null, "Equals on null should be false");
    assertNotEquals(rec, new Object(), "Equals on other object should be false");
    assertEquals(rec, new RoadEmissionCategory(), "Equals on same object should be true");
  }

  @Test
  void testEqualsType() {
    final RoadEmissionCategory rec = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.AUTO_BUS, true, 100, null);
    assertFalse(rec.equalsType(null), "EqualsType on null should be false");
    assertFalse(rec.equalsType(new RoadEmissionCategory(RoadType.URBAN_ROAD, VehicleType.HEAVY_FREIGHT, true, 100, null)),
        "EqualsType: false on dfferent road and vehicle type");
    assertFalse(rec.equalsType(new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.HEAVY_FREIGHT, true, 100, null)),
        "EqualsType: false on dfferent vehicle type");
    assertFalse(rec.equalsType(new RoadEmissionCategory(RoadType.URBAN_ROAD, VehicleType.AUTO_BUS, true, 100, null)),
        "EqualsType: false on different road type");
    assertTrue(rec.equalsType(new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.AUTO_BUS, false, 80, null)),
        "EqualsType: true on same road and vehicle, but different oter");
  }

  @Test
  void testCompareToSame() {
    final RoadEmissionCategory base = new TestCategoryBuilder().build();
    assertEquals(base.compareTo(base), 0, "Should compare to itself");
  }

  @Test
  void testCompareToDifferentRoadType() {
    final RoadEmissionCategory base = new TestCategoryBuilder().build();
    final RoadEmissionCategory test = new TestCategoryBuilder().roadType(RoadType.NON_URBAN_ROAD).build();
    assertNotEquals(base.compareTo(test), 0, "Should not be same on different road type");
  }

  @Test
  void testCompareToDifferentVehicleType() {
    final RoadEmissionCategory base = new TestCategoryBuilder().build();
    final RoadEmissionCategory test = new TestCategoryBuilder().vehicleType(VehicleType.AUTO_BUS).build();
    assertNotEquals(base.compareTo(test), 0, "Should not be same on different vehicle type");
  }

  @Test
  void testCompareToDifferentStrict() {
    final RoadEmissionCategory base = new TestCategoryBuilder().build();
    final RoadEmissionCategory test = new TestCategoryBuilder().strictEnforcement(false).build();
    assertTrue(base.compareTo(test) < 0, "Strict should be lower then non strict");
  }

  @Test
  void testCompareToStrictButLowerSpeed() {
    final RoadEmissionCategory base = new TestCategoryBuilder().strictEnforcement(false).build();
    final RoadEmissionCategory test = new TestCategoryBuilder().strictEnforcement(true).maximumSpeed(80).build();
    assertTrue(base.compareTo(test) > 0, "Strict should be lower then non strict");
  }

  @Test
  void testCompareToLowerSpeed() {
    final RoadEmissionCategory base = new TestCategoryBuilder().build();
    final RoadEmissionCategory test = new TestCategoryBuilder().maximumSpeed(80).build();
    assertTrue(base.compareTo(test) > 0, "Should return > 0 on lower speed");
  }

  @Test
  void testCompareToDifferentSpeedType() {
    final RoadEmissionCategory base = new TestCategoryBuilder().speedType(RoadSpeedType.URBAN_TRAFFIC_FREE_FLOW).build();
    final RoadEmissionCategory test = new TestCategoryBuilder().speedType(RoadSpeedType.URBAN_TRAFFIC_NORMAL).build();
    assertEquals(0, base.compareTo(test), "Should return 0 for freeway, but with different speed type");
  }

  @Test
  void testCompareToDifferentSpeedTypeNonFreeway() {
    final RoadEmissionCategory base = new TestCategoryBuilder()
        .roadType(RoadType.URBAN_ROAD)
        .build();
    final RoadEmissionCategory test = new TestCategoryBuilder()
        .roadType(RoadType.URBAN_ROAD)
        .speedType(RoadSpeedType.URBAN_TRAFFIC_NORMAL)
        .build();
    assertTrue(base.compareTo(test) > 0, "Should compare differently to non-standard (ordering doesn't really matter)");
  }

  @Test
  void testCompareToHigerSpeed() {
    final RoadEmissionCategory base = new TestCategoryBuilder().build();
    final RoadEmissionCategory test = new TestCategoryBuilder().maximumSpeed(120).build();
    assertTrue(base.compareTo(test) < 0, "Should return < 0 on higher speed");
  }

  @Test
  void testSpeedTypeUrban() {
    final TestCategoryBuilder builder = new TestCategoryBuilder().roadType(RoadType.URBAN_ROAD);
    final RoadEmissionCategory testNull = builder.speedType(null).build();
    assertEquals(RoadSpeedType.URBAN_TRAFFIC_FREE_FLOW, testNull.getSpeedType(), "Something should always be assigned for urban roads");

    for (final RoadSpeedType speedType : RoadSpeedType.values()) {
      final RoadEmissionCategory testNotNull = builder.speedType(speedType).build();
      assertEquals(speedType, testNotNull.getSpeedType(), "When something is supplied in constructor, it should always be used");
    }
  }

  @Test
  void testSpeedTypeNonUrban() {
    final TestCategoryBuilder builder = new TestCategoryBuilder().roadType(RoadType.NON_URBAN_ROAD);
    final RoadEmissionCategory testNullWithZeroSpeed = builder.maximumSpeed(0).speedType(null).build();
    assertEquals(RoadSpeedType.NON_URBAN_TRAFFIC, testNullWithZeroSpeed.getSpeedType(),
        "Should be assigned non urban traffic when there is no maximum speed");
    final RoadEmissionCategory testNullWithNonZeroSpeed = builder.maximumSpeed(50).speedType(null).build();
    assertEquals(RoadSpeedType.NATIONAL_ROAD, testNullWithNonZeroSpeed.getSpeedType(),
        "Should be assigned national road when there is maximum speed");

    for (final RoadSpeedType speedType : RoadSpeedType.values()) {
      final RoadEmissionCategory testNotNull = builder.speedType(speedType).build();
      assertEquals(speedType, testNotNull.getSpeedType(), "When something is supplied in constructor, it should always be used");
    }
  }

  @Test
  void testSpeedTypeFreeway() {
    final TestCategoryBuilder builder = new TestCategoryBuilder().roadType(RoadType.FREEWAY);
    final RoadEmissionCategory testNull = builder.speedType(null).build();
    assertNull(testNull.getSpeedType(), "Speedtype should always be null for freeways");

    for (final RoadSpeedType speedType : RoadSpeedType.values()) {
      final RoadEmissionCategory testNotNull = builder.speedType(speedType).build();
      assertNull(testNotNull.getSpeedType(), "Speedtype should always be null for freeways");
    }
  }

  private static class TestCategoryBuilder {
    RoadType roadType = RoadType.FREEWAY;
    VehicleType vehicleType = VehicleType.NORMAL_FREIGHT;
    boolean strictEnforcement = true;
    int maximumSpeed = 100;
    RoadSpeedType speedType = null;

    TestCategoryBuilder roadType(final RoadType roadType) {
      this.roadType = roadType;
      return this;
    }

    TestCategoryBuilder vehicleType(final VehicleType vehicleType) {
      this.vehicleType = vehicleType;
      return this;
    }

    TestCategoryBuilder strictEnforcement(final boolean strictEnforcement) {
      this.strictEnforcement = strictEnforcement;
      return this;
    }

    TestCategoryBuilder maximumSpeed(final int maximumSpeed) {
      this.maximumSpeed = maximumSpeed;
      return this;
    }

    TestCategoryBuilder speedType(final RoadSpeedType speedType) {
      this.speedType = speedType;
      return this;
    }

    RoadEmissionCategory build() {
      return new RoadEmissionCategory(roadType, vehicleType, strictEnforcement, maximumSpeed, speedType);
    }

  }

}
