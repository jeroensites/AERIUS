/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.paa;

import java.util.ArrayList;
import java.util.List;

import com.lowagie.text.pdf.PdfReader;

import nl.overheid.aerius.PAAConstants;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;

/**
 * Util class to read multi situation PAA import data and convert them to internal data structure objects.
 */
final class MultiSituationPAAImport {

  private MultiSituationPAAImport() {
  }

  protected static List<String> readGMLStrings(final PdfReader reader) {
    final List<String> gmlStrings = new ArrayList<>();

    int i = 0;
    String key = String.format(PAAConstants.CALC_PDF_METADATA_GML_KEY_FORMAT, i);
    if (reader.getInfo().containsKey(key)) {
      while (reader.getInfo().containsKey(key)) {
        gmlStrings.add((String) reader.getInfo().get(key));
        i += 1;
        key = String.format(PAAConstants.CALC_PDF_METADATA_GML_KEY_FORMAT, i);
      }
    }

    return gmlStrings;
  }

  protected static ScenarioGMLs toScenarioGMLS(final List<String> gmls) {
    return new ScenarioGMLs(gmls);
  }

  protected static boolean isMultiSituationPAAImport(final PdfReader reader) {
    return reader.getInfo().containsKey(String.format(PAAConstants.CALC_PDF_METADATA_GML_KEY_FORMAT, 0));
  }
}
