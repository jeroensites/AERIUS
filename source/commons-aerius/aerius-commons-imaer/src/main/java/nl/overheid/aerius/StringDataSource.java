/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;

/**
 * Data object to store a large String with a filename and mime type.
 */
public class StringDataSource implements Serializable {

  private static final long serialVersionUID = -6730559419519087483L;

  private String data;
  private String fileName;
  private String mimeType;

  /**
   * @param data The data for this datasource.
   * @param fileName The name the file should have.
   * @param mimeType The mimetype of the file.
   * @throws UnsupportedEncodingException
   */
  public StringDataSource(final String data, final String fileName, final String mimeType) throws UnsupportedEncodingException {
    this.data = data;
    this.fileName = fileName;
    this.mimeType = mimeType;
  }

  public String getData() {
    return data;
  }

  public String getFileName() {
    return fileName;
  }

  public String getMimeType() {
    return mimeType;
  }

  public void setData(final String data) throws IOException {
    this.data = data;
  }

  public void setFileName(final String fileName) {
    this.fileName = fileName;
  }

  public void setMimeType(final String mimeType) {
    this.mimeType = mimeType;
  }
}
