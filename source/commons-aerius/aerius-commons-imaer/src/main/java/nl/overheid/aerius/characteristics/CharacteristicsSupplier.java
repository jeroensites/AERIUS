/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.characteristics;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.InlandCategoryKey;
import nl.overheid.aerius.db.common.sector.category.ShippingCategoryRepository;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.source.plan.Plan;
import nl.overheid.aerius.shared.domain.v2.source.shipping.base.IsStandardShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.StandardInlandShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.StandardMooringInlandShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.ShippingMovementType;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.StandardMooringMaritimeShipping;
import nl.overheid.aerius.shared.emissions.shipping.ShippingLaden;

public class CharacteristicsSupplier {

  private final Map<InlandRouteKey, OPSSourceCharacteristics> cacheInlandRouteCharacteristics = new HashMap<>();
  private final Map<String, Map<ShippingLaden, OPSSourceCharacteristics>> cacheInlandDockedCharacteristics = new HashMap<>();
  private final Map<MaritimeRouteKey, OPSSourceCharacteristics> cacheMaritimeRouteCharacteristics = new HashMap<>();
  private final Map<String, OPSSourceCharacteristics> cacheMaritimeDockedCharacteristics = new HashMap<>();

  private final SectorCategories categories;
  private final PMF pmf;
  private final int year;

  public CharacteristicsSupplier(final SectorCategories categories, final PMF pmf, final int year) {
    this.categories = categories;
    this.pmf = pmf;
    this.year = year;
  }

  public OPSSourceCharacteristics getSectorCharacteristics(final int sectorId) {
    return categories.determineSectorById(sectorId).getDefaultCharacteristics();
  }

  public OPSSourceCharacteristics getPlanCharacteristics(final Plan plan) {
    return plan(plan.getPlanCode()).getCharacteristics();
  }

  public Map<ShippingLaden, OPSSourceCharacteristics> getInlandDockedCharacteristics(final StandardMooringInlandShipping mooringInlandShipping)
      throws SQLException {
    final String shipCode = mooringInlandShipping.getShipCode();
    if (!cacheInlandDockedCharacteristics.containsKey(shipCode)) {
      cacheInlandDockedCharacteristics.put(shipCode, getInlandDockedCharacteristicsFromDB(shipCode));
    }
    return cacheInlandDockedCharacteristics.get(shipCode);
  }

  private Map<ShippingLaden, OPSSourceCharacteristics> getInlandDockedCharacteristicsFromDB(final String shipCode)
      throws SQLException {
    final InlandShippingCategory category = inlandShipCategory(shipCode);
    try (final Connection con = pmf.getConnection()) {
      return ShippingCategoryRepository.getInlandCategoryMooringCharacteristics(con, category);
    }
  }

  public OPSSourceCharacteristics getInlandRouteCharacteristics(final StandardInlandShipping inlandShipping, final InlandWaterway waterway,
      final ShippingLaden laden) throws SQLException {
    final InlandRouteKey routeKey = new InlandRouteKey(waterway.getWaterwayCode(), waterway.getDirection(), laden, inlandShipping.getShipCode());
    if (!cacheInlandRouteCharacteristics.containsKey(routeKey)) {
      cacheInlandRouteCharacteristics.put(routeKey, getInlandRouteCharacteristicsFromDB(routeKey));
    }
    return cacheInlandRouteCharacteristics.get(routeKey);
  }

  private OPSSourceCharacteristics getInlandRouteCharacteristicsFromDB(final InlandRouteKey routeKey) throws SQLException {
    final InlandShippingCategory shipCategory = inlandShipCategory(routeKey.shipCode);
    final InlandWaterwayCategory waterwayCategory = inlandWaterwayCategory(routeKey.waterwayCode);
    try (final Connection con = pmf.getConnection()) {
      final Map<InlandCategoryKey, OPSSourceCharacteristics> characteristics = ShippingCategoryRepository.getInlandCategoryRouteCharacteristics(con,
          shipCategory);
      return characteristics.get(new InlandCategoryKey(routeKey.direction, routeKey.laden, waterwayCategory.getId()));
    }
  }

  public OPSSourceCharacteristics getMaritimeDockedCharacteristics(final StandardMooringMaritimeShipping mooringMaritimeShipping) throws SQLException {
    final String shipCode = mooringMaritimeShipping.getShipCode();
    if (!cacheMaritimeDockedCharacteristics.containsKey(shipCode)) {
      cacheMaritimeDockedCharacteristics.put(shipCode, getMaritimeDockedCharacteristicsFromDB(shipCode));
    }
    return cacheMaritimeDockedCharacteristics.get(shipCode);
  }

  private OPSSourceCharacteristics getMaritimeDockedCharacteristicsFromDB(final String shipCode) throws SQLException {
    final MaritimeShippingCategory maritimeShip = maritimeShipCategory(shipCode);
    try (final Connection con = pmf.getConnection()) {
      return ShippingCategoryRepository.getDockedCharacteristics(con, maritimeShip, year);
    }
  }

  public OPSSourceCharacteristics getMaritimeRouteCharacteristics(final IsStandardShipping maritimeShipping, final ShippingMovementType movementType)
      throws SQLException {
    final MaritimeRouteKey routeKey = new MaritimeRouteKey(movementType, maritimeShipping.getShipCode());
    if (!cacheMaritimeRouteCharacteristics.containsKey(routeKey)) {
      cacheMaritimeRouteCharacteristics.put(routeKey, getMaritimeRouteCharacteristicsFromDB(routeKey));
    }
    return cacheMaritimeRouteCharacteristics.get(routeKey);
  }

  private OPSSourceCharacteristics getMaritimeRouteCharacteristicsFromDB(final MaritimeRouteKey routeKey)
      throws SQLException {
    final MaritimeShippingCategory maritimeShip = maritimeShipCategory(routeKey.shipCode);
    try (final Connection con = pmf.getConnection()) {
      return ShippingCategoryRepository.getCharacteristics(con, maritimeShip, routeKey.movementType, year);
    }
  }

  private PlanCategory plan(final String planCode) {
    return categories.determinePlanEmissionCategoryByCode(planCode);
  }

  private InlandShippingCategory inlandShipCategory(final String shipCode) {
    return categories.getInlandShippingCategories().getShipCategoryByCode(shipCode);
  }

  private InlandWaterwayCategory inlandWaterwayCategory(final String waterwayCode) {
    return categories.getInlandShippingCategories().getWaterwayCategoryByCode(waterwayCode);
  }

  private MaritimeShippingCategory maritimeShipCategory(final String shipCode) {
    return categories.determineMaritimeShippingCategoryByCode(shipCode);
  }

  private static class InlandRouteKey {
    private final String waterwayCode;
    private final WaterwayDirection direction;
    private final ShippingLaden laden;
    private final String shipCode;

    private InlandRouteKey(final String waterwayCode, final WaterwayDirection direction, final ShippingLaden laden, final String shipCode) {
      this.waterwayCode = waterwayCode;
      this.direction = direction;
      this.laden = laden;
      this.shipCode = shipCode;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((direction == null) ? 0 : direction.hashCode());
      result = prime * result + ((laden == null) ? 0 : laden.hashCode());
      result = prime * result + ((shipCode == null) ? 0 : shipCode.hashCode());
      result = prime * result + ((waterwayCode == null) ? 0 : waterwayCode.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final InlandRouteKey other = (InlandRouteKey) obj;
      return direction == other.direction
          && laden == other.laden
          && ((shipCode == null && other.shipCode == null) || (shipCode != null && shipCode.equals(other.shipCode)))
          && ((waterwayCode == null && other.waterwayCode == null) || (waterwayCode != null && waterwayCode.equals(other.waterwayCode)));
    }
  }

  private static class MaritimeRouteKey {
    private final ShippingMovementType movementType;
    private final String shipCode;

    private MaritimeRouteKey(final ShippingMovementType movementType, final String shipCode) {
      this.movementType = movementType;
      this.shipCode = shipCode;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((movementType == null) ? 0 : movementType.hashCode());
      result = prime * result + ((shipCode == null) ? 0 : shipCode.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final MaritimeRouteKey other = (MaritimeRouteKey) obj;
      return movementType == other.movementType
          && ((shipCode == null && other.shipCode == null) || (shipCode != null && shipCode.equals(other.shipCode)));
    }

  }

}
