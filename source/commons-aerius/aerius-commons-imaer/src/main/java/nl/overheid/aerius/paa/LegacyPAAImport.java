/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa;

import java.util.ArrayList;
import java.util.List;

import com.lowagie.text.pdf.PdfReader;

import nl.overheid.aerius.PAAConstants;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Util class to read Legacy PAA import data and convert them to internal data structure objects.
 */
final class LegacyPAAImport {

  private LegacyPAAImport() {
    // util class
  }

  protected static List<String> readGMLStrings(final PdfReader reader) throws AeriusException {
    final List<String> gmlStrings = new ArrayList<>();

    //first situation should always be there.
    if (reader.getInfo().containsKey(PAAConstants.CALC_PDF_METADATA_GML_KEY)) {
      gmlStrings.add((String) reader.getInfo().get(PAAConstants.CALC_PDF_METADATA_GML_KEY));

      //second situation might not be
      if (reader.getInfo().containsKey(PAAConstants.CALC_PDF_METADATA_2ND_GML_KEY)) {
        gmlStrings.add((String) reader.getInfo().get(PAAConstants.CALC_PDF_METADATA_2ND_GML_KEY));
      }
    } else {
      throw new AeriusException(AeriusExceptionReason.PAA_VALIDATION_FAILED);
    }
    return gmlStrings;
  }

  protected static ScenarioGMLs toScenarioGMLs(final List<String> gmlStrings) {
    final ScenarioGMLs scenarioGMLs = new ScenarioGMLs();
    if (gmlStrings.size() > 1) {
      //proposed situation is the last GML in the list, current is the first.
      scenarioGMLs.setReferenceGML(gmlStrings.get(0));
      scenarioGMLs.setProposedGML(gmlStrings.get(gmlStrings.size() - 1));
    } else if (gmlStrings.size() == 1) {
      scenarioGMLs.setProposedGML(gmlStrings.get(0));
    }
    return scenarioGMLs;
  }

  protected static boolean isLegacyPAAImport(final PdfReader reader) {
    return reader.getInfo().containsKey(PAAConstants.CALC_PDF_METADATA_GML_KEY);
  }
}
