/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.paa;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.pdf.PdfReader;

import nl.overheid.aerius.PAAConstants;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.HashUtil;

/**
 * Util class to read a PAA import data from a stream and convert them to internal data structure objects.
 */
public class PAAImport {
  private static final Logger LOG = LoggerFactory.getLogger(PAAImport.class);

  private PAAImport() {
  }

  public static ScenarioGMLs importPAAFromStream(final InputStream inputStream) throws AeriusException {
    final List<String> gmlStrings;
    final String metadataHash;
    final ScenarioGMLs scenarioGMLs;

    try {
      // Open pdf
      final PdfReader reader = new PdfReader(inputStream);

      if (LegacyPAAImport.isLegacyPAAImport(reader)) {
        gmlStrings = LegacyPAAImport.readGMLStrings(reader);
        scenarioGMLs = LegacyPAAImport.toScenarioGMLs(gmlStrings);
      } else if (MultiSituationPAAImport.isMultiSituationPAAImport(reader)) {
        gmlStrings = MultiSituationPAAImport.readGMLStrings(reader);
        scenarioGMLs = MultiSituationPAAImport.toScenarioGMLS(gmlStrings);
      } else {
        throw new AeriusException(AeriusExceptionReason.PAA_VALIDATION_FAILED, "No import data found in pdf");
      }

      // Get metadata hash
      metadataHash = readMetadataHash(reader);
    } catch (final IOException e) {
      LOG.error("IOException while importing PAA", e);
      throw new AeriusException(AeriusExceptionReason.IMPORT_FILE_COULD_NOT_BE_READ);
    }

    // Validate calculation
    validate(gmlStrings, metadataHash);

    return scenarioGMLs;
  }

  private static String readMetadataHash(final PdfReader reader) throws AeriusException {
    // Try to get validation key
    if (!reader.getInfo().containsKey(PAAConstants.CALC_PDF_METADATA_HASH_KEY)) {
      throw new AeriusException(AeriusExceptionReason.PAA_VALIDATION_FAILED);
    }
    return (String) reader.getInfo().get(PAAConstants.CALC_PDF_METADATA_HASH_KEY);
  }

  private static void validate(final List<String> gmlStrings, final String metadataHash) throws AeriusException {
    final String hash = HashUtil.generateSaltedHash(PAAConstants.CALC_PDF_METADATA_HASH_SALT, gmlStrings);
    if (!hash.equals(metadataHash)) {
      throw new AeriusException(AeriusExceptionReason.PAA_VALIDATION_FAILED);
    }
  }
}
