/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.importer.ImportOption;
import nl.overheid.aerius.importer.ImporterFacade;
import nl.overheid.aerius.importer.PermitReferenceGenerator;
import nl.overheid.aerius.io.ImportableFile;
import nl.overheid.aerius.schematron.EmissionSourceSchematronVisitor;
import nl.overheid.aerius.schematron.ImaerSchematronValidator;
import nl.overheid.aerius.schematron.SchematronValidators;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLCorrection;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLDispersionLineFeature;
import nl.overheid.aerius.shared.domain.v2.nsl.NSLMeasureFeature;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.LocaleUtils;

public class ImaerController {

  private static final Logger LOG = LoggerFactory.getLogger(ImaerController.class);

  private final PMF pmf;
  private final GMLWriter gmlWriter;
  private final SchematronValidators validators;
  private final Map<Locale, SectorCategories> sectorCategoriesMap;

  public ImaerController(final PMF pmf) throws AeriusException, IOException {
    this.pmf = pmf;
    sectorCategoriesMap = LocaleUtils.getLocales().stream()
        .collect(Collectors.toMap(Function.identity(), locale -> {
          try {
            return SectorRepository.getSectorCategories(pmf, locale);
          } catch (final SQLException e) {
            throw new RuntimeException(e);
          }
        }));
    validators = new SchematronValidators();
    try {
      gmlWriter = new GMLWriter(ReceptorGridSettingsRepository.getReceptorGridSettings(pmf), new PermitReferenceGenerator());
    } catch (final SQLException e) {
      LOG.error("Failed creating GMLWriter", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public List<ImportParcel> processFile(final ImportableFile inputFile, final Set<ImportOption> options,
      final ImportProperties importProperties, final Locale locale) throws AeriusException {
    try {
      final List<ImportParcel> importParcels = importParcel(inputFile, importProperties, options, locale);

      for (final ImportParcel importParcel : importParcels) {
        processEmissionSources(options, importParcel);
        processCalculationPoints(options, importParcel);
        processDispersionLines(options, importParcel);
        processMeasures(options, importParcel);
        processCorrections(options, importParcel);
      }

      return importParcels;
    } catch (final IOException e) {
      LOG.error("Error during processing file", e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  private void processEmissionSources(final Set<ImportOption> options, final ImportParcel importParcel) throws AeriusException {
    // TODO: do we want to keep schematron validation?
    // It seems like much overhead (first generating GML snippets before schematron validation) for little gain
    // Benefit of it is that in theory you could bundle the schematron template files for external validation
    // If we do want to keep it, the GML for each object should be referenced outside of the domain objects.
    //
    if (ImportOption.VALIDATE_WITH_SCHEMATRON.in(options)) {
      final Map<EmissionSourceFeature, String> gmlPerSource = gmlWriter.getGmlByEmissionSource(importParcel.getSituation().getEmissionSourcesList());
      validateWithSchematron(gmlPerSource, importParcel);
    }
    if (ImportOption.PERSIST_INPUT.in(options)) {
      store(importParcel.getSituation().getEmissionSourcesList());
    }
  }

  private void processCalculationPoints(final Set<ImportOption> options, final ImportParcel importParcel) throws AeriusException {
    if (ImportOption.VALIDATE_WITH_SCHEMATRON.in(options)) {
      final List<CalculationPointFeature> points = importParcel.getCalculationPointsList();
      final Map<CalculationPointFeature, String> gmlPerCalculationPoint = gmlWriter.getGMLByCalculationPoint(points);
      validateWithSchematron(validators.getNslCalculationPointValidator(), points, gmlPerCalculationPoint::get, importParcel);
    }
  }

  private void processDispersionLines(final Set<ImportOption> options, final ImportParcel importParcel) throws AeriusException {
    if (ImportOption.VALIDATE_WITH_SCHEMATRON.in(options)) {
      final List<NSLDispersionLineFeature> dispersionLines = importParcel.getSituation().getNslDispersionLinesList();
      final Map<NSLDispersionLineFeature, String> gmlByDispersionLine = gmlWriter.getGmlByNslDispersionLines(dispersionLines);
      validateWithSchematron(validators.getDispersionLineValidator(), dispersionLines, gmlByDispersionLine::get, importParcel);
    }
  }

  private void processMeasures(final Set<ImportOption> options, final ImportParcel importParcel) throws AeriusException {
    if (options.contains(ImportOption.VALIDATE_WITH_SCHEMATRON)) {
      final List<NSLMeasureFeature> measures = importParcel.getSituation().getNslMeasuresList();
      final Map<NSLMeasureFeature, String> gmlByMeasure = gmlWriter.getGmlByNslMeasures(measures);
      validateWithSchematron(validators.getMeasureAreaValidator(), measures, gmlByMeasure::get, importParcel);
    }
  }

  private void processCorrections(final Set<ImportOption> options, final ImportParcel importParcel) throws AeriusException {
    if (ImportOption.VALIDATE_WITH_SCHEMATRON.in(options)) {
      final List<NSLCorrection> corrections = importParcel.getSituation().getNslCorrections();
      final Map<NSLCorrection, String> gmlByCorrection = gmlWriter.getGmlByNslCorrections(corrections);
      validateWithSchematron(validators.getCalculationPointCorrectionValidator(), corrections, gmlByCorrection::get, importParcel);
    }
  }

  private List<ImportParcel> importParcel(final ImportableFile inputFile, final ImportProperties importProperties,
      final Set<ImportOption> options, final Locale locale) throws IOException, AeriusException {
    final ImporterFacade facade = new ImporterFacade(pmf, sectorCategoriesMap.get(locale));

    facade.setImportOptions(options);
    try (InputStream is = inputFile.asInputStream()) {
      return facade.convertInputStream2ImportResult(inputFile.getFileName(), is, importProperties);
    }
  }

  private void validateWithSchematron(final Map<EmissionSourceFeature, String> gmlPerSource, final ImportParcel importParcel) throws AeriusException {
    for (final Entry<EmissionSourceFeature, String> entry : gmlPerSource.entrySet()) {
      final EmissionSourceSchematronVisitor schematronVisitor = new EmissionSourceSchematronVisitor(entry.getValue(), validators,
          importParcel.getExceptions(), importParcel.getWarnings());
      entry.getKey().accept(schematronVisitor);
    }
  }

  private <T> void validateWithSchematron(final ImaerSchematronValidator validator, final List<T> objects, final Function<T, String> gmlFunction,
      final ImportParcel importParcel) throws AeriusException {
    for (final T object : objects) {
      validator.validate(() -> gmlFunction.apply(object), importParcel.getExceptions(), importParcel.getWarnings());
    }
  }

  private void store(final List<EmissionSourceFeature> esl) {
    // TODO Auto-generated method stub
  }
}
