/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.sector.ShippingRepository;
import nl.overheid.aerius.emissions.CategoryBasedEmissionFactorSupplier;
import nl.overheid.aerius.gml.base.AeriusGMLVersion;
import nl.overheid.aerius.gml.base.GMLHelper;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.Conversion;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.GMLLegacyCodeType;
import nl.overheid.aerius.gml.base.conversion.MobileSourceOffRoadConversion;
import nl.overheid.aerius.gml.base.conversion.PlanConversion;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.HeatContentType;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.shared.emissions.EmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.EmissionsUpdater;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.GeometryCalculator;
import nl.overheid.aerius.util.GeometryCalculatorImpl;
import nl.overheid.aerius.validation.CategoryBasedValidationHelper;
import nl.overheid.aerius.validation.ValidationHelper;

/**
 *
 */
public class GMLHelperImpl implements GMLHelper {

  private static final Logger LOG = LoggerFactory.getLogger(GMLHelperImpl.class);

  private final PMF pmf;
  private final SectorCategories categories;
  private final GeometryCalculator geometryCalculator = new GeometryCalculatorImpl();

  public GMLHelperImpl(final PMF pmf, final SectorCategories categories) {
    this.pmf = pmf;
    this.categories = categories;
  }

  @Override
  public void enforceEmissions(final int year, final List<EmissionSourceFeature> emissionSourceList) throws AeriusException {
    final EmissionFactorSupplier supplier = new CategoryBasedEmissionFactorSupplier(categories, pmf, year);
    final EmissionsUpdater updater = new EmissionsUpdater(supplier, geometryCalculator);
    updater.updateEmissions(emissionSourceList);
  }

  @Override
  public List<InlandWaterway> suggestInlandShippingWaterway(final Geometry geometry) throws AeriusException {
    try (Connection con = pmf.getConnection()) {
      return ShippingRepository.suggestInlandShippingWaterway(con, geometry);
    } catch (final SQLException e) {
      LOG.error("SuggestInlandShippingWaterway sql exception:", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  @Override
  public Map<GMLLegacyCodeType, Map<String, Conversion>> getLegacyCodes(final AeriusGMLVersion version) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return GMLConversionRepository.getLegacyCodes(con, version);
    } catch (final SQLException e) {
      LOG.error("Error getting legacy codes for AeriusGMLVersion {}", version, e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  @Override
  public Map<String, MobileSourceOffRoadConversion> getLegacyMobileSourceOffRoadConversions() throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return GMLConversionRepository.getLegacyMobileSourceOffRoadConversions(con);
    } catch (final SQLException e) {
      LOG.error("Error getting legacy mobile source off road conversions", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  @Override
  public Map<String, PlanConversion> getLegacyPlanConversions() throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return GMLConversionRepository.getLegacyPlanConversions(con);
    } catch (final SQLException e) {
      LOG.error("Error getting legacy plan conversions", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  @Override
  public ReceptorGridSettings getReceptorGridSettings() throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return ReceptorGridSettingsRepository.getReceptorGridSettings(con);
    } catch (final SQLException e) {
      LOG.error("Error getting receptor grid settings", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  @Override
  public OPSSourceCharacteristics determineDefaultCharacteristicsBySectorId(final int sectorId) {
    final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
    final Sector sector = categories.determineSectorById(sectorId);
    if (sector != null && sector.getDefaultCharacteristics() != null) {
      final OPSSourceCharacteristics sectorDefaults = sector.getDefaultCharacteristics();
      characteristics.setDiurnalVariation(sectorDefaults.getDiurnalVariation());
      characteristics.setHeatContentType(HeatContentType.NOT_FORCED);
      characteristics.setHeatContent(sectorDefaults.getHeatContent());
      characteristics.setEmissionHeight(sectorDefaults.getEmissionHeight());
      characteristics.setSpread(sectorDefaults.getSpread());
      characteristics.setParticleSizeDistribution(sectorDefaults.getParticleSizeDistribution());
    }
    return characteristics;
  }

  @Override
  public ValidationHelper getValidationHelper() {
    return new CategoryBasedValidationHelper(categories);
  }

}
