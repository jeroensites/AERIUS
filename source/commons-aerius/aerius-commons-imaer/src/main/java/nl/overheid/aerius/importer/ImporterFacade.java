/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.time.Year;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.io.AplImportReader;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.emissions.CategoryBasedEmissionFactorSupplier;
import nl.overheid.aerius.ops.importer.BrnImportReader;
import nl.overheid.aerius.ops.importer.RcpImportReader;
import nl.overheid.aerius.paa.PAAImport;
import nl.overheid.aerius.shared.domain.importer.ImportProperties;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.importer.ImportType;
import nl.overheid.aerius.shared.emissions.EmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.EmissionsUpdater;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.GeometryCalculator;
import nl.overheid.aerius.srm.io.LegacyNSLImportReader;
import nl.overheid.aerius.util.GeometryCalculatorImpl;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.validation.EmissionSourceValidator;
import nl.overheid.aerius.validation.GenericSourcesValidator;

/**
 * Util class to convert an input stream to AERIUS data structures.
 */
public class ImporterFacade {

  private static final Logger LOGGER = LoggerFactory.getLogger(ImporterFacade.class);

  private final GeometryCalculator geometryCalculator = new GeometryCalculatorImpl();
  private final SectorCategories categories;
  private final PMF pmf;

  private final List<ImportParcel> importParcels = new ArrayList<>();
  private final EnumSet<ImportOption> importOptions = ImportOption.getDefaultOptions();
  private ImportParcel importParcel;

  public ImporterFacade(final PMF pmf) throws SQLException {
    this(pmf, LocaleUtils.getDefaultLocale());
  }

  public ImporterFacade(final PMF pmf, final Locale locale) throws SQLException {
    this(pmf, SectorRepository.getSectorCategories(pmf, locale));
  }

  public ImporterFacade(final PMF pmf, final SectorCategories categories) {
    this.pmf = pmf;
    this.categories = categories;
  }

  public boolean isValidateStrict() {
    return ImportOption.VALIDATE_STRICT.in(importOptions);
  }

  /**
   * Sets all import options. Clears any previous set or default options.
   */
  public void setImportOptions(final Set<ImportOption> importOptions) {
    this.importOptions.clear();
    this.importOptions.addAll(importOptions);
  }

  public void setImportOption(final ImportOption option, final boolean set) {
    if (set) {
      importOptions.add(option);
    } else {
      importOptions.remove(option);
    }
  }

  /**
   * Read the data from the input stream and return a ImportParcel containing useable data.
   *
   * @param filename
   *   name of the input stream to import, used to determine type
   * @param inputStream
   *   Read data from this stream
   * @return {@link ImportParcel}
   * @throws AeriusException
   *   incase of an internal AERIUS specific error
   * @throws IOException
   *   on IO error
   */
  public List<ImportParcel> convertInputStream2ImportResult(final String filename, final InputStream inputStream)
      throws IOException, AeriusException {
    return convertInputStream2ImportResult(filename, inputStream, new ImportProperties());
  }

  /**
   * Read the data from the input stream and return a ImportParcel containing useable data.
   *
   * @param filename name of file to import
   * @param inputStream Read data from this stream
   * @param importProperties The import properties to use during import
   * @return {@link ImportParcel}
   * @throws AeriusException in case of an internal AERIUS specific error
   * @throws IOException on IO error
   */
  public List<ImportParcel> convertInputStream2ImportResult(final String filename, final InputStream inputStream,
      final ImportProperties importProperties) throws IOException, AeriusException {
    final ImportType type = checkFileType(filename);
    if (importParcel == null) {
      importParcel = new ImportParcel();
      importParcels.add(importParcel);
    }

    switch (type) {
    case BRN:
      final BrnImportReader brnImportReader = new BrnImportReader(ImportOption.USE_VALID_SECTORS.in(importOptions));
      brnImportReader.read(filename, inputStream, categories, importProperties.getSubstance(), importParcel);
      break;
    case RCP:
      final RcpImportReader rcpImportReader = new RcpImportReader(
          ImportOption.USE_IMPORTED_LANDUSES.in(importOptions),
          ImportOption.USE_IMPORTED_RECEPTOR_HEIGHT.in(importOptions));
      rcpImportReader.read(filename, inputStream, categories, importProperties.getSubstance(), importParcel);
      break;
    case SRM2:
      convertCSV2ImportResult(filename, inputStream, importProperties);
      break;
    case GML:
      convertGML2ImportResult(inputStream, importProperties);
      break;
    case PAA:
      convertPAA2ImportResult(inputStream);
      break;
    case APL:
    case UPL:
      convertAPL2ImportResult(filename, inputStream);
      break;
    case ZIP:
      convertZIP2ImportResult(inputStream, importProperties);
      break;
    default:
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    importParcels.forEach(parcel -> parcel.setType(type));
    importParcels.forEach(parcel -> parcel.setType(type));
    GenericSourcesValidator.validateStrict(importParcel, isValidateStrict());
    return importParcels;
  }

  private static ImportType checkFileType(final String filename) throws AeriusException {
    final ImportType type = ImportType.determineByFilename(filename);
    if (type == null) {
      throw new AeriusException(AeriusExceptionReason.IMPORT_FILE_UNSUPPORTED, filename);
    }
    return type;
  }

  private void convertPAA2ImportResult(final InputStream inputStream) throws IOException, AeriusException {
    // Need context to set default values when needed during conversion.
    // retrieve GML strings from paa.
    final ScenarioGMLs scenarioGMLs = PAAImport.importPAAFromStream(inputStream);
    final boolean originalIncludeCalculationPoints = ImportOption.INCLUDE_CALCULATION_POINTS.in(importOptions);
    importOptions.remove(ImportOption.INCLUDE_CALCULATION_POINTS);
    importOptions.remove(ImportOption.VALIDATE_SOURCES);

    final ImaerImporter imaerImporter = new ImaerImporter(new GMLHelperImpl(pmf, categories));
    if (scenarioGMLs.isLegacy()) {
      convertLegacyPAA2ImportResults(scenarioGMLs, originalIncludeCalculationPoints, imaerImporter);
    } else {
      convertMultipleSituationPAA2ImportResult(scenarioGMLs, originalIncludeCalculationPoints, imaerImporter);
    }
  }

  private void convertMultipleSituationPAA2ImportResult(final ScenarioGMLs scenarioGMLs, final boolean originalIncludeCalculationPoints, final ImaerImporter imaerImporter)
      throws IOException, AeriusException {

    final Iterator<String> gmls = scenarioGMLs.getGmls().iterator();
    while (gmls.hasNext()) {
      final String gml = gmls.next();

      // Only include calculation points from the last GML. It doesn't matter which one really, because they all contain the same set.
      if (originalIncludeCalculationPoints && !gmls.hasNext()) {
        importOptions.add(ImportOption.INCLUDE_CALCULATION_POINTS);
      }

      try (final InputStream gmlInputStream = new ByteArrayInputStream(gml.getBytes(StandardCharsets.UTF_8))) {
        imaerImporter.importStream(gmlInputStream, importOptions, importParcel);
      }

      if (gmls.hasNext()) {
        importParcel = new ImportParcel();
        importParcels.add(importParcel);
      }
    }
  }

  private void convertLegacyPAA2ImportResults(final ScenarioGMLs scenarioGMLs, final boolean originalIncludeCalculationPoints,
      final ImaerImporter imaerImporter) throws IOException, AeriusException {
    // current situation first (if available).
    if (scenarioGMLs.getReferenceGML() != null) {
      try (final InputStream gmlInputStream = new ByteArrayInputStream(scenarioGMLs.getReferenceGML().getBytes(StandardCharsets.UTF_8))) {
        imaerImporter.importStream(gmlInputStream, importOptions, importParcel);
        importParcel.getSituation().setType(SituationType.REFERENCE);
        // Ensure proposed gets imported in different parcel
        importParcel = new ImportParcel();
        importParcels.add(importParcel);
      }
    }
    if (originalIncludeCalculationPoints) {
      importOptions.add(ImportOption.INCLUDE_CALCULATION_POINTS);
    }
    // proposed situation.
    try (final InputStream gmlInputStream = new ByteArrayInputStream(scenarioGMLs.getProposedGML().getBytes(StandardCharsets.UTF_8))) {
      imaerImporter.importStream(gmlInputStream, importOptions, importParcel);
      importParcel.getSituation().setType(SituationType.PROPOSED);
    }
  }

  private void convertCSV2ImportResult(final String filename, final InputStream inputStream, final ImportProperties importProperties)
      throws IOException, AeriusException {
    final LegacyNSLImportReader srm2Importer = new LegacyNSLImportReader();
    srm2Importer.read(filename, inputStream, categories, importProperties.getSubstance(), importParcel);
    EmissionSourceValidator.checkIntersections(importParcel.getSituation(), importParcel.getExceptions());
    // If a year was supplied, use that to enforce emissions. Otherwise no year to base it on for these files, so use current year.
    final int enforceEmissionsYear = importProperties.getYear().orElse(Year.now().getValue());
    enforceEmissions(enforceEmissionsYear);
    // Do not set the year for the situation:
    // if we would do that, Connect can't throw an exception if calculation year hasn't been supplied for a job with just these files.
  }

  private void enforceEmissions(final int importForYear) throws AeriusException {
    final EmissionFactorSupplier supplier = new CategoryBasedEmissionFactorSupplier(categories, pmf, importForYear);
    final EmissionsUpdater updater = new EmissionsUpdater(supplier, geometryCalculator);
    for (final ImportParcel parcel : importParcels) {
      updater.updateEmissions(parcel.getSituation().getEmissionSourcesList());
    }
  }

  private void convertAPL2ImportResult(final String filename, final InputStream inputStream) throws IOException, AeriusException {
    final AplImportReader aplImportReader = new AplImportReader();
    aplImportReader.read(filename, inputStream, categories, null, importParcel);
  }

  private void convertZIP2ImportResult(final InputStream inputStream, final ImportProperties importProperties)
      throws IOException, AeriusException {
    boolean foundFiles = false;
    try (final ZipInputStream zipStream = new ZipInputStream(inputStream)) {
      ZipEntry zipEntry;
      int convertedFileInZip = 0;
      while ((zipEntry = zipStream.getNextEntry()) != null) {
        final String name = zipEntry.getName();
        final ImportType entryMatchType = ImportType.determineByFilename(name);

        if (entryMatchType != null) {
          if (convertedFileInZip > 0) {
            importParcel = new ImportParcel();
            importParcels.add(importParcel);
          }
          convertInputStream2ImportResult(name, IOUtils.toBufferedInputStream(zipStream), importProperties);
          foundFiles = true;
          convertedFileInZip++;
        }
        // else skip this file, not supported.
      }
      if (convertedFileInZip == 2
          && importParcels.get(0).getType() == ImportType.GML
          && importParcels.get(0).getSituation().getType() == null
          && importParcels.get(1).getType() == ImportType.GML
          && importParcels.get(1).getSituation().getType() == null) {
        // Assume an old zip file, where order determined the type of the situation. First one reference, second proposed.
        importParcels.get(0).getSituation().setType(SituationType.REFERENCE);
        importParcels.get(1).getSituation().setType(SituationType.PROPOSED);
      }
    } catch (final EOFException e) {
      LOGGER.debug("Zip looks corrupt", e);
      throw new AeriusException(AeriusExceptionReason.ZIP_WITHOUT_USABLE_FILES);
    }
    if (!foundFiles) {
      throw new AeriusException(AeriusExceptionReason.ZIP_WITHOUT_USABLE_FILES);
    }
  }

  private void convertGML2ImportResult(final InputStream inputStream, final ImportProperties importProperties) throws AeriusException {
    final ImaerImporter imaerImporter = new ImaerImporter(new GMLHelperImpl(pmf, categories));

    // Need context to set default values when needed during conversion.
    imaerImporter.importStream(inputStream, importOptions, importParcel, importProperties.getYear());
  }
}
