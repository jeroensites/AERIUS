/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.validation;

import java.util.OptionalDouble;

import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;

class CategoryOffRoadValidationHelper implements OffRoadValidationHelper {

  private final SectorCategories categories;

  CategoryOffRoadValidationHelper(final SectorCategories categories) {
    this.categories = categories;
  }

  @Override
  public boolean isValidOffRoadMobileSourceCode(final String offRoadMobileSourceCode) {
    return categories.determineOffRoadMobileSourceCategoryByCode(offRoadMobileSourceCode) != null;
  }

  @Override
  public boolean expectsLiterFuelPerYear(final String offRoadMobileSourceCode) {
    return categories.determineOffRoadMobileSourceCategoryByCode(offRoadMobileSourceCode).expectsLiterFuel();
  }

  @Override
  public boolean expectsOperatingHoursPerYear(final String offRoadMobileSourceCode) {
    return categories.determineOffRoadMobileSourceCategoryByCode(offRoadMobileSourceCode).expectsOperatingHours();
  }

  @Override
  public boolean expectsLiterAdBluePerYear(final String offRoadMobileSourceCode) {
    return categories.determineOffRoadMobileSourceCategoryByCode(offRoadMobileSourceCode).expectsLiterAdBlue();
  }

  @Override
  public OptionalDouble getMaxAdBlueFuelRatio(final String offRoadMobileSourceCode) {
    final Double value = categories.determineOffRoadMobileSourceCategoryByCode(offRoadMobileSourceCode).getMaxAdBlueFuelRatio();
    return value == null ? OptionalDouble.empty() : OptionalDouble.of(value);
  }

}
