/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.validation;

import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;

/**
 * SectorCategories based ValidationHelper.
 */
public class CategoryBasedValidationHelper implements ValidationHelper {

  private final SectorCategories categories;

  public CategoryBasedValidationHelper(final SectorCategories categories) {
    this.categories = categories;
  }

  @Override
  public FarmLodgingValidationHelper farmLodgingValidation() {
    return new CategoryFarmLodgingValidationHelper(categories);
  }

  @Override
  public FarmlandValidationHelper farmlandValidation() {
    return new CategoryFarmlandValidationHelper(categories);
  }

  @Override
  public OffRoadValidationHelper offRoadMobileValidation() {
    return new CategoryOffRoadValidationHelper(categories);
  }

  @Override
  public PlanValidationHelper planValidation() {
    return new CategoryPlanValidationHelper(categories);
  }

  @Override
  public RoadValidationHelper roadValidation() {
    return new CategoryRoadValidationHelper(categories);
  }

  @Override
  public InlandShippingValidationHelper inlandShippingValidation() {
    return new CategoryInlandShippingValidationHelper(categories);
  }

  @Override
  public MaritimeShippingValidationHelper maritimeShippingValidation() {
    return new CategoryMaritimeShippingValidationHelper(categories);
  }

}
