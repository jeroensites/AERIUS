/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.gml.base.AeriusGMLVersion;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.Conversion;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.GMLLegacyCodeType;
import nl.overheid.aerius.gml.base.conversion.MobileSourceOffRoadConversion;
import nl.overheid.aerius.gml.base.conversion.PlanConversion;

/**
 * Test class for {@link GMLConversionRepository}.
 */
class GMLConversionRepositoryTest extends BaseDBTest {

  @Test
  void testGetLegacyCodes() throws SQLException {
    try (final Connection connection = getCalcConnection()) {
      //only testing if query works, nothing more.
      final Map<GMLLegacyCodeType, Map<String, Conversion>> conversionMap =
          GMLConversionRepository.getLegacyCodes(connection, AeriusGMLVersion.V0_5);
      assertNotNull(conversionMap, "conversionMap");
    }
  }

  @Test
  void testGetLegacyMobileSourceOffRoadConversions() throws SQLException {
    try (final Connection connection = getCalcConnection()) {
      //only testing if query works, nothing more.
      final Map<String, MobileSourceOffRoadConversion> conversions =
          GMLConversionRepository.getLegacyMobileSourceOffRoadConversions(connection);
      assertNotNull(conversions, "legacy mobile source off road conversions");
    }
  }

  @Test
  void testGetLegacyPlanConversions() throws SQLException {
    try (final Connection connection = getCalcConnection()) {
      //only testing if query works, nothing more.
      final Map<String, PlanConversion> conversions =
          GMLConversionRepository.getLegacyPlanConversions(connection);
      assertNotNull(conversions, "legacy plan conversions");
    }
  }
}
