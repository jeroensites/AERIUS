/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.io.ADMSInputReaderUtil;
import nl.aerius.adms.io.ADMSSourceFileType;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test to perform an ADMS run.
 */
class RunADMSTest extends BaseRunADMSTest {

  @Test
  void testFailureRun() {
    assertThrows(AeriusException.class, () -> {
      final RunADMS runner = factory.createRunADMS(getADMSWorkerConfiguration());
      final int groupKey = 1;
      final ADMSInputData input = createInput(groupKey);

      runner.run(input);
      fail("ADMS run should not run succesfull, but throw an exception.");
    }, "Expected an exception because it should give an error because no receptors were available.");
  }

  @Test
  void testRun() throws IOException, Exception {
    final RunADMS runner = factory.createRunADMS(getADMSWorkerConfiguration());
    final int groupKey = 1;
    final ADMSInputData input = createInput(groupKey);
    setReceptors(input);

    final CalculationResult result = runner.run(input);
    final List<AeriusResultPoint> list = result.getResults().get(groupKey);
    assertEquals(1, list.size(), "Should have 1 result.");
    assertNotNull(list.get(0).getEmissionResults().getHashMap().get(EmissionResultKey.NOX_CONCENTRATION), "Should have result for NOx");
  }

  private ADMSInputData createInput(final int groupKey) throws IOException, URISyntaxException {
    final ADMSInputData input = new ADMSInputData();

    input.setFileType(ADMSSourceFileType.UPL);
    input.setEmissionResultKeys(EnumSet.of(EmissionResultKey.NOX_CONCENTRATION));
    input.setSubstances(List.of(Substance.NOX));
    input.setMetDataFile(new File(getClass().getResource("io/oneday.met").toURI()).getAbsolutePath());
    ADMSInputReaderUtil.read2ADMSInputData("pointsource.upl", groupKey, input);

    return input;
  }

  private void setReceptors(final ADMSInputData input) {
    final Collection<AeriusPoint> receptors = new ArrayList<>();
    final AeriusPoint point = new AeriusPoint(1, AeriusPointType.POINT, 544400.0,  258900.0);
    point.setHeight(1.0);
    receptors.add(point);
    input.setReceptors(receptors);
  }
}
