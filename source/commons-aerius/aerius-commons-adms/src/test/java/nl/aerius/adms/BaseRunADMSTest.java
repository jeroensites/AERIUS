/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.domain.ADMSConfiguration;
import nl.overheid.aerius.worker.PropertiesUtil;

/**
 * Base class for tests that run the ADMS tool.
 */
class BaseRunADMSTest {

  private static final Logger LOG = LoggerFactory.getLogger(BaseRunADMSTest.class);

  protected ADMSConfiguration config;
  protected ADMSWorkerFactory factory;

  @BeforeEach
  public void setUp() throws Exception {
    factory = new ADMSWorkerFactory();
    config = ADMSWorkerFactory.createADMSConfiguration(getADMSWorkerConfiguration());
    Assumptions.assumeTrue(checkADMSAvailability(), "ADMS not installed.");
  }

  protected ADMSWorkerConfiguration getADMSWorkerConfiguration() throws IOException {
    final Properties properties =  PropertiesUtil.getFromTestPropertiesFile("adms");
    return factory.createConfiguration(properties);
  }

  private boolean checkADMSAvailability() {
    final File admsRoot = config.getADMSRoot();
    final boolean available = admsRoot != null && admsRoot.exists();

    if (!available) {
      LOG.warn("Could not find ADMS root directory ({}). {}", admsRoot, Thread.currentThread().getStackTrace()[2]);
    }
    return available;
  }
}
