/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link AplBlockReader}.
 */
class AplBlockReaderReaderTest {

  private static final String BLOCKTEST_TXT = "blocktest.txt";
  private static final AplFileKeys KEY = AplFileKeys.SRC_NAME;
  private static final String KEY_STRING = KEY.getKey();

  @Test
  void testReadBlock() throws IOException {
    try (final InputStream is = getClass().getResourceAsStream(BLOCKTEST_TXT);
        final InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
        final BufferedReader br = new BufferedReader(isr)) {
      final AplBlockReader reader = AplBlockReader.readBlock(br);
      assertEquals(28.966, reader.parseDouble(AplFileKeys.SRC_MOL_WEIGHT), "Incorrectly parsed double.");
      assertEquals(4, reader.parseInt(AplFileKeys.SRC_SOURCE_TYPE), "Incorrectly parsed integer.");
      Assertions.assertLinesMatch(List.of("NOx", "PM10", "PM2.5", "VOC", "NO2"), reader.parseStringList(AplFileKeys.SRC_POLLUTANTS),
          "Multiple values incorrectly parsed, got: " + reader.mapValue(AplFileKeys.SRC_POLLUTANTS));
      Assertions.assertArrayEquals(
          new double[] {0.20833600891961, 0.01863182336092, 0.0108896204166942, 0.0101529397070408,0.0436183239022891 },
          reader.parseDoubleList(AplFileKeys.SRC_POL_EMISSION_RATE).stream().mapToDouble(Double::doubleValue).toArray(),
          "Multiple double incorrectly parsed.");
    }
  }

  @Test
  void testParseDouble() {
    assertEquals(15.0, new AplBlockReader(Map.of(KEY_STRING, "1.5e+1"), null).parseDouble(KEY), "Should parse scientic numbers correctly.");
  }

  @Test
  void testParseDoubleList() {
    Assertions.assertArrayEquals(new double[] {15.0, 15.0},
        new AplBlockReader(Map.of(KEY_STRING, "1.5e+1 1.5e+1 "), null).parseDoubleList(KEY).stream().mapToDouble(Double::doubleValue).toArray(),
        "Should parse scientic numbers list correctly.");
  }

  @Test
  void testParseString() {
    Assertions.assertEquals("NOx", new AplBlockReader(Map.of(KEY_STRING, "\"NOx\""), null).parseString(KEY), "Should strip quotes from string.");
  }

  @Test
  void testParseStringList() {
    final String testString = " \"NOx\" \"PM10\" \"PM2.5\" \"VOC\" ";
    Assertions.assertLinesMatch(List.of("NOx", "PM10", "PM2.5", "VOC"), new AplBlockReader(Map.of(KEY_STRING, testString), null).parseStringList(KEY),
        "Multiple values incorrectly parsed.");
  }
}
