/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test reads AP/UPL files and writes them back and verifies if the content is still the same.
 */
class AplFileRoundtripTest {

  @TempDir
  File tempDir;

  @ParameterizedTest
  @ValueSource(strings = {"pointsource.upl", "pointsource.apl", "linesource.upl", "linesource.apl"})
  void testRoundtrip(final String name) throws IOException, AeriusException {
    final ADMSSourceFileType fileType = ADMSSourceFileType.fileTypeFromExtension(name);
    final AplFileReader reader = new AplFileReader(fileType);
    final AplFileContent content;

    try (InputStream is = getClass().getResourceAsStream(name)) {
      content = reader.readObjects(is);
    }
    content.setMetDataFile("");
    content.setAspFile(new File("dummy.asp")); // Set dummy asp filename as the name is not read from the apl/upl file.
    content.getSubstances().sort((s1, s2) -> Integer.compare(s2.ordinal(), s1.ordinal()));
    final int idx = name.indexOf('.');
    final File file = new File(tempDir, name.substring(0, idx));
    final AplFileWriter writer = new AplFileWriter(fileType, new AplPollutantBuilder());
    writer.writeFile(file, content);

    final String output = normalize(Files.readAllBytes(fileType.fileWithExtension(file).toPath()));
    final String reference = normalize(getClass().getResourceAsStream(name).readAllBytes());

    assertEquals(reference, output, "Read and written of file '" + name + "' should be the same");
  }

  private String normalize(final byte[] bytes) {
    return new String(bytes, StandardCharsets.UTF_8)
        .replaceAll("\\s{1,20}=", "=")
        // filter out dynamic output path of asp file.
        .replaceFirst("GrdPtsPointsFilePath= \"[^\\.]+\\.asp\"", "GrdPtsPointsFilePath= \"file.asp\"");
  }
}
