/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.io.ADMSSourceFileType;
import nl.aerius.adms.io.AplExportWriter;
import nl.aerius.adms.io.AplImportReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Round trip test for reading ADMS input, convert to AERIUS internal. Than convert back to ADMS input and write back to file.
 */
class ADMSConverterRoundtripTest {

  private static final int SOURCES_KEY = 1;

  private AplImportReader reader;

  @TempDir
  File tempDir;

  @BeforeEach
  void beforeEach() throws IOException {
    reader = new AplImportReader();
  }

  @ParameterizedTest
  @ValueSource(strings = {"pointsource.upl"})
  void testWrite(final String name) throws IOException, AeriusException {
    final String referenceFile = "../io/" + name;
    // Read the reference file
    final ImportParcel importParcel = new ImportParcel();
    try (InputStream is = getClass().getResourceAsStream(referenceFile)) {
      reader.read(referenceFile, is, null, null, importParcel);
    }
    final List<Substance> substances = List.of(Substance.NOX, Substance.NO2);
    // Write the reference file data back to a new file
    final ADMSInputData content = convert2ADMS(importParcel, substances);
    content.setFileType(ADMSSourceFileType.fileTypeFromExtension(name));
    final int idx = name.lastIndexOf('.');
    final File file = new File(tempDir, name.substring(0, idx));

    new AplExportWriter().write(file, content, SOURCES_KEY);

    final String output = normalize(Files.readAllBytes(content.getFileType().fileWithExtension(file).toPath()));
    final String reference = normalize(getClass().getResourceAsStream(referenceFile).readAllBytes());
    assertEquals(reference, output, "Read and written file should be the same");
  }

  private ADMSInputData convert2ADMS(final ImportParcel importParcel, final List<Substance> substances) throws AeriusException {
    final ADMSInputData inputData = new ADMSInputData();

    inputData.setMetDataFile("");
    inputData.setSubstances(substances);
    // Convert Emission Sources
    final EmissionSource2ADMSConverter converter = new EmissionSource2ADMSConverter();

    inputData.setEmissionSources(SOURCES_KEY,
        importParcel.getSituation().getEmissionSourcesList().stream()
          .map(esf -> converter.convert(esf.getProperties(), esf.getGeometry(), substances))
          .collect(Collectors.toList()));
    // Convert Buildings
    for (final BuildingFeature building : importParcel.getSituation().getBuildingsList()) {
      inputData.getEmissionSources().get(SOURCES_KEY).add(EmissionSource2ADMSConverter.convert(building));
    }
    return inputData;
  }

  private String normalize(final byte[] bytes) {
    return new String(bytes, StandardCharsets.UTF_8)
        .replaceAll("\\s{1,20}=", "=")
        // filter out dynamic output path of asp file.
        .replaceFirst("GrdPtsPointsFilePath= \"[^\\.]+\\.asp\"", "GrdPtsPointsFilePath= \"file.asp\"")
        // filter out dynamic output path of dummy .met file.
        .replaceFirst("MetDataFileWellFormedPath= \"[^\\.]+oneday\\.met\"", "MetDataFileWellFormedPath= \"\"");
  }
}
