/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import nl.aerius.adms.exception.ADMSInvalidLicense;
import nl.aerius.adms.exception.ADMSInvalidVersionException;

/**
 * Test class for {@link ADMSRunner} to test if ADMS errors are detected.
 */
class ADMSRunnerStartupTest extends BaseRunADMSTest {

  @TempDir
  File tempDir;

  /**
   * Should run without exceptions being thrown.
   */
  @Test
  void testValidInstallation() throws Exception {
    Assertions.assertNotNull(factory.createRunADMS(getADMSWorkerConfiguration()));
  }


  /**
   * Runs with license and executable copied to target directory.
   */
  @Test
  void testValidPassedLicense() throws Exception {
    // Runs
    final File file = new File(config.getADMSRoot(), ADMSRunner.ADMS_URBAN_LIC_NAME);
    final byte[] license = Files.readAllBytes(file.toPath());
    final File targetDirectory = run(new ADMSRunner(ADMSVersion.VERSION, config.getADMSRoot()), license);

    assertNotEquals(config.getADMSRoot(), targetDirectory, "Target directory should not be the same as configured directory.");
    assertFalse(new File(targetDirectory, ADMSRunner.ADMS_URBAN_LIC_NAME).exists(), "License file be removed.");
  }

  /**
   * Should thrown an error that the wrong ADMS version is used to run.
   */
  @Test
  void testInvalidADMSVersion() {
    Assertions.assertThrows(ADMSInvalidVersionException.class, () -> run(new ADMSRunner("none", config.getADMSRoot()), null));
  }

  /**
   * Pass an empty license to use. This should trigger an invalid license error.
   */
  @Test
  void testInvalidLicense() {
    Assertions.assertThrows(ADMSInvalidLicense.class, () -> run(new ADMSRunner(ADMSVersion.VERSION, config.getADMSRoot()), new byte[0]));
  }

  private File run(final ADMSRunner runner, final byte[] license) throws Exception, IOException {
    return runner.run(tempDir, List.of(), license);
  }
}
