/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Test class for {@link PltFileReader}.
 */
class PltFileReaderTest {

  private static final String PLT_FILE = "results.plt";

  @Test
  void testReadPltFile() throws IOException {
    final PltFileReader reader = new PltFileReader(List.of(EmissionResultKey.NOX_CONCENTRATION));

    try (final InputStream is = getClass().getResourceAsStream(PLT_FILE)) {
      final LineReaderResult<AeriusResultPoint> results = reader.readObjects(is);
      assertTrue(results.getExceptions().isEmpty(), "Plt file reading should not result in errors.");
      final List<AeriusResultPoint> points = results.getObjects();

      double referenceResult = 1.234567E+01;
      for (int i = 0; i < points.size(); i++) {
        final AeriusResultPoint arp = points.get(i);

        assertEquals(i + 1, arp.getId(), "Should have correct id");
        assertEquals(referenceResult, arp.getEmissionResult(EmissionResultKey.NOX_CONCENTRATION), "Should have expected result value");
        referenceResult *= 10;
      }
    }
  }
}
