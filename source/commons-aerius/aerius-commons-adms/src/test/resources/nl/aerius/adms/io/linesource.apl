&ADMS_HEADER
Comment     = "This is an ADMS-Urban Model parameter file"
Model       = "ADMS-Urban"
Version     = 5
FileVersion = 12
Complete    = 1
/
&ADMS_PARAMETERS_SUP
SupSiteName                 = " "
SupProjectName              = " "
SupUseAddInput              = 0
SupAddInputPath             = " "
SupReleaseType              = 0
SupModelBuildings           = 1
SupModelComplexTerrain      = 0
SupModelCoastline           = 0
SupCalcChm                  = 0
SupCalcDryDep               = 0
SupCalcWetDep               = 0
SupModelOdours              = 0
SupPaletteType              = 1
SupUseSpatialSplitting      = 0
SupNumSplitRegions          = 0
SupUseSrcExcByRec           = 0
SupUseSrcExcByEmRate        = 0
SupUseTimeVaryingEmissions  = 0
SupTimeVaryingEmissionsType = 0
SupUseTimeVaryingFAC        = 0
SupTimeVaryingFACPath       = " "
SupUseTimeVaryingHFC        = 0
SupTimeVaryingHFCPath       = " "
SupUseTimeVaryingScreenBySource= "00001100"
SupTimeVaryingEmissionFactorsWeekday =
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
SupTimeVaryingEmissionFactorsSaturday =
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
SupTimeVaryingEmissionFactorsSunday =
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
  1.0e+0 1.0e+0 1.0e+0 1.0e+0
SupTrafficEmissionsDataset  = "EFT v9.0 (2 VC)"
SupModelAircraft            = 0
SupAircraftDataPath         = " "
SupAircraftUseSpeedEmissionFile= 0
SupAircraftSpeedEmissionPath= " "
SupAircraftNumGroups        = 0
SupNO2PercentIndustrial     = 5
SupNO2PercentRoad           = 23.8
SupNO2PercentGrid           = 12
SupNO2PercentAircraft       = 5
/
&ADMS_PARAMETERS_SPLIT
SplitRegionRunType          = 0
SplitRegionRunSingleID      = 1
SplitTruncateOutputPoints   = 1
SplitTruncateSource =
  1 1 1 1
  1 0 0 0
SplitTruncateBufferSize =
  5.00e+2 5.00e+2 5.00e+2 5.00e+2
  5.00e+2 5.00e+2 5.00e+2 5.00e+2
SplitPartiallyInBufferType =
  0 0 0 0
  0 0 0 0
/
&ADMS_PARAMETERS_SER
SERSelectedSourceType       = 99
SERTruncateSource =
  1 1 1 1
  1 0 0 0
SERTruncateRadius =
  1.000e+3 1.000e+3 1.000e+3 1.000e+3
  1.000e+3 1.000e+3 1.000e+3 1.000e+3
/
&ADMS_PARAMETERS_SEE
SEEPollutant                = " "
SEETruncateSource =
  1 1 1 1
  1 0 0 0
SEETruncateEmRate =
  0.0e+0 0.0e+0 0.0e+0 0.0e+0
  0.0e+0 0.0e+0 0.0e+0 0.0e+0
/
&ADMS_PARAMETERS_MET
MetLatitude               = 5.2e+1
MetDataSource             = 0
MetDataFileWellFormedPath = ""
MetWindHeight             = 1.0e+1
MetWindInSectors          = 1
MetWindSectorSizeDegrees  = 1.0e+1
MetDataIsSequential       = 1
MetUseSubset              = 0
MetSubsetHourStart        = 1
MetSubsetDayStart         = 1
MetSubsetMonthStart       = 1
MetSubsetYearStart        = 2020
MetSubsetHourEnd          = 24
MetSubsetDayEnd           = 31
MetSubsetMonthEnd         = 12
MetSubsetYearEnd          = 2020
MetUseVerticalProfile     = 0
MetVerticalProfilePath    = " "
Met_DS_RoughnessMode      = 1
Met_DS_Roughness          = 5.0e-1
Met_DS_UseAdvancedMet     = 1
Met_DS_SurfaceAlbedoMode  = 0
Met_DS_SurfaceAlbedo      = 2.3e-1
Met_DS_PriestlyTaylorMode = 0
Met_DS_PriestlyTaylor     = 1.0e+0
Met_DS_MinLmoMode         = 1
Met_DS_MinLmo             = 3.0e+1
Met_DS_PrecipFactorMode   = 0
Met_DS_PrecipFactor       = 1.0e+0
Met_MS_RoughnessMode      = 3
Met_MS_Roughness          = 5.0e-1
Met_MS_UseAdvancedMet     = 0
Met_MS_SurfaceAlbedoMode  = 3
Met_MS_SurfaceAlbedo      = 2.3e-1
Met_MS_PriestlyTaylorMode = 3
Met_MS_PriestlyTaylor     = 1.0e+0
Met_MS_MinLmoMode         = 3
Met_MS_MinLmo             = 1.0e+0
MetHeatFluxType           = 0
MetInclBoundaryLyrHt      = 1
MetInclSurfaceTemp        = 0
MetInclLateralSpread      = 0
MetInclRelHumidity        = 0
MetHandNumEntries         = 0
/
&ADMS_PARAMETERS_BLD
BldNumBuildings = 2
BldName       =
  "Building001" "Building002"
BldType =
  0 1
BldX =
  5.4462016e+5 5.4466543e+5
BldY =
  2.5905844e+5 2.5905464e+5
BldHeight =
  1.0e+1 1.0e+1
BldLength =
  3.0e+1 3.0e+1
BldWidth =
  3.0e+1 3.0e+1
BldAngle =
  1.0e+1 0.0e+0
/

&ADMS_PARAMETERS_HIL
HilGridSize        = 2
HilUseTerFile      = 0
HilUseRoughFile    = 0
HilTerrainPath     = " "
HilRoughPath       = " "
HilCreateFlowField = 0
/
&ADMS_PARAMETERS_CST
CstPoint1X         = 0.0e+0
CstPoint1Y         = 0.0e+0
CstPoint2X         = -1.000e+3
CstPoint2Y         = 1.000e+3
CstLandPointX      = 5.00e+2
CstLandPointY      = 5.00e+2
/
&ADMS_PARAMETERS_GRD
GrdType            = 1
GrdCoordSysType    = 0
GrdSpacingType     = 0
GrdUseSourceOrientedGrids = 
 1
 0
GrdRegularMin      = 
 -1.000e+3 -1.000e+3 0.0e+0
GrdRegularMax      = 
 1.000e+3 1.000e+3 0.0e+0
GrdRegularNumPoints= 
 31 31 1
GrdVarSpaceNumPointsX  = 0
GrdVarSpaceNumPointsY  = 0
GrdVarSpaceNumPointsZ  = 0
GrdPtsNumPoints        = 0
GrdPtsUsePointsFile    = 1
GrdPtsPointsFilePath   = "/tmp/pointsfile.asp"
/
&ADMS_PARAMETERS_OPT
OptNumOutputs               = 2
OptPolName                  =
  "NOx" "NO2"
OptInclude                  =
  1 1
OptShortOrLong              =
  1 1
OptSamplingTime             =
  1.0e+0 1.0e+0
OptSamplingTimeUnits        =
  3 3
OptCondition                =
  0 0
OptNumPercentiles           =
  0 0
OptNumExceedences           =
  0 0
OptPercentiles              =
  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0
  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0
OptExceedences              =
  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0
  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0  0.0e+0
OptUnits                    =
  "ug/m3" "ug/m3"
OptValidity                 =
  7.5e+1 7.5e+1
OptGroupsOrSource           = 0
OptAllSources               = 1
OptNumGroups                = 0
OptIncludedSource           = ""
OptCreateComprehensiveFile  = 0
OptOutputPerSource          = 0
/

&ADMS_PARAMETERS_CHM
ChmScheme       = 1
/
&ADMS_PARAMETERS_BKG
BkgFilePath     = " "
BkgFixedLevels  = 2
/
&ADMS_COORDINATESYSTEM
ProjectedEPSG               = 27700
ProjectedName               = "OSGB 1936 British National Grid (epsg:27700)"
ProjectedWKT                = " "
/
&ADMS_PARAMETERS_ETC
SrcNumSources   = 1
PolNumPollutants= 8
PolNumIsotopes  = 1
/
&ADMS_MAPPERPROJECT
ProjectFilePath             = " "
/
&ADMS_POLLUTANT_DETAILS
PolName                  = "NOx"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 5.2e-1
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/
&ADMS_POLLUTANT_DETAILS
PolName                  = "NO2"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 5.2e-1
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/
&ADMS_POLLUTANT_DETAILS
PolName                  = "NO"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 8.0e-1
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/
&ADMS_POLLUTANT_DETAILS
PolName                  = "VOC"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 3.1e-1
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/
&ADMS_POLLUTANT_DETAILS
PolName                  = "O3"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 5.0e-1
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/
&ADMS_POLLUTANT_DETAILS
PolName                  = "SO2"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 3.7e-1
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/
&ADMS_POLLUTANT_DETAILS
PolName                  = "PM2.5"
PolPollutantType         = 1
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  2.5e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 1.0e+0
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ug/m3"
/
&ADMS_POLLUTANT_DETAILS
PolName                  = "PM10"
PolPollutantType         = 1
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 0.0e+0
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-5
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 1.0e+0
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ug/m3"
/
&ADMS_POLLUTANT_DETAILS
PolName                  = "NH3"
PolPollutantType         = 0
PolGasDepVelocityKnown   = 1
PolGasDepositionVelocity = 2.0e-2
PolGasType               = 1
PolParDepVelocityKnown   = 1
PolParTermVelocityKnown  = 1
PolParNumDepositionData  = 1
PolParDepositionVelocity =
  0.0e+0
PolParTerminalVelocity =
  0.0e+0
PolParDiameter =
  1.0e-6
PolParDensity =
  1.000e+3
PolParMassFraction =
  1.0e+0
PolWetWashoutKnown = 1
PolWetWashout      = 0.0e+0
PolWetWashoutA     = 1.0e-4
PolWetWashoutB     = 6.4e-1
PolConvFactor      = 1.41e+0
PolBkgLevel        = 0.0e+0
PolBkgUnits        = "ppb"
/
&ADMS_ISOTOPE_DETAILS
IsoName                  = "(unnamed-isotope)"
IsoPollutantType         = 0
IsoGasDepVelocityKnown   = 1
IsoGasDepositionVelocity = 0.0e+0
IsoGasType               = 0
IsoParDepVelocityKnown   = 1
IsoParTermVelocityKnown  = 1
IsoParNumDepositionData  = 1
IsoParDepositionVelocity =
  0.0e+0
IsoParTerminalVelocity =
  0.0e+0
IsoParDiameter =
  1.0e-6
IsoParDensity =
  1.000e+3
IsoParMassFraction =
  1.0e+0
IsoWetWashoutKnown = 1
IsoWetWashout      = 0.0e+0
IsoWetWashoutA     = 1.0e-4
IsoWetWashoutB     = 6.4e-1
IsoConvFactor      = 1.0e+0
IsoBkgLevel        = 0.0e+0
IsoBkgUnits        = "Bq/m3"
/

&ADMS_SOURCE_DETAILS
SrcName         = "LineSource"
SrcMainBuilding = "Building001"
SrcHeight       = 1.2e+1
SrcDiameter     = 1.0e+0
SrcVolFlowRate  = 1.1781e+1
SrcVertVeloc    = 1.5e+1
SrcTemperature  = 1.5e+1
SrcMolWeight    = 2.8966e+1
SrcDensity      = 1.225e+0
SrcSpecHeatCap  = 1.012e+3
SrcSourceType   = 3
SrcReleaseAtNTP = 0
SrcEffluxType   = 0
SrcBuoyancyType = 0
SrcPercentNOxAsNO2 = 5.0e+0
SrcX1           = 0.0e+0
SrcY1           = 0.0e+0
SrcL1           = 0.0e+0
SrcL2           = 0.0e+0
SrcFm            = 0.0e+0
SrcFb            = 0.0e+0
SrcMassFlux      = 0.0e+0
SrcAngle1        = 0.0e+0
SrcAngle2        = 0.0e+0
SrcMassH2O       = 0.0e+0
SrcUseVARFile    = 0
SrcNumGroups    = 0
SrcTraEmissionsMode = 0
SrcTraYear      = 2018
SrcTraRoadType  = "Unknown"
SrcTraGradient  = 0.0e+0
SrcNumVertices  = 4
SrcTraNumTrafficFlows = 0
SrcNumPollutants= 2
SrcPollutants =
  "NOx" "NO2"
SrcPolEmissionRate =
  1.0e+0 2.0e+0
SrcPolTotalemission =
  1.0e+0 1.0e+0
SrcPolStartTime =
  0.0e+0 0.0e+0
SrcPolDuration =
  0.0e+0 0.0e+0
SrcNumIsotopes = 0
/
  &ADMS_SOURCE_VERTEX
  SourceVertexX = 5.4461645e+5
  SourceVertexY = 2.5905626e+5
  /
  &ADMS_SOURCE_VERTEX
  SourceVertexX = 5.4462645e+5
  SourceVertexY = 2.5904626e+5
  /
  &ADMS_SOURCE_VERTEX
  SourceVertexX = 5.4464645e+5
  SourceVertexY = 2.5905636e+5
  /
  &ADMS_SOURCE_VERTEX
  SourceVertexX = 5.4467645e+5
  SourceVertexY = 2.5905636e+5
  /
