/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import java.util.List;

import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSBuilding.BuildingShape;
import nl.aerius.adms.domain.ADMSLimits;
import nl.aerius.adms.domain.ADMSSource;
import nl.aerius.adms.domain.ADMSSource.SourceType;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.OrientedEnvelope;
import nl.overheid.aerius.shared.domain.v2.building.Building;
import nl.overheid.aerius.shared.domain.v2.building.BuildingFeature;
import nl.overheid.aerius.shared.domain.v2.characteristics.ADMSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.characteristics.SourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.RoadEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Converter class to convert EmissionSource objects to ADMSSource objects.
 */
public class EmissionSource2ADMSConverter {

  /**
   * Converts an EmissionSource object to an ADMSSource object.
   *
   * @param source source to convert
   * @param geometry geometry of the source
   * @param substances substances of emissions to copy
   * @return
   */
  public <G extends Geometry> ADMSSource<G> convert(final EmissionSource source, final G geometry, final List<Substance> substances) {
    final ADMSSource<G> admsSource = new ADMSSource<>();
    final boolean roadSource = source instanceof RoadEmissionSource;
    admsSource.setGeometry(geometry);
    admsSource.setName(source.getLabel());
    admsSource.setSourceType(getSourceType(geometry, roadSource));
    substances.forEach(s -> admsSource.setEmission(s, source.getEmissions().getOrDefault(s, 0.0)));
    final SourceCharacteristics crs = source.getCharacteristics();
    if (crs instanceof ADMSSourceCharacteristics) {
      final ADMSSourceCharacteristics admscrs = (ADMSSourceCharacteristics) crs;

      setCharacteristics(admscrs, admsSource);
      if (geometry instanceof Point) {
        setCharacteristicsIfPointSource(admscrs, admsSource);
      }
      if (roadSource) {
        setCharacteristicsIfRoadSource(admscrs, admsSource);
      }
    }
    return admsSource;
  }

  private <G extends Geometry> SourceType getSourceType(final G geometry, final boolean roadSource) {
    final SourceType type;
    if (geometry instanceof Point) {
      type = SourceType.POINT;
    } else if (geometry instanceof LineString) {
      type = roadSource ? SourceType.ROAD : SourceType.LINE;
    } else if (geometry instanceof Polygon) {
      type = SourceType.AREA;
      // TODO determine difference between AREA and VOLUME sources:
      //type = SourceType.VOLUME;
    } else {
      throw new IllegalArgumentException("Unexpected geometry: " + geometry);
    }
    return type;
  }

  private <G extends Geometry> void setCharacteristics(final ADMSSourceCharacteristics sc, final ADMSSource<G> es) {
    es.setMainBuilding(sc.getBuildingId());
    es.setDiameter(sc.getDiameter());
    es.setHeight(sc.getHeight());

    // Set default to get sane default values as long as ADMS characteristics are not fully supported
    // throughout the rest of the application.

    if (sc.getDiameter() < ADMSLimits.SOURCE_DIAMETER_MINIMUM) {
      es.setDiameter(1.0);
    }
    if (sc.getSpecificHeatCapacity() > 0) {
      es.setTemperature(sc.getTemperature());
      es.setDensity(sc.getDensity());
      es.setVerticalVelocity(sc.getVerticalVelocity());
      es.setVolumetricFlowRate(sc.getVolumetricFlowRate());
      es.setSpecificHeatCapacity(sc.getSpecificHeatCapacity());
      es.setPercentNOxAsNO2(sc.getPercentNOxAsNO2());
      es.setMomentumFlux(sc.getMomentumFlux());
      es.setBuoyancyFlux(sc.getBuoyancyFlux());
      es.setMassFlux(sc.getMassFlux());
    }
  }

  private <G extends Geometry> void setCharacteristicsIfPointSource(final ADMSSourceCharacteristics sc, final ADMSSource<G> es) {
    // TODO set here characteristics that are specific for point sources.
  }

  private <G extends Geometry> void setCharacteristicsIfRoadSource(final ADMSSourceCharacteristics sc, final ADMSSource<G> es) {
    // TODO set here characteristics that are specific for road sources.
    // Width of line/road source, depth of volume source.
    //      private final double l1;
    //      private final double l2;
  }

  /**
   * Converts BuidingFeature to {@link ADMSBuilding}.
   *
   * @param feature Building feature
   * @return ADMSBuilding
   * @throws AeriusException
   */
  public static ADMSBuilding convert(final BuildingFeature feature) throws AeriusException {
    final ADMSBuilding building = new ADMSBuilding();
    final Building properties = feature.getProperties();

    building.setName(properties.getLabel());
    final Geometry geometry = feature.getGeometry();
    if (properties.isCircle()) {
      building.setBuildingCentre((Point) geometry);
      building.setShape(BuildingShape.CIRCULAR);
      building.setBuildingLength(properties.getRadius());
      building.setBuildingWidth(properties.getRadius());
      building.setBuildingOrientation(0);
    } else {
      final OrientedEnvelope envelope = GeometryUtil.determineOrientedEnvelope((Polygon) geometry);

      building.setBuildingCentre(GeometryUtil.determineCenter(geometry));
      building.setShape(BuildingShape.RECTANGULAR);
      building.setBuildingLength(envelope.getLength());
      building.setBuildingWidth(envelope.getWidth());
      building.setBuildingOrientation(envelope.getOrientation());
    }
    building.setBuildingHeight(properties.getHeight());
    return building;
  }
}
