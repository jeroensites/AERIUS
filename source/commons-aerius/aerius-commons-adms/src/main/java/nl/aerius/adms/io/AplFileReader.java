/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileConstants.ADMS_PARAMETERS_BLD;
import static nl.aerius.adms.io.AplFileConstants.ADMS_SOURCE_DETAILS;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.Substance;

/**
 * Reader to read ADMS project file with extension .apl or .upl.
 */
class AplFileReader {

  private final AplSourceReader sourceReader;
  private final AplBuildingsReader buildingsReader = new AplBuildingsReader();

  public AplFileReader(final ADMSSourceFileType fileType) {
    sourceReader = new AplSourceReader(fileType);
  }

  public AplFileContent readObjects(final InputStream inputStream) throws IOException {
    final AplFileContent content = new AplFileContent();

    try (final InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        final BufferedReader br = new BufferedReader(isr)) {
      while (readBlock(br, content)) {}
    }
    setSubstances(content);
    return content;
  }

  private boolean readBlock(final BufferedReader br, final AplFileContent result) throws IOException {
    final AplBlockReader blockReader = AplBlockReader.readBlock(br);
    final String blockName = blockReader.getBlockName();

    if (blockName == null) {
      return false;
    }
    switch (blockName) {
    case ADMS_PARAMETERS_BLD:
      buildingsReader.addBuildings(blockReader, result);
      break;
    case ADMS_SOURCE_DETAILS:
      final ADMSSource source = sourceReader.readSource(blockReader);
      if (source != null) {
        result.addObject(source);
      }
      break;
    }
    return true;
  }

  private static void setSubstances(final AplFileContent content) {
    final Set<Substance> substances = new HashSet<>();
    for (final ADMSSource<?> admsSource : content.getObjects()) {
      substances.addAll(admsSource.getSubstances());
    }
    substances.forEach(content::addSubstance);
  }
}
