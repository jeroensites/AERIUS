/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileConstants.ADMS_SOURCE_DETAILS;
import static nl.aerius.adms.io.AplFileConstants.ADMS_SOURCE_VERTEX;
import static nl.aerius.adms.io.AplFileKeys.SOURCE_VERTEX_X;
import static nl.aerius.adms.io.AplFileKeys.SOURCE_VERTEX_Y;
import static nl.aerius.adms.io.AplFileKeys.SRC_ANGLE_1;
import static nl.aerius.adms.io.AplFileKeys.SRC_ANGLE_2;
import static nl.aerius.adms.io.AplFileKeys.SRC_BUOYANCY_TYPE;
import static nl.aerius.adms.io.AplFileKeys.SRC_DENSITY;
import static nl.aerius.adms.io.AplFileKeys.SRC_DIAMETER;
import static nl.aerius.adms.io.AplFileKeys.SRC_EFFLUX_TYPE;
import static nl.aerius.adms.io.AplFileKeys.SRC_FB;
import static nl.aerius.adms.io.AplFileKeys.SRC_FM;
import static nl.aerius.adms.io.AplFileKeys.SRC_HEIGHT;
import static nl.aerius.adms.io.AplFileKeys.SRC_L1;
import static nl.aerius.adms.io.AplFileKeys.SRC_L2;
import static nl.aerius.adms.io.AplFileKeys.SRC_MAIN_BUILDING;
import static nl.aerius.adms.io.AplFileKeys.SRC_MASS_FLUX;
import static nl.aerius.adms.io.AplFileKeys.SRC_MASS_H2O;
import static nl.aerius.adms.io.AplFileKeys.SRC_MOL_WEIGHT;
import static nl.aerius.adms.io.AplFileKeys.SRC_NAME;
import static nl.aerius.adms.io.AplFileKeys.SRC_NUM_GROUPS;
import static nl.aerius.adms.io.AplFileKeys.SRC_NUM_ISOTOPES;
import static nl.aerius.adms.io.AplFileKeys.SRC_NUM_POLLUTANTS;
import static nl.aerius.adms.io.AplFileKeys.SRC_NUM_VERTICES;
import static nl.aerius.adms.io.AplFileKeys.SRC_PERCENT_NOX_AS_NO2;
import static nl.aerius.adms.io.AplFileKeys.SRC_POLLUTANTS;
import static nl.aerius.adms.io.AplFileKeys.SRC_POL_DURATION;
import static nl.aerius.adms.io.AplFileKeys.SRC_POL_EMISSION_RATE;
import static nl.aerius.adms.io.AplFileKeys.SRC_POL_START_TIME;
import static nl.aerius.adms.io.AplFileKeys.SRC_POL_TOTALEMISSION;
import static nl.aerius.adms.io.AplFileKeys.SRC_RELEASE_AT_NTP;
import static nl.aerius.adms.io.AplFileKeys.SRC_SOURCE_TYPE;
import static nl.aerius.adms.io.AplFileKeys.SRC_SPECIFIC_HEAT_CAPACITY;
import static nl.aerius.adms.io.AplFileKeys.SRC_TEMPERATURE;
import static nl.aerius.adms.io.AplFileKeys.SRC_TRA_EMISSIONS_MODE;
import static nl.aerius.adms.io.AplFileKeys.SRC_TRA_GRADIENT;
import static nl.aerius.adms.io.AplFileKeys.SRC_TRA_NUM_TRAFFIC_FLOWS;
import static nl.aerius.adms.io.AplFileKeys.SRC_TRA_ROAD_TYPE;
import static nl.aerius.adms.io.AplFileKeys.SRC_TRA_YEAR;
import static nl.aerius.adms.io.AplFileKeys.SRC_USE_VAR_FILE;
import static nl.aerius.adms.io.AplFileKeys.SRC_VERTICAL_VELOCITY;
import static nl.aerius.adms.io.AplFileKeys.SRC_VOLUMETRIC_FLOW_RATE;
import static nl.aerius.adms.io.AplFileKeys.SRC_X1;
import static nl.aerius.adms.io.AplFileKeys.SRC_Y1;

import java.util.List;
import java.util.stream.Collectors;

import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.BuoyancyType;
import nl.overheid.aerius.shared.domain.v2.characteristics.adms.EffluxType;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Builds an {@link ADMSSource} into a APL/UPL formatted ADMS_SOURCE_DETAILS string.
 */
final class AplSourceBuilder extends AplBuilder {

  private final ADMSSourceFileType fileType;

  private AplSourceBuilder(final ADMSSourceFileType fileType) {
    this.fileType = fileType;
    // Builder should be used through the static build method.
  }

  /**
   * Builds a APL/UPL formatted ADMS_SOURCE_DETAILS from the given {@link ADMSSource}.
   *
   * @param source source to get formatted string from
   * @param substances emission substances to be included
   * @return APL/UPL formatted ADMS_SOURCE_DETAILS string
   * @throws AeriusException
   */
  public static String build(final ADMSSourceFileType fileType, final ADMSSource<?> source, final List<Substance> substances) throws AeriusException {
    final AplSourceBuilder builder = new AplSourceBuilder(fileType);

    return builder.buildSource(source, substances);
  }

  private String buildSource(final ADMSSource<?> source, final List<Substance> substances) throws AeriusException {
    buildHeader(ADMS_SOURCE_DETAILS);
    buildRow(SRC_NAME, source.getName());
    buildRow(SRC_MAIN_BUILDING, source.getMainBuilding());
    buildRow(SRC_HEIGHT, source.getHeight());
    buildRow(SRC_DIAMETER, source.getDiameter());
    final EffluxType effluxType = EffluxType.valueOf(source.getEffluxType());

    if (effluxType == EffluxType.VOLUME) {
      buildRow(SRC_VOLUMETRIC_FLOW_RATE, source.getVolumetricFlowRate());
    } else {
      buildRow(SRC_VOLUMETRIC_FLOW_RATE);
    }
    if (effluxType == EffluxType.VELOCITY) {
      buildRow(SRC_VERTICAL_VELOCITY, source.getVerticalVelocity());
    } else {
      buildRow(SRC_VERTICAL_VELOCITY);
    }
    final BuoyancyType buoyancyType = BuoyancyType.valueOf(source.getBuoyancyType());

    if (buoyancyType == BuoyancyType.TEMPERATURE) {
      buildRow(SRC_TEMPERATURE, source.getTemperature());
    } else {
      buildRow(SRC_TEMPERATURE);
    }
    buildRow(SRC_MOL_WEIGHT);
    if (buoyancyType == BuoyancyType.DENSITY) {
      buildRow(SRC_DENSITY, source.getDensity());
    } else {
      buildRow(SRC_DENSITY);
    }
    buildRow(SRC_SPECIFIC_HEAT_CAPACITY);
    buildRow(SRC_SOURCE_TYPE, source.getSourceType().getType());
    buildRow(SRC_RELEASE_AT_NTP, source.getReleaseAtNTP());
    buildRow(SRC_EFFLUX_TYPE, source.getEffluxType());
    buildRow(SRC_BUOYANCY_TYPE, source.getBuoyancyType());
    if (ADMSSourceFileType.APL == fileType) {
      buildRow(SRC_PERCENT_NOX_AS_NO2, source.getPercentNOxAsNO2());
    }
    final Geometry geometry = source.getGeometry();
    if (geometry instanceof Point || source.getGeometry() instanceof Polygon) {
      final Point center = GeometryUtil.determineCenter(source.getGeometry());

      buildRow(SRC_X1, center.getX());
      buildRow(SRC_Y1, center.getY());
    } else {
      // TODO implement support for line.
      buildRow(SRC_X1, 0.0);
      buildRow(SRC_Y1, 0.0);
      //
    }
    buildRow(SRC_L1, 0.0); // TODO should be changed when other geometry support added.
    buildRow(SRC_L2, 0.0); // TODO should be changed when other geometry support added.
    if (ADMSSourceFileType.APL == fileType) {
      buildRow(SRC_FM, source.getMomentumFlux());
      buildRow(SRC_FB, source.getBuoyancyFlux());
      buildRow(SRC_MASS_FLUX, source.getMassFlux());
      buildRow(SRC_ANGLE_1, source.getAngle1());
      buildRow(SRC_ANGLE_2, source.getAngle2());
      buildRow(SRC_MASS_H2O, source.getMassH2O());
      buildRow(SRC_USE_VAR_FILE, 0);
    }
    buildRow(SRC_NUM_GROUPS);
    buildRow(SRC_TRA_EMISSIONS_MODE, 0);
    buildRow(SRC_TRA_YEAR, 2018);
    buildRow(SRC_TRA_ROAD_TYPE, "Unknown");
    buildRow(SRC_TRA_GRADIENT, 0.0);
    buildRow(SRC_NUM_VERTICES, computeNrOfVertices(source.getGeometry()));
    buildRow(SRC_TRA_NUM_TRAFFIC_FLOWS); // TODO should be changed when other geometry support added.

    final List<String> subs = substances.stream().map(Substance::getName).collect(Collectors.toList());
    final int size = subs.size();
    buildRow(SRC_NUM_POLLUTANTS, size);
    buildRowList(SRC_POLLUTANTS, subs);
    buildRowListDouble(SRC_POL_EMISSION_RATE, substances.stream()
        .map(s -> source.getEmission(s) / Substance.EMISSION_IN_G_PER_S_FACTOR).collect(Collectors.toList()));
    buildRowListDefaultDouble(SRC_POL_TOTALEMISSION, size);
    buildRowListDefaultDouble(SRC_POL_START_TIME, size);
    buildRowListDefaultDouble(SRC_POL_DURATION, size);
    buildRow(SRC_NUM_ISOTOPES, 0);
    buildEndMarker();
    buildOptionalVertices(source.getGeometry());
    return build();
  }

  private int computeNrOfVertices(final Geometry geometry) {
    if (geometry instanceof LineString) {
      return ((LineString) geometry).getCoordinates().length;
    } else if (geometry instanceof Polygon) {
      return ((Polygon) geometry).getCoordinates()[0].length - 1;
    } else {
      return 0;
    }
  }

  private void buildOptionalVertices(final Geometry geometry) {
    final double [][] coordinates;
    final int nrOfVertices;

    if (geometry instanceof LineString) {
      coordinates = ((LineString) geometry).getCoordinates();
      nrOfVertices = coordinates.length;
    } else if (geometry instanceof Polygon) {
      coordinates = ((Polygon) geometry).getCoordinates()[0];
      nrOfVertices = coordinates.length  - 1;
    } else {
      coordinates = null;
      nrOfVertices = 0;
    }
    for (int i = 0; i < nrOfVertices; i++) {
      appendVerticesPrefix();
      buildHeader(ADMS_SOURCE_VERTEX);
      appendVerticesPrefix();
      buildRow(SOURCE_VERTEX_X, coordinates[i][0]);
      appendVerticesPrefix();
      buildRow(SOURCE_VERTEX_Y, coordinates[i][1]);
      appendVerticesPrefix();
      buildEndMarker();
    }
  }

  private void appendVerticesPrefix() {
    buildPrefix("  ");
  }
}
