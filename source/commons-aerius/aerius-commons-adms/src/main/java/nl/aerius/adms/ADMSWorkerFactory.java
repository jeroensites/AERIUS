/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.util.Properties;

import nl.aerius.adms.domain.ADMSConfiguration;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.Worker;
import nl.overheid.aerius.worker.WorkerFactory;


/**
 * Worker factory to create new instances of ADMS Workers.
 */
public class ADMSWorkerFactory implements WorkerFactory<ADMSWorkerConfiguration> {

  @Override
  public ADMSWorkerConfiguration createConfiguration(final Properties properties) {
    return new ADMSWorkerConfiguration(properties);
  }

  @Override
  public Worker<ADMSInputData, CalculationResult> createWorkerHandler(final ADMSWorkerConfiguration config, final BrokerConnectionFactory factory)
      throws Exception {
    return new Worker<>() {
      private final RunADMS runADMS = createRunADMS(config);

      @Override
      public CalculationResult run(final ADMSInputData input, final WorkerIntermediateResultSender resultSender, final JobIdentifier correlation)
          throws Exception {
        return runADMS.run(input);
      }
    };
  }

  public RunADMS createRunADMS(final ADMSWorkerConfiguration workerConfiguration) throws Exception {
    return new RunADMS(createADMSConfiguration(workerConfiguration));
  }

  public static final ADMSConfiguration createADMSConfiguration(final ADMSWorkerConfiguration config) {
    final ADMSConfiguration admsConfig = new ADMSConfiguration();
    admsConfig.setADMSRoot(config.getADMSRoot());
    admsConfig.setRunFilesDirectory(config.getADMSRunFilesDirectory());
    admsConfig.setKeepGeneratedFiles(config.isADMSKeepGeneratedFiles());
    return admsConfig;
  }

  @Override
  public WorkerType getWorkerType() {
    return WorkerType.ADMS;
  }
}
