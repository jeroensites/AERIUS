/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

public final class ADMSLimits {

  public static final int SOURCE_HEIGHT_MINIMUM = 0;
  public static final int SOURCE_HEIGHT_MAXIMUM = 15_000;
  public static final double SOURCE_DIAMETER_MINIMUM = 0.001;
  public static final int SOURCE_DIAMETER_MAXIMUM = 100;
  public static final int SOURCE_VOLUMETRIC_FLOW_RATE_MINIMUM = 0;
  public static final int SOURCE_VOLUMETRIC_FLOW_RATE_MAXIMUM = 1_000_000_000; // 10^9
  public static final double SOURCE_VOLUMETRIC_FLOW_RATE_DEFAULT = 11.781;
  public static final int SOURCE_VERTICAL_VELOCITY_MINIMUM = 0;
  public static final int SOURCE_VERTICAL_VELOCITY_MAXIMUM = 1_000;
  public static final double SOURCE_VERTICAL_VELOCITY_DEFAULT = 15;
  public static final int SOURCE_TEMPERATURE_MINIMUM = -100;
  public static final int SOURCE_TEMPERATURE_MAXIMUM = 5_000;
  public static final int SOURCE_TEMPERATURE_DEFAULT = 15;
  public static final long SOURCE_MOL_WEIGHT_MINIMUM = 1;
  public static final long SOURCE_MOL_WEIGHT_MAXIMUM = 300;
  public static final double SOURCE_MOL_WEIGHT_DEFAULT = 28.966;
  public static final int SOURCE_DENSITY_MINIMUM = 0;
  public static final int SOURCE_DENSITY_MAXIMUM = 2;
  public static final int SOURCE_SPECIFIC_HEAT_CAPACITY_MINIMUM = 1;
  public static final int SOURCE_SPECIFIC_HEAT_CAPACITY_MAXIMUM = 100_000;
  public static final double SOURCE_SPECIFIC_HEAT_CAPACITY_DEFAULT = 1012;
  public static final int SOURCE_MOMENTUM_FLUX_MINIMUM = 1;
  public static final int SOURCE_MOMENTUM_FLUX_MAXIMUM = 1_000_000;
  public static final double SOURCE_MOMENTUM_FLUX_DEFAULT = 1.0;
  public static final int SOURCE_BUOYANCY_FLUX_MINIMUM = 0;
  public static final int SOURCE_BUOYANCY_FLUX_MAXIMUM = 10_000;
  public static final double SOURCE_BUOYANCY_FLUX_DEFAULT = 1.0;
  public static final long SOURCE_MASS_FLUX_MINIMUM = 0;
  public static final long SOURCE_MASS_FLUX_MAXIMUM = 100_000;
  public static final double SOURCE_MASS_FLUX_DEFAULT = 1;
  public static final long SOURCE_L1_MINIMUM = 0;
  public static final long SOURCE_L1_MAXIMUM = 1_000;

  private ADMSLimits() {
    // Static constants class.
  }
}
