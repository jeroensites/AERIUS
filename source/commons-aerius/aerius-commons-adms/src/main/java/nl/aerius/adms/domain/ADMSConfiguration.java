/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import java.io.File;

/**
 * Configuration file for ADMS.
 */
public class ADMSConfiguration {

  /**
   * Directory of ADMS executable.
   */
  private File admsRoot;
  /**
   * Directory to create temporary directory in to store input files to pass to ADMS.
   */
  private File runFilesDirectory;
  private boolean keepGeneratedFiles;

  public File getADMSRoot() {
    return admsRoot;
  }

  public void setADMSRoot(final File admsRoot) {
    this.admsRoot = admsRoot;
  }

  public File getRunFilesDirectory() {
    return runFilesDirectory;
  }

  public void setRunFilesDirectory(final File runFilesDirectory) {
    this.runFilesDirectory = runFilesDirectory;
  }

  public boolean isKeepGeneratedFiles() {
    return keepGeneratedFiles;
  }

  public void setKeepGeneratedFiles(final boolean keepGeneratedFiles) {
    this.keepGeneratedFiles = keepGeneratedFiles;
  }
}
