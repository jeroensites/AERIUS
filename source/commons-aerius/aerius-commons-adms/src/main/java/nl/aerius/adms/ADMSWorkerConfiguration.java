/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import nl.overheid.aerius.worker.WorkerConfiguration;

/**
 * ADMS worker configuration options.
 */
class ADMSWorkerConfiguration extends WorkerConfiguration {

  private static final String ADMS_WORKER_PREFIX = "adms";
  private static final String ADMS_ROOT = "root";
  private static final String ADMS_RUNFILES_DIRECTORY = "runfiles.directory";
  private static final String ADMS_KEEPGENERATEDFILES = "keepgeneratedfiles";

  /**
   * Configuration for ADMS-related workers.
   * @param properties Properties containing the predefined properties.
   */
  public ADMSWorkerConfiguration(final Properties properties) {
    super(properties, ADMS_WORKER_PREFIX);
  }

  @Override
  protected List<String> getValidationErrors() {
    final List<String> reasons = new ArrayList<>();
    if (getProcesses() > 0) {
      validateDirectoryProperty(ADMS_ROOT, getADMSRoot(), reasons, false);
      validateDirectoryProperty(ADMS_RUNFILES_DIRECTORY, reasons, true);
    }
    return reasons;
  }

  /**
   * @return The directory path containing the ADMS installation.
   */
  public File getADMSRoot() {
    final String root = getProperty(ADMS_ROOT);
    if (root == null) {
      return null;
    }
    return new File(root, ADMSVersion.VERSION);
  }

  /**
   * @return The directory where the files used by OPS can be (temporarily) stored.
   */
  public File getADMSRunFilesDirectory() {
    final String directory = getProperty(ADMS_RUNFILES_DIRECTORY);
    return directory == null ? null : new File(directory);
  }

  /**
   * @return If true doesn't delete the generated input and output files after an OPS run.
   */
  public boolean isADMSKeepGeneratedFiles() {
    return getPropertyBooleanSafe(ADMS_KEEPGENERATEDFILES);
  }
}
