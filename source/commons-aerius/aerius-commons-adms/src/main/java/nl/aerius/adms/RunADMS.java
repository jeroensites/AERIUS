/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import nl.aerius.adms.conversion.ADMSDepositionCalculator;
import nl.aerius.adms.domain.ADMSConfiguration;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.io.AplExportWriter;
import nl.aerius.adms.io.ErrorFileChecker;
import nl.aerius.adms.io.PltFileReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.FileUtil;

/**
 * Class to construct input for ADMS and start the tool and return the output.
 */
class RunADMS {

  private final ADMSRunner admsRunner;
  private final File runFilesDirectory;
  private final boolean keepGeneratedFiles;
  private final Validator validator;

  public RunADMS(final ADMSConfiguration configuration) throws Exception {
    admsRunner = new ADMSRunner(ADMSVersion.VERSION, configuration.getADMSRoot());
    runFilesDirectory = configuration.getRunFilesDirectory();
    keepGeneratedFiles = configuration.isKeepGeneratedFiles();
    startupRun();
    final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
    validator = validatorFactory.getValidator();
  }

  /**
   * Runs ADMS with no arguments to check for valid installation (version, license).
   *
   * @throws Exception Exception thrown in case of invalid version, license
   */
  private void startupRun() throws Exception {
    admsRunner.run(createOutputDirectory(), List.of());
  }

  public CalculationResult run(final ADMSInputData input) throws Exception {
    final CalculationResult result = new CalculationResult(CalculationEngine.ADMS);

    for (final Entry<Integer, Collection<EngineSource>> entry : input.getEmissionSources().entrySet()) {
      result.put(entry.getKey(), run(input, entry.getKey()));
    }
    return result;
  }

  private List<AeriusResultPoint> run(final ADMSInputData input, final Integer sourcesKey) throws Exception {
    validateInput(input);
    final File outputDirectory = createOutputDirectory();
    // outputFile is a random filename in a random directory. Each input file type creates a file by appending it's extension.
    final File outputFile = new File(outputDirectory, outputDirectory.getName());
    final File projectFile = writeInputFiles(outputFile, input, sourcesKey);

    admsRunner.run(outputDirectory, List.of(projectFile.getAbsolutePath()), input.getLicense());
    ErrorFileChecker.checkErrors(outputFile);
    ErrorFileChecker.checkWarnings(outputFile);
    final List<AeriusResultPoint> results = readOutputResults(outputFile, input.getEmissionResultKeys());

    postProcessResults(results, input.getEmissionResultKeys());
    if (!keepGeneratedFiles) {
      FileUtil.removeDirectoryRecursively(outputDirectory.toPath());
    }
    return results;
  }

  // Computes deposition from the concentration results.
  private void postProcessResults(final List<AeriusResultPoint> results, final EnumSet<EmissionResultKey> erk) {
    final Map<EmissionResultKey, EmissionResultKey> depositionKeys =
        erk.stream().filter(e -> e.getEmissionResultType() == EmissionResultType.DEPOSITION).collect(
            Collectors.toMap(Function.identity(), k -> EmissionResultKey.valueOf(k.getSubstance(), EmissionResultType.CONCENTRATION)));

    if (depositionKeys.isEmpty()) {
      return;
    }
    results.stream().forEach(r -> depositionKeys.forEach((d, c) -> r.setEmissionResult(d,
        ADMSDepositionCalculator.calculateNutrientNitrogenDeposition(d.getSubstance(), r.getEmissionResult(c)))));
  }

  private void validateInput(final ADMSInputData input) throws AeriusException {
    final List<String> errorMessages = new ArrayList<>();
    aggregateError(validator.validate(input), errorMessages);
    for (final Entry<Integer, Collection<EngineSource>> entry : input.getEmissionSources().entrySet()) {
      for (final EngineSource source : entry.getValue()) {
        aggregateError(validator.validate(source), errorMessages);
      }
    }
    if (!errorMessages.isEmpty()) {
      throw new AeriusException(AeriusExceptionReason.ADMS_INPUT_VALIDATION, String.join(", ", errorMessages));
    }
  }

  private static <C> void aggregateError(final Set<ConstraintViolation<C>> sViolations, final List<String> errorMessages) {
    if (!sViolations.isEmpty()) {
      for (final ConstraintViolation<C> violation : sViolations) {
        errorMessages.add(violation.getPropertyPath() + ": " + violation.getMessage());
      }
    }
  }

  private File createOutputDirectory() throws IOException {
    final File runDirectory = new File(runFilesDirectory, "amds-" + randomName());

    if (!runDirectory.exists() && !runDirectory.mkdirs()) {
      throw new IOException("Could not create directory " + runDirectory);
    }
    return runDirectory;
  }

  private static File writeInputFiles(final File outputFile, final ADMSInputData input, final Integer sourcesKey) throws IOException, AeriusException {
    return new AplExportWriter().write(outputFile, input, sourcesKey);
  }

  private static List<AeriusResultPoint> readOutputResults(final File outputFile, final EnumSet<EmissionResultKey> keys)
      throws IOException, AeriusException {
    final PltFileReader reader = new PltFileReader(keys);
    final LineReaderResult<AeriusResultPoint> results = reader.read(outputFile);

    if (results.getExceptions().isEmpty()) {
      return results.getObjects();
    }
    throw results.getExceptions().get(0);
  }

  private static String randomName() {
    return UUID.randomUUID().toString();
  }
}
