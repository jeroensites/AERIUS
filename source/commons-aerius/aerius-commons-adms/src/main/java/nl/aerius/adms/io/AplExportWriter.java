/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.stream.Collectors;

import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSInputData;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Writes all data to ADMS input files.
 */
public class AplExportWriter {

  /**
   * Write all input files for ADMS from the input data.
   *
   * @param outputFile generic output filename, specific input types will append their extension and write to those files
   * @param inputData input data to get input files from
   * @param sourcesKey key for the sources to take from the input data
   * @return
   * @throws IOException
   * @throws AeriusException
   */
  public File write(final File outputFile, final ADMSInputData inputData, final Integer sourcesKey) throws IOException, AeriusException {
    final AplFileContent content = new AplFileContent();
    // Write all input files.
    writeAspFile(outputFile, content, inputData);
    tempWriteMetDataFile(outputFile, content);
    return writeAplFile(outputFile, content, inputData, sourcesKey);
  }

  private static void writeAspFile(final File outputFile, final AplFileContent content, final ADMSInputData inputData) throws IOException {
    final File aspFile = AspFileWriter.fileWithExtension(outputFile);

    content.setAspFile(aspFile);
    AspFileWriter.writeFile(aspFile, inputData.getReceptors());
  }

  /**
   * For the time being write a dummy .met file next to the apl file and use that one as .met file.
   *
   * @param outputFile name of the apl output file
   * @param content content of the apl file to configure the met file in
   * @throws IOException
   */
  private static void tempWriteMetDataFile(final File outputFile, final AplFileContent content) throws IOException {
    final String tempMetFilename = "oneday.met";

    try (InputStream is = AplExportWriter.class.getResourceAsStream(tempMetFilename)) {
      final File tempMetFile = new File(outputFile.getParentFile(), tempMetFilename);

      Files.write(tempMetFile.toPath(), is.readAllBytes());
      content.setMetDataFile(tempMetFile.getAbsolutePath());
    }
  }

  /**
   * Write APL/UPL file.
   *
   * @param outputFile
   * @param content
   * @param inputData
   * @param sourcesKey
   * @return
   * @throws IOException
   * @throws AeriusException
   */
  private static File writeAplFile(final File outputFile, final AplFileContent content, final ADMSInputData inputData, final Integer sourcesKey)
      throws IOException, AeriusException {
    inputData.getSubstances().stream().forEach(content::addSubstance);
    inputData.getEmissionSources().get(sourcesKey).stream()
        .filter(ADMSSource.class::isInstance)
        .forEach(s -> content.addObject((ADMSSource<?>) s));
    content.setBuildings(inputData.getEmissionSources().get(sourcesKey).stream()
        .filter(ADMSBuilding.class::isInstance)
        .map(ADMSBuilding.class::cast)
        .collect(Collectors.toList()));
    return new AplFileWriter(inputData.getFileType()).writeFile(outputFile, content);
  }
}
