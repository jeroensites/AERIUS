/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import static nl.aerius.adms.io.AplFileConstants.ADMS_PARAMETERS_BLD_KEY;
import static nl.aerius.adms.io.AplFileConstants.ADMS_PARAMETERS_OPT_KEY;
import static nl.aerius.adms.io.AplFileConstants.ADMS_POLLUTANT_DETAILS_KEY;
import static nl.aerius.adms.io.AplFileConstants.GRD_PTS_POINTS_FILE_PATH_KEY;
import static nl.aerius.adms.io.AplFileConstants.HIL_ROUGH_PATH_KEY;
import static nl.aerius.adms.io.AplFileConstants.HIL_TERRAIN_PATH_KEY;
import static nl.aerius.adms.io.AplFileConstants.HIL_USE_ROUGH_FILE;
import static nl.aerius.adms.io.AplFileConstants.HIL_USE_TER_FILE;
import static nl.aerius.adms.io.AplFileConstants.MET_DATA_FILE_WELL_FORMED_PATH_KEY;
import static nl.aerius.adms.io.AplFileConstants.SUP_MODEL_BUILDINGS;
import static nl.aerius.adms.io.AplFileConstants.SUP_PROJECT_NAME_KEY;
import static nl.aerius.adms.io.AplFileConstants.SUP_SITE_NAME_KEY;
import static nl.aerius.adms.io.AplFileConstants.SUP_TIME_VARYING_FAC_PATH_KEY;
import static nl.aerius.adms.io.AplFileConstants.SUP_USE_TIME_VARYING_FAC;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Writes an ADMS APL or UPL file to disk.
 */
public class AplFileWriter {

  private static final String NO_EXTERNAL_FILE = "0";
  private static final String EMPTY_VALUE = " ";
  private static final String TEMPLATE_NAME = "template.upl";
  private final ADMSSourceFileType fileType;
  private final AplPollutantBuilder pollutantBuilder;

  public AplFileWriter(final ADMSSourceFileType fileType) throws IOException {
    this(fileType, new AplPollutantBuilder());
  }

  public AplFileWriter(final ADMSSourceFileType fileType, final AplPollutantBuilder pollutantBuilder) {
    this.fileType = fileType;
    this.pollutantBuilder = pollutantBuilder;
  }

  /**
   * Writes the content to the output file.
   *
   * @param outputFile file to write content in. File will be appended with file type extension
   * @param content content to store into the file
   * @return
   * @throws IOException
   * @throws AeriusException
   */
  public File writeFile(final File outputFile, final AplFileContent content) throws IOException, AeriusException {
    String template = getTemplateContent();
    // write general data
    template = insertGeneral(template, content);
    // write environment data, like meteo, terrain
    template = insertEnvironmentData(template, content);
    // write location of points file
    template = insertPointsFileLocation(template, content.getAspFile());
    // write buildings
    template = insertBuildings(template, content.getBuildings());
    // write pollutants
    template = insertPollutants(template, content.getSubstances());
    // Number of sources
    template = template.replace(AplFileConstants.SRC_NUM_SOURCES, String.valueOf(content.getObjects().size()));
    final File projectFile = fileType.fileWithExtension(outputFile);

    try (final FileWriter writer = new FileWriter(projectFile, StandardCharsets.UTF_8)) {
      // First write the template
      writer.append(template);
      // Write sources
      writeSources(writer, content.getObjects(), content.getSubstances());
    }
    return projectFile;
  }

  private String getTemplateContent() throws IOException {
    try (final InputStream is = getClass().getResourceAsStream(TEMPLATE_NAME)) {
      return new String(is.readAllBytes(), StandardCharsets.UTF_8);
    }
  }

  private static String insertGeneral(final String template, final AplFileContent content) {
    // TODO add support for site and project name inserting
    return template.replace(SUP_SITE_NAME_KEY, EMPTY_VALUE).replace(SUP_PROJECT_NAME_KEY, EMPTY_VALUE);
  }

  private static String insertEnvironmentData(final String template, final AplFileContent content) {
    // TODO add support for terrain and rough files. For now set to 0 to not use.
    return
        template
        .replace(MET_DATA_FILE_WELL_FORMED_PATH_KEY, content.getMetDataFile())
        .replace(SUP_USE_TIME_VARYING_FAC, NO_EXTERNAL_FILE).replace(SUP_TIME_VARYING_FAC_PATH_KEY, EMPTY_VALUE)
        .replace(HIL_USE_TER_FILE, NO_EXTERNAL_FILE).replace(HIL_TERRAIN_PATH_KEY, EMPTY_VALUE)
        .replace(HIL_USE_ROUGH_FILE, NO_EXTERNAL_FILE).replace(HIL_ROUGH_PATH_KEY, EMPTY_VALUE);
  }

  private static String insertPointsFileLocation(final String template, final File pointsFile) {
    return template.replace(GRD_PTS_POINTS_FILE_PATH_KEY, pointsFile.getAbsolutePath());
  }

  private static String insertBuildings(final String template, final List<ADMSBuilding> buildings) {
    return template.replace(SUP_MODEL_BUILDINGS, String.valueOf(buildings.isEmpty() ? 0 : 1))
        .replace(ADMS_PARAMETERS_BLD_KEY, AplBuildingsBuilder.build(buildings));
  }

  private String insertPollutants(final String template, final List<Substance> substances) {
    // Build &ADMS_PARAMETERS_OPT
    final String templateWithOpt = template.replace(ADMS_PARAMETERS_OPT_KEY, AplOutputPollutantsBuilder.build(substances));

    // Build &ADMS_POLLUTANT_DETAILS
    // TODO if other than default need to be added enable next line to read specific pollutant files.
    //    final String pollutants = substances.stream().map(pollutantBuilder::getPollutantBlock).collect(Collectors.joining(OSUtils.NL));
    final String pollutants  = pollutantBuilder.getUplDefault();
    return templateWithOpt
        .replace(ADMS_POLLUTANT_DETAILS_KEY, pollutants)
        .replace(AplFileConstants.POL_NUM_POLLUTANTS, String.valueOf(AplFileConstants.POL_NUM_POLLUTANTS_DEFAULT));
  }

  private void writeSources(final FileWriter writer, final List<ADMSSource<?>> sources, final List<Substance> substances)
      throws IOException, AeriusException {
    for (final ADMSSource<?> source : sources) {
      writer.append(AplSourceBuilder.build(fileType, source, substances));
    }
  }
}
