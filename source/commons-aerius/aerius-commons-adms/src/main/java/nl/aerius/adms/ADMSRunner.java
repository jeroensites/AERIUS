/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.exec.ExecuteParameters;
import nl.overheid.aerius.exec.ExecuteWrapper;
import nl.overheid.aerius.util.OSUtils;

/**
 * Runs the ADMS executable.
 */
class ADMSRunner {

  private static final Logger LOG = LoggerFactory.getLogger(ADMSRunner.class);

  public static final String ADMS_URBAN_LIC_NAME = "ADMS-Urban.lic";
  private static final String EXECUTABLE_NAME = OSUtils.isWindows() ? "ADMSUrbanModel.exe" : "ADMSUrbanModel.out";

  private final File admsRoot;
  private final String admsVersion;

  public ADMSRunner(final String admsVersion, final File admsRoot) {
    this.admsVersion = admsVersion;
    this.admsRoot = admsRoot;
  }

  public void run(final File targetDirectory, final List<String> parameters) throws Exception {
    run(targetDirectory, parameters, null);
  }

  /**
   * Runs the ADMS executable in the target directory with the given parameters.
   *
   * @param targetDirectory directory to run from
   * @param parameters parameters to pass
   * @param license optional pass the license
   * @return returns the directory where the results are stored.
   * @throws Exception exception thrown in case of error during run
   */
  public File run(final File targetDirectory, final List<String> parameters, final byte[] license) throws Exception {
    final String executablePath;
    if (license == null) {
      executablePath = admsRoot.getAbsolutePath();
    } else {
      copyADMS(targetDirectory, license);
      executablePath = targetDirectory.getAbsolutePath();
    }
    try {
      final ExecuteParameters executeParameters = new ExecuteParameters(EXECUTABLE_NAME, parameters.toArray(new String[parameters.size()]));
      final ADMSStreamGobblers streamGobblers = new ADMSStreamGobblers(admsVersion);
      final ExecuteWrapper wrapper = new ExecuteWrapper(executeParameters, executablePath, streamGobblers);

      wrapper.run(targetDirectory.getName(), targetDirectory);
      if (!streamGobblers.getExceptions().isEmpty()) {
        // Throw first found error.
        throw streamGobblers.getExceptions().get(0);
      }
    } finally {
      if (license != null) {
        clearADMS(targetDirectory);
      }
    }
    return targetDirectory;
  }

  private void copyADMS(final File targetDirectory, final byte[] license) throws IOException {
    // Safety check to only delete files if in a temporary directory and not in the same directory as the configured root
    if (!admsRoot.getAbsolutePath().equals(targetDirectory.getAbsolutePath())) {
      Files.copy(Path.of(admsRoot.getAbsolutePath(), EXECUTABLE_NAME), Path.of(targetDirectory.getAbsolutePath(), EXECUTABLE_NAME));
      final File outputFile = new File(targetDirectory, ADMS_URBAN_LIC_NAME);

      try (FileOutputStream outputStream = new FileOutputStream(outputFile)) {
        outputStream.write(license);
      }
    }
  }

  private void clearADMS(final File targetDirectory) {
    // Safety check to only delete files if in a temporary directory and not in the same directory as the configured root
    if (!admsRoot.getAbsolutePath().equals(targetDirectory.getAbsolutePath())) {
      if (!new File(targetDirectory, ADMS_URBAN_LIC_NAME).delete()) {
        LOG.warn("Could not delete ADMS license file from {}", targetDirectory);
      }
      if (!new File(targetDirectory, EXECUTABLE_NAME).delete()) {
        LOG.warn("Could not delete ADMS executable from {}", targetDirectory);
      }
    }
  }
}
