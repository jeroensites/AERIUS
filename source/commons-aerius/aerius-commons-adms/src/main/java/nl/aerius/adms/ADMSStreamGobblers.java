/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.aerius.adms.exception.ADMSInvalidLicense;
import nl.aerius.adms.exception.ADMSInvalidVersionException;
import nl.overheid.aerius.exec.ExecuteWrapper.StreamGobblers;
import nl.overheid.aerius.exec.StreamGobbler;

/**
 * Class to check if the ADMS version matches and if the license is valid.
 */
class ADMSStreamGobblers implements StreamGobblers {

  private static final Pattern VERSION_PATTERN = Pattern.compile("Version ([\\d\\.]+)");
  private static final String INVALID_LICENSE_STRING = " ERROR  : Your licence is invalid. This program will terminate.";
  private static final String ERROR_LINE = " Error ";

  private final List<Exception> exceptions = new ArrayList<>();
  private final String admsVersion;

  public ADMSStreamGobblers(final String admsVersion) {
    this.admsVersion = admsVersion;
  }

  @Override
  public StreamGobbler errorStreamGobbler(final String type, final String parentId) {
    return new StreamGobbler(type, parentId, ADMSStreamGobblers::ignoreAllLines);
  }

  @Override
  public StreamGobbler outputStreamGobbler(final String type, final String parentId) {
    return new StreamGobbler(type, parentId, this::logErrors);
  }

  public List<Exception> getExceptions() {
    return exceptions;
  }

  private static Boolean ignoreAllLines(final String line) {
    return false;
  }

  private Boolean logErrors(final String line) {
    final Matcher matcher = VERSION_PATTERN.matcher(line);
    if (matcher.find() && !admsVersion.equals(matcher.group(1))) {
      exceptions.add(new ADMSInvalidVersionException(matcher.group(1)));
      return true;
    }
    if (INVALID_LICENSE_STRING.equals(line)) {
      exceptions.add(new ADMSInvalidLicense(line));
      return true;
    }
    // log any line that starts with Error
    return line.startsWith(ERROR_LINE);
  }
}