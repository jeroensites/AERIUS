/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.domain;

import nl.aerius.adms.io.ADMSSourceFileType;
import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * Input file for ADMS calculation.
 */
public class ADMSInputData extends EngineInputData<EngineSource, AeriusPoint> {

  private static final long serialVersionUID = 2L;

  private ADMSSourceFileType fileType;
  /**
   * Optional provide an ADMS license file.
   */
  private byte[] license;

  private String metDataFile;

  public ADMSInputData() {
    super(Theme.NCA);
  }

  public ADMSSourceFileType getFileType() {
    return fileType;
  }

  public void setFileType(final ADMSSourceFileType fileType) {
    this.fileType = fileType;
  }

  public byte[] getLicense() {
    return license;
  }

  public void setLicense(final byte[] license) {
    this.license = license;
  }

  public String getMetDataFile() {
    return metDataFile;
  }

  public void setMetDataFile(final String metDataFile) {
    this.metDataFile = metDataFile;
  }
}
