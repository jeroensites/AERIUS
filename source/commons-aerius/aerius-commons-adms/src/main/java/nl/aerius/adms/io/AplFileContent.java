/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.io;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import nl.aerius.adms.domain.ADMSBuilding;
import nl.aerius.adms.domain.ADMSSource;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;

/**
 * Object to store the input/output of an ADMS APL/UPL file.
 */
class AplFileContent extends LineReaderResult<ADMSSource<?>> {

  private final List<Substance> substances = new ArrayList<>();
  private List<ADMSBuilding> buildings = new ArrayList<>();
  private File aspFile;
  private String metDataFile;

  public List<Substance> getSubstances() {
    return substances;
  }

  public void addSubstance(final Substance substance) {
    substances.add(substance);
  }

  public List<ADMSBuilding> getBuildings() {
    return buildings;
  }

  public void addBuilding(final ADMSBuilding building) {
    buildings.add(building);
  }

  public void setBuildings(final List<ADMSBuilding> buildings) {
    this.buildings = buildings;
  }

  public File getAspFile() {
    return aspFile;
  }

  public void setAspFile(final File aspFile) {
    this.aspFile = aspFile;
  }

  public String getMetDataFile() {
    return metDataFile;
  }

  public void setMetDataFile(final String metDataFile) {
    this.metDataFile = metDataFile;
  }
}
