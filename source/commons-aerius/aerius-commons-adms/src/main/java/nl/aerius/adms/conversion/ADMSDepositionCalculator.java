/*
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.aerius.adms.conversion;

import java.util.Map;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * Calculator to calculate deposition from concentration.
 */
public final class ADMSDepositionCalculator {

  // Deposition velocity factors grassland in m/s
  private static final double DEPOSITION_VELOCITY_GRASSLAND_NO2 = 0.0015;
  private static final double DEPOSITION_VELOCITY_GRASSLAND_NH3 = 0.02;
  private static final Map<Substance, Double> DEPOSITION_VELOCITY_GRASSLAND_MAP = Map.of(
      Substance.NO2, DEPOSITION_VELOCITY_GRASSLAND_NO2,
      Substance.NH3, DEPOSITION_VELOCITY_GRASSLAND_NH3);

  // Deposition velocity factors woodland in m/s
  private static final double DEPOSITION_VELOCITY_WOODLAND_NO2 = 0.003;
  private static final double DEPOSITION_VELOCITY_WOODLAND_NH3 = 0.03;
  private static final Map<Substance, Double> DEPOSITION_VELOCITY_WOODLAND_MAP = Map.of(
      Substance.NO2, DEPOSITION_VELOCITY_WOODLAND_NO2,
      Substance.NH3, DEPOSITION_VELOCITY_WOODLAND_NH3);

  // Conversion factor for eutrophication (final nitrogen deposition) in (µg/m3 to kg N/ha/yr)
  private static final double EUTROPHICATION_FACTOR_NO2 = 95.9;
  private static final double EUTROPHICATION_FACTOR_NH3 = 260;
  private static final Map<Substance, Double> EUTROPHICATION_FACTOR_MAP = Map.of(
      Substance.NO2, EUTROPHICATION_FACTOR_NO2,
      Substance.NH3, EUTROPHICATION_FACTOR_NH3);

  // Conversion factor for acidification (acid deposition) (µg/m3 to keq/ha/yr)
  private static final double ACIDIFICATION_FACTOR_NO2 = 6.84;
  private static final double ACIDIFICATION_FACTOR_NH3 = 18.5;
  private static final Map<Substance, Double> ACIDIFICATION_FACTOR_MAP = Map.of(
      Substance.NO2, ACIDIFICATION_FACTOR_NO2,
      Substance.NH3, ACIDIFICATION_FACTOR_NH3);

  private ADMSDepositionCalculator() {
    // Util class
  }

  /**
   * Calculates nutrient nitrogen deposition
   *
   * @param substance substance
   * @param concentration concentration in ug/m3
   * @return nutrient nitrogen deposition
   */
  public static final double calculateNutrientNitrogenDeposition(final Substance substance, final double concentration) {
    return compute(concentration, DEPOSITION_VELOCITY_WOODLAND_MAP, EUTROPHICATION_FACTOR_MAP, substance);
  }

  /**
   * Calculates acid deposition
   *
   * @param substance substance
   * @param concentration concentration in ug/m3
   * @return acid deposition
   */
  public static final double calculateAcidDeposition(final Substance substance, final double concentration) {
    return compute(concentration, DEPOSITION_VELOCITY_WOODLAND_MAP, ACIDIFICATION_FACTOR_MAP, substance);
  }

  private static double compute(final double concentration, final Map<Substance, Double> depositionVelocityMap,
      final Map<Substance, Double> depositionFactorMap, final Substance substance) {
    final Substance substanceToUse = substance == Substance.NOX ? Substance.NO2 : substance;

    if (!depositionVelocityMap.containsKey(substanceToUse)) {
      return 0.0;
    } else {
      return concentration * depositionVelocityMap.get(substanceToUse) * depositionFactorMap.get(substanceToUse);
    }
  }
}
