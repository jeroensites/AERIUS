/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.receptor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;

/**
 * Test class for {@link SubReceptorUtil}.
 */
class SubReceptorUtilTest {

  private static final HexagonZoomLevel ZOOM_LEVEL_1 = new HexagonZoomLevel(1, 10000);
  private static final double RADIUS = ZOOM_LEVEL_1.getHexagonRadius();
  private static final double HEIGHT = ZOOM_LEVEL_1.getHexagonHeight() / 2;

  static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> coords = new ArrayList<>();
    coords.add(new Object[] {0.0, 0.0});
    coords.add(new Object[] {10000.0, 20000.0});
    coords.add(new Object[] {-10000.0, 20000.0});
    coords.add(new Object[] {-10000.0, -20000.0});
    coords.add(new Object[] {10000.0, -20000.0});
    return coords;
  }

  private static final SubReceptorUtil<AeriusPoint> TEST_SUB_RECEPTOR_UTIL = new SubReceptorUtil<AeriusPoint>(ZOOM_LEVEL_1) {

    @Override
    public AeriusPoint newPoint(final AeriusPoint original) {
      return original.copy();
    }
  };

  @ParameterizedTest
  @MethodSource("data")
  void testDetermineSubPointsRing2(final double xCoord, final double yCoord) {
    final AeriusPoint originalPoint = new AeriusPoint(1, AeriusPointType.POINT, xCoord, yCoord);
    final List<AeriusPoint> subPoints = TEST_SUB_RECEPTOR_UTIL.determineSubPoints(originalPoint, 2);
    assertEquals(19, subPoints.size(), "Number of subpoints");
    final List<AeriusPoint> expectedPoints = getExpectedPointsRing2(originalPoint);

    validateResults(subPoints, expectedPoints);
  }

  @ParameterizedTest
  @MethodSource("data")
  void testDetermineSubPointsRing0(final double xCoord, final double yCoord) {
    final AeriusPoint originalPoint = new AeriusPoint(1, AeriusPointType.POINT, xCoord, yCoord);
    final List<AeriusPoint> subPoints = TEST_SUB_RECEPTOR_UTIL.determineSubPoints(originalPoint, 0);
    assertEquals(1, subPoints.size(), "Number of subpoints");
    final List<AeriusPoint> expectedPoints = new ArrayList<AeriusPoint>();
    expectedPoints.add(originalPoint);

    validateResults(subPoints, expectedPoints);
  }

  @ParameterizedTest
  @MethodSource("data")
  void testDetermineSubPointsRing1(final double xCoord, final double yCoord) {
    final AeriusPoint originalPoint = new AeriusPoint(1, AeriusPointType.POINT, xCoord, yCoord);
    final List<AeriusPoint> subPoints = TEST_SUB_RECEPTOR_UTIL.determineSubPoints(originalPoint, 1);
    assertEquals(7, subPoints.size(), "Number of subpoints");
    final List<AeriusPoint> expectedPoints = getExpectedPointsRing1(originalPoint);

    validateResults(subPoints, expectedPoints);
  }

  private void validateResults(final List<AeriusPoint> subPoints, final List<AeriusPoint> expectedPoints) {
    assertEquals(expectedPoints.size(), subPoints.size(), "Number of subpoints");
    final PointComparator comparator = new PointComparator();
    Collections.sort(subPoints, comparator);
    Collections.sort(expectedPoints, comparator);
    for (int i = 0; i < subPoints.size(); i++) {
      final AeriusPoint point = subPoints.get(i);
      final AeriusPoint expectedPoint = expectedPoints.get(i);
      assertEquals(expectedPoint.getX(), point.getX(), 1E-4, "X coordinate point " + point + " at index " + i);
      assertEquals(expectedPoint.getY(), point.getY(), 1E-4, "Y coordinate point " + point + " at index " + i);
    }
  }

  /**
   * @param original The point to base the list on.
   * @return The subpoints, based on this wonderful piece of ASCII-art.
   *
   * <pre>
   *   6   1
   *
   * 5   0   2
   *
   *   4   3
   *  </pre>
   */
  private List<AeriusPoint> getExpectedPointsRing1(final AeriusPoint originalPoint) {
    final List<AeriusPoint> subPoints = new ArrayList<>();
    //0
    subPoints.add(originalPoint);
    //2 and 5
    subPoints.add(getExpectedPoint(originalPoint, RADIUS, 0.0));
    subPoints.add(getExpectedPoint(originalPoint, -RADIUS, 0.0));
    //1, 3, 4 and 6
    subPoints.add(getExpectedPoint(originalPoint, RADIUS / 2, HEIGHT));
    subPoints.add(getExpectedPoint(originalPoint, RADIUS / 2, -HEIGHT));
    subPoints.add(getExpectedPoint(originalPoint, -RADIUS / 2, -HEIGHT));
    subPoints.add(getExpectedPoint(originalPoint, -RADIUS / 2, HEIGHT));
    return subPoints;
  }

  /**
   * @param original The point to base the list on.
   * @return The subpoints, based on this wonderful piece of ASCII-art.
   *
   * <pre>
   *     17   18   07
   *
   *   16   06   01   08
   *
   * 15   05   00   02   09
   *
   *   14   04   03   10
   *
   *     13   12   11
   *  </pre>
   */
  private List<AeriusPoint> getExpectedPointsRing2(final AeriusPoint originalPoint) {
    //00, 07, 09, 11, 13, 15 and 17 are in ring 1.
    final List<AeriusPoint> subPoints = getExpectedPointsRing1(originalPoint);
    //02 and 05
    subPoints.add(getExpectedPoint(originalPoint, RADIUS / 2, 0.0));
    subPoints.add(getExpectedPoint(originalPoint, -RADIUS / 2, 0.0));
    //01, 03, 04 and 06
    subPoints.add(getExpectedPoint(originalPoint, RADIUS / 4, HEIGHT / 2));
    subPoints.add(getExpectedPoint(originalPoint, RADIUS / 4, -HEIGHT / 2));
    subPoints.add(getExpectedPoint(originalPoint, -RADIUS / 4, -HEIGHT / 2));
    subPoints.add(getExpectedPoint(originalPoint, -RADIUS / 4, HEIGHT / 2));
    //12 and 18
    subPoints.add(getExpectedPoint(originalPoint, 0.0, -HEIGHT));
    subPoints.add(getExpectedPoint(originalPoint, 0.0, HEIGHT));
    //08, 10, 14 and 16
    subPoints.add(getExpectedPoint(originalPoint, RADIUS * 3 / 4, HEIGHT / 2));
    subPoints.add(getExpectedPoint(originalPoint, RADIUS * 3 / 4, -HEIGHT / 2));
    subPoints.add(getExpectedPoint(originalPoint, -RADIUS * 3 / 4, -HEIGHT / 2));
    subPoints.add(getExpectedPoint(originalPoint, -RADIUS * 3 / 4, HEIGHT / 2));
    return subPoints;
  }

  private AeriusPoint getExpectedPoint(final AeriusPoint originalPoint, final double displacedX, final double displacedY) {
    final AeriusPoint copy = originalPoint.copy();
    copy.setX(originalPoint.getX() + displacedX);
    copy.setY(originalPoint.getY() + displacedY);
    return copy;
  }

  private class PointComparator implements Comparator<AeriusPoint> {

    @Override
    public int compare(final AeriusPoint o1, final AeriusPoint o2) {
      int compared = Double.compare(o1.getX(), o2.getX());
      if (compared == 0) {
        compared = Double.compare(o1.getY(), o2.getY());
      }
      return compared;
    }

  }

}
