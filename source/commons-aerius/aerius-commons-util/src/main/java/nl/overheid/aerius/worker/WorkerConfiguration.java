/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.exception.InvalidInputException;
import nl.overheid.aerius.util.EnvUtils;

/**
 * Contains the properties that can be used by different WorkerConfinguration classes, so they don't have to keep track of it.
 * It's also possible to configure a property as environment variable. If the properties is prefixed with 'aerius.' and it exists and environment
 * variable the value of the environment variable overrides the property in the file. In such a case it's also possible to ommit the property in the
 * configuration file.
 */
public abstract class WorkerConfiguration {

  public static final int AUTO_PROCESSES = -1;

  protected static final String PROCESSES = "processes";

  private static final String AUTO = "auto";
  private static final Logger LOG = LoggerFactory.getLogger(WorkerConfiguration.class);

  private final Properties properties;
  private final String keyPrefix;

  /**
   * Save the properties. The keyPrefix is prepended to every getProperty call.
   * @param properties Properties containing the predefined properties.
   * @param keyPrefix property prefix
   */
  public WorkerConfiguration(final Properties properties, final String keyPrefix) {
    this.properties = properties;
    this.keyPrefix = keyPrefix == null || keyPrefix.isEmpty() ? "" : (keyPrefix + '.');
  }

  /**
   * Validate the configuration.
   * @throws InvalidInputException on validation errors
   */
  public void validate() throws InvalidInputException {
    final List<String> validationErrors = getValidationErrors();
    if (!validationErrors.isEmpty()) {
      throw new InvalidInputException(validationErrors);
    }
  }

  /**
   * Returns the number of worker processes that should be spawned.
   * @return
   */
  public int getProcesses() {
    return getProcesses(PROCESSES);
  }

  protected int getProcesses(final String key) {
    return AUTO.equals(getProperty(key)) ? AUTO_PROCESSES : getPropertyIntSafe(key);
  }

  /**
   * Get validation errors if there are any. If the set is null or empty, it indicates that all validations have passed.
   * @return List of validationErrors
   */
  protected abstract List<String> getValidationErrors();

  /**
   * Validate if the property key isn't empty and is a valid directory and if writable is true it checks if the directory is writable. If any of these
   * conditions are not met this is stored in the reasons list.
   *
   * @param key property key to check
   * @param reasons in case or errors add the errors to this list
   * @param writable checks if the directory is writable by this process.
   */
  protected boolean validateDirectoryProperty(final String key, final List<String> reasons, final boolean writable) {
    return validateRequiredProperty(key, reasons) &&
      validateDirectoryProperty(key, new File(getProperty(key)), reasons, writable);
  }

  /**
   * Validate if the property key isn't empty and is a valid directory and if writable is true it checks if the directory is writable. If any of these
   * conditions are not met this is stored in the reasons list.
   *
   * @param key property key to check
   * @param directory The directory derived from the key property.
   * @param reasons in case or errors add the errors to this list
   * @param writable checks if the directory is writable by this process.
   * @return returns true if directory property validates
   */
  protected boolean validateDirectoryProperty(final String key, final File directory, final List<String> reasons, final boolean writable) {
    boolean validated = false;

    if (validateRequiredProperty(key, reasons)) {
      if (directory.isDirectory()) {
        if (writable && !directory.canWrite()) {
          reasons.add("Property '" + getFullPropertyName(key) + "' not writable: " + directory);
        } else {
          validated = true;
        }
      } else {
        reasons.add("Property '" + getFullPropertyName(key) + "' is not a valid directory: " + directory);
      }
    }
    return validated;
  }

  /**
   * Validate if the property key isn't empty and is a valid file.
   * @param key property key to check
   * @param reasons in case or errors add the errors to this list
   * @param requiredExtension if set check whether file set has the required extension. Use <code>null</code> if you want to skip the check.
   *  e.g. <code>".csv"</code>.
   */
  protected void validateFilepathProperty(final String key, final List<String> reasons, final String requiredExtension) {
    if (validateRequiredProperty(key, reasons)) {
      final String value = getProperty(key);
      final File file = new File(value);
      if (file.isFile()) {
        if (requiredExtension != null && !value.endsWith(requiredExtension)) {
          reasons.add("Property '" + getFullPropertyName(key) + "' does not end with the required extension '" + requiredExtension + "': " + file);
        }
      } else {
        reasons.add("Property '" + getFullPropertyName(key) + "' is not a valid filepath: " + file);
      }
    }
  }

  /**
   * Validate if the required property is set. If set the method returns true
   * @param key key to validate
   * @param reasons add error to list of reasons in case the property isn't set
   * @return true if property is set
   */
  protected boolean validateRequiredProperty(final String key, final List<String> reasons) {
    final boolean empty = StringUtils.isEmpty(getProperty(key));
    if (empty) {
      reasons.add("Required property '" + getFullPropertyName(key) + "' is not set.");
    }
    return !empty;
  }

  protected String getProperty(final String key) {
    final String fullPropertyName = getFullPropertyName(key);
    final String prop = System.getProperty(fullPropertyName);

    if (prop == null) {
      final String env = EnvUtils.getEnvProperty(fullPropertyName);

      if (env == null) {
        return properties.getProperty(fullPropertyName);
      } else {
        LOG.info("Reading property {} from environment variable", fullPropertyName);
        return env;
      }
    } else {
      LOG.info("Reading property {} from property variable", fullPropertyName);
      return prop;
    }
  }

  protected Optional<String> getPropertyOptional(final String key) {
    return Optional.ofNullable(getProperty(key));
  }

  /**
   * Returns the full name of the property including the prefix.
   * @param key name of the property to get the full name
   * @return full property name
   */
  protected String getFullPropertyName(final String key) {
    return keyPrefix + key;
  }

  protected Integer getPropertyInt(final String key) {
    final String value = getProperty(key);
    return value == null ? null : Integer.valueOf(value);
  }

  protected Optional<Integer> getPropertyIntOptional(final String key) {
    return Optional.ofNullable(getPropertyInt(key));
  }

  protected int getPropertyIntSafe(final String key) {
    final String value = getProperty(key);
    return value == null ? 0 : Integer.parseInt(value);
  }

  protected Boolean getPropertyBoolean(final String key) {
    final String value = getProperty(key);
    return value == null ? null : Boolean.valueOf(value);
  }

  /**
   * Returns true if and only if property is true. If not found returns false.
   * @param key key to check
   * @return true if set or false if not found or set to false
   */
  protected boolean getPropertyBooleanSafe(final String key) {
    return Boolean.parseBoolean(getProperty(key));
  }
}
