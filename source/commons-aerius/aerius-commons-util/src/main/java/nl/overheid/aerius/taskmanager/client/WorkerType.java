/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client;

import nl.aerius.taskmanager.client.WorkerQueueType;

/**
 * Contains the names of the group of queues.
 */
public enum WorkerType {

  /**
   * Worker type for processing calculator/connect database actions.
   */
  CONNECT,
  /**
   * CERC ADMS calculation engine.
   */
  ADMS,
  /**
   * AERIUS SRM2 calculation engine.
   */
  ASRM,
  /**
   * OPS calculation engine.
   */
  OPS,
  /**
   * Worker type for processing messages (to be send to users for instance).
   */
  MESSAGE,

  /**
   * Test worker type. doesn't have it's own queue, but can be used in mockup testing.
   */
  TEST;

  private final WorkerQueueType type = new WorkerQueueType(name());

  public WorkerQueueType type() {
    return type;
  }
}
