/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.aerius.taskmanager.client.WorkerQueueType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;

/**
 * Abstract class handling the worker call. This class wraps the actual call in the
 * {@link #handleWorkLoad(Serializable,WorkerIntermediateResultSender, String)} and takes care of a number of worker specific error handling.
 * Subclasses should implement the actual handling of the data in {@link #run(Serializable, WorkerIntermediateResultSender, String)} which is typed.
 * @param <S> The input class
 * @param <T> The output class
 */
public final class WorkerHandlerImpl<S extends Serializable, T extends Serializable> implements WorkerHandler {

  private static final Logger LOG = LoggerFactory.getLogger(WorkerHandlerImpl.class);
  private final Worker<S, T> worker;
  private final WorkerQueueType workerType;

  public WorkerHandlerImpl(final Worker<S, T> worker, final WorkerQueueType workerType) {
    this.worker = worker;
    this.workerType = workerType;
  }

  @SuppressWarnings("unchecked")
  @Override
  public Serializable handleWorkLoad(final Serializable input, final WorkerIntermediateResultSender resultSender,
      final String correlationId) throws Exception {
    final JobIdentifier jobIdentifier = new JobIdentifier(correlationId);
    WorkerThreadUtil.setThreadName("Worker", workerType.propertyName(), jobIdentifier.getLocalId());
    Serializable result;
    try {
      LOG.info("Starting: {}", jobIdentifier);
      result = worker.run((S) input, resultSender, jobIdentifier);
    } catch (final AeriusException e) {
      LOG.error("AeriusException worker: ", e);
      result = e;
    } catch (final ClassCastException e) { // before catching Runtime as this is a RuntimeException, but handled as fatal error.
      LOG.error("Could not cast input to worker specific expected datatype", e);
      throw e;
    } catch (final RuntimeException e) {
      LOG.error("RuntimeException worker: ", e);
      result = new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    WorkerThreadUtil.setThreadName("Worker", workerType.propertyName(), "IDLE");
    return result;
  }

}
