/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.List;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.ops.OPSCustomCalculationPoint;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.NSLCalculationPoint;

/**
 * Utility class to help with converting from CalculationPointFeature (json serializable, new domain objects)
 * to AeriusPoint (points used in EngineInputData, old domain objects).
 *
 * Might be temporary, but a lot depends on the AeriusPoint class, hence it's easier to do this conversion before calculating.
 */
public final class FeaturePointToEnginePointUtil {

  private FeaturePointToEnginePointUtil() {
    // util class
  }

  public static List<AeriusPoint> convert(final List<CalculationPointFeature> features) {
    final List<AeriusPoint> points = features.stream().map(FeaturePointToEnginePointUtil::convert).collect(Collectors.toList());
    if (points.stream().allMatch(point -> point.getId() == 0)) {
      int index = 0;
      for (final AeriusPoint point : points) {
        point.setId(index++);
      }
    }
    return points;
  }

  public static AeriusPoint convert(final CalculationPointFeature feature) {
    final Point point = feature.getGeometry();
    final AeriusPoint returnPoint = determineType(feature.getProperties());
    returnPoint.setId(feature.getProperties().getId());
    returnPoint.setX(point.getX());
    returnPoint.setY(point.getY());
    return returnPoint;
  }

  private static AeriusPoint determineType(final CalculationPoint calculationPoint) {
    AeriusPoint returnPoint;
    if (calculationPoint instanceof NSLCalculationPoint) {
      returnPoint = toNSLPoint((NSLCalculationPoint) calculationPoint);
    } else if (calculationPoint instanceof OPSCustomCalculationPoint) {
      returnPoint = toReceptorPoint((OPSCustomCalculationPoint) calculationPoint);
    } else {
      returnPoint = new AeriusPoint();
    }
    // At the moment these points are always custom calculation points, so set appropriate point type
    returnPoint.setPointType(AeriusPointType.POINT);
    returnPoint.setGmlId(calculationPoint.getGmlId());
    returnPoint.setLabel(calculationPoint.getLabel());
    return returnPoint;
  }

  private static AeriusPoint toNSLPoint(final NSLCalculationPoint point) {
    final nl.overheid.aerius.shared.domain.nsl.NSLCalculationPoint returnPoint = new nl.overheid.aerius.shared.domain.nsl.NSLCalculationPoint();
    returnPoint.setHeight(point.getHeight());
    return returnPoint;
  }

  private static AeriusPoint toReceptorPoint(final OPSCustomCalculationPoint point) {
    final OPSReceptor receptor = new OPSReceptor();
    receptor.setHeight(point.getHeight());
    receptor.setAverageRoughness(point.getAverageRoughness());
    receptor.setLandUse(point.getLandUse());
    receptor.setLandUses(point.getLandUses());
    return receptor;
  }

}
