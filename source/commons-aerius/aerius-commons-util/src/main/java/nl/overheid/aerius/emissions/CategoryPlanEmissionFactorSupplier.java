/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.emissions;

import java.util.EnumMap;
import java.util.Map;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory.CategoryUnit;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.emissions.PlanEmissionFactorSupplier;

class CategoryPlanEmissionFactorSupplier implements PlanEmissionFactorSupplier {

  private final SectorCategories categories;

  CategoryPlanEmissionFactorSupplier(final SectorCategories categories) {
    this.categories = categories;
  }

  @Override
  public boolean isEmissionFactorPerUnits(final String planCode) {
    return plan(planCode).getCategoryUnit() != CategoryUnit.NO_UNIT;
  }

  @Override
  public Map<Substance, Double> getPlanEmissionFactors(final String planCode) {
    final Map<Substance, Double> emissionFactors = new EnumMap<>(Substance.class);
    final PlanCategory category = plan(planCode);
    for (final Substance substance : Substance.values()) {
      final double emissionFactor = category.getEmissionFactor(substance);
      if (emissionFactor > 0) {
        emissionFactors.put(substance, emissionFactor);
      }
    }
    return emissionFactors;
  }

  private PlanCategory plan(final String planCode) {
    return categories.determinePlanEmissionCategoryByCode(planCode);
  }

}
