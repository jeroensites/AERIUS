/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.nio.file.Paths;

/**
 *
 */
public final class WorkerUtil {

  private static final String FOLDER_NAME = "aerius_jobs";

  private WorkerUtil() {
    // Util class
  }

  public static File getFolderForJob(final JobIdentifier jobIdentifier) {
    final File folderForJob = getFolderForJobLocation(jobIdentifier.getLocalId());
    if (folderForJob.exists()) {
      // folder was already created earlier on.
    } else if (!folderForJob.mkdirs()) {
      throw new IllegalArgumentException("Could not create temp directories needed to write job files to: " + folderForJob);
    }
    return folderForJob;
  }

  private static File getFolderForJobLocation(final String correlationId) {
    return Paths.get(System.getProperty("java.io.tmpdir"), FOLDER_NAME, correlationId).toFile();
  }

}
