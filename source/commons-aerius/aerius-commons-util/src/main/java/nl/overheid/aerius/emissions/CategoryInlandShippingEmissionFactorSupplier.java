/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.emissions;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.InlandCategoryEmissionFactorKey;
import nl.overheid.aerius.db.common.sector.ShippingRepository;
import nl.overheid.aerius.db.common.sector.category.ShippingCategoryRepository;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.shared.emissions.InlandShippingEmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.shipping.InlandShippingRouteEmissionPoint;
import nl.overheid.aerius.shared.emissions.shipping.ShippingLaden;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

class CategoryInlandShippingEmissionFactorSupplier implements InlandShippingEmissionFactorSupplier {

  private static final Logger LOG = LoggerFactory.getLogger(CategoryInlandShippingEmissionFactorSupplier.class);

  private final Map<RouteKey, Map<Substance, Double>> cacheRouteEmissionFactors = new HashMap<>();
  private final Map<String, Map<Substance, Double>> cacheDockedEmissionFactors = new HashMap<>();

  private final SectorCategories categories;
  private final PMF pmf;
  private final int year;

  CategoryInlandShippingEmissionFactorSupplier(final SectorCategories categories, final PMF pmf, final int year) {
    this.categories = categories;
    this.pmf = pmf;
    this.year = year;
  }

  @Override
  public List<InlandShippingRouteEmissionPoint> determineInlandRoutePoints(final Geometry geometry, final Optional<InlandWaterway> waterway)
      throws AeriusException {
    final List<InlandShippingRouteEmissionPoint> points = new ArrayList<>();
    try (final Connection con = pmf.getConnection()) {
      points.addAll(ShippingRepository.getInlandShippingRoutePoints(con, geometry, waterway));
    } catch (final SQLException e) {
      LOG.error("Error determining inland route points", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    return points;
  }

  @Override
  public Map<Substance, Double> getInlandShippingRouteEmissionFactors(final String waterwayCode, final WaterwayDirection direction,
      final ShippingLaden laden, final String shipCode) throws AeriusException {
    final RouteKey routeKey = new RouteKey(waterwayCode, direction, laden, shipCode);
    if (!cacheRouteEmissionFactors.containsKey(routeKey)) {
      cacheRouteEmissionFactors.put(routeKey, getRouteEmissionFactorsFromDatabase(routeKey));
    }
    return cacheRouteEmissionFactors.get(routeKey);
  }

  private Map<Substance, Double> getRouteEmissionFactorsFromDatabase(final RouteKey routeKey) throws AeriusException {
    final Map<Substance, Double> factors = new EnumMap<>(Substance.class);
    final InlandShippingCategory ship = shipCategory(routeKey.shipCode);
    final InlandWaterwayCategory waterway = waterwayCategory(routeKey.waterwayCode);
    try (final Connection con = pmf.getConnection()) {
      final Map<InlandCategoryEmissionFactorKey, Double> databaseFactors = ShippingCategoryRepository.findInlandCategoryRouteEmissionFactors(con,
          ship, year);
      for (final Entry<InlandCategoryEmissionFactorKey, Double> entry : databaseFactors.entrySet()) {
        final InlandCategoryEmissionFactorKey key = entry.getKey();
        if (key.getDirection() == routeKey.direction
            && key.getWaterwayId() == waterway.getId()
            && (key.isLaden() == (routeKey.laden == ShippingLaden.LADEN))) {
          factors.put(key.getSubstance(), entry.getValue());
        }
      }
    } catch (final SQLException e) {
      LOG.error("Error determining inland route emission factors", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
    return factors;
  }

  @Override
  public Map<Substance, Double> getInlandShippingDockedEmissionFactors(final String shipCode) throws AeriusException {
    if (!cacheDockedEmissionFactors.containsKey(shipCode)) {
      cacheDockedEmissionFactors.put(shipCode, getDockedEmissionFactorsFromDatabase(shipCode));
    }
    return cacheDockedEmissionFactors.get(shipCode);
  }

  private Map<Substance, Double> getDockedEmissionFactorsFromDatabase(final String shipCode) throws AeriusException {
    final InlandShippingCategory ship = shipCategory(shipCode);
    try (final Connection con = pmf.getConnection()) {
      return ShippingCategoryRepository.findInlandCategoryMooringEmissionFactors(con, ship, year);
    } catch (final SQLException e) {
      LOG.error("Error determining inland docked emission factors", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  private InlandShippingCategory shipCategory(final String shipCode) {
    return categories.getInlandShippingCategories().getShipCategoryByCode(shipCode);
  }

  private InlandWaterwayCategory waterwayCategory(final String waterwayCode) {
    return categories.getInlandShippingCategories().getWaterwayCategoryByCode(waterwayCode);
  }

  private static class RouteKey {
    private final String waterwayCode;
    private final WaterwayDirection direction;
    private final ShippingLaden laden;
    private final String shipCode;

    private RouteKey(final String waterwayCode, final WaterwayDirection direction, final ShippingLaden laden, final String shipCode) {
      this.waterwayCode = waterwayCode;
      this.direction = direction;
      this.laden = laden;
      this.shipCode = shipCode;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((direction == null) ? 0 : direction.hashCode());
      result = prime * result + ((laden == null) ? 0 : laden.hashCode());
      result = prime * result + ((shipCode == null) ? 0 : shipCode.hashCode());
      result = prime * result + ((waterwayCode == null) ? 0 : waterwayCode.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final RouteKey other = (RouteKey) obj;
      return direction == other.direction
          && laden == other.laden
          && ((shipCode == null && other.shipCode == null) || (shipCode != null && shipCode.equals(other.shipCode)))
          && ((waterwayCode == null && other.waterwayCode == null) || (waterwayCode != null && waterwayCode.equals(other.waterwayCode)));
    }

  }

}
