/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service.export;

import java.io.IOException;

import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 *
 */
public class ExportTaskClientBean {

  private final TaskManagerClient client;
  private final WorkerType workerType;

  public ExportTaskClientBean(final TaskManagerClient client, final WorkerType workerType) {
    this.client = client;
    this.workerType = workerType;
  }

  public void startExport(final QueueEnum queueName, final ExportProperties exportProperties, final Scenario scenario,
      final String taskId) throws IOException {
    ExportTaskClient.startExport(client.getClient(), workerType, queueName.getQueueName(), exportProperties, scenario, taskId);
  }

}
