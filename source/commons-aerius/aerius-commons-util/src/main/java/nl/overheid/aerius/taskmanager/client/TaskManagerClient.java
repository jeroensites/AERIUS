/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client;

import java.io.IOException;
import java.io.Serializable;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.TaskManagerClientSender;
import nl.aerius.taskmanager.client.TaskResultCallback;

public class TaskManagerClient {

  private final TaskManagerClientSender client;

  public TaskManagerClient(final BrokerConnectionFactory factory) {
    client = new TaskManagerClientSender(factory);
  }

  public TaskManagerClientSender getClient() {
    return client;
  }

  /**
   * Convenience method of {@link #sendTask(Serializable, String, TaskResultHandler, WorkerType, String)}.
   * Should be used when we're not interested in results (fire-and-forget).
   *
   * @param input The input object which the worker needs to do the work.
   * @param queueNaming The exchange on which the task should be placed (should be known in taskmanager).
   * @param taskQueue The queue on which this task should be placed (should be known in taskmanager).
   * @throws IOException In case of errors communicating with queue.
   */
  public void sendTask(final Serializable input, final WorkerType queueNaming, final QueueEnum taskQueue) throws IOException {
    client.sendTask(input, queueNaming.type(), taskQueue.getQueueName());
  }

  /**
   * Convenience method of {@link #sendTask(Serializable, TaskResultHandler, String, String)}. Should be used when the callback handles everything and
   * does not need to know which specific input the task was send for.
   *
   * @param input The input object which the worker needs to do the work.
   * @param resultCallback The resultCallback which will receive results. Can be null, in which case the messages are only send and no results can be
   *          retrieved.
   * @param queueNaming The exchange on which the task should be placed (should be known in taskmanager).
   * @param taskQueue The queue on which this task should be placed (should be known in taskmanager).
   * @return a random unique task ID.
   * @throws IOException In case of errors communicating with queue.
   */
  public void sendTask(final Serializable input, final String uniqueId, final String taskId, final TaskResultCallback resultCallback,
      final WorkerType queueNaming, final String taskQueue) throws IOException {
    client.sendTask(input, uniqueId, taskId, resultCallback, queueNaming.type(), taskQueue);
  }
}
