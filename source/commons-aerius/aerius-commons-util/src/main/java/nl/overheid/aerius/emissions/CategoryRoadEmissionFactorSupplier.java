/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.emissions;

import java.util.EnumMap;
import java.util.Map;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadSpeedType;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.shared.emissions.RoadEmissionFactorSupplier;

class CategoryRoadEmissionFactorSupplier implements RoadEmissionFactorSupplier {

  private final SectorCategories categories;
  private final int year;

  CategoryRoadEmissionFactorSupplier(final SectorCategories categories, final int year) {
    this.categories = categories;
    this.year = year;
  }

  @Override
  public Map<Substance, Double> getRoadSpecificVehicleEmissionFactors(final String specificVehicleCode, final RoadType roadType) {
    final Map<Substance, Double> emissionFactors = new EnumMap<>(Substance.class);
    final OnRoadMobileSourceCategory category = categories.determineOnRoadMobileSourceCategoryByCode(specificVehicleCode);
    for (final Substance substance : Substance.values()) {
      final double emissionFactor = category.getEmissionFactor(substance, roadType, year);
      if (emissionFactor > 0) {
        emissionFactors.put(substance, emissionFactor);
      }
    }
    return emissionFactors;
  }

  @Override
  public Map<Substance, Double> getRoadStandardVehicleEmissionFactors(final VehicleType vehicleType, final RoadType roadType,
      final RoadSpeedType roadSpeedType, final Integer maximumSpeed, final Boolean strictEnforcement) {
    final Map<Substance, Double> emissionFactors = new EnumMap<>(Substance.class);
    final RoadEmissionCategory category = standardCategory(vehicleType, roadType, roadSpeedType, maximumSpeed, strictEnforcement);
    for (final Substance substance : Substance.values()) {
      final double emissionFactor = category.getEmissionFactor(new EmissionValueKey(year, substance));
      if (emissionFactor > 0) {
        emissionFactors.put(substance, emissionFactor);
      }
    }
    return emissionFactors;
  }

  @Override
  public Map<Substance, Double> getRoadStandardVehicleStagnatedEmissionFactors(final VehicleType vehicleType, final RoadType roadType,
      final RoadSpeedType roadSpeedType, final Integer maximumSpeed, final Boolean strictEnforcement) {
    final Map<Substance, Double> emissionFactors = new EnumMap<>(Substance.class);
    final RoadEmissionCategory category = standardCategory(vehicleType, roadType, roadSpeedType, maximumSpeed, strictEnforcement);
    for (final Substance substance : Substance.values()) {
      final double emissionFactor = category.getStagnatedEmissionFactor(new EmissionValueKey(year, substance));
      if (emissionFactor > 0) {
        emissionFactors.put(substance, emissionFactor);
      }
    }
    return emissionFactors;
  }

  private RoadEmissionCategory standardCategory(final VehicleType vehicleType, final RoadType roadType,
      final RoadSpeedType roadSpeedType, final Integer maximumSpeed, final Boolean strictEnforcement) {
    final RoadSpeedType correctRoadSpeedType = roadSpeedType == null && roadType == RoadType.NON_URBAN_ROAD ? RoadSpeedType.NATIONAL_ROAD : roadSpeedType;
    return categories.getRoadEmissionCategories().findClosestCategory(roadType, vehicleType, strictEnforcement, maximumSpeed, correctRoadSpeedType);
  }

}
