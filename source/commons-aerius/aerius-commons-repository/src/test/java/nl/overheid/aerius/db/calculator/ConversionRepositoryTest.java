/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.postgis.LineString;
import org.postgis.Polygon;
import org.postgresql.util.PSQLException;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.WeightedPoint;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Needs a proper database to test against, testing DB operations for conversions.
 */
class ConversionRepositoryTest extends BaseDBTest {

  @Test
  void testConvertToPoints() throws SQLException {
    final int numberOfCoordinates = 5;
    final ArrayList<org.postgis.Point> linePoints = new ArrayList<>();
    //create linestring with (0,0), (0,4000), (0,8000), (0,12000), (0,16000)
    for (int i = 0; i < numberOfCoordinates; i++) {
      final org.postgis.Point linePoint = new org.postgis.Point(0 * i, 4000 * i);
      linePoints.add(linePoint);
    }
    final LineString lineString = new LineString(linePoints.toArray(new org.postgis.Point[linePoints.size()]));
    List<Point> convertedPoints = new ArrayList<>();
    try (final Connection connection = getCalcConnection()) {
      final Double maxSegementSize = 76.4;
      convertedPoints = ConversionRepository.convertToPoints(connection, lineString, maxSegementSize);
      assertEquals(new BigDecimal((numberOfCoordinates - 1) * 4000).divide(new BigDecimal(maxSegementSize), 0,
          RoundingMode.UP).intValue(),
          convertedPoints.size(),
          "Number of segments");
    }
    final double segmentSize = new BigDecimal((numberOfCoordinates - 1) * 4000).divide(new BigDecimal(convertedPoints.size()), 4,
        RoundingMode.HALF_UP).doubleValue();
    for (int i = 0; i < convertedPoints.size(); i++) {
      final Point convertedPoint = convertedPoints.get(i);
      assertEquals(0, convertedPoint.getX(), 1E-2, "X-coord of point " + i);
      assertEquals(i * segmentSize + segmentSize / 2.0, convertedPoint.getY(), 1E-2, "Y-coord of point " + i);
    }
  }

  @Test
  void testValidateConvertToPoints() throws SQLException, AeriusException {
    //check if the DB version of line -> points gives us the same results as the java version.
    //Try some random linestring.
    final String line = "LINESTRING(231 12311,1155 382424,122342 2349)";
    final LineString lineString = new LineString(line);
    List<Point> convertedPointsDB = new ArrayList<>();
    final Double maxSegmentSize = 76.4;
    try (final Connection connection = getCalcConnection()) {
      convertedPointsDB = ConversionRepository.convertToPoints(connection, lineString, maxSegmentSize);
    }
    final List<Point> convertedPoints = GeometryUtil.convertToPoints(new WKTGeometry(line), maxSegmentSize);
    assertEquals(convertedPoints.size(), convertedPointsDB.size(), "Lists should have same size.");
    for (int i = 0; i < convertedPoints.size(); i++) {
      final Point convertedPoint = convertedPoints.get(i);
      final Point convertedPointDB = convertedPointsDB.get(i);
      assertEquals(convertedPoint.getX(), convertedPointDB.getX(), 1E-5, "X-coord of point " + i);
      assertEquals(convertedPoint.getY(), convertedPointDB.getY(), 1E-5, "Y-coord of point " + i);
    }
  }

  @Test
  void testConvertPolygonToPoints() throws SQLException {
    //create polygon with points (0,0), (0,4000), (4000,4000), (4000,0), (0,0)
    final int side = 4000;
    final String polygonWKT = "POLYGON((0 0,0 " + side + "," + side + " " + side + "," + side + " 0,0 0))";
    final Polygon polygon = new Polygon(polygonWKT);
    List<WeightedPoint> convertedPoints = new ArrayList<>();
    double convertPolygonToPointsGridSize;
    try (final Connection connection = getCalcConnection()) {
      polygon.setSrid(ReceptorGridSettingsRepository.getSrid(connection));
      convertedPoints = ConversionRepository.convertToPoints(connection, polygon);
      convertPolygonToPointsGridSize = ConstantRepository.getNumber(connection, ConstantsEnum.CONVERT_POLYGON_TO_POINTS_GRID_SIZE, Double.class);
    }
    //known case for squares where each side is divisable by the max square side size.
    //function starts with creating squares in the middle, radiating outwards.
    //each point inside the surface (in this case the square) will be returned.
    //points on the border will have a weight factor of 0.5 and those exactly on the corners 0.25.
    assertEquals(Math.pow(side / convertPolygonToPointsGridSize + 1, 2),
        convertedPoints.size(),
        0.5, "Number of segments");
    // The coordinates of the point of the b
    final double offset = convertPolygonToPointsGridSize / 4;
    for (final WeightedPoint point : convertedPoints) {
      assertTrue(point.getX() >= 0 && point.getX() <= side, "Point x coord inside square");
      assertTrue(point.getY() >= 0 && point.getY() <= side, "Point y coord inside square");
      //points on border get weight factor 0.5, others get 1 (are fully inside).
      if ((point.getX() == offset || point.getX() == (side - offset))
          && (point.getY() == offset || point.getY() == (side - offset))) {
        assertEquals(0.25, point.getFactor(), 0.001, "Weight factor points on the corners");
      } else if (point.getX() == offset || point.getX() == (side - offset)
          || point.getY() == offset || point.getY() == (side - offset)) {
        assertEquals(0.5, point.getFactor(), 0.001, "Weight factor points on the border");
      } else {
        assertEquals(1, point.getFactor(), 0.001, "Weight factor points within the polygon");
      }
    }
  }

  @Test
  void testConvertPolygonToPointsSmall() throws SQLException {
    //create small polygon with points (0,0), (0,20), (20,20), (20,0), (0,0)
    final int side = 20;
    final String polygonWKT = "POLYGON((0 0,0 " + side + "," + side + " " + side + "," + side + " 0,0 0))";
    final Polygon polygon = new Polygon(polygonWKT);
    List<WeightedPoint> convertedPoints = new ArrayList<>();
    try (final Connection connection = getCalcConnection()) {
      polygon.setSrid(ReceptorGridSettingsRepository.getSrid(connection));
      convertedPoints = ConversionRepository.convertToPoints(connection, polygon);
    }
    //if surface for the polygon is below the max surface, it should just return one point with full weight
    //it should be centered on polygon as well.
    assertEquals(1, convertedPoints.size(), "Number of segments");
    final WeightedPoint point = convertedPoints.get(0);
    assertTrue(point.getFactor() > 0.0, "Weight factor");
    assertTrue(point.getX() >= 0 && point.getX() <= side, "Point x coord inside square");
    assertTrue(point.getY() >= 0 && point.getY() <= side, "Point y coord inside square");
    assertEquals(side / 2.0, point.getX(), 0.001, "Point x coord center of square");
    assertEquals(side / 2.0, point.getY(), 0.001, "Point Y coord center of square");
  }

  @Test
  void testConvertPolygonToPointsSmallWidth() throws SQLException {
    //create diagonal small width long polygon with points (0,0), (0,20), (4000,4020), (4000,4000), (0,0)
    final int side = 20;
    final String polygonWKT = "POLYGON((0 0,0 " + side + ",4000 " + (4000 + side) + ",4000 4000,0 0))";
    final Polygon polygon = new Polygon(polygonWKT);
    List<WeightedPoint> convertedPoints = new ArrayList<>();
    try (final Connection connection = getCalcConnection()) {
      polygon.setSrid(ReceptorGridSettingsRepository.getSrid(connection));
      convertedPoints = ConversionRepository.convertToPoints(connection, polygon);
    }
    //if surface for the polygon is below the max surface, it should just return one point with full weight
    //it should be centered on polygon as well.
    assertTrue(convertedPoints.size() > 1, "Number of segments should be more than 1 (unsure how many exactly)");
    //can we tell anything about where the points should generally be?
  }

  /**
   * Tests self-intersecting polygons; They are not supported (yet) thus it should throw an exception.
   */
  @Test
  void testPolygotnToPointsBowTie() throws SQLException {
    //create Bow-Tie polygon with points (0,0), (4000,4000), (4000,0), (0,4000), (0,0)
    final int side = 4000;
    final String polygonWKT = "POLYGON((0 0," + side + " " + side + "," + side + " 0,0 " + side + ",0 0))";
    final Polygon polygon = new Polygon(polygonWKT);
    final List<WeightedPoint> convertedPoints = new ArrayList<>();
    try (final Connection connection = getCalcConnection()) {
      polygon.setSrid(ReceptorGridSettingsRepository.getSrid(connection));
      assertThrows(PSQLException.class, () -> ConversionRepository.convertToPoints(connection, polygon),
          "Should throw exception on self intersect.");
    }
  }
}
