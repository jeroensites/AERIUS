/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.wnb;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.info.HabitatInfo;
import nl.overheid.aerius.shared.domain.info.HabitatType;

/**
 * Test class for {@link HabitatRepository}.
 */
class HabitatRepositoryTest extends BaseDBTest {

  @Test
  void testGetHabitatTypes() throws SQLException {
    final ArrayList<HabitatType> habitatTypes = HabitatRepository.getHabitatTypes(getCalcConnection(), getCalcMessagesKey());
    assertFalse(habitatTypes.isEmpty(), "We have found habitats");
  }

  static void validateHabitatInfoWooldseVeen(final HabitatInfo info) {
    assertNotNull(info.getCode(), "Habitat code should never be null. Habitat ID: " + info.getId());
    assertNotNull(info.getName(), "Habitat name should never be null. Habitat ID: " + info.getId());
    //ecology quality and goals can be null (no habitat properties for the habitat/assessment area combination).
    //such is the case for HTs with ID 118/124/274.
    if (info.getId() == 274) {
      assertFalse(info.isDesignated(), "Habitat shouldn't be designated. Habitat ID: " + info.getId());
    } else {
      assertNotNull(info.getQualityGoal(), "Habitat quality goal should never be null. Habitat ID: " + info.getId());
      assertNotNull(info.getExtentGoal(), "Habitat extent goal should never be null. Habitat ID: " + info.getId());
      assertTrue(info.isDesignated(), "Habitat should be designated. Habitat ID: " + info.getId());
    }
  }

}
