/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.CalculationRepositoryTestBase;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculationsImpl;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.test.TestDomain;

/**
 * Needs a proper database to test against, testing DB operations for calculations.
 */
class CalculationRepositoryTest extends CalculationRepositoryTestBase {

  private static final double JUST_BELOW_THRESHOLD = 0.0499;
  private static final double ON_THRESHOLD = 0.05;
  private static final double JUST_ABOVE_THRESHOLD = 0.0501;
  private static final double ON_PERMIT_THRESHOLD = 1.0;
  private static final Substance TEST_SUBSTANCE_2 = Substance.NH3;

  @Test
  void testInsertCalculation() throws SQLException {
    final Calculation calculation = new Calculation();
    calculation.setYear(TestDomain.YEAR);
    final Calculation createdCalculation = CalculationRepository.insertCalculation(getCalcConnection(), calculation, null);
    assertNotEquals(0, createdCalculation.getCalculationId(), "Calculation ID");
  }

  @Test
  void testGetCalculation() throws SQLException {
    Calculation fromDB = null;
    fromDB = CalculationRepository.getCalculation(getCalcConnection(), -1);
    assertNull(fromDB, "Calculation null if unknown ID");
    fromDB = CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId());
    assertNotNull(fromDB, "Calculation");
    assertEquals(calculation.getCalculationId(), fromDB.getCalculationId(), "Calculation ID");
    assertEquals(calculation.getCreationDate(), fromDB.getCreationDate(), "Calculation creation date");
    assertEquals(calculation.getState(), fromDB.getState(), "Calculation state");
  }

  @Test
  void testRemoveCalculationResults() throws SQLException {
    CalculationRepository.removeCalculation(getCalcConnection(), calculation.getCalculationId());
    final Calculation delCalculation = CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId());
    assertNull(delCalculation, "Calculation should not be in the database anymore for id:" + calculation.getCalculationId());
  }

  private CalculationSetOptions getOptions() {
    final CalculationSetOptions options = new CalculationSetOptions();
    options.getSubstances().add(Substance.NH3);
    options.getSubstances().add(Substance.NOX);
    options.getSubstances().add(Substance.NO2);
    options.getEmissionResultKeys().add(EmissionResultKey.NH3_DEPOSITION);
    options.getEmissionResultKeys().add(EmissionResultKey.NOX_DEPOSITION);
    options.setCalculationType(CalculationType.PERMIT);
    return options;
  }

  private static List<EmissionSourceFeature> createSources() {
    final List<EmissionSourceFeature> esl = new ArrayList<>();
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final EmissionSource es = new GenericEmissionSource();
    feature.setProperties(es);
    esl.add(feature);
    return esl;
  }

  @Disabled("We do not have a threshold value it is set to 0.0 - test not applicable")
  @Test
  void testDeleteCalculationResultsUnderThreshold() throws SQLException {
    final int calculationId = insertResultsForDelete(1.0);
    CalculationRepository.deleteLowCalculationResults(getCalcConnection(), calculationId);
    assertDeleteResult(calculationId, 2, ON_PERMIT_THRESHOLD);
  }

  private int insertResultsForDelete(final double emission) throws SQLException {
    calculationResultScale = 1.0;
    final List<AeriusResultPoint> pr = new ArrayList<>();
    pr.add(createResult(RECEPTOR_POINT_ID_1, null, EmissionResultKey.NH3_DEPOSITION, 0, JUST_BELOW_THRESHOLD, 0));
    pr.add(createResult(RECEPTOR_POINT_ID_1 + 1, null, EmissionResultKey.NH3_DEPOSITION, 0, ON_THRESHOLD, 0));
    pr.add(createResult(RECEPTOR_POINT_ID_1 + 2, null, EmissionResultKey.NH3_DEPOSITION, 0, JUST_ABOVE_THRESHOLD, 0));
    pr.add(createResult(RECEPTOR_POINT_ID_1 + 3, null, EmissionResultKey.NH3_DEPOSITION, 0, emission, 0));
    final int size = pr.size();
    insertResults(pr, size);
    final int calculationId = calculation.getCalculationId();
    final PartialCalculationResult resultsBefore = CalculationInfoRepository.getCalculationResults(getCalcConnection(), calculationId);
    assertEquals(size, resultsBefore.getResults().size(), "Shoud be 4 receptors inserted");
    return calculationId;
  }

  private void assertDeleteResult(final int calculationId, final int size, final double emission) throws SQLException {
    final PartialCalculationResult resultsAfter = CalculationInfoRepository.getCalculationResults(getCalcConnection(), calculationId);
    assertEquals(size, resultsAfter.getResults().size(), "Shoud be " + Integer.toString(size) + " receptors after deletion");
    assertEquals(emission, resultsAfter.getResults().get(0).getProperties().getResults().get(EmissionResultKey.NH3_DEPOSITION),
        0.0001, "The " + emission + " should be present");
    assertEquals(0.0501, resultsAfter.getResults().get(1).getProperties().getResults().get(EmissionResultKey.NH3_DEPOSITION),
        0.0001,
        "The 0.0501 should be present");
  }

  @Test
  void testUpdateCalculationState() throws SQLException {
    Boolean updated = CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.RUNNING);
    assertNotNull(updated, "Updated");
    assertTrue(updated, "Updated");
    calculation = CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId());
    assertEquals(CalculationState.RUNNING, calculation.getState(), "Status");
    updated = CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.CANCELLED);
    assertTrue(updated, "Updated");
    calculation = CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId());
    assertEquals(CalculationState.CANCELLED, calculation.getState(), "Status");
    updated = CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.COMPLETED);
    assertTrue(updated, "Updated");
    calculation = CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId());
    assertEquals(CalculationState.COMPLETED, calculation.getState(), "Status");
    updated = CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.INITIALIZED);
    assertTrue(updated, "Updated");
    calculation = CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId());
    assertEquals(CalculationState.INITIALIZED, calculation.getState(), "Status");
    final boolean unknownUpdated = CalculationRepository.updateCalculationState(getCalcConnection(), -1, CalculationState.RUNNING);
    assertFalse(unknownUpdated, "Update for non-existing calculation ID");
  }

  @Test
  void testInsertCalculationResultsUnsafe() throws SQLException {
    final List<AeriusResultPoint> calculationResult1 = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION,
        EmissionResultKey.NH3_DEPOSITION);
    final int inserted1 = CalculationRepository.insertCalculationResultsUnsafe(getCalcConnection(), calculation.getCalculationId(),
        calculationResult1);
    // 3 receptors * 2 result types = 6 results.
    assertEquals(6, inserted1, "Number of emission results inserted");

    //inserting a second time works, but only with different substances.
    final List<AeriusResultPoint> calculationResult2 = getExampleOPSOutputData(EmissionResultKey.PM10_CONCENTRATION,
        EmissionResultKey.PM10_EXCEEDANCE_DAYS);
    final int inserted2 = CalculationRepository.insertCalculationResultsUnsafe(getCalcConnection(), calculation.getCalculationId(),
        calculationResult2);
    // 3 receptors * 2 result types = 6 results. However, num excess days won't be persisted. Hence 3 results.
    assertEquals(3, inserted2, "Number of emission results inserted");

    //try to insert the first again, and it results in an exception.
    try {
      CalculationRepository.insertCalculationResultsUnsafe(getCalcConnection(), calculation.getCalculationId(),
          calculationResult1);
      fail("expected a SQL exception at this point");
    } catch (final SQLException e) {
      assertEquals("23505", e.getSQLState(), "SQL state");
    }
  }

  @Test
  void testGetCalculationResultSets() throws SQLException {
    insertCalculationResults();
    final Collection<EmissionResultKey> set = CalculationRepository.getCalculationResultSets(getCalcConnection(), calculation.getCalculationId());
    assertEquals(3, set.size(), "Should have 3 emission result types in database");
  }

  @Test
  void testInsertCalculationPoints() throws SQLException {
    final int calculationPointSetId = insertCalculationPoints();
    // the ID returned is a serial, so can't guess what number it'll be besides not being 0.
    assertNotEquals(0, calculationPointSetId, "Number of emission results inserted");
  }

  @Test
  void testInsertCalculationPointResults() throws SQLException {
    final List<CalculationPointFeature> calculationPointList = getExampleCalculationPointOutputData();
    final List<AeriusResultPoint> results = toAeriusResultPoints(TEST_SUBSTANCE_2, calculationPointList);
    final int inserted = CalculationRepository.insertCalculationResults(getCalcConnection(), calculation.getCalculationId(),
        results);
    // 6 results, 4 result types for one receptor, 2 for the other.
    assertEquals(results.size() * 3, inserted, "Number of emission results inserted");
  }

  @Test
  void testinsertCalculationResultsBatch() throws SQLException {
    final int firstBatchSize = 10000;
    final int secondBatchSize = 500;

    // 10km is about 10000 entries.
    final List<AeriusResultPoint> results1 = createResults(firstBatchSize);
    final int inserted1 = CalculationRepository.insertCalculationResults(getCalcConnection(), calculation.getCalculationId(), results1);
    assertEquals(firstBatchSize * 2, inserted1, "Number of inserted emission results"); // Times 2 because 2 emission result types.

    final List<AeriusResultPoint> results2 = createResults(secondBatchSize);
    // This second batch should return 0 inserted as they are all already in the database.
    final int inserted2 = CalculationRepository.insertCalculationResults(getCalcConnection(), calculation.getCalculationId(), results2);
    assertEquals(firstBatchSize * 2, inserted1 + inserted2, "Number of inserted emission results");
  }

  private static ArrayList<AeriusResultPoint> createResults(final int size) {
    final ArrayList<AeriusResultPoint> results = new ArrayList<>();
    for (int i = 1; i <= size; i++) {
      final AeriusResultPoint rp = new AeriusResultPoint();
      rp.setId(i);
      rp.setEmissionResult(EmissionResultKey.NH3_CONCENTRATION, RECEPTOR_1_CONCENTRATION + i);
      rp.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, RECEPTOR_1_DEPOSITION + i);
      results.add(rp);
    }
    return results;
  }

  @Test
  void testinsertCalculationResultsWithoutOptions() throws SQLException {
    final List<AeriusResultPoint> results = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION,
        EmissionResultKey.NH3_DEPOSITION);

    assertThrows(IllegalArgumentException.class, () -> CalculationRepository.insertCalculationResults(getCalcConnection(), -1, results),
      "Should throw an SQL exception because you can't insert a calculation result for a calculating without calculation options.");
  }

  @Test
  void testInsertCalculationCustomJob() throws SQLException {
    CalculationRepository.insertCalculationBatchOptions(getCalcConnection(), calculation.getCalculationId(),
        "SomeDescription", "C:\\DUMBFILE.DMB;D:\\DOESNTMATTERMUCH");
    assertNotNull(calculation, "Saving should be used (this assert is just here so there's something to test...).");
  }

  @Test
  void testInsertDuplicatePartialCalculationResults() throws SQLException {
    insertCalculationResults();
    final List<AeriusResultPoint> partialCalculationResult = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION,
        EmissionResultKey.NH3_DEPOSITION);
    final AeriusResultPoint rp = new AeriusResultPoint();
    rp.setId(99);
    rp.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, 160.0);
    partialCalculationResult.add(rp);
    // Shouldn't insert the previously inserted results again.
    final int inserted = CalculationRepository.insertCalculationResults(getCalcConnection(), calculation.getCalculationId(),
        partialCalculationResult);
    assertEquals(1, inserted, "Number of emission results inserted");
  }

  @Test
  void testGetCalculatedPoints() throws SQLException {
    final int calculationPointSetId = insertCalculationPoints();
    removeCalculationResults();
    createCalculation(calculationPointSetId);
    insertCalculationResults();
    final List<CalculationPointFeature> calculationPointList = getExampleCalculationPointOutputData();
    final List<AeriusResultPoint> results = toAeriusResultPoints(TEST_SUBSTANCE_2, calculationPointList);
    CalculationRepository.insertCalculationResults(getCalcConnection(), calculation.getCalculationId(), results);
    final List<CalculationPointFeature> calculatedPoints = CalculationRepository.getCalculatedPoints(getCalcConnection(), calculation.getCalculationId());
    int calculationPoints = 0;
    int receptorPoints = 0;
    for (final CalculationPointFeature feature : calculatedPoints) {
      final CalculationPoint calculatedPoint = feature.getProperties();
      assertNotEquals(0, calculatedPoint.getId(), "ID should be set");
      assertNotEquals(0.0, feature.getGeometry().getX(), 1E-8, "X coord should be set");
      assertNotEquals(0.0, feature.getGeometry().getY(), 1E-8, "Y coord should be set");
      if (calculatedPoint instanceof CustomCalculationPoint) {
        calculationPoints++;
        assertNotNull(calculatedPoint.getLabel(), "Label shouldn't be null");
      } else if (calculatedPoint instanceof ReceptorPoint) {
        receptorPoints++;
      } else {
        fail("Did not expect point of type " + calculatedPoint);
      }
    }
    assertEquals(3, receptorPoints, "Nr of receptors");
    assertEquals(2, calculationPoints, "Nr of calculation points");
  }

  @Test
  void testInsertCalculationsWithoutPoints() throws SQLException {
    final List<CalculationPointFeature> calculationPoints = new ArrayList<>();
    final ScenarioCalculations cs = getExampleScenarioCalculations(calculationPoints);

    CalculationRepository.insertCalculations(getCalcConnection(), cs, true);
    assertNull(cs.getCalculationPointSetTracker().getCalculationPointSetId(), "Without calculation points, no point set ID");
    assertPersistedCalculation(cs);
  }

  @Test
  void testInsertCalculationsWithPoints() throws SQLException {
    final List<CalculationPointFeature> calculationPoints = new ArrayList<>();
    calculationPoints.add(createPoint(1, 1, 1));
    calculationPoints.add(createPoint(2, 30, 40));
    final ScenarioCalculations cs = getExampleScenarioCalculations(calculationPoints);

    CalculationRepository.insertCalculations(getCalcConnection(), cs, true);
    assertNotNull(cs.getCalculationPointSetTracker().getCalculationPointSetId(),
        "With calculation list filled with points, there should be a calculation point set ID");
    assertPersistedCalculation(cs);
  }

  private CalculationPointFeature createPoint(final int id, final double x, final double y) {
    final CalculationPointFeature feature = new CalculationPointFeature();
    feature.setGeometry(new Point(x, y));
    final CustomCalculationPoint properties = new CustomCalculationPoint();
    properties.setCustomPointId(id);
    feature.setProperties(properties);
    return feature;
  }

  private void assertPersistedCalculation(final ScenarioCalculations cs) throws SQLException {
    for (final Calculation calculation : cs.getCalculations()) {
      assertNotEquals(0, calculation.getCalculationId(), "Calculation ID");
      assertNotNull(CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId()), "Calculation in database");
    }
  }

  private ScenarioCalculations getExampleScenarioCalculations(final List<CalculationPointFeature> calculationPoints) {
    final Scenario scenario = new Scenario(Theme.WNB);
    final ScenarioSituation situation = new ScenarioSituation();
    situation.getEmissionSourcesList().addAll(createSources());
    situation.setYear(LocalDate.now().getYear());
    scenario.getSituations().add(situation);
    scenario.getCustomPointsList().addAll(calculationPoints);
    scenario.setOptions(getOptions());

    final ScenarioCalculations scenarioCalculations = new ScenarioCalculationsImpl(scenario);

    assertNull(scenarioCalculations.getCalculationPointSetTracker().getCalculationPointSetId(), "Calculation point set ID");
    for (final Calculation calculation : scenarioCalculations.getCalculations()) {
      assertEquals(0, calculation.getCalculationId(), "Calculation ID");
    }
    return scenarioCalculations;
  }

  @Override
  protected Connection getConnection() throws SQLException {
    return getCalcConnection();
  }

}
