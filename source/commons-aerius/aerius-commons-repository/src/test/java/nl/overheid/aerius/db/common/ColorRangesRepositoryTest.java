/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.result.range.ColorRange;
import nl.overheid.aerius.shared.domain.result.range.ColorRangeType;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test claass for ColorRangesRepository.
 */
class ColorRangesRepositoryTest extends BaseDBTest {

  static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> connectionTests = new ArrayList<>();
    final Object[] calculator = new Object[3];
    calculator[0] = getCalcPMF();
    calculator[1] = "Calculator";
    calculator[2] = new DBMessages.DBMessagesKey(ProductType.CALCULATOR, LocaleUtils.getDefaultLocale());
    connectionTests.add(calculator);
    return connectionTests;
  }

  @ParameterizedTest
  @MethodSource("data")
  void testGetColorRanges(final PMF pmf, final String testDescription, final DBMessages.DBMessagesKey messagesKey) throws SQLException {

    final Map<ColorRangeType, List<ColorRange>> colorRanges = ColorRangesRepository.getColorRanges(pmf.getConnection());
    assertNotNull(colorRanges, "Returned list should not be null");
    assertFalse(colorRanges.isEmpty(), "Returned list should not be empty");

    final List<ColorRange> colorRange = colorRanges.get(ColorRangeType.DEPOSITION_CONTRIBUTION);
    assertNotNull(colorRange, "(DEPOSITION_CONTRIBUTION) list should not be null");
    assertFalse(colorRange.isEmpty(), "(DEPOSITION_CONTRIBUTION) list should not be empty");

  }
}
