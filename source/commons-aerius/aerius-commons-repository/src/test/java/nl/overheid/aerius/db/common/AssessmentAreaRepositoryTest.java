/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link AssessmentAreaRepository}.
 */
class AssessmentAreaRepositoryTest extends BaseDBTest {

  static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> connectionTests = new ArrayList<>();
    final Object[] calculator = new Object[2];
    calculator[0] = getCalcPMF();
    calculator[1] = "Calculator";
    connectionTests.add(calculator);
    return connectionTests;
  }

  @ParameterizedTest
  @MethodSource("data")
  void testGetNatura2kAreas(final PMF pmf, final String testDescription) throws SQLException {
    final List<AssessmentArea> n2kAreas = AssessmentAreaRepository.getNatura2kAreas(pmf.getConnection());
    assertNotNull(n2kAreas, testDescription + ": Areas shouldn't be null");
    assertFalse(n2kAreas.isEmpty(), testDescription + ": Areas should be found");
    for (final AssessmentArea area : n2kAreas) {
      assertNotEquals(0, area.getId(), testDescription + ": Area ID");
      assertNotNull(area.getName(), testDescription + ": Area name");
    }
  }

  @ParameterizedTest
  @MethodSource("data")
  void testGetAssessmentArea(final PMF pmf, final String testDescription) throws SQLException {
    AssessmentArea area = AssessmentAreaRepository.getAssessmentArea(pmf.getConnection(), 0);
    assertNull(area, testDescription + ": AssessmentArea should be null");

    area = AssessmentAreaRepository.getAssessmentArea(pmf.getConnection(), TestDomain.BINNENVELD_ID);
    assertNotNull(area, testDescription + ": AssessmentArea shouldn't be null");
    assertEquals(TestDomain.BINNENVELD_ID, area.getId(), testDescription + ": Area ID");
    assertEquals(TestDomain.BINNENVELD_ID, area.getId(), testDescription + ": Area AssessmentAreaID");
    assertEquals("Binnenveld", area.getName(), testDescription + ": Area name");
    assertNotNull(area.getBounds(), testDescription + ": Area bounding box");
  }

  @ParameterizedTest
  @MethodSource("data")
  void testGetAssessmentAreas(final PMF pmf, final String testDescription) throws SQLException {
    final Map<Integer, AssessmentArea> areas = AssessmentAreaRepository.getAssessmentAreas(pmf.getConnection());
    assertNotNull(areas, testDescription + ": Areas shouldn't be null");
    assertFalse(areas.isEmpty(), testDescription + ": Areas should be found");

    for (final Entry<Integer, AssessmentArea> entry : areas.entrySet()) {
      final AssessmentArea area = entry.getValue();
      assertNotEquals(0, area.getId(), testDescription + ": Area ID");
      assertEquals(entry.getKey().intValue(), area.getId(), testDescription + ": Area ID");
      assertNotNull(area.getName(), testDescription + ": Area name");
      assertNotNull(area.getBounds(), testDescription + ": Area bounds");
    }
  }

}
