/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.wnb;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.ops.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;

/**
 * Test class for {@link ReceptorRepository}.
 */
class ReceptorRepositoryTest extends BaseDBTest {

  private static final int RECEPTOR_ID = 2793000;
  private static final double X_COORD = 198100.4155;
  private static final double Y_COORD = 394908.2347;
  private static final double AVERAGE_ROUGHNESS = 0.51979;
  private static final LandUse DOMINANT_LAND_USE = LandUse.DECIDUOUS_FOREST;
  private static final int[] LAND_USE_PERCENTAGES = new int[] { 11, 10, 0, 9, 59, 0, 10, 0, 1 };

  private static final int RECEPTOR_ID_SPECIAL_CASE = 4832573; // receptor where rounded percentages would add up to 98
  private static final int[] SPECIAL_CASE_LAND_USE_PERCENTAGES = new int[] { 4, 21, 0, 23, 17, 1, 2, 20, 12 };

  private static final String SQL_GET_LAND_USE_ENUM = "SELECT unnest(enum_range(NULL::land_use_classification))";

  @Test
  void testSetTerrainData() throws SQLException {
    OPSReceptor point = new OPSReceptor(0);
    ReceptorRepository.setTerrainData(getCalcConnection(), RECEPTOR_UTIL, RECEPTOR_GRID_SETTINGS, point);
    assertEquals(0.1, point.getAverageRoughness(), 0.00001, "Receptor average roughness, uninitialized");
    assertEquals(LandUse.OTHER_NATURE, point.getLandUse(), "Receptor dominant land use, uninitialized");
    assertNull(point.getLandUses(), "Receptor land use fractions, uninitialized");

    point = new OPSReceptor(RECEPTOR_ID);
    ReceptorRepository.setTerrainData(getCalcConnection(), RECEPTOR_UTIL, RECEPTOR_GRID_SETTINGS, point);
    assertEquals(0, point.getX(), 0.0001, "Receptor X coordinate, ID only");
    assertEquals(0, point.getY(), 0.0001, "Receptor Y coordinate, ID only");
    assertEquals(0.1, point.getAverageRoughness(), 0.00001, "Receptor average roughness, ID only");
    assertEquals(LandUse.OTHER_NATURE, point.getLandUse(), "Receptor dominant land use, ID only");
    assertNull(point.getLandUses(), "Receptor land use fractions, ID only");

    point = new OPSReceptor(0, X_COORD - 1, Y_COORD + 1);
    ReceptorRepository.setTerrainData(getCalcConnection(), RECEPTOR_UTIL, RECEPTOR_GRID_SETTINGS, point);
    assertEquals(AVERAGE_ROUGHNESS, point.getAverageRoughness(), 0.00001, "Receptor average roughness, right coordinates");
    assertEquals(DOMINANT_LAND_USE, point.getLandUse(), "Receptor dominant land use, right coordinates");
    assertArrayEquals(LAND_USE_PERCENTAGES, point.getLandUses(), "Receptor land use percentage, right coordinates");
    // setting terrain properties shouldn't set ID.
    assertEquals(0, point.getId(), "Receptor ID, right coordinates");
    // and it shouldn't adjust coords.
    assertEquals(X_COORD - 1, point.getX(), 0.0001, "Receptor X coordinate, right coordinates");
    assertEquals(Y_COORD + 1, point.getY(), 0.0001, "Receptor Y coordinate, right coordinates");

    point = setCoordinatesFromId(new OPSReceptor(RECEPTOR_ID_SPECIAL_CASE));
    ReceptorRepository.setTerrainData(getCalcConnection(), RECEPTOR_UTIL, RECEPTOR_GRID_SETTINGS, point);
    assertArrayEquals(SPECIAL_CASE_LAND_USE_PERCENTAGES, point.getLandUses(), "Receptor land use percentages");

    point = new OPSReceptor(0, 1, 1);
    // is within NL bounding box, but not defined in table.
    ReceptorRepository.setTerrainData(getCalcConnection(), RECEPTOR_UTIL, RECEPTOR_GRID_SETTINGS, point);
    assertEquals(0.1, point.getAverageRoughness(), 0.00001, "Receptor average roughness, wrong coordinates");
    assertEquals(LandUse.OTHER_NATURE, point.getLandUse(), "Receptor dominant land use, wrong coordinates");
    assertNull(point.getLandUses(), "Receptor land use fractions, wrong coordinates");
  }

  @Test
  void testGetPermitRequiredPoints() throws SQLException {
    final Map<Integer, List<AeriusPoint>> points = ReceptorRepository.getPermitRequiredPoints(getCalcConnection(), RECEPTOR_UTIL);
    assertNotNull(points, "Receptors");
    assertNotEquals(0, points.size(), "Number of receptors");
  }

  @Test
  void testLandUseEnum() throws SQLException {
    // Checks for typos in the Java enum values
    final LandUse[] expectedLandUses = LandUse.values();
    final List<LandUse> actualLandUses = new ArrayList<>();
    try (final PreparedStatement ps = getCalcConnection().prepareStatement(SQL_GET_LAND_USE_ENUM)) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        actualLandUses.add(LandUse.safeValueOf(rs.getString(1)));
      }
    }
    assertArrayEquals(expectedLandUses, actualLandUses.toArray(), "Land use enum in Java and DB");
  }
}
