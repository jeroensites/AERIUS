/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.domain.summary.CustomCalculationPointResult;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SituationResultsAreaSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsHabitatSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.summary.SurfaceChartResults;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Unit test for calculating contribution statistics related to calculation results.
 */
class MaxTempContributionResultsTest extends ResultsTestBase {

  @Override
  @BeforeEach
  public void prepareTests() throws AeriusException, SQLException {
    super.prepareTests();
  }

  @Test
  void testGetOverallStatistics() throws AeriusException {
    final SituationResultsSummary results = resultsRepository.determineReceptorResultsSummary(situationCalculations, ScenarioResultType.MAX_TEMPORARY_CONTRIBUTION, jobId, null, SummaryHexagonType.RELEVANT_HEXAGONS, this::determineAssessmentArea, this::determineHabitatType);
    final SituationResultsStatistics statistics = results.getStatistics();

    assertEquals(16887.105, statistics.get(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE), 0.001, "Cartographic surface");
    assertEquals(1454.1952, statistics.get(ResultStatisticType.MAX_TOTAL), 0.001, "Max total");
    assertEquals(351.345, statistics.get(ResultStatisticType.MAX_TEMP_CONTRIBUTION), 0.001, "Highest temp contribution");
  }

  @Test
  void testGetAreaStatistics() throws AeriusException {
    final SituationResultsSummary results = resultsRepository.determineReceptorResultsSummary(situationCalculations, ScenarioResultType.MAX_TEMPORARY_CONTRIBUTION, jobId, null, SummaryHexagonType.RELEVANT_HEXAGONS, this::determineAssessmentArea, this::determineHabitatType);

    assertEquals(2, results.getAreaStatistics().size(), "Number of areas with results");

    final SituationResultsAreaSummary areaSummary = results.getAreaStatistics().get(0);
    assertEquals("Veluwe", areaSummary.getAssessmentArea().getName(), "Area with highest result");
    assertEquals(10000.0, areaSummary.getStatistics().get(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE), 0.001, "Cartographic surface");
    assertEquals(1454.1952, areaSummary.getStatistics().get(ResultStatisticType.MAX_TOTAL), 0.001, "Max total");
    assertEquals(351.345, areaSummary.getStatistics().get(ResultStatisticType.MAX_TEMP_CONTRIBUTION), 0.001, "Highest temp contribution");
  }

  @Test
  void testGetHabitatStatistics() throws AeriusException {
    final SituationResultsSummary results = resultsRepository.determineReceptorResultsSummary(situationCalculations, ScenarioResultType.MAX_TEMPORARY_CONTRIBUTION, jobId, null, SummaryHexagonType.RELEVANT_HEXAGONS, this::determineAssessmentArea, this::determineHabitatType);

    final SituationResultsAreaSummary areaSummary = results.getAreaStatistics().get(0);
    assertEquals(1, areaSummary.getHabitatSummaries().size(), "Number of habitats with results");

    final SituationResultsHabitatSummary habitatSummary = areaSummary.getHabitatSummaries().get(0);
    assertEquals("Droge heiden", habitatSummary.getHabitatType().getName(), "Habitat with result");
    assertEquals(10000.0, habitatSummary.getStatistics().get(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE), 0.001, "Cartographic surface");
    assertEquals(1454.1952, habitatSummary.getStatistics().get(ResultStatisticType.MAX_TOTAL), 0.001, "Max total");
    assertEquals(351.345, habitatSummary.getStatistics().get(ResultStatisticType.MAX_TEMP_CONTRIBUTION), 0.001, "Highest temp contribution");
  }

  @Test
  void testGetChartStatistics() throws AeriusException {
    final SituationResultsSummary results = resultsRepository.determineReceptorResultsSummary(situationCalculations, ScenarioResultType.MAX_TEMPORARY_CONTRIBUTION, jobId, null, SummaryHexagonType.RELEVANT_HEXAGONS, this::determineAssessmentArea, this::determineHabitatType);

    final SituationResultsAreaSummary areaSummary = results.getAreaStatistics().get(0);
    assertEquals(13, areaSummary.getChartResults().size(), "Number chart categories");

    final SurfaceChartResults chartResults = areaSummary.getChartResults().get(12);
    assertEquals(142.86, chartResults.getLowerBound(), 0.001, "Lower bound of chart category");
    assertEquals(10000.0, chartResults.getCartographicSurface(), 0.001, "Surface of chart category");
  }

  @Test
  void testGetStatisticMarkers() throws AeriusException {
    final SituationResultsSummary results = resultsRepository.determineReceptorResultsSummary(situationCalculations, ScenarioResultType.MAX_TEMPORARY_CONTRIBUTION, jobId, null, SummaryHexagonType.RELEVANT_HEXAGONS, this::determineAssessmentArea, this::determineHabitatType);

    assertEquals(2, results.getMarkers().size(), "Number of markers");
    final int receptorId1 = Math.min(results.getMarkers().get(0).getReceptorId(), results.getMarkers().get(1).getReceptorId());
    final int receptorId2 = Math.max(results.getMarkers().get(0).getReceptorId(), results.getMarkers().get(1).getReceptorId());
    assertEquals(RECEPTOR_POINT_ID_1, receptorId1, "Receptor ID of marker 1");
    assertEquals(RECEPTOR_POINT_ID_2, receptorId2, "Receptor ID of marker 2");
  }

  @Test
  void testDetermineCustomCalculationPointResultsSummary() throws AeriusException {
    final SituationResultsSummary results = resultsRepository.determineCustomCalculationPointResultsSummary(situationCalculations,
        ScenarioResultType.MAX_TEMPORARY_CONTRIBUTION, null);

    final List<CustomCalculationPointResult> customPointResults = results.getCustomPointResults();
    assertEquals(2, customPointResults.size(), "Custom point results");
    for (final CustomCalculationPointResult pointResult : customPointResults) {
      if (pointResult.getCustomPointId() == 1) {
        assertEquals(702.69, pointResult.getResult(), 0.001, "Result for point 1");
      } else if (pointResult.getCustomPointId() == 2) {
        assertEquals(3.69, pointResult.getResult(), 0.001, "Result for point 2");
      } else {
        fail("Unexpected custom point id: " + pointResult.getCustomPointId());
      }
    }
    final SituationResultsStatistics statistics = results.getStatistics();

    assertEquals(2, statistics.size(), "Number of statistics");
    assertEquals(2, statistics.get(ResultStatisticType.COUNT_CALCULATION_POINTS), 0.001, "Number of calculation point results");
    assertEquals(702.69, statistics.get(ResultStatisticType.MAX_TEMP_CONTRIBUTION), 0.001, "Max temp contribution");
  }

}
