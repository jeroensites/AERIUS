/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.info.HabitatInfo;
import nl.overheid.aerius.shared.domain.info.Natura2000Info;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.test.TestDomain;

/**
 * Unit test for {@link ReceptorInfoRepository}.
 */
class ReceptorInfoRepositoryTest extends BaseDBTest {

  private static final int RECEPTOR_POINT_ID = 4275970;
  private static final int NATURA2000_ID = 65;

  @Test
  void testGetNaturaAreaInfo() throws SQLException {
    List<Natura2000Info> infos = ReceptorInfoRepository.getNaturaAreaInfo(getCalcConnection(), 0, getCalcMessagesKey());
    assertNotNull(infos, "Result shouldn't be null");
    assertEquals(0, infos.size(), "Number of nature areas");
    infos = ReceptorInfoRepository.getNaturaAreaInfo(getCalcConnection(), RECEPTOR_POINT_ID, getCalcMessagesKey());
    assertNotNull(infos, "Result shouldn't be null");
    assertEquals(1, infos.size(), "Number of nature areas");
    final Natura2000Info info = infos.get(0);
    assertEquals(NATURA2000_ID, info.getId(), "Assessment Area id");
    assertEquals(NATURA2000_ID, info.getId(), "Natura 2000 Area id");
    assertEquals("Binnenveld", info.getName(), "Nature area name");
    assertEquals(3, info.getHabitats().size(), "Number of habitat types in nature area");
    assertEquals("HR", info.getProtection(), "Nature area protection");
    assertNull(info.getGeometry(), "Geometry shouldn't be filled: too much info");
  }

  @Test
  void testGetReceptorHabitatAreaInfo() throws SQLException {
    final List<HabitatInfo> infos = ReceptorInfoRepository.getReceptorHabitatAreaInfo(getCalcConnection(), RECEPTOR_POINT_ID,
        getCalcMessagesKey());
    assertNotNull(infos, "Result shouldn't be empty");
    assertEquals(1, infos.size(), "Number of habitat types");
    final HabitatInfo info = infos.get(0);
    assertEquals("H7140A", info.getCode(), "Code of habitat type");
    assertEquals("Overgangs- en trilvenen (trilvenen)", info.getName(), "Name of habitat type");
    assertEquals(1214, info.getCriticalLevels().get(EmissionResultKey.NOXNH3_DEPOSITION), 0.0, "KDW of habitat type");
    assertNotEquals(0.0, info.getCartographicSurface(), 1E-3, "Hexagon overlap of habitat type");
  }

  @Test
  void testGetBackgroundEmissionResultInfo() throws SQLException {
    EmissionResults info =
        ReceptorInfoRepository.getBackgroundEmissionResult(getCalcConnection(), getReceptorPoint(RECEPTOR_POINT_ID), RECEPTOR_POINT_ID, 13);
    assertTrue(info.isEmpty(), "Emission results info object should be empty when retrieving wrong year");
    info = ReceptorInfoRepository.getBackgroundEmissionResult(getCalcConnection(), getReceptorPoint(1), 1, TestDomain.YEAR);
    assertTrue(info.isEmpty(), "Emission results info object should be empty when retrieving outside NL");
    info = ReceptorInfoRepository.getBackgroundEmissionResult(getCalcConnection(), getReceptorPoint(RECEPTOR_POINT_ID), RECEPTOR_POINT_ID,
        TestDomain.YEAR);
    assertNotNull(info, "Emission results info object");
    assertNotEquals(0, info.get(EmissionResultKey.NOXNH3_DEPOSITION), 1, "Background NH3+NOx deposition");
  }

  private Point getReceptorPoint(final int id) {
    return RECEPTOR_UTIL.getPointFromReceptorId(id);
  }
}
