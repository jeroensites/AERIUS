/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.calculator.JobRepository.RepositoryAttribute;
import nl.overheid.aerius.db.connect.ConnectUserRepository;
import nl.overheid.aerius.db.test.CalculationRepositoryTestBase;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.JobProgress;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.connect.ConnectUser;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Unit test for {@link JobRepository}.
 */
class JobRepositoryTest extends CalculationRepositoryTestBase {

  private static final String TEST_API_KEY = "wnaR9FavGRGv8RXCmdfXKEqeIt1DTZUS";
  private static final String TEST_EMAIL = "test@example.com";

  @Test
  void testGetUnexistingJobId() throws SQLException {
    final int jobId = JobRepository.getJobId(getCalcConnection(), UUID.randomUUID().toString());
    assertEquals(0, jobId, "No job id should be found");
  }

  @Test
  void testGetUnexistingJobProgress() throws SQLException {
    final JobProgress jp = JobRepository.getProgress(getCalcConnection(), UUID.randomUUID().toString());
    assertNull(jp, "No job progress should be found");
  }

  @Test
  void testCreateEmptyJobWithoutCalculations() throws SQLException, AeriusException {
    final ConnectUser user = setupUser();
    final String correlationIdentifier = JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION);
    assertFalse(correlationIdentifier.isEmpty(), "correlationIdentifier shouldn't be empty");

    final JobProgress jp = JobRepository.getProgress(getCalcConnection(), correlationIdentifier);
    validateEmptyProgress(jp);
    assertSame(JobState.INITIALIZED, jp.getState(), "State may must be initialized");
    assertNull(jp.getCreationDateTime(), "Creation time may not be set");
    assertNull(jp.getName(), "Name should not be set");
  }

  @Test
  void testCreateEmptyJobWithCalculations() throws SQLException, AeriusException {
    final ConnectUser user = setupUser();
    final String correlationIdentifier = JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION);
    insertCalculationResults();
    JobRepository.attachCalculations(getCalcConnection(), correlationIdentifier, Collections.singletonList(calculation));

    final JobProgress jp = JobRepository.getProgress(getCalcConnection(), correlationIdentifier);
    validateEmptyProgress(jp);
    assertEquals(JobState.RUNNING, jp.getState(), "State must be initialized");
    assertNotNull(jp.getCreationDateTime(), "Creation time must be set");
    assertFalse(jp.getCreationDateTime().getTime() > new Date().getTime(), "Creation time can't be in the future");
    assertTrue(calculation.getCreationDate().getTime() < jp.getCreationDateTime().getTime(), "Creation time must match that of the calculation");
  }

  @Test
  void testUpdateJobProgress() throws SQLException, AeriusException {
    final ConnectUser user = setupUser();
    final String testName = "My very long and weird job name.. GI%4j5h4g4jgR$_ 43-A";
    final String correlationIdentifier = JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION, Optional.of(testName));
    JobProgress jp;

    JobRepository.increaseHexagonCounter(getCalcConnection(), correlationIdentifier, 1);
    jp = JobRepository.getProgress(getCalcConnection(), correlationIdentifier);
    assertEquals(JobType.CALCULATION, jp.getType(), "Type must be a calculation");
    assertEquals(1, jp.getHexagonCount(), "Hexagon counter must be incremented");
    assertEquals(testName, jp.getName(), "Name must be updated");
    assertNotNull(jp.getStartDateTime(), "Start time must be set");
    assertFalse(jp.getStartDateTime().getTime() > new Date().getTime(), "Start time can't be in the future");

    JobRepository.increaseHexagonCounter(getCalcConnection(), correlationIdentifier, 12345);
    jp = JobRepository.getProgress(getCalcConnection(), correlationIdentifier);
    assertEquals(1 + 12345, jp.getHexagonCount(), "Hexagon counter must be incremented");

    JobRepository.setEndTimeToNow(getCalcConnection(), correlationIdentifier);
    jp = JobRepository.getProgress(getCalcConnection(), correlationIdentifier);
    assertNotNull(jp.getEndDateTime(), "End time must be set");
    assertFalse(jp.getEndDateTime().getTime() > new Date().getTime(), "End time can't be in the future");

    JobRepository.setResultUrl(getCalcConnection(), correlationIdentifier, "abc");
    jp = JobRepository.getProgress(getCalcConnection(), correlationIdentifier);
    assertNotNull(jp.getResultUrl(), "Result url must be set");

    // Ensure getProgressForUser gives same result
    final List<JobProgress> jpl = JobRepository.getProgressForUser(getCalcConnection(), user);
    assertEquals(1, jpl.size(), "User must have 1 progress result");
    jp = jpl.get(0);
    assertEquals(1 + 12345, jp.getHexagonCount(), "Hexagon counter must be incremented");
    assertNotNull(jp.getStartDateTime(), "Start time must be set");
    assertFalse(jp.getStartDateTime().getTime() > new Date().getTime(), "Start time can't be in the future");
    assertNotNull(jp.getEndDateTime(), "End time must be set");
    assertFalse(jp.getEndDateTime().getTime() > new Date().getTime(), "End time can't be in the future");
    assertNotNull(jp.getResultUrl(), "Result url must be set");
    assertNotNull(jp.getKey(), "Key should be present");
  }

  @Test
  void testGetCalculationsProgressForUser() throws SQLException, AeriusException {
    List<JobProgress> progresses;

    final ConnectUser user = setupUser();
    assertJobProgressAmount(user, 0);

    final String correlationIdentifier1 = JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION);
    final String correlationIdentifier2 = JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION);

    assertJobProgressAmount(user, 2);

    progresses = JobRepository.getProgressForUser(getCalcConnection(), user);
    final JobProgress jp1a = progresses.get(0); // order by job_id
    final JobProgress jp2a = progresses.get(1);
    assertEquals(0, jp1a.getHexagonCount(), "Hexagon counter must be 0 (1st entry)");
    assertEquals(0, jp2a.getHexagonCount(), "Hexagon counter must be 0 (2nd entry)");
    assertNotNull(jp1a.getStartDateTime(), "Start time must be set (1st entry)");
    assertNotNull(jp2a.getStartDateTime(), "Start time must be set (2nd entry)");

    JobRepository.increaseHexagonCounter(getCalcConnection(), correlationIdentifier1, 12345);
    progresses = JobRepository.getProgressForUser(getCalcConnection(), user);
    final JobProgress jp1b = progresses.get(0);
    final JobProgress jp2b = progresses.get(1);
    assertEquals(12345, jp1b.getHexagonCount(), "Hexagon counter must be 0 (1st entry)");
    assertEquals(0, jp2b.getHexagonCount(), "Hexagon counter must still be 0 (2nd entry)");
    assertNotNull(jp1b.getStartDateTime(), "Start time must be set (2nd entry)");
    assertNotNull(jp2b.getStartDateTime(), "Start time must be set (2nd entry)");
  }

  @Test
  void testRemoveJobsWithMinAge() throws SQLException, AeriusException {
    final ConnectUser user = setupUser();
    final int jobCountOffset = JobRepository.removeJobsWithMinAge(getCalcConnection(), 5);

    final String correlationIdentifier1 = JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION);
    insertCalculationResults();
    JobRepository.attachCalculations(getCalcConnection(), correlationIdentifier1, Collections.singletonList(calculation));
    CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.COMPLETED);
    JobRepository.updateJobStatus(getCalcConnection(), correlationIdentifier1, JobState.COMPLETED);
    newCalculation();
    final String correlationIdentifier2 = JobRepository.createJob(getCalcConnection(), user, JobType.REPORT);
    insertCalculationResults();
    JobRepository.attachCalculations(getCalcConnection(), correlationIdentifier2, Collections.singletonList(calculation));
    CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.COMPLETED);
    JobRepository.updateJobStatus(getCalcConnection(), correlationIdentifier2, JobState.COMPLETED);
    newCalculation();
    final String correlationIdentifier3 = JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION);
    insertCalculationResults();
    JobRepository.attachCalculations(getCalcConnection(), correlationIdentifier3, Collections.singletonList(calculation));
    CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.COMPLETED);
    JobRepository.updateJobStatus(getCalcConnection(), correlationIdentifier3, JobState.COMPLETED);

    final Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MINUTE, -10); // so the timestamps created have the amount of days + 10 minutes of age
    calendar.add(Calendar.DAY_OF_YEAR, -1);
    final Date endTimeJob1 = calendar.getTime();
    calendar.add(Calendar.DAY_OF_YEAR, -1);
    final Date startTimeJob2 = calendar.getTime();
    calendar.add(Calendar.DAY_OF_YEAR, -1);
    final Date endTimeJob3 = calendar.getTime();

    JobRepository.updateField(getCalcConnection(), correlationIdentifier1, RepositoryAttribute.END_TIME, new Timestamp(endTimeJob1.getTime()));
    JobRepository.updateField(getCalcConnection(), correlationIdentifier2, RepositoryAttribute.START_TIME, new Timestamp(startTimeJob2.getTime()));
    JobRepository.updateField(getCalcConnection(), correlationIdentifier2, RepositoryAttribute.END_TIME, null);
    JobRepository.updateField(getCalcConnection(), correlationIdentifier3, RepositoryAttribute.END_TIME, new Timestamp(endTimeJob3.getTime()));

    // start point - I am expecting 3 jobs
    assertJobProgressAmount(user, 3);

    assertEquals(jobCountOffset, JobRepository.removeJobsWithMinAge(getCalcConnection(), 5), "There should be 0 removals");
    assertJobProgressAmount(user, 3);

    assertEquals(jobCountOffset + 1, JobRepository.removeJobsWithMinAge(getCalcConnection(), 3), "There should be 1 removal");
    assertJobProgressAmount(user, 2);

    assertEquals(jobCountOffset + 2, JobRepository.removeJobsWithMinAge(getCalcConnection(), 0), "There should be 2 removals");
    assertJobProgressAmount(user, 0);
  }

  @Override
  protected Connection getConnection() throws SQLException {
    return getCalcConnection();
  }

  private ConnectUser setupUser() throws SQLException, AeriusException {
    final ConnectUser user = new ConnectUser();
    user.setApiKey(TEST_API_KEY);
    user.setEmailAddress(TEST_EMAIL);
    ConnectUserRepository.createUser(getCalcConnection(), user);
    return user;
  }

  private void assertJobProgressAmount(final ConnectUser user, final int amount) throws SQLException {
    assertEquals(amount, JobRepository.getProgressForUser(getCalcConnection(), user).size(), "Couldn't find the expected amount of jobs");
  }

  private void validateEmptyProgress(final JobProgress jp) throws SQLException {
    assertNotNull(jp, "Job progress should be found");
    assertEquals(JobType.CALCULATION, jp.getType(), "Job type must be a calculation");
    assertEquals(0, jp.getHexagonCount(), "Hexagon counter must be 0");
    assertNotNull(jp.getStartDateTime(), "Start time must be set");
    assertNull(jp.getEndDateTime(), "End time may not be set");
    assertNull(jp.getResultUrl(), "Result url may not be set");
    assertNotNull(jp.getKey(), "jobKey should be present");
  }

}
