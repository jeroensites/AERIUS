/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test class for {@link AeriusExceptionMessages}.
 */
class AeriusExceptionMessagesTest {

  private static final String JUST_A_SENSTENCE = "JUST A SENSTENCE";

  @Test
  void testGetMessage() {
    final AeriusExceptionMessages bundle = new AeriusExceptionMessages(LocaleUtils.getDefaultLocale());
    final AeriusException ae666 = new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    final String ae666Message = bundle.getString(ae666);
    assertFalse(ae666Message.isEmpty(), "Check interne error");
    final AeriusException ae1001 = new AeriusException(AeriusExceptionReason.CALCULATION_NO_SOURCES);
    assertNotEquals("Check not always internal error", bundle.getString(ae1001), ae666Message);
    assertTrue(bundle.getString(ae1001).contains("{0}"), "Check contains replace toke");
    final AeriusException ae1001_2 = new AeriusException(AeriusExceptionReason.CALCULATION_NO_SOURCES, JUST_A_SENSTENCE);
    assertTrue(bundle.getString(ae1001_2).contains(JUST_A_SENSTENCE), "Check contains replaced value");
    final AeriusException ae668 = new AeriusException(AeriusExceptionReason.SQL_ERROR_CONSTANTS_MISSING);
    assertNotEquals("Check not a internal error ", bundle.getString(ae668), ae666Message);
    assertFalse(bundle.getString(ae668).isEmpty(),  "Check not empty");
  }
}
