/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.common.sector.InlandCategoryEmissionFactorKey;
import nl.overheid.aerius.db.common.sector.InlandCategoryKey;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.AbstractCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.FarmlandCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType.FuelTypeProperties;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.ShippingMovementType;
import nl.overheid.aerius.shared.emissions.shipping.ShippingLaden;

/**
 * Needs a proper database to test against, testing DB operations for categories.
 * Tests for now only check if queries work, not if the returned values are as expected. That'd require a strict test database.
 */
class CategoryRepositoriesTest extends BaseDBTest {

  private static final int YEAR = 2020;
  private static final int FARM_LODGING_TYPE_ID_WITH_ADDITIONAL = 184;
  private static final int FARM_LODGING_TYPE_ID_WITH_REDUCTIVE = 45;

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.RoadEmissionCategoryRepository#findAllRoadEmissionCategories(java.sql.Connection)}.
   * @throws SQLException
   */
  @Test
  void testFindAllRoadEmissionCategories() throws SQLException {
    final RoadEmissionCategories categories = RoadEmissionCategoryRepository.findAllRoadEmissionCategories(getCalcConnection());
    assertNotNull(categories, "Retrieved categories should not be null");
  }

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.FarmLodgingCategoryRepository#getFarmLodgingCategories}.
   * @throws SQLException
   */
  @Test
  void testGetFarmLodgingCategories() throws SQLException {
    final FarmLodgingCategories categories = FarmLodgingCategoryRepository.getFarmLodgingCategories(getCalcConnection(), getCalcMessagesKey());
    testReturnedCategories(categories.getFarmAdditionalLodgingSystemCategories());
    testReturnedCategories(categories.getFarmReductiveLodgingSystemCategories());
    testFarmLodgingCategories(categories.getFarmLodgingSystemCategories());
  }

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.FarmlandEmissionCategoryRepository#findAllFarmlandEmissionCategories}.
   * @throws SQLException
   */
  @Test
  void testFindAllFarmlandCategories() throws SQLException {
    final ArrayList<FarmlandCategory> categories =
        FarmlandEmissionCategoryRepository.findAllFarmlandEmissionCategories(getCalcConnection(), getCalcMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories);
  }

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.MobileSourceCategoryRepository#findOffRoadMobileSourceCategories(java.sql.Connection)}.
   * @throws SQLException
   */
  @Test
  void testFindOffRoadMobileSourceCategories() throws SQLException {
    final ArrayList<OffRoadMobileSourceCategory> categories =
        MobileSourceCategoryRepository.findOffRoadMobileSourceCategories(getCalcConnection(), getCalcMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories);
  }

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.MobileSourceCategoryRepository#findOffRoadMobileSourceCategories(java.sql.Connection)}.
   * @throws SQLException
   */
  @Test
  void testFindOnRoadMobileSourceCategories() throws SQLException {
    final ArrayList<OnRoadMobileSourceCategory> categories =
        MobileSourceCategoryRepository.findOnRoadMobileSourceCategories(getCalcConnection(), getCalcMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories);
  }

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.ShippingCategoryRepository#findAllMaritimeShippingCategories(Connection, nl.overheid.aerius.i18n.DBMessages.DBMessagesKey)}
   * @throws SQLException
   */
  @Test
  void testFindAllMaritimeShippingCategories() throws SQLException {
    final List<MaritimeShippingCategory> categories = getTestMaritimeShippingCategories();
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories);
  }

  @Test
  void testFindAllInlandShipCategories() throws SQLException {
    final List<InlandShippingCategory> categories = getTestInlandShippingCategories();
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories);
  }

  @Test
  void testGetInlandWaterwayCategories() throws SQLException {
    final List<InlandWaterwayCategory> categories =
        ShippingCategoryRepository.getInlandWaterwayCategories(getCalcConnection(), getCalcMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
  }

  @Test
  void testGetInlandShippingCategories() throws SQLException {
    final InlandShippingCategories categories =
        ShippingCategoryRepository.findInlandShippingCategories(getCalcConnection(), getCalcMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
    final InlandWaterwayCategory cemtCategory = categories.getWaterwayCategoryByCode("CEMT_I");
    assertNotNull(cemtCategory, "Existing waterway type: CEMT_I");
    assertEquals(1, cemtCategory.getDirections().size(), "Number of directions for CEMT_I");
    final InlandWaterwayCategory waalCategory = categories.getWaterwayCategoryByCode("Waal");
    assertNotNull(waalCategory, "Another existing waterway type: Waal");
    assertEquals(2, waalCategory.getDirections().size(), "Number of directions for Waal");
    final InlandShippingCategory shipCategory = categories.getShipCategoryByCode("C3B");
    assertNotNull(shipCategory, "Existing ship category");
    assertFalse(categories.isValidCombination(shipCategory, cemtCategory), "Ship not allowed on CEMT_I");
    assertTrue(categories.isValidCombination(shipCategory, waalCategory), "Ship allowed on Waal");
  }

  @Test
  void testGetMaritimeCategoryEmissionFactors() throws SQLException {
    final MaritimeShippingCategory category = getTestMaritimeShippingCategories().get(0);
    final Map<Substance, Double> emissionFactors =
        ShippingCategoryRepository.findMaritimeCategoryEmissionFactors(getCalcConnection(), category, ShippingMovementType.INLAND, YEAR);
    assertNotNull(emissionFactors, "Retrieved emissionFactors should not be null");
  }

  @Test
  void testGetMaritimeCategoryDockedEmissionFactors() throws SQLException {
    final MaritimeShippingCategory category = getTestMaritimeShippingCategories().get(0);
    final Map<Substance, Double> emissionFactors =
        ShippingCategoryRepository.findMaritimeCategoryDockedEmissionFactors(getCalcConnection(), category, YEAR);
    assertNotNull(emissionFactors, "Retrieved emissionFactors should not be null");
  }

  @Test
  void testGetMaritimeShippingCharacteristics() throws SQLException {
    final MaritimeShippingCategory category = getTestMaritimeShippingCategories().get(0);
    final OPSSourceCharacteristics characteristics =
        ShippingCategoryRepository.getCharacteristics(getCalcConnection(), category, ShippingMovementType.INLAND, YEAR);
    assertNotNull(characteristics, "Retrieved characteristics should not be null");
  }

  @Test
  void testGetInlandCategoryRouteEmissionFactors() throws SQLException {
    final InlandShippingCategory category = getTestInlandShippingCategories().get(0);
    final Map<InlandCategoryEmissionFactorKey, Double> emissionFactors =
        ShippingCategoryRepository.findInlandCategoryRouteEmissionFactors(getCalcConnection(), category, YEAR);
    assertNotNull(emissionFactors, "Retrieved emissionFactors should not be null");
    boolean foundLaden = false;
    boolean foundUnladen = false;
    final Set<WaterwayDirection> directions = new HashSet<>();
    for (final Entry<InlandCategoryEmissionFactorKey, Double> entry : emissionFactors.entrySet()) {
      assertNotEquals(0.0, entry.getValue().doubleValue(), 1E-6, "Retrieved emissionFactor");
      if (entry.getKey().isLaden()) {
        foundLaden = true;
      } else {
        foundUnladen = true;
      }
      directions.add(entry.getKey().getDirection());
    }
    assertTrue(foundLaden, "Expected emissionfactors for laden ships.");
    assertTrue(foundUnladen, "Expected emissionfactors for unladen ships.");
    assertEquals(WaterwayDirection.values().length, directions.size(), "Directions found " + directions);
  }

  @Test
  void testGetInlandCategoryRouteCharacteristics() throws SQLException {
    final InlandShippingCategory category = getTestInlandShippingCategories().get(0);
    final Map<InlandCategoryKey, OPSSourceCharacteristics> characteristics =
        ShippingCategoryRepository.getInlandCategoryRouteCharacteristics(getCalcConnection(), category);
    assertNotNull(characteristics, "Retrieved characteristics should not be null");
    for (final Entry<InlandCategoryKey, OPSSourceCharacteristics> entry : characteristics.entrySet()) {
      assertNotEquals(0, entry.getValue().getHeatContent(), "Retrieved heat content");
      assertNotEquals(0, entry.getValue().getEmissionHeight(), "Retrieved emission height");
    }
  }

  @Test
  void testGetInlandCategoryMooringEmissionFactors() throws SQLException {
    final InlandShippingCategory category = getTestInlandShippingCategories().get(0);
    final Map<Substance, Double> emissionFactors =
        ShippingCategoryRepository.findInlandCategoryMooringEmissionFactors(getCalcConnection(), category, YEAR);
    assertNotNull(emissionFactors, "Retrieved emissionFactors should not be null");
    for (final Entry<Substance, Double> entry : emissionFactors.entrySet()) {
      assertNotEquals(0.0, entry.getValue(), 1E-3, "Retrieved emissionFactor");
    }
  }

  @Test
  void testGetInlandCategoryMooringCharacteristics() throws SQLException {
    final InlandShippingCategory category = getTestInlandShippingCategories().get(0);
    final Map<ShippingLaden, OPSSourceCharacteristics> characteristics =
        ShippingCategoryRepository.getInlandCategoryMooringCharacteristics(getCalcConnection(), category);
    assertNotNull(characteristics, "Retrieved characteristics should not be null");
    assertEquals(2, characteristics.size(), "Should have characteristics for both laden and unladen.");
    for (final Entry<ShippingLaden, OPSSourceCharacteristics> entry : characteristics.entrySet()) {
      assertNotEquals(0, entry.getValue().getHeatContent(), "Retrieved heat content");
      assertNotEquals(0, entry.getValue().getEmissionHeight(), "Retrieved emission height");
    }
  }

  @Test
  void testFindAllPlanCategories() throws SQLException {
    final ArrayList<PlanCategory> categories =
        PlanEmissionCategoryRepository.findAllPlanEmissionCategories(getCalcConnection(), getCalcMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories);
    for (final PlanCategory category : categories) {
      assertNotNull(category.getCharacteristics(), "Returned characteristics for category " + category);
      assertNotEquals(1, category.getCharacteristics().getHeatContent(), 1E-6, "Height for category " + category);
    }
  }

  @Test
  void testFindAllOffRoadMachineryTypes() throws SQLException {
    final ArrayList<OffRoadMachineryType> categories =
        OffRoadMachineryTypesRepository.findAllOffRoadMachineryTypes(getCalcConnection(), getCalcMessagesKey());
    assertNotNull(categories, "Retrieved categories should not be null");
    testReturnedCategories(categories);
    for (final OffRoadMachineryType machineryType : categories) {
      assertFalse(machineryType.getFuelTypes().isEmpty(), "OffRoadMachineryType should not be empty:" + machineryType.getName());
      for (final FuelTypeProperties ftp : machineryType.getFuelTypes()) {
        assertNotNull(ftp.getFuelType(), "FuelType of properties should not be null:" + machineryType.getName() + ", machinery:" + ftp.getId());
      }
    }
  }

  private void testReturnedCategories(final List<? extends AbstractCategory> categories) {
    assertNotNull(categories, "Testing categories should not be null");
    final HashSet<Integer> ids = new HashSet<>();
    for (final AbstractCategory category : categories) {
      assertFalse(ids.contains(category.getId()),
          category.getClass().getSimpleName() + " category list has duplicate IDs. ID: " + category.getId());
      ids.add(category.getId());
      assertNotNull(category.getCode(), category.getClass().getSimpleName() + " code was null. ID: " + category.getId());
      assertNotNull(category.getName(), category.getClass().getSimpleName() + " name was null. ID: " + category.getId());
      //description can be null
    }
  }

  private void testFarmLodgingCategories(final List<FarmLodgingCategory> categories) {
    testReturnedCategories(categories);
    for (final FarmLodgingCategory cat : categories) {
      assertFalse(cat.getFarmLodgingSystemDefinitions().isEmpty(), cat.getClass().getSimpleName() + " has no lodging system definitions. ID: " +
              cat.getId());
      final HashSet<Integer> ids = new HashSet<>();
      for (final FarmLodgingSystemDefinition def : cat.getFarmLodgingSystemDefinitions()) {
        assertFalse(ids.contains(def.getId()), def.getClass().getSimpleName() + " category list has duplicate IDs. ID: " +
                def.getId() + ", lodging ID: " + cat.getId());
        ids.add(def.getId());
        assertNotNull(def.getCode(), def.getClass().getSimpleName() + " code was null. ID: " + def.getId() +
                ", lodging ID: " + cat.getId());
        assertNotNull(def.getName(), def.getClass().getSimpleName() + " name was null. ID: " + def.getId() +
                ", lodging ID: " + cat.getId());
        //description can be null
      }

      if (cat.getId() == FARM_LODGING_TYPE_ID_WITH_ADDITIONAL) {
        assertFalse(cat.getFarmAdditionalLodgingSystemCategories().isEmpty(), cat.getClass().getSimpleName() + " has no additional lodging systems. ID: " +
                cat.getId());
      }

      if (cat.getId() == FARM_LODGING_TYPE_ID_WITH_REDUCTIVE) {
        assertFalse(cat.getFarmReductiveLodgingSystemCategories().isEmpty(), cat.getClass().getSimpleName() + " has no reductive lodging systems. ID: " +
                cat.getId());
      }
    }
  }


  private List<MaritimeShippingCategory> getTestMaritimeShippingCategories() throws SQLException {
    return ShippingCategoryRepository.findAllMaritimeShippingCategories(getCalcConnection(), getCalcMessagesKey());
  }

  private List<InlandShippingCategory> getTestInlandShippingCategories() throws SQLException {
    return ShippingCategoryRepository.findAllInlandShippingCategories(getCalcConnection(), getCalcMessagesKey());
  }

}
