/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.db.common.GeneralRepositoryBean;
import nl.overheid.aerius.db.test.CalculationRepositoryTestBase;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.JobType;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculation;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;


public abstract class ResultsTestBase extends CalculationRepositoryTestBase {

  protected static final int ASSESSMENT_AREA_1 = 57;
  protected static final int ASSESSMENT_AREA_2 = 2;

  protected final ResultsSummaryRepository resultsRepository = new ResultsSummaryRepository(getCalcPMF(), new ReceptorUtil(RECEPTOR_GRID_SETTINGS));

  protected int jobId;
  protected SituationCalculations situationCalculations;

  protected int calculationIdProposed;
  protected Calculation calculationProposed;
  protected int calculationIdReference;
  protected Calculation calculationReference;
  protected int calculationIdTemporary;
  protected Calculation calculationTemporary;

  protected final GeneralRepositoryBean generalRepositoryBean = new GeneralRepositoryBean(getCalcPMF());

  protected void prepareTests() throws SQLException, AeriusException {
    final int calculationPointSetId = insertCalculationPoints();
    removeCalculationResults();
    createCalculation(calculationPointSetId);
    final List<CalculationPointFeature> calculationPointList = getExampleCalculationPointOutputData();

    calculationIdProposed = calculation.getCalculationId();
    calculationResultScale = 3.0;
    insertCalculationResults();
    calculationResultScale = 6.0;
    List<AeriusResultPoint> results = toAeriusResultPoints(Substance.NH3, calculationPointList);
    insertResults(results, 6);
    calculationProposed = calculation;

    newCalculation(calculationPointSetId);
    calculationIdReference = calculation.getCalculationId();
    calculationResultScale = 1.0;
    insertCalculationResults();
    calculationResultScale = 2.0;
    results = toAeriusResultPoints(Substance.NH3, calculationPointList);
    insertResults(results, 6);
    calculationReference = calculation;

    newCalculation(calculationPointSetId);
    calculationIdTemporary = calculation.getCalculationId();
    calculationResultScale = 1.5;
    insertCalculationResults();
    calculationResultScale = 3.0;
    results = toAeriusResultPoints(Substance.NH3, calculationPointList);
    insertResults(results, 6);
    calculationTemporary = calculation;

    jobId = createJob();

    situationCalculations = new SituationCalculations();
    final SituationCalculation proposedCalculation = new SituationCalculation();
    proposedCalculation.setCalculationId(calculationIdProposed);
    proposedCalculation.setSituationType(SituationType.PROPOSED);
    situationCalculations.add(proposedCalculation);

    final SituationCalculation referenceCalculation = new SituationCalculation();
    referenceCalculation.setCalculationId(calculationIdReference);
    referenceCalculation.setSituationType(SituationType.REFERENCE);
    situationCalculations.add(referenceCalculation);

    final SituationCalculation temporaryCalculation = new SituationCalculation();
    temporaryCalculation.setCalculationId(calculationIdTemporary);
    temporaryCalculation.setSituationType(SituationType.TEMPORARY);
    situationCalculations.add(temporaryCalculation);

    resultsRepository.insertResultsSummaryForArea(jobId, situationCalculations, ASSESSMENT_AREA_1);
    resultsRepository.insertResultsSummaryForArea(jobId, situationCalculations, ASSESSMENT_AREA_2);
  }

  private int createJob() throws SQLException, AeriusException {
    final String correlationIdentifier = JobRepository.createJob(getCalcConnection(), JobType.CALCULATION);
    JobRepository.attachCalculations(getCalcConnection(), correlationIdentifier, List.of(calculationProposed, calculationReference));
    return JobRepository.getJobId(getCalcConnection(), correlationIdentifier);
  }

  protected AssessmentArea determineAssessmentArea(final int assessmentAreaId) {
    final Optional<AssessmentArea> area = generalRepositoryBean.determineAssessmentArea(assessmentAreaId);
    return area.orElseGet(() -> {
      final AssessmentArea fakeArea = new AssessmentArea();
      fakeArea.setId(assessmentAreaId);
      fakeArea.setName("Gebied " + assessmentAreaId);
      return fakeArea;
    });
  }

  protected HabitatType determineHabitatType(final int habitatTypeId) {
    final Optional<HabitatType> habitatType = generalRepositoryBean.determineHabitatType(habitatTypeId);
    return habitatType.orElseGet(() -> {
      final HabitatType fakeHabitatType = new HabitatType();
      fakeHabitatType.setId(habitatTypeId);
      fakeHabitatType.setName("Habitat " + habitatTypeId);
      return fakeHabitatType;
    });
  }

  @Override
  protected Connection getConnection() throws SQLException {
    return getCalcConnection();
  }
}
