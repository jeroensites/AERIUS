/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.db.common.configuration;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.BaseTestDatabase;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link AppConfigurationRepository}.
 */
class AppConfigurationRepositoryTest extends BaseTestDatabase {

  @Test
  void testGetCalculatorContext() throws SQLException, AeriusException {
    try (Connection con = getCalcConnection()) {
      final ApplicationConfiguration config = AppConfigurationRepository.getAppConfiguration(con, getCalcMessagesKey(), AeriusCustomer.RIVM);
      assertNotNull(config, "Context for calculator");
      validateBaseContext(config);
    }
  }


  private void validateBaseContext(final ApplicationConfiguration configuration) {
    final ProductType product = ProductType.CALCULATOR;
    final SectorCategories categories = configuration.getSectorCategories();

    assertNotNull(categories.getSectors(), "sectors for " + product);
    assertNotNull(categories.getFarmLodgingCategories(), "farmLodgingCategories for " + product);
    assertNotNull(categories.getRoadEmissionCategories(), "roadEmissionCategories for " + product);
    assertNotNull(categories.getOnRoadMobileSourceCategories(), "onRoadMobileSourceCategories for " + product);
    assertNotNull(categories.getOffRoadMobileSourceCategories(), "offRoadMobileSourceCategories for " + product);
    assertNotNull(categories.getPlanEmissionCategories(), "planEmissionCategories for " + product);
    assertNotNull(categories.getMaritimeShippingCategories(), "maritimeShippingCategories for " + product);
    assertNotNull(categories.getInlandShippingCategories(), "inlandShippingCategories for " + product);
    assertNotNull(categories.getShippingSnappableNodes(), "shippingSnappableNodes for " + product);
    assertNotNull(categories.getOffRoadMachineryTypes(), "offRoadMachineryTypes for " + product);
  }

}
