/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.test.CalculationRepositoryTestBase;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationResultSetType;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;

/**
 * Needs a proper database to test against, testing DB operations for calculations.
 */
class CalculationInfoRepositoryTest extends CalculationRepositoryTestBase {

  private static final int RECEPTOR_1_ASSESSMENT_AREA_ID = 57;
  private static final int RECEPTOR_2_ASSESSMENT_AREA_ID = 2;
  private static final int RECEPTOR_1_HABITAT_ID = 124;
  private static final int RECEPTOR_2_HABITAT_ID = 56;

  private static final Substance TEST_SUBSTANCE_2 = Substance.NH3;

  @Test
  void testGetPartialCalculationResults() throws SQLException {
    final List<AeriusResultPoint> customPointResults = getTestCustomPointResults();
    final List<AeriusResultPoint> referenceResult = getTestReferenceResults(customPointResults);

    final PartialCalculationResult emptyResult = CalculationInfoRepository.getCalculationResults(getCalcConnection(), -1);
    assertNotNull(emptyResult, "Calculation result");
    assertEquals(0, emptyResult.getResults().size(), "Number of receptor points in empty calculation result");

    final PartialCalculationResult partialResult = CalculationInfoRepository.getCalculationResults(getCalcConnection(),
        calculation.getCalculationId());
    assertNotNull(partialResult, "Calculation result");
    assertEquals(referenceResult.size(), partialResult.getResults().size(), "Number of result points");

    matchCalculationResults(referenceResult, customPointResults, partialResult);
  }

  @Test
  void testGetPartialCalculationSectorResults() throws SQLException {
    final List<AeriusResultPoint> customPointResults = getTestCustomPointResults();
    final List<AeriusResultPoint> referenceResult = getTestReferenceResults(customPointResults);

    final PartialCalculationResult emptyResult = CalculationInfoRepository.getCalculationSectorResults(getCalcConnection(), -1,
        TEST_CALCULATION_RESULTS_SECTOR_ID);
    assertNotNull(emptyResult, "Calculation result");
    assertEquals(0, emptyResult.getResults().size(), "Number of receptor points in empty calculation result");

    final PartialCalculationResult partialResult = CalculationInfoRepository.getCalculationSectorResults(getCalcConnection(),
        calculation.getCalculationId(), TEST_CALCULATION_RESULTS_SECTOR_ID);
    assertNotNull(partialResult, "Calculation result");
    assertEquals(referenceResult.size(), partialResult.getResults().size(), "Number of result points");

    matchCalculationResults(referenceResult, customPointResults, partialResult);
  }

  private List<AeriusResultPoint> getTestCustomPointResults() throws SQLException {
    final int calculationPointSetId = insertCalculationPoints();
    //trick to set the right calculation point set id:
    //The calc id returned from insertCalculationPoints is actually the calculation point set ID...
    removeCalculationResults();
    createCalculation(calculationPointSetId);
    final List<CalculationPointFeature> calculationPointList = getExampleCalculationPointOutputData();
    final List<AeriusResultPoint> results = toAeriusResultPoints(TEST_SUBSTANCE_2, calculationPointList);
    CalculationRepository.insertCalculationResults(getCalcConnection(), calculation.getCalculationId(), results);
    CalculationRepository.insertCalculationResults(getCalcConnection(), calculation.getCalculationId(), CalculationResultSetType.SECTOR,
        TEST_CALCULATION_RESULTS_SECTOR_ID, results);
    return results;
  }

  private List<AeriusResultPoint> getTestReferenceResults(final List<AeriusResultPoint> customPointResults) throws SQLException {
    insertCalculationResults();
    final List<AeriusResultPoint> referenceResult = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION);
    referenceResult.addAll(customPointResults);
    return referenceResult;
  }

  private void matchCalculationResults(final List<AeriusResultPoint> referenceResult, final List<AeriusResultPoint> customPointResults,
      final PartialCalculationResult partialCalculationResult) {
    int matched = 0;
    int matchedCustomResultPoints = 0;
    for (final CalculationPointFeature point : partialCalculationResult.getResults()) {
      for (final AeriusResultPoint referencePoint : referenceResult) {
        if (referencePoint.getId() == point.getProperties().getId()) {
          matched++;
          if (referencePoint.getPointType() == AeriusPointType.POINT) {
            matchedCustomResultPoints++;
            assertNotNull(point.getProperties().getLabel(), "Custom point label");
            assertEquals(referencePoint.getLabel(), point.getProperties().getLabel(), "Custom point label");
            assertEquals(referencePoint.getX(), point.getGeometry().getX(), 1E-1, "x coord custom point");
            assertEquals(referencePoint.getY(), point.getGeometry().getY(), 1E-1, "y coord custom point");
          }
          assertEquals(referencePoint.getEmissionResult(EmissionResultKey.NH3_CONCENTRATION), point.getProperties().getResults().get(EmissionResultKey.NH3_CONCENTRATION),
              1E-3, "Concentration");
          assertEquals(referencePoint.getEmissionResult(EmissionResultKey.NH3_DEPOSITION), point.getProperties().getResults().get(EmissionResultKey.NH3_DEPOSITION),
              1E-3, "Deposition");
        }
      }
    }
    assertEquals(referenceResult.size(), matched, "Number of matched points");
    assertEquals(customPointResults.size(), matchedCustomResultPoints, "Number of matched custom points");
  }

  @Test
  void testGetEmissionResults() throws SQLException {
    insertCalculationResults();

    final Map<EmissionResultKey, Double> noEmissionResults = CalculationInfoRepository.getEmissionResults(getCalcConnection(),
        calculation.getCalculationId(), -1);
    assertTrue(noEmissionResults.isEmpty(), "No emission results for non-existing receptor ID");
    final Map<EmissionResultKey, Double> emissionResults = CalculationInfoRepository.getEmissionResults(getCalcConnection(),
        calculation.getCalculationId(), RECEPTOR_POINT_ID_1);
    assertEquals(RECEPTOR_1_DEPOSITION, emissionResults.get(EmissionResultKey.NH3_DEPOSITION), 0.00001, "Emission results");
  }

  @Test
  void testDetermineBoundingBox() throws SQLException {
    BBox boundingBox = CalculationInfoRepository.determineBoundingBox(getCalcConnection(), calculation.getCalculationId());
    assertNotNull(boundingBox, "boundingBox");
    assertEquals(0, boundingBox.getMinX(), 1, "Min x when no results");
    assertEquals(0, boundingBox.getMaxX(), 1, "Max x when no results");
    assertEquals(0, boundingBox.getMinY(), 1, "Min y when no results");
    assertEquals(0, boundingBox.getMaxY(), 1, "Max y when no results");

    insertCalculationResults();

    boundingBox = CalculationInfoRepository.determineBoundingBox(getCalcConnection(), calculation.getCalculationId());
    assertNotNull(boundingBox, "boundingBox");

    assertEquals(112174, boundingBox.getMinX(), 1, "Min x with results");
    assertEquals(187150, boundingBox.getMaxX(), 1, "Max x with results");
    assertEquals(463895, boundingBox.getMinY(), 1, "Min y with results");
    assertEquals(557060, boundingBox.getMaxY(), 1, "Max y with results");
  }

  @Test
  void testGetCalculationAssessmentHabitatReceptors() throws SQLException {
    insertCalculationResults();

    Map<Integer, Double> results = CalculationInfoRepository.getCalculationAssessmentHabitatReceptors(getCalcConnection(),
        calculation.getCalculationId(),
        RECEPTOR_1_ASSESSMENT_AREA_ID, 1);
    assertNotNull(results, "Result with incorrect habitat id");
    assertTrue(results.isEmpty(), "Number of results with incorrect habitat id");

    results = CalculationInfoRepository.getCalculationAssessmentHabitatReceptors(getCalcConnection(), calculation.getCalculationId(),
        1, RECEPTOR_1_HABITAT_ID);
    assertNotNull(results, "Result with incorrect assessment area id");
    assertTrue(results.isEmpty(), "Number of results with incorrect assessment area id");

    results = CalculationInfoRepository.getCalculationAssessmentHabitatReceptors(getCalcConnection(), calculation.getCalculationId(),
        RECEPTOR_1_ASSESSMENT_AREA_ID, RECEPTOR_1_HABITAT_ID);
    assertNotNull(results, "Result");
    assertEquals(1, results.size(), "Number of results");
    assertEquals(RECEPTOR_POINT_ID_1, results.keySet().iterator().next().intValue(), "Receptor point found");
    assertEquals(10000.0, results.get(RECEPTOR_POINT_ID_1), 1, "Surface for receptor 1");

    results = CalculationInfoRepository.getCalculationAssessmentHabitatReceptors(getCalcConnection(), calculation.getCalculationId(),
        RECEPTOR_2_ASSESSMENT_AREA_ID, RECEPTOR_2_HABITAT_ID);
    assertNotNull(results, "Result for receptor 2");
    assertEquals(1, results.size(), "Number of results for receptor 2");
    assertEquals(RECEPTOR_POINT_ID_2, results.keySet().iterator().next().intValue(), "Receptor point found for receptor 2");
    assertEquals(830.943, results.get(RECEPTOR_POINT_ID_2), 1, "Surface for receptor 2");
  }

  @Override
  protected Connection getConnection() throws SQLException {
    return getCalcConnection();
  }

}
