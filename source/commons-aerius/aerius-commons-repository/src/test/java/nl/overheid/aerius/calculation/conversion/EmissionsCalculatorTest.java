/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.jupiter.api.BeforeAll;

import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link EmissionsCalculator} class.
 * @deprecated should be replaced by EmissionsCalculator in imaer-shared
 */
@Deprecated
public class EmissionsCalculatorTest extends BaseDBTest {

  private static final EmissionValueKey KEY_NOX = new EmissionValueKey(2020, Substance.NOX);
  private static TestDomain testDomain;

  @BeforeAll
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    testDomain = new TestDomain(getCalcPMF());
  }

//  @Test
//  public void testGetEmissionsMaritimeMooring() throws SQLException, AeriusException {
//    final MaritimeMooringEmissionSource emissionSource = testDomain.getMooringMaritimeShipEmissionSource(new MaritimeMooringEmissionSource());
//    for (final MooringMaritimeVesselGroup vesselGroup : emissionSource.getEmissionSubSources()) {
//      final EmissionValues emissions = EmissionsCalculator.getEmissions(getCalcConnection(), vesselGroup, null, KEY_NOX.hatch(), null);
//      //VesselGroup will return values.
//      validateReturnedEmissions(emissions, "MooringMaritimeVesselGroup");
//      //Using a geometry for 2nd argument matters not the slightest for mooring.
//      final EmissionValues emissionsWithGeometry =
//          EmissionsCalculator.getEmissions(getCalcConnection(), vesselGroup, getExampleGeometry(1), KEY_NOX.hatch(), null);
//      validateReturnedEmissions(emissionsWithGeometry, "MooringMaritimeVesselGroup with geometry");
//      for (final Entry<EmissionValueKey, Double> entry : emissions.entrySet()) {
//        assertEquals("Returned emission wasn't the same when using a source geometry. EVK: " + entry.getKey(),
//            entry.getValue().doubleValue(),
//            emissionsWithGeometry.getEmission(entry.getKey()), 0.001);
//      }
//    }
//  }
//
//  @Test
//  public void testGetEmissionsMaritimeRoute() throws SQLException, AeriusException {
//    final MaritimeRouteEmissionSource maritimeRouteEmissionValues = testDomain.getShipEmissionRouteSource();
//    for (final RouteMaritimeVesselGroup vesselGroup : maritimeRouteEmissionValues.getEmissionSubSources()) {
//      final EmissionValues emissions = EmissionsCalculator.getEmissions(getCalcConnection(), vesselGroup, getExampleGeometry(1), KEY_NOX.hatch(),
//          null);
//      //VesselGroup will return values.
//      validateReturnedEmissions(emissions, "RouteMaritimeVesselGroup");
//      final EmissionValues emissionsOtherGeometry =
//          EmissionsCalculator.getEmissions(getCalcConnection(), vesselGroup, getExampleGeometry(2), KEY_NOX.hatch(), null);
//      validateReturnedEmissions(emissionsOtherGeometry, "RouteMaritimeVesselGroup with other geometry");
//      for (final Entry<EmissionValueKey, Double> entry : emissions.entrySet()) {
//        assertNotEquals("Returned emission was the same when using a different source geometry. EVK: " + entry.getKey(),
//            entry.getValue().doubleValue(),
//            emissionsOtherGeometry.getEmission(entry.getKey()), 0.001);
//      }
//    }
//  }
//
//  @Test
//  public void testGetEmissionsInlandRouteOldStyle() throws SQLException, AeriusException {
//    final InlandRouteEmissionSource inlandRouteEmissionValues = testDomain.getInlandRouteEmissionValues();
//    inlandRouteEmissionValues.setWaterwayCategory(null);
//    for (final RouteInlandVesselGroup vesselGroup : inlandRouteEmissionValues.getEmissionSubSources()) {
//      final EmissionValues emissions = EmissionsCalculator.getEmissions(getCalcConnection(), vesselGroup, getExampleGeometry(1), KEY_NOX.hatch(),
//          inlandRouteEmissionValues);
//      //VesselGroup will return values.
//      validateReturnedEmissions(emissions, "RouteInlandVesselGroup");
//      final EmissionValues emissionsOtherGeometry =
//          EmissionsCalculator.getEmissions(getCalcConnection(), vesselGroup, getExampleGeometry(2), KEY_NOX.hatch(), inlandRouteEmissionValues);
//      validateReturnedEmissions(emissionsOtherGeometry, "RouteInlandVesselGroup with other geometry");
//      for (final Entry<EmissionValueKey, Double> entry : emissions.entrySet()) {
//        assertNotEquals("Returned emission was the same when using a different source geometry. EVK: " + entry.getKey(),
//            entry.getValue().doubleValue(),
//            emissionsOtherGeometry.getEmission(entry.getKey()), 0.001);
//      }
//    }
//  }
//
//  @Test
//  public void testGetEmissionsInlandRouteWithWaterways() throws SQLException, AeriusException {
//    final InlandRouteEmissionSource inlandRouteEmissionValues = testDomain.getInlandRouteEmissionValues();
//    for (final RouteInlandVesselGroup vesselGroup : inlandRouteEmissionValues.getEmissionSubSources()) {
//      final EmissionValues emissions = EmissionsCalculator.getEmissions(getCalcConnection(), vesselGroup, getExampleGeometry(1), KEY_NOX.hatch(),
//          inlandRouteEmissionValues);
//      //VesselGroup will return values.
//      validateReturnedEmissions(emissions, "RouteInlandVesselGroup");
//      final EmissionValues emissionsOtherGeometry =
//          EmissionsCalculator.getEmissions(getCalcConnection(), vesselGroup, getExampleGeometry(2), KEY_NOX.hatch(), inlandRouteEmissionValues);
//      validateReturnedEmissions(emissionsOtherGeometry, "RouteInlandVesselGroup with other geometry");
//      for (final Entry<EmissionValueKey, Double> entry : emissions.entrySet()) {
//        assertNotEquals("Returned emission was the same when using a different source geometry. EVK: " + entry.getKey(),
//            entry.getValue().doubleValue(),
//            emissionsOtherGeometry.getEmission(entry.getKey()), 0.001);
//      }
//    }
//  }
//
//  @Test
//  public void testGetEmissionsInlandMooring() throws SQLException, AeriusException {
//    final WKTGeometry routeGeometry = getExampleGeometry(1);
//    final ShippingRoute route = new ShippingRoute();
//    route.setGeometry(routeGeometry);
//    final InlandMooringEmissionSource inlandMooringEmissionValues = testDomain.getInlandMooringEmissionSource(route);
//    for (final InlandMooringVesselGroup vesselGroup : inlandMooringEmissionValues.getEmissionSubSources()) {
//      final EmissionValues emissions = EmissionsCalculator.getEmissions(getCalcConnection(), vesselGroup, null, KEY_NOX.hatch(),
//          null);
//      //VesselGroup will return values.
//      validateReturnedEmissions(emissions, "InlandMooringVesselGroup");
//      //Using a geometry for 2nd argument matters not the slightest for mooring.
//      final EmissionValues emissionsWithGeometry =
//          EmissionsCalculator.getEmissions(getCalcConnection(), vesselGroup, getExampleGeometry(1), KEY_NOX.hatch(), null);
//      validateReturnedEmissions(emissionsWithGeometry, "InlandMooringVesselGroup with geometry");
//      for (final Entry<EmissionValueKey, Double> entry : emissions.entrySet()) {
//        assertEquals("Returned emission wasn't the same when using a source geometry. EVK: " + entry.getKey(),
//            entry.getValue().doubleValue(),
//            emissionsWithGeometry.getEmission(entry.getKey()), 0.001);
//      }
//    }
//  }
//
//  private WKTGeometry getExampleGeometry(final int kilometer) {
//    final int meter = kilometer * 1000;
//    final WKTGeometry geometry = new WKTGeometry("LINESTRING(80000 435000," + (80000 + meter) + " 435000)");
//    geometry.setMeasure(meter);
//    return geometry;
//  }
//
//  private void validateReturnedEmissions(final EmissionValues emissions, final String description) {
//    assertNotNull("Returned emissions map was null for " + description, emissions);
//    assertFalse("Returned emissions map was empty for " + description, emissions.entrySet().isEmpty());
//    for (final Entry<EmissionValueKey, Double> entry : emissions.entrySet()) {
//      assertTrue("Returned emission was 0 for " + description + ". EVK: " + entry.getKey(), entry.getValue() > 0);
//    }
//  }
//
//  @Test
//  public void testSetEmissions() throws SQLException, AeriusException {
//    final ArrayList<EmissionSource> sources = new ArrayList<>();
//
//    final MaritimeMooringEmissionSource emissionSource = testDomain.getMooringMaritimeShipEmissionSource(new MaritimeMooringEmissionSource());
//    sources.add(emissionSource);
//
//    for (final EmissionSource source : sources) {
//      assertEquals("Without setting emissions for " + source, 0.0, emissionSource.getEmission(KEY_NOX), 1E-3);
//    }
//    EmissionsCalculator.setEmissions(getCalcConnection(), sources, KEY_NOX.hatch());
//    for (final EmissionSource source : sources) {
//      assertNotEquals("After setting emissions for " + source, 0.0, emissionSource.getEmission(KEY_NOX), 1E-3);
//    }
//  }

}
