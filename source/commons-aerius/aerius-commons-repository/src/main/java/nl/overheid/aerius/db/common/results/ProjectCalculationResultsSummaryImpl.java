/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.summary.ProjectSituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SituationResultsAreaSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

class ProjectCalculationResultsSummaryImpl extends ResultsSummaryImpl<ProjectCalculationResultsSummaryInput> {

  private static final Logger LOG = LoggerFactory.getLogger(ProjectCalculationResultsSummaryImpl.class);

  private static final StaticWhereClause SITUATION_RESULT_TYPE =
      new StaticWhereClause(RepositoryAttribute.RESULT_TYPE.name() + " = '" + ScenarioResultType.SITUATION_RESULT.name().toLowerCase() + "'");

  private static final Query QUERY_OVERALL_DEPOSITION_STATISTICS = QueryBuilder
      .from("ae_scenario_project_calculation_statistics(?, ?, ?)", QueryAttribute.PROPOSED_CALCULATION_ID,
          QueryAttribute.REFERENCE_CALCULATION_ID, QueryAttribute.NETTING_CALCULATION_ID)
      .where(WHERE_HEXAGON_TYPE)
      .getQuery();

  private static final Query QUERY_AREA_DEPOSITION_STATISTICS = QueryBuilder
      .from("scenario_calculation_assessment_area_statistics")
      .select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.HEXAGON_TYPE, RepositoryAttribute.RESULT_STATISTIC_TYPE, QueryAttribute.VALUE)
      .where(QueryAttribute.CALCULATION_ID)
      .where(WHERE_HEXAGON_TYPE)
      .where(WHERE_RESULT_TYPE)
      .getQuery();

  private static final Query QUERY_SITUATION_RESULT_AREAS = QueryBuilder
      .from("scenario_calculation_assessment_area_statistics")
      .select(QueryAttribute.ASSESSMENT_AREA_ID)
      .distinct()
      .where(QueryAttribute.CALCULATION_ID)
      .where(WHERE_HEXAGON_TYPE)
      .where(SITUATION_RESULT_TYPE)
      .getQuery();

  private static final Query QUERY_AREA_CHART_STATISTICS = QueryBuilder
      .from("scenario_calculation_assessment_area_chart_statistics")
      .select(RepositoryAttribute.LOWER_BOUND, QueryAttribute.CARTOGRAPHIC_SURFACE)
      .where(QueryAttribute.CALCULATION_ID, QueryAttribute.ASSESSMENT_AREA_ID)
      .where(WHERE_HEXAGON_TYPE)
      .where(WHERE_RESULT_TYPE)
      .getQuery();

  private static final Query QUERY_AREA_DEPOSITION_STATISTIC_MARKERS = QueryBuilder
      .from("scenario_calculation_assessment_area_statistic_markers")
      .select(RepositoryAttribute.RESULT_STATISTIC_TYPE, QueryAttribute.RECEPTOR_ID)
      .where(QueryAttribute.CALCULATION_ID)
      .where(WHERE_HEXAGON_TYPE)
      .where(WHERE_RESULT_TYPE)
      .getQuery();

  private static final Query QUERY_HABITAT_DEPOSITION_STATISTICS = QueryBuilder
      .from("scenario_calculation_critical_deposition_area_statistics")
      .select(QueryAttribute.CRITICAL_DEPOSITION_AREA_ID, RepositoryAttribute.HEXAGON_TYPE, RepositoryAttribute.RESULT_STATISTIC_TYPE,
          QueryAttribute.VALUE)
      .where(QueryAttribute.CALCULATION_ID, QueryAttribute.ASSESSMENT_AREA_ID)
      .where(WHERE_HEXAGON_TYPE)
      .where(WHERE_RESULT_TYPE)
      .getQuery();

  private static final Query QUERY_RECEPTOR_STATISTICS = QueryBuilder
      .from("ae_scenario_calculation_receptor_statistics(?::scenario_result_type, ?)", QueryAttribute.RESULT_TYPE, QueryAttribute.CALCULATION_ID)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.RESULT_STATISTIC_TYPE, QueryAttribute.RECEPTOR_ID)
      .where(WHERE_HEXAGON_TYPE)
      .getQuery();

  private static final Query QUERY_CALCULATION_POINTS = QueryBuilder
      .from("ae_scenario_calculation_point_project_results(?, ?, ?)", QueryAttribute.PROPOSED_CALCULATION_ID,
          QueryAttribute.REFERENCE_CALCULATION_ID, QueryAttribute.NETTING_CALCULATION_ID)
      .select(QueryAttribute.CALCULATION_POINT_ID, RepositoryAttribute.DEPOSITION)
      .getQuery();

  private static final Query INSERT_AREA_DEPOSITION_STATISTICS = QueryBuilder
      .from("ae_scenario_project_calculation_fill_calculation_statistics(?, ?, ?, ?, ?)", QueryAttribute.JOB_ID,
          QueryAttribute.PROPOSED_CALCULATION_ID, QueryAttribute.REFERENCE_CALCULATION_ID, QueryAttribute.NETTING_CALCULATION_ID,
          QueryAttribute.ASSESSMENT_AREA_ID)
      .getQuery();

  public ProjectCalculationResultsSummaryImpl(final PMF pmf, final ReceptorUtil receptorUtil) {
    super(pmf, receptorUtil);
  }

  @Override
  public SituationResultsSummary determineReceptorResultsSummary(final SituationCalculations situationCalculations, final int jobId,
      final Integer calculationId, final SummaryHexagonType hexagonType, final IntFunction<AssessmentArea> assessmentAreaRetriever,
      final IntFunction<HabitatType> habitatTypeRetriever) throws AeriusException {

    final SituationResultsSummary srs = super.determineReceptorResultsSummary(situationCalculations, jobId, calculationId, hexagonType,
        assessmentAreaRetriever, habitatTypeRetriever);
    final ProjectCalculationResultsSummaryInput summaryInput = createSummaryInput(situationCalculations, calculationId);

    try (final Connection con = pmf.getConnection()) {
      final List<AssessmentArea> omittedAreas = determineOmittedAreas(con, summaryInput, hexagonType, assessmentAreaRetriever,
          srs.getAreaStatistics());

      return new ProjectSituationResultsSummary(srs.getStatistics(), srs.getAreaStatistics(), srs.getMarkers(), srs.getReceptors(),
          srs.getCustomPointResults(), omittedAreas);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting results summary", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  @Override
  protected ProjectCalculationResultsSummaryInput createSummaryInput(final SituationCalculations situationCalculations, final Integer calculationId) {
    return ProjectCalculationResultsSummaryInput.fromSituationCalculations(situationCalculations, calculationId);
  }

  @Override
  protected List<ProjectCalculationResultsSummaryInput> createSummaryInputs(final SituationCalculations situationCalculations) {
    return situationCalculations.getCalculationIdsOfSituationType(SituationType.PROPOSED).stream()
        .map(proposedCalculationId -> createSummaryInput(situationCalculations, proposedCalculationId))
        .collect(Collectors.toList());
  }

  @Override
  protected PreparedStatement prepareOverallStatisticsStatement(final Connection connection, final int jobId,
      final ProjectCalculationResultsSummaryInput summaryInput, final SummaryHexagonType hexagonType) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_OVERALL_DEPOSITION_STATISTICS.get());
    QUERY_OVERALL_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.PROPOSED_CALCULATION_ID, summaryInput.getProposedCalculationId());
    QUERY_OVERALL_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.REFERENCE_CALCULATION_ID, summaryInput.getReferenceCalculationId());
    QUERY_OVERALL_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.NETTING_CALCULATION_ID, summaryInput.getNettingCalculationId());
    QUERY_OVERALL_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    return stmt;
  }

  @Override
  protected PreparedStatement prepareAreaStatisticsStatement(final Connection connection, final int jobId,
      final ProjectCalculationResultsSummaryInput summaryInput, final SummaryHexagonType hexagonType) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_AREA_DEPOSITION_STATISTICS.get());
    QUERY_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput.getProposedCalculationId());
    QUERY_AREA_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.RESULT_TYPE, ScenarioResultType.PROJECT_CALCULATION.name().toLowerCase());
    QUERY_AREA_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    return stmt;
  }

  @Override
  protected PreparedStatement prepareHabitatStatisticsStatement(final Connection connection, final int jobId,
      final ProjectCalculationResultsSummaryInput summaryInput, final SummaryHexagonType hexagonType, final int assessmentAreaId)
      throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_HABITAT_DEPOSITION_STATISTICS.get());
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput.getProposedCalculationId());
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.RESULT_TYPE,
        ScenarioResultType.PROJECT_CALCULATION.name().toLowerCase());
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);
    return stmt;
  }

  @Override
  protected PreparedStatement prepareChartStatisticsStatement(final Connection connection, final int jobId,
      final ProjectCalculationResultsSummaryInput summaryInput, final SummaryHexagonType hexagonType, final int assessmentAreaId)
      throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_AREA_CHART_STATISTICS.get());
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput.getProposedCalculationId());
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, RepositoryAttribute.RESULT_TYPE, ScenarioResultType.PROJECT_CALCULATION.name().toLowerCase());
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);
    return stmt;
  }

  @Override
  protected PreparedStatement prepareStatisticMarkersStatement(final Connection connection, final int jobId,
      final ProjectCalculationResultsSummaryInput summaryInput, final SummaryHexagonType hexagonType) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.get());
    QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput.getProposedCalculationId());
    QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.setParameter(stmt, RepositoryAttribute.RESULT_TYPE,
        ScenarioResultType.PROJECT_CALCULATION.name().toLowerCase());
    QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    return stmt;
  }

  @Override
  protected PreparedStatement prepareReceptorStatisticsStatement(final Connection connection, final int jobId,
      final ProjectCalculationResultsSummaryInput summaryInput, final SummaryHexagonType hexagonType) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_RECEPTOR_STATISTICS.get());
    QUERY_RECEPTOR_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput.getProposedCalculationId());
    QUERY_RECEPTOR_STATISTICS.setParameter(stmt, QueryAttribute.RESULT_TYPE, ScenarioResultType.PROJECT_CALCULATION.name().toLowerCase());
    QUERY_RECEPTOR_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    return stmt;
  }

  @Override
  protected PreparedStatement prepareCustomCalculationPointResultsStatement(final Connection connection,
      final ProjectCalculationResultsSummaryInput summaryInput) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_CALCULATION_POINTS.get());
    QUERY_CALCULATION_POINTS.setParameter(stmt, QueryAttribute.PROPOSED_CALCULATION_ID, summaryInput.getProposedCalculationId());
    QUERY_CALCULATION_POINTS.setParameter(stmt, QueryAttribute.REFERENCE_CALCULATION_ID, summaryInput.getReferenceCalculationId());
    QUERY_CALCULATION_POINTS.setParameter(stmt, QueryAttribute.NETTING_CALCULATION_ID, summaryInput.getNettingCalculationId());
    return stmt;
  }

  @Override
  protected PreparedStatement prepareInsertAreaResultsSummaryStatement(final Connection connection, final int jobId,
      final ProjectCalculationResultsSummaryInput summaryInput, final int assessmentAreaId) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(INSERT_AREA_DEPOSITION_STATISTICS.get());
    INSERT_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.JOB_ID, jobId);
    INSERT_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.PROPOSED_CALCULATION_ID, summaryInput.getProposedCalculationId());
    INSERT_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.REFERENCE_CALCULATION_ID, summaryInput.getReferenceCalculationId());
    INSERT_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.NETTING_CALCULATION_ID, summaryInput.getNettingCalculationId());
    INSERT_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);
    return stmt;
  }

  @Override
  protected int compare(final SituationResultsStatistics statistics1, final SituationResultsStatistics statistics2) {
    int sorted = compareValues(
        statistics1.get(ResultStatisticType.MAX_INCREASE),
        statistics2.get(ResultStatisticType.MAX_INCREASE));
    if (sorted == 0) {
      sorted = compareValues(
          statistics1.get(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE),
          statistics2.get(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE));
    }
    return sorted;
  }

  private List<AssessmentArea> determineOmittedAreas(final Connection con, final ProjectCalculationResultsSummaryInput summaryInput,
      final SummaryHexagonType hexagonType, final IntFunction<AssessmentArea> assessmentAreaRetriever,
      final List<SituationResultsAreaSummary> areaStatistics) throws SQLException {
    final Set<Integer> areaStatisticsIdentifiers = areaStatistics.stream()
        .map(SituationResultsAreaSummary::getAssessmentArea)
        .map(AssessmentArea::getId)
        .collect(Collectors.toSet());
    final List<AssessmentArea> omittedAreas = new ArrayList<>();

    try (final PreparedStatement preparedStatement = prepareSituationResultAreasStatement(con, summaryInput, hexagonType)) {
      try (final ResultSet rs = preparedStatement.executeQuery()) {
        while (rs.next()) {
          final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
          if (!areaStatisticsIdentifiers.contains(assessmentAreaId)) {
            omittedAreas.add(assessmentAreaRetriever.apply(assessmentAreaId));
          }
        }
      }
    }
    return omittedAreas;
  }

  private PreparedStatement prepareSituationResultAreasStatement(final Connection connection,
      final ProjectCalculationResultsSummaryInput summaryInput, final SummaryHexagonType hexagonType) throws SQLException {

    final PreparedStatement stmt = connection.prepareStatement(QUERY_SITUATION_RESULT_AREAS.get());
    QUERY_SITUATION_RESULT_AREAS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput.getProposedCalculationId());
    QUERY_SITUATION_RESULT_AREAS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());

    return stmt;
  }
}
