/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.configuration;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jsinterop.annotations.JsProperty;

import nl.aerius.search.domain.SearchCapability;
import nl.aerius.search.domain.SearchRegion;
import nl.overheid.aerius.db.common.ColorRangesRepository;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.layer.LayerRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.config.AppThemeConfiguration;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.ResultView;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Abstract base class for fill theme specific configurations.
 *
 * Override methods to return another value as the default provided in this class.
 */
public abstract class AbstractAppThemeConfigRepository {

  private static final List<ResultView> RESULT_VIEWS = Arrays.asList(ResultView.DEPOSITION_DISTRIBUTION, ResultView.MARKERS,
      ResultView.HABITAT_TYPES);
  private static final List<CalculationType> CALCULATION_TYPES = Arrays.asList(CalculationType.PERMIT, CalculationType.CUSTOM_POINTS);
  private static final List<Substance> EMISSION_SUBSTANCES_ROAD = Arrays.asList(Substance.NOX, Substance.NO2, Substance.NH3);
  private static final List<Substance> SUBSTANCES = Arrays.asList(Substance.NOX, Substance.NH3);
  private static final List<SituationType> AVAILABLE_SITUATION_TYPES = Arrays.asList(SituationType.REFERENCE, SituationType.TEMPORARY,
      SituationType.PROPOSED, SituationType.NETTING);
  private static final @JsProperty List<SectorGroup> ALLOWED_SECTORGROUPS = Arrays.asList(SectorGroup.ENERGY, SectorGroup.AGRICULTURE,
      SectorGroup.ROAD_TRANSPORTATION, SectorGroup.INDUSTRY, SectorGroup.LIVE_AND_WORK, SectorGroup.RAIL_TRANSPORTATION,
      SectorGroup.AVIATION, SectorGroup.SHIPPING, SectorGroup.MOBILE_EQUIPMENT, SectorGroup.OTHER); // TODO PLAN
  private static final int MAX_NUMBER_OF_SITUATIONS = 6;

  // Note: the functionality for importing assessment points directly into the list in the interface depends on this limit being 1. If this limit
  // is increased, then the warning for files that contain no custom assessment points won't trigger for the second and subsequent files.
  private static final int MAX_NUMBER_OF_FILES_SITUATION_IMPORT = 1;
  private static final String DEFAULT_METEO_YEAR = "Meerjarige meteorologie";

  /**
   * Fills the theme configuration.
   *
   * @param con
   * @param messagesKey
   * @return
   * @throws SQLException
   * @throws AeriusException
   */
  public final AppThemeConfiguration fill(final Connection con, final DBMessagesKey messagesKey) throws SQLException, AeriusException {
    final AppThemeConfiguration config = new AppThemeConfiguration(getTheme());

    config.setCharacteristicsType(getCharacteristicsType());
    config.setCalculationYears(getCalculationYears(con));
    config.setCalculationYearDefault(getCalculationYearDefault());
    config.setAvailableSituationTypes(getAvailableSituationTypes());
    config.setEmissionResultKeys(getEmissionResultKeys());
    config.setSubstances(getSubstances());
    config.setEmissionSubstancesRoad(getEmissionSubstancesRoad());
    config.setImportSubstances(getImportSubstances());
    config.setCalculationTypes(getCalculationTypes());
    config.setDefaultCalculationType(getDefaultCalculationType());
    config.setResultViews(getResultViews());
    config.setSectorGroups(getSectorGroups());
    config.setEmissionResultValueDisplaySettings(getEmissionResultValueDisplaySettings(con));
    config.setMaxNumberOfSituationsPerType(getMaxNumberOfSituationsPerType());
    config.setMaxNumberOfSituations(getMaxNumberOfSituations());
    config.setMaxNumberOfFilesSituationImport(getMaxNumberOfFilesSituationImport());
    config.setMeteoYears(getMeteoYears());
    config.setDefaultMeteoYear(getDefaultMeteoYear());
    config.setSearchRegion(getSearchRegion());
    config.setSearchCapabilities(getSearchCapabilities());
    config.setEmisionSourceUIViewType(SectorRepository.getEmissionCalculationMethods(con));
    config.setSectorPropertiesSet(SectorRepository.getSectorProperties(con));
    // Add layers
    config.setBaseLayers(LayerRepository.getBaseLayers(con, config.getTheme(), messagesKey));
    LayerRepository.getLayers(con, config.getTheme(), messagesKey).forEach(layer -> config.setLayer(layer.getName(), layer));

    // Add color ranges
    config.setColorRanges(ColorRangesRepository.getColorRanges(con));

    return config;
  }

  protected abstract Theme getTheme();

  protected abstract SearchRegion getSearchRegion();

  protected abstract List<SearchCapability> getSearchCapabilities();

  protected abstract CharacteristicsType getCharacteristicsType();

  protected List<String> getCalculationYears(final Connection con) throws SQLException {
    final Integer minYear = ConstantRepository.getNumber(con, ConstantsEnum.MIN_YEAR, Integer.class);
    final Integer maxYear = ConstantRepository.getNumber(con, ConstantsEnum.MAX_YEAR, Integer.class);
    final List<String> years = new ArrayList<>();
    for (int year = minYear; year <= maxYear; year++) {
      years.add(Integer.toString(year));
    }
    return years;
  }

  protected String getCalculationYearDefault() {
    return Integer.toString(Year.now().getValue());
  }

  protected List<SituationType> getAvailableSituationTypes() {
    return AVAILABLE_SITUATION_TYPES;
  }

  protected abstract List<EmissionResultKey> getEmissionResultKeys();

  protected List<Substance> getSubstances() {
    return SUBSTANCES;
  }

  protected List<Substance> getEmissionSubstancesRoad() {
    return EMISSION_SUBSTANCES_ROAD;
  }

  protected List<Substance> getImportSubstances() {
    return SUBSTANCES;
  }

  protected List<CalculationType> getCalculationTypes() {
    return CALCULATION_TYPES;
  }

  protected CalculationType getDefaultCalculationType() {
    return CalculationType.PERMIT;
  }

  protected List<ResultView> getResultViews() {
    return RESULT_VIEWS;
  }

  protected List<SectorGroup> getSectorGroups() {
    return ALLOWED_SECTORGROUPS;
  }

  protected EmissionResultValueDisplaySettings getEmissionResultValueDisplaySettings(final Connection con) throws SQLException {
    final double conversionFactor = ConstantRepository.getDouble(con, ConstantsEnum.EMISSION_RESULT_DISPLAY_CONVERSION_FACTOR);
    final DepositionValueDisplayType conversionUnit = DepositionValueDisplayType
        .valueOf(ConstantRepository.getString(con, ConstantsEnum.EMISSION_RESULT_DISPLAY_UNIT));
    final int depositionValueRoundingLength = ConstantRepository.getInteger(con, ConstantsEnum.EMISSION_RESULT_DISPLAY_ROUNDING_LENGTH);
    final int depositionValuePreciseRoundingLength = ConstantRepository.getInteger(con,
        ConstantsEnum.EMISSION_RESULT_DISPLAY_PRECISE_ROUNDING_LENGTH);

    return new EmissionResultValueDisplaySettings(conversionUnit, conversionFactor, depositionValueRoundingLength,
        depositionValuePreciseRoundingLength);
  }

  protected Map<SituationType, Integer> getMaxNumberOfSituationsPerType() {
    final Map<SituationType, Integer> maxNumberOfSituationsPerType = new HashMap<>();
    maxNumberOfSituationsPerType.put(SituationType.REFERENCE, 1);
    maxNumberOfSituationsPerType.put(SituationType.NETTING, 1);

    return maxNumberOfSituationsPerType;
  }

  protected int getMaxNumberOfFilesSituationImport() {
    return MAX_NUMBER_OF_FILES_SITUATION_IMPORT;
  }

  protected int getMaxNumberOfSituations() {
    return MAX_NUMBER_OF_SITUATIONS;
  }

  protected List<String> getMeteoYears() {
    final ArrayList<String> years = new ArrayList<>();
    years.add(DEFAULT_METEO_YEAR);
    return years;
  }

  protected String getDefaultMeteoYear() {
    return DEFAULT_METEO_YEAR;
  }
}
