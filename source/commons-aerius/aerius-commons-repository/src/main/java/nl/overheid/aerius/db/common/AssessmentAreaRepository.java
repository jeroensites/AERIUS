/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.postgis.PGbox2d;

import nl.overheid.aerius.db.geo.PGisUtils;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SelectClause;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;

/**
 * Repository class to use for assessment area selections.
 */
public final class AssessmentAreaRepository {

  private static final String NATURE2000_AREAS = "natura2000_areas";

  private static final Query NATURA2000_AREAS_LIST = QueryBuilder.from(NATURE2000_AREAS)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.NAME).orderBy(QueryAttribute.NAME).getQuery();

  private static final Query ASSESSMENT_AREA = QueryBuilder.from("assessment_areas").select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.NAME)
      .select(new SelectClause("Box2D(geometry)", QueryAttribute.BOUNDINGBOX.name())).where(QueryAttribute.ASSESSMENT_AREA_ID).getQuery();

  private static final Query NATURA2000_AREAS_BOX_LIST = QueryBuilder.from(NATURE2000_AREAS)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.NAME).select(new SelectClause("Box2D(geometry)", QueryAttribute.BOUNDINGBOX.name()))
      .getQuery();

  // Not allowed to instantiate.
  private AssessmentAreaRepository() {}

  /**
   * Retrieve a list of all n2k areas available in the database.
   * @param con The connection to use.
   * @return The list of all n2k areas (just names and assessment area IDs)
   * @throws SQLException When an error occurs executing the query.
   */
  public static List<AssessmentArea> getNatura2kAreas(final Connection con) throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(NATURA2000_AREAS_LIST.get())) {
      final ResultSet rs = statement.executeQuery();

      final List<AssessmentArea> lst = new ArrayList<>();

      while (rs.next()) {
        final AssessmentArea info = new AssessmentArea();
        info.setId(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs));
        info.setName(QueryAttribute.NAME.getString(rs));
        lst.add(info);
      }

      return lst;
    }
  }

  /**
   * @param con The connection to use.
   * @param assessmentAreaId The ID of the assessment area to retrieve.
   * @return The assessment area corresponding to the ID, or null if not found.
   * @throws SQLException In case of database exceptions.
   */
  public static AssessmentArea getAssessmentArea(final Connection con, final int assessmentAreaId) throws SQLException {
    AssessmentArea area = null;
    try (final PreparedStatement stmt = con.prepareStatement(ASSESSMENT_AREA.get())) {
      ASSESSMENT_AREA.setParameter(stmt, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);

      final ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        area = new AssessmentArea();
        area.setId(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs));
        area.setName(QueryUtil.getString(rs, QueryAttribute.NAME));
        area.setBounds(PGisUtils.getBox((PGbox2d) QueryAttribute.BOUNDINGBOX.getObject(rs)));
      }
    }
    return area;
  }

  /**
   * @param con The connection to use.
   * @return All assessment area.
   * @throws SQLException In case of database exceptions.
   */
  public static Map<Integer, AssessmentArea> getAssessmentAreas(final Connection con) throws SQLException {
    final Map<Integer, AssessmentArea> areas = new HashMap<>();
    try (final PreparedStatement stmt = con.prepareStatement(NATURA2000_AREAS_BOX_LIST.get())) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final AssessmentArea area = new AssessmentArea();
          area.setId(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs));
          area.setName(QueryUtil.getString(rs, QueryAttribute.NAME));
          area.setBounds(PGisUtils.getBox((PGbox2d) QueryAttribute.BOUNDINGBOX.getObject(rs)));
          areas.put(area.getId(), area);
        }
      }
    }
    return areas;
  }

}
