/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import nl.overheid.aerius.db.common.sector.SourceCharacteristicsUtil;
import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.JoinClause.JoinType;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory.CategoryUnit;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;

/**
 *
 */
public final class PlanEmissionCategoryRepository {

  private enum RepositoryAttribute implements Attribute {

    PLAN_CATEGORY_ID, CATEGORY_UNIT, ROAD_TYPE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  /**
   */
  private static final Query PLAN_EMISSION_FACTORS_QUERY = QueryBuilder.from("plan_categories_source_characteristics_view")
      .join(new JoinClause("system.plan_category_properties", RepositoryAttribute.PLAN_CATEGORY_ID, JoinType.LEFT))
      .select(RepositoryAttribute.PLAN_CATEGORY_ID, QueryAttribute.CODE, QueryAttribute.NAME, QueryAttribute.SUBSTANCE_ID,
          RepositoryAttribute.CATEGORY_UNIT, QueryAttribute.EMISSION_FACTOR, QueryAttribute.ICON_TYPE)
      .select(SourceCharacteristicsUtil.OPS_CHARACTERISTICS_ATTRIBUTES).getQuery();

  private PlanEmissionCategoryRepository() {
    // util class
  }

  /**
   * Returns all Plan Emission Categories.
   *
   * @param con Database connection
   * @param messagesKey DBMessagesKey to use for i18n stuff
   * @return ArrayList containing all plan emission categories.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static ArrayList<PlanCategory> findAllPlanEmissionCategories(final Connection con, final DBMessagesKey messagesKey) throws SQLException {
    final AbstractCategoryCollector<PlanCategory> collector = new AbstractCategoryCollector<PlanCategory>(RepositoryAttribute.PLAN_CATEGORY_ID,
        messagesKey) {

      @Override
      void setDescription(final PlanCategory category, final ResultSet rs) {
        DBMessages.setPlanCategoryMessages(category, messagesKey);
      }

      @Override
      PlanCategory getNewCategory() {
        return new PlanCategory();
      }

      @Override
      void setRemainingInfo(final PlanCategory category, final ResultSet rs) throws SQLException {
        category.setCategoryUnit(QueryUtil.getEnum(rs, RepositoryAttribute.CATEGORY_UNIT, CategoryUnit.class));
        final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
        SourceCharacteristicsUtil.setSourceCharacteristicsFromResultSet(characteristics, rs, QueryAttribute.GCN_SECTOR_ID.getInt(rs));
        category.setCharacteristics(characteristics);
      }

      @Override
      public void appendEntity(final PlanCategory entity, final ResultSet rs) throws SQLException {
        final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
        final double emissionFactor = QueryAttribute.EMISSION_FACTOR.getDouble(rs);
        entity.setEmissionFactor(substance, emissionFactor);
      }

    };

    try (final PreparedStatement statement = con.prepareStatement(PLAN_EMISSION_FACTORS_QUERY.get())) {
      final ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        collector.collectEntity(rs);
      }
    }

    return collector.getEntities();
  }
}
