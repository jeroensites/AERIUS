/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.wnb;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.ops.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

/**
 * DB Class used for AeriusPoint specific queries with the database.
 */
public final class ReceptorRepository {

  private static final Logger LOG = LoggerFactory.getLogger(ReceptorRepository.class);

  private static final double DEFAULT_AVERAGE_ROUGHNESS = 0.1;
  private static final LandUse DEFAULT_LANDUSE = LandUse.OTHER_NATURE;

  private static final Attributes RECEPTOR_ATTRIBUTES = new Attributes(
      QueryAttribute.RECEPTOR_ID, QueryAttribute.AVERAGE_ROUGHNESS, QueryAttribute.DOMINANT_LAND_USE, QueryAttribute.LAND_USES);

  private static final Query AERIUS_POINT_TERRAIN_PROPERTIES = QueryBuilder.from("terrain_properties_view")
      .select(RECEPTOR_ATTRIBUTES).where(QueryAttribute.RECEPTOR_ID).getQuery();
  private static final Query AERIUSPOINTS_PERMIT_REQUIRED = QueryBuilder.from("permit_required_receptors_view")
      .join(new JoinClause("receptors_to_assessment_areas", QueryAttribute.RECEPTOR_ID)).getQuery();

  private static final Query AERIUS_POINTS_FOR_ASSESSMENT_AREA = QueryBuilder.from("receptors_to_assessment_areas")
      .select(RECEPTOR_ATTRIBUTES)
      .join(new JoinClause("terrain_properties_view", QueryAttribute.RECEPTOR_ID))
      .where(QueryAttribute.ASSESSMENT_AREA_ID).getQuery();

  private ReceptorRepository() {
    // Don't allow to instantiate.
  }

  /**
   * Set the terrain data on all points that have no or incomplete terrain data.
   * @param pmf database manager
   * @param receptors receptors to check and complete
   * @throws SQLException database error.
   */
  public static <P extends AeriusPoint> void setTerrainData(final PMF pmf, final Iterable<P> receptors) throws SQLException {
    try (Connection con = pmf.getConnection()) {
      final ReceptorGridSettings rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(con);
      final ReceptorUtil ru = new ReceptorUtil(rgs);
      for (final P ap : receptors) {
        if (ap instanceof OPSReceptor && ((OPSReceptor) ap).hasNoTerrainData()) {
          setTerrainData(con, ru, rgs, (OPSReceptor) ap);
        }
      }
    }
  }

  /**
   * Set the terrain properties (average roughness or z0 and dominant land use) for a AeriusPoint.
   * Will use default values if AeriusPoint outside the NL bounding box or
   * if no average roughness or dominant land use could be determined.
   * Does not reset the ID of the AeriusPoint.
   *
   * @param con Database connection
   * @param point Receptor point to set the terrain properties for.
   * @throws SQLException exception if query fails
   */
  static void setTerrainData(final Connection con, final ReceptorUtil ru, final ReceptorGridSettings rgs, final OPSReceptor point)
      throws SQLException {
    point.setAverageRoughness(DEFAULT_AVERAGE_ROUGHNESS);
    point.setLandUse(DEFAULT_LANDUSE);
    if (rgs.getBoundingBox().isPointWithinBoundingBox(point)) {
      //don't expect the point to have set the right Id (calculation points use a different ID for instance).
      final int receptorId = ru.getReceptorIdFromPoint(point);

      try (final PreparedStatement stmt = con.prepareStatement(AERIUS_POINT_TERRAIN_PROPERTIES.get())) {
        AERIUS_POINT_TERRAIN_PROPERTIES.setParameter(stmt, QueryAttribute.RECEPTOR_ID, receptorId);

        try (final ResultSet result = stmt.executeQuery()) {
          if (result.next()) {
            setLandUse(point, result);
          }
        }
      }
    } else {
      LOG.info("Receptor point to get terrain data for is outside grid bounding box, so no terrain data added: {}", point);
    }
  }

  /**
   * Queries the permit required receptor points.
   * Average roughness and dominant land use per receptor id are returned as well.
   *
   * @param con Database connection
   * @param ru receptor util
   * @return list of permit required receptors, including avg roughness and dominant land use
   * @throws SQLException exception if query fails
   */
  public static Map<Integer, List<AeriusPoint>> getPermitRequiredPoints(final Connection con, final ReceptorUtil ru) throws SQLException {
    final Map<Integer, List<AeriusPoint>> pointMap = new HashMap<>();
    try (final PreparedStatement stmt = con.prepareStatement(AERIUSPOINTS_PERMIT_REQUIRED.get())) {
      final ResultSet result = stmt.executeQuery();

      while (result.next()) {
        final int assessId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(result);
        final List<AeriusPoint> points = pointMap.computeIfAbsent(assessId, aid -> new ArrayList<>());
        final OPSReceptor point = new OPSReceptor(QueryAttribute.RECEPTOR_ID.getInt(result));
        setLandUse(point, result);
        point.setCoordinates(ru.getPointFromReceptorId(point.getId()).getCoordinates());
        points.add(point);
      }
      return pointMap;
    }
  }

  private static void setLandUse(final OPSReceptor point, final ResultSet result) throws SQLException {
    point.setAverageRoughness(QueryAttribute.AVERAGE_ROUGHNESS.getDouble(result));
    point.setLandUse(LandUse.safeValueOf(QueryAttribute.DOMINANT_LAND_USE.getString(result)));
    final Array landUsesArray = result.getArray(QueryAttribute.LAND_USES.attribute());

    if (landUsesArray != null) {
      point.setLandUses(ArrayUtils.toPrimitive((Integer[]) landUsesArray.getArray()));
    }
  }

}
