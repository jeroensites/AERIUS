/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.enums;

import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.ProductType;

/**
 * Global constants stored in the database and that can only be used on the server (i.e. everywhere
 * except the client). Constants that also need to be accessed in the client should
 * be placed in {@link nl.overheid.aerius.shared.constants.SharedConstantsEnum}.
 *
 * <p>These can be accessed live from the database via the methods in
 * {@link nl.overheid.aerius.db.common.ConstantRepository}.</p>
 *
 * <p>The values of the entries are stored in the database tables <code>public.constants</code>
 * and <code>system.constants</code>.</p>
 */
public enum ConstantsEnum {

  /**
   * organization managing AERIUS product version.
   */
  CUSTOMER,
  /**
   * Email address used as the from e-mail of e-mails send.
   */
  NOREPLY_EMAIL(ProductType.CALCULATOR),
  /**
   * Default base location where a file can be downloaded from.
   *
   * @deprecated Will be replaced by the file server
   */
  DEFAULT_FILE_MAIL_DOWNLOAD_LINK(ProductType.CALCULATOR),
  /**
   * Internal file service URL used by inter worker processes.
   */
  FILE_SERVICE_URL_INTERNAL(ProductType.CALCULATOR),

  /**
   *
   */
  CURRENT_DATABASE_NAME(ProductType.CALCULATOR),
  CURRENT_DATABASE_VERSION(ProductType.CALCULATOR),
  /**
   * The type of release for the currently running application (valid values: PRODUCTION, CONCEPT, DEPRECATED).
   */
  RELEASE(ProductType.CALCULATOR),
  /**
   * Maximum number of sources allowed in the calculator.
   */
  CALCULATOR_LIMITS_MAX_SOURCES(ProductType.CALCULATOR),
  /**
   * Maximum length of a line allowed in the calculator in meters.
   */
  CALCULATOR_LIMITS_MAX_LINE_LENGTH(ProductType.CALCULATOR),
  /**
   * Maximum surface allowed in the calculator in hectares.
   */
  CALCULATOR_LIMITS_MAX_POLYGON_SURFACE(ProductType.CALCULATOR),
  /**
   * Length of a segment when dividing a line in points in meters.
   */
  CONVERT_LINE_TO_POINTS_SEGMENT_SIZE(ProductType.CALCULATOR),
  /**
   * Size used when converting polygon to grid based points.
   */
  CONVERT_POLYGON_TO_POINTS_GRID_SIZE(ProductType.CALCULATOR),
  /**
   *
   */
  CALCULATOR_LARGE_CALCULATIONS_SPLIT(ProductType.CALCULATOR),
  // --- CHUNKER related constants for the UI ---

  /**
   * An 'OPS Calculation Time Unit' is determined by factoring the number of sources with the number of receptors.
   * The resulting number linearly scales with the time it theoretically takes OPS to finish a calculation.
   *
   * This constant represents the ideal value - expressed in 'OPS Calculation Time Units' - for any one chunk.
   */
  CHUNKER_ENGINE_UNITS_UI_MAX(ProductType.CALCULATOR),

  /**
   * The minimum number of receptors to calculate in a single chunk for the UI. This limit is imposed because
   * the overhead introduced by a low number of receptors becomes substantial.
   */
  CHUNKER_RECEPTORS_UI_MIN(ProductType.CALCULATOR),

  /**
   * The maximum number of receptors to calculate in a single chunk for the UI. This limit is imposed because
   * the responsiveness of the UI is related to the number of receptors it needs to process.
   */
  CHUNKER_RECEPTORS_UI_MAX(ProductType.CALCULATOR),

  // --- CHUNKER related constants for the Worker ---

  /**
   * The maximum 'Calculation Engine Time Unit' to use by the worker.
   */
  CHUNKER_ENGINE_UNITS_WORKER_MAX(ProductType.CALCULATOR),

  /**
   * The minimum number of receptors to calculate in a single chunk for a worker. This limit is imposed because
   * the overhead introduced by a low number of receptors becomes substantial.
   */
  CHUNKER_RECEPTORS_WORKER_MIN(ProductType.CALCULATOR),

  /**
   * The maximum number of receptors to calculate in a single chunk for a worker. Limit is imposed for consistency.
   */
  CHUNKER_RECEPTORS_WORKER_MAX(ProductType.CALCULATOR),

  // ----------

  /**
   * Maximum number of engine sources in a single calculation in the calculator ui.
   */
  MAX_ENGINE_SOURCES_UI(ProductType.CALCULATOR),
  /**
   * Maximum number of task client threads (in the webserver).
   */
  NUMBER_OF_TASKCLIENT_THREADS(ProductType.CALCULATOR),

  /**
   * Melding threshold value (drempelwaarde). If a receptor has more deposition than this, at least a
   * "melding" must be made.
   * (Note: if we do not want to use the database term "pronouncement" in Java (for "melding"), then we
   * need to add an "alias" functionality to this Enum to handle the different constant names.)
   */
  PRONOUNCEMENT_THRESHOLD_VALUE(ProductType.CALCULATOR),

  /**
   * Determines if it is allowed to generate a new API key for Connect.
   */
  CONNECT_GENERATING_APIKEY_ENABLED(ProductType.CALCULATOR),
  /**
   * Specifies the amount of max concurrent jobs for a new user.
   */
  CONNECT_MAX_CONCURRENT_JOBS_FOR_NEW_USER(ProductType.CALCULATOR),
  /**
   * Specifies the default for period job rate limit.
   */
  CONNECT_DEFAULT_PERIOD_JOB_RATE_LIMIT(ProductType.CALCULATOR),

  /**
   * The conversion factor for emission results, to be used in concert with unit display settings.
   */
  EMISSION_RESULT_DISPLAY_CONVERSION_FACTOR(ProductType.CALCULATOR),

  /**
   * The display unit for emission results, to be used in concert with conversion factor.
   */
  EMISSION_RESULT_DISPLAY_UNIT(ProductType.CALCULATOR),

  /**
   * The number of decimal points to display emission results for.
   */
  EMISSION_RESULT_DISPLAY_ROUNDING_LENGTH(ProductType.CALCULATOR),

  /**
   * The number of precise decimal points to display emission results for.
   */
  EMISSION_RESULT_DISPLAY_PRECISE_ROUNDING_LENGTH(ProductType.CALCULATOR),

  /**
   * The minimum calculation year
   */
  MIN_YEAR,

  /**
   * The maximum calculation year
   */
  MAX_YEAR,

  /**
   * Surface of zoom level 1.
   */
  SURFACE_ZOOM_LEVEL_1(ProductType.CALCULATOR),

  /**
   * Number of zoom levels.
   */
  MAX_ZOOM_LEVEL(ProductType.CALCULATOR),

  /**
   * Default locale.
   */
  DEFAULT_LOCALE(ProductType.CALCULATOR),

  /**
   * PASSKEY a UUID secret string that is used to update system info messages in the databse
   * Used to update system info messages tables.
   */
  SYSTEM_INFO_PASSKEY,

  /**
   * Hostname for the graphite metrics collection instance.
   *
   * If this property has no value, metrics will not be reported (although they will be collected).
   */
  METRICS_GRAPHITE_HOST,

  /**
   * Port number for the grahpite metrics collection instance.
   *
   * Optional property: The default value for this property is 2004 in MetricFactory.
   */
  METRICS_GRAPHITE_PORT,

  /**
   * Report interval for metric reporting.
   *
   * Optional property: The default value for this property is 5 in MetricFactory.
   */
  METRICS_GRAHPITE_INTERVAL,

  /**
   * Report prefix for metric reporting.
   *
   * Optional property: The default value for this property is empty.
   */
  METRICS_GRAPHITE_PREFIX;

  private final ArrayList<ProductType> productTypes;

  ConstantsEnum(final ProductType... products) {
    productTypes = new ArrayList<>();
    for (final ProductType product : products) {
      productTypes.add(product);
    }
  }

  public ArrayList<ProductType> getProductTypes() {
    return productTypes;
  }
}
