/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.emissions.shipping.MaritimeShippingRouteEmissionPoint;

/**
 *
 */
public class MaritimeShippingRoutePoint extends SridPoint implements MaritimeShippingRouteEmissionPoint {

  private static final long serialVersionUID = -1590741358793134358L;

  private final double maneuverFactor;
  private final double measure;

  /**
   * @param x The x-coordinate representing this point.
   * @param y The y-coordinate representing this point.
   * @param measure the length of the line related to this point
   * @param maneuverFactor The maneuver factor for this point (1 if no maneuver area, like a lock or 'sluis').
   */
  public MaritimeShippingRoutePoint(final int x, final int y, final double measure, final double maneuverFactor) {
    super(x, y);
    this.measure = measure;
    this.maneuverFactor = maneuverFactor;
  }

  public double getMeasure() {
    return measure;
  }

  @Override
  public double getManeuverFactor() {
    return maneuverFactor;
  }

  @Override
  public String toString() {
    return "MaritimeShippingRoutePoint [" + super.toString() + ", maneuverFactor=" + maneuverFactor + ", measure=" + measure + "]";
  }

  @Override
  public double getSegmentLength() {
    return measure;
  }
}
