/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.db.common.configuration;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.db.common.BasePMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.shared.config.ApplicationConfiguration;
import nl.overheid.aerius.shared.constants.DrivingSide;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.AeriusCustomer;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.EnvUtils;

/**
 * Repository class to get the application configuration.
 */
public class AppConfigurationRepository {
  private static final Logger LOGGER = LoggerFactory.getLogger(AppConfigurationRepository.class);

  /**
   * Returns the application configuration and a per theme specific configuration.
   * The themes queried depends on the database configuration.
   *
   * @param con the database connection
   * @param dbMessagesKey the db message key
   * @param customer implementing organization
   * @return A filled context object.
   * @throws SQLException    When a database error occurs.
   * @throws AeriusException When another error occurs.
   */
  public static ApplicationConfiguration getAppConfiguration(final Connection con, final DBMessagesKey dbMessagesKey, final AeriusCustomer customer)
      throws SQLException, AeriusException {
    final ApplicationConfiguration appConfig = new ApplicationConfiguration();
    fillContextWithBaseData(con, appConfig, dbMessagesKey);
    fillThemeConfigurations(con, dbMessagesKey, customer, appConfig);
    fillApplicationSettings(con, appConfig);
    return appConfig;
  }

  private static void fillThemeConfigurations(final Connection con, final DBMessagesKey messagesKey, final AeriusCustomer customer,
      final ApplicationConfiguration appConfig) throws SQLException, AeriusException {
    final List<AbstractAppThemeConfigRepository> themeConfigs;
    switch (customer) {
    case RIVM:
      themeConfigs = List.of(new WnbAppThemeConfigRepository());
      break;
    case JNCC:
      themeConfigs = List.of(new NcaAppThemeConfigRepository());
      break;
    default:
      LOGGER.warn("Unknown customer {} passed to fill theme configurations.", customer);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
    for (final AbstractAppThemeConfigRepository atr : themeConfigs) {
      appConfig.getContexts().put(atr.getTheme(), atr.fill(con, messagesKey));
    }
  }

  private static void fillApplicationSettings(final Connection con, final ApplicationConfiguration appConfig) throws SQLException {
    addSetting(con, appConfig, SharedConstantsEnum.MANUAL_URL);
    addSetting(con, appConfig, SharedConstantsEnum.QUICK_START_URL);
    addSetting(con, appConfig, SharedConstantsEnum.BIJ12_HELPDESK_URL);
    addSetting(con, appConfig, SharedConstantsEnum.OPS_FACTSHEET_URL);
    addSetting(con, appConfig, SharedConstantsEnum.RELEASE_DETAILS_URL);
    addSetting(con, appConfig, SharedConstantsEnum.CALCULATOR_BOUNDARY);

    addEnumSetting(con, appConfig, SharedConstantsEnum.DRIVING_SIDE, DrivingSide.class);
    addIntSetting(con, appConfig, SharedConstantsEnum.SYSTEM_INFO_POLLING_TIME);

    addVersions(con, appConfig);
    addUrls(con, appConfig);
    addEasterEgg(con, appConfig);
  }

  private static void addUrls(final Connection con, final ApplicationConfiguration applicationConfiguration) throws SQLException {
    addSettingPreferEnvironment(con, applicationConfiguration, SharedConstantsEnum.CONNECT_INTERNAL_URL);
    addSettingPreferEnvironment(con, applicationConfiguration, SharedConstantsEnum.GEOSERVER_INTERNAL_URL);
    addSettingPreferEnvironment(con, applicationConfiguration, SharedConstantsEnum.SEARCH_ENDPOINT_URL);
  }

  private static void addVersions(final Connection con, final ApplicationConfiguration applicationConfiguration) throws SQLException {
    applicationConfiguration.getSettings().setSetting(SharedConstantsEnum.DATABASE_VERSION, BasePMF.getDatabaseVersion(con));
    applicationConfiguration.getSettings().setSetting(SharedConstantsEnum.AERIUS_VERSION, AeriusVersion.getVersionNumber());
  }

  private static void addEasterEgg(final Connection con, final ApplicationConfiguration appConfig) throws SQLException {
    addOptionalSetting(con, appConfig, SharedConstantsEnum.TETRIS_ENDPOINT_URL);
    addOptionalSetting(con, appConfig, SharedConstantsEnum.TETRIS_ORIGIN_POINT);
  }

  private static void fillContextWithBaseData(final Connection con, final ApplicationConfiguration c, final DBMessagesKey messagesKey)
      throws SQLException {
    c.setReceptorGridSettings(ReceptorGridSettingsRepository.getReceptorGridSettings(con));
    c.setSectorCategories(SectorRepository.getSectorCategories(con, messagesKey));
  }

  private static void addSetting(final Connection con, final ApplicationConfiguration appc, final SharedConstantsEnum constant) throws SQLException {
    appc.getSettings().setSetting(constant, ConstantRepository.getString(con, constant));
  }

  private static void addOptionalSetting(final Connection con, final ApplicationConfiguration appc, final Enum constant) throws SQLException {
    appc.getSettings().setSetting(constant, ConstantRepository.getString(con, constant, false));
  }

  private static <E extends Enum<E>> void addEnumSetting(final Connection con, final ApplicationConfiguration appc,
      final SharedConstantsEnum constant, final Class<E> enumClass) throws SQLException {
    appc.getSettings().setSetting(constant, ConstantRepository.getEnum(con, constant, enumClass));
  }

  private static void addIntSetting(final Connection con, final ApplicationConfiguration appc, final Enum constant) throws SQLException {
    appc.getSettings().setSetting(constant, ConstantRepository.getNumber(con, constant, Integer.class));
  }

  private static void addDoubleSetting(final Connection con, final ApplicationConfiguration appc, final Enum constant) throws SQLException {
    appc.getSettings().setSetting(constant, ConstantRepository.getNumber(con, constant, Double.class));
  }

  private static void addBooleanSetting(final Connection con, final ApplicationConfiguration appc, final Enum constant) throws SQLException {
    appc.getSettings().setSetting(constant, ConstantRepository.getBoolean(con, constant));
  }

  private static void addSettingPreferEnvironment(final Connection con, final ApplicationConfiguration appc, final SharedConstantsEnum constant)
      throws SQLException {
    final String envProperty = EnvUtils.getEnvProperty(constant.name());
    appc.getSettings().setSetting(constant, envProperty != null ? envProperty : ConstantRepository.getString(con, constant));
  }

}
