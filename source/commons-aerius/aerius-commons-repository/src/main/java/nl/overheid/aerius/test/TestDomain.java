/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.EPSG;
import nl.overheid.aerius.geo.shared.EPSGProxy;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.FarmLodgingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.InlandShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MaritimeMaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MooringInlandShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.MooringMaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.farm.AdditionalLodgingSystem;
import nl.overheid.aerius.shared.domain.v2.source.farm.CustomFarmLodging;
import nl.overheid.aerius.shared.domain.v2.source.farm.ReductiveLodgingSystem;
import nl.overheid.aerius.shared.domain.v2.source.farm.StandardFarmLodging;
import nl.overheid.aerius.shared.domain.v2.source.offroad.CustomOffRoadMobileSource;
import nl.overheid.aerius.shared.domain.v2.source.offroad.StandardOffRoadMobileSource;
import nl.overheid.aerius.shared.domain.v2.source.plan.Plan;
import nl.overheid.aerius.shared.domain.v2.source.road.CustomVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.RoadType;
import nl.overheid.aerius.shared.domain.v2.source.road.SpecificVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.StandardVehicles;
import nl.overheid.aerius.shared.domain.v2.source.road.ValuesPerVehicleType;
import nl.overheid.aerius.shared.domain.v2.source.road.VehicleType;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.InlandWaterway;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.StandardInlandShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.StandardMooringInlandShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.inland.WaterwayDirection;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.StandardMaritimeShipping;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.StandardMooringMaritimeShipping;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Convenience class to avoid having to write the same test code over and over again.
 *
 */
public class TestDomain {

  public static final String DV_CONTINUOUS = TestSectorCategories.DV_CONTINUOUS.getCode();
  public static final String DV_INDUSTRIAL_ACTIVITY = TestSectorCategories.DV_INDUSTRIAL_ACTIVITY.getCode();
  public static final String DV_SPACE_HEATING = TestSectorCategories.DV_SPACE_HEATING.getCode();
  public static final String DV_SPACE_TRAFFIC = TestSectorCategories.DV_SPACE_TRAFFIC.getCode();
  public static final String DV_SPACE_ANIMAL_HOUSING = TestSectorCategories.DV_SPACE_ANIMAL_HOUSING.getCode();
  public static final String DV_SPACE_FERTILISER = TestSectorCategories.DV_SPACE_FERTILISER.getCode();

  public static final int XCOORD_1 = 136558;
  public static final int YCOORD_1 = 455251;
  public static final int XCOORD_2 = 208413;
  public static final int YCOORD_2 = 474162;
  public static final int XCOORD_3 = XCOORD_1;
  public static final int YCOORD_3 = YCOORD_2;

  public static final int YEAR = 2020;

  public static final int BINNENVELD_ID = 65;
  public static final int DWINGELDERVELD_ID = 30;
  public static final int VELUWE_ID = 57;
  public static final int DUINEN_TEXEL_ID = 2;
  public static final int DUINEN_VLIELAND_ID = 3;
  public static final int SOLLEVELD_ID = 99;
  public static final int WOOLDSE_VEEN_ID = 64;
  public static final int OLDENZAAL_ID = 50;
  public static final int SCHOORLSE_DUINEN_ID = 86;
  public static final int NOORDHOLLANDS_DUINRESERVAAT_ID = 87;

  public static final EmissionValueKey EVK_WITH_YEAR_NH3 = new EmissionValueKey(YEAR, Substance.NH3);
  public static final EmissionValueKey EVK_WITH_YEAR_NOX = new EmissionValueKey(YEAR, Substance.NOX);

  private static final EmissionValueKey KEY_NOX = new EmissionValueKey(Substance.NOX);
  private static final EmissionValueKey KEY_NH3 = new EmissionValueKey(Substance.NH3);

  public static final EmissionResultKey DEFAULT_ERK_NH3 = EmissionResultKey.NH3_DEPOSITION;
  public static final EmissionResultKey DEFAULT_ERK_NOX = EmissionResultKey.NOX_DEPOSITION;

  public static final String USERROLE_REGISTER_SUPERUSER = "register_superuser";
  public static final String USERROLE_REGISTER_EDITOR = "register_editor";
  public static final String USERROLE_REGISTER_VIEWER = "register_viewer";

  private static final String INLAND_WATERWAY_CODE = "CEMT_VIb";

  private static final String LINESTRING = "LINESTRING(";
  private static final int FARM_SECTOR = 4120;

  private final SectorCategories categories;

  public TestDomain() {
    this.categories = TestSectorCategories.construct();
  }

  public TestDomain(final PMF pmf) throws SQLException {
    this.categories = SectorRepository.getSectorCategories(pmf, LocaleUtils.getDefaultLocale());
  }

  public Sector getSectorById(final int id) {
    return categories.getSectorById(id);
  }

  public FarmLodgingEmissionSource getFarmEmissionSource() {
    final FarmLodgingEmissionSource source = new FarmLodgingEmissionSource();
    source.setSectorId(FARM_SECTOR);
    final FarmLodgingCategory animalCategory1 = categories.getFarmLodgingCategories().getFarmLodgingSystemCategories().get(0);
    final StandardFarmLodging lodging1 = new StandardFarmLodging();
    lodging1.setFarmLodgingCode(animalCategory1.getCode());
    lodging1.setNumberOfAnimals(3);

    final FarmLodgingCategory animalCategory2 = categories.getFarmLodgingCategories().getFarmLodgingSystemCategories().get(1);
    final StandardFarmLodging lodging2 = new StandardFarmLodging();
    lodging2.setFarmLodgingCode(animalCategory2.getCode());
    lodging2.setNumberOfAnimals(1000);
    lodging2.setSystemDefinitionCode(animalCategory2.getFarmLodgingSystemDefinitions().get(0).getCode());

    final AdditionalLodgingSystem additional1 = new AdditionalLodgingSystem();
    additional1.setNumberOfAnimals(500);
    additional1.setLodgingSystemCode(categories.getFarmLodgingCategories().getFarmAdditionalLodgingSystemCategories().get(0).getCode());
    lodging2.getAdditionalLodgingSystems().add(additional1);
    final AdditionalLodgingSystem additional2 = new AdditionalLodgingSystem();
    additional2.setNumberOfAnimals(250);
    additional2.setLodgingSystemCode(categories.getFarmLodgingCategories().getFarmAdditionalLodgingSystemCategories().get(1).getCode());
    lodging2.getAdditionalLodgingSystems().add(additional2);

    final ReductiveLodgingSystem reductive1 = new ReductiveLodgingSystem();
    reductive1.setLodgingSystemCode(categories.getFarmLodgingCategories().getFarmReductiveLodgingSystemCategories().get(0).getCode());
    lodging2.getReductiveLodgingSystems().add(reductive1);

    final ReductiveLodgingSystem reductive2 = new ReductiveLodgingSystem();
    reductive2.setLodgingSystemCode(categories.getFarmLodgingCategories().getFarmReductiveLodgingSystemCategories().get(1).getCode());
    lodging2.getReductiveLodgingSystems().add(reductive2);

    final CustomFarmLodging lodging3 = new CustomFarmLodging();
    lodging3.setNumberOfAnimals(908);
    lodging3.setDescription("Schaap");
    lodging3.getEmissionFactors().put(Substance.NH3, 2000.0);

    source.getSubSources().add(lodging1);
    source.getSubSources().add(lodging2);
    source.getSubSources().add(lodging3);

    source.getEmissions().put(Substance.NH3, 10101.9);
    return source;
  }

  public SRM2RoadEmissionSource getSRM2EmissionSource() {
    final SRM2RoadEmissionSource emissionSource = new SRM2RoadEmissionSource();
    emissionSource.setSectorId(RoadType.FREEWAY.getSectorId());

    final StandardVehicles lt = new StandardVehicles();
    final ValuesPerVehicleType valuesForLightTraffic = new ValuesPerVehicleType();
    valuesForLightTraffic.setVehiclesPerTimeUnit(980);
    valuesForLightTraffic.setStagnationFraction(0.2);
    lt.getValuesPerVehicleTypes().put(VehicleType.LIGHT_TRAFFIC, valuesForLightTraffic);
    lt.setTimeUnit(TimeUnit.DAY);
    lt.setMaximumSpeed(100);
    lt.setStrictEnforcement(false);

    final StandardVehicles hf = new StandardVehicles();
    final ValuesPerVehicleType valuesForHeavyFreight = new ValuesPerVehicleType();
    valuesForHeavyFreight.setVehiclesPerTimeUnit(200);
    lt.getValuesPerVehicleTypes().put(VehicleType.HEAVY_FREIGHT, valuesForHeavyFreight);
    hf.setTimeUnit(TimeUnit.DAY);
    hf.setMaximumSpeed(80);
    hf.setStrictEnforcement(false);

    final OnRoadMobileSourceCategory mobileSourceCategory1 = categories.getOnRoadMobileSourceCategories().get(0);
    final OnRoadMobileSourceCategory mobileSourceCategory2 = categories.getOnRoadMobileSourceCategories().get(1);
    final SpecificVehicles specific1 = new SpecificVehicles();
    specific1.setVehicleCode(mobileSourceCategory1.getCode());
    specific1.setVehiclesPerTimeUnit(30000);
    specific1.setTimeUnit(TimeUnit.DAY);
    final SpecificVehicles specific2 = new SpecificVehicles();
    specific2.setVehicleCode(mobileSourceCategory2.getCode());
    specific2.setVehiclesPerTimeUnit(15000);
    specific2.setTimeUnit(TimeUnit.DAY);

    final CustomVehicles custom = new CustomVehicles();
    custom.setDescription("Custom bike");
    custom.setVehiclesPerTimeUnit(3244);
    custom.setTimeUnit(TimeUnit.DAY);
    custom.getEmissionFactors().put(Substance.NOX, 0.04);

    emissionSource.getSubSources().add(lt);
    emissionSource.getSubSources().add(hf);

    //temporary disable euroklasse AER-2037 CALCULATION_VEHICLE_SPECIFIC_EMISSIONS_DISALLOWED
    //emissionSource.getEmissionSubSources().add(specific1);
    //emissionSource.getEmissionSubSources().add(specific2);

    emissionSource.getSubSources().add(custom);

    emissionSource.getEmissions().put(Substance.NOX, 634.9);

    return emissionSource;
  }

  public OffRoadMobileEmissionSource getOffRoadMobileEmissionSource(final OffRoadMobileEmissionSource emissionSource) {
    final OffRoadMobileSourceCategory mobileSourceCategory1 = categories.getOffRoadMobileSourceCategories().get(0);
    final StandardOffRoadMobileSource vehicleEmissionValues = new StandardOffRoadMobileSource();
    vehicleEmissionValues.setOffRoadMobileSourceCode(mobileSourceCategory1.getCode());
    vehicleEmissionValues.setLiterFuelPerYear(30000);
    vehicleEmissionValues.setOperatingHoursPerYear(100);
    vehicleEmissionValues.setDescription("My Little Offroader");
    emissionSource.getSubSources().add(vehicleEmissionValues);
    final CustomOffRoadMobileSource vehicleEmissionValuesCustom = new CustomOffRoadMobileSource();
    final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
    characteristics.setHeatContent(20.0);
    characteristics.setSpread(5.0);
    characteristics.setEmissionHeight(10);
    vehicleEmissionValuesCustom.setCharacteristics(characteristics);
    vehicleEmissionValuesCustom.getEmissions().put(Substance.NOX, 101010.0);
    vehicleEmissionValuesCustom.setDescription("My Big Custom Bike");
    emissionSource.getSubSources().add(vehicleEmissionValuesCustom);
    return emissionSource;
  }

  public MooringMaritimeShippingEmissionSource getMooringMaritimeShipEmissionSource(final MooringMaritimeShippingEmissionSource emissionSource) {
    final StandardMooringMaritimeShipping vesselGroupEmissionValues = new StandardMooringMaritimeShipping();
    vesselGroupEmissionValues.setShipCode(categories.getMaritimeShippingCategories().get(0).getCode());
    vesselGroupEmissionValues.setDescription("Motorboot");
    vesselGroupEmissionValues.setAverageResidenceTime(100);
    vesselGroupEmissionValues.setShipsPerTimeUnit(30000);
    vesselGroupEmissionValues.setTimeUnit(TimeUnit.YEAR);

    emissionSource.getSubSources().add(vesselGroupEmissionValues);

    final StandardMooringMaritimeShipping vesselGroupEmissionValues1 = new StandardMooringMaritimeShipping();
    vesselGroupEmissionValues1.setShipCode(categories.getMaritimeShippingCategories().get(1).getCode());
    vesselGroupEmissionValues1.setDescription("Puntertje");
    vesselGroupEmissionValues1.setAverageResidenceTime(200);
    vesselGroupEmissionValues1.setShipsPerTimeUnit(15000);
    vesselGroupEmissionValues1.setTimeUnit(TimeUnit.YEAR);
    emissionSource.getSubSources().add(vesselGroupEmissionValues1);
    return emissionSource;
  }

  public MaritimeMaritimeShippingEmissionSource getShipEmissionRouteSource() {
    final MaritimeMaritimeShippingEmissionSource emissionValues = new MaritimeMaritimeShippingEmissionSource();
    final StandardMaritimeShipping vesselGroupEmissionValues = new StandardMaritimeShipping();
    vesselGroupEmissionValues.setShipCode(categories.getMaritimeShippingCategories().get(0).getCode());
    vesselGroupEmissionValues.setDescription("Motorboot");
    vesselGroupEmissionValues.setMovementsPerTimeUnit(30000);
    vesselGroupEmissionValues.setTimeUnit(TimeUnit.YEAR);
    emissionValues.getSubSources().add(vesselGroupEmissionValues);
    final StandardMaritimeShipping vesselGroupEmissionValues1 = new StandardMaritimeShipping();
    vesselGroupEmissionValues1.setShipCode(categories.getMaritimeShippingCategories().get(1).getCode());
    vesselGroupEmissionValues1.setDescription("Puntertje");
    vesselGroupEmissionValues1.setMovementsPerTimeUnit(15000);
    vesselGroupEmissionValues1.setTimeUnit(TimeUnit.YEAR);
    emissionValues.getSubSources().add(vesselGroupEmissionValues1);
    return emissionValues;
  }

  public InlandShippingEmissionSource getInlandRouteEmissionValues() {
    final InlandShippingEmissionSource emissionValues = new InlandShippingEmissionSource();
    final InlandWaterway inlandWaterway = new InlandWaterway();
    inlandWaterway.setWaterwayCode(INLAND_WATERWAY_CODE);
    inlandWaterway.setDirection(WaterwayDirection.IRRELEVANT);
    emissionValues.setWaterway(inlandWaterway);
    final StandardInlandShipping vesselGroupEmissionValues = new StandardInlandShipping();
    vesselGroupEmissionValues.setShipCode(categories.getInlandShippingCategories().getShipCategories().get(0).getCode());
    vesselGroupEmissionValues.setDescription("Duikboot");
    vesselGroupEmissionValues.setMovementsAtoBPerTimeUnit(50);
    vesselGroupEmissionValues.setTimeUnitAtoB(TimeUnit.YEAR);
    vesselGroupEmissionValues.setMovementsBtoAPerTimeUnit(150);
    vesselGroupEmissionValues.setTimeUnitBtoA(TimeUnit.YEAR);
    vesselGroupEmissionValues.setPercentageLadenAtoB(20);
    vesselGroupEmissionValues.setPercentageLadenBtoA(40);
    emissionValues.getSubSources().add(vesselGroupEmissionValues);
    final StandardInlandShipping vesselGroupEmissionValues1 = new StandardInlandShipping();
    vesselGroupEmissionValues1.setShipCode(categories.getInlandShippingCategories().getShipCategories().get(1).getCode());
    vesselGroupEmissionValues1.setDescription("Veerpont");
    vesselGroupEmissionValues1.setMovementsAtoBPerTimeUnit(300);
    vesselGroupEmissionValues1.setTimeUnitAtoB(TimeUnit.YEAR);
    vesselGroupEmissionValues1.setMovementsBtoAPerTimeUnit(380);
    vesselGroupEmissionValues1.setTimeUnitBtoA(TimeUnit.YEAR);
    vesselGroupEmissionValues1.setPercentageLadenAtoB(80);
    vesselGroupEmissionValues1.setPercentageLadenBtoA(82);
    emissionValues.getSubSources().add(vesselGroupEmissionValues1);
    return emissionValues;
  }

  public MooringInlandShippingEmissionSource getInlandMooringEmissionSource() {
    final InlandWaterway mooringRouteArrivalWaterwayType = new InlandWaterway();
    mooringRouteArrivalWaterwayType.setWaterwayCode(INLAND_WATERWAY_CODE);
    mooringRouteArrivalWaterwayType.setDirection(WaterwayDirection.IRRELEVANT);

    final MooringInlandShippingEmissionSource emissionSource = new MooringInlandShippingEmissionSource();
    final StandardMooringInlandShipping vesselGroupEmissionValues = new StandardMooringInlandShipping();
    vesselGroupEmissionValues.setShipCode(categories.getInlandShippingCategories().getShipCategories().get(0).getCode());
    vesselGroupEmissionValues.setDescription("Vliegende Hollander");
    vesselGroupEmissionValues.setAverageResidenceTime(5);
    vesselGroupEmissionValues.setPercentageLaden(60);
    vesselGroupEmissionValues.setShipsPerTimeUnit(3500);
    vesselGroupEmissionValues.setTimeUnit(TimeUnit.YEAR);
    emissionSource.getSubSources().add(vesselGroupEmissionValues);

    final StandardMooringInlandShipping vesselGroupEmissionValues1 = new StandardMooringInlandShipping();
    vesselGroupEmissionValues1.setShipCode(categories.getInlandShippingCategories().getShipCategories().get(1).getCode());
    vesselGroupEmissionValues1.setDescription("Neeltje Jacoba");
    vesselGroupEmissionValues1.setAverageResidenceTime(3);
    vesselGroupEmissionValues1.setPercentageLaden(72);
    vesselGroupEmissionValues1.setShipsPerTimeUnit(330);
    vesselGroupEmissionValues1.setTimeUnit(TimeUnit.YEAR);
    emissionSource.getSubSources().add(vesselGroupEmissionValues1);
    return emissionSource;
  }

  public PlanEmissionSource getPlanEmissionSource() {
    final PlanCategory planCategory1 = categories.getPlanEmissionCategories().get(0);
    final PlanCategory planCategory2 = categories.getPlanEmissionCategories().get(1);
    final PlanEmissionSource source = new PlanEmissionSource();
    final Plan planEmission = new Plan();
    planEmission.setPlanCode(planCategory1.getCode());
    planEmission.setAmount(30000);
    planEmission.setDescription("Some appartments");
    source.getSubSources().add(planEmission);
    final Plan planEmission1 = new Plan();
    planEmission1.setPlanCode(planCategory2.getCode());
    planEmission1.setAmount(15000);
    planEmission1.setDescription("Some more appartments");
    source.getSubSources().add(planEmission1);
    return source;
  }

  public static GenericEmissionSource getGenericEmissionSource() {
    return getGenericEmissionSource(new GenericEmissionSource());
  }

  public static GenericEmissionSource getGenericEmissionSource(final GenericEmissionSource source) {
    source.getEmissions().put(Substance.NH3, 657.0);
    source.setCharacteristics(getDefaultCharacteristics());
    return source;
  }

  public static <E extends EmissionSource> EmissionSourceFeature getSource(final String id, final Geometry geometry, final String label,
      final E source) {
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    feature.setId(id);
    feature.setGeometry(geometry);
    source.setLabel(label);
    if (source.getSectorId() == 0) {
      source.setSectorId(Sector.SECTOR_DEFAULT.getSectorId());
    }
    //default characteristics for this sector.
    source.setCharacteristics(getDefaultCharacteristics());
    feature.setProperties(source);
    return feature;
  }

  public static OPSSourceCharacteristics getDefaultCharacteristics() {
    final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
    characteristics.setHeatContent(0.28);
    characteristics.setEmissionHeight(22.0);
    characteristics.setDiameter(0);
    characteristics.setSpread(11.0);
    characteristics.setDiurnalVariation(DiurnalVariation.INDUSTRIAL_ACTIVITY);
    characteristics.setParticleSizeDistribution(1800);
    return characteristics;
  }

  public static OPSSourceCharacteristics getNonDefaultCharacteristics() {
    final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
    characteristics.setHeatContent(564.584);
    characteristics.setEmissionHeight(11.12);
    characteristics.setDiameter(5);
    characteristics.setSpread(649.10);
    characteristics.setDiurnalVariation(DiurnalVariation.INDUSTRIAL_ACTIVITY);
    characteristics.setParticleSizeDistribution(2);
    return characteristics;
  }

  public static List<EmissionSourceFeature> getExampleSourceList() {
    final List<EmissionSourceFeature> emissionSourceList = new ArrayList<>();
    final Point point1 = new Point(XCOORD_1, YCOORD_1);
    emissionSourceList.add(getSource("1", point1, "ExampleSource1", getGenericEmissionSource()));
    final Point point2 = new Point(XCOORD_1, YCOORD_1);
    final GenericEmissionSource source2 = new GenericEmissionSource();
    source2.getEmissions().put(Substance.NH3, 267.0);
    source2.getEmissions().put(Substance.NOX, 901.0);
    emissionSourceList.add(getSource("2", point2, "ExampleSource2", source2));
    return emissionSourceList;
  }

  public static AeriusResultPoint getExamplePointBinnenveld() { //point in Binnenveld with 2 habitat types.
    return new AeriusResultPoint(4286670, AeriusPointType.RECEPTOR, 167843, 447435);
  }

  public static AeriusResultPoint getExamplePointDuinen() {
    //point with 2 assessment areas (Noordhollands Duinreservaat (87) and Schoorlse Duinen(86)) with at least a habitat type each.
    return new AeriusResultPoint(6388705, AeriusPointType.RECEPTOR, 104481, 521277);
  }

  public static AeriusResultPoint getExamplePointDwingelderveld() {
    //point in Dwingelderveld with at least one species.
    return new AeriusResultPoint(6903101, AeriusPointType.RECEPTOR, 225832, 539330);
  }

  public static ReceptorGridSettings getExampleGridSettings() {
    final EPSG epsg = EPSGProxy.getEPSG(28992);
    final BBox bbox = new BBox(3604.0, 287959.0, 296800.0, 629300.0);
    final ArrayList<HexagonZoomLevel> zoomLevels = new ArrayList<HexagonZoomLevel>();
    for (int i = 1; i <= 5; i++) {
      zoomLevels.add(new HexagonZoomLevel(i, 10000));
    }
    final int hexHor = 1529;
    return new ReceptorGridSettings(bbox, epsg, hexHor, zoomLevels);
  }

  /**
   * @return
   */
  public SectorCategories getCategories() {
    return categories;
  }
}
