/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

class TempEffectResultsSummaryImpl extends TempResultsSummaryImpl<TempEffectResultsSummaryInput> {

  private static final Query QUERY_OVERALL_DEPOSITION_STATISTICS = QueryBuilder
      .from("ae_scenario_temporary_effect_statistics(?::integer[], ?, ?)", QueryAttribute.TEMPORARY_CALCULATION_IDS,
          QueryAttribute.REFERENCE_CALCULATION_ID, QueryAttribute.NETTING_CALCULATION_ID)
      .where(WHERE_HEXAGON_TYPE)
      .getQuery();

  private static final Query QUERY_CALCULATION_POINTS = QueryBuilder
      .from("ae_scenario_calculation_point_temporary_results(?::integer[], ?, ?)", QueryAttribute.TEMPORARY_CALCULATION_IDS,
          QueryAttribute.REFERENCE_CALCULATION_ID, QueryAttribute.NETTING_CALCULATION_ID)
      .select(QueryAttribute.CALCULATION_POINT_ID, RepositoryAttribute.DEPOSITION)
      .getQuery();

  private static final Query INSERT_AREA_DEPOSITION_STATISTICS = QueryBuilder
      .from("ae_scenario_temporary_effect_fill_calculation_statistics(?, ?::integer[], ?, ?, ?)", QueryAttribute.JOB_ID,
          QueryAttribute.TEMPORARY_CALCULATION_IDS, QueryAttribute.REFERENCE_CALCULATION_ID, QueryAttribute.NETTING_CALCULATION_ID,
          QueryAttribute.ASSESSMENT_AREA_ID)
      .getQuery();

  public TempEffectResultsSummaryImpl(final PMF pmf, final ReceptorUtil receptorUtil) {
    super(pmf, receptorUtil);
  }

  @Override
  protected ScenarioResultType getScenarioResultType() {
    return ScenarioResultType.MAX_TEMPORARY_EFFECT;
  }

  @Override
  protected TempEffectResultsSummaryInput createSummaryInput(final SituationCalculations situationCalculations, final Integer calculationId) {
    return TempEffectResultsSummaryInput.fromSituationCalculations(situationCalculations);
  }

  @Override
  protected List<TempEffectResultsSummaryInput> createSummaryInputs(final SituationCalculations situationCalculations) {
    return Collections.singletonList(createSummaryInput(situationCalculations, null));
  }

  @Override
  protected PreparedStatement prepareOverallStatisticsStatement(final Connection connection, final int jobId,
      final TempEffectResultsSummaryInput summaryInput, final SummaryHexagonType hexagonType) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_OVERALL_DEPOSITION_STATISTICS.get());
    QUERY_OVERALL_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.TEMPORARY_CALCULATION_IDS,
        QueryUtil.toNumericSQLArray(connection, summaryInput.getTemporaryCalculationsIds()));
    QUERY_OVERALL_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.REFERENCE_CALCULATION_ID, summaryInput.getReferenceCalculationId());
    QUERY_OVERALL_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.NETTING_CALCULATION_ID, summaryInput.getNettingCalculationId());
    QUERY_OVERALL_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase(Locale.ROOT));
    return stmt;
  }

  @Override
  protected PreparedStatement prepareCustomCalculationPointResultsStatement(final Connection connection,
      final TempEffectResultsSummaryInput summaryInput) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_CALCULATION_POINTS.get());
    QUERY_CALCULATION_POINTS.setParameter(stmt, QueryAttribute.TEMPORARY_CALCULATION_IDS,
        QueryUtil.toNumericSQLArray(connection, summaryInput.getTemporaryCalculationsIds()));
    QUERY_CALCULATION_POINTS.setParameter(stmt, QueryAttribute.REFERENCE_CALCULATION_ID, summaryInput.getReferenceCalculationId());
    QUERY_CALCULATION_POINTS.setParameter(stmt, QueryAttribute.NETTING_CALCULATION_ID, summaryInput.getNettingCalculationId());
    return stmt;
  }

  @Override
  protected PreparedStatement prepareInsertAreaResultsSummaryStatement(final Connection connection, final int jobId,
      final TempEffectResultsSummaryInput summaryInput, final int assessmentAreaId) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(INSERT_AREA_DEPOSITION_STATISTICS.get());
    INSERT_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.JOB_ID, jobId);
    INSERT_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.TEMPORARY_CALCULATION_IDS,
        QueryUtil.toNumericSQLArray(connection, summaryInput.getTemporaryCalculationsIds()));
    INSERT_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.REFERENCE_CALCULATION_ID, summaryInput.getReferenceCalculationId());
    INSERT_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.NETTING_CALCULATION_ID, summaryInput.getNettingCalculationId());
    INSERT_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);
    return stmt;
  }

  @Override
  protected int compare(final SituationResultsStatistics statistics1, final SituationResultsStatistics statistics2) {
    int sorted = compareValues(
        statistics1.get(ResultStatisticType.MAX_TEMP_INCREASE),
        statistics2.get(ResultStatisticType.MAX_TEMP_INCREASE));
    if (sorted == 0) {
      sorted = compareValues(
          statistics1.get(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE),
          statistics2.get(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE));
    }
    return sorted;
  }
}
