/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import nl.overheid.aerius.db.EntityCollector;
import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType.FuelTypeProperties;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType.MachineryFuelType;

/**
 *
 */
public final class OffRoadMachineryTypesRepository {

  private enum RepositoryAttribute implements Attribute {

    MACHINERY_TYPE_ID,
    MACHINERY_CODE,

    MACHINERY_FUEL_TYPE_ID,
    FUEL_CODE,
    FUEL_NAME,
    FUEL_DENSITY,

    POWER,
    LOAD,
    ENERGY_EFFICIENCY,

    SORT_ORDER;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final Query OFF_ROAD_MACHINERY_TYPES_QUERY = QueryBuilder.from("machinery_types_view")
      .select(RepositoryAttribute.MACHINERY_TYPE_ID, QueryAttribute.SECTOR_ID, RepositoryAttribute.MACHINERY_CODE,
          RepositoryAttribute.MACHINERY_FUEL_TYPE_ID, RepositoryAttribute.FUEL_CODE,
          RepositoryAttribute.FUEL_DENSITY, RepositoryAttribute.POWER, RepositoryAttribute.LOAD, RepositoryAttribute.ENERGY_EFFICIENCY,
          QueryAttribute.SUBSTANCE_ID, QueryAttribute.EMISSION_FACTOR)
      .orderBy(RepositoryAttribute.SORT_ORDER).getQuery();
  private static final Query OFF_ROAD_MACHINERY_FUEL_TYPES_QUERY = QueryBuilder.from("machinery_fuel_types_view")
      .select(QueryAttribute.SECTOR_ID, RepositoryAttribute.MACHINERY_FUEL_TYPE_ID, RepositoryAttribute.FUEL_CODE, RepositoryAttribute.FUEL_DENSITY)
      .orderBy(QueryAttribute.SECTOR_ID, RepositoryAttribute.FUEL_NAME).getQuery();

  private static final Map<String, String> OTHER_MACHINERY_NAMES = new HashMap<String, String>() {{
    put("nl", "Anders");
    put("en", "Other");
   }};

  private OffRoadMachineryTypesRepository() {
  }

  /**
   * Returns all Off Road Machinery Types from the database.
   *
   * @param con Connection to use
   * @param messagesKey DBMessagesKey to get descriptions from
   * @param locale The locale to use for descriptions
   * @return ArrayList with all OffRoadMachineryTypes
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static ArrayList<OffRoadMachineryType> findAllOffRoadMachineryTypes(final Connection con, final DBMessagesKey messagesKey)
      throws SQLException {
    final ArrayList<OffRoadMachineryType> machineryTypes = new ArrayList<OffRoadMachineryType>();
    //assure the 'other' types are first in the list.
    machineryTypes.addAll(getOtherTypes(con, messagesKey));
    machineryTypes.addAll(getNormalTypes(con, messagesKey));
    return machineryTypes;
  }

  private static ArrayList<OffRoadMachineryType> getNormalTypes(final Connection con, final DBMessagesKey messagesKey) throws SQLException {
    final EntityCollector<OffRoadMachineryType> collector = new EntityCollector<OffRoadMachineryType>() {

      @Override
      public int getKey(final ResultSet rs) throws SQLException {
        return QueryUtil.getInt(rs, RepositoryAttribute.MACHINERY_TYPE_ID);
      }

      @Override
      public OffRoadMachineryType fillEntity(final ResultSet rs) throws SQLException {
        final OffRoadMachineryType machineryType = new OffRoadMachineryType();
        //ID needed for the description.
        machineryType.setId(getKey(rs));
        machineryType.setSectorId(QueryAttribute.SECTOR_ID.getInt(rs));
        machineryType.setCode(QueryUtil.getString(rs, RepositoryAttribute.MACHINERY_CODE));
        DBMessages.setOffRoadMachineryTypeMessages(machineryType, messagesKey);
        return machineryType;
      }
    };
    try (final PreparedStatement statement = con.prepareStatement(OFF_ROAD_MACHINERY_TYPES_QUERY.get())) {
      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        final OffRoadMachineryType machineryType = collector.collectEntity(rs);

        final int fuelTypeId = QueryUtil.getInt(rs, RepositoryAttribute.MACHINERY_FUEL_TYPE_ID);
        final Optional<FuelTypeProperties> existingType = machineryType.getFuelTypes().stream()
            .filter(fuelTypeProperties -> fuelTypeProperties.getFuelType().getId() == fuelTypeId)
            .findFirst();
        final FuelTypeProperties fuelTypeProperties;

        if (existingType.isPresent()) {
          fuelTypeProperties = existingType.get();
        } else {
          fuelTypeProperties = new FuelTypeProperties();
          fuelTypeProperties.setFuelType(fillBaseFuelProperties(rs, messagesKey));
          fuelTypeProperties.setPower(QueryUtil.getInt(rs, RepositoryAttribute.POWER));
          fuelTypeProperties.setLoad(QueryUtil.getDouble(rs, RepositoryAttribute.LOAD));
          fuelTypeProperties.setEnergyEfficiencyInGramPerKWH(QueryUtil.getInt(rs, RepositoryAttribute.ENERGY_EFFICIENCY));
          machineryType.getFuelTypes().add(fuelTypeProperties);
        }

        final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
        final double emissionFactor = QueryAttribute.EMISSION_FACTOR.getDouble(rs);
        fuelTypeProperties.setEmissionFactor(substance, emissionFactor);
      }
    }
    return collector.getEntities();
  }

  private static ArrayList<OffRoadMachineryType> getOtherTypes(final Connection con, final DBMessagesKey messagesKey) throws SQLException {
    // select "other" entries, which aren't real machinery types. Use negative sector IDs as fake ID's to avoid conflicts.
    //not the best approach I guess, but beats what it was (something tied to the 'normal' types)
    //The ID's are not really used anywhere but here, only in UI perhaps but not to lookup the types later on or something.
     final String language = messagesKey.getLocale().getLanguage();

    final EntityCollector<OffRoadMachineryType> collector = new EntityCollector<OffRoadMachineryType>() {

      @Override
      public int getKey(final ResultSet rs) throws SQLException {
        return -QueryAttribute.SECTOR_ID.getInt(rs);
      }

      @Override
      public OffRoadMachineryType fillEntity(final ResultSet rs) throws SQLException {
        final OffRoadMachineryType machineryType = new OffRoadMachineryType();
        machineryType.setSectorId(QueryAttribute.SECTOR_ID.getInt(rs));
        machineryType.setName(OTHER_MACHINERY_NAMES.get(language));
        machineryType.setCode(OffRoadMachineryType.OTHER_CODE);
        return machineryType;
      }
    };
    try (final PreparedStatement statement = con.prepareStatement(OFF_ROAD_MACHINERY_FUEL_TYPES_QUERY.get())) {
      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        final OffRoadMachineryType machineryType = collector.collectEntity(rs);
        final FuelTypeProperties fuelType = new FuelTypeProperties();
        fuelType.setFuelType(fillBaseFuelProperties(rs, messagesKey));
        machineryType.getFuelTypes().add(fuelType);
      }
    }
    return collector.getEntities();
  }

  private static MachineryFuelType fillBaseFuelProperties(final ResultSet rs, final DBMessagesKey messagesKey) throws SQLException {
    final MachineryFuelType fuelType = new MachineryFuelType();
    fuelType.setId(QueryUtil.getInt(rs, RepositoryAttribute.MACHINERY_FUEL_TYPE_ID));
    fuelType.setCode(QueryUtil.getString(rs, RepositoryAttribute.FUEL_CODE));
    fuelType.setDensityFuel(QueryUtil.getDouble(rs, RepositoryAttribute.FUEL_DENSITY));
    DBMessages.setOffRoadMachineryFuelTypeMessages(fuelType, messagesKey);
    return fuelType;
  }

}
