/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.meteo.Meteo;

public class MeteoRepository {

  private enum RepositoryAttribute implements Attribute {
    START_YEAR, END_YEAR;

    @Override
    public String attribute() {
      return name().toLowerCase(Locale.getDefault());
    }

  }

  private static final Query GET_METEO_DATA = QueryBuilder.from("system.meteo")
      .select(QueryAttribute.CODE, QueryAttribute.DESCRIPTION, RepositoryAttribute.START_YEAR, RepositoryAttribute.END_YEAR)
      .getQuery();

  public static Map<String, Meteo> getMeteoDefinitions(final Connection con) throws SQLException {
    final HashMap<String, Meteo> results = new HashMap<>();

    try (final PreparedStatement stmt = con.prepareStatement(GET_METEO_DATA.get())) {

      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {

        final String code = QueryUtil.getString(rs, QueryAttribute.CODE);
        final String description = QueryUtil.getString(rs, QueryAttribute.DESCRIPTION);
        final int startYear = QueryUtil.getInt(rs, RepositoryAttribute.START_YEAR);
        final int endYear = QueryUtil.getInt(rs, RepositoryAttribute.END_YEAR);
        results.put(code, new Meteo(code, description, startYear, endYear));
      }
    }
    return results;
  }
}
