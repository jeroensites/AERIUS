/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import java.sql.SQLException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;

/**
 * Repository bean for sector related database operations.
 */
public class SectorRepositoryBean {

  private static final Logger LOG = LoggerFactory.getLogger(SectorRepositoryBean.class);

  private final PMF pmf;


  public SectorRepositoryBean(final PMF pmf) {
    this.pmf = pmf;
  }

  /**
   * Retrieve the sector categories for a locale.
   */
  public SectorCategories getSectorCategories(final Locale locale) throws AeriusException {
    try {
      return SectorRepository.getSectorCategories(pmf, locale);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting sector categories", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

}
