/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

class SituationResultsSummaryImpl extends ResultsSummaryImpl<Integer> {

  private static final Query QUERY_OVERALL_DEPOSITION_STATISTICS = QueryBuilder
      .from("ae_calculation_statistics(?)", QueryAttribute.CALCULATION_ID)
      .where(WHERE_HEXAGON_TYPE)
      .getQuery();

  private static final Query QUERY_AREA_DEPOSITION_STATISTICS = QueryBuilder
      .from("scenario_calculation_assessment_area_statistics")
      .select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.HEXAGON_TYPE, RepositoryAttribute.RESULT_STATISTIC_TYPE, QueryAttribute.VALUE)
      .where(QueryAttribute.CALCULATION_ID)
      .where(WHERE_HEXAGON_TYPE)
      .where(WHERE_RESULT_TYPE)
      .getQuery();

  private static final Query QUERY_AREA_CHART_STATISTICS = QueryBuilder
      .from("scenario_calculation_assessment_area_chart_statistics")
      .select(RepositoryAttribute.LOWER_BOUND, QueryAttribute.CARTOGRAPHIC_SURFACE)
      .where(QueryAttribute.CALCULATION_ID, QueryAttribute.ASSESSMENT_AREA_ID)
      .where(WHERE_HEXAGON_TYPE)
      .where(WHERE_RESULT_TYPE)
      .getQuery();

  private static final Query QUERY_AREA_DEPOSITION_STATISTIC_MARKERS = QueryBuilder
      .from("scenario_calculation_assessment_area_statistic_markers")
      .select(RepositoryAttribute.RESULT_STATISTIC_TYPE, QueryAttribute.RECEPTOR_ID)
      .where(QueryAttribute.CALCULATION_ID)
      .where(WHERE_HEXAGON_TYPE)
      .where(WHERE_RESULT_TYPE)
      .getQuery();

  private static final Query QUERY_HABITAT_DEPOSITION_STATISTICS = QueryBuilder
      .from("scenario_calculation_critical_deposition_area_statistics")
      .select(QueryAttribute.CRITICAL_DEPOSITION_AREA_ID, RepositoryAttribute.HEXAGON_TYPE, RepositoryAttribute.RESULT_STATISTIC_TYPE,
          QueryAttribute.VALUE)
      .where(QueryAttribute.CALCULATION_ID, QueryAttribute.ASSESSMENT_AREA_ID)
      .where(WHERE_HEXAGON_TYPE)
      .where(WHERE_RESULT_TYPE)
      .getQuery();

  private static final Query QUERY_RECEPTOR_STATISTICS = QueryBuilder
      .from("ae_scenario_calculation_receptor_statistics(?::scenario_result_type, ?)", QueryAttribute.RESULT_TYPE, QueryAttribute.CALCULATION_ID)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.RESULT_STATISTIC_TYPE, QueryAttribute.RECEPTOR_ID)
      .where(WHERE_HEXAGON_TYPE)
      .getQuery();

  private static final Query QUERY_CALCULATION_POINTS = QueryBuilder
      .from("ae_calculation_point_summed_deposition_results(?)", QueryAttribute.CALCULATION_ID)
      .select(QueryAttribute.CALCULATION_POINT_ID, RepositoryAttribute.DEPOSITION)
      .where(new StaticWhereClause("deposition > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal"))
      .getQuery();

  private static final Query INSERT_AREA_DEPOSITION_STATISTICS = QueryBuilder
      .from("ae_fill_calculation_statistics(?, ?, ?)", QueryAttribute.JOB_ID, QueryAttribute.CALCULATION_ID, QueryAttribute.ASSESSMENT_AREA_ID)
      .getQuery();

  public SituationResultsSummaryImpl(final PMF pmf, final ReceptorUtil receptorUtil) {
    super(pmf, receptorUtil);
  }

  @Override
  protected Integer createSummaryInput(final SituationCalculations situationCalculations, final Integer calculationId) {
    return calculationId;
  }

  @Override
  protected List<Integer> createSummaryInputs(final SituationCalculations situationCalculations) {
    return situationCalculations.getCalculationIds().stream()
        .map(calculationId -> createSummaryInput(situationCalculations, calculationId))
        .collect(Collectors.toList());
  }

  @Override
  protected PreparedStatement prepareOverallStatisticsStatement(final Connection connection, final int jobId, final Integer summaryInput,
      final SummaryHexagonType hexagonType) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_OVERALL_DEPOSITION_STATISTICS.get());
    QUERY_OVERALL_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput);
    QUERY_OVERALL_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    return stmt;
  }

  @Override
  protected PreparedStatement prepareAreaStatisticsStatement(final Connection connection, final int jobId, final Integer summaryInput,
      final SummaryHexagonType hexagonType) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_AREA_DEPOSITION_STATISTICS.get());
    QUERY_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput);
    QUERY_AREA_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.RESULT_TYPE, ScenarioResultType.SITUATION_RESULT.name().toLowerCase());
    QUERY_AREA_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    return stmt;
  }

  @Override
  protected PreparedStatement prepareHabitatStatisticsStatement(final Connection connection, final int jobId, final Integer summaryInput,
      final SummaryHexagonType hexagonType, final int assessmentAreaId) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_HABITAT_DEPOSITION_STATISTICS.get());
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput);
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.RESULT_TYPE, ScenarioResultType.SITUATION_RESULT.name().toLowerCase());
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);
    return stmt;
  }

  @Override
  protected PreparedStatement prepareChartStatisticsStatement(final Connection connection, final int jobId, final Integer summaryInput,
      final SummaryHexagonType hexagonType, final int assessmentAreaId) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_AREA_CHART_STATISTICS.get());
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput);
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, RepositoryAttribute.RESULT_TYPE, ScenarioResultType.SITUATION_RESULT.name().toLowerCase());
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);
    return stmt;
  }

  @Override
  protected PreparedStatement prepareStatisticMarkersStatement(final Connection connection, final int jobId, final Integer summaryInput,
      final SummaryHexagonType hexagonType) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.get());
    QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput);
    QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.setParameter(stmt, RepositoryAttribute.RESULT_TYPE,
        ScenarioResultType.SITUATION_RESULT.name().toLowerCase());
    QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    return stmt;
  }

  @Override
  protected PreparedStatement prepareReceptorStatisticsStatement(final Connection connection, final int jobId, final Integer summaryInput,
      final SummaryHexagonType hexagonType) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_RECEPTOR_STATISTICS.get());
    QUERY_RECEPTOR_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput);
    QUERY_RECEPTOR_STATISTICS.setParameter(stmt, QueryAttribute.RESULT_TYPE, ScenarioResultType.SITUATION_RESULT.name().toLowerCase());
    QUERY_RECEPTOR_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    return stmt;
  }

  @Override
  protected PreparedStatement prepareCustomCalculationPointResultsStatement(final Connection connection, final Integer summaryInput)
      throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_CALCULATION_POINTS.get());
    QUERY_CALCULATION_POINTS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput);
    return stmt;
  }

  @Override
  protected PreparedStatement prepareInsertAreaResultsSummaryStatement(final Connection connection, final int jobId, final Integer summaryInput,
      final int assessmentAreaId) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(INSERT_AREA_DEPOSITION_STATISTICS.get());
    INSERT_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.JOB_ID, jobId);
    INSERT_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.CALCULATION_ID, summaryInput);
    INSERT_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);
    return stmt;
  }

  @Override
  protected int compare(final SituationResultsStatistics statistics1, final SituationResultsStatistics statistics2) {
    int sorted = compareValues(
        statistics1.get(ResultStatisticType.MAX_CONTRIBUTION),
        statistics2.get(ResultStatisticType.MAX_CONTRIBUTION));
    if (sorted == 0) {
      sorted = compareValues(
          statistics1.get(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE),
          statistics2.get(ResultStatisticType.SUM_CARTOGRAPHIC_SURFACE));
    }
    return sorted;
  }
}
