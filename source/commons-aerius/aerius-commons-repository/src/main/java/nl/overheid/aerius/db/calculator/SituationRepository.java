/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.InsertBuilder;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;

/**
scenario_situations
- scenario_situation_id
- scenario_id
- code (uid)
- type (current, proposed)
- name
 */
public class SituationRepository {

  private enum RepositoryAttribute implements Attribute {
    SCENARIO_SITUATION_ID,
    SCENARIO_ID;

    @Override
    public String attribute() {
      return name().toLowerCase(Locale.ENGLISH);
    }
  }

  private static final Logger LOGGER = LoggerFactory.getLogger(SituationRepository.class);

  private static final String SITUATIONS = "situations";

  private static final Query INSERT_SITUATION =
      InsertBuilder.into(SITUATIONS)
      .insert(RepositoryAttribute.SCENARIO_SITUATION_ID, RepositoryAttribute.SCENARIO_ID, QueryAttribute.CODE,
          QueryAttribute.TYPE, QueryAttribute.NAME).getQuery();


}
