/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.postgis.PGbox2d;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.geo.PGisUtils;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.EPSG;
import nl.overheid.aerius.geo.shared.EPSGProxy;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;

/**
 * Repository class to query system wide database constants that are hard-coded in the schema of AERIUS databases.
 */
public final class ReceptorGridSettingsRepository {

  private static final String SRID_QUERY = "SELECT * FROM ae_get_srid()";

  private static final String CALCULATOR_GRID_BOUNDARY_BOX_QUERY = "SELECT * FROM ae_get_calculator_grid_boundary_box()";

  private static final String DETERMINE_NUMBER_OF_HEXAGON_ROWS = "SELECT * FROM ae_determine_number_of_hexagon_rows(?)";

  // Not allowed to instantiate.
  private ReceptorGridSettingsRepository() {
  }

  public static ReceptorGridSettings getReceptorGridSettings(final PMF pmf) throws SQLException {
    try (Connection con = pmf.getConnection()) {
      return getReceptorGridSettings(con);
    }
  }

  public static ReceptorGridSettings getReceptorGridSettings(final Connection con) throws SQLException {
    final EPSG epsg = EPSGProxy.getEPSG(ReceptorGridSettingsRepository.getSrid(con));
    final BBox bbox = ReceptorGridSettingsRepository.getCalculatorGridBBox(con);
    final ArrayList<HexagonZoomLevel> zoomLevels = ReceptorGridSettingsRepository.getZoomLevels(con);
    final int hexHor = ReceptorGridSettingsRepository.getNumberOfHexagonRows(con, 1);
    return new ReceptorGridSettings(bbox, epsg, hexHor, zoomLevels);
  }

  /**
   * Returns the SRID set in the database.
   * @param pmf database pmf
   * @return srid in the database
   * @throws SQLException
   */
  public static int getSrid(final PMF pmf) throws SQLException {
    try (Connection con = pmf.getConnection()) {
      return getSrid(con);
    }
  }

  /**
   * Returns the SRID set in the database.
   * @param con database connection
   * @return srid in the database
   * @throws SQLException
   */
  public static int getSrid(final Connection con) throws SQLException {
    try (final PreparedStatement ps = con.prepareStatement(SRID_QUERY);
        final ResultSet rs = ps.executeQuery()) {
      if (rs.next()) {
        return rs.getInt(1);
      }
    }
    throw new IllegalArgumentException("The method ae_get_srid() doesn't return a value, but it must");
  }

  /**
   * Returns the bounding box of the calculation receptor grid as set in the database.
   * @param con database connection
   * @return the calculation receptor grid bounding box
   * @throws SQLException
   */
  public static BBox getCalculatorGridBBox(final Connection con) throws SQLException {
    try (final PreparedStatement ps = con.prepareStatement(CALCULATOR_GRID_BOUNDARY_BOX_QUERY);
        final ResultSet rs = ps.executeQuery()) {
      if (rs.next()) {
        return PGisUtils.getBox((PGbox2d) rs.getObject(1));
      }
    }
    throw new IllegalArgumentException("The method ae_get_calculator_grid_boundary_box() doesn't return a value, but it must");
  }

  /**
   * Returns the number of hexagons at a given row for the given zoom level.
   * @param con database connection
   * @param zoomLevel the zoomLevel to get the number of hexagons for
   * @return the numbe of hexagons on a row for the given zoom level
   * @throws SQLException
   */
  public static int getNumberOfHexagonRows(final Connection con, final int zoomLevel) throws SQLException {
    try (final PreparedStatement ps = con.prepareStatement(DETERMINE_NUMBER_OF_HEXAGON_ROWS)) {
      ps.setInt(1, zoomLevel);
      try (final ResultSet rs = ps.executeQuery()) {
        if (rs.next()) {
          return rs.getInt(1);
        }
      }
    }
    throw new IllegalArgumentException("The method ae_determine_number_of_hexagon_rows(zoomLevel) doesn't return a value, but it must");
  }

  public static ArrayList<HexagonZoomLevel> getZoomLevels(final Connection con) throws SQLException {
    final int numberOfZoomLevels = ConstantRepository.getInteger(con, ConstantsEnum.MAX_ZOOM_LEVEL);
    final int surfaceZoomLevel1 = ConstantRepository.getInteger(con, ConstantsEnum.SURFACE_ZOOM_LEVEL_1);
    final ArrayList<HexagonZoomLevel> zoomLevels = new ArrayList<HexagonZoomLevel>();
    for (int i = 1; i <= numberOfZoomLevels; i++) {
      zoomLevels.add(new HexagonZoomLevel(i, surfaceZoomLevel1));
    }
    return zoomLevels;
  }
}
