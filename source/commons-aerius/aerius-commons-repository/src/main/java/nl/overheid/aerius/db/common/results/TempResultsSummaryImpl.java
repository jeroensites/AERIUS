/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;


abstract class TempResultsSummaryImpl<I> extends ResultsSummaryImpl<I> {

  private static final Query QUERY_AREA_DEPOSITION_STATISTICS = QueryBuilder
      .from("scenario_assessment_area_statistics")
      .select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.HEXAGON_TYPE, RepositoryAttribute.RESULT_STATISTIC_TYPE, QueryAttribute.VALUE)
      .where(QueryAttribute.JOB_ID)
      .where(WHERE_HEXAGON_TYPE)
      .where(WHERE_RESULT_TYPE)
      .getQuery();

  private static final Query QUERY_AREA_CHART_STATISTICS = QueryBuilder
      .from("scenario_assessment_area_chart_statistics")
      .select(RepositoryAttribute.LOWER_BOUND, QueryAttribute.CARTOGRAPHIC_SURFACE)
      .where(QueryAttribute.JOB_ID, QueryAttribute.ASSESSMENT_AREA_ID)
      .where(WHERE_HEXAGON_TYPE)
      .where(WHERE_RESULT_TYPE)
      .getQuery();

  private static final Query QUERY_AREA_DEPOSITION_STATISTIC_MARKERS = QueryBuilder
      .from("scenario_assessment_area_statistic_markers")
      .select(RepositoryAttribute.RESULT_STATISTIC_TYPE, QueryAttribute.RECEPTOR_ID)
      .where(QueryAttribute.JOB_ID)
      .where(WHERE_HEXAGON_TYPE)
      .where(WHERE_RESULT_TYPE)
      .getQuery();

  private static final Query QUERY_HABITAT_DEPOSITION_STATISTICS = QueryBuilder
      .from("scenario_critical_deposition_area_statistics")
      .select(QueryAttribute.CRITICAL_DEPOSITION_AREA_ID, RepositoryAttribute.HEXAGON_TYPE, RepositoryAttribute.RESULT_STATISTIC_TYPE,
          QueryAttribute.VALUE)
      .where(QueryAttribute.JOB_ID, QueryAttribute.ASSESSMENT_AREA_ID)
      .where(WHERE_HEXAGON_TYPE)
      .where(WHERE_RESULT_TYPE)
      .getQuery();

  private static final Query QUERY_RECEPTOR_STATISTICS = QueryBuilder
      .from("ae_scenario_receptor_statistics(?::scenario_result_type, ?)", QueryAttribute.RESULT_TYPE, QueryAttribute.JOB_ID)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.RESULT_STATISTIC_TYPE, QueryAttribute.RECEPTOR_ID)
      .where(WHERE_HEXAGON_TYPE)
      .getQuery();

  public TempResultsSummaryImpl(final PMF pmf, final ReceptorUtil receptorUtil) {
    super(pmf, receptorUtil);
  }

  protected abstract ScenarioResultType getScenarioResultType();

  @Override
  protected PreparedStatement prepareAreaStatisticsStatement(final Connection connection, final int jobId, final I summaryInput, final SummaryHexagonType hexagonType) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_AREA_DEPOSITION_STATISTICS.get());
    QUERY_AREA_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.JOB_ID, jobId);
    QUERY_AREA_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.RESULT_TYPE, getScenarioResultType().name().toLowerCase());
    QUERY_AREA_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    return stmt;
  }

  @Override
  protected PreparedStatement prepareHabitatStatisticsStatement(final Connection connection, final int jobId, final I summaryInput, final SummaryHexagonType hexagonType, final int assessmentAreaId) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_HABITAT_DEPOSITION_STATISTICS.get());
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.JOB_ID, jobId);
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.RESULT_TYPE, getScenarioResultType().name().toLowerCase());
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    QUERY_HABITAT_DEPOSITION_STATISTICS.setParameter(stmt, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);
    return stmt;
  }

  @Override
  protected PreparedStatement prepareChartStatisticsStatement(final Connection connection, final int jobId, final I summaryInput, final SummaryHexagonType hexagonType, final int assessmentAreaId) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_AREA_CHART_STATISTICS.get());
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, QueryAttribute.JOB_ID, jobId);
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, RepositoryAttribute.RESULT_TYPE, getScenarioResultType().name().toLowerCase());
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    QUERY_AREA_CHART_STATISTICS.setParameter(stmt, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);
    return stmt;
  }

  @Override
  protected PreparedStatement prepareStatisticMarkersStatement(final Connection connection, final int jobId, final I summaryInput, final SummaryHexagonType hexagonType) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.get());
    QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.setParameter(stmt, QueryAttribute.JOB_ID, jobId);
    QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.setParameter(stmt, RepositoryAttribute.RESULT_TYPE, getScenarioResultType().name().toLowerCase());
    QUERY_AREA_DEPOSITION_STATISTIC_MARKERS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    return stmt;
  }

  @Override
  protected PreparedStatement prepareReceptorStatisticsStatement(final Connection connection, final int jobId, final I summaryInput, final SummaryHexagonType hexagonType) throws SQLException {
    final PreparedStatement stmt = connection.prepareStatement(QUERY_RECEPTOR_STATISTICS.get());
    QUERY_RECEPTOR_STATISTICS.setParameter(stmt, QueryAttribute.JOB_ID, jobId);
    QUERY_RECEPTOR_STATISTICS.setParameter(stmt, QueryAttribute.RESULT_TYPE, getScenarioResultType().name().toLowerCase());
    QUERY_RECEPTOR_STATISTICS.setParameter(stmt, RepositoryAttribute.HEXAGON_TYPE, hexagonType.name().toLowerCase());
    return stmt;
  }

}
