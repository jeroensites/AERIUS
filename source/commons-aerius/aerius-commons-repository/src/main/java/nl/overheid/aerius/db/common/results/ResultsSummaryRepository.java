/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.util.DoubleSummaryStatistics;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntFunction;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.summary.CustomCalculationPointResult;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ScenarioResultType;
import nl.overheid.aerius.shared.domain.summary.SituationResultsReceptors;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

public class ResultsSummaryRepository {

  private final Map<ScenarioResultType, ResultsSummaryImpl<?>> availableSummaryTypes = new HashMap<>();

  /**
   * Only use this constructor if you're certain the repository won't be used to retrieve information.
   */
  public ResultsSummaryRepository(final PMF pmf) {
    this(pmf, null);
  }

  public ResultsSummaryRepository(final PMF pmf, final ReceptorUtil receptorUtil) {
    availableSummaryTypes.put(ScenarioResultType.SITUATION_RESULT, new SituationResultsSummaryImpl(pmf, receptorUtil));
    availableSummaryTypes.put(ScenarioResultType.PROJECT_CALCULATION, new ProjectCalculationResultsSummaryImpl(pmf, receptorUtil));
    availableSummaryTypes.put(ScenarioResultType.MAX_TEMPORARY_CONTRIBUTION, new TempContributionResultsSummaryImpl(pmf, receptorUtil));
    availableSummaryTypes.put(ScenarioResultType.MAX_TEMPORARY_EFFECT, new TempEffectResultsSummaryImpl(pmf, receptorUtil));
  }

  public SituationResultsSummary determineReceptorResultsSummary(final SituationCalculations situationCalculations,
      final ScenarioResultType resultType, final int jobId, final Integer calculationId, final SummaryHexagonType hexagonType,
      final IntFunction<AssessmentArea> assessmentAreaRetriever, final IntFunction<HabitatType> habitatTypeRetriever)
      throws AeriusException {

    final ResultsSummaryImpl<?> resultsSummaryImpl = availableSummaryTypes.get(resultType);

    if (resultsSummaryImpl == null) {
      return null;
    } else {
      return resultsSummaryImpl.determineReceptorResultsSummary(situationCalculations, jobId, calculationId, hexagonType, assessmentAreaRetriever,
          habitatTypeRetriever);
    }
  }

  public SituationResultsSummary determineCustomCalculationPointResultsSummary(final SituationCalculations situationCalculations,
      final ScenarioResultType resultType, final Integer calculationId) throws AeriusException {
    final List<CustomCalculationPointResult> customPointResults = determineCustomCalculationPointResults(
        situationCalculations, resultType, calculationId);
    final SituationResultsStatistics statistics = determineStatistics(customPointResults, resultType);

    return new SituationResultsSummary(statistics, List.of(), List.of(), new SituationResultsReceptors(), customPointResults);
  }

  private List<CustomCalculationPointResult> determineCustomCalculationPointResults(final SituationCalculations situationCalculations,
      final ScenarioResultType resultType, final Integer calculationId) throws AeriusException {

    final ResultsSummaryImpl<?> resultsSummaryImpl = availableSummaryTypes.get(resultType);

    if (resultsSummaryImpl == null) {
      return List.of();
    } else {
      return resultsSummaryImpl.determineCustomCalculationPointResults(situationCalculations, calculationId);
    }
  }

  private static SituationResultsStatistics determineStatistics(final List<CustomCalculationPointResult> customPointResults,
      final ScenarioResultType resultType) {
    final SituationResultsStatistics statistics = new SituationResultsStatistics();
    statistics.put(ResultStatisticType.COUNT_CALCULATION_POINTS, Double.valueOf(customPointResults.size()));

    switch (resultType) {
    case SITUATION_RESULT:
      addSituationStatistics(statistics, customPointResults);
      break;
    case PROJECT_CALCULATION:
      addProjectStatistics(statistics, customPointResults);
      break;
    case MAX_TEMPORARY_CONTRIBUTION:
      addTemporaryContributionStatistics(statistics, customPointResults);
      break;
    case MAX_TEMPORARY_EFFECT:
      addTemporaryEffectStatistics(statistics, customPointResults);
      break;
    }

    return statistics;
  }

  private static void addSituationStatistics(final SituationResultsStatistics statistics,
      final List<CustomCalculationPointResult> customPointResults) {
    statistics.put(ResultStatisticType.MAX_CONTRIBUTION, customPointResults.stream()
        .mapToDouble(CustomCalculationPointResult::getResult)
        .max()
        .orElse(0));
  }

  private static void addProjectStatistics(final SituationResultsStatistics statistics,
      final List<CustomCalculationPointResult> customPointResults) {
    final DoubleSummaryStatistics increasingResults = customPointResults.stream()
        .mapToDouble(CustomCalculationPointResult::getResult)
        .filter(x -> x > 0)
        .summaryStatistics();
    statistics.put(ResultStatisticType.COUNT_CALCULATION_POINTS_INCREASE, Double.valueOf(increasingResults.getCount()));
    statistics.put(ResultStatisticType.MAX_INCREASE, increasingResults.getCount() > 0 ? increasingResults.getMax() : 0);
    final DoubleSummaryStatistics decreasingResults = customPointResults.stream()
        .mapToDouble(x -> -x.getResult())
        .filter(x -> x > 0)
        .summaryStatistics();
    statistics.put(ResultStatisticType.COUNT_CALCULATION_POINTS_DECREASE, Double.valueOf(decreasingResults.getCount()));
    statistics.put(ResultStatisticType.MAX_DECREASE, decreasingResults.getCount() > 0 ? decreasingResults.getMax() : 0);
  }

  private static void addTemporaryContributionStatistics(final SituationResultsStatistics statistics,
      final List<CustomCalculationPointResult> customPointResults) {
    statistics.put(ResultStatisticType.MAX_TEMP_CONTRIBUTION, customPointResults.stream()
        .mapToDouble(CustomCalculationPointResult::getResult)
        .max()
        .orElse(0));
  }

  private static void addTemporaryEffectStatistics(final SituationResultsStatistics statistics,
      final List<CustomCalculationPointResult> customPointResults) {
    statistics.put(ResultStatisticType.MAX_TEMP_INCREASE, customPointResults.stream()
        .mapToDouble(CustomCalculationPointResult::getResult)
        .max()
        .orElse(0));
  }

  public void insertResultsSummaryForArea(final int jobId, final SituationCalculations situationCalculations, final int areaId)
      throws AeriusException {
    for (final ResultsSummaryImpl<?> resultsSummaryImpl : availableSummaryTypes.values()) {
      resultsSummaryImpl.insertResultsSummaryForArea(jobId, situationCalculations, areaId);
    }
  }

}
