/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.calculator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.BatchInserter;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SelectClause;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationResultSetType;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations;
import nl.overheid.aerius.shared.domain.calculation.ScenarioCalculations.CalculationPointSetTracker;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.point.ReceptorPoint;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;
import nl.overheid.aerius.util.EnumUtil;

/**
 * DB Class to insert/modify and get Calculation specific objects from the database.
 */
public final class CalculationRepository {

  // The logger.
  private static final Logger LOG = LoggerFactory.getLogger(CalculationRepository.class);

  /**
   * Batch size interval between batch commits to the database.
   *
   * A high value (>1000) will take a long time to complete, an even higher value will cause OutOfMemory, a low value (<20) is inefficient because
   * then batching would have no real effect.
   */
  private static final int BATCH_SIZE = 500;
  private static final int UNSAFE_BATCH_SIZE = 1000;

  // SQL queries for calculation(s)
  private static final String GET_CALCULATION =
      "SELECT calculation_id, calculation_point_set_id, year, creation_time, state "
          + " FROM calculations WHERE calculation_id = ? ";
  private static final String INSERT_CALCULATION =
      "INSERT INTO calculations (calculation_point_set_id, year) VALUES (?,?)";
  private static final String UPDATE_CALCULATION_STATE =
      "UPDATE calculations "
          + " SET state = ?::calculation_state_type WHERE calculation_id = ? ";

  // SQL queries for calculation point sets
  private static final String CREATE_CALCULATION_POINT_SET =
      "INSERT INTO calculation_point_sets DEFAULT VALUES";
  private static final String INSERT_CALCULATION_POINTS =
      "INSERT INTO calculation_points (calculation_point_set_id, calculation_point_id, label, nearest_receptor_id, geometry)"
          + " VALUES (?,?,?,?,ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()))";

  // SQL queries for calculation result sets
  private static final String INSERT_CALCULATION_RESULT_SET =
      "INSERT INTO calculation_result_sets (calculation_id, result_set_type, result_set_type_key, result_type, substance_id) "
          + " VALUES (?, ?::calculation_result_set_type, ?, ?::emission_result_type, ?)";
  private static final String GET_CALCULATION_RESULT_SETS =
      "SELECT * FROM calculation_result_sets WHERE calculation_id = ?";
  private static final String GET_CALCULATION_RESULT_SET_ID =
      "SELECT calculation_result_set_id FROM calculation_result_sets "
          + " WHERE calculation_id = ? AND result_type = ?::emission_result_type AND substance_id = ? "
          + " AND result_set_type = ?::calculation_result_set_type AND result_set_type_key = ?";

  // SQL queries for calculation results inserts
  private static final String SAFE_INSERT_CALCULATION_RESULTS =
      "INSERT INTO calculation_results (calculation_result_set_id, receptor_id, result) "
          + "SELECT ?,?,? WHERE NOT EXISTS (SELECT calculation_result_set_id FROM calculation_results "
          + "WHERE calculation_result_set_id = ? AND receptor_id = ? LIMIT 1)";
  private static final String UNSAFE_INSERT_CALCULATION_RESULTS =
      "INSERT INTO calculation_results (calculation_result_set_id, receptor_id, result) "
          + "VALUES (?,?,?)";
  private static final String SAFE_INSERT_CALCULATION_POINT_RESULTS =
      "INSERT INTO calculation_point_results (calculation_result_set_id, calculation_point_id, result) "
          + " SELECT ?,?,? WHERE NOT EXISTS (SELECT calculation_result_set_id FROM calculation_point_results "
          + " WHERE calculation_result_set_id = ? AND calculation_point_id = ? LIMIT 1) ";
  private static final String UNSAFE_INSERT_CALCULATION_POINT_RESULTS =
      "INSERT INTO calculation_point_results (calculation_result_set_id, calculation_point_id, result) "
          + "VALUES (?,?,?)";

  // Misc other SQL queries.
  private static final String INSERT_CALCULATION_BATCH_OPTIONS =
      "INSERT INTO calculation_batch_options "
          + " (calculation_id, description, input_file) "
          + " VALUES (?,?,?)";

  private static final Query GET_ALL_RECEPTORS = QueryBuilder.from("calculation_results_view")
      .select(QueryAttribute.RECEPTOR_ID).distinct()
      .where(QueryAttribute.CALCULATION_ID).getQuery();

  private static final Query GET_ALL_CALCULATION_POINTS = QueryBuilder.from("calculation_point_results_view")
      .select(QueryAttribute.CALCULATION_POINT_ID, QueryAttribute.LABEL)
      .select(new SelectClause("ST_X(geometry)", QueryAttribute.X_COORD.attribute()),
          new SelectClause("ST_Y(geometry)", QueryAttribute.Y_COORD.attribute())).distinct()
      .where(QueryAttribute.CALCULATION_ID).getQuery();

  private static final String DELETE_CALCULATION = "SELECT ae_delete_calculation(?)";

  private static final String DELETE_LOW_CALCULATION_RESULTS = "SELECT ae_delete_low_calculation_results(?)";


  // Not allowed to instantiate.
  private CalculationRepository() {}

  /**
   * @param connection The connection to use
   * @param calculationId The id of the calculation trying to retrieve
   * @return The calculation object, null if not found. Calculation options (if existing) are NOT set. (use getCalculationOptions to get those.)
   * @throws SQLException If an error occurred communicating with the DB.
   */
  public static Calculation getCalculation(final Connection connection, final int calculationId) throws SQLException {
    final Calculation calculation = new Calculation();
    calculation.setCalculationId(calculationId);
    return getCalculation(connection, calculation);
  }

  private static Calculation getCalculation(final Connection connection, final Calculation calculation) throws SQLException {
    final Calculation foundCalculation;
    try (final PreparedStatement selectPS = connection.prepareStatement(GET_CALCULATION)) {
      selectPS.setLong(1, calculation.getCalculationId());
      try (final ResultSet rs = selectPS.executeQuery()) {
        // calculation_id, creation_time, state, year
        if (rs.next()) {
          calculation.setCalculationId(rs.getInt("calculation_id"));
          final CalculationSetOptions options = calculation.getOptions();
          if (rs.getInt("calculation_point_set_id") > 0) {
            options.setCalculationType(CalculationType.CUSTOM_POINTS);
          }
          calculation.setCreationDate(rs.getTimestamp("creation_time"));
          calculation.setState(EnumUtil.get(CalculationState.class, rs.getString("state")));
          calculation.setYear(rs.getInt("year"));
          foundCalculation = calculation;
        } else {
          foundCalculation = null;
        }
      }
    }
    return foundCalculation;
  }

  /**
   * @param connection The connection to use.
   * @param calculation the calculation object to persist to the database and fill with some extra information.
   * @param calculationPointSetId The calculation point set ID that the calculation will use.
   * @return The filled calculation.
   * @throws SQLException If an error occurred communicating with the DB.
   */
  public static Calculation insertCalculation(final Connection connection, final Calculation calculation, final Integer calculationPointSetId)
      throws SQLException {
    // ensure a calculation has a set it belongs to, even if it's the only one.
    try (final PreparedStatement insertPS = connection.prepareStatement(INSERT_CALCULATION, Statement.RETURN_GENERATED_KEYS)) {
      if (calculationPointSetId == null) {
        insertPS.setNull(1, Types.INTEGER);
      } else {
        insertPS.setInt(1, calculationPointSetId);
      }
      insertPS.setInt(2, calculation.getYear());
      insertPS.executeUpdate();
      try (final ResultSet rs = insertPS.getGeneratedKeys()) {
        if (rs.next()) {
          calculation.setCalculationId(rs.getInt(1));
        } else {
          throw new SQLException("No generated key obtained while saving new calculation.");
        }
      }
    }
    return getCalculation(connection, calculation);
  }

  /**
   * @param connection The connection to use.
   * @param calculationId The id of the calculation to insert the custom job info for.
   * @param description The description to use.
   * @param inputFile The input file that are/were used for the job.
   * @throws SQLException If an error occurred communicating with the DB.
   */
  public static void insertCalculationBatchOptions(final Connection connection, final int calculationId, final String description,
      final String inputFile) throws SQLException {
    try (final PreparedStatement insertPS = connection.prepareStatement(INSERT_CALCULATION_BATCH_OPTIONS)) {
      // (calculation_id, category, description, input_file)
      int paramIdx = 1;
      insertPS.setLong(paramIdx++, calculationId);
      // category can be null
      insertPS.setString(paramIdx++, description);
      insertPS.setString(paramIdx++, inputFile);
      insertPS.executeUpdate();
    }
  }

  /**
   * @param connection The connection to use.
   * @param calculationId The id of the calculation to update the state for.
   * @param state The state to set for this particular calculation.
   * @return True if update was successful (calculation was found in DB and updated). False if calculation could not be found.
   * @throws SQLException If an error occurred communicating with the DB.
   */
  public static boolean updateCalculationState(final Connection connection, final int calculationId, final CalculationState state)
      throws SQLException {
    boolean updated = false;
    try (final PreparedStatement updatePS = connection.prepareStatement(UPDATE_CALCULATION_STATE)) {
      updatePS.setString(1, state.toString());
      updatePS.setInt(2, calculationId);
      updated = updatePS.executeUpdate() > 0;
    }
    return updated;
  }

  /**
   * Remove all calculation results for a calculation id.
   *
   * @param connection The connection to use
   * @param calculationId if of calculation to remove
   */
  public static void removeCalculation(final Connection connection, final int calculationId) {
    try {
      try (final PreparedStatement selectPS = connection.prepareStatement(DELETE_CALCULATION)) {
        selectPS.setInt(1, calculationId);
        selectPS.execute();
      }
    } catch (final SQLException e) {
      LOG.error("Delete of calculation {} failed.", calculationId, e);
    }
  }

  /**
   * @param connection The connection to use
   * @param calculationPoints The calculation points to persist as a set to the database.
   * @param <T> The type of AeriusPoints to insert.
   * @return The ID that the inserted set has obtained.
   * @throws SQLException In case of database errors.
   */
  public static int insertCalculationPointSet(final Connection connection, final List<CalculationPointFeature> calculationPoints)
      throws SQLException {
    final int calculationPointSetId;

    try (final PreparedStatement insertPS = connection.prepareStatement(CREATE_CALCULATION_POINT_SET, Statement.RETURN_GENERATED_KEYS)) {
      insertPS.executeUpdate();
      try (final ResultSet rs = insertPS.getGeneratedKeys()) {
        if (rs.next()) {
          calculationPointSetId = rs.getInt(1);
        } else {
          throw new SQLException("No generated key obtained while creating new calculation point set.");
        }
      }
    }
    return insertCalculationPointsForSet(connection, INSERT_CALCULATION_POINTS, calculationPointSetId, calculationPoints);
  }

  /**
   * @param connection
   * @param destinationQuery
   * @param calculationPointSetId
   * @param calculationPoints
   * @return
   * @throws SQLException
   */
  static int insertCalculationPointsForSet(final Connection connection, final String destinationQuery,
      final int calculationPointSetId, final List<CalculationPointFeature> calculationPoints) throws SQLException {
    final ReceptorGridSettings rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(connection);
    final ReceptorUtil receptorUtil = new ReceptorUtil(rgs);
    final BatchInserter<CalculationPointFeature> inserter = new BatchInserter<CalculationPointFeature>() {
      @Override
      public void setParameters(final PreparedStatement ps, final CalculationPointFeature feature) throws SQLException {
        final Point point = feature.getGeometry();
        final CalculationPoint calculationPoint = feature.getProperties();
        final int nearestReceptorId = receptorUtil.getReceptorIdFromCoordinate(point.getX(), point.getY(), rgs.getZoomLevel1());
        int paramIdx = 1;
        ps.setInt(paramIdx++, calculationPointSetId);
        ps.setInt(paramIdx++, calculationPoint.getId());
        ps.setString(paramIdx++, calculationPoint.getLabel());
        ps.setInt(paramIdx++, nearestReceptorId);
        ps.setDouble(paramIdx++, point.getX());
        ps.setDouble(paramIdx++, point.getY());
      }
    };
    inserter.setBatchSize(BATCH_SIZE);
    final int inserted = inserter.insertBatch(connection, destinationQuery, calculationPoints);
    if (inserted != calculationPoints.size()) {
      throw new SQLException("Did not save all calculation points! calculation point set id: " + calculationPointSetId + ". Inserted points: "
          + inserted + ". Expected: " + calculationPoints.size());
    }

    return calculationPointSetId;
  }

  public static Collection<EmissionResultKey> getCalculationResultSets(final Connection connection, final int calculationId) throws SQLException {
    final Set<EmissionResultKey> erks = EnumSet.noneOf(EmissionResultKey.class);

    try (final PreparedStatement selectPS = connection.prepareStatement(GET_CALCULATION_RESULT_SETS)) {
      QueryUtil.setValues(selectPS, calculationId);
      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          erks.add(EmissionResultKey.valueOf(Substance.substanceFromId(rs.getInt("substance_id")),
              EmissionResultType.safeValueOf((String) rs.getObject("result_type"))));
        }
      }
    }
    return erks;
  }

  private static int getCalculationResultSetId(final Connection connection, final int calculationId, final EmissionResultKey key,
      final CalculationResultSetType crsType, final int resetSetTypeKey) throws SQLException {
    int resultSetId;
    try (final PreparedStatement selectPS = connection.prepareStatement(GET_CALCULATION_RESULT_SET_ID)) {
      QueryUtil.setValues(selectPS, calculationId, key.getEmissionResultType().toDatabaseString(), key.getSubstance().getId(),
          crsType.toDatabaseString(), resetSetTypeKey);
      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          resultSetId = rs.getInt("calculation_result_set_id");
        } else {
          resultSetId = -1;
        }
      }
    }
    // if no id in db create one
    if (resultSetId == -1) {
      try {
        resultSetId = insertCalculationResultSet(connection, calculationId, key, crsType, resetSetTypeKey);
      } catch (final PSQLException e) {
        if (PMF.UNIQUE_VIOLATION.equals(e.getSQLState())) {
          // poor man fallback in case 2 threads try to create the result set at the same time.
          resultSetId = getCalculationResultSetId(connection, calculationId, key, crsType, resetSetTypeKey);
        } else {
          throw e;
        }
      }
    }
    return resultSetId;
  }

  /**
   * Creates a new calculation in the database. Including the calculation points (no results).
   *
   * @param con The connection to use
   * @param scenarioCalculations the scenario calculations to store
   * @param customPointsInDatabase
   * @throws SQLException If an error occurred communicating with the DB
   */
  public static void insertCalculations(final Connection con, final ScenarioCalculations scenarioCalculations, final boolean customPointsInDatabase)
      throws SQLException {
    final CalculationPointSetTracker pointSetTracker = scenarioCalculations.getCalculationPointSetTracker();
    if (customPointsInDatabase && !scenarioCalculations.getCalculationPoints().isEmpty()
        && pointSetTracker.getCalculationPointSetId() == null) {
      pointSetTracker.setCalculationPointSetId(insertCalculationPointSet(con, scenarioCalculations.getCalculationPoints()));
    }
    for (final Calculation calculation : scenarioCalculations.getCalculations()) {
      insertCalculation(con, calculation, pointSetTracker.getCalculationPointSetId());
    }
  }

  private static int insertCalculationResultSet(final Connection connection, final int calculationId, final EmissionResultKey key,
      final CalculationResultSetType crsType, final int resetSetTypeKey) throws SQLException {
    try (final PreparedStatement insertPS = connection.prepareStatement(INSERT_CALCULATION_RESULT_SET, Statement.RETURN_GENERATED_KEYS)) {
      QueryUtil.setValues(insertPS, calculationId, crsType.toDatabaseString(), resetSetTypeKey, key.getEmissionResultType().toDatabaseString(),
          key.getSubstance().getId());
      insertPS.executeUpdate();
      try (final ResultSet insertRS = insertPS.getGeneratedKeys()) {
        if (insertRS.next()) {
          return insertRS.getInt(1);
        } else {
          throw new SQLException("No generated key obtained while saving new calculation result set. (" + calculationId + ", " + key + ")");
        }
      }
    }
  }

  public static int insertCalculationResults(final Connection connection, final int calculationId, final List<AeriusResultPoint> result)
      throws SQLException {
    return insertCalculationResults(connection, calculationId, CalculationResultSetType.TOTAL, 0, result);
  }

  /**
   * Insert results from a calculation. If calculation is not found, an illegal argument exception will occur. If there is already a result in the
   * database for the corresponding substance and receptor ID, the new value will NOT overwrite the existing value. It won't cause an exception either
   * and will just continue to insert the next result.
   *
   * @param connection The connection to use
   * @param calculationId id of the calculation
   * @param result Contains all results (where each receptor ID should correspond to supplied AeriusPoints)
   * @return The number of rows inserted
   * @throws SQLException If an error occurred communicating with the DB or when any of the ID's are unknown
   */
  public static int insertCalculationResults(final Connection connection, final int calculationId, final CalculationResultSetType crsType,
      final int resetSetTypeKey, final List<AeriusResultPoint> result) throws SQLException {
    return insertCalculationResults(connection, calculationId, crsType, resetSetTypeKey, result, SAFE_INSERT_CALCULATION_RESULTS,
        SAFE_INSERT_CALCULATION_POINT_RESULTS, true);
  }

  public static int insertCalculationResultsUnsafe(final Connection connection, final int calculationId, final List<AeriusResultPoint> result)
      throws SQLException {
    return insertCalculationResults(connection, calculationId, CalculationResultSetType.TOTAL, 0, result, UNSAFE_INSERT_CALCULATION_RESULTS,
        UNSAFE_INSERT_CALCULATION_POINT_RESULTS, false);
  }

  /**
   * Insert results from a calculation _unsafely_. If calculation is not found, an illegal argument exception will occur.
   *
   * Use only when you can guarantee there will be no duplicates.
   *
   * @param connection The connection to use.
   * @param calculationId id of the calculation
   * @param result contains the results (where each receptor ID should correspond to supplied AeriusPoints).
   * @return The number of rows inserted.
   * @throws SQLException If an error occurred communicating with the DB or when any of the ID's are unknown.
   */
  public static int insertCalculationResultsUnsafe(final Connection connection, final int calculationId, final CalculationResultSetType crsType,
      final int resetSetTypeKey, final List<AeriusResultPoint> result) throws SQLException {
    return insertCalculationResults(connection, calculationId, crsType, resetSetTypeKey, result, UNSAFE_INSERT_CALCULATION_RESULTS,
        UNSAFE_INSERT_CALCULATION_POINT_RESULTS, false);
  }

  private static Map<AeriusPointType, List<PointResult>> toPointResults(final Connection connection, final int calculationId,
      final CalculationResultSetType crsType, final int resetSetTypeKey, final List<AeriusResultPoint> result) throws SQLException {
    final Calculation calculation = getCalculation(connection, calculationId);
    if (calculation == null) {
      throw new IllegalArgumentException("Calculation should exist in DB.");
    }
    // each emission result key has their own set in DB, only query for it once though.
    final Map<EmissionResultKey, Integer> resultSetIdMap = new HashMap<>();
    final Map<AeriusPointType, List<PointResult>> insertables = new HashMap<>();
    for (final AeriusResultPoint receptorPoint : result) {
      if (!insertables.containsKey(receptorPoint.getPointType())) {
        insertables.put(receptorPoint.getPointType(), new ArrayList<PointResult>());
      }
      final List<PointResult> points = insertables.get(receptorPoint.getPointType());
      for (final Entry<EmissionResultKey, Double> emissionResult : receptorPoint.getEmissionResults().entrySet()) {
        if (emissionResult.getKey().getSubstance() != Substance.NOXNH3
            && emissionResult.getKey().getEmissionResultType() != EmissionResultType.EXCEEDANCE_DAYS) {
          if (!resultSetIdMap.containsKey(emissionResult.getKey())) {
            resultSetIdMap.put(emissionResult.getKey(),
                getCalculationResultSetId(connection, calculation.getCalculationId(), emissionResult.getKey(), crsType, resetSetTypeKey));
          }
          points.add(new PointResult(receptorPoint.getId(), resultSetIdMap.get(emissionResult.getKey()), emissionResult.getValue()));
        }
      }
    }
    return insertables;
  }

  private static int insertCalculationResults(final Connection con, final int calculationId, final CalculationResultSetType crsType,
      final int resetSetTypeKey, final List<AeriusResultPoint> result, final String receptorInsertQuery, final String insertPointQuery,
      final boolean safeInserts) throws SQLException {
    final Map<AeriusPointType, List<PointResult>> splitResults = toPointResults(con, calculationId, crsType, resetSetTypeKey, result);
    int inserted = 0;
    if (splitResults.containsKey(AeriusPointType.RECEPTOR)) {
      inserted += insertCalculationResults(con, splitResults.get(AeriusPointType.RECEPTOR), receptorInsertQuery, safeInserts);
    }
    if (splitResults.containsKey(AeriusPointType.POINT)) {
      inserted += insertCalculationResults(con, splitResults.get(AeriusPointType.POINT), insertPointQuery, safeInserts);
    }
    return inserted;
  }

  private static int insertCalculationResults(final Connection connection, final List<PointResult> insertables, final String query,
      final boolean safeInserts) throws SQLException {
    final BatchInserter<PointResult> inserter = new BatchInserter<CalculationRepository.PointResult>() {

      @Override
      public void setParameters(final PreparedStatement ps, final PointResult object) throws SQLException {
        int paramIdx = 1;
        ps.setInt(paramIdx++, object.getResultSetId());
        ps.setInt(paramIdx++, object.getPointId());
        // Negative values not allowed; safety check here:
        if (object.getResult() < 0) {
          LOG.warn("Result < 0: resultsetId:{}, id:{}, value:{}", object.getResultSetId(), object.getPointId(), object.getResult());
        }
        ps.setDouble(paramIdx++, Math.max(0, object.getResult()));
        if (safeInserts) {
          ps.setInt(paramIdx++, object.getResultSetId());
          ps.setInt(paramIdx++, object.getPointId());
        }
      }
    };
    inserter.setBatchSize(safeInserts ? BATCH_SIZE : UNSAFE_BATCH_SIZE);
    return inserter.insertBatch(connection, query, insertables);
  }

  /**
   * Delete calculation results that are below the threshold value.
   *
   * @param connection database connection
   * @param calculationId Calculation id of the situation to delete results for
   * @throws SQLException sql error
   */
  public static void deleteLowCalculationResults(final Connection connection, final int calculationId) throws SQLException {
    try (final PreparedStatement ps = connection.prepareStatement(DELETE_LOW_CALCULATION_RESULTS)) {
      QueryUtil.setValues(ps, calculationId);
      ps.execute();
    }
  }

  /**
   * Retrieve the points for a certain calculation (without results).
   *
   * @param connection The connection to use
   * @param calculationId The ID of the calculation to insert the options for
   * @return The (current) CalculationResult that match this calculation ID
   * @throws SQLException If an error occurred communicating with the DB
   */
  public static List<CalculationPointFeature> getCalculatedPoints(final Connection connection, final int calculationId) throws SQLException {
    final List<CalculationPointFeature> points = new ArrayList<>();

    points.addAll(getReceptors(connection, calculationId));
    points.addAll(getCalculationPoints(connection, calculationId));

    return points;
  }

  private static List<CalculationPointFeature> getReceptors(final Connection connection, final int calculationId) throws SQLException {
    final List<CalculationPointFeature> receptors = new ArrayList<>();
    final ReceptorUtil receptorUtil = new ReceptorUtil(ReceptorGridSettingsRepository.getReceptorGridSettings(connection));
    try (final PreparedStatement ps = connection.prepareStatement(GET_ALL_RECEPTORS.get())) {
      GET_ALL_RECEPTORS.setParameter(ps, QueryAttribute.CALCULATION_ID, calculationId);

      try (final ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          final int receptorId = QueryAttribute.RECEPTOR_ID.getInt(rs);
          final CalculationPointFeature receptorPoint = toReceptorFeature(receptorUtil, receptorId);
          receptors.add(receptorPoint);
        }
      }
    }
    return receptors;
  }

  private static CalculationPointFeature toReceptorFeature(final ReceptorUtil receptorUtil, final int receptorId) {
    final CalculationPointFeature feature = new CalculationPointFeature();
    feature.setId(String.valueOf(receptorId));

    final Point point = receptorUtil.getPointFromReceptorId(receptorId);
    feature.setGeometry(point);

    final ReceptorPoint receptor = new ReceptorPoint();
    receptor.setReceptorId(receptorId);
    feature.setProperties(receptor);
    return feature;
  }

  private static List<CalculationPointFeature> getCalculationPoints(final Connection connection, final int calculationId) throws SQLException {
    final List<CalculationPointFeature> points = new ArrayList<>();
    try (final PreparedStatement ps = connection.prepareStatement(GET_ALL_CALCULATION_POINTS.get())) {
      GET_ALL_CALCULATION_POINTS.setParameter(ps, QueryAttribute.CALCULATION_ID, calculationId);

      try (final ResultSet rs = ps.executeQuery()) {

        while (rs.next()) {
          final int calculationPointId = QueryAttribute.CALCULATION_POINT_ID.getInt(rs);
          final double x = QueryUtil.getDouble(rs, QueryAttribute.X_COORD);
          final double y = QueryUtil.getDouble(rs, QueryAttribute.Y_COORD);
          final String label = QueryAttribute.LABEL.getString(rs);
          final CalculationPointFeature calculationPoint = toCustomFeature(calculationPointId, x, y, label);
          points.add(calculationPoint);
        }
      }
    }
    return points;
  }

  private static CalculationPointFeature toCustomFeature(final int calculationPointId, final double x, final double y, final String label) {
    final CalculationPointFeature feature = new CalculationPointFeature();
    feature.setId(String.valueOf(calculationPointId));
    feature.setGeometry(new Point(x, y));

    final CustomCalculationPoint customPoint = new CustomCalculationPoint();
    customPoint.setCustomPointId(calculationPointId);
    customPoint.setLabel(label);
    feature.setProperties(customPoint);
    return feature;
  }

  private static class PointResult {

    private final int pointId;
    private final int resultSetId;
    private final double result;

    PointResult(final int pointId, final int resultSetId, final double result) {
      this.pointId = pointId;
      this.resultSetId = resultSetId;
      this.result = result;
    }

    public int getPointId() {
      return pointId;
    }

    public int getResultSetId() {
      return resultSetId;
    }

    public double getResult() {
      return result;
    }

    @Override
    public String toString() {
      return "PointResult [resultSetId=" + resultSetId + ", pointId=" + pointId + ", result=" + result + "]";
    }
  }
}
