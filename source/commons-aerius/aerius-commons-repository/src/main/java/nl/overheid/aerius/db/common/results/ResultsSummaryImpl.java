/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.results;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntFunction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.db.util.WhereClause;
import nl.overheid.aerius.shared.domain.calculation.SituationCalculations;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.summary.CustomCalculationPointResult;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticType;
import nl.overheid.aerius.shared.domain.summary.ResultStatisticsMarker;
import nl.overheid.aerius.shared.domain.summary.SituationResultsAreaSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsHabitatSummary;
import nl.overheid.aerius.shared.domain.summary.SituationResultsReceptor;
import nl.overheid.aerius.shared.domain.summary.SituationResultsReceptors;
import nl.overheid.aerius.shared.domain.summary.SituationResultsStatistics;
import nl.overheid.aerius.shared.domain.summary.SituationResultsSummary;
import nl.overheid.aerius.shared.domain.summary.SummaryHexagonType;
import nl.overheid.aerius.shared.domain.summary.SurfaceChartResults;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.geometry.ReceptorUtil;

abstract class ResultsSummaryImpl<I> {

  private static final Logger LOG = LoggerFactory.getLogger(ResultsSummaryImpl.class);

  protected enum RepositoryAttribute implements Attribute {
    SUM_CARTOGRAPHIC_SURFACE,
    SUM_CARTOGRAPHIC_SURFACE_INCREASE,
    SUM_CARTOGRAPHIC_SURFACE_DECREASE,
    MAX_CONTRIBUTION,
    MAX_INCREASE,
    MAX_DECREASE,
    MAX_TOTAL,

    COLOR_RANGE_TYPE,
    LOWER_BOUND,

    HEXAGON_TYPE,
    RESULT_TYPE,
    RESULT_STATISTIC_TYPE,

    DEPOSITION;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  protected static final WhereClause WHERE_HEXAGON_TYPE =
      new StaticWhereClause(RepositoryAttribute.HEXAGON_TYPE.name() + " = ?::hexagon_type", RepositoryAttribute.HEXAGON_TYPE);

  protected static final WhereClause WHERE_RESULT_TYPE =
      new StaticWhereClause(RepositoryAttribute.RESULT_TYPE.name() + " = ?::scenario_result_type", RepositoryAttribute.RESULT_TYPE);

  private static final int RESULT_COMPARISON_FACTOR = 100;

  protected final PMF pmf;
  protected final ReceptorUtil receptorUtil;

  /**
   * Only use this constructor if you're certain the repository won't be used to retrieve information.
   */
  ResultsSummaryImpl(final PMF pmf) {
    this(pmf, null);
  }

  ResultsSummaryImpl(final PMF pmf, final ReceptorUtil receptorUtil) {
    this.pmf = pmf;
    this.receptorUtil = receptorUtil;
  }

  protected abstract I createSummaryInput(final SituationCalculations situationCalculations, final Integer calculationId);

  protected abstract List<I> createSummaryInputs(final SituationCalculations situationCalculations);

  protected abstract PreparedStatement prepareOverallStatisticsStatement(final Connection connection, final int jobId, final I summaryInput,
      final SummaryHexagonType hexagonType) throws SQLException;

  protected abstract PreparedStatement prepareAreaStatisticsStatement(final Connection connection, final int jobId, final I summaryInput,
      final SummaryHexagonType hexagonType) throws SQLException;

  protected abstract PreparedStatement prepareHabitatStatisticsStatement(final Connection connection, final int jobId, final I summaryInput,
      final SummaryHexagonType hexagonType, final int assessmentAreaId) throws SQLException;

  protected abstract PreparedStatement prepareChartStatisticsStatement(final Connection connection, final int jobId, final I summaryInput,
      final SummaryHexagonType hexagonType, final int assessmentAreaId) throws SQLException;

  protected abstract PreparedStatement prepareStatisticMarkersStatement(final Connection connection, final int jobId, final I summaryInput,
      final SummaryHexagonType hexagonType) throws SQLException;

  protected abstract PreparedStatement prepareReceptorStatisticsStatement(final Connection connection, final int jobId, final I summaryInput,
      final SummaryHexagonType hexagonType) throws SQLException;

  protected abstract PreparedStatement prepareCustomCalculationPointResultsStatement(final Connection connection, final I summaryInput)
      throws SQLException;

  protected abstract PreparedStatement prepareInsertAreaResultsSummaryStatement(final Connection connection, final int jobId, final I summaryInput,
      final int assessmentAreaId) throws SQLException;

  protected abstract int compare(final SituationResultsStatistics statistics1, final SituationResultsStatistics statistics2);

  public SituationResultsSummary determineReceptorResultsSummary(final SituationCalculations situationCalculations, final int jobId,
      final Integer calculationId, final SummaryHexagonType hexagonType, final IntFunction<AssessmentArea> assessmentAreaRetriever,
      final IntFunction<HabitatType> habitatTypeRetriever) throws AeriusException {
    final I summaryInput = createSummaryInput(situationCalculations, calculationId);
    try (final Connection con = pmf.getConnection()) {
      final SituationResultsStatistics overallStatistics = determineOverallStatistics(con, jobId, summaryInput, hexagonType);
      final List<SituationResultsAreaSummary> areaSummaries = determineAreaStatistics(con, jobId, summaryInput, hexagonType,
          assessmentAreaRetriever, habitatTypeRetriever);
      final List<ResultStatisticsMarker> markers = determineStatisticMarkers(con, jobId, summaryInput, hexagonType);
      final SituationResultsReceptors receptors = determineReceptorStatistics(con, jobId, summaryInput, hexagonType, assessmentAreaRetriever);
      return new SituationResultsSummary(overallStatistics, areaSummaries, markers, receptors, List.of());
    } catch (final SQLException e) {
      LOG.error("SQL error while getting results summary", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public List<CustomCalculationPointResult> determineCustomCalculationPointResults(final SituationCalculations situationCalculations,
      final Integer calculationId) throws AeriusException {
    final I summaryInput = createSummaryInput(situationCalculations, calculationId);
    try (final Connection con = pmf.getConnection()) {
      return determineCustomCalculationPointResults(con, summaryInput);
    } catch (final SQLException e) {
      LOG.error("SQL error while getting custom point results", e);
      throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
    }
  }

  public SituationResultsStatistics determineOverallStatistics(final Connection connection, final int jobId, final I summaryInput,
      final SummaryHexagonType hexagonType) throws SQLException {
    try (final PreparedStatement stmt = prepareOverallStatisticsStatement(connection, jobId, summaryInput, hexagonType)) {
      final SituationResultsStatistics result = new SituationResultsStatistics();

      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final ResultStatisticType statisticType = QueryUtil.getEnum(rs, RepositoryAttribute.RESULT_STATISTIC_TYPE, ResultStatisticType.class);
          final double value = QueryAttribute.VALUE.getDouble(rs);
          result.put(statisticType, value);
        }
      }
      return result;
    }
  }

  private List<SituationResultsAreaSummary> determineAreaStatistics(final Connection connection, final int jobId, final I summaryInput,
      final SummaryHexagonType hexagonType, final IntFunction<AssessmentArea> assessmentAreaRetriever,
      final IntFunction<HabitatType> habitatTypeRetriever) throws SQLException {
    final Map<Integer, SituationResultsAreaSummary> resultsPerArea = new HashMap<>();
    try (final PreparedStatement stmt = prepareAreaStatisticsStatement(connection, jobId, summaryInput, hexagonType)) {

      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
          if (!resultsPerArea.containsKey(assessmentAreaId)) {
            final AssessmentArea area = assessmentAreaRetriever.apply(assessmentAreaId);
            final List<SurfaceChartResults> chartResults = determineChartResults(connection, jobId, summaryInput, hexagonType, assessmentAreaId);
            final List<SituationResultsHabitatSummary> habitatSummaries =
                determineHabitatStatistics(connection, jobId, summaryInput, hexagonType, assessmentAreaId, habitatTypeRetriever);
            resultsPerArea.put(assessmentAreaId, new SituationResultsAreaSummary(area, chartResults, habitatSummaries));
          }
          final ResultStatisticType statisticType = QueryUtil.getEnum(rs, RepositoryAttribute.RESULT_STATISTIC_TYPE, ResultStatisticType.class);
          final double value = QueryAttribute.VALUE.getDouble(rs);
          resultsPerArea.get(assessmentAreaId).getStatistics().put(statisticType, value);
        }
      }
      final List<SituationResultsAreaSummary> results = new ArrayList<>(resultsPerArea.values());
      Collections.sort(results, this::compare);
      return results;
    }
  }

  private List<SituationResultsHabitatSummary> determineHabitatStatistics(final Connection connection, final int jobId, final I summaryInput,
      final SummaryHexagonType hexagonType, final int assessmentAreaId, final IntFunction<HabitatType> habitatTypeRetriever)
      throws SQLException {
    final Map<Integer, SituationResultsHabitatSummary> resultsPerHabitat = new HashMap<>();
    try (final PreparedStatement stmt = prepareHabitatStatisticsStatement(connection, jobId, summaryInput, hexagonType, assessmentAreaId)) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final int habitatTypeId = QueryAttribute.CRITICAL_DEPOSITION_AREA_ID.getInt(rs);
          if (!resultsPerHabitat.containsKey(habitatTypeId)) {
            final HabitatType habitatType = habitatTypeRetriever.apply(habitatTypeId);

            resultsPerHabitat.put(habitatTypeId, new SituationResultsHabitatSummary(habitatType));
          }
          final ResultStatisticType statisticType = QueryUtil.getEnum(rs, RepositoryAttribute.RESULT_STATISTIC_TYPE, ResultStatisticType.class);
          final double value = QueryAttribute.VALUE.getDouble(rs);
          resultsPerHabitat.get(habitatTypeId).getStatistics().put(statisticType, value);
        }
      }
      final List<SituationResultsHabitatSummary> results = new ArrayList<>(resultsPerHabitat.values());
      Collections.sort(results, this::compare);
      return results;
    }
  }

  private List<SurfaceChartResults> determineChartResults(final Connection connection, final int jobId, final I summaryInput,
      final SummaryHexagonType hexagonType, final int assessmentAreaId) throws SQLException {
    final List<SurfaceChartResults> chartResults = new ArrayList<>();
    try (final PreparedStatement stmt = prepareChartStatisticsStatement(connection, jobId, summaryInput, hexagonType, assessmentAreaId)) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final double lowerBound = QueryUtil.getDouble(rs, RepositoryAttribute.LOWER_BOUND);
          final double cartographicSurface = QueryAttribute.CARTOGRAPHIC_SURFACE.getDouble(rs);
          chartResults.add(new SurfaceChartResults(lowerBound, cartographicSurface));
        }
      }
      return chartResults;
    }
  }

  private List<ResultStatisticsMarker> determineStatisticMarkers(final Connection connection, final int jobId, final I summaryInput,
      final SummaryHexagonType hexagonType) throws SQLException {
    final Map<Integer, ResultStatisticsMarker> markersPerReceptor = new HashMap<>();
    try (final PreparedStatement stmt = prepareStatisticMarkersStatement(connection, jobId, summaryInput, hexagonType)) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final int receptorId = QueryAttribute.RECEPTOR_ID.getInt(rs);
          if (receptorId != 0) {
            if (!markersPerReceptor.containsKey(receptorId)) {
              final Point point = receptorUtil.getPointFromReceptorId(receptorId);

              markersPerReceptor.put(receptorId, new ResultStatisticsMarker(receptorId, point));
            }
            final ResultStatisticType statisticType = QueryUtil.getEnum(rs, RepositoryAttribute.RESULT_STATISTIC_TYPE, ResultStatisticType.class);
            markersPerReceptor.get(receptorId).getStatisticsTypes().add(statisticType);
          }
        }
      }
      return new ArrayList<>(markersPerReceptor.values());
    }
  }

  private SituationResultsReceptors determineReceptorStatistics(final Connection connection, final int jobId, final I summaryInput,
      final SummaryHexagonType hexagonType, final IntFunction<AssessmentArea> assessmentAreaRetriever) throws SQLException {
    final SituationResultsReceptors receptorMap = new SituationResultsReceptors();
    try (final PreparedStatement stmt = prepareReceptorStatisticsStatement(connection, jobId, summaryInput, hexagonType)) {
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
          final int receptorId = QueryAttribute.RECEPTOR_ID.getInt(rs);
          final AssessmentArea area = assessmentAreaRetriever.apply(assessmentAreaId);
          final ResultStatisticType statisticType = QueryUtil.getEnum(rs, RepositoryAttribute.RESULT_STATISTIC_TYPE, ResultStatisticType.class);

          final SituationResultsReceptor receptor = new SituationResultsReceptor(receptorId, area.getName());
          receptorMap.put(statisticType, receptor);
        }
      }
      return receptorMap;
    }
  }

  private List<CustomCalculationPointResult> determineCustomCalculationPointResults(final Connection connection, final I summaryInput)
      throws SQLException {
    try (final PreparedStatement stmt = prepareCustomCalculationPointResultsStatement(connection, summaryInput)) {
      final List<CustomCalculationPointResult> results = new ArrayList<>();
      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final int calculationPointId = QueryAttribute.CALCULATION_POINT_ID.getInt(rs);
          final double result = QueryUtil.getDouble(rs, RepositoryAttribute.DEPOSITION);

          results.add(new CustomCalculationPointResult(calculationPointId, result));
        }
      }
      return results;
    }
  }

  public void insertResultsSummaryForArea(final int jobId, final SituationCalculations situationCalculations, final int areaId)
      throws AeriusException {
    final List<I> summaryInputs = createSummaryInputs(situationCalculations);
    for (final I summaryInput : summaryInputs) {
      try (final Connection con = pmf.getConnection();
          final PreparedStatement stmt = prepareInsertAreaResultsSummaryStatement(con, jobId, summaryInput, areaId)) {
        stmt.execute();
      } catch (final SQLException e) {
        LOG.error("SQL error while inserting situation results area summary", e);
        throw new AeriusException(AeriusExceptionReason.SQL_ERROR);
      }
    }
  }

  private int compare(final SituationResultsHabitatSummary habitatSummary1, final SituationResultsHabitatSummary habitatSummary2) {
    int compared = compare(habitatSummary1.getStatistics(), habitatSummary2.getStatistics());
    if (compared == 0) {
      compared = habitatSummary1.getHabitatType().getName().compareTo(habitatSummary2.getHabitatType().getName());
    }
    return compared;
  }

  private int compare(final SituationResultsAreaSummary areaSummary1, final SituationResultsAreaSummary areaSummary2) {
    int compared = compare(areaSummary1.getStatistics(), areaSummary2.getStatistics());
    if (compared == 0) {
      compared = areaSummary1.getAssessmentArea().getName().compareTo(areaSummary2.getAssessmentArea().getName());
    }
    return compared;
  }

  protected static int compareValues(final double value1, final double value2) {
    // ensure values are compared so high values are first, and compare based on 2 digit precision.
    return Long.compare(
        Math.round(value2 * RESULT_COMPARISON_FACTOR),
        Math.round(value1 * RESULT_COMPARISON_FACTOR));
  }

}
