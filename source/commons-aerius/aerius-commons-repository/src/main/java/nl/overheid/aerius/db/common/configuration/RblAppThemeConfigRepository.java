/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.configuration;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import jsinterop.annotations.JsProperty;

import nl.aerius.search.domain.SearchCapability;
import nl.aerius.search.domain.SearchRegion;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;

public class RblAppThemeConfigRepository extends AbstractAppThemeConfigRepository {
  private static final ArrayList<Substance> EMISSION_SUBSTANCES_ROAD = new ArrayList<>(
      EnumSet.of(Substance.NOX, Substance.NO2, Substance.PM10, Substance.PM25, Substance.EC));
  private static final List<EmissionResultKey> EMISSION_RESULT_KEYS = Arrays.asList(
      EmissionResultKey.NOX_CONCENTRATION,
      EmissionResultKey.NO2_CONCENTRATION, EmissionResultKey.NO2_EXCEEDANCE_HOURS,
      EmissionResultKey.PM10_CONCENTRATION, EmissionResultKey.PM10_EXCEEDANCE_DAYS,
      EmissionResultKey.PM25_CONCENTRATION,
      EmissionResultKey.EC_CONCENTRATION);
  private static final @JsProperty List<SectorGroup> ALLOWED_SECTORGROUPS = new ArrayList<>(EnumSet.of(SectorGroup.ROAD_TRANSPORTATION));

  @Override
  protected Theme getTheme() {
    return Theme.RBL;
  }

  @Override
  protected CharacteristicsType getCharacteristicsType() {
    return null;
  }

  @Override
  protected SearchRegion getSearchRegion() {
    return SearchRegion.NL;
  }

  @Override
  protected List<SearchCapability> getSearchCapabilities() {
    return Arrays.asList(SearchCapability.BASIC_INFO, SearchCapability.COORDINATE);
  }

  @Override
  protected List<String> getCalculationYears(final Connection con) throws SQLException {
    return Arrays.asList(
        "2019",
        "2020",
        "2021",
        "2022",
        "2023",
        "2024",
        "2025",
        "2026",
        "2027",
        "2028",
        "2029",
        "2030");
  }

  protected List<String> getSrm2Options() {
    return Arrays.asList(
        "2018",
        "2020",
        "2030");
  }

  @Override
  protected List<Substance> getSubstances() {
    return EMISSION_SUBSTANCES_ROAD;
  }

  @Override
  protected List<Substance> getEmissionSubstancesRoad() {
    return EMISSION_SUBSTANCES_ROAD;
  }

  @Override
  protected List<EmissionResultKey> getEmissionResultKeys() {
    return EMISSION_RESULT_KEYS;
  }

  @Override
  protected List<CalculationType> getCalculationTypes() {
    return Arrays.asList(CalculationType.CUSTOM_POINTS);
  }

  @Override
  protected String getCalculationYearDefault() {
    return "2019";
  }

  @Override
  protected CalculationType getDefaultCalculationType() {
    return CalculationType.CUSTOM_POINTS;
  }

  @Override
  protected List<SectorGroup> getSectorGroups() {
    return ALLOWED_SECTORGROUPS;
  }

}
