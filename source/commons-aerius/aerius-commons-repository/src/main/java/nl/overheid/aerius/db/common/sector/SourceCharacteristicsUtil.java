/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import java.sql.ResultSet;
import java.sql.SQLException;

import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;
import nl.overheid.aerius.shared.domain.v2.characteristics.HeatContentType;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;

/**
 * Util class for sector related database operations. Used both in sector repository and category repository.
 */
public final class SourceCharacteristicsUtil {

  public static final Attributes DIURNAL_VARIATION_ATTRIBUTES = new Attributes(QueryAttribute.EMISSION_DIURNAL_VARIATION_ID,
      QueryAttribute.EMISSION_DIURNAL_VARIATION_CODE);

  public static final Attributes OPS_CHARACTERISTICS_ATTRIBUTES = new Attributes(QueryAttribute.GCN_SECTOR_ID, QueryAttribute.HEAT_CONTENT,
      QueryAttribute.HEIGHT, QueryAttribute.SPREAD).add(DIURNAL_VARIATION_ATTRIBUTES);

  private SourceCharacteristicsUtil() {}

  /**
   * Set the right properties on the source characteristics object based on the supplied resultset. Currently using the following columns:
   * heat_content, height, spread, diurnal_variation.
   *
   * <p>
   * particle_size_distribution is not used, instead the sectorId will be used for the PSD property.
   *
   * @param sc The SourceCharacteristics object to fill.
   * @param rs The result set containing the right columns.
   * @param particleSizeDistribution The right value for PSD.
   * @param messagesKey
   * @throws SQLException In case of an error getting the right value from the result set.
   */
  public static void setSourceCharacteristicsFromResultSet(final OPSSourceCharacteristics sc, final ResultSet rs, final int particleSizeDistribution)
      throws SQLException {
    setSourceCharacteristicsFromResultSet(sc, rs, particleSizeDistribution, null);
  }

  /**
   * Set the right properties on the source characteristics object based on the supplied resultset. Currently using the following columns:
   * heat_content, height, spread, diurnal_variation.
   *
   * <p>
   * particle_size_distribution is not used, instead the sectorId will be used for the PSD property.
   *
   * @param sc The SourceCharacteristics object to fill.
   * @param rs The result set containing the right columns.
   * @param particleSizeDistribution The right value for PSD.
   * @param messagesKey
   * @throws SQLException In case of an error getting the right value from the result set.
   */
  public static void setSourceCharacteristicsFromResultSet(final OPSSourceCharacteristics sc, final ResultSet rs, final int particleSizeDistribution,
      final DBMessagesKey messagesKey) throws SQLException {
    final DiurnalVariationSpecification specification = new DiurnalVariationSpecification();
    setDiurnalVariationSpecificationFromResultSet(specification, rs, messagesKey);

    sc.setDiurnalVariation(DiurnalVariation.safeValueOf(specification.getCode()));

    sc.setHeatContentType(HeatContentType.NOT_FORCED);
    sc.setHeatContent(QueryAttribute.HEAT_CONTENT.getDouble(rs));
    sc.setEmissionHeight(QueryAttribute.HEIGHT.getDouble(rs));
    // Source characteristics are queried for NOx, so the psd in the query results is always 0.
    // For PM10 the psd must be equal to the sector id, which is harmless for NOx and NH3.
    sc.setParticleSizeDistribution(particleSizeDistribution == 0 ? QueryAttribute.GCN_SECTOR_ID.getInt(rs) : particleSizeDistribution);
    sc.setSpread(QueryAttribute.SPREAD.getDouble(rs));
  }

  private static void setDiurnalVariationSpecificationFromResultSet(final DiurnalVariationSpecification specification, final ResultSet rs,
      final DBMessagesKey messagesKey) throws SQLException {
    specification.setId(QueryAttribute.EMISSION_DIURNAL_VARIATION_ID.getInt(rs));
    specification.setCode(QueryAttribute.EMISSION_DIURNAL_VARIATION_CODE.getString(rs));

    if (messagesKey != null) {
      DBMessages.setDiurnalVariationMessages(specification, messagesKey);
    }
  }
}
