/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.configuration;

import java.util.Arrays;
import java.util.List;

import nl.aerius.search.domain.SearchCapability;
import nl.aerius.search.domain.SearchRegion;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.v2.characteristics.CharacteristicsType;

public final class WnbAppThemeConfigRepository extends AbstractAppThemeConfigRepository {

  private static final List<EmissionResultKey> EMISSION_RESULT_KEYS = Arrays.asList(EmissionResultKey.NOXNH3_DEPOSITION);

  @Override
  protected Theme getTheme() {
    return Theme.WNB;
  }

  @Override
  protected CharacteristicsType getCharacteristicsType() {
    return CharacteristicsType.OPS;
  }

  @Override
  protected List<EmissionResultKey> getEmissionResultKeys() {
    return EMISSION_RESULT_KEYS;
  }

  @Override
  protected SearchRegion getSearchRegion() {
    return SearchRegion.NL;
  }

  @Override
  protected List<SearchCapability> getSearchCapabilities() {
    return Arrays.asList(SearchCapability.BASIC_INFO, SearchCapability.RECEPTOR, SearchCapability.COORDINATE, SearchCapability.ASSESSMENT_AREA);
  }
}
