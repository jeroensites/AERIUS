/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Basic class containing all information in respect to a given Receptor.
 */
public class ReceptorInfo implements Serializable {

  private static final long serialVersionUID = 1L;

  private List<Natura2000Info> naturaInfo = new ArrayList<>();
  private List<HabitatInfo> habitatTypeInfo = new ArrayList<>();
  private EmissionResultInfo emissionResultInfo;

  public List<Natura2000Info> getNaturaInfo() {
    return naturaInfo;
  }

  public void setNaturaInfo(final List<Natura2000Info> naturaInfo) {
    this.naturaInfo = naturaInfo;
  }

  public List<HabitatInfo> getHabitatTypeInfo() {
    return habitatTypeInfo;
  }

  public void setHabitatTypeInfo(final List<HabitatInfo> habitatTypes) {
    this.habitatTypeInfo = habitatTypes;
  }

  public EmissionResultInfo getEmissionResultInfo() {
    return emissionResultInfo;
  }

  public void setEmissionResultInfo(final EmissionResultInfo emissionResultInfo) {
    this.emissionResultInfo = emissionResultInfo;
  }
}
