/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.importer;

import java.util.Optional;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * Immutable class to contain (optional) properties that can be used when importing.
 */
public class ImportProperties {

  private final Substance substance;
  private final Integer year;

  /**
   * Constructor where all optional fields are assumed to be null.
   */
  public ImportProperties() {
    this(null, null);
  }

  /**
   * @param substance The substance to use when importing. Can be null.
   * @param year The year to use when importing, to determine emissions for instance. Can be null.
   */
  public ImportProperties(final Substance substance, final Integer year) {
    this.substance = substance;
    this.year = year;
  }

  /**
   * @return The substance, which can be null.
   */
  public Substance getSubstance() {
    return substance;
  }

  /**
   * @return The substance, wrapped in an optional object.
   */
  public Optional<Substance> getOptSubstance() {
    return Optional.ofNullable(substance);
  }

  /**
   * @return The year, wrapped in an optional object.
   */
  public Optional<Integer> getYear() {
    return Optional.ofNullable(year);
  }

}
