/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;

/**
 * Data class to contain result values for a single point.
 */
public class CriticalLevels extends EmissionResults {
  private static final long serialVersionUID = -8053797145112010089L;

  /**
   * Put the given key and value in the emission result set. Hatch the ERK.
   *
   * @param key The EmissionResultKey to put
   * @param value the value to put.
   */
  @Override
  public void put(final EmissionResultKey key, final Double value) {
    putHatched(key, value);
  }

  /**
   * Put the given key and value in the emission result set. Hatches the ERK in order to duplicate the value.
   *
   * Note that in this case the individual values will /not/ be summed in the get() method.
   *
   * @param key The EmissionResultKey to put
   * @param value the value to put.
   */
  public void putHatched(final EmissionResultKey key, final Double value) {
    super.put(key, value);

    for (final EmissionResultKey hatchedKey : key.hatch()) {
      super.put(hatchedKey, value);
    }
  }

  /**
   * Put the given key and value in the emission result set. Don't hatch the ERK.
   *
   * @param key The EmissionResultKey to put
   * @param value the value to put.
   */
  public void putUnhatched(final EmissionResultKey key, final Double value) {
    super.put(key, value);
  }
}
