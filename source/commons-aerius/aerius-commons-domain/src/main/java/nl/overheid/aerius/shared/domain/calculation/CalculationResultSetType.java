/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

/**
 * Type of calculation results set.
 * Maps to database enum calculation_result_set_type.
 */
public enum CalculationResultSetType {

  /**
   * Results are the total of all sectors results.
   */
  TOTAL,

  /**
   * Results are for a specific sector. This should be combined with the sector id.
   */
  SECTOR;

  /**
   * Returns the name as represented in the database.
   * @return name as represented in the database
   */
  public String toDatabaseString() {
    return name().toLowerCase();
  }
}
