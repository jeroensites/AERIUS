/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared;

import static nl.overheid.aerius.shared.RequestMappings.FILE_CODE;
import static nl.overheid.aerius.shared.RequestMappings.LINKED_FILE_TYPE;

/**
 * Request mapping to internal services.
 */
public class InternalRequestMappings {
  private static final String SLASH = "/";

  /** WUI SERVICES PARAMS **/
  private static final String WUI_SERVICES_API_VERSION = "v2";

  private static final String VALIDATION = "VALIDATION";
  private static final String IMPORTPARCEL = "IMPORTPARCEL";

  private static final String FILES_BARE = "files";
  private static final String EMPTY = "empty";

  /** SERVICES **/
  private static final String SERVICES_VERSION = SLASH + WUI_SERVICES_API_VERSION + SLASH;
  private static final String FILES = SERVICES_VERSION + FILES_BARE;
  private static final String PUBLIC = "public";

  public static final String FILE_ADD = FILES;
  public static final String EMPTY_FILE_ADD = FILES + SLASH + EMPTY;
  public static final String LINKED_FILE = FILES + SLASH + FILE_CODE + SLASH + LINKED_FILE_TYPE;
  public static final String FILE_RETRIEVE_VALIDATION = FILES + SLASH + FILE_CODE + SLASH + VALIDATION;
  public static final String FILE_RETRIEVE_IMPORTPARCEL = FILES + SLASH + FILE_CODE + SLASH + IMPORTPARCEL;
  public static final String FILE_RETRIEVE_RAW = FILES + SLASH + FILE_CODE;
  public static final String FILE_RETRIEVE_RAW_PUBLIC = FILES + SLASH + PUBLIC + SLASH + FILE_CODE;
  public static final String FILE_DELETE = FILES + SLASH + FILE_CODE;

  /**
   * FILE SERVICE Request query parameter names
   */
  public static final String FILE_PUBLIC_PARAMETER = "publiclyAccessible";
}
