/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import nl.overheid.aerius.shared.domain.scenario.SituationType;

/**
 * Class to link a situation to a calculation.
 */
public final class SituationCalculation {

  private String situationId;
  private int calculationId;
  private SituationType situationType;

  public final void setSituationId(final String situationId) {
    this.situationId = situationId;
  }

  public final void setCalculationId(final int calculationId) {
    this.calculationId = calculationId;
  }

  public final String getSituationId() {
    return situationId;
  }

  public final int getCalculationId() {
    return calculationId;
  }

  public void setSituationType(final SituationType situationType) {
    this.situationType = situationType;
  }

  public SituationType getSituationType() {
    return situationType;
  }
}
