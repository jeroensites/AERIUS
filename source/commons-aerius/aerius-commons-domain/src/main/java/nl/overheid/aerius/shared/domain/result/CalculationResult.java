/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.result;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.shared.domain.CalculationEngine;

/**
 * Data object calculation results are stored by the calculation tools and send to the server.
 */
public class CalculationResult implements Serializable {

  private static final long serialVersionUID = 2L;

  private Map<Integer, List<AeriusResultPoint>> results = new HashMap<>();
  private CalculationEngine engine;

  public CalculationResult(final CalculationEngine engine) {
    this.engine = engine;
  }

  public CalculationEngine getEngine() {
    return engine;
  }

  public Map<Integer, List<AeriusResultPoint>> getResults() {
    return results;
  }

  public void put(final Integer key, final List<AeriusResultPoint> sectorResults) {
    results.put(key, sectorResults);
  }
}
