/*
 * Copyright the State of the Netherlands
 * Crown copyright
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

/**
 * Interface for model specific building data objects.
 */
public interface EngineBuilding {

  /**
   * @return Length of a rectangular building or diameter of a circular building, in metres.
   */
  double getBuildingLength();

  /**
   * Length of a rectangular building or diameter of a circular building, in metres.
   *
   * @param length Length (m)/ Diameter (m)
   */
  void setBuildingLength(final double length);

  /**
   * @return Width of a rectangular building, in metres.
   */
  double getBuildingWidth();

  /**
   * Width of a rectangular building, in metres.
   *
   * @param width width (m)
   */
  void setBuildingWidth(final double width);

  /**
   * @return Angle the length of a rectangular building makes with north, measured clockwise in degrees.
   */
  double getBuildingOrientation();

  /**
   * Angle the length of a rectangular building makes with north, measured clockwise in degrees.
   *
   * @param angle Angle (Degrees)
   */
  void setBuildingOrientation(final double angle);

  /**
   * @return Height of the building, in metres.
   */
  double getBuildingHeight();

  /**
   * Height of the building, in metres.
   *
   * @param height height (m)
   */
  void setBuildingHeight(final double height);
}
