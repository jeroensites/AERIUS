/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.meteo.Meteo;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Base class for Calculation Engine input data.
 */
public class EngineInputData<T extends EngineSource, R extends AeriusPoint> implements Serializable {

  private static final long serialVersionUID = 1L;

  private final Theme theme;
  private int year;
  private Meteo meteo;
  private final Map<Integer, Collection<T>> emissionSources = new HashMap<>();
  private Collection<R> receptors = new ArrayList<>();
  private List<Substance> substances;
  private EnumSet<EmissionResultKey> emissionResultKeys;

  protected EngineInputData(final Theme theme) {
    this.theme = theme;
  }

  public Theme getTheme() {
    return theme;
  }

  public int getYear() {
    return year;
  }

  public void setYear(final int year) {
    this.year = year;
  }

  public Meteo getMeteo() {
    return meteo;
  }

  public void setMeteo(final Meteo meteo) {
    this.meteo = meteo;
  }

  public int getSourcesSize() {
    int size = 0;
    for (final Entry<Integer, Collection<T>> entry : emissionSources.entrySet()) {
      size += entry.getValue().size();
    }
    return size;
  }

  public Map<Integer, Collection<T>> getEmissionSources() {
    return emissionSources;
  }

  public void setEmissionSources(final Integer sourcesKey, final Collection<T> sectorEmissionSources) {
    this.emissionSources.put(sourcesKey, sectorEmissionSources);
  }

  @NotNull
  @Size(min = 1)
  @Valid
  public Collection<R> getReceptors() {
    return receptors;
  }

  public void setReceptors(final Collection<R> receptors) {
    this.receptors = receptors;
  }

  public List<Substance> getSubstances() {
    return substances;
  }

  public void setSubstances(final List<Substance> substances) {
    this.substances = substances;
  }

  public EnumSet<EmissionResultKey> getEmissionResultKeys() {
    return emissionResultKeys;
  }

  public void setEmissionResultKeys(final EnumSet<EmissionResultKey> emissionResultKeys) {
    this.emissionResultKeys = emissionResultKeys;
  }

  @Override
  public String toString() {
    return "#receptors=" + receptors + ",#emissionSources=" + emissionSources.size()
    + ", year=" + year + ", substances=" + substances + ", emissionResultKeys=" + emissionResultKeys + ", meteo=" + meteo;
  }

}
