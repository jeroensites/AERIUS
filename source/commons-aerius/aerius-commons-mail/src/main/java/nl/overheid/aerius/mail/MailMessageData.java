/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import nl.overheid.aerius.enums.MessagesEnum;

/**
 * Data class containing everything needed to send an email by the message worker.
 */
public class MailMessageData implements Serializable {

  private static final long serialVersionUID = -9107196937321893399L;

  private final Locale locale;
  private final MailTo mailTo;
  private final HashMap<ReplacementToken, MessagesEnum> dbReplacements = new HashMap<>();
  private final HashMap<ReplacementToken, String> replacements = new HashMap<>();
  private final ArrayList<MailAttachment> attachments = new ArrayList<>();

  public MailMessageData(final MessagesEnum subjectTemplate, final MessagesEnum bodyTemplate, final Locale locale, final MailTo mailTo) {
    dbReplacements.put(ReplacementToken.MAIL_SUBJECT, subjectTemplate);
    dbReplacements.put(ReplacementToken.MAIL_CONTENT, bodyTemplate);
    this.locale = locale;
    this.mailTo = mailTo;
  }

  public Locale getLocale() {
    return locale;
  }

  public MailTo getMailTo() {
    return mailTo;
  }

  public void setReplacement(final ReplacementToken token, final String replaceWith) {
    if (replaceWith == null) {
      replacements.remove(token);
    } else {
      replacements.put(token, replaceWith);
    }
  }

  public HashMap<ReplacementToken, String> getReplacements() {
    return replacements;
  }

  public void setDbReplacement(final ReplacementToken token, final MessagesEnum replaceWith) {
    dbReplacements.put(token, replaceWith);
  }

  public HashMap<ReplacementToken, MessagesEnum> getDbReplacements() {
    return dbReplacements;
  }

  public void addAttachment(final MailAttachment attachment) {
    attachments.add(attachment);
  }

  public ArrayList<MailAttachment> getAttachments() {
    return attachments;
  }

  @Override
  public String toString() {
    return "MailMessageData [dbReplacements=" + dbReplacements + ", locale=" + locale + ", mailTo=" + mailTo
        + ", replacements=" + replacements.size() + ", attachments=" + attachments.size() + "]";
  }

}
