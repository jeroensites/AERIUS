/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mail;

/**
 *
 */
public enum ReplacementToken {

  /**
   * The calculation creation date (date only, no time).
   */
  CALC_CREATION_DATE,

  /**
   * The calculation creation time (time only, no date).
   */
  CALC_CREATION_TIME,

  /**
   * The full download link.
   */
  DOWNLOAD_LINK,

  /**
   * The error code.
   */
  ERROR_CODE,

  /**
   * The error message.
   */
  ERROR_MESSAGE,

  /**
   * The solution for the error.
   */
  ERROR_SOLUTION,

  /**
   * The subject of the mail.
   */
  MAIL_SUBJECT,

  MAIL_SUBJECT_TEMPLATE,

  /**
   * The content of the mail.
   */
  MAIL_CONTENT,

  MAIL_CONTENT_TEMPLATE,

  /**
   * The signature of the mail.
   */
  MAIL_SIGNATURE,

  /**
   * The content of the substantiation text.
   */
  MAIL_SUBSTANTIATION,

  /**
   * The placeholder for the melding remarks.
   */
  MELDING_SUBSTANTIATION,

  /**
   * The placeholder for the melding attachments send result.
   */
  MELDING_ATTACHMENTS_RESULT_LIST,

  /**
   * Password reset url.
   */
  PASSWORD_RESET_URL,

  /**
   * New plain-text password.
   */
  PASSWORD_RESET_PLAIN_TEXT_NEW,

  /**
   * New connect apikey.
   */
  CONNECT_APIKEY,

  /**
   * The name of the project.
   */
  PROJECT_NAME,

  /**
   * The reference of the request (melding or permit).
   */
  AERIUS_REFERENCE,
  /**
   * The creation date of the (melding or permit).
   */
  MELDING_CREATE_DATE,
  /**
   * The date time that the melding is removed.
   */
  MELDING_REMOVE_DATE,
  /**
   * The name of the person who removed the melding.
   */
  MELDING_REMOVED_BY,

  /**
   * The name of the authority.
   */
  AUTHORITY,

  /**
   * The reason why a melding was not registered.
   */
  REASON_NOT_REGISTERED_MELDING,

  /**
   * The placeholder that will be replaced if we have a technical issue for melding.
   */
  TECHNICAL_REASON_NOT_REGISTERED_MELDING,

  /**
   * A mail was successfully send (used in the overview mail for meldingen to authorities).
   */
  SEND_OK,

  /**
   * Failure to send a mail (used in the overview mail for meldingen to authorities).
   */
  SEND_FAIL,

  /**
   * Current document mail number (used for document mails to authority for meldingen).
   */
  DOCUMENT_MAIL_NR,
  /**
   * Max document mail number (used for document mails to authority for meldingen).
   */
  MAX_DOCUMENT_MAIL_NR,

  /**
   * Current timestamp (includes date and time).
   */
  CURRENT_TIMESTAMP,

  /**
   * Job description from the user for the job.
   */
  JOB,
  /**
   * Manual url.
   */
  MANUAL_URL,
  /**
   * Quick start url
   */
  QUICK_START_URL,
  /**
   * Bij12 helpdesk url
   */
  BIJ12_HELPDESK_URL;
}
