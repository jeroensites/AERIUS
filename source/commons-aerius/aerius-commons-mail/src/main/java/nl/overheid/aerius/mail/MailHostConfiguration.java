/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mail;

import org.apache.commons.mail.HtmlEmail;

import nl.overheid.aerius.util.EnvUtils;

/**
 * HtmlEmail configuration from environment settings.
 */
public final class MailHostConfiguration {

  private static final String ENV_MAIL_HOST = "mail.host";
  private static final String DEFAULT_MAIL_HOST = "localhost";

  private static final String ENV_MAIL_USERNAME = "mail.username";
  private static final String ENV_MAIL_PASSWORD = "mail.password";

  private static final String ENV_MAIL_SMTP_PORT = "mail.smtp.port";
  private static final String ENV_MAIL_SMTP_SSL_PORT = "mail.smtp.ssl.port";

  private static final String ENV_MAIL_SSL_ENABLE = "mail.ssl.enable";
  private static final String ENV_MAIL_STARTTLS_ENABLE = "mail.starttls.enable";

  private MailHostConfiguration() {
    // Don't allow to instantiate.
  }

  /**
   * Configures a `HtmlEmail` with the settings from the environment.
   *
   * @return Configured email
   */
  public static HtmlEmail constructConfiguredEmail() {

    // Host
    final HtmlEmail email = new HtmlEmail();
    email.setHostName(EnvUtils.getEnvProperty(ENV_MAIL_HOST, DEFAULT_MAIL_HOST));

    // Authentication
    final String username = EnvUtils.getEnvProperty(ENV_MAIL_USERNAME);
    final String password = EnvUtils.getEnvProperty(ENV_MAIL_PASSWORD);
    if (username != null && password != null) {
      email.setAuthentication(username, password);
    }

    // Ports
    final String port = EnvUtils.getEnvProperty(ENV_MAIL_SMTP_PORT);
    if (port != null) {
      email.setSmtpPort(Integer.parseInt(port));
    }
    final String sslPort = EnvUtils.getEnvProperty(ENV_MAIL_SMTP_SSL_PORT);
    if (sslPort != null) {
      email.setSslSmtpPort(sslPort);
    }

    // SSL
    final String sslEnable = EnvUtils.getEnvProperty(ENV_MAIL_SSL_ENABLE);
    if ("true".equalsIgnoreCase(sslEnable)) {
      email.setSSLOnConnect(true);
      email.setSSLCheckServerIdentity(true);
    }

    // StartTLS
    final String startTlsEnable = EnvUtils.getEnvProperty(ENV_MAIL_STARTTLS_ENABLE);
    if ("true".equalsIgnoreCase(startTlsEnable)) {
      email.setStartTLSEnabled(true);
      email.setStartTLSRequired(true);
    }

    return email;
  }

}
