/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mail;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.text.StrSubstitutor;

/**
 *
 */
class MailReplaceTokenMap extends HashMap<ReplacementToken, String> {

  private static final long serialVersionUID = 2286580298232711319L;

  private static final String TOKEN_CALC_CREATION_DATE_FORMAT = "EEEE dd MMMM yyyy";
  private static final String TOKEN_CALC_CREATION_TIME_FORMAT = "HH:mm";
  private static final String TOKEN_CALC_CREATION_DATE_TIME_FORMAT = TOKEN_CALC_CREATION_DATE_FORMAT + " " + TOKEN_CALC_CREATION_TIME_FORMAT;
  private static final char TOKEN_PREFIX = '[';
  private static final char TOKEN_SUFFIX = ']';

  /**
   * @param template The template to use for getting the mail text.
   * @return The template where every occurrence of the replacement tokens in this map are replaced.
   * If a token was in the template, but not in this map, it will remain as it is in the text.
   */
  public String getMailText(final String template) {
    if (template == null) {
      throw new IllegalArgumentException("Template for mail not allowed to be null.");
    }
    final Map<String, String> replacements = new HashMap<String, String>();
    for (final Map.Entry<ReplacementToken, String> entry : this.entrySet()) {
      replacements.put(entry.getKey().name(), entry.getValue());
    }
    //StrSubstitutor works in a recursive way. No need to keep on replacing.
    final StrSubstitutor substitutor = new StrSubstitutor(replacements);
    substitutor.setVariablePrefix(TOKEN_PREFIX);
    substitutor.setVariableSuffix(TOKEN_SUFFIX);
    return substitutor.replace(template);
  }

  /**
   * @param date The date to format according to default format for mail text.
   * @param locale The locale to use to format it.
   * @return The formatted date. (only date information, no time).
   */
  public static String getDefaultDateFormatted(final Date date, final Locale locale) {
    return new SimpleDateFormat(TOKEN_CALC_CREATION_DATE_FORMAT, locale).format(date);
  }

  /**
   * @param date The time to format according to default format for mail text.
   * @param locale The locale to use to format it.
   * @return The formatted time. (only time information, no date).
   */
  public static String getDefaultTimeFormatted(final Date date, final Locale locale) {
    return new SimpleDateFormat(TOKEN_CALC_CREATION_TIME_FORMAT, locale).format(date);
  }

  /**
   * @param date The date time to format according to default format for mail text.
   * @param locale The locale to use to format it.
   * @return The formatted date and time.
   */
  public static String getDefaultDateTimeFormatted(final Date date, final Locale locale) {
    return new SimpleDateFormat(TOKEN_CALC_CREATION_DATE_TIME_FORMAT, locale).format(date);
  }

}
