/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import nl.overheid.aerius.worker.WorkerConfiguration;

public class PdfExportWorkerConfiguration extends WorkerConfiguration {

  private static final String PDF_EXPORT_WORKER_PREFIX = "database.pdf";
  private static final String WEBSERVER_URL = "webserver.url";
  private static final String FILE_SERVICE_URL = "fileservice.url";

  // Development property to disable cleanup
  private static final String PREVENT_CLEANUP = "fileservice.prevent.cleanup";

  public PdfExportWorkerConfiguration(final Properties properties) {
    super(properties, PDF_EXPORT_WORKER_PREFIX);
  }

  @Override
  protected List<String> getValidationErrors() {
    final List<String> reasons = new ArrayList<>();
    if (getProcesses() > 0) {
      validateRequiredProperty(WEBSERVER_URL, reasons);
      validateRequiredProperty(FILE_SERVICE_URL, reasons);
    }
    return reasons;
  }

  public String getWebserverUrl() {
    return getProperty(WEBSERVER_URL);
  }

  public String getFileServiceUrl() {
    return getProperty(FILE_SERVICE_URL);
  }

  public boolean preventCleanup() {
    return getPropertyBooleanSafe(PREVENT_CLEANUP);
  }
}
