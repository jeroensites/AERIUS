/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.export.util;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.pdf.TimeoutException;
import nl.aerius.print.ExportJob;
import nl.overheid.aerius.export.PdfExportWorkerConfiguration;
import nl.overheid.aerius.shared.RequestMappings;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.worker.JobIdentifier;

public class PdfContentGenerator {

  private static final Logger LOG = LoggerFactory.getLogger(PdfContentGenerator.class);

  private static final String FILENAME_PREFIX = "AERIUS_export";
  private static final int RETRY_COUNT = 21;
  private final String temporaryFileDirectory;
  private final PdfExportWorkerConfiguration configuration;
  private final JobIdentifier jobIdentifier;
  private final String scenarioFileIdentifier;

  public PdfContentGenerator(final PdfExportWorkerConfiguration configuration, final JobIdentifier jobIdentifier, final String scenarioFileIdentifier)
      throws IOException {
    this.configuration = configuration;
    this.jobIdentifier = jobIdentifier;
    this.scenarioFileIdentifier = scenarioFileIdentifier;
    temporaryFileDirectory = Files.createTempDirectory("PDFGenerator").toString() + "/";
  }

  private static Map<String, Object> getPrintParams() {
    final Map<String, Object> printParams = new HashMap<>();
    printParams.put("preferCSSPageSize", true);
    printParams.put("printBackground", true);
    printParams.put("margins", "default");

    return printParams;
  }

  private static String getFileNameWithoutExtension(final String scenarioReference) {
    return FileUtil.getFileName(FILENAME_PREFIX, "", scenarioReference, null);
  }

  public File generate(final String reference, final String proposedSituationId) throws AeriusException {
    try {
    final String filePath = ExportJob
        .create(getPreviewUrl(reference, proposedSituationId))
        .handle(getFileNameWithoutExtension(reference))
        .completeOrFailViaIndicator()
        .retry(RETRY_COUNT)
        .destination(temporaryFileDirectory)
        .print(getPrintParams())
        .save()
        .outputDocument();
      return new File(filePath);
    } catch (final TimeoutException e) {
      throw new AeriusException(AeriusExceptionReason.PAA_EXPORT_TIMEOUT);
    }
  }

  private String getPreviewUrl(final String reference, final String proposedSituationId) throws AeriusException {
    try {
      final URIBuilder uriBuilder = new URIBuilder(configuration.getWebserverUrl());
      uriBuilder.setPath(RequestMappings.PRINT_WNB);
      uriBuilder.addParameter("jobId", jobIdentifier.getCorrelationId());
      uriBuilder.addParameter("reference", reference);
      uriBuilder.addParameter("proposedSituationId", proposedSituationId);
      uriBuilder.addParameter("scenarioFileCodes", scenarioFileIdentifier);
      uriBuilder.addParameter("internal", "true");

      final String url = uriBuilder.build().toString();
      LOG.info("Generating pdf with url {}", url);

      return url;
    } catch (final URISyntaxException ignored) {
      LOG.error("Unable to build a URI from preview url '{}'", configuration.getWebserverUrl());
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

}
