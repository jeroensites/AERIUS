/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import nl.overheid.aerius.export.util.PdfContentGenerator;
import nl.overheid.aerius.export.util.PdfContentProcessor;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.JobIdentifier;

public class PdfGenerator {

  private final PdfContentGenerator pdfContentGenerator;
  private final ReleaseType releaseType;

  public PdfGenerator(final PdfExportWorkerConfiguration configuration, final JobIdentifier jobIdentifier, final String scenarioFileIdentifier,
      final ReleaseType releaseType) throws IOException {
    this.releaseType = releaseType;
    pdfContentGenerator = new PdfContentGenerator(configuration, jobIdentifier, scenarioFileIdentifier);
  }

  public byte[] generateFileContents(final String reference, final String proposedSituationId, final List<String> gmls)
      throws AeriusException, IOException {
    try (final InputStream is = new FileInputStream(generate(reference, proposedSituationId, gmls))) {
      return is.readAllBytes();
    }
  }

  private File generate(final String reference, final String proposeSituationId, final List<String> gmls) throws AeriusException, IOException {
    final File content = pdfContentGenerator.generate(reference, proposeSituationId);

    final PdfContentProcessor processorUtil = new PdfContentProcessor(reference, releaseType, gmls);
    processorUtil.process(content);

    return content;
  }
}
