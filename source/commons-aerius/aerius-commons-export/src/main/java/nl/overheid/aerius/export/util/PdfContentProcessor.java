/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.export.util;

import static nl.overheid.aerius.PAAConstants.CALC_PDF_METADATA_GML_KEY_FORMAT;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.VerticalAlignment;
import com.itextpdf.svg.converter.SvgConverter;

import nl.overheid.aerius.PAAConstants;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.util.HashUtil;
import nl.overheid.aerius.util.LocaleUtils;

public class PdfContentProcessor {
  private static final String FONT = "NotoSansTC-Regular.otf";
  private static final String LOGO = "logo.svg";
  private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd MMMM yyyy", LocaleUtils.getDefaultLocale());
  private static final float LOGO_HEIGHT = 75F;
  private static final float MARGIN_HOR = 42F;
  private static final float MARGIN_VER = 24F;
  private static final float FONT_SIZE_TITLE = 14F;
  private static final float FONT_SIZE_FOOTER = 8F;

  private final PdfFont pdfFont;
  private final String logoAsString;
  private final Paragraph documentName;
  private final Paragraph referenceAndDate;
  private final List<String> gmls;
  private final ReleaseType releaseType;

  public PdfContentProcessor(final String reference, final ReleaseType releaseType, final List<String> gmls) throws IOException {
    this.gmls = gmls;
    this.releaseType = releaseType;

    try (final InputStream is = PdfContentProcessor.class.getResourceAsStream(FONT)) {
      final byte[] fontBytes = is.readAllBytes();
      pdfFont = PdfFontFactory.createFont(fontBytes, PdfEncodings.IDENTITY_H, PdfFontFactory.EmbeddingStrategy.PREFER_EMBEDDED);
    }

    try (final InputStream is = PdfContentProcessor.class.getResourceAsStream(LOGO)) {
      logoAsString = new String(is.readAllBytes(), StandardCharsets.UTF_8);
    }

    documentName = new Paragraph(Messages.projectTitle(releaseType))
        .setFont(pdfFont)
        .setFontSize(FONT_SIZE_TITLE);

    final LocalDateTime now = LocalDateTime.now();
    referenceAndDate = new Paragraph(String.format("%s (%s)", reference, DATE_TIME_FORMATTER.format(now)))
        .setFont(pdfFont)
        .setFontSize(FONT_SIZE_FOOTER);
  }

  public void process(final File file) throws IOException {
    try (final PdfDocument pdfDocument = new PdfDocument(new PdfReader(new FileInputStream(file)), new PdfWriter(new FileOutputStream(file)))) {

      final Rectangle pageSize = pdfDocument.getDefaultPageSize();
      final int numberOfPages = pdfDocument.getNumberOfPages();

      final Document document = new Document(pdfDocument);

      // Draw logo on the coverpage
      SvgConverter.drawOnDocument(logoAsString, pdfDocument, 1, MARGIN_HOR, pageSize.getHeight() - MARGIN_VER - LOGO_HEIGHT);

      // Make and draw the title on the coverpage
      final DeviceRgb titleColor = new DeviceRgb(27, 61, 110);
      final Paragraph documentTitle = new Paragraph(Messages.projectTitle(releaseType))
          .setFont(pdfFont)
          .setFontColor(titleColor)
          .setFontSize(FONT_SIZE_TITLE);

      document.showTextAligned(documentTitle, pageSize.getWidth() / 2, pageSize.getHeight() - MARGIN_VER - FONT_SIZE_TITLE, 1,
          TextAlignment.LEFT, VerticalAlignment.TOP, 0);

      // Add header and footer to all other pages.
      for (int i = 2; i <= numberOfPages; i++) {
        final Paragraph pageNumber = new Paragraph(String.format("%d/%d", i, numberOfPages))
            .setFont(pdfFont)
            .setFontSize(FONT_SIZE_FOOTER);

        SvgConverter.drawOnDocument(logoAsString, pdfDocument, i, MARGIN_HOR, pageSize.getHeight() - MARGIN_VER - LOGO_HEIGHT);
        document.showTextAligned(documentName, pageSize.getWidth() - MARGIN_HOR, pageSize.getHeight() - MARGIN_VER - FONT_SIZE_TITLE, i,
            TextAlignment.RIGHT, VerticalAlignment.TOP, 0);
        document.showTextAligned(pageNumber, pageSize.getWidth() - MARGIN_HOR, MARGIN_VER, i, TextAlignment.RIGHT, VerticalAlignment.BOTTOM, 0);
        document.showTextAligned(referenceAndDate, MARGIN_HOR, MARGIN_VER, i, TextAlignment.LEFT, VerticalAlignment.BOTTOM, 0);
      }

      // Add gml strings to document info
      for (int i = 0; i < gmls.size(); i++) {
        final String key = String.format(CALC_PDF_METADATA_GML_KEY_FORMAT, i);
        document.getPdfDocument().getDocumentInfo().setMoreInfo(key, gmls.get(i));
      }

      // Set the hash in metadata
      final String metadataHash = HashUtil.generateSaltedHash(PAAConstants.CALC_PDF_METADATA_HASH_SALT, gmls);
      if (StringUtils.isEmpty(metadataHash)) {
        throw new IllegalArgumentException("Could not generate metadata hash");
      }
      document.getPdfDocument().getDocumentInfo().setMoreInfo(PAAConstants.CALC_PDF_METADATA_HASH_KEY, metadataHash);
    }
  }
}
