Feature: AERIUS open data, test the depositions sources in different output formats

  Scenario: Validate the open data source depositions:depositions in application/json format
    Given i make a GetFeature request to get 'depositions:depositions' in 'application/json' with filter 'receptor_id=291553'
    Then the GetFeature response status code is 200
    And the GetFeature JSON response contains the key 'type' with type 'string'
    And the GetFeature JSON response contains the key 'totalFeatures' with type 'number'
    And the GetFeature JSON response contains the key 'features' with type 'array'
    And the GetFeature JSON response contains the key 'crs' with type 'object'
    And the GetFeature JSON response contains the key 'bbox' with type 'array'
    And the GetFeature response properties are equal to 'json/depositions-depositions.json'
    And the GetFeature response properties contains the key 'receptor_id' with type 'number'

  Scenario: Validate the open data source depositions:depositions in GML2 format
    Given i make a GetFeature request to get 'depositions:depositions' in 'GML2' with filter 'receptor_id=291553'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMember>'
    And the GetFeature response contains '<depositions:year>'
    And the GetFeature response contains '<depositions:zoom_level>'
    And the GetFeature response contains '<depositions:total_deposition>'
    And the GetFeature response contains '<depositions:geometry>'

  Scenario: Validate the open data source depositions:depositions in gml3 format
    Given i make a GetFeature request to get 'depositions:depositions' in 'gml3' with filter 'receptor_id=291553'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMembers>'
    And the GetFeature response contains '<depositions:year>'
    And the GetFeature response contains '<depositions:zoom_level>'
    And the GetFeature response contains '<depositions:total_deposition>'
    And the GetFeature response contains '<depositions:geometry>'

  Scenario: Validate the open data source depositions:depositions in gml32 format
    Given i make a GetFeature request to get 'depositions:depositions' in 'gml32' with filter 'receptor_id=291553'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<wfs:member>'
    And the GetFeature response contains '<depositions:year>'
    And the GetFeature response contains '<depositions:zoom_level>'
    And the GetFeature response contains '<depositions:total_deposition>'
    And the GetFeature response contains '<depositions:geometry>'

  Scenario: Validate the open data source depositions:depositions in KML format
    Given i make a GetFeature request to get 'depositions:depositions' in 'KML' with filter 'receptor_id=291553'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<SimpleData name="year">'
    And the GetFeature response contains '<SimpleData name="receptor_id">'
    And the GetFeature response contains '<SimpleData name="zoom_level">'
    And the GetFeature response contains '<SimpleData name="total_deposition">'
    And the GetFeature response contains '<Polygon>'

  Scenario: Validate the open data source depositions:depositions in CSV format
    Given i make a GetFeature request to get 'depositions:depositions' in 'CSV' with filter 'receptor_id=291553'
    Then the GetFeature response status code is 200
    And the GetFeature response contains 'FID,year,receptor_id,zoom_level,total_deposition,geometry'
