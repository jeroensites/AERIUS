Feature: AERIUS open data, test the development_spaces sources in different output formats

  Scenario: Validate the open data source development_spaces:exceeding_hexagons in application/json format
    Given i make a GetFeature request to get 'development_spaces:exceeding_hexagons' in 'application/json'
    Then the GetFeature response status code is 200
    And the GetFeature JSON response contains the key 'type' with type 'string'
    And the GetFeature JSON response contains the key 'totalFeatures' with type 'number'
    And the GetFeature JSON response contains the key 'features' with type 'array'
    And the GetFeature JSON response contains the key 'crs' with type 'object'
    And the GetFeature JSON response contains the key 'bbox' with type 'array'
    And the GetFeature response properties are equal to 'json/development_spaces-exceeding_hexagons.json'
    And the GetFeature response properties contains the key 'receptor_id' with type 'number'

  Scenario: Validate the open data source development_spaces:exceeding_hexagons in GML2 format
    Given i make a GetFeature request to get 'development_spaces:exceeding_hexagons' in 'GML2'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMember>'
    And the GetFeature response contains '<development_spaces:measure_reference>'
    And the GetFeature response contains '<development_spaces:measure_name>'
    And the GetFeature response contains '<development_spaces:receptor_id>'
    And the GetFeature response contains '<development_spaces:zoom_level>'
    And the GetFeature response contains '<development_spaces:initial_available_space>'
    And the GetFeature response contains '<development_spaces:assigned_space>'
    And the GetFeature response contains '<development_spaces:available_space>'
    And the GetFeature response contains '<development_spaces:geometry>'
    
  Scenario: Validate the open data source development_spaces:exceeding_hexagons in gml3 format    
    Given i make a GetFeature request to get 'development_spaces:exceeding_hexagons' in 'gml3'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<gml:featureMembers>'
    And the GetFeature response contains '<development_spaces:measure_reference>'
    And the GetFeature response contains '<development_spaces:measure_name>'
    And the GetFeature response contains '<development_spaces:receptor_id>'
    And the GetFeature response contains '<development_spaces:zoom_level>'
    And the GetFeature response contains '<development_spaces:initial_available_space>'
    And the GetFeature response contains '<development_spaces:assigned_space>'
    And the GetFeature response contains '<development_spaces:available_space>'
    And the GetFeature response contains '<development_spaces:geometry>'

  Scenario: Validate the open data source development_spaces:exceeding_hexagons in gml32 format
    Given i make a GetFeature request to get 'development_spaces:exceeding_hexagons' in 'gml32'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<wfs:member>'
    And the GetFeature response contains '<development_spaces:measure_reference>'
    And the GetFeature response contains '<development_spaces:measure_name>'
    And the GetFeature response contains '<development_spaces:receptor_id>'
    And the GetFeature response contains '<development_spaces:zoom_level>'
    And the GetFeature response contains '<development_spaces:initial_available_space>'
    And the GetFeature response contains '<development_spaces:assigned_space>'
    And the GetFeature response contains '<development_spaces:available_space>'
    And the GetFeature response contains '<development_spaces:geometry>'

  Scenario: Validate the open data source development_spaces:exceeding_hexagons in KML format
    Given i make a GetFeature request to get 'development_spaces:exceeding_hexagons' in 'KML'
    Then the GetFeature response status code is 200
    And the GetFeature response contains '<name>exceeding_hexagons</name>'
    And the GetFeature response contains '<SimpleData name="receptor_id">'
    And the GetFeature response contains '<SchemaData schemaUrl="#exceeding_hexagons_1">'

  Scenario: Validate the open data source development_spaces:exceeding_hexagons in CSV format
    Given i make a GetFeature request to get 'development_spaces:exceeding_hexagons' in 'CSV'
    Then the GetFeature response status code is 200
    And the GetFeature response contains 'FID,measure_reference,measure_name,receptor_id,zoom_level,initial_available_space,assigned_space,available_space,geometry'
