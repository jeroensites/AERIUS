/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

Then(`the GetFeature response status code is {int}`, (statusCode) => {
  cy.get('@getFeatureRequest').should((response) => {
    expect(response.status).to.eq(statusCode)
  })
})

Then(`the GetFeature JSON response contains the key {string}`, (key) => {
  cy.get('@getFeatureRequest').should((response) => {
    expect(response.body).to.have.property(key)
  })
})

Then(`the GetFeature JSON response contains the key {string} with type {string}`, (key, type) => {
  cy.get('@getFeatureRequest').should((response) => {
    expect(response.body[key]).to.be.a(type)

  })
})

Then(`the GetFeature response properties contains the key {string} with type {string}`, (key, type) => {
  cy.get('@getFeatureRequest').should((response) => {
    expect(response.body.features[0].properties[key]).to.be.a(type)
  })
})

Then(`the GetFeature response properties are equal to {string}`, (fixture) => {
  cy.fixture(fixture).as('jsonFixture')
  cy.get('@jsonFixture').then((jsonFixture) => {
    const fixture = jsonFixture
    cy.get('@getFeatureRequest').then((response) => {
      expect(response.body.features[0].properties).to.contain(fixture)
    })
  })
})

Then(`the GetFeature response contains {string}`, (string) => {
  cy.get('@getFeatureRequest').then((response) => {
    expect(response.body).to.contain(string)
  })
})
