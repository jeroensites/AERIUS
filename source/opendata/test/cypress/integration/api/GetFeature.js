/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

class GetFeature {
  constructor() {
    this.versionWMS = '1.1.0'
  }

  /**
   * Make a GetFeature request to the given endpoint
   * @param {String} endpoint - The endpoint for the request
   * @param {String} typeName - The type of layer to request
   * @param {String} outputFormat - The output format for the response of the request
   * @param {Number} maxFeatures - (optional) The type of features the response should have
   * @alias getFeatureRequest
   * @example
   * getFeature.getFeatureRequest('base_geometries/ows, base_geometries:hexagons, application/json, 10')
   */
  getFeatureRequest(endpoint, typeName, outputFormat, maxFeatures = 1) {
    cy.request({
      method: 'GET',
      url: endpoint,
      qs: {
        service: 'WFS',
        version: this.versionWMS,
        request: 'GetFeature',
        typeName: typeName,
        maxFeatures: maxFeatures,
        outputFormat: outputFormat,
      },
    }).as('getFeatureRequest')
  }

  /**
   * Make a GetFeature request to the given endpoint with the given filter
   * @param {String} endpoint - The endpoint for the request
   * @param {String} typeName - The type of layer to request
   * @param {String} outputFormat - The output format for the response of the request
   * @param {String} filter - The cql filter for the request
   * @param {Number} maxFeatures - (optional) The type of features the response should have
   * @alias getFeatureRequest
   * @example
   * getFeature.getFeatureRequestFilter('base_geometries/ows, base_geometries:hexagons, application/json, natura2000_area_id=65')
   */
  getFeatureRequestFilter(endpoint, typeName, outputFormat, filter, maxFeatures = 1) {
    cy.request({
      method: 'GET',
      url: endpoint,
      qs: {
        service: 'WFS',
        version: this.versionWMS,
        request: 'GetFeature',
        typeName: typeName,
        maxFeatures: maxFeatures,
        outputFormat: outputFormat,
        cql_filter: filter,
      },
    }).as('getFeatureRequest')
  }
}

export default GetFeature
