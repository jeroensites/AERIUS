# AERIUS - Open Data Test Automation

## Table Of Contents

- [Installation](#installation)
- [Running Tests](#running-tests)


## Installation

Install NodeJS

- LTS version
- Tested with version 12.16.3

```
https://nodejs.org/en/
```

Go to the location of the `package.json` and:

```
npm install
```


## Running Tests

- Set the environment variable CYPRESS_BASE_URL to the base url of the Geoserver application.

Open the Cypress GUI to start tests:

```
npx cypress open
```

Run all tests:

```
npm run test:all
```
