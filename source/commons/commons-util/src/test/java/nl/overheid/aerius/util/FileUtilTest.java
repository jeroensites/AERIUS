/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link FileUtil}.
 */
class FileUtilTest {

  @Test
  void testGetExtension() {
    final File fileWithoutExtension = new File("NO");
    assertNull(FileUtil.getExtension(fileWithoutExtension), "File without extension should return null");
    final File fileWithExtension = new File("YES.tst");
    assertEquals("tst", FileUtil.getExtension(fileWithExtension), "File with extension");
    final File fileWithDoubleExtension = new File("YES.another.tst");
    assertEquals("tst", FileUtil.getExtension(fileWithDoubleExtension), "File with double dots");
  }

  @Test
  void testGetFiles() throws FileNotFoundException {
    final String file = FileUtilTest.class.getResource("").getFile();
    assertFalse(FileUtil.getFilesWithExtension(new File(file), null).isEmpty(), "Check if find files in directory with no filter");
    assertFalse(FileUtil.getFilesWithExtension(new File(file), new FilenameFilter() {
      @Override
      public boolean accept(final File dir, final String name) {
        return name.endsWith("class");
      }
    }).isEmpty(), "Check if find files in directory with");
    assertFalse(FileUtil.getFilesWithExtension(new File(file, FileUtilTest.class.getSimpleName() + ".class"), new FilenameFilter() {
      @Override
      public boolean accept(final File dir, final String name) {
        return name.endsWith("class");
      }
    }).isEmpty(), "Check if find this file");
  }
}
