/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link LocaleUtils}.
 */
class LocaleUtilsTest {

  private static final Locale DEFAULT_LOCALE = new Locale("nl");

  @Test
  void testGetLocale() {
    assertEquals(DEFAULT_LOCALE, LocaleUtils.getLocale(null), "getLocale with null argument");
    assertEquals(DEFAULT_LOCALE, LocaleUtils.getLocale(""), "getLocale with empty string argument");
    assertEquals(DEFAULT_LOCALE, LocaleUtils.getLocale("nl"), "getLocale with default string argument");
    assertEquals(Locale.ENGLISH, LocaleUtils.getLocale("en"), "getLocale with 'en' argument");
    // TODO: For some reason these return nl atm? seems wrong!
    assertEquals(DEFAULT_LOCALE, LocaleUtils.getLocale("en-US"), "getLocale with 'en-US' string argument");
    assertEquals(DEFAULT_LOCALE, LocaleUtils.getLocale("en_US"), "getLocale with 'en_US' string argument");
    assertEquals(DEFAULT_LOCALE, LocaleUtils.getLocale("ok"), "getLocale with 'ok' string argument");
  }

  @Test
  void testGetSupportedLocaleOrDefault() {
    // TODO: what's the difference with normal getLocale?
    assertEquals(DEFAULT_LOCALE, LocaleUtils.getSupportedLocaleOrDefault(null), "getSupportedLocaleOrDefault with null argument");
    assertEquals(DEFAULT_LOCALE, LocaleUtils.getSupportedLocaleOrDefault(""), "getSupportedLocaleOrDefault with empty string argument");
    assertEquals(DEFAULT_LOCALE, LocaleUtils.getSupportedLocaleOrDefault("nl"), "getSupportedLocaleOrDefault with default string argument");
    assertEquals(Locale.ENGLISH, LocaleUtils.getSupportedLocaleOrDefault("en"), "getSupportedLocaleOrDefault with 'en' argument");
    // TODO: For some reason these return nl atm? seems wrong!
    assertEquals(DEFAULT_LOCALE, LocaleUtils.getSupportedLocaleOrDefault("en-US"), "getSupportedLocaleOrDefault with 'en-US' string argument");
    assertEquals(DEFAULT_LOCALE, LocaleUtils.getSupportedLocaleOrDefault("en_US"), "getSupportedLocaleOrDefault with 'en_US' string argument");
    assertEquals(DEFAULT_LOCALE, LocaleUtils.getSupportedLocaleOrDefault("ok"), "getSupportedLocaleOrDefault with 'ok' string argument");
  }

  @Test
  void testGetLocales() {
    assertEquals(List.of(DEFAULT_LOCALE, Locale.ENGLISH), LocaleUtils.getLocales(), "List of supported locales");
  }

  @Test
  void testIsValidLocale() {
    assertTrue(LocaleUtils.isValidLocale(DEFAULT_LOCALE), "We do know our default");
    assertTrue(LocaleUtils.isValidLocale(Locale.ENGLISH), "We do know english");
    assertFalse(LocaleUtils.isValidLocale(new Locale("fr")), "We don't know french");
    assertFalse(LocaleUtils.isValidLocale(null), "We don't know nothing");
  }

  @Test
  void testGetDefaultLocale() {
    assertEquals(DEFAULT_LOCALE, LocaleUtils.getDefaultLocale(), "getDefaultLocale should return our default");
  }

}
