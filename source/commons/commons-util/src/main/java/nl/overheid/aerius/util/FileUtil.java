/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Util class to store data to file.
 */
public final class FileUtil {

  private static final Logger LOG = LoggerFactory.getLogger(FileUtil.class);

  public interface FileMatcher<T> {
    T get(File dir, String name, Matcher matcher);
  }

  private FileUtil() {
    //util class
  }

  /**
   * Write the content to a file in the temp directory. The file name of the
   * file is prefixed with an unique string.
   *
   * @param prefix file prefix
   * @param fileName name of the file
   * @param content content of the file
   * @throws IOException exception on write file error
   */
  public static void toFile(final String prefix, final String fileName, final String content) throws IOException {
    try (final BufferedWriter writer = Files.newBufferedWriter(File.createTempFile(prefix, fileName).toPath(), StandardCharsets.UTF_8)) {
      writer.write(content);
      writer.flush();
    }
  }

  /**
   * Strips possibly invalid characters from the filename to ensure a valid filename.
   *
   * @param name The possibly invalid filename
   * @return Valid filename
   */
  public static String getSafeFilename(final String name) {
    return ImaerFileUtil.getSafeFilename(name);
  }

  /**
   * Get an (Aerius) file name without extension.
   * Format: prefix_datestring[_optionalName]
   * @param prefix Prefix to use in the filename.
   * @param optionalName The optional name to use in the filename.
   * @param optionalDate The optional date to use for the datestring. If null, current time will be used.
   * @return The file name that can be used without extension.
   */
  public static String getFileName(final String prefix, final String optionalName, final Date optionalDate) {
    return ImaerFileUtil.getFileName(prefix, optionalName, optionalDate);
  }

  /**
   * Get an (Aerius) file name.
   * Format: prefix_datestring[_optionalName][.]extension
   * @param prefix Prefix to use in the filename.
   * @param extension Extension to use for the filename. Will be prefixed by a . if not in the string.
   * @param optionalName The optional name to use in the filename.
   * @param optionalDate The optional date to use for the datestring. If null, current time will be used.
   * @return The file name that can be used.
   */
  public static String getFileName(final String prefix, final String extension, final String optionalName,
      final Date optionalDate) {
    return ImaerFileUtil.getFileName(prefix, extension, optionalName, optionalDate);
  }

  /**
   * @param file The file to determine the extension for.
   * @return The extension (without .) or null if no extension.
   */
  public static String getExtension(final File file) {
    final String fileName = file.getName();
    final int posOfDot = fileName.lastIndexOf('.');
    return posOfDot == -1 ? null : fileName.substring(posOfDot + 1);
  }

  /**
   * @param file The file to remove the extension from.
   * @return The file without extension (without .) or the file name if no extension.
   */
  public static String getFileWithoutExtension(final File file) {
    final String fileName = file.getName();
    final int posOfDot = fileName.lastIndexOf('.');
    return posOfDot == -1 ? fileName : fileName.substring(0, posOfDot);
  }

  /**
   * Returns a list of file objects. If a file matches the give filter the filter creates a object of that file name.
   * @param path
   * @param pattern
   * @param filter
   * @return
   * @throws FileNotFoundException
   */
  public static <T> List<T> getFilteredFiles(final File path, final Pattern pattern, final FileMatcher<T> filter) throws FileNotFoundException {
    if (!path.exists()) {
      throw new FileNotFoundException(path.toString());
    }
    final List<T> list = new ArrayList<>();

    path.list((dir, name) -> {
      final Matcher matcher = pattern.matcher(name);

      if (matcher.matches()) {
        list.add(filter.get(dir, name, matcher));
      }
      return false;
    });
    return list;
  }

  /**
   * Finds all files with a filename that has the right extension in the directory, recursively.
   * @param path The directory to search for files.
   * @param filenameFilter The filter to limit extensions to search for. (without .)
   * @return List of files in the directory that have that extension.
   * @throws FileNotFoundException When the directory does not exist or is not a directory.
   */
  public static List<File> getFilesWithExtension(final File path, final FilenameFilter filenameFilter) throws FileNotFoundException {
    return ImaerFileUtil.getFilesWithExtension(path, filenameFilter);
  }

  /**
   * Finds all files with fileName in the directory, recursively.
   * @param directory The directory to search for files.
   * @param fileName The filename to search for.
   * @return List of files in the directory that have that extension.
   * @throws FileNotFoundException When the directory does not exist or is not a directory.
   */
  public static List<File> getFiles(final String directory, final String fileName) throws FileNotFoundException {
    return ImaerFileUtil.getFiles(directory, fileName);
  }

  /**
   * Remove the directory and it's contents, recursively.
   * It's <strong>highly recommended</strong> to provide the <code>allowedFilenames</code> parameter
   * It could save lives, or <strong>at least</strong> your job.
   * @param path Path to the directory to remove.
   * @param allowedFilenames The filenames patterns that are allowed to be removed.
   * It's advised to use this as a failsafe, to make sure you are not deleting a lot of other stuff by specifying the wrong directory.
   * If no allowedFilenames are supplied, the whole directory will be removed.
   * @throws IOException on I/O error or when we are trying to remove a file not matched with the given allowedFilenames.
   */
  public static void removeDirectoryRecursively(final Path path, final Pattern... allowedFilenames) throws IOException {
    if (path != null
        && (path.toFile().getAbsolutePath().isEmpty()
            || (path.getRoot() != null && path.getRoot().toFile().getAbsoluteFile().equals(path.toFile().getAbsoluteFile())))) {
      LOG.error("Trying to delete root! for: {}", path);
    } else {
      Files.walkFileTree(path, new DeleteFileVisitor(allowedFilenames));
    }
  }
}
