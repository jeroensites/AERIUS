/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Utility for one-way encryption of strings.
 */
public final class HashUtil {

  private static final Logger LOG = LoggerFactory.getLogger(HashUtil.class);

  // Not to be constructed.
  private HashUtil() { }

  /**
   * Generates a SHA-1 encrypted salted hash value of the specified data string
   * or returns null if the hash could not be created.
   * @param salt the salt to add to the data.
   * @param data the data to encrypt.
   * @return the encrypted data as a base64 encoded string.
   */
  public static String generateSaltedHash(final String salt, final List<String> data) {
    String hash = null;
    try {
      String saltedData = "";
      for (final String str : data) {
        saltedData = salt + str;
      }
      final MessageDigest sha = MessageDigest.getInstance("SHA-1");
      sha.update(saltedData.getBytes(StandardCharsets.UTF_8));
      hash = new String(Base64.encodeBase64(sha.digest()));
    } catch (final NoSuchAlgorithmException e) {
      LOG.warn("Could not generate hash.", e);
    }
    return hash;
  }
}
