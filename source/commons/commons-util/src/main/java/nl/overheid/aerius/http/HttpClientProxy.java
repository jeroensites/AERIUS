/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.http;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

/**
 * Wrapper to get remote content and return it as String.
 */
public final class HttpClientProxy {

  private HttpClientProxy() {}

  public static String deleteRemoteContent(final CloseableHttpClient client, final String url)
      throws URISyntaxException, IOException, ParseException, HttpException {
    final HttpDelete deleteRequest = new HttpDelete(url);
    try {
      try (final CloseableHttpResponse httpResponse = client.execute(deleteRequest)) {
        final String content = EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8);
        if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
          return content;
        } else {
          throw new HttpException(httpResponse.getStatusLine().getStatusCode(), content);
        }
      }
    } finally {
      // Abort in the outer try so the inner try handles the normal try-with case, then abort. Resetting here really does nothing
      // because these are http requests that are immediate from the client's perspective, but it solves a sonar issue, so...
      deleteRequest.reset();
    }
  }

  public static String postFile(final CloseableHttpClient client, final String url, final String filename, final File file, final Map<String, Object> requestParams)
      throws URISyntaxException, IOException, ParseException, HttpException {
    final HttpPost postRequest = new HttpPost(url);
    final MultipartEntityBuilder builder = MultipartEntityBuilder.create();

    builder.addBinaryBody("file", file, ContentType.DEFAULT_BINARY, filename);

    if (requestParams != null) {
      requestParams.forEach((key, value) -> builder.addTextBody(key, value.toString()));
    }
    builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
    postRequest.setEntity(builder.build());
    postRequest.addHeader(HttpHeaders.ACCEPT, ContentType.APPLICATION_JSON.getMimeType());
    try (final CloseableHttpResponse httpResponse = client.execute(postRequest)) {
      final String content = EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8);
      if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        return content;
      } else {
        throw new HttpException(httpResponse.getStatusLine().getStatusCode(), content);
      }
    }
  }
}
