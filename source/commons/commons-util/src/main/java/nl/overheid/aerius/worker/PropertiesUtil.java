/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import nl.overheid.aerius.util.OSUtils;

/**
 * Utility class to read the worker properties file.
 */
public final class PropertiesUtil {

  private PropertiesUtil() {
  }

  /**
   * Load properties from a file. This can be a file on the classpath, but if there's a localfile
   * (relative to the application), that file will be used. If no file is present with the fileName
   * (either local or on the classpath) an exception will occur.
   * @param fileName The name of the file to load the properties from.
   * @return The properties loaded from the property file.
   * @throws IOException When an exception occurred while reading the property file.
   */
  public static Properties getFromPropertyFile(final String fileName) throws IOException {
    final File localFile = new File(fileName);
    final Properties props = new Properties();
    if (localFile.exists()) {
      try (final InputStream is = new FileInputStream(localFile)) {
        props.load(is);
      }
    } else {
      try (final InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName)) {
        if (is == null) {
          throw new FileNotFoundException("Worker property file not found:" + fileName);
        }
        props.load(is);
      }
    }
    return props;
  }

  /**
   * Loads the properties from a file specific for testing. Depending on the OS it either loads the linux or windows variant of the properties file.
   * The file name convention used is <em>worker_[name]_[(linux|windows)].properties</em>
   *
   * @param name part in file name to indicate the module the test files are for.
   * @return properties in the os specific properties file
   * @throws IOException exception in case file could not be read
   */
  public static Properties getFromTestPropertiesFile(final String name) throws IOException {
    return getFromPropertyFile(String.format("worker_%s_%s.properties", name, OSUtils.isWindows() ? "windows" : "linux"));
  }
}
