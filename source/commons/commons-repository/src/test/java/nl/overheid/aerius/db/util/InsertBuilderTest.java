/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link InsertBuilder}.
 */
class InsertBuilderTest {

  private static final String TEST_TABLE = "SomeTable";

  private enum TestAttribute implements Attribute {
    TEST_COLUMN_1, TEST_COLUMN_2, TEST_COLUMN_3;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  @Test
  void testInto() {
    final InsertBuilder builder = InsertBuilder.into(TEST_TABLE);
    final Query query = builder.getQuery();
    assertEquals("INSERT INTO SomeTable() VALUES ()", query.get(), "Query without attributes");
  }

  @Test
  void testInsertAttribute() {
    final InsertBuilder builder = InsertBuilder.into(TEST_TABLE);
    builder.insert(TestAttribute.TEST_COLUMN_1);
    Query query = builder.getQuery();
    assertEquals("INSERT INTO SomeTable(test_column_1) VALUES (?)", query.get(), "Query with 1 attribute");
    assertEquals(1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Query with 1 attribute index known attribute");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Query with 1 attribute index unknown attribute");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3), "Query with 1 attribute index unknown attribute 2");

    builder.insert(TestAttribute.TEST_COLUMN_3, TestAttribute.TEST_COLUMN_2);
    query = builder.getQuery();
    assertEquals("INSERT INTO SomeTable(test_column_1, test_column_3, test_column_2) VALUES (?, ?, ?)", query.get(), "Query with 3 attributes");
    assertEquals(1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Query with 3 attributes index known attribute 1");
    assertEquals(3, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Query with 3 attributes index known attribute 2");
    assertEquals(2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3), "Query with 3 attributes index known attribute 3");
  }

  @Test
  void testInsertWithClause() {
    final InsertBuilder builder = InsertBuilder.into(TEST_TABLE);
    builder.insert(new InsertClause("someColumn", TestAttribute.TEST_COLUMN_1));
    Query query = builder.getQuery();
    assertEquals("INSERT INTO SomeTable(someColumn) VALUES (?)", query.get(), "Query with 1 insertclause");
    assertEquals(1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Query with 1 insertclause index known attribute");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Query with 1 insertclause index unknown attribute");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3), "Query with 1 insertclause index unknown attribute 2");

    builder.insert(new InsertClause("WeirdColumn", "WeirdValue", TestAttribute.TEST_COLUMN_3),
        new InsertClause("AndSomething", TestAttribute.TEST_COLUMN_2));
    query = builder.getQuery();
    assertEquals("INSERT INTO SomeTable(someColumn, WeirdColumn, AndSomething) VALUES (?, WeirdValue, ?)", query.get(), "Query with 3 insertclauses");
    assertEquals(1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Query with 3 insertclauses index known attribute 1");
    assertEquals(3, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Query with 3 insertclauses index known attribute 2");
    assertEquals(2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3), "Query with 3 insertclauses index known attribute 3");
  }


}
