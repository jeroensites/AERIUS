/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.util.OrderByClause.OrderType;

/**
 * Test class for {@link QueryBuilder}.
 */
class QueryBuilderTest {

  private static final String TEST_TABLE = "SomeTable";

  private enum TestAttribute implements Attribute {
    TEST_COLUMN_1, TEST_COLUMN_2, TEST_COLUMN_3;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private enum TestOtherEnum {
    ONE_VALUE, TWO_VALUE
  }

  @Test
  void testFrom() {
    Query query = QueryBuilder.from(TEST_TABLE).getQuery();
    assertEquals("SELECT * FROM SomeTable", query.get(), "Query with just from");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with just from");

    query = QueryBuilder.from(TEST_TABLE, TestAttribute.TEST_COLUMN_1).getQuery();
    assertEquals("SELECT * FROM SomeTable", query.get(), "Query with from and with from-attributes");
    assertEquals(1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with from and with from-attributes");
  }

  @Test
  void testSelectAttribute() {
    Query query = QueryBuilder.from(TEST_TABLE).select(TestAttribute.TEST_COLUMN_1).getQuery();
    assertEquals("SELECT test_column_1 FROM SomeTable", query.get(), "Query with 1 select");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 1 select");

    query = QueryBuilder.from(TEST_TABLE).select(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("SELECT test_column_1, test_column_2 FROM SomeTable", query.get(), "Query with 2 selects");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 2 selects");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 2 selects");

    query = QueryBuilder.from(TEST_TABLE).select(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_3, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("SELECT test_column_1, test_column_3, test_column_2 FROM SomeTable", query.get(), "Query with 3 selects");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 3 selects");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 3 selects");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3), "Index of attribute with 3 selects");
  }

  @Test
  void testSelectWithClause() {
    Query query = QueryBuilder.from(TEST_TABLE).select(new SelectClause(TestAttribute.TEST_COLUMN_1)).getQuery();
    assertEquals("SELECT test_column_1 FROM SomeTable", query.get(), "Query with 1 selectclause");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 1 selectclause");

    query = QueryBuilder.from(TEST_TABLE).select(new SelectClause(TestAttribute.TEST_COLUMN_1),
        new SelectClause(TestAttribute.TEST_COLUMN_2)).getQuery();
    assertEquals("SELECT test_column_1, test_column_2 FROM SomeTable", query.get(), "Query with 2 selectclauses");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 2 selectclauses");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 2 selectclauses");

    query = QueryBuilder.from(TEST_TABLE).select(new SelectClause(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_3),
        new SelectClause(TestAttribute.TEST_COLUMN_2)).getQuery();
    assertEquals("SELECT test_column_1, test_column_2 FROM SomeTable", query.get(), "Query with 2 selectclauses with 1 attribute");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 2 selectclauses with 1 attribute");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 2 selectclauses with 1 attribute");
    assertEquals(1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3), "Index of attribute with 2 selectclauses with 1 attribute");
  }

  @Test
  void testJoin() {
    Query query = QueryBuilder.from(TEST_TABLE).join(new JoinClause("OtherTable", TestAttribute.TEST_COLUMN_1)).getQuery();
    assertEquals("SELECT * FROM SomeTable INNER JOIN OtherTable USING (test_column_1)", query.get(), "Query with 1 join");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 1 join");

    query = QueryBuilder.from(TEST_TABLE).join(new JoinClause("OtherTable", TestAttribute.TEST_COLUMN_1),
        new JoinClause("AnotherTable", TestAttribute.TEST_COLUMN_2)).getQuery();
    assertEquals("SELECT * FROM SomeTable INNER JOIN OtherTable USING (test_column_1) INNER JOIN AnotherTable USING (test_column_2)",
        query.get(), "Query with 2 joins");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 2 joins");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 2 joins");

    query = QueryBuilder.from(TEST_TABLE).join(new JoinClause("OtherTable", "SomeJoin")).getQuery();
    assertEquals("SELECT * FROM SomeTable INNER JOIN OtherTable USING (SomeJoin)", query.get(), "Query with 1 string-based join");

    query = QueryBuilder.from(TEST_TABLE).join(new JoinClause("OtherTable", TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_3)).getQuery();
    assertEquals("SELECT * FROM SomeTable INNER JOIN OtherTable USING (test_column_1)", query.get(), "Query with 1 attribute for join");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 1 attribute for join");
    assertEquals(1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3), "Index of attribute with 1 attribute for join");

    query = QueryBuilder.from(TEST_TABLE).join(new JoinClause("OtherTable", "join1", TestAttribute.TEST_COLUMN_3),
        new JoinClause("AnotherTable", "join2", TestAttribute.TEST_COLUMN_1)).getQuery();
    assertEquals("SELECT * FROM SomeTable INNER JOIN OtherTable USING (join1) INNER JOIN AnotherTable USING (join2)",
        query.get(), "Query with 2 attribute for joins");
    assertEquals(2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 2 attribute for joins");
    assertEquals(1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3), "Index of attribute with 2 attribute for joins");
  }

  @Test
  void testWhereAttribute() {
    Query query = QueryBuilder.from(TEST_TABLE).where(TestAttribute.TEST_COLUMN_1).getQuery();
    assertEquals("SELECT * FROM SomeTable WHERE test_column_1 = ?", query.get(), "Query with 1 where");
    assertEquals(1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 1 where");

    query = QueryBuilder.from(TEST_TABLE).where(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("SELECT * FROM SomeTable WHERE test_column_1 = ? AND test_column_2 = ?", query.get(), "Query with 2 wheres");
    assertEquals(1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 2 wheres");
    assertEquals(2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 2 wheres");

    query = QueryBuilder.from(TEST_TABLE).where(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_3, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("SELECT * FROM SomeTable WHERE test_column_1 = ? AND test_column_3 = ? AND test_column_2 = ?", query.get(), "Query with 3 wheres");
    assertEquals(1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 3 wheres");
    assertEquals(3, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 3 wheres");
    assertEquals(2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3), "Index of attribute with 3 wheres");
  }

  @Test
  void testWhereWithClause() {
    final WhereClause whereClause1 = new WhereClause() {

      @Override
      public String toWherePart() {
        return "SomeWherePart";
      }

      @Override
      public Attribute[] getParamAttributes() {
        return new Attribute[0];
      }

    };
    Query query = QueryBuilder.from(TEST_TABLE).where(whereClause1).getQuery();
    assertEquals("SELECT * FROM SomeTable WHERE SomeWherePart", query.get(), "Query with 1 whereclause");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 1 whereclause");

    final WhereClause whereClause2 = new WhereClause() {

      @Override
      public String toWherePart() {
        return "SomeOtherWherePart";
      }

      @Override
      public Attribute[] getParamAttributes() {
        return new Attribute[] {TestAttribute.TEST_COLUMN_2, TestAttribute.TEST_COLUMN_1};
      }

    };
    query = QueryBuilder.from(TEST_TABLE).where(whereClause1, whereClause2).getQuery();
    assertEquals("SELECT * FROM SomeTable WHERE SomeWherePart AND SomeOtherWherePart", query.get(), "Query with 2 whereclauses");
    assertEquals(2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 2 whereclauses");
    assertEquals(1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 2 whereclauses");

  }

  @Test
  void testGroupBy() {
    Query query = QueryBuilder.from(TEST_TABLE).groupBy(TestAttribute.TEST_COLUMN_1).getQuery();
    assertEquals("SELECT * FROM SomeTable GROUP BY test_column_1", query.get(), "Query with 1 groupby");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 1 groupby");

    query = QueryBuilder.from(TEST_TABLE).groupBy(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("SELECT * FROM SomeTable GROUP BY test_column_1, test_column_2", query.get(), "Query with 2 groupbys");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 2 groupbys");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 2 groupbys");

    query = QueryBuilder.from(TEST_TABLE).groupBy(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_3, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("SELECT * FROM SomeTable GROUP BY test_column_1, test_column_3, test_column_2", query.get(), "Query with 3 groupbys");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 3 groupbys");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 3 groupbys");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3), "Index of attribute with 3 groupbys");
  }

  @Test
  void testOrderByAttribute() {
    Query query = QueryBuilder.from(TEST_TABLE).orderBy(TestAttribute.TEST_COLUMN_1).getQuery();
    assertEquals("SELECT * FROM SomeTable ORDER BY test_column_1 ASC", query.get(), "Query with 1 orderby");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 1 orderby");

    query = QueryBuilder.from(TEST_TABLE).orderBy(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("SELECT * FROM SomeTable ORDER BY test_column_1 ASC, test_column_2 ASC", query.get(), "Query with 2 orderbys");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 2 orderbys");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 2 orderbys");

    query = QueryBuilder.from(TEST_TABLE).orderBy(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_3, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("SELECT * FROM SomeTable ORDER BY test_column_1 ASC, test_column_3 ASC, test_column_2 ASC", query.get(), "Query with 3 orderbys");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 3 orderbys");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 3 orderbys");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3), "Index of attribute with 3 orderbys");
  }

  @Test
  void testOrderByWithClause() {
    Query query = QueryBuilder.from(TEST_TABLE).orderBy(new OrderByClause(TestAttribute.TEST_COLUMN_1)).getQuery();
    assertEquals("SELECT * FROM SomeTable ORDER BY test_column_1 ASC", query.get(), "Query with 1 orderby clause");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 1 orderby clause");

    query = QueryBuilder.from(TEST_TABLE).orderBy(new OrderByClause(TestAttribute.TEST_COLUMN_1),
        new OrderByClause(TestAttribute.TEST_COLUMN_2, OrderType.DESC)).getQuery();
    assertEquals("SELECT * FROM SomeTable ORDER BY test_column_1 ASC, test_column_2 DESC", query.get(), "Query with 2 orderby clauses");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 2 orderby clauses");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 2 orderby clauses");

    query = QueryBuilder.from(TEST_TABLE).orderBy(new OrderByClause(TestAttribute.TEST_COLUMN_1),
        new OrderByClause(TestAttribute.TEST_COLUMN_3, OrderType.ASC), new OrderByClause(TestAttribute.TEST_COLUMN_2, OrderType.DESC)).getQuery();
    assertEquals("SELECT * FROM SomeTable ORDER BY test_column_1 ASC, test_column_3 ASC, test_column_2 DESC", query.get(), "Query with 3 orderby clauses");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute with 3 orderby clauses");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute with 3 orderby clauses");
    assertEquals(0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3), "Index of attribute with 3 orderby clauses");
  }

  @Test
  void testLimit() {
    Query query = QueryBuilder.from(TEST_TABLE).limit().getQuery();
    assertEquals("SELECT * FROM SomeTable LIMIT ?", query.get(), "Query with limit");

    query = QueryBuilder.from(TEST_TABLE).limit().limit().getQuery();
    assertEquals("SELECT * FROM SomeTable LIMIT ?", query.get(), "Query with 2 limits (no effect)");
  }

  @Test
  void testLimitWithInt() {
    Query query = QueryBuilder.from(TEST_TABLE).limit(4).getQuery();
    assertEquals("SELECT * FROM SomeTable LIMIT 4", query.get(), "Query with set limit");

    query = QueryBuilder.from(TEST_TABLE).limit(4).limit(8).getQuery();
    assertEquals("SELECT * FROM SomeTable LIMIT 8", query.get(), "Query with 2 set limits (last should be used)");
  }

  @Test
  void testOffset() {
    Query query = QueryBuilder.from(TEST_TABLE).offset().getQuery();
    assertEquals("SELECT * FROM SomeTable", query.get(), "Query with just offset (no effect)");

    query = QueryBuilder.from(TEST_TABLE).limit().offset().getQuery();
    assertEquals("SELECT * FROM SomeTable LIMIT ? OFFSET ?", query.get(), "Query with limit and offset");

    query = QueryBuilder.from(TEST_TABLE).limit().offset().offset().getQuery();
    assertEquals("SELECT * FROM SomeTable LIMIT ? OFFSET ?", query.get(), "Query with limit and 2x offset (no extra effect)");
  }

  @Test
  void testDistinct() {
    Query query = QueryBuilder.from(TEST_TABLE).distinct().getQuery();
    assertEquals("SELECT DISTINCT * FROM SomeTable", query.get(), "Query with distinct");

    query = QueryBuilder.from(TEST_TABLE).distinct().distinct().getQuery();
    assertEquals("SELECT DISTINCT * FROM SomeTable", query.get(), "Query with 2x distinct (no extra effect)");
  }

  @Test
  void testGetQuery() {
    final Query query = QueryBuilder.from(TEST_TABLE, TestAttribute.TEST_COLUMN_1)
        .orderBy(TestAttribute.TEST_COLUMN_1)
        .groupBy(TestAttribute.TEST_COLUMN_2)
        .select(TestAttribute.TEST_COLUMN_1)
        .join(new JoinClause("SomeOtherView", TestAttribute.TEST_COLUMN_2, TestAttribute.TEST_COLUMN_2))
        .where(TestAttribute.TEST_COLUMN_3)
        .limit()
        .offset()
        .getQuery();
    //order in which the methods are called doesn't matter for the structure of the resulting query. Only matters for indexes of attributes.
    assertEquals("SELECT test_column_1 FROM SomeTable "
        + "INNER JOIN SomeOtherView USING (test_column_2) WHERE test_column_3 = ? GROUP BY test_column_2 ORDER BY test_column_1 ASC LIMIT ? OFFSET ?", query.get(), "Query with all stuff used");
    assertEquals(1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1), "Index of attribute for query with all stuff used");
    assertEquals(2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2), "Index of attribute for query with all stuff used");
    assertEquals(3, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3), "Index of attribute for query with all stuff used");
  }

}
