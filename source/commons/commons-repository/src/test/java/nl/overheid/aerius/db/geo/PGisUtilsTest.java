/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.geo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.postgis.PGbox2d;
import org.postgis.PGgeometry;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.geo.shared.WKTGeometry.TYPE;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;

/**
 * Test class for {@link PGisUtils}.
 */
class PGisUtilsTest {

  @Test
  void testGetPoint() {
    final Point nullPoint = PGisUtils.getPoint(null);
    assertNull(nullPoint, "input null should return null");

    //only testing if point actually gets converted to a point, not any other geometry.
    final org.postgis.Point pgPoint = getExamplePGPoint();
    final PGgeometry pgGeometry = new PGgeometry();
    pgGeometry.setGeometry(pgPoint);
    final Point point = PGisUtils.getPoint(pgGeometry);
    assertEquals(pgPoint.getX(), point.getX(), 0.001, "X coord");
    assertEquals(pgPoint.getY(), point.getY(), 0.001, "Y coord");

    //not only points will be converted, any geometry will
    final org.postgis.LineString pgLineString = getExamplePGLineString();
    final PGgeometry pgGeometryWithLine = new PGgeometry();
    pgGeometryWithLine.setGeometry(pgLineString);
    final Point pointFromLineString = PGisUtils.getPoint(pgGeometry);
    assertNotNull(pointFromLineString, "Any geometry should be converted");
    //could probably figure out where the point should be in this case, but not bothering.
  }

  @Test
  void testGetGeometry() {
    final org.postgis.Point pgPoint = getExamplePGPoint();
    final PGgeometry pgGeometryWithPoint = new PGgeometry();
    pgGeometryWithPoint.setGeometry(pgPoint);
    final WKTGeometry point = PGisUtils.getGeometry(pgGeometryWithPoint);
    assertTrue(point.getType() == TYPE.POINT, "Geometry should be point");
  }

  @Test
  void testGetBox() {
    final org.postgis.Point lowerLeftPoint = new org.postgis.Point(10.0, 10.0);
    final org.postgis.Point upperRightPoint = new org.postgis.Point(20.0, 30.0);
    final PGbox2d pgBBox = new PGbox2d(lowerLeftPoint, upperRightPoint);
    final BBox bbox = PGisUtils.getBox(pgBBox);
    assertEquals(pgBBox.getLLB().getX(), bbox.getMinX(), 0.001, "minX");
    assertEquals(10, bbox.getMinY(), 0.001, "minY");
    assertEquals(20, bbox.getMaxX(), 0.001, "maxX");
    assertEquals(30, bbox.getMaxY(), 0.001, "maxY");
    assertEquals(10, bbox.getWidth(), 0.001, "width");
    assertEquals(20, bbox.getHeight(), 0.001, "height");
  }

  @Test
  void testGetBoxNullArgument() {
    final BBox nullBBox = PGisUtils.getBox(null);
    assertNull(nullBBox, "input null should return null");
  }

  @Test
  void testGetGeometryWKT() {
    //just test if it returns as expected, no need for all cases as that'd be testing the postgis library...
    org.postgis.Geometry geometry = PGisUtils.getGeometry("POINT(1 2)");
    geometry = PGisUtils.getGeometry("POINT (1 2)");
    assertNotNull(geometry, "Geometry");
    assertTrue(geometry instanceof org.postgis.Point, "Geometry is a point");
    assertEquals(1, geometry.getPoint(0).getX(), 0.001, "Point x");
    assertEquals(2, geometry.getPoint(0).getY(), 0.001, "Point y");
  }

  @Test
  void testGetGeometryWrongWKT() {
    assertThrows(IllegalArgumentException.class, () -> PGisUtils.getGeometry("PINT(1 2)"),
        "Should throw an exception on invalid geometry.");
  }

  private org.postgis.Point getExamplePGPoint() {
    final double xCoord = 1.10;
    final double yCoord = 3.01;
    return new org.postgis.Point(xCoord, yCoord);
  }

  private org.postgis.LineString getExamplePGLineString() {
    final double xCoord0 = 4.10;
    final double yCoord0 = 4.01;
    final double xCoord1 = 5.05;
    final double yCoord1 = 7.698;
    final org.postgis.Point[] points = new org.postgis.Point[2];
    points[0] = new org.postgis.Point(xCoord0, yCoord0);
    points[1] = new org.postgis.Point(xCoord1, yCoord1);
    return new org.postgis.LineString(points);
  }

}
