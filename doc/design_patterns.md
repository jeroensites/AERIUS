# Design Patterns and Goals

This document describes various (loosely enforced) design patterns and goals that are being employed in all Java code.

## Exception handling

To present the user with user friendly messages a special exception is  available: `AeriusException`
This exception should be used when an error is thrown that contains information that should be shown to the user.
An AeriusException is created with a `Reason`.
This refers to a code in the `AeriusExceptionMessage` properties file.
This file contains the language specific user friendly textual representation of the error message.

## User text, Internationalization

Any text the user reads may not be put directly in source code.
There are 2 places to put texts:
- In properties files
- In the database.
Most messages should be put in the properties files.
Texts in the database are used for e-mail template messages, because this allows for easy changes in production, while properties files can only be changed at compile time.

## Database access

Database access from Java code is mainly done via the aerius-database project.
Database access is implemented directly JDBC, without the use of an ORM.
This design was chosen because the database uses a lot of views and Postgis extended functionality that mostly doesn't work well with ORM implementations.

In this pattern the only way to access the database is via Repository classes.
These classes provide static methods that get the connection object passed as an argument.

## Unit tests

All products have unit tests. These test can be run with maven.
Some tests require a database.
These databases have the convention `unittest_{product name}`.
Specific details can be found in the properties files in the test/resources maps.

# API

Server side API's should be developed specification first.
This means the API should be described in an OpenAPI specification and from this specification the API interface will be generated.
If, for whatever reason, there needs to be deviated from this approach, it should be documented.
