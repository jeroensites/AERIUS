# AERIUS Applications

This page describes the applications of AERIUS.

AERIUS is a system consisting of several applications that communicate via a message bus.
The user facing applications are GWT applications running in the browser and running on a Tomcat webserver.
All tasks requested by the user are send over the messagebus to the worker applications.
These workers can be seen as dedicated services.
All applications are written in Java code.
The applications all use centralized product specific databases.
Meaning for each specific product one database is available.
The database runs on PostgreSQL with the geo extension Postgis.
Due to the data structures, i.e. operations on lots of geo points part of the business logic is build in sql stored procedures.

## Webservers

The webservers are the user facing applications.
The webservers host the GWT applications.
The GWT applications can be seen as fat clients running the browser.
They mainly communicate with the server by only sending data back and to the browser, also called ajax.
The webservers itself are Apache Tomcat applications.
There are 5 different GWT/tomcat applications in the AERIUS system.

### Calculator

The Calculator webserver application is a GWT application with which a user can add emission sources, perform calculations, and create pdf documents.
The calculator has a strong map component based on the OpenLayers JavaScript library.
The library handles all geo information, like showing map layers, map interaction.
The calculator runs on top of the calculator database. The calculator doesn't persist user data.
But the user can import and export the data as IMAER data (GML or PDF).
Calculation results are temporary persisted during the session of the user.

### Connect

The Connect webserver application is REST service intended for machine to machine interaction.

### Geoserver

All AERIUS maps are provided by the Geoserver map server.
The map configurations are stored in text based files,
which are packaged into a single deploy file together with the Geoserver binary during build.
These configurations are managed in the SCM repository.
For some maps a custom style is provided through a SLD.
The SLD's are served via a servlet in the product specific webapplication.
Maps are accessed via a proxy servlet in the product webapplication where, if available,
the SLD url is appended to the map url that is sent to Geoserver.

## Workers

Workers are Java applications.
Some listen to the RabbitMQ queue for new tasks, or run on an internal loop, or are run as command line applications.

### Calculation Workers

Calculation workers run the scientific models on the given input sources and output at the given result points.
Result points are always returned even if there would be no result.
The principle of a calculaton workers is that it calculates the effect of the input for a specific point location, a result point,
which allows the calculation to be parallelized.
A set of result points can be split over multiple calculation workers, were each calculation worker gets the same input and a subset of the result points.
The calculation workers gets the data specific for the model used as input.
The input data therefore is only converted once in the preparation phase before calculation starts.
Calculation workers therefore don't have a connection to a AERIUS database, because all information is in the input.
One Calculation worker can run multiple processes.
These workers can be configured and the number of processes run on a single machine depends on how the specific calculation workers operates.

#### OPS Worker

The OPS worker is the worker that calculates results with the 'Operationele Prioritaire Stoffen' (OPS) model.
The application is a native application developed and maintained by the RIVM.
The executable is wrapped by the OPS Worker.
The worker generates the required input files based, runs the OPS executable, parses the result files, and sends the results back.
The number of concurrent OPS executables that can be run on one machine is theoretically limited by the number of cores.
Since OPS takes 100% CPU when it runs, it doesn't make much sense to configure the number of concurrent worker processes on one machine to be more than the available cores.

#### SRM2 Worker

The SRM2 worker is the workers that calculates results with the Standaardrekenmethode 2 (SRM2) from the "Regeling beoordeling luchtkwaliteit 2007" (a model specific for road traffic).
The model itself is implemented in OpenCL and integreted in the SRM2 Worker using the Java JOCL library.
The model uses static location specific input parameters that are installed with the worker.
There are 3 sources of this data:
1) The data map of PreSRM.
2) Preprocessed data with a small Pascal application that is part of AERIUS.
This application runs PreSRM on 1x1km areas covering the whole of the Netherlands
and generates an static input file for each year needed.
3) A file containing the deposition velocity covering the whole of the Netherlands.
This file is prepared by the maintainer of AERIUS.
For every new version of PreSRM this data must be updated and regenerated.
When a process is run OpenCL takes up all available cores.
But because of the calculation distance between sources and result points in many cases no calculations need to be done for the input.
This is handled before the OpenCL calculation is done, so the number of processes can be set higher than the available cores.
A value between 10 and 20 is recommended.

### Database Workers

Database workers are basically workers that use a database.
For these workers the database is seen as the limiting factor and therefore they are refered to as database workers.
Most workers listen to a queue for tasks, and some workers run on an internal loop.
There are also command line workers, which are technically also called database workers.
These workers get their input via the command line.

#### Calculation Worker

The calculation worker handles tasks from the Calculator web application.
These tasks can perform a calculation, generate a pdf calculation report, or create an IMAER gml file.
The calculation worker uses the calculator database.

A separate worker is the Import worker.
This worker reads an IMAER and converts it to the internal data structure, has a separate queue and is both available for Calculator and Register.

#### Message Worker

The message worker delivers input (files or messages) to the user via e-mail.
E-mails are created using templates.
The templates use input variables which can be used to customize the e-mail message from the templates.
The worker uses the calculator database.

### Scheduled Workers

Scheduled workers run on their own schedule, or query the database at a specific time.
Opposite to all other workers they don't listen to a queue.

## Task Manager

The taskmanager can be found in the repository https://github.com/aerius/taskmanager