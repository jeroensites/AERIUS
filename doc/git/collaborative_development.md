# Collaborative development

This document outlines some basic guidelines for doing efficient collaborative development, initiating and dealing with reviews, and the process to getting a pull-request merged into Master. It is by no means a template which must be followed to the letter, or at all - it is merely a set of words combined into a sentence comprising a document from which one may derive some insight.

## Master branch maintenance hierarchy

Typically, be it intentional or otherwise, the responsibility of maintaining (part of) a development branch and merging features into it will converge unto a single team member, which we shall henceforth call the maintainer. This maintainer does the final review of all pull-requests and merges them into the master branch. At this point, the feature will have sufficiently been tested from a code-quality and integrity perspective, whether by the contributor, sonar/build tools, or another team member - the code works and is sound.

There can be multiple maintainers for different parts of the application, ie. a maintainer for Web/UI work, a maintainer for repository/task work, and a maintainer for the database. They may intertwine or overlap at times, but typically some obvious single person will have some say over whether or not, why or why not, or when and when not, to merge a feature into master. When your feature is ready, properly reviewed, and can be merged into master - ping this person on GitHub in order to initiate final review.

## Reviewing features

When your feature is 'ready enough' to be reviewed by peers, be sure to make them aware of that responsibility. Ping them on GitHub, assign a PR to him(/her?), or whatever.

Reviewing a feature can be very superficial or in-depth, entirely up to the reviewer and the feature in question. Typically, if you've reviewed a PR, notify the owner of its condition using simple super-short key phrases such as:

- concept ACK (I'm OK with this idea, haven't looked at the code)
- ut ACK (untested ACK - I've looked at the code and can see nothing obviously fishy)
- tested ACK (I've actually pulled this PR, built the project, and did some digging - it works excellently!)
- NACK { elaborate } (This is utter garbage, and you should feel bad. Here's why [...])

## Collaborating on a single Pull-Request

When multiple people are contributing commits to a single PR, it's important not to get commits mixed up into an incompatible tree. Because maintaining a PR often involves forced pushes to the remote (as a result of rebasing against upstream/master), it is not easily compatible with collaborative development out-of-the-box.

When multiple people are working on a single pull-request, be sure to be aware of this. *Don't do any force pushes or rebases without the other guy being aware and ready for it*. It's pretty hard to lose a commit entirely, but there's no reason to make things complicated.

Some pointers:

- If there needs to be a rebase, let the PR owner do this: he proposed the damn thing, he's responsible for its merge-ability.
- If your local tree diverged from the PR, here's a reasonably safe way of migrating to it:

In any case, you should:

````
fetch upstream
stash your uncommitted (and untracked! git stash -u) changes
make a note of any unpushed commits
````

Then you can either:

````
checkout the PR in another branch
cherry-pick your commits
apply the stash
remove the old branch
rename the current branch
````

Alternatively (less safe, but faster):

````
hard reset to upstream PR
cherry-pick commits (they are in the cache)
apply stash
````

Then ping the other guy to fetch your stuff, and not have it happen again.
