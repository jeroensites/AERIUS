# UI User Input Validation

To help the user entering data the user interface provides input validation on input fields.
Technically should input validation be implemented using as much as possible available mechanisms.
In the Calculator application there are 2 preferred mechanisms preferred:

1. Standard HTML input validation

Modern HTML input supports a number of input validations.
These limit the input or supply different input visualizations.
An example is `type="number"`, which limits the input to numbers.
Another example is `min="0"`. This restricts input to numbers above 0.
However the validations are not perfect because with the `min` tag it's still possible to type in a negative number, but you can't use the arrow keys to go below zero.

2. Validation with `vuelidate`

Vuelidate is a JavaScript addon for Vue to provide data validation.
To use vuelidate within the Calculator application we use the wrapper `gwt-vuelidate` from our `gwt-client-common` library repository.
Vuelidate allows acting on the validations from within the code.
This makes it possible to provide feedback to the user or stop the user from continuing on validation errors.
For more details on the available feature see the vuelidate documentation or browser the code.


## Example Implementation

To help with implementing validation here the general concepts of how to add validation is shown in examples.
Validation is part of a `@Component`.
In this example all classes use `My` as prefix to the name.
In your own implementation the names should be relevant for their context.
The `vuelidate` library works on `@Data` objects.
This makes it a bit harder to use as most of the time we want data to persist in more complex objects.
This example shows how to work with a complex example.
If you can work directly with `@Data` annotated objects it's more simple.
For more detailed code see our code base for actual usages.

The example is written to do validation on data for a complex object `MyDataObject`.
This object has 2 private fields that are represented by 2 `@Data` annotated String fields in the Component.

```java
@Component(
customizeOptions = MyValidators.class,
directives = ValidateDirective.class
)
public class MyComponent extends BasicVueComponent implements HasCreated, HasActivated {
  // This is your complex object you want to use as base data
  @Prop @JsProperty MyDataObject myDataObject;

  @JsProperty(name = "$v") MyValidations validation;
  // These are the data fields representing data fields in you complex object.
  // The name here is arbitrary and simply derived by adding V to the name of the original field.
  @Data String dataField1V;
  @Data String dataField2V;

  @Computed
  protected MyValidations getV() {
    return validation;
  }

  @Override
  public void created() {
    VuelidateUtil.proxy(this);
    activated();
  }

  @Override
  public void activated() {
    // Initialize the fields with the data from the complex object
    dataField1V = String.valueOf(myDataObject.getDataField1());
    dataField2V = String.valueOf(myDataObject.getDataField2());
  }

  // The Computed methods take care of the data in the complex object.
  // For return types use objects. For int -> Number, for double -> Double
  @Computed
  protected Number getDataField1() {
    return myDataObject.getDataField1();
  }

  @Computed
  protected void setDataField1(final String data) {
    ValidationUtil.setSafeIntegerValue(myDataObject::setDataField1, data)
    // Wrap input parsing because input can be anything entered by the user at this point.
    try {
      myDataObject.setDataField1(Integer.valueOf(data));
    } catch (final NumberFormatException e) {
    }
    // Set raw data on field so it can do the validation
    this.dataField1V = data;
    // Inform the application about the newly input validation state
    updateDirty();
  }

  @Computed
  protected Double getDataField2() {
    return myDataObject.getDataField2();
  }

  @Computed
  public void setDataField2(final String data) {
    ValidationUtil.setSafeDoubleValue(myDataObject::setDataField2, data)
    this.dataField2V = data;
    updateDirty();
  }

  // This method informs the parent about the edit state. If any fields is modified dirty is true
  private void updateDirty() {
    vue().$emit("isDirty", validation.dataField1V.dirty || validation.dataField2V.dirty);
  }

  public boolean isError() {
    return validation.dataField1V.error || validation.dataField2V.error;
  }
  ...
}
```

This is how it is bound to an input element in the html:

```xml
    <input
      type="number" // html input type
      min="0.0"  // html input minium set to 0
      v-validate:dataField1V.input // this refers to the name of field in MyValidations
      v-model="dataField1"  // This refers to the @Computed get and set methods
      >
    <input
      type="number" // html input type
      min="0.0"  // html input minium set to 0
      v-validate:dataField2V.input // this refers to the name of field in MyValidations
      v-model="dataField2"  // This refers to the @Computed get and set methods
      >
```

Initialization of the validations is done in a class extending `ValidationOptions`.
The String argument passed to `install` must be application unique!
The argument must also match the name in the validations object below!

```java
class MyValidators extends ValidationOptions<MyComponent> {
  @Override
  protected void constructValidators(final Object options, final ValidatorInstaller v) {
    final MyComponent instance = Js.uncheckedCast(options);

    v.install("dataField1V", () -> instance.dataField1 = null,
        ValidatorBuilder.create().required().integer());
    v.install("dataField2V", () -> instance.dataField2 = null,
        ValidatorBuilder.create().required().decimal().minValue(0));
  }
}
```

This is the class holding all field specific validators:

```java
@JsType(isNative = true, namespace = JsPackage.GLOBAL)
class MyValidations {
  public @JsProperty Validations dataField1V;
  public @JsProperty Validations dataField2V;
}
```