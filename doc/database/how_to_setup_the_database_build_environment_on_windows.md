# How to setup the database-build-environment on Windows

## Prepare PostgreSQL and PostGIS
Download the latest PostgreSQL 12 installer. The current version is `postgresql-12.5-1-windows-x64.exe`.

* Run the installer and use the default configuration.
* Use Stack Builder to install the latest (spatial extention) PostGIS 3.1.1 bundle.

Open pgAdmin4 and connect to the database as user `postgres`. Create a new Login Role called `aerius` and make sure all Role privileges are checked. Reconnect to the database as user `aerius`. 

## Prepare Ruby
Download the latest RubyInstaller from https://rubyinstaller.org/downloads/. The current version is `rubyinstaller-devkit-3.0.0-1-x64.exe`.

Run the installer as Administrator. Check *Add Ruby executables to you PATH* during the install.

In a new commandprompt, install the following Ruby gems like so (make sure to run it as Administrator):

```
gem install net-ssh -v 4.2.0
gem install net-sftp -v 2.1.2
gem install clbustos-rtf
```

(At the time of writing the latest `net-ssh` and `net-sftp` sometimes causing problems while syncing large files. Therefore `net-ssh` version 4.2.0 and `net-sftp` version 2.1.2 are recommended.)

## Prepare Git
* Download the latest Git from https://git-scm.com/download/win, and run the installer 
* Checkout the **AERIUS-II** git repository from https://github.com/aerius/AERIUS-II.git
* Checkout the **aerius-database-build** git repository from https://github.com/aerius/aerius-database-build.git (at sibling level of AERIUS-II, so same parent folder)

## Prepare database build script
Define your own settings in `\AERIUS-II\aerius-database-build\config\AeriusSettings.User.rb`.

Specify at least the `db-data` folder and create that path on your machine. Also specify the password for SFTP and for the `aerius` user that you created earlier.

Example:

```
$dbdata_path = 'c:/Database/db-data/aeriusII/'

$sftp_data_readonly_username = '...'
$sftp_data_readonly_password = '...'

$pg_port = 5433   # if nondefault
$pg_password = '...'   # for aerius user
#$pg_username = 'postgres'  # if you do not want to use aerius user

$pg_bin_path = 'C:/Program Files/PostgreSQL/10/bin/'  # on Windows, if not in PATH
$git_bin_path = 'c:/Users/xyz/AppData/Local/Atlassian/SourceTree/git_local/bin/'  # on Windows, if not in PATH

#$database_tablespace = 'ssd'  # if you want to build on a nondefault tablespace
```

## Run database build scripts
The following commands are most important. They need to run from one of the AERIUS databases root folders, e.g. `aerius-database-calculator`.

It is recommended to make `.bat` files to easily run them in the future. (If you do not, then do not include the `%*` below.)

- **Download all needed data files from SFTP** to `db-data`:<br>
[`sync_db.bat`]<br>
`ruby ..\..\aerius-database-build\bin\SyncDBData.rb settings.rb --from-sftp --to-local`

- **Test database structure only** (no data, very quick):<br>
[`test_build.bat`]<br>
`ruby ..\..\aerius-database-build\bin\Build.rb test_build.rb settings.rb -v structure %*`

  * To skip the check for missing data files, use `test_structure.rb` instead of `test_build.rb`.

- **Build full database**:<br>
[`build_quick.bat`]<br>
`ruby ..\..\aerius-database-build\bin\Build.rb default.rb settings.rb --flags QUICK -v # %*`

  * To include creation of a database dump and vacuuming, remove `--flags QUICK`.

  * You can override the default database name with parameter `--database-name`. Character `#` is substituted with the git hash.

Once the bare scripts work, you can also use [Maven](/doc/development_build_test.md) for building your databases.
