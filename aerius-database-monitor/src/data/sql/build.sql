SELECT ae_raise_notice('Build: sector_depositions_no_policies @ ' || timeofday());

{multithread on: SELECT unnest(enum_range(NULL::sectorgroup_type)) AS sector_group ORDER BY sector_group}
	SELECT setup.ae_build_sector_depositions_no_policies('{sector_group}');
{/multithread}


-- Other depositions
BEGIN;
	SELECT ae_raise_notice('Build: other_depositions @ ' || timeofday());

	SELECT setup.ae_build_other_depositions();
COMMIT;


-- Totale depositie (met other depositions)
BEGIN;
	SELECT ae_raise_notice('Build: depositions_no_policies @ ' || timeofday());

	INSERT INTO depositions_no_policies(receptor_id, year, deposition)
		SELECT receptor_id, year, deposition FROM setup.build_depositions_no_policies_view;
COMMIT;


-- Delta depositie
BEGIN;
	SELECT ae_raise_notice('Build: delta_depositions_no_policies @ ' || timeofday());

	INSERT INTO delta_depositions_no_policies(receptor_id, year, delta_deposition)
		SELECT receptor_id, year, delta_deposition FROM setup.build_delta_depositions_no_policies_view;
COMMIT;


-- Receptoren met KDW overschrijding
BEGIN;
	SELECT ae_raise_notice('Build: exceeding_receptors @ ' || timeofday());

	INSERT INTO exceeding_receptors(receptor_id, year)
		SELECT receptor_id, year FROM setup.build_exceeding_receptors_view;
COMMIT;


-- Assessment_areas en de stikstofgevoelige doelstellingstypen (relevante goal_habitat_types)
BEGIN;
	SELECT ae_raise_notice('Build: assessment_areas_to_relevant_goal_habitats @ ' || timeofday());

	INSERT INTO assessment_areas_to_relevant_goal_habitats(assessment_area_id, goal_habitat_type_id, coverage, geometry)
		SELECT assessment_area_id, goal_habitat_type_id, coverage, geometry FROM setup.build_assessment_areas_to_relevant_goal_habitats_view;
COMMIT;


-- Assessment_areas en het relevante deel van de hexagonen
BEGIN;
	SELECT ae_raise_notice('Build: receptors_to_assessment_areas_on_relevant_goal_habitats @ ' || timeofday());

	INSERT INTO receptors_to_assessment_areas_on_relevant_goal_habitats(assessment_area_id, receptor_id, weight, surface, geometry)
		SELECT assessment_area_id, receptor_id, weight, surface, geometry FROM setup.build_receptors_to_assessment_areas_on_relevant_goal_habitats_view;
COMMIT;
