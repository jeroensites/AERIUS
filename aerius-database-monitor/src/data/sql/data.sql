BEGIN; SELECT setup.ae_load_table('substances', '{data_folder}/public/substances_20190716.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('years', '{data_folder}/public/years_monitor_20211224.txt'); COMMIT;

--load Monitor-specific sectors
BEGIN; SELECT setup.ae_load_table('sectors', '{data_folder}/public/sectors_monitor_20210430.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('gcn_sectors', '{data_folder}/public/gcn_sectors_monitor_20210430.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('sectors_main_gcn_sector', '{data_folder}/public/sectors_main_gcn_sector_monitor_20210315.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('emission_diurnal_variations', '{data_folder}/public/emission_diurnal_variations_20170119.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('gcn_sector_source_characteristics', '{data_folder}/public/gcn_sector_source_characteristics_20210107.txt'); COMMIT;


{import_common 'areas_hexagons_and_receptors/load_monitor.sql'}


BEGIN; SELECT setup.ae_load_table('sectors_sectorgroup', '{data_folder}/public/sectors_sectorgroup_monitor_20210315.txt'); COMMIT;


BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_economic_scale_factors', '{data_folder}/setup/setup.gcn_sector_economic_scale_factors_20210326.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_economic_scale_factors_no_economy', '{data_folder}/setup/setup.gcn_sector_economic_scale_factors_no_economy_20210326.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_agriculture', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_agriculture_20210325.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_industry', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_industry_20210325.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_other', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_other_20210325.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_road_transportation', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_road_transportation_20210325.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_road_freeway', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_road_freeway_20210325.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_shipping', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_shipping_20210325.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('other_depositions', '{data_folder}/public/other_depositions_20210603.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('foreign_countries', '{data_folder}/public/foreign_countries_20210701.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('sectors_abroad', '{data_folder}/public/sectors_abroad_20210701.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('sectorgroup_depositions_abroad', '{data_folder}/public/sectorgroup_depositions_abroad_20210604.txt'); COMMIT;
