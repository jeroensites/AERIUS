/*
 * Insert system constants.
 */

INSERT INTO system.constants (key, value) VALUES ('DEFAULT_LOCALE', 'en');

-- Release (valid values: PRODUCTION, CONCEPT, DEPRECATED)
INSERT INTO system.constants (key, value) VALUES ('RELEASE', 'CONCEPT');

INSERT INTO system.constants (key, value) VALUES ('CONNECT_INTERNAL_URL', 'http://localhost:8081/api');
INSERT INTO system.constants (key, value) VALUES ('GEOSERVER_INTERNAL_URL', 'http://localhost:8082/geoserver-calculator/wms');
INSERT INTO system.constants (key, value) VALUES ('SEARCH_ENDPOINT_URL', '/api/search/');

-- Calculation config for workers
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_ENGINE_UNITS_WORKER_MAX', '100000');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_WORKER_MIN', '5');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_WORKER_MAX', '500');

-- email constants
INSERT INTO system.constants (key, value) VALUES ('NOREPLY_EMAIL', 'noreply@aerius.nl');
INSERT INTO system.constants (key, value) VALUES ('DEFAULT_FILE_MAIL_DOWNLOAD_LINK', 'https://test.aerius.nl/downloads/');

-- Emission result display settings
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_CONVERSION_FACTOR', 0.014286);
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_UNIT', 'KILOGRAM_UNITS'); -- Option of: MOLAR_UNITS, KILOGRAM_UNITS
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_ROUNDING_LENGTH', 3);
INSERT INTO system.constants (key, value) VALUES ('EMISSION_RESULT_DISPLAY_PRECISE_ROUNDING_LENGTH', 4);

-- System info message default empty 
INSERT INTO system.constants (key, value) VALUES ('SYSTEM_INFO_PASSKEY', '');
INSERT INTO system.constants (key, value) VALUES ('SYSTEM_INFO_POLLING_TIME', 120000);
