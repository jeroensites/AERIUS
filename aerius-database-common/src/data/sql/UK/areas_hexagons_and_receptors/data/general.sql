BEGIN; SELECT setup.ae_load_table('setup.province_land_borders', '{data_folder}/UK/setup/setup.province_land_borders_20161104.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('countries', '{data_folder}/UK/public/countries_20211110.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('authorities', '{data_folder}/UK/public/authorities_20211110.txt', TRUE); COMMIT;

BEGIN; SELECT setup.ae_load_table('natura2000_areas', '{data_folder}/UK/public/natura2000_areas_20211110.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('natura2000_directive_areas', '{data_folder}/UK/public/natura2000_directive_areas_20211110.txt', TRUE); COMMIT;
-- UK has no specific natura2000_area_properties, so insert something sensible for now.
-- definitief is a dutch term, but that's needed due to enum aspect. It ensures that every habitat_area within the natura2000_area is considered relevant.
INSERT INTO natura2000_area_properties (natura2000_area_id, registered_surface, design_status)
	SELECT natura2000_area_id, ST_Area(geometry), 'definitief' FROM natura2000_areas;

BEGIN; SELECT setup.ae_load_table('habitat_types', '{data_folder}/UK/public/habitat_types_20211110.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitat_type_critical_levels', '{data_folder}/UK/public/habitat_type_critical_levels_20211111.txt', TRUE); COMMIT;
--habitat_areas data was big enough that it was causing issues on windows machine (something with not being able to stat the file by a couple of postgres versions)
--hence this split version (where only the first one has a header).
BEGIN; SELECT setup.ae_load_table('habitat_areas', '{data_folder}/UK/public/habitat_areas_splitaa_20211111.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitat_areas', '{data_folder}/UK/public/habitat_areas_splitab_20211111.txt', FALSE); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitat_areas', '{data_folder}/UK/public/habitat_areas_splitac_20211111.txt', FALSE); COMMIT;
-- UK has no specific habitat relations or properties data either, so insert something sensible for now.
-- Since we have no other information, use a 1:1 relation between habitat type and goal habitat type
INSERT INTO habitat_type_relations (habitat_type_id, goal_habitat_type_id)
	SELECT habitat_type_id, habitat_type_id FROM habitat_types;
-- All habitat-areas are going to be marked as relevant
-- (level isn't entirely correct, but can't use 'none' and nothing else really seems to match)
INSERT INTO habitat_properties (goal_habitat_type_id, assessment_area_id, quality_goal, extent_goal, design_status)
	SELECT DISTINCT habitat_type_id, assessment_area_id, 'level'::habitat_goal_type, 'level'::habitat_goal_type, 'definitief'::design_status_type
		FROM habitat_areas;

-- Currently no species data for UK yet
--BEGIN; SELECT setup.ae_load_table('species', '{data_folder}/UK/public/species_20161104.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('species_properties', '{data_folder}/UK/public/species_properties_20161104.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('species_to_habitats', '{data_folder}/UK/public/species_to_habitats_20161104.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('designated_species', '{data_folder}/UK/public/designated_species_20161104.txt'); COMMIT;

--No need for province areas for UK (WMS layer isn't used at the moment). It also depends on authority IDs
--BEGIN; SELECT setup.ae_load_table('province_areas', '{data_folder}/UK/public/province_areas_20161104.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('setup.geometry_of_interests', '{data_folder}/UK/setup/setup.geometry_of_interests_20211112.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('receptors', '{data_folder}/UK/public/receptors_20211112.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('hexagons', '{data_folder}/UK/public/hexagons_20211112.txt'); COMMIT;

--No terrain properties (yet)
--BEGIN; SELECT setup.ae_load_table('terrain_properties', '{data_folder}/UK/public/terrain_properties_20161022.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('receptors_to_assessment_areas', '{data_folder}/UK/public/receptors_to_assessment_areas_20211112.txt'); COMMIT;
/* Om te genereren: import_common 'modules/areas_hexagons_and_receptors/build/receptors_to_assessment_areas.sql'*/

--same as habitat_areas, following files were split.
BEGIN; SELECT setup.ae_load_table('relevant_habitat_areas', '{data_folder}/UK/public/relevant_habitat_areas_aa_20211112.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('relevant_habitat_areas', '{data_folder}/UK/public/relevant_habitat_areas_ab_20211112.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('relevant_habitat_areas', '{data_folder}/UK/public/relevant_habitat_areas_ac_20211112.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitats', '{data_folder}/UK/public/habitats_aa_20211112.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitats', '{data_folder}/UK/public/habitats_ab_20211112.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('habitats', '{data_folder}/UK/public/habitats_ac_20211112.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('relevant_habitats', '{data_folder}/UK/public/relevant_habitats_aa_20211112.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('relevant_habitats', '{data_folder}/UK/public/relevant_habitats_ab_20211112.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('relevant_habitats', '{data_folder}/UK/public/relevant_habitats_ac_20211112.txt'); COMMIT;
/* Om te genereren: import_common 'modules/areas_hexagons_and_receptors/build/habitats.sql' */

BEGIN; SELECT setup.ae_load_table('receptors_to_critical_deposition_areas', '{data_folder}/UK/public/receptors_to_critical_deposition_areas_20211112.txt'); COMMIT;
/* Om te genereren: import_common 'modules/areas_hexagons_and_receptors/build/receptors_to_critical_deposition_areas.sql' */

--No longer required (only needed for search widget it seems)
--BEGIN; SELECT setup.ae_load_table('assessment_areas_to_province_areas', '{data_folder}/UK/public/assessment_areas_to_province_areas_20161104.txt'); COMMIT;
/* Om te genereren: import_common 'modules/areas_hexagons_and_receptors/build/assessment_areas_to_province_areas.sql'*/

--Should be a deprecated table, but for now needed by some views.
INSERT INTO pas_assessment_areas (assessment_area_id)
	SELECT assessment_area_id FROM natura2000_areas;

--No specific colors yet, so input something based on habitat critical levels.
INSERT INTO system.habitat_type_colors(habitat_type_id, fill_color, stroke_color)
SELECT
	habitat_type_id, 
	fill_color,
	stroke_color

	FROM
		(SELECT
			habitat_type_id,
			ae_critical_deposition_classification(critical_level) AS critical_deposition_classification

			FROM habitat_type_critical_levels

			WHERE
				result_type = 'deposition'
				AND substance_id = '1711'
		) AS habitats

		INNER JOIN
			(SELECT
				unnest(ARRAY['high_sensitivity', 'normal_sensitivity', 'low_sensitivity']) AS critical_deposition_classification,
				unnest(ARRAY['7B3294', 'C2A5CF', 'EBF5C8']) AS fill_color,
				unnest(ARRAY['E4E4E4', 'E4E4E4', 'E4E4E4']) AS stroke_color
			) AS colors USING (critical_deposition_classification)

	ORDER BY habitat_type_id;
