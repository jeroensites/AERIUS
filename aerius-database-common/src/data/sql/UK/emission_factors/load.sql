--TODO: fill based on data supplied by UK partners

/* Road data */
--for now use the dutch emission factors
BEGIN; SELECT setup.ae_load_table('road_categories', '{data_folder}/public/road_categories_20160602.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_speed_profiles', '{data_folder}/public/road_speed_profiles_20200929.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_category_emission_factors', '{data_folder}/public/road_category_emission_factors_20210519.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('mobile_source_on_road_categories', '{data_folder}/public/mobile_source_on_road_categories_20210618.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('mobile_source_on_road_category_emission_factors', '{data_folder}/public/mobile_source_on_road_category_emission_factors_TEMP2035_20210630.txt'); COMMIT;

/* Farm animal category and -lodging data */
--Data supplied so far for UK
BEGIN; SELECT setup.ae_load_table('farm_animal_categories', '{data_folder}/UK/public/farm_animal_categories_20211110.txt', TRUE); COMMIT;
--Not available/supplied (unknown for UK)
--BEGIN; SELECT setup.ae_load_table('farm_lodging_system_definitions', '{data_folder}/UK/public/farm_lodging_system_definitions_20161107.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('farm_lodging_types', '{data_folder}/UK/public/farm_lodging_types_20211110.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_type_emission_factors', '{data_folder}/UK/public/farm_lodging_type_emission_factors_20211110.txt', TRUE); COMMIT;
--Not available/supplied (unknown for UK)
--BEGIN; SELECT setup.ae_load_table('farm_lodging_types_other_lodging_type', '{data_folder}/UK/public/farm_lodging_types_other_lodging_type_20161107.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_lodging_types_to_lodging_system_definitions', '{data_folder}/UK/public/farm_lodging_types_to_lodging_system_definitions_20161107.txt'); COMMIT;

--Not available/supplied (unknown for UK)
--BEGIN; SELECT setup.ae_load_table('farm_additional_lodging_systems', '{data_folder}/UK/public/farm_additional_lodging_systems_20161107.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_additional_lodging_system_emission_factors', '{data_folder}/UK/public/farm_additional_lodging_system_emission_factors_20161107.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_additional_lodging_systems_to_lodging_system_definitions', '{data_folder}/UK/public/farm_additional_lodging_systems_to_lodging_system_definitions_20161107.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_lodging_types_to_additional_lodging_systems', '{data_folder}/UK/public/farm_lodging_types_to_additional_lodging_systems_20161107.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('farm_reductive_lodging_systems', '{data_folder}/UK/public/farm_reductive_lodging_systems_20211110.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_reductive_lodging_system_reduction_factors', '{data_folder}/UK/public/farm_reductive_lodging_system_reduction_factors_20211110.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_types_to_reductive_lodging_systems', '{data_folder}/UK/public/farm_lodging_types_to_reductive_lodging_systems_20211111.txt', TRUE); COMMIT;
--Not available/supplied (unknown for UK)
--BEGIN; SELECT setup.ae_load_table('farm_reductive_lodging_systems_to_lodging_system_definitions', '{data_folder}/UK/public/farm_reductive_lodging_systems_to_lodging_system_definitions_20161107.txt'); COMMIT;

--Not available/supplied (unknown for UK)
--BEGIN; SELECT setup.ae_load_table('farm_lodging_fodder_measures', '{data_folder}/UK/public/farm_lodging_fodder_measures_20161107.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_lodging_fodder_measure_reduction_factors', '{data_folder}/UK/public/farm_lodging_fodder_measure_reduction_factors_20161107.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('farm_lodging_fodder_measures_animal_category', '{data_folder}/UK/public/farm_lodging_fodder_measures_animal_category_20161107.txt'); COMMIT;

/* Farmland categories */
--for now use the dutch categories
BEGIN; SELECT setup.ae_load_table('farmland_categories', '{data_folder}/temp/temp_farmland_categories_20200527.txt'); COMMIT;

--Other categories: plan, mobile_source_off_road, shipping_maritime and shipping_inland
--Not using these (yet). Shipping requires something 
