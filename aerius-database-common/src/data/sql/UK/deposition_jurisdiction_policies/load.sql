-- Not (yet) supplied.
--BEGIN; SELECT setup.ae_load_table('depositions_jurisdiction_policies', '{data_folder}/UK/public/depositions_jurisdiction_policies_20161026.txt'); COMMIT;

--Temporary: insert background for each available receptor. Later on can fill this based on background_cell_depositions (or adjust views).
INSERT INTO depositions_jurisdiction_policies (year, receptor_id, total_deposition)
SELECT generate_series(2019, 2035) AS year, receptor_id, 0 AS total_deposition
	FROM receptors;
