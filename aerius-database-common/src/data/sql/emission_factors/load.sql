/* Road data */
BEGIN; SELECT setup.ae_load_table('road_categories', '{data_folder}/public/road_categories_20160602.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_speed_profiles', '{data_folder}/public/road_speed_profiles_20200929.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('road_category_emission_factors', '{data_folder}/public/road_category_emission_factors_20210519.txt'); COMMIT;

/* Farm animal category and -lodging data */
BEGIN; SELECT setup.ae_load_table('farm_animal_categories', '{data_folder}/public/farm_animal_categories_20210607.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_system_definitions', '{data_folder}/public/farm_lodging_system_definitions_20211026.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('farm_lodging_types', '{data_folder}/public/farm_lodging_types_20211026.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_type_emission_factors', '{data_folder}/public/farm_lodging_type_emission_factors_20211026.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_types_other_lodging_type', '{data_folder}/public/farm_lodging_types_other_lodging_type_20211026.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_types_to_lodging_system_definitions', '{data_folder}/public/farm_lodging_types_to_lodging_system_definitions_20211026.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('farm_additional_lodging_systems', '{data_folder}/public/farm_additional_lodging_systems_20210607.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_additional_lodging_system_emission_factors', '{data_folder}/public/farm_additional_lodging_system_emission_factors_20210607.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_additional_lodging_systems_to_lodging_system_definitions', '{data_folder}/public/farm_additional_lodging_systems_to_lodging_system_definitions_20210607.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_types_to_additional_lodging_systems', '{data_folder}/public/farm_lodging_types_to_additional_lodging_systems_20200914.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('farm_reductive_lodging_systems', '{data_folder}/public/farm_reductive_lodging_systems_20211026.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_reductive_lodging_system_reduction_factors', '{data_folder}/public/farm_reductive_lodging_system_reduction_factors_20211026.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_reductive_lodging_systems_to_lodging_system_definitions', '{data_folder}/public/farm_reductive_lodging_systems_to_lodging_system_definitions_20211026.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_types_to_reductive_lodging_systems', '{data_folder}/public/farm_lodging_types_to_reductive_lodging_systems_20210607.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('farm_lodging_fodder_measures', '{data_folder}/public/farm_lodging_fodder_measures_20210607.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_fodder_measure_reduction_factors', '{data_folder}/public/farm_lodging_fodder_measure_reduction_factors_20210607.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_fodder_measures_animal_category', '{data_folder}/public/farm_lodging_fodder_measures_animal_category_20210628.txt'); COMMIT;

/* Farmland categories */
BEGIN; SELECT setup.ae_load_table('farmland_categories', '{data_folder}/temp/temp_farmland_categories_20200527.txt'); COMMIT;

/* Plan emissionfactor data */
--No longer required. Plans are converted to generic sources on import.
--BEGIN; SELECT setup.ae_load_table('plan_categories', '{data_folder}/public/plan_categories_20141127.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('plan_category_emission_factors', '{data_folder}/public/plan_category_emission_factors_20160418.txt'); COMMIT;

/* Mobile source data */
BEGIN; SELECT setup.ae_load_table('mobile_source_off_road_categories', '{data_folder}/public/mobile_source_off_road_categories_20210923.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('mobile_source_off_road_category_emission_factors', '{data_folder}/public/mobile_source_off_road_category_emission_factors_20210922.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('mobile_source_off_road_category_adblue_properties', '{data_folder}/public/mobile_source_off_road_category_adblue_properties_20210929.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('mobile_source_on_road_categories', '{data_folder}/public/mobile_source_on_road_categories_20210618.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('mobile_source_on_road_category_emission_factors', '{data_folder}/public/mobile_source_on_road_category_emission_factors_TEMP2035_20210630.txt'); COMMIT;

/* Shipping data */
BEGIN; SELECT setup.ae_load_table('shipping_maritime_categories', '{data_folder}/public/shipping_maritime_categories_20140331.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_category_maneuver_properties', '{data_folder}/temp/temp_shipping_maritime_category_maneuver_properties_20140402.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_maneuver_areas', '{data_folder}/temp/temp_shipping_maritime_maneuver_areas_20140422.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_category_emission_factors', '{data_folder}/public/shipping_maritime_category_emission_factors_TEMP2035_20210616.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_category_source_characteristics', '{data_folder}/public/shipping_maritime_category_source_characteristics_TEMP2035_20210616.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_maritime_mooring_maneuver_factors', '{data_folder}/public/shipping_maritime_mooring_maneuver_factors_20210604.txt'); COMMIT;

/* Inland shipping data */
BEGIN; SELECT setup.ae_load_table('shipping_inland_categories', '{data_folder}/public/shipping_inland_categories_20140327.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_inland_category_source_characteristics', '{data_folder}/public/shipping_inland_category_source_characteristics_20210412.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_inland_category_source_characteristics_docked', '{data_folder}/public/shipping_inland_category_source_characteristics_docked_20140326.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_inland_category_emission_factors', '{data_folder}/public/shipping_inland_category_emission_factors_TEMP2035_20210616.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('shipping_inland_category_emission_factors_docked', '{data_folder}/public/shipping_inland_category_emission_factors_docked_TEMP2035_20210616.txt'); COMMIT;

/* Machinery data */
--Machinery data is up for removal with changes to custom offroad mobile sources (AER3-961)
--Commented out since machinery_fuel_types_to_sectors contains an old sector ID (3230)
--BEGIN; SELECT setup.ae_load_table('machinery_fuel_types', '{data_folder}/public/machinery_fuel_types_20200701.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('machinery_types', '{data_folder}/public/machinery_types_20201130.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('machinery_type_fuel_options', '{data_folder}/public/machinery_type_fuel_options_20210302.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('machinery_type_emission_factors', '{data_folder}/public/machinery_type_emission_factors_20210302.txt'); COMMIT;
--BEGIN; SELECT setup.ae_load_table('machinery_fuel_types_to_sectors', '{data_folder}/public/machinery_fuel_types_to_sectors_20200701.txt'); COMMIT;
