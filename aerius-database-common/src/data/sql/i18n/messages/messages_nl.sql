-- email constants
INSERT INTO i18n.messages (key, language_code, message) VALUES ('CONNECT_APIKEY_CONFIRM_SUBJECT', 'nl', 'Uw AERIUS Connect API key');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('CONNECT_APIKEY_CONFIRM_BODY', 'nl',
E'<p style="font-family:Arial;font-size:14px;">Geachte heer/mevrouw,</p>
<p style="font-family:Arial;font-size:14px;">U heeft op [CALC_CREATION_DATE] om [CALC_CREATION_TIME] uur een API key aangevraagd voor AERIUS Connect.
De API key is gegenereerd en kan direct worden gebruikt om toegang te krijgen tot de Connect services.</p>
<p style="font-family:Arial;font-size:14px;">Uw API key is: <b>[CONNECT_APIKEY]</b></p>');

INSERT INTO i18n.messages (key, language_code, message) VALUES ('MAIL_CONTENT_TEMPLATE', 'nl',
E'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<table border="0" width="600">
<tr><td>[MAIL_CONTENT]
<p style="font-family:Arial;font-size:14px;">Met vriendelijke groet,<br />
[MAIL_SIGNATURE]</p>
<p style="font-family:Arial;font-size:14px;">PS: Dit is een automatisch gegenereerde email; u kunt op deze mail niet reageren via \'beantwoorden\'.</p><br /></td></tr>
<tr><td>
<div style="width:560px;border:1px solid #b7c5c5;border-radius:15px;padding:8px 20px 0">
	<table style="border:0" width="560px">
		<tbody>
			<tr>
				<td colspan="2" style="padding-bottom:10px">
					<img alt="AERIUS" height="34" src="data:image/gif;base64,R0lGODlhjwAfANU/AKm5y+Hy+TKItZWmvGeFpZitwrTB0dLl7zdZg+Pt8yhNetLa5HOuzeDm7FVzl8LR3nWNqdHt98LN2UNki/H2+aOyxvDy9TlrlIqiu/r7/WV9noWbtB1FdEtrkc3W4R5pmcTi8MPc6cPX5FiCpBs7bJTU7EdojrPg8WbC5PD5/HbI5ki13imp2ZG/14XO6VWcwaPa73aYtVhvk16w1JvN4zmv2ypfi1e84Ul2nLTT5COTxRtUgxqj1h18rRlCcv///yH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAzODAxMTc0MDcyMDY4MTE4MDgzRTJEQ0EyOUQ3RjY2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkQ2MTY4Qjg0QzdBMTExRTJBRjAyQTNBREMzOTczOTEwIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkQ2MTY4QjgzQzdBMTExRTJBRjAyQTNBREMzOTczOTEwIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowMjgwMTE3NDA3MjA2ODExODIyQUZENkE3ODIxNkZEMCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowMzgwMTE3NDA3MjA2ODExODA4M0UyRENBMjlEN0Y2NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAEAAD8ALAAAAACPAB8AAAb/wJ9wSCwaj8ikcslsOp/QnwFB0lii2Kx2yy1uFk2I9VeZXLvotHrb4HSYFc1PQRrY1/i83ujw+QBLCA0/JBIKDQh7iotdBn4+ChRKJEIkdYSMmZpNGQ4InwgDShqidTISMpuqq1sWEwMVPxAKYKy2t00DVDK1uL6/UCU8w8QpSCA6ycrLOjRDIT3R0j0CLy0hRyIf2x9FDzvgO0gj4UcWcSaPJg4QTBQFGh0T8/T17BJZN8TEJ8fM/86EQJtGsMeLBEW0cfMWTtwRcuCMDFDwqKKfJYYsarTo4IyTFPuIqfD3b1nAHwMLThMgaYjCbQzLPZQ5ZMPGRw6UeOBws6cP/xMZnsAgtoIYC5LJQChdCiLAs2lDDjCYxoDIy25EvtEsAtGhEAkVIVSQQLbCnSQTfPoU5UQFsQj7IhxBpmxJyh5FWkB1yQ3rEK0RZwYWAuGRgSgL1Pqc8ITFsBo/ig5zMXeZ3b1DEkzDJuRqzMFct1L8iQWA4p4cnJwQ+cMF0cp1ldw1spnvwqwNx21VV9pngQ4+nbge1g/EPqdF6Ca7LI22tAO2YeLeSqQrkdE+DkMxfXNChsQ9ndQoJmRfCSPKdTCPViSHNAFW+372Ghp04UcQtDfhvvEB4fBMwDXMDUPoMwwK6JVk2VPNEfGCNFVF55cQgNFX3VZgWaRAfkzwZ/8RAUNYgJ1FTQgzzHlCmDiMMUSkVxIRswkh1XsISTifbqCRcRMCsSThYUUbFCBkAX1s1IRkPMglhIDEJadgbAKpRNBJnck3XY5DWFeEBEVq1A4SP55WERMgDXMUEY4NM1KLyzC1FIxSRvMCdAlZ+Vduglk4RAMDpGORfkWEKeZFSwx1YBEoGOUklEjcVVAL2dhJIZ5GaJlEAxpUlNMRgg7KRKIhhTqMkkKktx5eP2gmDWfx3XYndVnCekSmj4A5qJFLiKorZUOYKhtmekXDUp2uCtFAQ4MYYQM4ODSRIaFGdCrmEqvpGuoKbDJ6RIw/UCCANJC2Kh0RDRVgBGAxNOH/iB8m2HprRakp4dZkR6jIA3I/+JoEtz+495y4EwpBQDg29PLDAheE498SiDzClhEebKSABRRTLGgiSozXpBFM8oBivm26CQKdKGEmxINyAnxuQzuMIOTA4YxAxAKkjFXWLI8g4JERCGikQBHrWoSBEh0nkSYPCJb6ZDIzMMgeEapGE+4PnhkRA8tY25DsV2rRosSzj/xMRNCPTNASEiomfcS8Kyq9dNNRNkhEsNHQWbURGGAd89Zc9+QA30gYMKLYQ5D9U41JIAlDEoZu7OI/cJcs9xDeSvNClcUW0UABIyS8Aw4xLKy5Aw5gt84AgCuRAAaeIMAYERJ8MoEGAAS1AkQQADs=" width="143" /></td>
			</tr>
			<tr valign="top">
				<td style="font-family:Arial;font-size:12px;width:260px">
					<a href="[BIJ12_HELPDESK_URL]/" style="color:#333;text-decoration:none">www.bij12.nl/onderwerpen/stikstof-en-natura2000/helpdesk/</a><br />
					<a href="https://www.aerius.nl" style="color:#333;text-decoration:none">www.aerius.nl</a><br />
					<a href="http://twitter.com/AERIUSapp" style="color:#333;text-decoration:none">twitter.com/AERIUSapp</a><br />
					&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" style="font-family:Arial;color:#6e6e6e;font-size:12px;padding-top:8px">
					AERIUS is ontwikkeld in opdracht van de Rijksoverheid en de gezamenlijke provincies</td>
			</tr>
		</tbody>
	</table>
</div>
<div style="width:600px;text-align:center">
	<img alt="Ministerie van Landbouw, Natuur en Voedselkwaliteit" height="51" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAk4AAAAzCAIAAADuGBCUAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3BpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowMzgwMTE3NDA3MjA2ODExODA4M0UyRENBMjlEN0Y2NiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpGRTlFQjY3NENBMEQxMUU4OTE1Q0JBNUVGMzlGNzRERSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpGRTlFQjY3M0NBMEQxMUU4OTE1Q0JBNUVGMzlGNzRERSIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxOCAoTWFjaW50b3NoKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOmY5NzFhMjMyLWM0N2MtNDgyZC1hNWNlLWM3NzI0M2EwMTMzMCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowMzgwMTE3NDA3MjA2ODExODA4M0UyRENBMjlEN0Y2NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pt+q0bsAABC3SURBVHja7J0LTJRXFsfRannKAA5vFFGhAoo8ViVYpYqIoq4ratWlq9XQpm5NVrM222bdrk3dxOyatN3VbJtqrFbXV31VpaJlrVqoj4quWKUUUUpRRARGYADRun85zc2Xb4YREBX0/4uZ3Pnu/c59nXPPPR/zXbvcvXvXjhDSPD1HLHxIkm8cfZ/DS8gjoCuHgBBCCF0dIYQQQldHCCGE0NURQgghdHWEEEIIXR0hhBBCV0cIIYTQ1RFCCKGrI4QQQujqCCGEELo6QgghhK6OEEIIoasjhBBC6OoIIYQQujpCCCF0dYQQQghdHSGEEEJXRwghhNDVEUIIIXR1hHRWfAwuHARC6OoIeTKprDWHBXonxIZxKAihqyPkicXf6Obl0QOBHdweR4OQp9fV1dfX5+XlPfZuow1oSbuIunz58uLFi0tLSx/xvUJ2dvbSpUupxx0hnluQ8oKX0VBWUY3ADulHprStVebHZYPbtm2Drj64zhPSzq7uv02oFVmAskpixYoVp0+fRu6+ffvwFWVUQspjFYZCoxjSIkdEHTp06LF3G21AS3SdraqqaoOoPn36yPLxiO8V/Pz8ampqbMxg2zpFWsWLI6OmjolBoqzcJJ99A4yLZiXA/7V5AwTHILoBi1u3bp1VpbWqzLgFlmhbfrvYIAxc7L2F5YcMGQJdfXCdV2D9QQNkFbLaGOo/UXSzsfU7cuQIEqNHjxbtXLBggWStXLkSaRTYvHlzVFRUeHg4LEfKqISswh9++OHMmTNxJScnRy5WVFRcu3ZNtDA9PT05ORnypbotW7ZcunRp8uTJoaGhSOfm5qamprq7u6O6fv36Xbx4cdCgQXPmzFFanpmZCctxcXGJi4v74YcftOV9fHwCAwNHjBixYcMGlHnttddwi6RRPiEhAdKmTJkibYDw2NjY9CYgYePGjZa3u7m5qebV1dUdOHAAAseOHevh4YFB0Nm/5AYFBZnNZtVsbQe1EtRytnDhQtSCRQ1l3nzzTRQWgegXJMyYMQNjqCSrcRBwu1yUWmQocF11CtNEXX94lJRX1ZgbvDx6RA7odaWsKi6q/66DpwaGBFRU1bZ5A3T8+PFJkyZduHABGhgcHIxFXJRWFAxqjH1kSkoKrEynzL179z7UhOjA7t27R44c2bdvX2UX/v7+YoMwIskVG2yt7qFq1Vq0aseOHUjAolVd0kKYJxI7d+5EO1UfP/nkE3zCFqCZyBVDw1exXzitwsJC1IjqVJNgTU5OTvicP3++CMG9G5vAXagFDUBj0AtpNgZN9F9aIjJRBk3Cp/qq6hJ7hBxLU1WjjVxq+5MW1UG5YQOYY/WsY2UTypPBaSHXtnRIgCeAGURHR2ujENgtVHDZsmViY2DPnj1QTegWbA8+DOklS5ZAiR0cHJA7fvx4FIYGy64NOzVkpaWlwY9CWmVlpa78xIkTsYWE/UybNg0y4XGRhoeT8rLuQ6GlDbAcuQL/LQnL27XNky7AM8Gk0Tvxo7owC8aJ1mqb3ZwE2R+gOvg5pIcNG6aitIiIiPj4eITFkABL00rW7V7VRe3Q+fr6ajtF2hHdzyzPF93zHGkpQyePCps9KfqFmMB3Fow/l//ThStlzd1iO+rCJ4wLfq6goADeCzs5FQyJGmCBhv7L4q5TZmw9cQVqg3mHskE/kaW1C/UkQOXK48S26Z5SexRGkyBNmgTbkRbKLhO2oPbK4OWXXxZbQBr2hcK4IrtMbTfRJCl//fr12tpao9E4ePBgXdWjRo2C41RPRFWzxROjUoyVyBRHJdfVV1WXskdLU1WjTc1/AqM6efyIBPZiAwYMEKW5cuWKcnswRai+benwZNCVvXv3FhcXawMRGAMUFCuy2ihhDytZECtPV3SKJbaqIk65gj2X1fJSGO1HO0Um9NXR0VHKa40EbdAGi83drm2eJOCZYPMQK8apQ7VWJaxKkARGeO7cuZLGUEMgPBZsG0sV9t2fffZZc+NgedHq0JF2pLLW/OLIqNlThiO9fmfWwVPfy8VN6cejwwNv1tSb6xp8PA2l101bj5xWd6UmDZ2aFHO1zLRqU2bW+Uvuzk62a5Hg7NixYwi/EGrA1dmYdJ0yY61XLhP6iXVfewtMT3yVZW7bdE9lbdu2TSm5tjwqgimpehUIxVQz1AZa9zc8WXnQJGdnZ5iJOCRd1RMmTCgvL0fU+MsEVVbqmt1ClD1amqrtvpNO7OokkoMZILF69WrEQ3Idux7Jwh4HqypUEHqm0wZ5OA4tgdnAX4ong7fTPR2NiYmBPUO4FEDhuCakXpW2vqf28cG9MGwkXnrpJewZLcuL4mI7Jq5o3759qEvKa9uArkGO5UZVd7u2SaqwlGnhHwO0HdRVh0Vqy5Ytyt3Ko5jU1FSYPfa52KqjhS2NNjS1kIfE2OcHJsdHfF9w2ctoQKxWaqqB8yspr4LnGxgSgAJn8ortmv6Ah084vOFhQX0DjJEhvvFDnss+XQBXZ1s+NNPJyQnKCRNLTk7GFbPZfN9blDIrVyf6iStqEbfUcF1u23RPnkBKyCi7Q8uKLH+Hgn0zyot9IRddwKKhglTlWlAG4wBLRPDn7e1ttS8zZsxYu3atyGmu2bq/Dtr4Y6ENUyWdl2es/n4PjqGsrCw2NrawsPDChQtQL3yeOHECrgu6iHT37t2hXocPH25sbKyursaVkJCQgICAbt26rVmzJj8/H+EaNAYeDou4yWRCYdkiHTx4sKSkxNfXd/fu3Tk5OdOnT5fnbHCiqBRmBgmwWElfvXoVGo9ikHznzh00AIlevXqhPMRCoeURq7pXlUf0hsZ7enpC79EGyPzmm2/efvttKQ/1hSh0BHd9/vnnWE2wh0VH0tPTDQZDbm6u5e2qSUgjF/2FnODgYHxFS3o1YTQa7z3LOn8eubgXcrTNxgJkKUFyo6Oju3btin0DtuTI7dGjB3am48aNQ7qoqCgjI8PDwwN9xIxoJcs46KpTtWAosCO+e/eudEoGmbSNv6/dL6FbfWMjEu++Osbby3gi98dtB046Ojy7dP7kWcmR4cGBg0N7Txsbae9gH+jXMymuf/KIsKED/fx6Gi8Wl928WTcyJshgcD117vKhnHzIwT/HZ7v/ad44q9aH2Rw9ejQsBRoLDcH+EvYliqTMDa5CEjplDg0NhdqIWkZERKxatUprF5GRkWKDyIICSy5UBcqGqtugewcOHMCyEBQUtH///hs3btjb2zs6Okqx27dvIyEVffTRR8iFNKRROyRjfYBZoWFiaAUFBfPmzXNpAl9FVFhYmJeXF0JbbAd//vlnyEQL4U1xHTeKIaM8akeTsFhhuLTNhuZv3boVpg0Tg8yzZ8/KcoHh0n6V3iEt/dKuRWKq2tG26mtJx6cLVsPO2G6EWbJPhILKDzratzwhip4j7j00S4x5zq7pFyhJcQP/ODcR6YLCy5knfyz8qTxyQK/hkb2++Do/JSE89a31yHrrlQn9/F23Hcgtq6iW3H59ArDne/3drSITcs4XXbtx9H0Ob6tANLakCZowaRVdO6m6BwYGit/CPu6+St/a8oRY4mU0xEb28ze6ZWSfO3QiH1cMrj1qzA0rd3x1Jq+4t79PSJDPl8cLK6pq4QudnexvNdSjJHKdHO2N7j0aG2//e0s2PJzI4Xi2DWxY09LSaMLkaYnqCHnEUZ1d068oB/X3h7uKGhAYF9Xf18vg4XR39+F7vxmZPnZQ1pli+LyyctPsKcNv1tSH93H9eGcOQroxw/qeK7yBK7sOnsK98IWlpl9+6MiojhBGdYR0LMTPIbD76/ykuAj/7Rmn/vbxIT8vt4WzR10suXmlrAqODQVyvisy1zX079tn8Zx4XEx9a33+pdLxzw/48yujRAiPhCakQ7s6Hk3USZGzJGx0WQ4Ya9XIqMK2Z7ODHAXXLngZDfBzv0mMsbe3//J4IQI7XHzjg+17D59Pjo/oH+iNqG7qmJiM7HM+ngZHR4cV6w6/tykTIaCPp9sXX+cNCh+A9MCQAA8350c2uWqaWnWGXAsntx0P2CPkMbg6Hk3UQh7X0USYDtSFLiCB2blvX9Tbss0VkFdlWzUyqrCaTaudldz7qkSn4HReUWxkv4nxYRWVJoRrUc95rV72u8SYe68QILf0+r1zUtKmxSXFDUQYZzLdRPlFsxL+ueS3ri4OBUXX6urqX58ZV1ZRLa+cP5rJ1b173sKe6ia3uekTw38yJpc82XSzoeg8mqjDHk2UlJSEKlDMaDTKL6TlBV5UiiztMWahoaF79uxRb8XKgHt7e6MLqte6A19kc2O144MHD1aDrE66kcPeIFmdQ+br66skS25mZqZSiU56ektlrRkuKsncAOoaGhG64d+uD14VF5h++GxKQrizk8ORU4WT44OLy29tP/i/iqpaFyf77wsuT1y0ckHKCyaT6VZDPQrbrig+Pl5NLlQIuwelnFpdlfPqtJOrDBCTq7Ms7eQuX768tZOrnT6tRYvhPwGTS57eqM6ORxN14KOJ5CV6ebEpLCwM5bF7QKW4KyMjQ3fKmmTJjejgzJkzxVVrTz7TbdUxR1Y7rh1kXUSoPVxNK1lytSrRea0lLNA7LWXop3tOxc19XyKz4+dKZk8Z7m90gzPbkfldeWU1Ln53+eb6nVlvfLB9VvKwqPDArDPFoX5eK3d8lZD2rwpzFzkY+r4RlZpcrXJCVbRjq5tcZYCWlqXALW2YXBsWjSY9GZNLnsaozo5HE2maZNchjybC2oRAVk4mQ3nx4liYtKeCyalpWgePfffq1atljrQnn2kly0FlUp2u41jXLAfZcp/UnOTODlyaqabOrumnmKWmGni7N/6xFf7sni45O727Jv3YmYsDQwIQ7WWdvwT3VlZRvT3j1MaME3K7h5tzrbmhJRXBFWFyEZEjpNMpp3ZsdZOrNUCdZSlOnjyJ/eKDTG5zkgnpfK6ORxN1/KOJoqOjMVBY3WRfgr6g5fjEKllUVKQ9ZU2ylHeXtiFottOcfKatRR1UZtlxSGtukHWjbVVyZ6ekvAohWkiQD5yWHOW8aFZCdHjgiCEh7ywYj6+3GuoRt00fO+gXtaxsEN+GkE48pbOTfeFP5ff3qf7+cDmI3nTKaXlenZpcrQHK5Fq1O8wdgv7p06c/yOTyDW7S6Wj2YDAeTdTBjyZClslkwmDiK+5CM+DsXV1dJ02ahDLaU9YwEd9++y2WMxTLysravHkz5giOUHvymapFe1AZFEDbcdwuf0TUDTJ0A+OJexEipKenoyIMuJKMUZW5RmtxLyrtdKeUycFgjs92v26qzc0rNrq5jIwJ6e3p0cfTfeq4X/X2cnZx7AavVlN/p66xC0oiIf88nO56ujs33rZz6PLMjMQhcdHBG3Zl7zp6Vkm2ejAY6N69O0Y+MTERA6hVTqPRqDuvTk0uNE0ZoEyu1u6U2Y4ZMwZWA72NjY1t+eSq6bO0aJSBQYm9d8bJJU8JfIW81fBooqcN9Qp5Za15eFjQsj+kBPm4GFx7nMm/iotHT+a/tynTenRucPnL/F/Lm+a9A/xMN6uzz5a8vvw/qgBfISfkcT7AJLYflvJooqcTd2en80XXfv/Op/J1UH9/L6OhrNwEF2j1/+UpNdXIewin84rkkBS+PE4IozpCOnRUpwMeTvyfjXttl2FURwijOkI6epDXLmUIIQ8bnoFJCCGEro4QQgihqyOEEELo6gghhBC6OkIIIYSujhBCCGkt/xdgAEI3dJZfmVUMAAAAAElFTkSuQmCC" width="567" /></div>
	<div class="tpl-content-highlight" style="width:560px;padding:40px 20px 0"><p style="font-family:Arial;font-size:12px;color:#6e6e6e">Aan de verstrekte gegevens kunnen geen rechten worden ontleend. De eigenaar van AERIUS aanvaardt geen aansprakelijkheid voor de inhoud van de door de gebruiker aangeboden informatie. Bovenstaande gegevens zijn enkel bruikbaar tot er een nieuwe versie van AERIUS beschikbaar is. AERIUS is een geregistreerd handelsmerk in Europa. Alle rechten die niet expliciet worden verleend, zijn voorbehouden.</p>
	</div>
    </td></tr></table>
</body>
</html>');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('NSL_MAIL_CONTENT_TEMPLATE', 'nl',
E'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<table border="0" width="600">
<tr><td>[MAIL_CONTENT]
<p style="font-family:Arial;font-size:14px;">Met vriendelijke groet,<br />
[MAIL_SIGNATURE]</p>
<p style="font-family:Arial;font-size:14px;">PS: Dit is een automatisch gegenereerde email; u kunt op deze mail niet reageren via \'beantwoorden\'.</p><br /></td></tr>
<tr><td>
<div style="width:560px;border:1px solid #b7c5c5;border-radius:15px;padding:8px 20px 0">
	<table style="border:0" width="560px">
		<tbody>
			<tr>
				<td colspan="2" style="padding-bottom:10px">
					<img alt="AERIUS" height="34" src="data:image/gif;base64,R0lGODlhjwAfANU/AKm5y+Hy+TKItZWmvGeFpZitwrTB0dLl7zdZg+Pt8yhNetLa5HOuzeDm7FVzl8LR3nWNqdHt98LN2UNki/H2+aOyxvDy9TlrlIqiu/r7/WV9noWbtB1FdEtrkc3W4R5pmcTi8MPc6cPX5FiCpBs7bJTU7EdojrPg8WbC5PD5/HbI5ki13imp2ZG/14XO6VWcwaPa73aYtVhvk16w1JvN4zmv2ypfi1e84Ul2nLTT5COTxRtUgxqj1h18rRlCcv///yH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAzODAxMTc0MDcyMDY4MTE4MDgzRTJEQ0EyOUQ3RjY2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkQ2MTY4Qjg0QzdBMTExRTJBRjAyQTNBREMzOTczOTEwIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkQ2MTY4QjgzQzdBMTExRTJBRjAyQTNBREMzOTczOTEwIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowMjgwMTE3NDA3MjA2ODExODIyQUZENkE3ODIxNkZEMCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowMzgwMTE3NDA3MjA2ODExODA4M0UyRENBMjlEN0Y2NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAEAAD8ALAAAAACPAB8AAAb/wJ9wSCwaj8ikcslsOp/QnwFB0lii2Kx2yy1uFk2I9VeZXLvotHrb4HSYFc1PQRrY1/i83ujw+QBLCA0/JBIKDQh7iotdBn4+ChRKJEIkdYSMmZpNGQ4InwgDShqidTISMpuqq1sWEwMVPxAKYKy2t00DVDK1uL6/UCU8w8QpSCA6ycrLOjRDIT3R0j0CLy0hRyIf2x9FDzvgO0gj4UcWcSaPJg4QTBQFGh0T8/T17BJZN8TEJ8fM/86EQJtGsMeLBEW0cfMWTtwRcuCMDFDwqKKfJYYsarTo4IyTFPuIqfD3b1nAHwMLThMgaYjCbQzLPZQ5ZMPGRw6UeOBws6cP/xMZnsAgtoIYC5LJQChdCiLAs2lDDjCYxoDIy25EvtEsAtGhEAkVIVSQQLbCnSQTfPoU5UQFsQj7IhxBpmxJyh5FWkB1yQ3rEK0RZwYWAuGRgSgL1Pqc8ITFsBo/ig5zMXeZ3b1DEkzDJuRqzMFct1L8iQWA4p4cnJwQ+cMF0cp1ldw1spnvwqwNx21VV9pngQ4+nbge1g/EPqdF6Ca7LI22tAO2YeLeSqQrkdE+DkMxfXNChsQ9ndQoJmRfCSPKdTCPViSHNAFW+372Ghp04UcQtDfhvvEB4fBMwDXMDUPoMwwK6JVk2VPNEfGCNFVF55cQgNFX3VZgWaRAfkzwZ/8RAUNYgJ1FTQgzzHlCmDiMMUSkVxIRswkh1XsISTifbqCRcRMCsSThYUUbFCBkAX1s1IRkPMglhIDEJadgbAKpRNBJnck3XY5DWFeEBEVq1A4SP55WERMgDXMUEY4NM1KLyzC1FIxSRvMCdAlZ+Vduglk4RAMDpGORfkWEKeZFSwx1YBEoGOUklEjcVVAL2dhJIZ5GaJlEAxpUlNMRgg7KRKIhhTqMkkKktx5eP2gmDWfx3XYndVnCekSmj4A5qJFLiKorZUOYKhtmekXDUp2uCtFAQ4MYYQM4ODSRIaFGdCrmEqvpGuoKbDJ6RIw/UCCANJC2Kh0RDRVgBGAxNOH/iB8m2HprRakp4dZkR6jIA3I/+JoEtz+495y4EwpBQDg29PLDAheE498SiDzClhEebKSABRRTLGgiSozXpBFM8oBivm26CQKdKGEmxINyAnxuQzuMIOTA4YxAxAKkjFXWLI8g4JERCGikQBHrWoSBEh0nkSYPCJb6ZDIzMMgeEapGE+4PnhkRA8tY25DsV2rRosSzj/xMRNCPTNASEiomfcS8Kyq9dNNRNkhEsNHQWbURGGAd89Zc9+QA30gYMKLYQ5D9U41JIAlDEoZu7OI/cJcs9xDeSvNClcUW0UABIyS8Aw4xLKy5Aw5gt84AgCuRAAaeIMAYERJ8MoEGAAS1AkQQADs=" width="143" /></td>
			</tr>
			<tr valign="top">
				<td style="font-family:Arial;font-size:12px;width:260px">
					<a href="https://www.infomil.nl/onderwerpen/lucht-water/luchtkwaliteit/slag/monitoren-nsl/handleiding/" style="color:#333;text-decoration:none">www.infomil.nl/onderwerpen/lucht-water/luchtkwaliteit/slag/monitoren-nsl/handleiding/</a><br />
					<a href="https://www.aerius.nl" style="color:#333;text-decoration:none">www.aerius.nl</a><br />
					<a href="http://twitter.com/AERIUSapp" style="color:#333;text-decoration:none">twitter.com/AERIUSapp</a><br />
					&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" style="font-family:Arial;color:#6e6e6e;font-size:12px;padding-top:8px">
					AERIUS is ontwikkeld in opdracht van de Rijksoverheid en de gezamenlijke provincies</td>
			</tr>
		</tbody>
	</table>
</div>
<div style="width:600px;text-align:center">
	<div class="tpl-content-highlight" style="width:560px;padding:40px 20px 0"><p style="font-family:Arial;font-size:12px;color:#6e6e6e">Aan de verstrekte gegevens kunnen geen rechten worden ontleend. De eigenaar van AERIUS aanvaardt geen aansprakelijkheid voor de inhoud van de door de gebruiker aangeboden informatie. Bovenstaande gegevens zijn enkel bruikbaar tot er een nieuwe versie van AERIUS beschikbaar is. AERIUS is een geregistreerd handelsmerk in Europa. Alle rechten die niet expliciet worden verleend, zijn voorbehouden.</p>
	</div>
    </td></tr></table>
</body>
</html>');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('MAIL_SUBJECT_TEMPLATE', 'nl', '[MAIL_SUBJECT]'); -- for now just the subject itself, we just want to provide the option to change the template

INSERT INTO i18n.messages (key, language_code, message) VALUES ('MAIL_SIGNATURE_DEFAULT', 'nl', 'Het AERIUS team');

INSERT INTO i18n.messages (key, language_code, message) VALUES ('ERROR_MAIL_SUBJECT', 'nl', 'Melding van AERIUS betreffende uw aangevraagde bestand');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('ERROR_MAIL_CONTENT', 'nl',
E'<p style="font-family:Arial;font-size:14px;">Geachte heer/mevrouw,</p>
<p style="font-family:Arial;font-size:14px;">U heeft op [CALC_CREATION_DATE] om [CALC_CREATION_TIME] uur een berekening in AERIUS gestart. Deze berekening kon niet worden voltooid.</p>

<table border="0" cellpadding="5">
<tr valign="top"><th style="font-family:Arial;font-size:14px;text-align:left">code</th><th style="font-family:Arial;font-size:14px;text-align:left">probleem</th><th style="font-family:Arial;font-size:14px;text-align:left">oplossing</th></tr>
<tr valign="top"><td><p style="font-family:Arial;font-size:14px;color:#ff0000">[ERROR_CODE]</p></td>
<td><p style="font-family:Arial;font-size:14px;color:#ff0000"">[ERROR_MESSAGE]</p></td>
<td><p style="font-family:Arial;font-size:14px;">[ERROR_SOLUTION]</p></td></tr>
</table>
<p style="font-family:Arial;font-size:14px;">Deze melding is ook bekend bij de beheerder van AERIUS. </p>
<p style="font-family:Arial;font-size:14px;">Heeft u nog vragen naar aanleiding van deze melding, lees dan eerst de <a href="#">veelgestelde vragen</a> over berekeningen. Uiteraard kunt u ook contact opnemen met de <a href="[BIJ12_HELPDESK_URL]">helpdesk Stikstof en Natura 2000</a>.
</p>');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('NSL_ERROR_MAIL_SUBJECT', 'nl', 'Melding van AERIUS betreffende uw aangevraagde bestand');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('NSL_ERROR_MAIL_CONTENT', 'nl',
E'<p style="font-family:Arial;font-size:14px;">Geachte heer/mevrouw,</p>
<p style="font-family:Arial;font-size:14px;">U heeft op [CALC_CREATION_DATE] om [CALC_CREATION_TIME] uur een berekening in AERIUS gestart. Deze berekening kon niet worden voltooid.</p>

<table border="0" cellpadding="5">
<tr valign="top"><th style="font-family:Arial;font-size:14px;text-align:left">code</th><th style="font-family:Arial;font-size:14px;text-align:left">probleem</th><th style="font-family:Arial;font-size:14px;text-align:left">oplossing</th></tr>
<tr valign="top"><td><p style="font-family:Arial;font-size:14px;color:#ff0000">[ERROR_CODE]</p></td>
<td><p style="font-family:Arial;font-size:14px;color:#ff0000"">[ERROR_MESSAGE]</p></td>
<td><p style="font-family:Arial;font-size:14px;">[ERROR_SOLUTION]</p></td></tr>
</table>
<p style="font-family:Arial;font-size:14px;">Deze melding is ook bekend bij de beheerder van AERIUS. </p>
<p style="font-family:Arial;font-size:14px;">Heeft u nog vragen naar aanleiding van deze melding, lees dan eerst de <a href="#">veelgestelde vragen</a> over berekeningen. Uiteraard kunt u ook contact opnemen met de helpdesk luchtwaliteit van Infomil, te bereiken via Luchtkwaliteit@RWS.nl.
</p>');


-- Default email stuff
INSERT INTO i18n.messages (key, language_code, message) VALUES ('DEFAULT_FILE_MAIL_SUBJECT', 'nl', 'Uw AERIUS aangevraagde bestand');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('DEFAULT_FILE_MAIL_CONTENT', 'nl',
E'<p style="font-family:Arial;font-size:14px;">Geachte heer/mevrouw,</p>
<p style="font-family:Arial;font-size:14px;">U heeft op [CALC_CREATION_DATE] om [CALC_CREATION_TIME] uur een export in AERIUS gestart. Deze export is klaar en u kunt het bestand tot uiterlijk 3 dagen na het starten van de export ophalen.</p>
<div style="text-align:center"><a href="[DOWNLOAD_LINK]" style="display:inline-block;font-family:Arial;font-size:14px;background:linear-gradient(#DBE1E1, #B8C6C5) repeat scroll 0 0 transparent;width:auto;padding:10px 40px 0px;border:1px solid #4c4c4c; -moz-border-radius: 2px;border-radius:2px;box-shadow: 0 1px 0 #FFFFFF inset;color: #333333;height: 33px;text-align: center;text-shadow: 0 1px 0 white;text-decoration:none;font-weight:bold">Bestand ophalen</a></div>
<p style="font-family:Arial;font-size:14px;">Heeft u nog vragen, naar aanleiding van de export of over AERIUS, bekijk dan eerst de <a href="[MANUAL_URL]">handleiding</a> of onze website <a href="https://www.aerius.nl">AERIUS.nl</a>. Uiteraard kunt u ook contact opnemen met de <a href="[BIJ12_HELPDESK_URL]">helpdesk Stikstof en Natura 2000</a>.</p>');

-- email for PAA (PDF Export)
INSERT INTO i18n.messages (key, language_code, message) VALUES ('PAA_MAIL_SUBJECT', 'nl', 'Uw AERIUS berekening: [PROJECT_NAME]');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('PAA_MAIL_CONTENT', 'nl',
E'<p style="font-family:Arial;font-size:14px;">Geachte heer/mevrouw,</p>
<p style="font-family:Arial;font-size:14px;">U heeft op [CALC_CREATION_DATE] om [CALC_CREATION_TIME] uur een berekening in AERIUS gestart. Deze berekening is klaar en u kunt het PDF-bestand tot uiterlijk 3 dagen na het starten van de berekening ophalen.</p>
<div style="text-align:center"><a href="[DOWNLOAD_LINK]" style="display:inline-block;font-family:Arial;font-size:14px;background:linear-gradient(#DBE1E1, #B8C6C5) repeat scroll 0 0 transparent;width:auto;padding:10px 40px 0px;border:1px solid #4c4c4c; -moz-border-radius: 2px;border-radius:2px;box-shadow: 0 1px 0 #FFFFFF inset;color: #333333;height: 33px;text-align: center;text-shadow: 0 1px 0 white;text-decoration:none;font-weight:bold">Bestand ophalen</a></div>
<p style="font-family:Arial;font-size:14px;">Het PDF-bestand kunt u gebruiken om de bronnen van uw AERIUS berekening te importeren in andere geodata systemen. Daarnaast kunt u het bestand importeren in AERIUS om verder te rekenen of om uw bronnen aan te passen.</p>
<p style="font-family:Arial;font-size:14px;">Heeft u nog vragen, naar aanleiding van de berekening of over AERIUS, bekijk dan eerst de <a href="[MANUAL_URL]">handleiding</a> of onze website <a href="https://www.aerius.nl/">AERIUS.nl</a>. Uiteraard kunt u ook contact opnemen met de <a href="[BIJ12_HELPDESK_URL]">Helpdesk Stikstof en Natura 2000</a>.</p>');

-- email for CSV
INSERT INTO i18n.messages (key, language_code, message) VALUES ('CSV_MAIL_SUBJECT', 'nl', 'Uw AERIUS aangevraagde bestand [AERIUS_REFERENCE]');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('CSV_MAIL_SUBJECT_JOB', 'nl', 'Uw AERIUS aangevraagde berekening [JOB] ([AERIUS_REFERENCE])');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('CSV_MAIL_CONTENT', 'nl',
E'<p style="font-family:Arial;font-size:14px;">Geachte heer/mevrouw,</p>
<p style="font-family:Arial;font-size:14px;">U heeft op [CALC_CREATION_DATE] om [CALC_CREATION_TIME] uur een berekening in AERIUS gestart. Deze berekening is klaar en u kunt het CSV-bestand tot uiterlijk 3 dagen na het starten van de berekening ophalen.</p>
<div style="text-align:center"><a href="[DOWNLOAD_LINK]" style="display:inline-block;font-family:Arial;font-size:14px;background:linear-gradient(#DBE1E1, #B8C6C5) repeat scroll 0 0 transparent;width:auto;padding:10px 40px 0px;border:1px solid #4c4c4c; -moz-border-radius: 2px;border-radius:2px;box-shadow: 0 1px 0 #FFFFFF inset;color: #333333;height: 33px;text-align: center;text-shadow: 0 1px 0 white;text-decoration:none;font-weight:bold">Bestand ophalen</a></div>
<p style="font-family:Arial;font-size:14px;">Heeft u nog vragen, naar aanleiding van de berekening of over AERIUS, bekijk dan eerst de <a href="[MANUAL_URL]">handleiding</a> of onze website <a href="https://www.aerius.nl">AERIUS.nl</a>. Uiteraard kunt u ook contact opnemen met de <a href="[BIJ12_HELPDESK_URL]">helpdesk Stikstof en Natura 2000</a>.</p>');

-- email for GML (uses default subject)
INSERT INTO i18n.messages (key, language_code, message) VALUES ('GML_MAIL_SUBJECT', 'nl', 'Uw AERIUS aangevraagde bestand [AERIUS_REFERENCE]');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('GML_MAIL_SUBJECT_JOB', 'nl', 'Uw AERIUS aangevraagde berekening [JOB] ([AERIUS_REFERENCE])');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('GML_MAIL_CONTENT', 'nl',
E'<p style="font-family:Arial;font-size:14px;">Geachte heer/mevrouw,</p>
<p style="font-family:Arial;font-size:14px;">U heeft op [CALC_CREATION_DATE] om [CALC_CREATION_TIME] uur een berekening in AERIUS gestart. Deze berekening is klaar en u kunt het GML-bestand tot uiterlijk 3 dagen na het starten van de berekening ophalen.</p>
<div style="text-align:center"><a href="[DOWNLOAD_LINK]" style="display:inline-block;font-family:Arial;font-size:14px;background:linear-gradient(#DBE1E1, #B8C6C5) repeat scroll 0 0 transparent;width:auto;padding:10px 40px 0px;border:1px solid #4c4c4c; -moz-border-radius: 2px;border-radius:2px;box-shadow: 0 1px 0 #FFFFFF inset;color: #333333;height: 33px;text-align: center;text-shadow: 0 1px 0 white;text-decoration:none;font-weight:bold">Bestand ophalen</a></div>
<p style="font-family:Arial;font-size:14px;">Het GML-bestand kunt u gebruiken om de bronnen en resultaten van uw AERIUS berekening te importeren in andere geodata systemen. Daarnaast kunt u het bestand importeren in AERIUS om verder te rekenen of om uw bronnen aan te passen.</p>
<p style="font-family:Arial;font-size:14px;">Heeft u nog vragen, naar aanleiding van de berekening of over AERIUS, bekijk dan eerst de <a href="[MANUAL_URL]">handleiding</a> of onze website <a href="https://www.aerius.nl">AERIUS.nl</a>. Uiteraard kunt u ook contact opnemen met de <a href="[BIJ12_HELPDESK_URL]">helpdesk Stikstof en Natura 2000</a>.</p>');

-- email for NSL Exports
INSERT INTO i18n.messages (key, language_code, message) VALUES ('NSL_MAIL_SUBJECT', 'nl', 'Uw AERIUS aangevraagde bestand [AERIUS_REFERENCE]');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('NSL_MAIL_CONTENT', 'nl',
E'<p style="font-family:Arial;font-size:14px;">Geachte heer/mevrouw,</p>
<p style="font-family:Arial;font-size:14px;">U heeft op [CALC_CREATION_DATE] om [CALC_CREATION_TIME] uur een berekening in AERIUS rekentool lucht gestart. Deze berekening is klaar en u kunt betreffende exportbestanden tot uiterlijk 3 dagen na het starten van de berekening ophalen.</p>
<div style="text-align:center"><a href="[DOWNLOAD_LINK]" style="display:inline-block;font-family:Arial;font-size:14px;background:linear-gradient(#DBE1E1, #B8C6C5) repeat scroll 0 0 transparent;width:auto;padding:10px 40px 0px;border:1px solid #4c4c4c; -moz-border-radius: 2px;border-radius:2px;box-shadow: 0 1px 0 #FFFFFF inset;color: #333333;height: 33px;text-align: center;text-shadow: 0 1px 0 white;text-decoration:none;font-weight:bold">Bestand ophalen</a></div>
<p style="font-family:Arial;font-size:14px;">Heeft u nog vragen naar aanleiding van de berekening of over AERIUS lucht, dan kunt u contact opnemen met de helpdesk luchtkwaliteit van Infomil, te bereiken via Luchtkwaliteit@RWS.nl.</p>');
