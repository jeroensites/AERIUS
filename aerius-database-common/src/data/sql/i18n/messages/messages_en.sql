-- email constants
INSERT INTO i18n.messages (key, language_code, message) VALUES ('CONNECT_APIKEY_CONFIRM_SUBJECT', 'en', 'Uw AERIUS Connect API key');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('CONNECT_APIKEY_CONFIRM_BODY', 'en',
E'<p style="font-family:Arial;font-size:14px;">Geachte heer/mevrouw,</p>
<p style="font-family:Arial;font-size:14px;">U heeft op [CALC_CREATION_DATE] om [CALC_CREATION_TIME] uur een API key aangevraagd voor AERIUS Connect.
De API key is gegenereerd en kan direct worden gebruikt om toegang te krijgen tot de Connect services.</p>
<p style="font-family:Arial;font-size:14px;">Uw API key is: <b>[CONNECT_APIKEY]</b></p>');

INSERT INTO i18n.messages (key, language_code, message) VALUES ('MAIL_CONTENT_TEMPLATE', 'en',
E'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<table border="0" width="600">
<tr><td>[MAIL_CONTENT]
<p style="font-family:Arial;font-size:14px;">With kind regards,<br />
[MAIL_SIGNATURE]</p>
<p style="font-family:Arial;font-size:14px;">PS: This email has been automatically generated; you cannot use \'Reply\' to send a reaction to this mail.</p><br /></td></tr>
<tr><td>
<div style="width:560px;border:1px solid #b7c5c5;border-radius:15px;padding:8px 20px 0">
	<table style="border:0" width="560px">
		<tbody>
			<tr>
				<td colspan="2" style="padding-bottom:10px">
					<img alt="AERIUS" height="34" src="data:image/gif;base64,R0lGODlhjwAfANU/AKm5y+Hy+TKItZWmvGeFpZitwrTB0dLl7zdZg+Pt8yhNetLa5HOuzeDm7FVzl8LR3nWNqdHt98LN2UNki/H2+aOyxvDy9TlrlIqiu/r7/WV9noWbtB1FdEtrkc3W4R5pmcTi8MPc6cPX5FiCpBs7bJTU7EdojrPg8WbC5PD5/HbI5ki13imp2ZG/14XO6VWcwaPa73aYtVhvk16w1JvN4zmv2ypfi1e84Ul2nLTT5COTxRtUgxqj1h18rRlCcv///yH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAzODAxMTc0MDcyMDY4MTE4MDgzRTJEQ0EyOUQ3RjY2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkQ2MTY4Qjg0QzdBMTExRTJBRjAyQTNBREMzOTczOTEwIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkQ2MTY4QjgzQzdBMTExRTJBRjAyQTNBREMzOTczOTEwIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowMjgwMTE3NDA3MjA2ODExODIyQUZENkE3ODIxNkZEMCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowMzgwMTE3NDA3MjA2ODExODA4M0UyRENBMjlEN0Y2NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAEAAD8ALAAAAACPAB8AAAb/wJ9wSCwaj8ikcslsOp/QnwFB0lii2Kx2yy1uFk2I9VeZXLvotHrb4HSYFc1PQRrY1/i83ujw+QBLCA0/JBIKDQh7iotdBn4+ChRKJEIkdYSMmZpNGQ4InwgDShqidTISMpuqq1sWEwMVPxAKYKy2t00DVDK1uL6/UCU8w8QpSCA6ycrLOjRDIT3R0j0CLy0hRyIf2x9FDzvgO0gj4UcWcSaPJg4QTBQFGh0T8/T17BJZN8TEJ8fM/86EQJtGsMeLBEW0cfMWTtwRcuCMDFDwqKKfJYYsarTo4IyTFPuIqfD3b1nAHwMLThMgaYjCbQzLPZQ5ZMPGRw6UeOBws6cP/xMZnsAgtoIYC5LJQChdCiLAs2lDDjCYxoDIy25EvtEsAtGhEAkVIVSQQLbCnSQTfPoU5UQFsQj7IhxBpmxJyh5FWkB1yQ3rEK0RZwYWAuGRgSgL1Pqc8ITFsBo/ig5zMXeZ3b1DEkzDJuRqzMFct1L8iQWA4p4cnJwQ+cMF0cp1ldw1spnvwqwNx21VV9pngQ4+nbge1g/EPqdF6Ca7LI22tAO2YeLeSqQrkdE+DkMxfXNChsQ9ndQoJmRfCSPKdTCPViSHNAFW+372Ghp04UcQtDfhvvEB4fBMwDXMDUPoMwwK6JVk2VPNEfGCNFVF55cQgNFX3VZgWaRAfkzwZ/8RAUNYgJ1FTQgzzHlCmDiMMUSkVxIRswkh1XsISTifbqCRcRMCsSThYUUbFCBkAX1s1IRkPMglhIDEJadgbAKpRNBJnck3XY5DWFeEBEVq1A4SP55WERMgDXMUEY4NM1KLyzC1FIxSRvMCdAlZ+Vduglk4RAMDpGORfkWEKeZFSwx1YBEoGOUklEjcVVAL2dhJIZ5GaJlEAxpUlNMRgg7KRKIhhTqMkkKktx5eP2gmDWfx3XYndVnCekSmj4A5qJFLiKorZUOYKhtmekXDUp2uCtFAQ4MYYQM4ODSRIaFGdCrmEqvpGuoKbDJ6RIw/UCCANJC2Kh0RDRVgBGAxNOH/iB8m2HprRakp4dZkR6jIA3I/+JoEtz+495y4EwpBQDg29PLDAheE498SiDzClhEebKSABRRTLGgiSozXpBFM8oBivm26CQKdKGEmxINyAnxuQzuMIOTA4YxAxAKkjFXWLI8g4JERCGikQBHrWoSBEh0nkSYPCJb6ZDIzMMgeEapGE+4PnhkRA8tY25DsV2rRosSzj/xMRNCPTNASEiomfcS8Kyq9dNNRNkhEsNHQWbURGGAd89Zc9+QA30gYMKLYQ5D9U41JIAlDEoZu7OI/cJcs9xDeSvNClcUW0UABIyS8Aw4xLKy5Aw5gt84AgCuRAAaeIMAYERJ8MoEGAAS1AkQQADs=" width="143" /></td>
			</tr>
			<tr valign="top">
				<td style="font-family:Arial;font-size:12px;width:260px">
					<a href="[BIJ12_HELPDESK_URL]" style="color:#333;text-decoration:none">www.bij12.nl/onderwerpen/stikstof-en-natura2000/helpdesk (dutch only)/</a><br />
					<a href="http://www.aerius.nl/en" style="color:#333;text-decoration:none">www.aerius.nl</a><br />
					<a href="http://twitter.com/AERIUSapp" style="color:#333;text-decoration:none">twitter.com/AERIUSapp</a><br />
					&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" style="font-family:Arial;color:#6e6e6e;font-size:12px;padding-top:8px">Development of AERIUS is commissioned by the Dutch government.</td>
			</tr>
		</tbody>
	</table>
</div>
<div style="width:600px;text-align:center">
	<img alt="Ministerie van Landbouw, Natuur en Voedselkwaliteit" height="51" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAk4AAAAzCAIAAADuGBCUAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3BpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowMzgwMTE3NDA3MjA2ODExODA4M0UyRENBMjlEN0Y2NiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDozRUQzMURDRENBMkMxMUU4OTE1Q0JBNUVGMzlGNzRERSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDozRUQzMURDQ0NBMkMxMUU4OTE1Q0JBNUVGMzlGNzRERSIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxOCAoTWFjaW50b3NoKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjU3ZmMwYzQyLTQxMDQtNDdhYy1hZTg3LWU0MGMzYTdkODViMiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowMzgwMTE3NDA3MjA2ODExODA4M0UyRENBMjlEN0Y2NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PvD0gmUAABDSSURBVHja7J0NTFRXFsex1QIDMkgHREAQFIqglo+CBCuUUkVQ14Vqratra0ObsjVZm22zNutmbeomm12TtrtusGmN/bBblfpBW+mitVZbSdWArB+IFEGKKCICIzB8ady/nObm9c0wKIIV+f9iJve9d8959953zj33PGauw65fv+5ACOmZB6evGCDNl799i8NLyB3gPg4BIYQQhjpCCCGEoY4QQghhqCOEEEIY6gghhBCGOkIIIYShjhBCCGGoI4QQwlBHCCGEMNQRQgghDHWEEEIIQx0hhBDCUEcIIYQw1BFCCCEMdYQQQhjqCCGEEIY6QgghhKGOEEIIYagjhBBCGOoIGax4G105CIQw1BFyb9LYagkLGJ0cF8ahIIShjpB7Fl+Tu5fHSCR2CHscDUKGbqhrb28vLS29q7QVFBSsXr16oEV0nD179pVXXqmtrbVT5+jRo6u7aWpquiXBAWoPsZ/PLc94zMtkrGtoRmKHcr/fBaYOg7/587eETWO7Te+jUZHBEeq+7kaZrABnkMLatWvhHri6a9cuHKKOKkh9xANYOaqhLHpE1b59+/pxgtZpg/uhGbekxMfHp6WlRdvrXr1dJ9IHxo0bJ621U+fjjz9+9tlnMeDu7u6qX70KqoeFkYeIzZrWfbyZ9pCeeCoh8sknolGoqzfLZ5Cf6eVFyYh/fV525OTkyBOBAXzwwQcow9Rxybq+7nwfXEBnbBI+s7Ozb9X7ejLym/EpOzEYo4H2yLDoRuB2NJMhxXA7S7YDBw6g8Pjjj4vJLl++XC6tW7cOZVTYvHlzZGRkeHg4LF7qqILEg/Xr1z/99NM4U1RUJCcbGhouXrwoNpqXl5eWlgb9OIQzHz9+fPLkyQsXLnRycpLMaffu3SjMnDnzhx9+wNXFixfjdjD9vXv3ItK4urrGx8frtLW1tYn7JScnb9myRaRGjRqFNnt7ewcEBEyfPn3Tpk0Qf/HFF+HVUkfrV3ndQArOLyIWi0W1DXW0IhgEHFZWVs6bNw+3lgYHBgZC5MyZMxB55plntF3DOKhOqYlsxYoVMr9gZSBtgza0WYZaeo0u7+sGhzj//vvvo1pGRgZGQEmhR2iwelgof/755xs3bkxNTYUeTBMVFRXbt2+HlPQxKSlJjRJOQurTTz+FNmk2fePmqalvarF0eHmMjAgde76uKT5yws49hZNC/BqaWvu8DDp06NDcuXNPnTqFJxscHIwpHhaVnp4ufgHjx5PCU4OXyXlxgfHjx/v7+ytTERv29PSEia5cuRLPGpaZlZUFw4Nt4BZ41ikpKbLEUcYmjgzPwi0gjs8dO3bgLlDe2toqJ+EXvr6+4n3KAmHVaBtCJuxTrPFGytvYKPYmqpQdSkHr4+JH0gAFDtGLL774QuYfDI5yKAzLTWpWswdtlVmdHkzKCQkJ8Cj1jmJdNyqSIWjhqn3t0IBwCOeMiorS5kPwWxjomjVrJM6BxMREZCGwSPW6Q2oiDMBSYdOrVq2CxeMqPjMzMxFBcdVaG+IuZGfPno3YoKQkds6ZMycmJgY+OX/+fHgUwjC8HXVUCBe/kjghBRHRtk0nIofQlpubK43BhIKaiC5oDwpom1ZcdSooKEgmFOiXOAfQJLQNM4XMUNqWSL+gRw5RB4MAr0ZZ2yPt4KPXmCsxQ6mojLgoAyuaMUrSfqiVk9Cjmk3fsIPua5YlVTdm/MyM2HlJYUvnRj0WHfD68tQTZedOna/rScR+toRPOBfiXHl5OQIMXEBlSGJCmM3FANR5cQGEMZ2pwIanTp0KEYlncXFxYrcIVKiDp5+fn6+WRyIiPgjLLCkpEQODFJTDlpYsWaL8Qr3bELuF5aOMM9Cmbgdk0QblaLPWDlVflI+LH9kcExcXF7VcVg6lLLlXzWr2oOkyq9Mjrx9RgH2HhoaKSZ0/f16FPbiiGLcdEHtgc1iRVVdXaxMFOANSCtif5CWy9ENKYa0BYeB4NzKtq0kcKZQKtFptYv0SRfCpkxI3RuNlKsGqVhvnrIEIlCPVU23TieBQCqoxchdtwbprKrZhbJctW6YmOJkm5LBXtxT9kNL2yGZNdTtrVHfUfVWziU0aWy1PJUQuTZ+G8oc7Du4pPC0nP8k7FBUecKWl3dLW4e1prL1k3nrgqJJanBL7ZEr0hTrzvz/Ze7CkcpSLwf5dJDn7/vvvkTZJXtKTASjEBZDrIBJY2zBWTnv27Ll06ZIEM/Xc4S9aHxEKCwvPdAMpuDDM0tnZWUKX3FR0SkjW2q3yvltC+bgdG5Y3Ez05VK+arftIGOoc5E29LAxReO+995KTk+U8chG5hBUlYgkyJ2QGOt+TV+cwWfgJ4qVEMkQ73dvR6Oho+DOUyys4rLmwbISv2lhBe3vHd6NmAVTDSSwwxf202lQc0klpJ32sASVnQgfhNrLq7Ald2yCoFdHexWYmhMUB3KynrmF62rJliywCpEm4HbqD0VOxp1dn1vZIN8hYvOMWUsfm3+EghRaqaYvcDDMfnZSWOOV0+VkvkxG5Wq25BcGvpr4JkW9SiB8qFJdWO3T/AQ+fCHjTwgKD/EwRIWMSYx4qOFqOUGdfP56UwWCAScPF0tLScMZisfQqgvrwU5iZLtQJuAQzlrff4kTy3PGJnE+nCveFHrgzPuHvSOA2b94Mm4S49bJJzsB+xJYcevuLb092aO2tupe6sGQ0zM5c0TfNZIhwv81vEu7atauuri4uLq6iouLUqVOwZnwePnwYoQs2h/KIESMWLly4f//+rq6u5uZmnAkJCfHz8xs+fPiGDRvKysqQYMHIEOEwlZvNZlSW9RqWljU1NWPGjMnNzS0qKlqwYAHKkKqqqsrPz/fw8EA0NZlMN14KlZRALaTgumgPAsaFCxeQX27btg2GLi9XrbUFBgZCFRo/a9YsJQWnxVUkguiRp6envPrHTcX/y8vLR48eHRwcLKEaPcrLyzMajVgMQuSRRx7Rti02NlYrEhYWJneBNlRGg6EfghiKa9euYcQmTpzY2dmpxNEw6ZRcjYqKuu+++7BiwGIcd5e2Qflzzz1XX1+PCtAzduxYWUagGRhMpf/q1asoTJkyBc1QPUL3Mekc7gb1cUmWKbgj6hw7dgwFNAO927p1K3oqo7R79+6AgABts9V9Cfj7xv9K6tbe1YXCGy88MdrLdPj4jzm7jzg7PbA6a96itIjw4ICHJ/rPnxnh6OQY4PNgSvyEtOlhsZN8fB40namuu3KlLSE60Gh0Kzxxdl9RGfTgn/MDI/743Cyb3odngXQKtg07x+NAvIF/yXNR7oa4IgWch1dC6rPPPkNohMlpTQVmGRERMWzYMDz99PR0GAlugYcLqU2bNrm5uc2dO/fcuXPqoaMAtbAr5Goow0hOnjy5cuVKtASOJtYlOsX7xAJxu+LiYrgqmgdTFGsXDVCLqUDsDQavtUPlDsrH0Tz5yzo8S723gBJ0EzfCnIBmaP1RWXKvmmX2kO6TIciw69evD64WIxWTl3uwY/WFDkIGjgenr8DnjOiHHLq/gZISP+kPy2agXF5xdu+RHyvO1UeEjp0WMfbL78oyksMXv/YhLr32/Ozxvm45u4/XNTTL1fHj/LDme+mNraITekqqLl7+9q0704WcnBxfX9++JTfyNRCVDmZlZQ1oU5GcreqGrk36kUH2E3K4AfIPiXNY39EZyB3Dy2SMixjva3LPLzix73AZzhjdRrZYOtZt/6a4tNrf1zsk0PurQxUNTa2IhS4Gx86OdtTEVYOzo2nUyK6uq9lbChDhRM+dbHl2drbBYOjzSzykevJKBnEuKSlpoFuLhWxmZiZdmwz1rI6QXySrc+j+FuXkCb4IV5GhAfGRE8Z4GT0M13P33/jOyIKZkw8WVyPm1dWbl6ZPu9LSHj7O7d0dRUjpnpgadKLiMs7s3FMIWcTCWvNPP8q8Y1kdIczqCCE3hcQ5JHZ/yUqJn+K7Lb/wr+/u8/FyX7E06UzNlfN1TQhsqFB0ssrS1jEhaNwrzyTi5OLXPiyrrE19NPRPzyeJEm4JTchdHeq4NdEdIycn53b2HrsdcbXtWb881nsJL5MRce7XM6IdHR2/OlSBxA4nX3172xf7S9ISp0wIGI2s7sknovMLTnh7Gp2dndZ+sP/NT/YiBfT2dP/yu9LJ4aEoTwrx83B3oXkQ8suHOm5NpIvBd35ropiYGLX3mHZjNnkotySungjGFkPaa+BXPw2Wx9q3p3lPcrS0Ki5i/JzEsIZGM9K1yIe83lvz2xnRN35CgKu1l27sk5I5Pz4lfhLSOLP5Cuq/vCj5n6t+4+bqVF51sa2t/aWn4+samuUn5/eGeXBrLjIoGG4nQnBrIqHftybCAMpvWtPS0oKCgmTTMhlM+ZWhdFb3OHB3+U65dv8wKLQ+1Ik7dG9mhqZiLsOt169fj/FRW4WhkbKU0W5+JhOoPFbZkwxnjhw5EhgY6ND9M6+B/hreXUhjqwUhKsXSAdo6upC64d/Ot1+QEJi3/1hGcriLwelAYcW8xODq+s5te/7X0NTqanA8XX52zsvrlmc8ZjabOzvaUbnXtGmwmMfDDz8s9o9LOgfn3EoGR1bnwK2Jfk4/bk2ESxDH6GHEpONovHajr9TUVJvbuGAmmj17tnb/MNxId2gtjkU3mip7s6F5ajmvGulga2M29VjVXmsJCQkGg8FkMskTHIKEBYzOzIj96PPC+GVvSWZ26ETN0vRpviZ3BLPte0/WNzbj5MmzVz7ccfDVt7ctSpsaGR5wsLh6oo/Xuu3fJGf+q8EyTDaGtp82DRbzUPZv7eCEDIKszoFbE1nRj1sTofE5OTlqUzHtYMpGXza3L5G7y+yj3T9Md2gtLu+XRL92DH+WstxEjzAbYrWBJ6J2zxlqIKSZW9ocur+KWWtuQbR79R9bEc9uLKdcDG9syPu++MykED9kewdLKhHe6hqat+UXfpx/WMQ93F1aLR293mUwmkdoaKg4OHI7lfkRcreHOm5NZPOlbn9tTYSVtWSlKnbq+mLz7yWyMtDtH6Y7tCmOluOSbNKGxBRrEZGCiNoX1M7GbNq2QRZZ3ZDdJ7OmvgkpWkigN4KWbOX88qLkqPCA6TEhry9PxWFnRzvytgUzJ/8UWho7JLYhpZNI6WJwrDhXb/8ug9Q8xMHhI9xGldyF9LgxGLcmGritieS//rl8+bKjoyOyVeuNvt555x1chX7ZrkwagDqYhiCu3T8Mk5f2EHEIhzpxWXRLGzo7O5csWQIlslWYtAE3Re6u271MPW4okb3WcAbJPYZ9qP28VzYGc35gxCVz6/HSapO7a0J0iL+nxzjPUU/OesTfy8XVeTiiWkv7tbauYTdeobdfk38ehuueo1y6rjo4Dbt/4YyY+KjgTTsLdn57TGm2uTHY4DIP5VmxsbFff/01gjR//U3uQu7cT8i5NdEvjnxvqM//cRcW/gh1Q/C/slM/IW9stUwLC1zz+4xAb1ej28jisgs4+e2Rsjc/2WtT0Nvo+uesX8kvzf39fMxXmguO1bz0t/+oCnfVT8hpHmTIvcDsd7Kzs/39/W9na6LKykqkaNya6HZAmo6ENTc3F2v8BQsW3Oo8ePr0afVfDg1NRrkYSqou/u71j+Rw8gRfL5Oxrt6MEGjz/+WpNbfI7xCOllbJJil384/HaR6EWR0hQxeV1elAhJP4Z0fWfh1uDEbIPZXVEXJPJnn9UocQMtBwD0xCCCEMdYQQQghDHSGEEMJQRwghhDDUEUIIIQx1hBBCyK3yfwEGAE5nkL9v42BmAAAAAElFTkSuQmCC" width="567" /></div>
	<div class="tpl-content-highlight" style="width:560px;padding:40px 20px 0"><p style="font-family:Arial;font-size:12px;color:#6e6e6e">Aan de verstrekte gegevens kunnen geen rechten worden ontleend. De eigenaar van AERIUS aanvaardt geen aansprakelijkheid voor de inhoud van de door de gebruiker aangeboden informatie. Bovenstaande gegevens zijn enkel bruikbaar tot er een nieuwe versie van AERIUS beschikbaar is. AERIUS is een geregistreerd handelsmerk in Europa. Alle rechten die niet expliciet worden verleend, zijn voorbehouden.</p>
	</div>
    </td></tr></table>
</body>
</html>');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('NSL_MAIL_CONTENT_TEMPLATE', 'en',
E'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<table border="0" width="600">
<tr><td>[MAIL_CONTENT]
<p style="font-family:Arial;font-size:14px;">Met vriendelijke groet,<br />
[MAIL_SIGNATURE]</p>
<p style="font-family:Arial;font-size:14px;">PS: Dit is een automatisch gegenereerde email; u kunt op deze mail niet reageren via \'beantwoorden\'.</p><br /></td></tr>
<tr><td>
<div style="width:560px;border:1px solid #b7c5c5;border-radius:15px;padding:8px 20px 0">
	<table style="border:0" width="560px">
		<tbody>
			<tr>
				<td colspan="2" style="padding-bottom:10px">
					<img alt="AERIUS" height="34" src="data:image/gif;base64,R0lGODlhjwAfANU/AKm5y+Hy+TKItZWmvGeFpZitwrTB0dLl7zdZg+Pt8yhNetLa5HOuzeDm7FVzl8LR3nWNqdHt98LN2UNki/H2+aOyxvDy9TlrlIqiu/r7/WV9noWbtB1FdEtrkc3W4R5pmcTi8MPc6cPX5FiCpBs7bJTU7EdojrPg8WbC5PD5/HbI5ki13imp2ZG/14XO6VWcwaPa73aYtVhvk16w1JvN4zmv2ypfi1e84Ul2nLTT5COTxRtUgxqj1h18rRlCcv///yH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAzODAxMTc0MDcyMDY4MTE4MDgzRTJEQ0EyOUQ3RjY2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkQ2MTY4Qjg0QzdBMTExRTJBRjAyQTNBREMzOTczOTEwIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkQ2MTY4QjgzQzdBMTExRTJBRjAyQTNBREMzOTczOTEwIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowMjgwMTE3NDA3MjA2ODExODIyQUZENkE3ODIxNkZEMCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowMzgwMTE3NDA3MjA2ODExODA4M0UyRENBMjlEN0Y2NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAEAAD8ALAAAAACPAB8AAAb/wJ9wSCwaj8ikcslsOp/QnwFB0lii2Kx2yy1uFk2I9VeZXLvotHrb4HSYFc1PQRrY1/i83ujw+QBLCA0/JBIKDQh7iotdBn4+ChRKJEIkdYSMmZpNGQ4InwgDShqidTISMpuqq1sWEwMVPxAKYKy2t00DVDK1uL6/UCU8w8QpSCA6ycrLOjRDIT3R0j0CLy0hRyIf2x9FDzvgO0gj4UcWcSaPJg4QTBQFGh0T8/T17BJZN8TEJ8fM/86EQJtGsMeLBEW0cfMWTtwRcuCMDFDwqKKfJYYsarTo4IyTFPuIqfD3b1nAHwMLThMgaYjCbQzLPZQ5ZMPGRw6UeOBws6cP/xMZnsAgtoIYC5LJQChdCiLAs2lDDjCYxoDIy25EvtEsAtGhEAkVIVSQQLbCnSQTfPoU5UQFsQj7IhxBpmxJyh5FWkB1yQ3rEK0RZwYWAuGRgSgL1Pqc8ITFsBo/ig5zMXeZ3b1DEkzDJuRqzMFct1L8iQWA4p4cnJwQ+cMF0cp1ldw1spnvwqwNx21VV9pngQ4+nbge1g/EPqdF6Ca7LI22tAO2YeLeSqQrkdE+DkMxfXNChsQ9ndQoJmRfCSPKdTCPViSHNAFW+372Ghp04UcQtDfhvvEB4fBMwDXMDUPoMwwK6JVk2VPNEfGCNFVF55cQgNFX3VZgWaRAfkzwZ/8RAUNYgJ1FTQgzzHlCmDiMMUSkVxIRswkh1XsISTifbqCRcRMCsSThYUUbFCBkAX1s1IRkPMglhIDEJadgbAKpRNBJnck3XY5DWFeEBEVq1A4SP55WERMgDXMUEY4NM1KLyzC1FIxSRvMCdAlZ+Vduglk4RAMDpGORfkWEKeZFSwx1YBEoGOUklEjcVVAL2dhJIZ5GaJlEAxpUlNMRgg7KRKIhhTqMkkKktx5eP2gmDWfx3XYndVnCekSmj4A5qJFLiKorZUOYKhtmekXDUp2uCtFAQ4MYYQM4ODSRIaFGdCrmEqvpGuoKbDJ6RIw/UCCANJC2Kh0RDRVgBGAxNOH/iB8m2HprRakp4dZkR6jIA3I/+JoEtz+495y4EwpBQDg29PLDAheE498SiDzClhEebKSABRRTLGgiSozXpBFM8oBivm26CQKdKGEmxINyAnxuQzuMIOTA4YxAxAKkjFXWLI8g4JERCGikQBHrWoSBEh0nkSYPCJb6ZDIzMMgeEapGE+4PnhkRA8tY25DsV2rRosSzj/xMRNCPTNASEiomfcS8Kyq9dNNRNkhEsNHQWbURGGAd89Zc9+QA30gYMKLYQ5D9U41JIAlDEoZu7OI/cJcs9xDeSvNClcUW0UABIyS8Aw4xLKy5Aw5gt84AgCuRAAaeIMAYERJ8MoEGAAS1AkQQADs=" width="143" /></td>
			</tr>
			<tr valign="top">
				<td style="font-family:Arial;font-size:12px;width:260px">
					<a href="https://www.infomil.nl/onderwerpen/lucht-water/luchtkwaliteit/slag/monitoren-nsl/handleiding/" style="color:#333;text-decoration:none">www.infomil.nl/onderwerpen/lucht-water/luchtkwaliteit/slag/monitoren-nsl/handleiding/</a><br />
					<a href="http://www.aerius.nl" style="color:#333;text-decoration:none">www.aerius.nl</a><br />
					<a href="http://twitter.com/AERIUSapp" style="color:#333;text-decoration:none">twitter.com/AERIUSapp</a><br />
					&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" style="font-family:Arial;color:#6e6e6e;font-size:12px;padding-top:8px">
					AERIUS is ontwikkeld in opdracht van de Rijksoverheid en de gezamenlijke provincies</td>
			</tr>
		</tbody>
	</table>
</div>
<div style="width:600px;text-align:center">
	<div class="tpl-content-highlight" style="width:560px;padding:40px 20px 0"><p style="font-family:Arial;font-size:12px;color:#6e6e6e">Aan de verstrekte gegevens kunnen geen rechten worden ontleend. De eigenaar van AERIUS aanvaardt geen aansprakelijkheid voor de inhoud van de door de gebruiker aangeboden informatie. Bovenstaande gegevens zijn enkel bruikbaar tot er een nieuwe versie van AERIUS beschikbaar is. AERIUS is een geregistreerd handelsmerk in Europa. Alle rechten die niet expliciet worden verleend, zijn voorbehouden.</p>
	</div>
    </td></tr></table>
</body>
</html>');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('MAIL_SUBJECT_TEMPLATE', 'en', '[MAIL_SUBJECT]'); -- for now just the subject itself, we just want to provide the option to change the template

INSERT INTO i18n.messages (key, language_code, message) VALUES ('MAIL_SIGNATURE_DEFAULT', 'en', 'The AERIUS team');

INSERT INTO i18n.messages (key, language_code, message) VALUES ('ERROR_MAIL_SUBJECT', 'en', 'Notice from AERIUS concerning your requested export');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('ERROR_MAIL_CONTENT', 'en',
E'<p style="font-family:Arial;font-size:14px;">Dear AERIUS user,</p>
<p style="font-family:Arial;font-size:14px;">On [CALC_CREATION_DATE] at [CALC_CREATION_TIME] you have started an AERIUS calculation. This calculation could not be completed successfully.</p>

<table border="0" cellpadding="5">
<tr valign="top"><th style="font-family:Arial;font-size:14px;text-align:left">code</th><th style="font-family:Arial;font-size:14px;text-align:left">problem</th><th style="font-family:Arial;font-size:14px;text-align:left">solution</th></tr>
<tr valign="top"><td><p style="font-family:Arial;font-size:14px;color:#ff0000">[ERROR_CODE]</p></td>
<td><p style="font-family:Arial;font-size:14px;color:#ff0000"">[ERROR_MESSAGE]</p></td>
<td><p style="font-family:Arial;font-size:14px;">[ERROR_SOLUTION]</p></td></tr>
</table>
<p style="font-family:Arial;font-size:14px;">This notice is also available to AERIUS operations. </p>
<p style="font-family:Arial;font-size:14px;">When you have any questions concerning this notice, first read the <a href="#">frequently asked questions</a> concerning calculations. You may also consider our (dutch only) helpdesk at bij12: <a href="[BIJ12_HELPDESK_URL]">[BIJ12_HELPDESK_URL]</a>.
</p>');

INSERT INTO i18n.messages (key, language_code, message) VALUES ('NSL_ERROR_MAIL_SUBJECT', 'en', 'Melding van AERIUS betreffende uw aangevraagde bestand');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('NSL_ERROR_MAIL_CONTENT', 'en',
E'<p style="font-family:Arial;font-size:14px;">Geachte heer/mevrouw,</p>
<p style="font-family:Arial;font-size:14px;">U heeft op [CALC_CREATION_DATE] om [CALC_CREATION_TIME] uur een berekening in AERIUS gestart. Deze berekening kon niet worden voltooid.</p>

<table border="0" cellpadding="5">
<tr valign="top"><th style="font-family:Arial;font-size:14px;text-align:left">code</th><th style="font-family:Arial;font-size:14px;text-align:left">probleem</th><th style="font-family:Arial;font-size:14px;text-align:left">oplossing</th></tr>
<tr valign="top"><td><p style="font-family:Arial;font-size:14px;color:#ff0000">[ERROR_CODE]</p></td>
<td><p style="font-family:Arial;font-size:14px;color:#ff0000"">[ERROR_MESSAGE]</p></td>
<td><p style="font-family:Arial;font-size:14px;">[ERROR_SOLUTION]</p></td></tr>
</table>
<p style="font-family:Arial;font-size:14px;">Deze melding is ook bekend bij de beheerder van AERIUS. </p>
<p style="font-family:Arial;font-size:14px;">Heeft u nog vragen naar aanleiding van deze melding, lees dan eerst de <a href="#">veelgestelde vragen</a> over berekeningen. Uiteraard kunt u ook contact opnemen met de helpdesk luchtkwaliteit van Infomil, te bereiken via Luchtkwaliteit@RWS.nl.
</p>');

-- Default email stuff
INSERT INTO i18n.messages (key, language_code, message) VALUES ('DEFAULT_FILE_MAIL_SUBJECT', 'en', 'Your requested AERIUS file');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('DEFAULT_FILE_MAIL_CONTENT', 'en',
E'<p style="font-family:Arial;font-size:14px;">Dear AERIUS user,</p>
<p style="font-family:Arial;font-size:14px;">On [CALC_CREATION_DATE] at [CALC_CREATION_TIME] you have started an export from AERIUS. This export has been completed. You can download the file upto 3 days from the starting of the export.</p>
<div style="text-align:center"><a href="[DOWNLOAD_LINK]" style="display:inline-block;font-family:Arial;font-size:14px;background:linear-gradient(#DBE1E1, #B8C6C5) repeat scroll 0 0 transparent;width:auto;padding:10px 40px 0px;border:1px solid #4c4c4c; -moz-border-radius: 2px;border-radius:2px;box-shadow: 0 1px 0 #FFFFFF inset;color: #333333;height: 33px;text-align: center;text-shadow: 0 1px 0 white;text-decoration:none;font-weight:bold">Download file</a></div>
<p style="font-family:Arial;font-size:14px;">When you have any questions concerning this export or about AERIUS in general first browse the <a href="https://www.aerius.nl/nl/publicaties/handleidingen-en-leeswijzers">manuals ( handleidingen, only available in dutch)</a> or our website at <a href="http://www.aerius.nl/en">AERIUS.nl</a>. Further assistance can be found at the (dutch only) Bij12 helpdesk: <a href="[BIJ12_HELPDESK_URL]">[BIJ12_HELPDESK_URL]</a>.</p>');

-- email for PAA (PDF Export)
INSERT INTO i18n.messages (key, language_code, message) VALUES ('PAA_MAIL_SUBJECT', 'en', 'Your AERIUS export: [PROJECT_NAME]');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('PAA_MAIL_CONTENT', 'en',
E'<p style="font-family:Arial;font-size:14px;">Dear AERIUS user,</p>
<p style="font-family:Arial;font-size:14px;">On [CALC_CREATION_DATE] at [CALC_CREATION_TIME] you have started an AERIUS export. This export has been completed. You can download the resulting PDF-file upto 3 days from the starting time of the export.</p>
<div style="text-align:center"><a href="[DOWNLOAD_LINK]" style="display:inline-block;font-family:Arial;font-size:14px;background:linear-gradient(#DBE1E1, #B8C6C5) repeat scroll 0 0 transparent;width:auto;padding:10px 40px 0px;border:1px solid #4c4c4c; -moz-border-radius: 2px;border-radius:2px;box-shadow: 0 1px 0 #FFFFFF inset;color: #333333;height: 33px;text-align: center;text-shadow: 0 1px 0 white;text-decoration:none;font-weight:bold">Download file</a></div>
<p style="font-family:Arial;font-size:14px;">The PDF-file may be used to import sources of your AERIUS calculation into other geodata systems. It may also be re-imported into AERIUS for further calculations or re-editing of your sources. The PDF has not been translated into English because its contents is subject to Dutch law.</p>
<p style="font-family:Arial;font-size:14px;">When you have any questions concerning this export or about AERIUS in general first browse the <a href="https://www.aerius.nl/nl/publicaties/handleidingen-en-leeswijzers">manuals ( handleidingen, only available in dutch)</a> or our website at <a href="http://www.aerius.nl/en">AERIUS.nl</a>. Further assistance can be found at the (dutch only) Bij12 helpdesk: <a href="[BIJ12_HELPDESK_URL]">[BIJ12_HELPDESK_URL]</a>.</p>');

-- email for CSV
INSERT INTO i18n.messages (key, language_code, message) VALUES ('CSV_MAIL_SUBJECT', 'en', 'Uw AERIUS aangevraagde bestand [AERIUS_REFERENCE]');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('CSV_MAIL_SUBJECT_JOB', 'en', 'Uw AERIUS aangevraagde berekening [JOB] ([AERIUS_REFERENCE])');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('CSV_MAIL_CONTENT', 'en',
E'<p style="font-family:Arial;font-size:14px;">Geachte heer/mevrouw,</p>
<p style="font-family:Arial;font-size:14px;">U heeft op [CALC_CREATION_DATE] om [CALC_CREATION_TIME] uur een berekening in AERIUS gestart. Deze berekening is klaar en u kunt het CSV-bestand tot uiterlijk 3 dagen na het starten van de berekening ophalen.</p>
<div style="text-align:center"><a href="[DOWNLOAD_LINK]" style="display:inline-block;font-family:Arial;font-size:14px;background:linear-gradient(#DBE1E1, #B8C6C5) repeat scroll 0 0 transparent;width:auto;padding:10px 40px 0px;border:1px solid #4c4c4c; -moz-border-radius: 2px;border-radius:2px;box-shadow: 0 1px 0 #FFFFFF inset;color: #333333;height: 33px;text-align: center;text-shadow: 0 1px 0 white;text-decoration:none;font-weight:bold">Bestand ophalen</a></div>
<p style="font-family:Arial;font-size:14px;">Heeft u nog vragen, naar aanleiding van de berekening of over AERIUS, bekijk dan eerst de <a href="[MANUAL_URL]">handleiding</a> of onze website <a href="http://www.aerius.nl">AERIUS.nl</a>. Uiteraard kunt u ook contact opnemen met onze helpdesk: <a href="[BIJ12_HELPDESK_URL]">[BIJ12_HELPDESK_URL]</a>.</p>');

-- email for GML (uses default subject)
INSERT INTO i18n.messages (key, language_code, message) VALUES ('GML_MAIL_SUBJECT', 'en', 'Uw AERIUS aangevraagde bestand [AERIUS_REFERENCE]');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('GML_MAIL_SUBJECT_JOB', 'en', 'Uw AERIUS aangevraagde berekening [JOB] ([AERIUS_REFERENCE])');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('GML_MAIL_CONTENT', 'en',
E'<p style="font-family:Arial;font-size:14px;">Geachte heer/mevrouw,</p>
<p style="font-family:Arial;font-size:14px;">U heeft op [CALC_CREATION_DATE] om [CALC_CREATION_TIME] uur een berekening in AERIUS gestart. Deze berekening is klaar en u kunt het GML-bestand tot uiterlijk 3 dagen na het starten van de berekening ophalen.</p>
<div style="text-align:center"><a href="[DOWNLOAD_LINK]" style="display:inline-block;font-family:Arial;font-size:14px;background:linear-gradient(#DBE1E1, #B8C6C5) repeat scroll 0 0 transparent;width:auto;padding:10px 40px 0px;border:1px solid #4c4c4c; -moz-border-radius: 2px;border-radius:2px;box-shadow: 0 1px 0 #FFFFFF inset;color: #333333;height: 33px;text-align: center;text-shadow: 0 1px 0 white;text-decoration:none;font-weight:bold">Bestand ophalen</a></div>
<p style="font-family:Arial;font-size:14px;">Het GML-bestand kunt u gebruiken om de bronnen en resultaten van uw AERIUS berekening te importeren in andere geodata systemen. Daarnaast kunt u het bestand importeren in AERIUS om verder te rekenen of om uw bronnen aan te passen.</p>
<p style="font-family:Arial;font-size:14px;">Heeft u nog vragen, naar aanleiding van de berekening of over AERIUS, bekijk dan eerst de <a href="[MANUAL_URL]">handleiding</a> of onze website <a href="http://www.aerius.nl">AERIUS.nl</a>. Uiteraard kunt u ook contact opnemen met onze helpdesk: <a href="[BIJ12_HELPDESK_URL]">[BIJ12_HELPDESK_URL]</a>.</p>');

-- email for NSL Exports
INSERT INTO i18n.messages (key, language_code, message) VALUES ('NSL_MAIL_SUBJECT', 'en', 'Uw AERIUS aangevraagde bestand [AERIUS_REFERENCE]');
INSERT INTO i18n.messages (key, language_code, message) VALUES ('NSL_MAIL_CONTENT', 'en',
E'<p style="font-family:Arial;font-size:14px;">Geachte heer/mevrouw,</p>
<p style="font-family:Arial;font-size:14px;">U heeft op [CALC_CREATION_DATE] om [CALC_CREATION_TIME] uur een berekening in AERIUS rekentool lucht gestart. Deze berekening is klaar en u kunt betreffende exportbestanden tot uiterlijk 3 dagen na het starten van de berekening ophalen.</p>
<div style="text-align:center"><a href="[DOWNLOAD_LINK]" style="display:inline-block;font-family:Arial;font-size:14px;background:linear-gradient(#DBE1E1, #B8C6C5) repeat scroll 0 0 transparent;width:auto;padding:10px 40px 0px;border:1px solid #4c4c4c; -moz-border-radius: 2px;border-radius:2px;box-shadow: 0 1px 0 #FFFFFF inset;color: #333333;height: 33px;text-align: center;text-shadow: 0 1px 0 white;text-decoration:none;font-weight:bold">Bestand ophalen</a></div>
<p style="font-family:Arial;font-size:14px;">Heeft u nog vragen naar aanleiding van de berekening of over AERIUS lucht, dan kunt u contact opnemen met de helpdesk luchtkwaliteit van Infomil, te bereiken via Luchtkwaliteit@RWS.nl.</p>');
