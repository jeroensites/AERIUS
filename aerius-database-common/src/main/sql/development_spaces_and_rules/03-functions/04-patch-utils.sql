/*
 * ae_floor_to_precision
 * ---------------------
 * Rond een getal naar beneden af zodanig dat deze tenminste nog in de opgegeven precisie past.
 * Dus in plaats van afronden op een aantal decimalen, is het afronden op een aantal significante cijfers.
 * Ondersteunende functie voor {@link ae_fit_demand_to_development_space_precision}.
 *
 * Voorbeelden met precisie = 6:
 *   `1.23456789 => 1.23456`
 *   `1234.56789 => 1234.56`
 *   `123456789 => 123456000`
 *   `0.000123456789 => 0.000123456`.
 *
 * @param v_number Het getal dat afgerond moet worden.
 * @param v_precision De precisie (aantal significante cijfers) waar naar afgerond moet worden. Om het getal in
 *   een `real` floating point te laten passen, gebruik 6.
 */
CREATE OR REPLACE FUNCTION ae_floor_to_precision(v_number numeric, v_precision integer)
	RETURNS numeric AS
$BODY$
DECLARE
	divisor numeric = (10 ^ floor(log(abs(v_number)) - v_precision + 1));
BEGIN
	RETURN COALESCE(floor(v_number / NULLIF(divisor, 0)) * divisor, 0);
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;


/*
 * ae_ceil_to_precision
 * --------------------
 * Rond een getal naar boven af zodanig dat deze tenminste nog in de opgegeven precisie past.
 * Dus in plaats van afronden op een aantal decimalen, is het afronden op een aantal significante cijfers.
 * Ondersteunende functie voor {@link ae_fit_demand_to_development_space_precision}.
 *
 * Voorbeelden met precisie = 6:
 *   `1.23456789 => 1.23457`
 *   `1234.56789 => 1234.57`
 *   `123456789 => 123457000`
 *   `0.000123456789 => 0.000123457`.
 *
 * @param v_number Het getal dat afgerond moet worden.
 * @param v_precision De precisie (aantal significante cijfers) waar naar afgerond moet worden. Om het getal in
 *   een `real` floating point te laten passen, gebruik 6.
 */
CREATE OR REPLACE FUNCTION ae_ceil_to_precision(v_number numeric, v_precision integer)
	RETURNS numeric AS
$BODY$
DECLARE
	divisor numeric = (10 ^ floor(log(abs(v_number)) - v_precision + 1));
BEGIN
	RETURN COALESCE(ceil(v_number / NULLIF(divisor, 0)) * divisor, 0);
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;


/*
 * ae_fit_demand_to_development_space_precision
 * --------------------------------------------
 * Deze functie wordt vooralsnog alleen gebruikt in beheerspatches tijdens het onderhoudtraject, waarbij vaak op receptorniveau een extern opgegeven
 * hoeveelheid ontwikkelingsruimte moet worden verplaatst, bijvoorbeeld van S2 naar S1.
 *
 * De functie rond een getal zodanig af dat deze significant genoeg is om van een gegeven ontwikkelingsruimte te worden afgetrokken of er bij te worden
 * opgeteld. Opgegeven wordt de oorspronkelijke ontwikkelingsruimte, de operatie (optellen of aftrekken), het getal dat wordt opgeteld of afgetrokken,
 * en de maximale precisie van de nieuwe ontwikkelingsruimte (aangezien `space` een `real` veld is, staat deze default op 6).
 *
 * Een voorbeeld is de operatie `27.0039 - 0.00002417 = 27.00387583 ≈ 27.0039`. Beide zijn geldige `real` getallen met een precisie van 6. Als de
 * uitkomst echter weer moet worden opgeslagen in een `real`, dan verandert het oorspronkelijke getal niet omdat er te weinig precisie is. Uiteindelijk
 * wordt er zo dus geen ontwikkelingsruimte verplaatst en kan het zijn dat de patch niet het beoogde resultaat levert.
 * De aanroep `ae_fit_demand_to_development_space_precision(0.00002417, 27.0039, 'subtract')` zal in dit geval `0.0001` teruggeven, het minimale
 * getal waarop de bovenstaande operatie wél effect zal hebben: `27.0039 - 0.0001 = 27.0038`.
 *
 * @param v_demand Getal welke de ontwikkelingsruimte 'behoefte' voorstelt welke moet worden verplaatst.
 * @param v_space Getal welke de ontwikkelingsruimte voorstelt waar de 'behoefte' naar toe moet of van af moet worden verplaatst.
 * @param v_action De operatie: `'add'::development_space_action_type` of `'subtract'::development_space_action_type`.
 * @param v_max_precision De maximale precisie (aantal significante cijfers) waarin de 'aangepaste ontwikkelingsruimte' kan worden opgeslagen. Default
 *   is dit 6, omdat de ontwikkelingsruimte momenteel in een `real` veld staat.
 */
CREATE OR REPLACE FUNCTION ae_fit_demand_to_development_space_precision(v_demand numeric, v_space numeric, v_action development_space_action_type, v_max_precision integer = 6)
	RETURNS numeric AS
$BODY$
	SELECT CASE
	WHEN v_action = 'subtract' THEN
		v_space - ae_floor_to_precision(v_space - v_demand, v_max_precision)
	WHEN v_action = 'add' THEN
		ae_ceil_to_precision(v_space + v_demand, v_max_precision) - v_space
	END;
$BODY$
LANGUAGE SQL IMMUTABLE;
