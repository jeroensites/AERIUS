/*
 * ae_update_initial_available_development_spaces_full
 * ---------------------------------------------------
 * Functie die de initieel te benutten ontwikkelingsruimte in de gehele tabel {@link initial_available_development_spaces} update.
 * In het geval van gedeelde segmenten wordt ook de initieel te benutten ruimte van het andere segment geupdate.
 *
 * Zie de view voor een verdere toelichting.
 *
 * LET OP: update alleen initial_space voor segment 2 en GWR, NIET voor prioritaire projecten.
 *
 * @see calc_initial_available_development_spaces_view
 */
CREATE OR REPLACE FUNCTION ae_update_initial_available_development_spaces_full()
	RETURNS void AS
$BODY$
BEGIN
	UPDATE initial_available_development_spaces

		SET space = calc_view.space

		FROM calc_initial_available_development_spaces_view AS calc_view

		WHERE
			calc_view.segment IN ('projects', 'permit_threshold') AND
			initial_available_development_spaces.receptor_id = calc_view.receptor_id AND
			initial_available_development_spaces.segment = calc_view.segment AND
			initial_available_development_spaces.space <> calc_view.space
	;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_update_initial_available_development_spaces_for_segment
 * ----------------------------------------------------------
 * Functie die de initieel te benutten ontwikkelingsruimte in de tabel {@link initial_available_development_spaces} update
 * voor een gegeven segment (S2 en GWR).
 * In het geval van gedeelde segmenten wordt ook de initieel te benutten ruimte van het andere segment geupdate.
 *
 * Zie de view voor een verdere toelichting.
 *
 * LET OP: update alleen initial_space voor segment 2 en GWR, NIET voor prioritaire projecten.
 *
 * @see calc_initial_available_development_spaces_view
 */
CREATE OR REPLACE FUNCTION ae_update_initial_available_development_spaces_for_segment(v_segment segment_type)
	RETURNS void AS
$BODY$
BEGIN
	IF v_segment <> 'priority_projects' THEN
		UPDATE initial_available_development_spaces

			SET space = calc_view.space

			FROM calc_initial_available_development_spaces_view AS calc_view

			WHERE
				calc_view.segment = v_segment AND
				initial_available_development_spaces.segment = calc_view.segment AND
				initial_available_development_spaces.receptor_id = calc_view.receptor_id AND
				initial_available_development_spaces.space <> calc_view.space
		;
	END IF;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_update_initial_available_development_spaces_for_receptor
 * -----------------------------------------------------------
 * Functie die de initieel te benutten ontwikkelingsruimte in de tabel {@link initial_available_development_spaces} update
 * voor een gegeven receptor_id.
 * In het geval van gedeelde segmenten wordt ook de initieel te benutten ruimte van het andere segment geupdate.
 *
 * Zie de view voor een verdere toelichting.
 *
 * LET OP: update alleen initial_space voor segment 2 en GWR, NIET voor prioritaire projecten.
 *
 * @see calc_initial_available_development_spaces_view
 */
CREATE OR REPLACE FUNCTION ae_update_initial_available_development_spaces_for_receptor(v_receptor_id int)
	RETURNS void AS
$BODY$
BEGIN
	UPDATE initial_available_development_spaces

		SET space = calc_view.space

		FROM calc_initial_available_development_spaces_view AS calc_view

		WHERE
			calc_view.receptor_id = v_receptor_id AND
			calc_view.segment IN ('projects', 'permit_threshold') AND
			initial_available_development_spaces.segment = calc_view.segment AND
			initial_available_development_spaces.receptor_id = calc_view.receptor_id AND
			initial_available_development_spaces.space <> calc_view.space
	;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_update_initial_available_development_spaces_for_request
 * ----------------------------------------------------------
 * Functie die de initieel te benutten ontwikkelingsruimte in de tabel {@link initial_available_development_spaces} update
 * voor een gegeven request_id.
 * In het geval van gedeelde segmenten wordt ook de initieel te benutten ruimte van het andere segment geupdate.
 *
 * Zie de view voor een verdere toelichting.
 *
 * LET OP: update alleen initial_space voor segment 2 en GWR, NIET voor prioritaire projecten.
 *
 * @see calc_initial_available_development_spaces_view
 */
CREATE OR REPLACE FUNCTION ae_update_initial_available_development_spaces_for_request(v_request_id int)
	RETURNS void AS
$BODY$
BEGIN
	UPDATE initial_available_development_spaces

		SET space = calc_view.space

		FROM calc_initial_available_development_spaces_view AS calc_view
			INNER JOIN request_demands_view USING (receptor_id)
			INNER JOIN requests USING (request_id, segment)

		WHERE
			request_demands_view.request_id = v_request_id AND
			requests.segment IN ('projects', 'permit_threshold') AND
			initial_available_development_spaces.segment = calc_view.segment AND
			initial_available_development_spaces.receptor_id = calc_view.receptor_id AND
			initial_available_development_spaces.space <> calc_view.space
	;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_update_initial_available_development_spaces_for_calculation
 * --------------------------------------------------------------
 * Functie die de initieel te benutten ontwikkelingsruimte in de tabel {@link initial_available_development_spaces} update
 * voor een gegeven segment (S2 en GWR) en calculation_id.
 * In het geval van gedeelde segmenten wordt ook de initieel te benutten ruimte van het andere segment geupdate.
 *
 * Zie de view voor een verdere toelichting.
 *
 * LET OP: update alleen initial_space voor segment 2 en GWR, NIET voor prioritaire projecten.
 *
 * @see calc_initial_available_development_spaces_view
 */
CREATE OR REPLACE FUNCTION ae_update_initial_available_development_spaces_for_calculation(v_segment segment_type, v_calculation_id integer)
	RETURNS void AS
$BODY$
BEGIN
	IF v_segment <> 'priority_projects' THEN
		UPDATE initial_available_development_spaces

			SET space = calc_view.space

			FROM calc_initial_available_development_spaces_view AS calc_view
				INNER JOIN calculation_results USING (receptor_id)
				INNER JOIN calculation_result_sets USING (calculation_result_set_id)

			WHERE
				calc_view.segment = v_segment AND
				calculation_result_sets.calculation_id = v_calculation_id AND
				initial_available_development_spaces.segment = calc_view.segment AND
				initial_available_development_spaces.receptor_id = calc_view.receptor_id AND
				initial_available_development_spaces.space <> calc_view.space
		;
	END IF;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_update_initial_available_development_spaces_for_demand
 * ---------------------------------------------------------
 * Functie die de initieel te benutten ontwikkelingsruimte in de tabel {@link initial_available_development_spaces} update
 * voor een gegeven segment (S2 en GWR) en de current en proposed calculation_id.
 * In het geval van gedeelde segmenten wordt ook de initieel te benutten ruimte van het andere segment geupdate.
 *
 * Zie de view voor een verdere toelichting.
 *
 * LET OP: update alleen initial_space voor segment 2 en GWR, NIET voor prioritaire projecten.
 *
 * @see calc_initial_available_development_spaces_view
 */
CREATE OR REPLACE FUNCTION ae_update_initial_available_development_spaces_for_demand(v_segment segment_type, v_proposed_calculation_id integer, v_current_calculation_id integer)
	RETURNS void AS
$BODY$
BEGIN
	IF v_segment <> 'priority_projects' THEN
		UPDATE initial_available_development_spaces

			SET space = calc_view.space

			FROM calc_initial_available_development_spaces_view AS calc_view
				INNER JOIN development_space_demands USING (receptor_id)

			WHERE
				calc_view.segment = v_segment AND
				development_space_demands.proposed_calculation_id = v_proposed_calculation_id AND
				development_space_demands.current_calculation_id = COALESCE(v_current_calculation_id, 0) AND
				initial_available_development_spaces.segment = calc_view.segment AND
				initial_available_development_spaces.receptor_id = calc_view.receptor_id AND
				initial_available_development_spaces.space <> calc_view.space
		;
	END IF;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
