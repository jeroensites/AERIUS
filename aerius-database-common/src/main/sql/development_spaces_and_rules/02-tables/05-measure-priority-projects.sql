/*
 * public_measure_priority_projects
 * --------------------------------
 * Subset van measure_priority_projects
 * Bevat alleen de maatregelen waartegen getoetst kan worden in Calculator.
 */
CREATE TABLE public_measure_priority_projects (
	priority_project_reference text NOT NULL,
	name text NOT NULL,

	CONSTRAINT public_measure_priority_projects_pkey PRIMARY KEY (priority_project_reference)
);
