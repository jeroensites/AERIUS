/*
 * build_development_spaces_view
 * -----------------------------
 * View om de development_spaces tabel te vullen (met alle waarden op 0).
 */
CREATE OR REPLACE VIEW setup.build_development_spaces_view AS
SELECT
	segment,
	unnest(CASE WHEN segment = 'permit_threshold' OR segment = 'priority_projects' THEN ARRAY['assigned']::development_space_state[] ELSE enum_range(NULL::development_space_state) END) AS status,
	receptor_id,
	0 AS space

	FROM reserved_development_spaces
;


/*
 * build_initial_available_development_spaces_view
 * -----------------------------------------------
 * View om de initial_available_development_spaces tabel te vullen.
 *
 * @see calc_initial_available_development_spaces_view
 */
CREATE OR REPLACE VIEW setup.build_initial_available_development_spaces_view AS
SELECT
	segment,
	receptor_id,
	space

	FROM calc_initial_available_development_spaces_view
;
