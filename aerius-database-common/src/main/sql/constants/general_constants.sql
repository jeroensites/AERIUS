/**
 * Drempelwaarde. Als een receptor meer depositie heeft dan deze drempelwaarde, dan is er op zijn minst een
 * melding moodzaklijk. Onder de drempelwaarde hoeft er niets geregisteerd te worden.
 */
INSERT INTO constants (key, value) VALUES ('PRONOUNCEMENT_THRESHOLD_VALUE', '0.00499999');

/**
 * Afstand in meters voor omzetting van lijn/vlak in puntbronnen.
 */
INSERT INTO constants (key, value) VALUES ('CONVERT_LINE_TO_POINTS_SEGMENT_SIZE', '25');
INSERT INTO constants (key, value) VALUES ('CONVERT_POLYGON_TO_POINTS_GRID_SIZE', '100');
INSERT INTO constants (key, value) VALUES ('CONVERT_INLAND_SHIPPING_LINE_TO_POINTS_SEGMENT_SIZE', '25');

INSERT INTO constants (key, value) VALUES ('WORST_CASE_Z0', '1.6');
INSERT INTO constants (key, value) VALUES ('WORST_CASE_LAND_USE', 'bebouwing');
