/*
 * gml_conversions
 * ---------------
 * Conversietabel voor aangepaste id's en codes die in oude GML versies gebruikt worden.
 */
CREATE TABLE system.gml_conversions (
	gml_version text NOT NULL,
	type text NOT NULL,
	old_value text NOT NULL,
	new_value text NOT NULL,
	issue_warning boolean NOT NULL,

	CONSTRAINT gml_conversions_pkey PRIMARY KEY (gml_version, type, old_value)
);


/*
 * gml_mobile_source_off_road_conversions
 * --------------------------------------
 * Conversietabel met waardes benodigd voor conversie van eigenschappen van de oude rekenmethode (IMAER <= 3.1) naar de nieuwe rekenmethode (IMAER >= 4.0).
 * Op basis van de oude offroad mobiele codes en de aanwezige waardes in de GML kan hiermee een inschatting gemaakt worden van de totale draaiuren.
 *
 * @column new_fuel_consumption liter brandstof verbruik per uur (l/uur)
 */
CREATE TABLE system.gml_mobile_source_off_road_conversions (
	old_code text NOT NULL,
	new_fuel_consumption posreal NOT NULL,

	CONSTRAINT gml_mobile_source_off_road_conversions_pkey PRIMARY KEY (old_code)
);


/*
 * gml_plan_conversions
 * --------------------
 * Conversietabel met waardes benodigd voor conversie van eigenschappen van ruimtelijke plannen in oudere versies (IMAER <= 4.0) naar generieke bronnen.
 * Op basis van de oude plan codes en de aanwezige waardes in de GML kan hiermee een generieke bron gemaakt worden.
 * De emissiefactoren zijn hierbij voor de stof NOx aangezien dat de enige stof is waarvoor emissiefactoren > 0 aanwezig waren.
 * Om te voorkomen dat karakteristieken mee wijzigen mochten de GCN sector karakteristieken ooit wijzigen zijn deze direct in de tabel opgenomen.
 *
 * @column name De naam van de categorie. Wordt niet direct in de code gebruikt.
 * @column gcn_sector_id De GCN sector waaraan de categorie was verbonden. Op basis hiervan werden karakteristieken bepaald. 
 * @column emission_factor De emissiefactor voor NOX in kg/j/[eenheid behorend bij de categorie]
 */
CREATE TABLE system.gml_plan_conversions (
	old_code text NOT NULL,
	name text NOT NULL,
	gcn_sector_id integer NOT NULL,
	emission_factor posreal NOT NULL,
	heat_content posreal NOT NULL,
	height posreal NOT NULL,
	spread posreal NOT NULL,
	emission_diurnal_variation_id integer NOT NULL,
	particle_size_distribution integer NOT NULL,

	CONSTRAINT gml_plan_conversions_pkey PRIMARY KEY (old_code),
	CONSTRAINT gml_plan_conversion_fkey_emission_diurnal_variations FOREIGN KEY (emission_diurnal_variation_id) REFERENCES emission_diurnal_variations
);
