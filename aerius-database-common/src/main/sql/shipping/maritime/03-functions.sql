/*
 * ae_standalone_maritime_shipping_route
 * -------------------------------------
 * Functie voor het bepalen van de punten voor een route voor zeescheepvaart, zowel binnenvaart als op zee.
 * @param v_route De route die het schip vaart
 * @param v_max_segment_size De maximale lengte die een punt mag representeren.
 * @return De verzameling punten die de route moet voorstellen. Elk punt krijgt mee welke afstand deze in de route representeert.
 * en of het een stukje van de route representeert waar gemanouvreerd wordt voor de meegegeven scheepscategorie (zo ja, welke factor).
 */
CREATE OR REPLACE FUNCTION ae_standalone_maritime_shipping_route(v_route geometry, v_max_segment_size double precision)
	RETURNS TABLE(point_geometry geometry, length posreal, maneuver_factor posreal) AS
$BODY$
DECLARE
	length_per_point posreal;

BEGIN
	CREATE TEMPORARY TABLE tmp_shipping_emission_points (point geometry) ON COMMIT DROP;
	INSERT INTO tmp_shipping_emission_points SELECT ae_line_to_point_sources(v_route, v_max_segment_size);
	-- length per point = total length / # points
	SELECT ST_Length(v_route) / COUNT(*) INTO length_per_point FROM tmp_shipping_emission_points;

	RETURN QUERY SELECT points.*, length_per_point, COALESCE(maneuver_area.maneuver_factor, 1::posreal)
		FROM tmp_shipping_emission_points AS points
			LEFT JOIN shipping_maritime_maneuver_areas AS maneuver_area ON ST_Intersects(points.point, maneuver_area.geometry);

	DROP TABLE tmp_shipping_emission_points;
	RETURN;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_maneuvering_maritime_shipping_route
 * --------------------------------------
 * Functie voor het bepalen van de punten voor een route voor zeescheepvaart waarbij gemanouvreerd wordt.
 * @param v_route De route die het schip vaart
 * @param v_max_segment_size De maximale lengte die een punt mag representeren.
 * @param v_maneuver_factor De factor die voor de punten als standaard manouvreer factor geldt.
 * @return De verzameling punten die de route moet voorstellen. Elk punt krijgt mee welke afstand deze in de route representeert.
 * en of het een stukje van de route representeert waar gemanouvreerd wordt voor de meegegeven scheepscategorie (zo ja, welke factor).
 */
CREATE OR REPLACE FUNCTION ae_maneuvering_maritime_shipping_route(v_route geometry, v_max_segment_size double precision, v_maneuver_factor posreal)
  RETURNS TABLE(point_geometry geometry, length posreal, maneuver_factor posreal) AS
$BODY$
DECLARE
	length_per_point posreal;
BEGIN
	CREATE TEMPORARY TABLE tmp_shipping_emission_points_maneuver (point geometry) ON COMMIT DROP;
	INSERT INTO tmp_shipping_emission_points_maneuver SELECT ae_line_to_point_sources(v_route, v_max_segment_size);
	-- length per point = total length / # points
	SELECT ST_Length(v_route) / COUNT(*) INTO length_per_point FROM tmp_shipping_emission_points_maneuver;
	RETURN QUERY SELECT
			points.*,
			length_per_point,
			GREATEST(v_maneuver_factor, COALESCE(maneuver_area.maneuver_factor, 0::posreal))
		FROM tmp_shipping_emission_points_maneuver AS points
			LEFT JOIN shipping_maritime_maneuver_areas AS maneuver_area ON ST_Intersects(points.point, maneuver_area.geometry);

	DROP TABLE tmp_shipping_emission_points_maneuver;
	RETURN;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_maritime_mooring_inland_shipping_route
 * -----------------------------------------
 * Functie voor het bepalen van de punten voor een route voor zeescheepvaart waarbij aangemeerd wordt.
 * Hierbij wordt rekening gehouden met het deel waarbij een manouvreer factor vanaf de aanlegplaats gebruikt moet worden.
 * @param transfer_route De route die het schip vaart om bij de havenmond te komen of om bij een andere aanlegplaats te komen.
 * @param v_max_segment_size De maximale lengte die een punt mag representeren.
 * @param v_maneuver_length De lengte waarvoor een manouvreer factor geldt vanaf een aanlegplaats.
 * @param v_maneuver_factor De factor die voor het manouvreren van/naar de aanlegplaats gebruikt dient te worden.
 * @param v_mooring_on_a Waarde die aangeeft of er aan het begin van de route een aanlegplaats ligt (aan de A zijde).
 * @param v_mooring_on_b Waarde die aangeeft of er aan het eind van de route een aanlegplaats ligt (aan de B zijde).
 * @return De verzameling punten die de route moet voorstellen. Elk punt krijgt mee welke afstand deze in de route representeert.
 * en of het een stukje van de route representeert waar gemanouvreerd wordt voor de meegegeven scheepscategorie (zo ja, welke factor).
 */
CREATE OR REPLACE FUNCTION ae_maritime_mooring_inland_shipping_route(transfer_route geometry, v_max_segment_size double precision, v_maneuver_length posreal, v_maneuver_factor posreal, v_mooring_on_a boolean, v_mooring_on_b boolean)
  RETURNS TABLE(point_geometry geometry, length posreal, maneuver_factor posreal) AS
$BODY$
DECLARE
	remaining_part geometry;
	maneuver_a_part geometry;
	maneuver_a_fraction posreal;
	maneuver_b_part geometry;
	maneuver_b_fraction posreal;
BEGIN
	-- First determine the maneuvering partion of the route from side A. 
	SELECT CASE WHEN v_maneuver_factor = 1 OR NOT v_mooring_on_a THEN 0 ELSE v_maneuver_length / ST_Length(transfer_route) END INTO maneuver_a_fraction;

	IF maneuver_a_fraction = 0 THEN
		SELECT transfer_route INTO remaining_part;
	ELSEIF maneuver_a_fraction > 1 THEN
		SELECT transfer_route INTO maneuver_a_part;
	ELSE
		SELECT ST_LineSubstring(transfer_route, 0, maneuver_a_fraction) INTO maneuver_a_part;
		SELECT ST_LineSubstring(transfer_route, maneuver_a_fraction, 1) INTO remaining_part;
	END IF;

	-- Then determine the maneuvering partion of the route from side B if there is any remaining part.
	IF remaining_part IS NOT NULL THEN
		SELECT CASE WHEN v_maneuver_factor = 1 OR NOT v_mooring_on_b THEN 0 ELSE v_maneuver_length / ST_Length(remaining_part) END INTO maneuver_b_fraction;

		IF maneuver_b_fraction = 0 THEN
			-- Basically no change, keep the remaining part as it is
			SELECT remaining_part INTO remaining_part;
		ELSEIF maneuver_b_fraction > 1 THEN
			-- Full remaining part is maneuvering, be sure to set remaining part to null
			SELECT remaining_part INTO maneuver_b_part;
			SELECT NULL INTO remaining_part;
		ELSE
			SELECT ST_LineSubstring(remaining_part, 1 - maneuver_b_fraction, 1) INTO maneuver_b_part;
			SELECT ST_LineSubstring(remaining_part, 0, 1 - maneuver_b_fraction) INTO remaining_part;
		END IF;
	END IF;

	IF maneuver_a_part IS NOT NULL THEN
		RETURN QUERY SELECT * FROM ae_maneuvering_maritime_shipping_route(maneuver_a_part, v_max_segment_size, v_maneuver_factor);
	END IF;

	IF remaining_part IS NOT NULL THEN
		RETURN QUERY SELECT * FROM ae_standalone_maritime_shipping_route(remaining_part, v_max_segment_size);
	END IF;

	IF maneuver_b_part IS NOT NULL THEN
		RETURN QUERY SELECT * FROM ae_maneuvering_maritime_shipping_route(maneuver_b_part, v_max_segment_size, v_maneuver_factor);
	END IF;

	RETURN;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_category_maritime_mooring_inland_shipping_route
 * --------------------------------------------------
 * Functie voor het bepalen van de route die een schip vaart binnen de haven,
 * uitgaande van een transfer route naar de havenmond.
 * Deze route wordt direct omgezet naar punten die gebruikt kunnen worden voor emissiebronnen.
 * @param v_category_id Het ID van de scheepscategorie waarvoor de route bepaalt moet worden (inclusief bepalen van het manoeuvreer gedeelte).
 * @param transfer_route De route die het schip vaart om bij de havenmond te komen of om bij een andere aanlegplaats te komen.
 * @param v_max_segment_size De maximale lengte die een punt mag representeren.
 * @param v_mooring_on_a Waarde die aangeeft of er aan het begin van de route een aanlegplaats ligt (aan de A zijde).
 * @param v_mooring_on_b Waarde die aangeeft of er aan het eind van de route een aanlegplaats ligt (aan de B zijde).
 * @return De verzameling punten die de route moet voorstellen. Elk punt krijgt mee welke afstand deze in de route representeert.
 * en of het een stukje van de route representeert waar gemanouvreerd wordt voor de meegegeven scheepscategorie.
 */
CREATE OR REPLACE FUNCTION ae_category_maritime_mooring_inland_shipping_route(v_category_id int, transfer_route geometry, v_max_segment_size double precision, v_mooring_on_a boolean, v_mooring_on_b boolean)
	RETURNS TABLE(point_geometry geometry, length posreal, maneuver_factor posreal) AS
$BODY$
DECLARE
	maneuver_length posreal;
	maneuver_factor posreal;
BEGIN
	SELECT category.maneuver_length, category.maneuver_factor INTO maneuver_length, maneuver_factor
		FROM shipping_maritime_category_maneuver_properties AS category
		WHERE category.shipping_maritime_category_id = v_category_id;

	RETURN QUERY SELECT * FROM ae_maritime_mooring_inland_shipping_route(transfer_route, v_max_segment_size, maneuver_length, maneuver_factor, v_mooring_on_a, v_mooring_on_b);
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_custom_maritime_mooring_inland_shipping_route
 * ------------------------------------------------
 * Functie voor het bepalen van de route die een schip vaart binnen de haven, uitgaande van een transfer route naar de havenmond.
 * Dit op basis van de brutotonnage van het betreffende schip, ipv de categorie_id.
 * Deze route wordt direct omgezet naar punten die gebruikt kunnen worden voor emissiebronnen.
 * @param v_tonnage De brutotonnage van het schip waarvoor de route bepaalt moet worden (inclusief bepalen van het manoeuvreer gedeelte).
 * @param transfer_route De route die het schip vaart om bij de havenmond te komen of om bij een andere aanlegplaats te komen.
 * @param v_max_segment_size De maximale lengte die een punt mag representeren.
 * @param v_mooring_on_a Waarde die aangeeft of er aan het begin van de route een aanlegplaats ligt (aan de A zijde).
 * @param v_mooring_on_b Waarde die aangeeft of er aan het eind van de route een aanlegplaats ligt (aan de B zijde).
 * @return De verzameling punten die de route moet voorstellen. Elk punt krijgt mee welke afstand deze in de route representeert.
 * en of het een stukje van de route representeert waar gemanouvreerd wordt voor de meegegeven scheepscategorie.
 */
CREATE OR REPLACE FUNCTION ae_custom_maritime_mooring_inland_shipping_route(v_tonnage int, transfer_route geometry, v_max_segment_size double precision, v_mooring_on_a boolean, v_mooring_on_b boolean)
	RETURNS TABLE(point_geometry geometry, length posreal, maneuver_factor posreal) AS
$BODY$
DECLARE
	v_tonnage_category_id integer;
	maneuver_length posreal;
	maneuver_factor posreal;
BEGIN
	v_tonnage_category_id := tonnage_category_id FROM shipping_maritime_mooring_maneuver_factors WHERE tonnage_lower_threshold < v_tonnage ORDER BY tonnage_lower_threshold DESC LIMIT 1;

	SELECT mooring_maneuver_factors.maneuver_length, mooring_maneuver_factors.maneuver_factor INTO maneuver_length, maneuver_factor
		FROM shipping_maritime_mooring_maneuver_factors AS mooring_maneuver_factors
		WHERE tonnage_category_id = v_tonnage_category_id;

	RETURN QUERY SELECT * FROM ae_maritime_mooring_inland_shipping_route(transfer_route, v_max_segment_size, maneuver_length, maneuver_factor, v_mooring_on_a, v_mooring_on_b);
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
