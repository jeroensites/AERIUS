/*
 * ae_delete_job
 * -------------
 * Gebruik deze functie om een job te verwijderen inclusief bijbehorende rekenresultaten.
 *
 * @param v_job_id Id van de job die verwijderd moet worden.
 */
CREATE OR REPLACE FUNCTION ae_delete_job(v_job_id integer)
	RETURNS void AS
$BODY$
BEGIN
	PERFORM ae_delete_job_calculations(v_job_id);

	DELETE FROM job_progress WHERE job_id = v_job_id;
	DELETE FROM jobs WHERE job_id = v_job_id;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

/*
 * ae_delete_job_calculations
 * --------------------------
 * Gebruik deze functie om alleen de rekenresultaten van een job te verwijderen.
 *
 * @param v_job_id Id van de job waarvan de rekenresultaten verwijderd moet worden.
 */
CREATE OR REPLACE FUNCTION ae_delete_job_calculations(v_job_id integer)
	RETURNS void AS
$BODY$
DECLARE
	v_calculation_id integer;
BEGIN
	FOR v_calculation_id IN
		(SELECT calculation_id FROM job_calculations WHERE job_id = v_job_id)
	LOOP
		DELETE FROM job_calculations WHERE job_id = v_job_id AND calculation_id = v_calculation_id;
		PERFORM ae_delete_calculation(v_calculation_id);
	END LOOP;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

/*
 * ae_delete_user_calculation_point_set_and_points
 * -----------------------------------------------
 * Gebruik deze functie om een gebruikers receptor/rekenpunt-set te
 * verwijderen inclusief bijbehorende receptoren.
 *
 * @param v_set_id Id van de receptorset die verwijderd moet worden.
 */
CREATE OR REPLACE FUNCTION ae_delete_user_calculation_point_set_and_points(v_set_id integer)
  RETURNS void AS
$BODY$
BEGIN
  DELETE FROM user_calculation_points WHERE user_calculation_point_set_id = v_set_id;
  DELETE FROM user_calculation_point_sets WHERE user_calculation_point_set_id = v_set_id;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

/*
 * ae_update_job_status
 * --------------------
 * Gebruik deze functie om de status van job te wijzigen.
 * Zorgt ervoor dat bepaalde status wijzigingen niet mogelijk zijn.
 * Als voorbeeld: als state al 'completed' is, dan kan deze niet meer aangepast worden naar 'cancelled'.
 * Onjuiste status wijzigingen zorgen niet voor foutmelding, maar worden niet uitgevoerd.
 *
 * @param v_job_id Id van de job waarvan de status gewijzigd moet worden.
 * @param v_job_status Status waarin de job gewijzigd moet worden.
 */
CREATE OR REPLACE FUNCTION ae_update_job_status(v_job_key text, v_job_status job_state_type)
	RETURNS void AS
$BODY$
	UPDATE jobs 
		SET state = v_job_status 
		WHERE key = v_job_key
	   		AND (state IN ('initialized', 'running') OR v_job_status = 'deleted'::job_state_type);
$BODY$
LANGUAGE SQL VOLATILE;

/*
 * ae_scenario_get_calculation_id_of_situation_type
 * ------------------------------------------------
 * Verkrijgt de calculation_id van een berekening binnen een bepaalde job.
 * De berekening wordt gezocht op basis van situatie type.
 */
CREATE OR REPLACE FUNCTION ae_scenario_get_calculation_id_of_situation_type(v_job_key text, v_situation_type situation_type)
	RETURNS int AS
$BODY$
	SELECT COALESCE(
		(
		SELECT calculation_id
		FROM job_calculations
			INNER JOIN jobs USING (job_id)
		WHERE key = v_job_key
			AND situation_type = v_situation_type
		LIMIT 1
		), 0)
$BODY$
LANGUAGE SQL STABLE;

/*
 * ae_scenario_get_calculation_ids_of_situation_type
 * -------------------------------------------------
 * Verkrijgt de calculation_ids van berekeningen binnen een bepaalde job.
 * De berekeningen worden gezocht op basis van situatie type.
 */
CREATE OR REPLACE FUNCTION ae_scenario_get_calculation_ids_of_situation_type(v_job_key text, v_situation_type situation_type)
	RETURNS int[] AS
$BODY$
	SELECT ARRAY(
		SELECT calculation_id
		FROM job_calculations
			INNER JOIN jobs USING (job_id)
		WHERE key = v_job_key
			AND situation_type = v_situation_type
		)
$BODY$
LANGUAGE SQL STABLE;

/*
 * ae_scenario_get_calculation_id_of_situation_reference
 * -----------------------------------------------------
 * Verkrijgt de calculation_id van een berekening binnen een bepaalde job.
 * De berekening wordt gezocht op basis van situatie referentie.
 */
CREATE OR REPLACE FUNCTION ae_scenario_get_calculation_id_of_situation_reference(v_job_key text, v_situation_reference text)
	RETURNS int AS
$BODY$
	SELECT COALESCE(
		(
		SELECT calculation_id
		FROM job_calculations
			INNER JOIN jobs USING (job_id)
		WHERE key = v_job_key
			AND situation_reference = v_situation_reference
		LIMIT 1
		), 0)
$BODY$
LANGUAGE SQL STABLE;
