/*
 * job_state_type
 * --------------
 * Geeft de status aan van een job.
 */
CREATE TYPE job_state_type AS ENUM
  ('initialized', 'running', 'cancelled', 'completed', 'deleted', 'error');

/*
 * job_type
 * --------
 * Type van een job.
 */
CREATE TYPE job_type AS ENUM
	('calculation', 'calculate_delta', 'report', 'priority_project_utilisation');

/*
 * situation_type
 * --------------
 * Type van een situatie.
 */
CREATE TYPE situation_type AS ENUM
	('proposed', 'reference', 'temporary', 'netting');
