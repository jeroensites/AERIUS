/*
 * mobile_source_off_road_categories
 * ---------------------------------
 * De categorieën van verschillende soorten offroad mobiele bronnen (stageklassen).
 * De naam is hierbij de identificatie van de categorie voor de gebruiker.
 */
CREATE TABLE mobile_source_off_road_categories
(
	mobile_source_off_road_category_id smallint NOT NULL,
	code text NOT NULL UNIQUE,
	name text NOT NULL UNIQUE,
	description text,
	sort_order integer NOT NULL UNIQUE,

	CONSTRAINT mobile_source_off_road_categories_pkey PRIMARY KEY (mobile_source_off_road_category_id)
);


/*
 * mobile_source_off_road_category_adblue_properties
 * -------------------------------------------------
 * Eigenschappen per stageklasse die nodig zijn om de adblue validaties uit te voeren (onafhankelijk van de stof).
 *
 * Niet voor alle stageklasses zijn adblue emissieberekeningen mogelijk, in dat geval ontbreekt het record.
 *
 * @column max_adblue_fuel_ratio Maximum ratio between liters adblue and fuel that should be used.
 */
CREATE TABLE mobile_source_off_road_category_adblue_properties
(
	mobile_source_off_road_category_id smallint NOT NULL,
	max_adblue_fuel_ratio fraction NOT NULL,

	CONSTRAINT mobile_source_off_road_category_adblue_prop_pkey PRIMARY KEY (mobile_source_off_road_category_id),
	CONSTRAINT mobile_source_off_road_category_adblue_prop_fkey_cat_id FOREIGN KEY (mobile_source_off_road_category_id) REFERENCES mobile_source_off_road_categories
);


/*
 * mobile_source_off_road_category_emission_factors
 * ------------------------------------------------
 * De emissie factoren voor stageklassen.
 * 
 * Er zijn emissiefactoren beschikbaar voor brandstofverbruik en draaiuren.
 * Afhankelijk van de categorie is 1 van beide aanwezig (of niet 0) of beide aanwezig.
 * Daarnaast is een emissiefactor beschikbaar voor AdBlue, welke de totale emissie weer kan reduceren (dit zijn negatieve getallen).
 *
 * @column emission_factor_per_liter_fuel f1, emissie factor per liter brandstof (kg/l)
 * @column emission_factor_per_operating_hour f2, emissie factor per draaiuur (stationair + werkend) (kg/uur)
 * @column emission_factor_per_liter_adblue f3, emissie factor per liter adblue (kg/l).
 */
CREATE TABLE mobile_source_off_road_category_emission_factors
(
	mobile_source_off_road_category_id smallint NOT NULL,
	substance_id smallint NOT NULL,
	emission_factor_per_liter_fuel posreal,
	emission_factor_per_operating_hour posreal,
	emission_factor_per_liter_adblue real,

	CONSTRAINT mobile_source_off_road_category_efac_pkey PRIMARY KEY (mobile_source_off_road_category_id, substance_id),
	CONSTRAINT mobile_source_off_road_category_efac_fkey_substances FOREIGN KEY (substance_id) REFERENCES substances,
	CONSTRAINT mobile_source_off_road_category_efac_fkey_mobile_off_road_cat FOREIGN KEY (mobile_source_off_road_category_id) REFERENCES mobile_source_off_road_categories
);


/*
 * mobile_source_on_road_categories
 * --------------------------------
 * De categorieën van verschillende soorten onroad mobiele bronnen.
 * Dit is qua structuur dezelfde tabel als mobile_source_off_road_categories.
 * Hierdoor zou overerving wel kunnen, echter is het nadeel hierbij dat voor de ID's
 * vervolgens rekening gehouden moet worden met de andere tabel. Hierom is gekozen NIET gebruik te maken van overerving.
 * Er is een kans dat de lijsten afzonderlijk van elkaar zullen gaan wijzigen.
 *
 * De naam is hierbij de identificatie van de categorie voor de gebruiker.
 */
CREATE TABLE mobile_source_on_road_categories
(
	mobile_source_on_road_category_id smallint NOT NULL,
	code text NOT NULL UNIQUE,
	name text NOT NULL UNIQUE,
	description text,

	CONSTRAINT mobile_source_on_road_categories_pkey PRIMARY KEY (mobile_source_on_road_category_id)
);


/*
 * mobile_source_on_road_category_emission_factors
 * -----------------------------------------------
 * De emissie factoren voor verschillende soorten onroad mobiele bronnen.
 * De emissie factoren zijn hier in kg/km/voertuig.
 */
CREATE TABLE mobile_source_on_road_category_emission_factors
(
	mobile_source_on_road_category_id smallint NOT NULL,
	road_type road_type NOT NULL,
	year year_type NOT NULL,
	substance_id smallint NOT NULL,
	emission_factor posreal NOT NULL,

	CONSTRAINT mobile_source_on_road_efac_pkey PRIMARY KEY (mobile_source_on_road_category_id, road_type, year, substance_id),
	CONSTRAINT mobile_source_on_road_efac_fkey_substances FOREIGN KEY (substance_id) REFERENCES substances,
	CONSTRAINT mobile_source_on_road_efac_fkey_mobile_on_road_cat FOREIGN KEY (mobile_source_on_road_category_id) REFERENCES mobile_source_on_road_categories
);
