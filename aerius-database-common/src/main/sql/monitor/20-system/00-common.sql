{import_common 'system/general/'}
-- One of the validations during database build depends on sector_properties (sector_calculation_properties to be exact).
{import_common 'system/sector_properties/'}
{import_common 'system/habitat_type_colors/'}