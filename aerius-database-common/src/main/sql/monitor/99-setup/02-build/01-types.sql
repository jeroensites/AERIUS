/*
 * deposition_correction_type
 * --------------------------
 * Type depositiecorrectie.
 */
CREATE TYPE setup.deposition_correction_type AS ENUM
	('farm_stagnation_correction', 'rwc_correction', 'post_correction');
