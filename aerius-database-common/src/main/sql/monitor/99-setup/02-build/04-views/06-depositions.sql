/*
 * build_depositions_no_policies_view
 * ----------------------------------
 * View om depositions_no_policies tabel mee te vullen.
 */
CREATE OR REPLACE VIEW setup.build_depositions_no_policies_view AS
SELECT
	receptor_id,
	year,
	deposition + COALESCE(other_deposition, 0) AS deposition

	FROM
		(SELECT
			receptor_id,
			year,
			SUM(deposition) AS deposition

			FROM sector_depositions_no_policies

			GROUP BY receptor_id, year

		) AS sector

		LEFT JOIN
			(SELECT
				receptor_id,
				year,
				SUM(deposition) AS other_deposition

				FROM other_depositions

				GROUP BY receptor_id, year

		) AS other USING (receptor_id, year)
;


/*
 * build_delta_depositions_no_policies_view
 * ----------------------------------------
 * View om delta_depositions_no_policies tabel mee te vullen.
 */
CREATE OR REPLACE VIEW setup.build_delta_depositions_no_policies_view AS
SELECT
	receptor_id,
	year,
	ROUND((future.deposition - reference.deposition)::numeric, 2) AS delta_deposition

	FROM
		(SELECT receptor_id, deposition FROM depositions_no_policies INNER JOIN years USING (year) WHERE year_category = 'reference') AS reference
		INNER JOIN depositions_no_policies AS future USING (receptor_id)
		INNER JOIN years USING (year)

	WHERE year_category IN ('future')
;


/*
 * build_exceeding_receptors_view
 * ------------------------------
 * View om exceeding_receptors tabel mee te vullen.
 *
 * Retourneert (per jaar) de receptoren waarbij de KDW overschreden wordt (KDW - 70 mol).
 */
CREATE OR REPLACE VIEW setup.build_exceeding_receptors_view AS
SELECT
	receptor_id,
	year

	FROM included_receptors
		CROSS JOIN years

		LEFT JOIN -- Waar dreigt de KDW (in het 'last' jaar) overschreden te worden.
			(SELECT
				receptor_id,
				bool_or(critical_deposition - deposition < 70) AS exceeding
				
				 FROM depositions_no_policies
					INNER JOIN critical_depositions USING (receptor_id)
					INNER JOIN years USING (year)
				
				WHERE year_category = 'last'

				GROUP BY receptor_id
				
			) AS exceedings USING (receptor_id)
		
	WHERE
		year_category IN ('last', 'future')
		AND exceeding
;
