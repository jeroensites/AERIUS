/*
 * assessment_areas_to_goal_habitat_types_view
 * -------------------------------------------
 * Retourneert de stikstofgevoelige doelstellingstypen (goal_habitat_type) per assessment_area.
 */
CREATE OR REPLACE VIEW setup.assessment_areas_to_goal_habitat_types_view AS
SELECT
	assessment_area_id,
	goal_habitat_type_id

	FROM
		-- Habitat
		(SELECT 
			assessment_area_id,
			goal_habitat_type_id

			FROM habitat_properties

			WHERE NOT (quality_goal = 'none' AND extent_goal = 'none')
		UNION
		-- Soorten
		SELECT
			assessment_area_id,
			goal_habitat_type_id

			FROM species_to_habitats
		UNION
		-- H9999 ..
		SELECT
			DISTINCT
				assessment_area_id,
				goal_habitat_type_id

				FROM relevant_habitats
					INNER JOIN habitat_type_relations USING (habitat_type_id)

		) AS all_designated

		INNER JOIN habitat_type_relations USING (goal_habitat_type_id)
		INNER JOIN habitat_type_critical_depositions_view USING (habitat_type_id)

	GROUP BY assessment_area_id, goal_habitat_type_id
	HAVING bool_or(sensitive) IS TRUE
;


/*
 * build_assessment_areas_to_relevant_goal_habitats_view
 * -----------------------------------------------------
 * View om de lookup-table assessment_areas_to_relevant_goal_habitats te vullen.
 * @see setup.assessment_areas_to_relevant_goal_habitats
 */
CREATE OR REPLACE VIEW setup.build_assessment_areas_to_relevant_goal_habitats_view AS
SELECT
	assessment_area_id,
	goal_habitat_type_id,
	ae_weighted_avg(habitat_coverage::numeric, ST_Area(geometry)::numeric)::fraction AS coverage,
	ST_CollectionExtract(ST_Multi(ST_Union(geometry)), 3) AS geometry

	FROM relevant_habitats
		INNER JOIN habitat_type_relations USING (habitat_type_id)
		INNER JOIN setup.assessment_areas_to_goal_habitat_types_view USING (assessment_area_id, goal_habitat_type_id)

	GROUP BY assessment_area_id, goal_habitat_type_id
;


/*
 * build_receptors_to_assessment_areas_on_relevant_goal_habitats_view
 * ------------------------------------------------------------------
 * View om de lookup-table receptors_to_assessment_areas_on_relevant_goal_habitats te vullen.
 * @see receptors_to_assessment_areas_on_relevant_goal_habitats
 */
CREATE OR REPLACE VIEW setup.build_receptors_to_assessment_areas_on_relevant_goal_habitats_view AS
SELECT
	assessment_area_id,
	receptor_id,
	(ST_Area(geometry) / 10000.0)::numeric AS weight,
	ST_Area(geometry) AS surface,
	ST_Multi(geometry) AS geometry

	FROM
		(SELECT
			assessment_area_id,
			receptor_id,
			ST_CollectionExtract(ST_Union(geometry), 3) AS geometry

			FROM
				(SELECT
					assessment_area_id,
					(setup.ae_determine_hexagon_intersections(geometry)).receptor_id,
					(setup.ae_determine_hexagon_intersections(geometry)).geometry

					FROM assessment_areas_to_relevant_goal_habitats

				) AS intersections

			GROUP BY assessment_area_id, receptor_id

		) AS geometries
;
