/*
 * ae_build_other_depositions
 * --------------------------
 * Build functie voor het aanpassen van de other_deposition tabel.
 */
CREATE OR REPLACE FUNCTION setup.ae_build_other_depositions()
	RETURNS void AS
$BODY$
BEGIN
	PERFORM ae_raise_notice('other_depositions - remaining_deposition @ ' || timeofday());

	EXECUTE $$
		UPDATE other_depositions
			SET other_deposition_type = 'remaining_deposition'
			WHERE other_deposition_type = 'dune_area_correction'
	$$;

	RETURN;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;