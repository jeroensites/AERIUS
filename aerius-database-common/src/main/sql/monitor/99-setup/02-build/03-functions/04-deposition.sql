/*
 * ae_build_sector_depositions_no_policies
 * ---------------------------------------
 * Functie voor het vullen van de depositie-tabel voor beleidsscenario zonder-beleid, per jaar en AERIUS-sector.
 * De functie wordt aangeroepen per sectorgroep.
 * Dit is een lange functie, echter opdelen ervan zal de logica alleen maar verwarrender maken. Deze keuze is dus bewust.
 */
CREATE OR REPLACE FUNCTION setup.ae_build_sector_depositions_no_policies(v_sectorgroup sectorgroup_type)
	RETURNS void AS
$BODY$
BEGIN

	CREATE TEMPORARY TABLE tmp_sector_depositions_no_policies (
		year year_type NOT NULL,
		sector_id integer NOT NULL,
		receptor_id integer NOT NULL,
		source_deposition_scaled real NOT NULL,
		past_deposition real NOT NULL,
		deposition_future_past real NOT NULL,
		future_with_growth_deposition real NOT NULL,

		CONSTRAINT tmp_sector_depositions_no_policies_pkey PRIMARY KEY (year, sector_id, receptor_id)
	) ON COMMIT DROP;


	-- -- LAST YEAR (NO POLICIES) -- --

	RAISE NOTICE 'Start calculating deposition past year no policies of sectorgroup: % @ %', v_sectorgroup, timeofday();

	EXECUTE $$
		INSERT INTO tmp_sector_depositions_no_policies (year, sector_id, receptor_id, source_deposition_scaled, past_deposition, deposition_future_past, future_with_growth_deposition)
		SELECT
			year,
			sector_id,
			receptor_id,
			source_depositions_scaled.deposition AS source_deposition_scaled,
			0 AS past_deposition,
			0 AS deposition_future_past,
			0 AS future_with_growth_deposition

			FROM
				-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
				-- 1) RIVM depositie (relevant voor niet verfijnde (delen van) sectoren).
				-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
				(SELECT
					last_year AS year,
					sector_id,
					receptor_id,
					SUM(deposition * scale_factor) AS deposition

					FROM setup.gcn_sector_depositions_no_policies_$$ || v_sectorgroup || $$_view
						INNER JOIN (
							SELECT
								year AS last_year,
								gcn_sector_id,
								substance_id,
								scale_factor 

								FROM setup.gcn_sector_economic_scale_factors
									INNER JOIN years USING (year)

								WHERE year_category NOT IN ('source', 'reference')

							) AS factors USING (gcn_sector_id, substance_id)

						INNER JOIN years USING (year)
						INNER JOIN gcn_sectors USING (gcn_sector_id)

					WHERE year_category = 'source'

					GROUP BY last_year, sector_id, receptor_id

				) AS source_depositions_scaled
	$$;


	-- -- FUTURE YEARS (NO POLICIES) -- --

	RAISE NOTICE 'Start calculating deposition future years no policies of sectorgroup: % @ %', v_sectorgroup, timeofday();

	EXECUTE $$
		INSERT INTO tmp_sector_depositions_no_policies (year, sector_id, receptor_id, source_deposition_scaled, past_deposition, deposition_future_past, future_with_growth_deposition)
		SELECT
			year,
			sector_id,
			receptor_id,
			source_depositions_scaled.deposition AS source_deposition_scaled,
			0 AS past_deposition,
			0 AS deposition_future_past,
			0 AS future_with_growth_deposition
		
			FROM
				-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
				-- 1) RIVM depositie (relevant voor niet verfijnde (delen van) sectoren).
				-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --				
				(SELECT
					future_year AS year,
					sector_id,
					receptor_id,
					SUM(deposition * scale_factor) AS deposition

					FROM setup.gcn_sector_depositions_no_policies_$$ || v_sectorgroup || $$_view
						INNER JOIN (
							SELECT
								year AS future_year,
								gcn_sector_id,
								substance_id,
								scale_factor

								FROM setup.gcn_sector_economic_scale_factors_no_economy
									INNER JOIN years USING (year)
								
							) AS factors USING (gcn_sector_id, substance_id)

						INNER JOIN gcn_sectors USING (gcn_sector_id)
						INNER JOIN years USING (year)

					WHERE year_category = 'source'

					GROUP BY future_year, sector_id, receptor_id

				) AS source_depositions_scaled
	$$;


	-- -- FIX LAST YEAR COMPLETENESS -- --

	RAISE NOTICE 'Ensuring all sectors in sectorgroup % in future years have data in last year @ %', v_sectorgroup, timeofday();

	INSERT INTO tmp_sector_depositions_no_policies (year, sector_id, receptor_id, source_deposition_scaled, past_deposition, deposition_future_past, future_with_growth_deposition)
	SELECT
		year,
		sector_id,
		receptor_id,
		0 AS source_deposition_scaled,
		0 AS past_deposition,
		0 AS deposition_future_past,
		0 AS future_with_growth_deposition

		FROM
			-- all sectors with receptors EXCEPT those in past year = those that only exist in future year
			(SELECT DISTINCT sector_id, receptor_id FROM tmp_sector_depositions_no_policies
			 EXCEPT
			 SELECT DISTINCT sector_id, receptor_id FROM tmp_sector_depositions_no_policies INNER JOIN years using (year) WHERE year_category = 'last'
			) AS sectors_receptors
			CROSS JOIN years

		WHERE year_category = 'last'
	;

	RAISE NOTICE 'Appending sectorgroup % to data table of deposition no policies @ %', v_sectorgroup, timeofday();

	INSERT INTO sector_depositions_no_policies (year, sector_id, receptor_id, deposition)
	SELECT
		year,
		sector_id,
		receptor_id,
		GREATEST(source_deposition_scaled + past_deposition + deposition_future_past + future_with_growth_deposition, 0) AS deposition

		FROM tmp_sector_depositions_no_policies
	;

	RAISE NOTICE 'Appending sectorgroup % to analysis table of deposition no policies @ %', v_sectorgroup, timeofday();

	INSERT INTO setup.sector_depositions_no_policies_analysis (year, sector_id, source_deposition_scaled, past_deposition, deposition_future_past, future_with_growth_deposition)
	SELECT
		year,
		sector_id,
		AVG(source_deposition_scaled) AS source_deposition_scaled,
		AVG(past_deposition) AS past_deposition,
		AVG(deposition_future_past) AS deposition_future_past,
		AVG(future_with_growth_deposition) AS future_with_growth_deposition

		FROM tmp_sector_depositions_no_policies

		GROUP BY year, sector_id
	;

	DROP TABLE tmp_sector_depositions_no_policies;

	RETURN;
END;

$BODY$
LANGUAGE plpgsql VOLATILE;