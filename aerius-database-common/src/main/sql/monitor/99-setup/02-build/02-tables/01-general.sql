/*
 * sector_depositions_no_policies_analysis
 * ---------------------------------------
 * Tussentabel voor analysedoeleinden, met de gemiddelde (AVG) depositiebijdrage zonder beleid per toekomstjaar en AERIUS-sector.
 * De depositie is opgesplitst in de losse onderdelen waaruit deze is opgebouwd.
 */
CREATE TABLE setup.sector_depositions_no_policies_analysis (
	year year_type NOT NULL,
	sector_id integer NOT NULL,
	source_deposition_scaled real NOT NULL,
	past_deposition real NOT NULL,
	deposition_future_past real NOT NULL,
	future_with_growth_deposition real NOT NULL,

	CONSTRAINT sector_depositions_no_policies_analysis_pkey PRIMARY KEY (year, sector_id)--,
--	CONSTRAINT sector_depositions_no_policies_analysis_fkey_sectors FOREIGN KEY (sector_id) REFERENCES sectors
);