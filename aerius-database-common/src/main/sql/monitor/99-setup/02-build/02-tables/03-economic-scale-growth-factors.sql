/*
 * gcn_sector_economic_scale_factors
 * ---------------------------------
 * Economische groei schaalfactoren per GCN-sector, stof en jaar (ABR: 2,5% economische groei) uitgaande het jaar 2011.
 */
CREATE TABLE setup.gcn_sector_economic_scale_factors (
	year year_type NOT NULL,
	gcn_sector_id integer NOT NULL,
	substance_id smallint NOT NULL,
	scale_factor real NOT NULL,

	CONSTRAINT gcn_sector_economic_scale_factors_pkey PRIMARY KEY (year, gcn_sector_id, substance_id),
	CONSTRAINT gcn_sector_economic_scale_factors_fkey_gcn_sectors FOREIGN KEY (gcn_sector_id) REFERENCES gcn_sectors,
	CONSTRAINT gcn_sector_economic_scale_factors_fkey_substances FOREIGN KEY (substance_id) REFERENCES substances
);


/*
 * gcn_sector_economic_scale_factors_no_economy
 * --------------------------------------------
 * Economische groei schaalfactoren per per GCN-sector, stof en jaar (POR: 0,0% economsiche groei) uitgaande het jaar 2011.
 */
CREATE TABLE setup.gcn_sector_economic_scale_factors_no_economy (
	year year_type NOT NULL,
	gcn_sector_id integer NOT NULL,
	substance_id smallint NOT NULL,
	scale_factor real NOT NULL,

	CONSTRAINT gcn_sector_economic_scale_factors_no_economy_pkey PRIMARY KEY (year, gcn_sector_id, substance_id),
	CONSTRAINT gcn_sector_economic_scale_factors_no_economy_fkey_gcn_sectors FOREIGN KEY (gcn_sector_id) REFERENCES gcn_sectors,
	CONSTRAINT gcn_sector_economic_scale_factors_no_economy_fkey_substances FOREIGN KEY (substance_id) REFERENCES substances
);