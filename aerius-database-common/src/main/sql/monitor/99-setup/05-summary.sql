/*
 * ae_output_summary_table
 * -----------------------
 * Genereert overzichten van data als CSV bestanden. Dient gebruikt te worden om database releases inhoudelijk met elkaar te kunnen vergelijken.
 * Deze functie genereert bestanden volgens de opgegeven bestandsspecificatie. In de bestandsspecificatie moet de string {title} gebruikt worden,
 * deze zal per tabel vervangen worden door de naam van die tabel.
 * Verder wordt {datesuffix} vervangen door de huidige datum in YYYYMMDD formaat.
 * @param filespec Pad en bestandsspecificatie zoals hierboven beschreven.
 */
CREATE OR REPLACE FUNCTION setup.ae_output_summary_table(filespec text)
	RETURNS void AS
$BODY$
BEGIN
	DROP TABLE IF EXISTS tmp_summary_log;

	CREATE TEMPORARY TABLE tmp_summary_log (
		name regproc NOT NULL,
		start_time timestamp NOT NULL,
		duration interval NOT NULL
	) ON COMMIT DROP;

	PERFORM ae_run_and_log_summary('setup.ae_output_summary_table_sectorgroup_deposition_per_n2000', filespec);

	PERFORM ae_run_and_log_summary('setup.ae_output_summary_table_deposition_per_n2000', filespec);
	PERFORM ae_run_and_log_summary('setup.ae_output_summary_table_deposition_per_n2000_habitat', filespec);

	PERFORM ae_run_and_log_summary('setup.ae_output_summary_table_other_deposition_per_n2000', filespec);

	PERFORM ae_run_and_log_summary('setup.ae_output_summary_table_analysis_per_sector', filespec);

	PERFORM ae_run_and_log_summary('setup.ae_output_summary_table_n2000_per_sector_deposition_statistics', filespec);
	PERFORM ae_run_and_log_summary('setup.ae_output_summary_table_sector_deposition_statistics', filespec);

	PERFORM ae_run_and_log_summary('setup.ae_output_summary_table_n2000_nitrogen_load_classification_distribution', filespec);
	PERFORM ae_run_and_log_summary('setup.ae_output_summary_table_nitrogen_load_classification_distribution', filespec);


	DROP TABLE tmp_summary_log;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_run_and_log_summary
 * ----------------------
 * Draait een specifieke summary, houdt de doorlooptijd bij en schrijft deze weg. 
 * De doorlooptijd wordt bijgehouden in de tabel tmp_summary_log die wordt aangemaakt in een andere functie (setup.ae_output_summary_table).
 * Als de tijdelijke log-tabel niet bestaat geeft deze functie een foutmelding, dus deze functie kan niet los worden aangeroepen.
 * De logfile wordt opgeslagen volgens dezelfde filespec als de summaries (met title "log").
 * @summary_function Naam van de uit te voeren summary functie.
 * @param filespec Pad en bestandsspecificatie zoals beschreven bij ae_output_summary_table().
 */
CREATE OR REPLACE FUNCTION ae_run_and_log_summary(summary_function regproc, filespec text)
	 RETURNS void AS
$BODY$
DECLARE
	start_time timestamp;
	duration interval;
BEGIN
	IF NOT EXISTS (SELECT 1 FROM pg_tables WHERE tablename = 'tmp_summary_log') THEN
		RAISE EXCEPTION 'Cannot run this function independently; use setup.ae_output_summary_table() or run the summary without logging.';
	END IF;
	
	start_time := clock_timestamp();
	EXECUTE 'SELECT ' || summary_function || '(''' || filespec || ''')';
	duration := clock_timestamp() - start_time;

	PERFORM ae_raise_notice('Duration of summary ' || summary_function || '(): ' || duration);

	INSERT INTO tmp_summary_log (name, start_time, duration) VALUES (summary_function, start_time, duration);
	
	PERFORM setup.ae_write_summary_table(filespec, 'log', 'SELECT * FROM tmp_summary_log ORDER BY start_time');	
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_write_summary_table
 * ----------------------
 * Hulpfunctie om een queryresultaat weg te schrijven naar een CSV bestand met de juiste bestandsnaam.
 * @param filespec Pad en bestandsspecificatie zoals hierboven beschreven.
 * @param title Titel van de query voor in de bestandsnaam.
 * @param query De query.
 */
CREATE OR REPLACE FUNCTION setup.ae_write_summary_table(filespec text, title text, query text)
	RETURNS void AS
$BODY$
DECLARE
	filename text;
BEGIN
	filename := replace(filespec, '{datesuffix}', to_char(current_timestamp, 'YYYYMMDD'));
	filename := replace(filename, '{title}', 'summary_' || title);
	filename := '''' || filename || '''';

	EXECUTE $$ COPY ($$ || query || $$) TO $$ || filename || $$ CSV HEADER DELIMITER E';' $$;
END;
$BODY$

LANGUAGE plpgsql VOLATILE;


/*
 * ae_output_summary_table_sectorgroup_deposition_per_n2000
 * --------------------------------------------------------
 * Genereert een CSV met overzichten van de depositie per jaar en N2000 gebied.
 * De getallen worden gegeven voor de sectorgroepen als maximum, en als gewogen gemiddelde op oppervlakte.
 * De gebruikte oppervlakte van het N2000 gebied is de overlap tussen dat gebied en de KDW-gebieden die er in liggen,
 * waarvan de KDW < 2400.
 * Er wordt ook een CSV gemaakt van de sectorgroep 'farm' verder uitgespecificeerd per sector.
 * @param filespec Pad en bestandsspecificatie zoals beschreven bij ae_output_summary_table().
 */
 CREATE OR REPLACE FUNCTION setup.ae_output_summary_table_sectorgroup_deposition_per_n2000(filespec text)
	RETURNS void AS
$BODY$
DECLARE
	query text;
	v_policy_type text;
BEGIN

	FOR v_policy_type IN
		SELECT 'no_policies'
	LOOP
		RAISE NOTICE 'Creating summary for the deposition (%) of the sectorgroups per N2000...', v_policy_type;
		query := $$
			SELECT
				year,
				assessment_area_id AS natura2000_id,
				natura2000_areas.name AS natura2000_name,

				SUM(surface) AS total_habitat_surface,

				MAX(total_deposition_agriculture) AS max_deposition_agriculture,
				ae_weighted_avg(total_deposition_agriculture::numeric, weight::numeric)::real AS avg_deposition_agriculture,

				MAX(total_deposition_industry) AS max_deposition_industry,
				ae_weighted_avg(total_deposition_industry::numeric, weight::numeric)::real AS avg_deposition_industry,

				MAX(total_deposition_shipping) AS max_deposition_shipping,
				ae_weighted_avg(total_deposition_shipping::numeric, weight::numeric)::real AS avg_deposition_shipping,

				MAX(total_deposition_road_transportation) AS max_deposition_road_transportation,
				ae_weighted_avg(total_deposition_road_transportation::numeric, weight::numeric)::real AS avg_deposition_road_transportation,

				MAX(total_deposition_road_freeway) AS max_deposition_road_freeway,
				ae_weighted_avg(total_deposition_road_freeway::numeric, weight::numeric)::real AS avg_deposition_road_freeway,

				MAX(total_deposition_other) AS max_deposition_other,
				ae_weighted_avg(total_deposition_other::numeric, weight::numeric)::real AS avg_deposition_other

				FROM (SELECT year, receptor_id, SUM(GREATEST(deposition, 0)) AS total_deposition_agriculture FROM sector_depositions_$$ || v_policy_type || $$ INNER JOIN sectors_sectorgroup USING (sector_id) WHERE sectorgroup = 'agriculture' GROUP BY year, receptor_id) AS total_depositions_agriculture
					INNER JOIN (SELECT year, receptor_id, SUM(GREATEST(deposition, 0)) AS total_deposition_industry FROM sector_depositions_$$ || v_policy_type || $$ INNER JOIN sectors_sectorgroup USING (sector_id) WHERE sectorgroup = 'industry' GROUP BY year, receptor_id) AS total_depositions_industry USING (year, receptor_id)
					INNER JOIN (SELECT year, receptor_id, SUM(GREATEST(deposition, 0)) AS total_deposition_shipping FROM sector_depositions_$$ || v_policy_type || $$ INNER JOIN sectors_sectorgroup USING (sector_id) WHERE sectorgroup = 'shipping' GROUP BY year, receptor_id) AS total_depositions_shipping USING (year, receptor_id)
					INNER JOIN (SELECT year, receptor_id, SUM(GREATEST(deposition, 0)) AS total_deposition_road_transportation FROM sector_depositions_$$ || v_policy_type || $$ INNER JOIN sectors_sectorgroup USING (sector_id) WHERE sectorgroup = 'road_transportation' GROUP BY year, receptor_id) AS total_depositions_road_transportation USING (year, receptor_id)
					INNER JOIN (SELECT year, receptor_id, SUM(GREATEST(deposition, 0)) AS total_deposition_road_freeway FROM sector_depositions_$$ || v_policy_type || $$ INNER JOIN sectors_sectorgroup USING (sector_id) WHERE sectorgroup = 'road_freeway' GROUP BY year, receptor_id) AS total_depositions_road_freeway USING (year, receptor_id)
					INNER JOIN (SELECT year, receptor_id, SUM(GREATEST(deposition, 0)) AS total_deposition_other FROM sector_depositions_$$ || v_policy_type || $$ INNER JOIN sectors_sectorgroup USING (sector_id) WHERE sectorgroup = 'other' GROUP BY year, receptor_id) AS total_depositions_other USING (year, receptor_id)

					INNER JOIN receptors_to_assessment_areas_on_relevant_habitat_view USING (receptor_id)
					INNER JOIN natura2000_areas USING (assessment_area_id)

				GROUP BY year, assessment_area_id, natura2000_name

				ORDER BY year, assessment_area_id, natura2000_name
		$$;

		PERFORM setup.ae_write_summary_table(filespec, 'sectorgroup_deposition_' || v_policy_type || '_per_n2000', query);

		RAISE NOTICE 'Creating summary for the deposition (%) of the sectors in sectorgroup farm per N2000...', v_policy_type;
		query := $$
			SELECT
				year,
				assessment_area_id AS natura2000_id,
				natura2000_areas.name AS natura2000_name,

				SUM(surface) AS total_habitat_surface,

				MAX(total_deposition_agriculture) AS max_deposition_agriculture,
				ae_weighted_avg(total_deposition_agriculture::numeric, weight::numeric)::real AS avg_deposition_agriculture,

				MAX(depositions_agriculture_lodging.deposition) AS max_deposition_4110_lodging,
				ae_weighted_avg(COALESCE(depositions_agriculture_lodging.deposition, 0)::numeric, weight::numeric)::real AS avg_deposition_4110_lodging,

				MAX(depositions_agriculture_fertilizer_storage.deposition) AS max_deposition_4120_fertilizer_storage,
				ae_weighted_avg(COALESCE(depositions_agriculture_fertilizer_storage.deposition, 0)::numeric, weight::numeric)::real AS avg_deposition_4120_fertilizer_storage,

				MAX(depositions_agriculture_grazing.deposition) AS max_deposition_4130_grazing,
				ae_weighted_avg(COALESCE(depositions_agriculture_grazing.deposition, 0)::numeric, weight::numeric)::real AS avg_deposition_4130_grazing,

				MAX(depositions_agriculture_fertilizer_use.deposition) AS max_deposition_4140_fertilizer_use,
				ae_weighted_avg(COALESCE(depositions_agriculture_fertilizer_use.deposition, 0)::numeric, weight::numeric)::real AS avg_deposition_4140_fertilizer_use,

				MAX(depositions_agriculture_greenhouse.deposition) AS max_deposition_4320_greenhouse,
				ae_weighted_avg(COALESCE(depositions_agriculture_greenhouse.deposition, 0)::numeric, weight::numeric)::real AS avg_deposition_4320_greenhouse,

				MAX(depositions_agriculture_other.deposition) AS max_deposition_4600_other,
				ae_weighted_avg(COALESCE(depositions_agriculture_other.deposition, 0)::numeric, weight::numeric)::real AS avg_deposition_4600_other

				FROM (SELECT year, receptor_id, SUM(GREATEST(deposition, 0)) AS total_deposition_agriculture FROM sector_depositions_$$ || v_policy_type || $$ INNER JOIN sectors_sectorgroup USING (sector_id) WHERE sectorgroup = 'agriculture' GROUP BY year, receptor_id) AS total_depositions_agriculture
					LEFT JOIN (SELECT year, receptor_id, GREATEST(deposition, 0) AS deposition FROM sector_depositions_$$ || v_policy_type || $$ WHERE sector_id = 4110) AS depositions_agriculture_lodging USING (year, receptor_id)
					LEFT JOIN (SELECT year, receptor_id, GREATEST(deposition, 0) AS deposition FROM sector_depositions_$$ || v_policy_type || $$ WHERE sector_id = 4120) AS depositions_agriculture_fertilizer_storage USING (year, receptor_id)
					LEFT JOIN (SELECT year, receptor_id, GREATEST(deposition, 0) AS deposition FROM sector_depositions_$$ || v_policy_type || $$ WHERE sector_id = 4130) AS depositions_agriculture_grazing USING (year, receptor_id)
					LEFT JOIN (SELECT year, receptor_id, GREATEST(deposition, 0) AS deposition FROM sector_depositions_$$ || v_policy_type || $$ WHERE sector_id = 4140) AS depositions_agriculture_fertilizer_use USING (year, receptor_id)
					LEFT JOIN (SELECT year, receptor_id, GREATEST(deposition, 0) AS deposition FROM sector_depositions_$$ || v_policy_type || $$ WHERE sector_id = 4320) AS depositions_agriculture_greenhouse USING (year, receptor_id)
					LEFT JOIN (SELECT year, receptor_id, GREATEST(deposition, 0) AS deposition FROM sector_depositions_$$ || v_policy_type || $$ WHERE sector_id = 4600) AS depositions_agriculture_other USING (year, receptor_id)

					INNER JOIN receptors_to_assessment_areas_on_relevant_habitat_view USING (receptor_id)
					INNER JOIN natura2000_areas USING (assessment_area_id)

				GROUP BY year, assessment_area_id, natura2000_name

				ORDER BY year, assessment_area_id, natura2000_name
		$$;

		PERFORM setup.ae_write_summary_table(filespec, 'sectorgroup_agriculture_deposition_' || v_policy_type || '_per_n2000', query);

	END LOOP;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_output_summary_table_deposition_per_n2000
 * --------------------------------------------
 * Genereert een CSV met overzichten van de depositie per jaar en N2000 gebied.
 * De getallen worden gegeven voor de sectorgroepen als maximum, en als gewogen gemiddelde op oppervlakte.
 * De gebruikte oppervlakte van het N2000 gebied is de overlap tussen dat gebied en de KDW-gebieden die er in liggen,
 * waarvan de KDW < 2400.
 * Er wordt ook een CSV gemaakt van de sectorgroep 'farm' verder uitgespecificeerd per sector.
 * @param filespec Pad en bestandsspecificatie zoals beschreven bij ae_output_summary_table().
 */
CREATE OR REPLACE FUNCTION setup.ae_output_summary_table_deposition_per_n2000(filespec text)
	RETURNS void AS
$BODY$
DECLARE
	query text;
BEGIN
	RAISE NOTICE 'Creating summary for the total deposition per N2000...';
	query := $$
		SELECT
			year,
			assessment_area_id AS natura2000_id,
			natura2000_areas.name AS natura2000_name,

			SUM(surface) AS total_habitat_surface,

			MAX(no_policies.deposition) AS max_total_deposition_no_policies,
			ae_weighted_avg(no_policies.deposition::numeric, weight::numeric)::real AS avg_total_deposition_no_policies

			FROM depositions_no_policies AS no_policies
				INNER JOIN receptors_to_assessment_areas_on_relevant_habitat_view USING (receptor_id)
				INNER JOIN natura2000_areas USING (assessment_area_id)

			GROUP BY year, assessment_area_id, natura2000_name

			ORDER BY year, assessment_area_id, natura2000_name
	$$;

	PERFORM setup.ae_write_summary_table(filespec, 'total_deposition_per_n2000', query);

END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_output_summary_table_deposition_per_n2000_habitat
 * ----------------------------------------------------
 * Genereert een CSV met overzichten van de depositie per jaar, N2000 gebied en habitat type.
 * De getallen worden gegeven voor de sectorgroepen als maximum, en als gewogen gemiddelde op oppervlakte.
 * De gebruikte oppervlakte van het N2000 gebied is de overlap tussen dat gebied en de KDW-gebieden die er in liggen,
 * waarvan de KDW < 2400.
 * Er wordt ook een CSV gemaakt van de sectorgroep 'farm' verder uitgespecificeerd per sector.
 * @param filespec Pad en bestandsspecificatie zoals beschreven bij ae_output_summary_table().
 */
CREATE OR REPLACE FUNCTION setup.ae_output_summary_table_deposition_per_n2000_habitat(filespec text)
	RETURNS void AS
$BODY$
DECLARE
	query text;
BEGIN

	RAISE NOTICE 'Creating summary for the total deposition per N2000 and habitat type...';
	query := $$
		SELECT
			year,
			assessment_area_id AS natura2000_id,
			natura2000_areas.name AS natura2000_name,

			habitat_type_id,
			habitat_types.name AS habitat_type_name,

			SUM(surface) AS total_habitat_surface,

			MAX(no_policies.deposition) AS max_total_deposition_no_policies,
			ae_weighted_avg(no_policies.deposition::numeric, weight::numeric)::real AS avg_total_deposition_no_policies

			FROM depositions_no_policies AS no_policies
				INNER JOIN receptors_to_relevant_habitats_view USING (receptor_id)
				INNER JOIN habitat_types USING (habitat_type_id)
				INNER JOIN natura2000_areas USING (assessment_area_id)

			GROUP BY year, assessment_area_id, natura2000_name, habitat_type_id, habitat_types.name

			ORDER BY year, assessment_area_id, natura2000_name, habitat_type_id, habitat_types.name
	$$;

	PERFORM setup.ae_write_summary_table(filespec, 'total_deposition_per_n2000_habitat', query);

END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_output_summary_table_other_deposition_per_n2000
 * --------------------------------------------------
 * Genereert een CSV met overzichten van de depositiebijdrage die niet aan een sector is toe te kennen, per jaar en N2000 gebied.
 * Deze deposties zijn opgedeeld in types en worden gegeven als maximum, en als gewogen gemiddelde op oppervlakte.
 * De gebruikte oppervlakte van het N2000 gebied is de overlap tussen dat gebied en de KDW-gebieden die er in liggen,
 * waarvan de KDW < 2400.
 * @param filespec Pad en bestandsspecificatie zoals beschreven bij ae_output_summary_table().
 */
CREATE OR REPLACE FUNCTION setup.ae_output_summary_table_other_deposition_per_n2000(filespec text)
	RETURNS void AS
$BODY$
DECLARE
	query text;
BEGIN
	RAISE NOTICE 'Creating summary for the other-deposition per N2000...';
	query := $$
		SELECT
			year,
			assessment_area_id AS natura2000_id,
			natura2000_areas.name AS natura2000_name,

			SUM(surface) AS total_habitat_surface,

			MAX(total_deposition) AS max_other_deposition,
			ae_weighted_avg(total_deposition::numeric, weight::numeric)::real AS avg_other_deposition,

			MAX(depositions_abroad.deposition) AS max_abroad_deposition,
			ae_weighted_avg(COALESCE(depositions_abroad.deposition, 0)::numeric, weight::numeric)::real AS avg_abroad_deposition,

			MAX(depositions_remaining.deposition) AS max_remaining_deposition,
			ae_weighted_avg(COALESCE(depositions_remaining.deposition, 0)::numeric, weight::numeric)::real AS avg_remaining_deposition,

			MAX(ammonia_from_sea.deposition) AS ammonia_from_sea,
			ae_weighted_avg(COALESCE(ammonia_from_sea.deposition, 0)::numeric, weight::numeric)::real AS avg_ammonia_from_sea,

			MAX(corrections_measurement.deposition) AS max_measurement_correction,
			ae_weighted_avg(COALESCE(corrections_measurement.deposition, 0)::numeric, weight::numeric)::real AS avg_measurement_correction,

			MAX(returned_deposition_space.deposition) AS max_returned_deposition_space,
			ae_weighted_avg(COALESCE(returned_deposition_space.deposition, 0)::numeric, weight::numeric)::real AS avg_returned_deposition_space,

			MAX(returned_deposition_space_limburg.deposition) AS max_returned_deposition_space_limburg,
			ae_weighted_avg(COALESCE(returned_deposition_space_limburg.deposition, 0)::numeric, weight::numeric)::real AS avg_returned_deposition_space_limburg,

			MAX(deposition_space_addition.deposition) AS max_deposition_space_addition,
			ae_weighted_avg(COALESCE(deposition_space_addition.deposition, 0)::numeric, weight::numeric)::real AS avg_deposition_space_addition,

			MAX(assigned_space.deposition) AS max_assigned_space_deposition,
			ae_weighted_avg(COALESCE(assigned_space.deposition, 0)::numeric, weight::numeric)::real AS avg_assigned_space_deposition,

			MAX(assigned_space.deposition) AS max_assigned_space_deposition,
			ae_weighted_avg(COALESCE(assigned_space.deposition, 0)::numeric, weight::numeric)::real AS avg_assigned_space_deposition

			FROM (SELECT year, receptor_id, SUM(COALESCE(deposition, 0)) AS total_deposition FROM other_depositions GROUP BY year, receptor_id) AS total_other_depositions
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'abroad_deposition') AS depositions_abroad USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'remaining_deposition') AS depositions_remaining USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'ammonia_from_sea') AS ammonia_from_sea USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'measurement_correction') AS corrections_measurement USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'returned_deposition_space') AS returned_deposition_space USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'returned_deposition_space_limburg') AS returned_deposition_space_limburg USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'deposition_space_addition') AS deposition_space_addition USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'assigned_space_below_threshold') AS assigned_space USING (year, receptor_id)

				INNER JOIN receptors_to_assessment_areas_on_relevant_habitat_view USING (receptor_id)
				INNER JOIN natura2000_areas USING (assessment_area_id)

			GROUP BY year, assessment_area_id, natura2000_name

			ORDER BY year, assessment_area_id, natura2000_name
	$$;


	PERFORM setup.ae_write_summary_table(filespec, 'other_deposition_per_n2000', query);


	RAISE NOTICE 'Creating summary for the other-deposition (only exceeding) per N2000...';
	query := $$
		SELECT
			year,
			assessment_area_id AS natura2000_id,
			natura2000_areas.name AS natura2000_name,

			SUM(surface) AS total_habitat_surface,

			MAX(total_deposition) AS max_other_deposition,
			ae_weighted_avg(total_deposition::numeric, weight::numeric)::real AS avg_other_deposition,

			MAX(depositions_abroad.deposition) AS max_abroad_deposition,
			ae_weighted_avg(COALESCE(depositions_abroad.deposition, 0)::numeric, weight::numeric)::real AS avg_abroad_deposition,

			MAX(depositions_remaining.deposition) AS max_remaining_deposition,
			ae_weighted_avg(COALESCE(depositions_remaining.deposition, 0)::numeric, weight::numeric)::real AS avg_remaining_deposition,

			MAX(corrections_measurement.deposition) AS max_measurement_correction,
			ae_weighted_avg(COALESCE(corrections_measurement.deposition, 0)::numeric, weight::numeric)::real AS avg_measurement_correction,

			MAX(ammonia_from_sea.deposition) AS max_ammonia_from_sea,
			ae_weighted_avg(COALESCE(ammonia_from_sea.deposition, 0)::numeric, weight::numeric)::real AS avg_ammonia_from_sea,

			MAX(returned_deposition_space.deposition) AS max_returned_deposition_space,
			ae_weighted_avg(COALESCE(returned_deposition_space.deposition, 0)::numeric, weight::numeric)::real AS avg_returned_deposition_space,

			MAX(returned_deposition_space_limburg.deposition) AS max_returned_deposition_space_limburg,
			ae_weighted_avg(COALESCE(returned_deposition_space_limburg.deposition, 0)::numeric, weight::numeric)::real AS avg_returned_deposition_space_limburg,

			MAX(deposition_space_addition.deposition) AS max_deposition_space_addition,
			ae_weighted_avg(COALESCE(deposition_space_addition.deposition, 0)::numeric, weight::numeric)::real AS avg_deposition_space_addition,

			MAX(assigned_space.deposition) AS max_assigned_space_deposition,
			ae_weighted_avg(COALESCE(assigned_space.deposition, 0)::numeric, weight::numeric)::real AS avg_assigned_space_deposition,

			MAX(assigned_space.deposition) AS max_assigned_space_deposition,
			ae_weighted_avg(COALESCE(assigned_space.deposition, 0)::numeric, weight::numeric)::real AS avg_assigned_space_deposition

			FROM (SELECT year, receptor_id, SUM(COALESCE(deposition, 0)) AS total_deposition FROM other_depositions GROUP BY year, receptor_id) AS total_other_depositions
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'abroad_deposition') AS depositions_abroad USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'remaining_deposition') AS depositions_remaining USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'ammonia_from_sea') AS ammonia_from_sea USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'measurement_correction') AS corrections_measurement USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'returned_deposition_space') AS returned_deposition_space USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'returned_deposition_space_limburg') AS returned_deposition_space_limburg USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'deposition_space_addition') AS deposition_space_addition USING (year, receptor_id)
				LEFT JOIN (SELECT year, receptor_id, COALESCE(deposition, 0) AS deposition FROM other_depositions WHERE other_deposition_type = 'assigned_space_below_threshold') AS assigned_space USING (year, receptor_id)

				INNER JOIN receptors_to_assessment_areas_on_relevant_habitat_view USING (receptor_id)
				INNER JOIN exceeding_receptors USING (receptor_id, year)
				INNER JOIN natura2000_areas USING (assessment_area_id)

			GROUP BY year, assessment_area_id, natura2000_name

			ORDER BY year, assessment_area_id, natura2000_name
	$$;

	PERFORM setup.ae_write_summary_table(filespec, 'other_deposition_only_exceeding_per_n2000', query);
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_output_summary_table_analysis_per_sector
 * -------------------------------------------
 * Genereert een CSV met overzichten van de analysis tabellen.
 * @param filespec Pad en bestandsspecificatie zoals beschreven bij ae_output_summary_table().
 */
CREATE OR REPLACE FUNCTION setup.ae_output_summary_table_analysis_per_sector(filespec text)
	RETURNS void AS
$BODY$
DECLARE
	query text;
BEGIN
	RAISE NOTICE 'Creating summary for analysis tables...';

	query := $$ SELECT * FROM sectors INNER JOIN setup.sector_depositions_no_policies_analysis USING (sector_id) ORDER BY year, sector_id $$;

	PERFORM setup.ae_write_summary_table(filespec, 'average_depositions_no_policies_analysis_per_sector', query);
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_output_summary_table_n2000_per_sector_deposition_statistics
 * --------------------------------------------------------------
 * Genereert een CSV met de gewogen gemiddelde-, minimale- en maximale geschaalde depositie per n2000 gebied en jaar 
 * voor de reguliere sectoren en other_depositions.
 * @param filespec Pad en bestandsspecificatie zoals beschreven bij ae_output_summary_table().
 */
 CREATE OR REPLACE FUNCTION setup.ae_output_summary_table_n2000_per_sector_deposition_statistics(filespec text)
	RETURNS void AS
$BODY$
DECLARE
	query text;
BEGIN
	RAISE NOTICE 'Creating summary for deposition statistics per n2000 per sector and other_depositions per year...';

	query := $$ SELECT * FROM natura2000_area_sector_deposition_statistics_view ORDER BY natura2000_area_id, year, sector_code $$;

	PERFORM setup.ae_write_summary_table(filespec, 'average_deposition_per_n2000_per_sector_other_depositions', query);
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_output_summary_table_sector_deposition_statistics
 * ----------------------------------------------------
 * Genereert een CSV met de gewogen gemiddelde-, minimale- en maximale geschaalde depositie per jaar 
 * voor de reguliere sectoren en other_depositions voor heel Nederland (alle n2000- gebieden).
 * @param filespec Pad en bestandsspecificatie zoals beschreven bij ae_output_summary_table().
 */
 CREATE OR REPLACE FUNCTION setup.ae_output_summary_table_sector_deposition_statistics(filespec text)
	RETURNS void AS
$BODY$
DECLARE
	query text;
BEGIN
	RAISE NOTICE 'Creating summary for deposition statistics per n2000 per sector and other_depositions per year...';

	query := $$ SELECT * FROM sector_deposition_statistics_view ORDER BY year, sector_code $$;

	PERFORM setup.ae_write_summary_table(filespec, 'deposition_statistics_per_sector_other_depositions', query);
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_output_summary_table_n2000_nitrogen_load_classification_distribution
 * -----------------------------------------------------------------------
 * Genereert een CSV met per natura2000 gebied en jaar de mate van stikstofoverbelasting, oppervlake en gekarteerde oppervlakte van 
 * de betreffende receptoren.
 * @param filespec Pad en bestandsspecificatie zoals beschreven bij ae_output_summary_table().
 */
 CREATE OR REPLACE FUNCTION setup.ae_output_summary_table_n2000_nitrogen_load_classification_distribution(filespec text)
	RETURNS void AS
$BODY$
DECLARE
	query text;
BEGIN
	RAISE NOTICE 'Creating summary for nitrogen load classification per n2000 per year...';

	query := $$ SELECT * FROM natura2000_area_nitrogen_load_classification_distribution_view ORDER BY natura2000_area_id, year $$;

	PERFORM setup.ae_write_summary_table(filespec, 'nitrogen_load_classification_per_n2000_per_year', query);
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_output_summary_table_nitrogen_load_classification_distribution
 * -----------------------------------------------------------------
 * Genereert een CSV met per jaar de mate van stikstofoverbelasting, oppervlake en gekarteerde oppervlakte van 
 * de betreffende receptoren voor heel Nederland (alle n2000 gebieden samen).
 * @param filespec Pad en bestandsspecificatie zoals beschreven bij ae_output_summary_table().
 */
 CREATE OR REPLACE FUNCTION setup.ae_output_summary_table_nitrogen_load_classification_distribution(filespec text)
	RETURNS void AS
$BODY$
DECLARE
	query text;
BEGIN
	RAISE NOTICE 'Creating summary for nitrogen load classification per year...';

	query := $$ SELECT * FROM nitrogen_load_classification_distribution_view ORDER BY year $$;

	PERFORM setup.ae_write_summary_table(filespec, 'nitrogen_load_classification_per_year', query);
END;
$BODY$
LANGUAGE plpgsql VOLATILE;