/*
 * ae_validate_all
 * ---------------
 * Alle validatie functies draaien.
 */
CREATE OR REPLACE FUNCTION setup.ae_validate_all()
	RETURNS TABLE (validaton_result_id integer, validation_run_id integer, name regproc, result setup.validation_result_type) AS
$BODY$
DECLARE
	num_errors integer;
	num_warnings integer;
BEGIN
	RAISE NOTICE '** Validating all...';

	PERFORM setup.ae_perform_and_report_validation('setup.ae_validate_tables_not_empty');
	PERFORM setup.ae_perform_and_report_validation('setup.ae_validate_incorrect_imports');
	--PERFORM setup.ae_perform_and_report_validation('setup.ae_validate_geometries');
	PERFORM setup.ae_perform_and_report_validation('setup.ae_validate_empty_geometries');

	PERFORM setup.ae_perform_and_report_validation('setup.ae_validate_system_habitat_colors');
	PERFORM setup.ae_perform_and_report_validation('setup.ae_validate_habitat_type_critical_level');
	PERFORM setup.ae_perform_and_report_validation('setup.ae_validate_relevant_habitats');

	PERFORM setup.ae_perform_and_report_validation('setup.ae_validate_years');

	PERFORM setup.ae_perform_and_report_validation('setup.ae_validate_gcn_sector_years');
	PERFORM setup.ae_perform_and_report_validation('setup.ae_validate_gcn_sector_completeness_economic');

	PERFORM setup.ae_perform_and_report_validation('setup.ae_validate_terrain_properties');

	PERFORM setup.ae_perform_and_report_validation('setup.ae_validate_receptor_data_completeness');

	PERFORM setup.ae_perform_and_report_validation('setup.ae_validate_natura2000_authorities');

	num_errors := (SELECT COUNT(*) FROM setup.validation_results WHERE validation_results.result = 'error' AND validation_results.validation_run_id = setup.ae_current_validation_run_id());
	num_warnings := (SELECT COUNT(*) FROM setup.validation_results WHERE validation_results.result = 'warning' AND validation_results.validation_run_id = setup.ae_current_validation_run_id());
	RAISE NOTICE '** Validation complete: % error(s), % warning(s).', num_errors, num_warnings;

	RETURN QUERY (SELECT * FROM setup.validation_results WHERE validation_results.validation_run_id = setup.ae_current_validation_run_id() ORDER BY validation_results.result DESC, validation_results.name);
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_validate_years
 * -----------------
 * Valideert de jaren-tabel; het juiste aantal jaren per jaartype moet hier in staan. Bronjaar, landbouwbronjaar, en basisjaar mogen maar 1x voorkomen.
 * De jaren-tabel is de basis voor heel validaties.
 */
CREATE OR REPLACE FUNCTION setup.ae_validate_years()
	RETURNS SETOF setup.validation_result AS
$BODY$
DECLARE
	rec record;
BEGIN
	RAISE NOTICE '* Validating table years...';

	FOR rec IN
	SELECT
		year_category,
		COUNT(year) AS num_years

		FROM unnest(enum_range(NULL::year_category_type)) AS year_category
			LEFT JOIN years USING (year_category)

		WHERE year_category IN ('source', 'past', 'last')

		GROUP BY year_category
		HAVING COUNT(year) <> 1
	LOOP
		RETURN NEXT setup.ae_to_validation_result('error', 'years',
			format('Year category %s must be present exactly one time, but is now present %s times.', rec.year_category, rec.num_years));
	END LOOP;
	RETURN;
END;
$BODY$
LANGUAGE plpgsql STABLE;


/*
 * ae_validate_gcn_sector_years
 * ----------------------------
 * Valideert of de GCN sector (schaalfactoren) tabellen met een jaar erin, de exact juiste set van jaren bevatten. (Bijv. alleen toekomst)
 */
CREATE OR REPLACE FUNCTION setup.ae_validate_gcn_sector_years()
	RETURNS SETOF setup.validation_result AS
$BODY$
DECLARE
	table_name regclass;
BEGIN
	RAISE NOTICE '* Validating GCN sector years...';

	FOREACH table_name IN ARRAY
		ARRAY['setup.gcn_sector_economic_scale_factors']
	LOOP
		RETURN QUERY SELECT * FROM setup.ae_validate_year_set(table_name, (SELECT array_agg(year::smallint) FROM years WHERE year_category IN ('past', 'last')));
	END LOOP;

	FOREACH table_name IN ARRAY
		ARRAY['setup.gcn_sector_economic_scale_factors_no_economy']
	LOOP
		RETURN QUERY SELECT * FROM setup.ae_validate_year_set(table_name, (SELECT array_agg(year::smallint) FROM years WHERE year_category = 'future'));
	END LOOP;
	RETURN;
END;
$BODY$
LANGUAGE plpgsql STABLE;


/*
 * ae_validate_gcn_sector_completeness_economic
 * --------------------------------------------
 * Valideert of alle economische groei schaalfactoren en groeifactoren aanwezig zijn.
 * De GCN-sectoren per stof in setup.gcn_sector_economic_scale_factors zijn hierbij leidend voor de andere tabellen.
 */
CREATE OR REPLACE FUNCTION setup.ae_validate_gcn_sector_completeness_economic()
	RETURNS SETOF setup.validation_result AS
$BODY$
DECLARE
	rec record;
BEGIN
	RAISE NOTICE '* Validating completeness of GCN-sector economic scale factors...';
	FOR rec IN
		EXECUTE setup.ae_validate_get_completeness_sql('setup.gcn_sector_economic_scale_factors', 'years, substances',
			$$ (substance_id = 11 OR substance_id = 17) AND year_category IN ('past', 'last') $$)
	LOOP
		RETURN NEXT setup.ae_to_validation_result('error', 'setup.gcn_sector_economic_scale_factors',
			format('year %s, substance %s, has no scale factor', rec.year, rec.substance_id));
	END LOOP;

	FOR rec IN
		EXECUTE setup.ae_validate_get_completeness_sql('setup.gcn_sector_economic_scale_factors_no_economy', 'years, gcn_sectors, substances',
			$$ year_category = 'future'
				AND (substance_id, gcn_sector_id) IN (SELECT DISTINCT substance_id, gcn_sector_id FROM setup.gcn_sector_economic_scale_factors) $$)
	LOOP
		RETURN NEXT setup.ae_to_validation_result('error', 'setup.gcn_sector_economic_scale_factors_no_economy',
			format('year %s, GCN-sector %s, substance %s, has no scale factor', rec.year, rec.gcn_sector_id, rec.substance_id));
	END LOOP;
	RETURN;
END;
$BODY$
LANGUAGE plpgsql STABLE;


/*
 * ae_validate_receptor_data_completeness
 * --------------------------------------
 * Controleert of er genoeg receptoren staan in de receptor data tabellen.
 */
CREATE OR REPLACE FUNCTION setup.ae_validate_receptor_data_completeness()
	RETURNS SETOF setup.validation_result AS
$BODY$
DECLARE
	table_name regclass;
BEGIN
	RAISE NOTICE '* Validating completeness of receptor data...';

	-- setup.sector_economic_growths: (year, sector_id)={2020|2030,4120}; found 528783, expected 528803; WIL JE NIET
	-- setup.gcn_sector_depositions_no_policies_agriculture: (year, gcn_sector_id, substance_id)={2011,4110|4120,11}; found 528796, expected 528803	FROMs. WIL JE NIET
	-- sector_economic_desires: (year, sector_id)={2020|2030,4120}; found 528783, expected 528803   FROMs. WIL JE NIET

	/*
	 * Niet in onderstaande checks:
	 * setup.gcn_sector_corrections_no_policies
	 * setup.gcn_sector_depositions_no_policies_no_growth
	 *
	 * Deze tabellen worden waar ze gebruikt worden ge-FULL OUTER JOIN-ed, waardoor dit niet uitmaakt.
	 */

	FOREACH table_name IN ARRAY
		ARRAY['setup.gcn_sector_depositions_no_policies_agriculture',
			'setup.gcn_sector_depositions_no_policies_industry',
			'setup.gcn_sector_depositions_no_policies_other',
			'setup.gcn_sector_depositions_no_policies_road_freeway',
			'setup.gcn_sector_depositions_no_policies_road_transportation',
			'setup.gcn_sector_depositions_no_policies_shipping',
			'depositions_no_policies',
			'other_depositions',
			'sector_depositions_no_policies']
	LOOP
		RAISE NOTICE '* Validating completeness of receptor data table ''%''...', table_name;
		RETURN QUERY SELECT * FROM setup.ae_validate_completeness_receptors(table_name, TRUE);
	END LOOP;

	RETURN;
END;
$BODY$
LANGUAGE plpgsql STABLE;