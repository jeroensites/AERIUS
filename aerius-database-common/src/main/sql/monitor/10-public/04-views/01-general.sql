/*
 * non_exceeding_receptors_view
 * ----------------------------
 * Lijst met OR relevante receptoren waar de KDW niet overschreden dreigt te worden.
 * LET OP: deze functie is pas na de database build te gebruiken. Tijdens de build wordt de override tabel namelijk gevuld.
 *
 * Bepaald door:
 * - Begin met all receptoren op een relevant (aangewezen) habitat-gebied (included_receptors)
 * - Filter alle receptoren er uit waarbij de KDW na realisatie van de behoefte dreigt overschreden te worden (exceeding_receptors)
 */
CREATE OR REPLACE VIEW non_exceeding_receptors_view AS
SELECT receptor_id
	FROM included_receptors		
		LEFT JOIN exceeding_receptors USING (receptor_id)
	
	WHERE exceeding_receptors.receptor_id IS NULL
;


/*
 * sectorgroup_depositions_abroad_extended_view
 * --------------------------------------------
 * View die sectorgroup_depositions_abroad uitbreidt met de met namen van de sector / landen groepen ipv de id's.
 */
CREATE OR REPLACE VIEW sectorgroup_depositions_abroad_extended_view AS
SELECT 
    year,
    foreign_country_group_index::foreign_country_group_type AS foreign_country_group,
    sectorgroup_abroad_index::sectorgroup_abroad_type AS sectorgroup_abroad,
    receptor_id, 
    deposition

    FROM sectorgroup_depositions_abroad 

    ORDER BY year, foreign_country_group_index, sectorgroup_abroad_index, receptor_id 
;
