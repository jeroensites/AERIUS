/*
 * sector_depositions_base_view
 * ----------------------------
 * Tussenview, voorziet natura2000_area_sector_deposition_statistics_view en sector_deposition_statistics_view van identieke basis-data:
 * Retourneert de geschaalde depositie en weight per natura2000_area en jaar van de alle reguliere sectoren en 
 * overige sectoren: buitenland, ammoniak van zee en meetcorrectie.
 * Let op: alle jaren uit sector_depositions_no_policies en other_depositions worden teruggegeven!
 * @column weight bepaald op basis van oppervlake.
 */
CREATE OR REPLACE VIEW sector_depositions_base_view AS
SELECT -- de data voor alle reguliere sectoren  
	year,
	natura2000_areas.natura2000_area_id,
	sector_id::text AS sector_code,
	sectorgroup::text,
	deposition,
	weight	

	FROM sector_depositions_no_policies
		INNER JOIN receptors_to_assessment_areas_on_relevant_goal_habitats USING (receptor_id)
		INNER JOIN natura2000_areas USING (assessment_area_id)
		INNER JOIN authorities USING (authority_id)
		INNER JOIN sectors_sectorgroup USING(sector_id)

	WHERE authorities.type <> 'foreign'
UNION
SELECT  -- de data voor de overige sectoren (other depositions)
	year,
	natura2000_area_id,
	other_deposition_type::text AS sector_code,
	other_deposition_type::text AS sectorgroup,
	deposition,
	weight	

	FROM other_depositions
		INNER JOIN receptors_to_assessment_areas_on_relevant_goal_habitats USING (receptor_id)
		INNER JOIN natura2000_areas USING (assessment_area_id)
		INNER JOIN authorities USING (authority_id)

	WHERE authorities.type <> 'foreign'
;


/*
 * natura2000_area_sector_deposition_statistics_view
 * -------------------------------------------------
 * Retourneert de gewogen gemiddelde-, minimale- en maximale depositie per natura2000_area en jaar van de reguliere sectoren en 
 * overige sectoren: buitenland, ammoniak van zee en meetcorrectie.
 * @column avg_deposition het gewogen gemiddelde op basis van oppervlake.
 */
CREATE OR REPLACE VIEW natura2000_area_sector_deposition_statistics_view AS
SELECT 
	year,
	natura2000_area_id,
	sector_code,
	sectorgroup,
	ae_round_deposition(ae_weighted_avg(deposition::numeric, weight))::numeric AS avg_deposition,
	ae_round_deposition(MAX(deposition::numeric)) AS max_deposition,
	ae_round_deposition(MIN(deposition::numeric)) AS min_deposition

	FROM sector_depositions_base_view

	GROUP BY year, natura2000_area_id, sector_code, sectorgroup

	ORDER BY year, natura2000_area_id, sector_code
;


/*
 * sector_deposition_statistics_view
 * ---------------------------------
 * Retourneert de gewogen gemiddelde-, minimale- en maximale depositie per jaar van de reguliere sectoren en 
 * overige sectoren: buitenland, ammoniak van zee en meetcorrectie.
 * @column avg_deposition het gewogen gemiddelde op basis van oppervlake.
 */
CREATE OR REPLACE VIEW sector_deposition_statistics_view AS
SELECT 
	year,
	sector_code,
	sectorgroup,
	ae_round_deposition(ae_weighted_avg(deposition::numeric, weight))::numeric AS avg_deposition,
	ae_round_deposition(MAX(deposition::numeric)) AS max_deposition,
	ae_round_deposition(MIN(deposition::numeric)) AS min_deposition

	FROM sector_depositions_base_view

	GROUP BY year, sector_code, sectorgroup

	ORDER BY year, sector_code 
;


/*
 * habitat_type_nitrogen_load_classification_base_view
 * ---------------------------------------------------
 * Retourneert per assessment_area, habitat_type en receptor_id de stikstof load classificatie (mate van overbelasting) per jaar en het gekarteerde oppervlak behorende bij de classificatie.
 * de load classificatie wordt bepaald aan de hand van de KDW in mol stikstof per habitat.
 * Let op: alle jaren uit depositions_no_policies worden teruggegeven, filteren van de te tonen jaren wordt in de DAL gedaan!
 */
CREATE OR REPLACE VIEW habitat_type_nitrogen_load_classification_base_view AS
SELECT
	assessment_area_id,
	year,
	receptor_id,
	critical_deposition_area_id AS habitat_type_id,
	(surface::numeric * receptor_habitat_coverage::numeric) AS cartographic_surface,
	ae_get_nitrogen_load_classification(critical_deposition::numeric, deposition::numeric) AS classification
	
	FROM receptors_to_critical_deposition_areas
		INNER JOIN depositions_no_policies USING (receptor_id)
		INNER JOIN habitat_type_critical_depositions_view ON (receptors_to_critical_deposition_areas.critical_deposition_area_id = habitat_type_critical_depositions_view.habitat_type_id)
	
	WHERE type = 'relevant_habitat'
;


/*
 * natura2000_area_nitrogen_load_classification_distribution_view
 * --------------------------------------------------------------
 * Retourneert per natura2000 gebied en jaar de mate van overbelasting met het bijbehorende gekarteerde oppervlak van de habitats.
 * Let op: alle jaren uit habitat_type_nitrogen_load_classification_base_view worden teruggegeven, filteren van de te tonen jaren wordt in de DAL gedaan!
 */
CREATE OR REPLACE VIEW natura2000_area_nitrogen_load_classification_distribution_view AS
SELECT
	natura2000_area_id,
	year,
	classification,
	SUM(cartographic_surface) AS cartographic_surface

	FROM habitat_type_nitrogen_load_classification_base_view
		INNER JOIN natura2000_areas USING (assessment_area_id)
		INNER JOIN authorities USING (authority_id)

	WHERE authorities.type <> 'foreign'

	GROUP BY natura2000_area_id, year, classification

	ORDER BY natura2000_area_id, year, classification
;


/*
 * nitrogen_load_classification_distribution_view
 * ----------------------------------------------
 * Retourneert per jaar de mate van overbelasting met het bijbehorende gekarteerde oppervlak van de habitats voor heel Nederland (alle n2000-gebieden samen).
 * Let op: alle jaren uit habitat_type_nitrogen_load_classification_base_view worden teruggegeven, filteren van de te tonen jaren wordt in de DAL gedaan!
 */
CREATE OR REPLACE VIEW nitrogen_load_classification_distribution_view AS
SELECT
	year,
	classification,
	SUM(cartographic_surface) AS cartographic_surface

	FROM habitat_type_nitrogen_load_classification_base_view
		INNER JOIN natura2000_areas USING (assessment_area_id)
		INNER JOIN authorities USING (authority_id)

	WHERE authorities.type <> 'foreign'

	GROUP BY year, classification

	ORDER BY year, classification
;
