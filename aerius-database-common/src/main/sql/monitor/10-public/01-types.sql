/*
 * other_deposition_type
 * ---------------------
 * Type van de andere depositietypen.
 */
CREATE TYPE other_deposition_type AS ENUM
	('abroad_deposition', 'remaining_deposition', 'measurement_correction', 'dune_area_correction', 'returned_deposition_space', 'returned_deposition_space_limburg', 'deposition_space_addition', 'assigned_space_below_threshold', 'ammonia_from_sea');


/*
 * nitrogen_load_classification
 * ----------------------------
 * Classificatie stikstofbelasting. Dit is de afwijking van de totale depositie op de KDW.
 */
CREATE TYPE nitrogen_load_classification AS ENUM
	('no_nitrogen_problem', 'near_overload', 'light_overload', 'moderate_overload', 'heavy_overload');


/*
 * sectorgroup_type
 * ----------------
 * De 6 sectorgroepen die we momenteel in AERIUS-monitor onderscheiden.
 */
CREATE TYPE sectorgroup_type AS ENUM
	('agriculture', 'industry', 'shipping', 'road_transportation', 'road_freeway', 'other');


/*
 * foreign_country_group_type
 * --------------------------
 * Type voor de landindeling voor de buitenland deposities.
 * LET OP: de volgorde van deze landen(groepen) is expliciet zo gekozen, de index hiervan wordt gebruikt in Monitor als countrygroup- indeling (1 = belgium, 2 = germany etc.)
 * Aanpassen van de volgorde heeft dus grote gevolgen voor de buitenland-data in Monitor.
 */
CREATE TYPE foreign_country_group_type AS ENUM
	('belgium', 'germany', 'france', 'united_kingdom', 'other');


/*
 * sectorgroup_abroad_type
 * -----------------------
 * Type voor de sectorgroep indeling voor de buitenland deposities.
 * LET OP: de volgorde van deze sectorgroepen is expliciet zo gekozen, de index hiervan wordt gebruikt in Monitor als sectorgroup- indeling (1 = agriculture, 2 = road_transportation etc.)
 * Aanpassen van de volgorde heeft dus grote gevolgen voor de buitenland-data in Monitor.
 */
 CREATE TYPE sectorgroup_abroad_type AS ENUM
	('agriculture', 'road_transportation', 'industry', 'other');



/*
 * ae_integer_to_foreign_country_group_type
 * ----------------------------------------
 * Hulpfunctie voor de cast van integer naar foreign_country_group
 */
CREATE OR REPLACE FUNCTION ae_integer_to_foreign_country_group_type(anyint integer)
	RETURNS foreign_country_group_type AS
$BODY$
	SELECT ae_enum_by_index(null::foreign_country_group_type, $1);
$BODY$
LANGUAGE sql IMMUTABLE;


/*
 * ae_integer_to_sectorgroup_abroad_type
 * -------------------------------------
 * Hulpfunctie voor de cast van integer naar sectorgroup_abroad
 */
CREATE OR REPLACE FUNCTION ae_integer_to_sectorgroup_abroad_type(anyint integer)
	RETURNS sectorgroup_abroad_type AS
$BODY$
	SELECT ae_enum_by_index(null::sectorgroup_abroad_type, $1);
$BODY$
LANGUAGE sql IMMUTABLE;


/*
 * Cast definitie voor integer naar foreign_country_group
 */
CREATE CAST (integer AS foreign_country_group_type) WITH FUNCTION ae_integer_to_foreign_country_group_type(integer);


/*
 * Cast definitie voor integer naar sectorgroup_abroad
 */
CREATE CAST (integer AS sectorgroup_abroad_type) WITH FUNCTION ae_integer_to_sectorgroup_abroad_type(integer);

