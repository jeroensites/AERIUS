/*
 * assessment_areas_to_relevant_goal_habitats
 * ------------------------------------------
 * De stikstofgevoelige doelstellingstypen (goal_habitat_type) per assessment_area, uitgebreid met de dekkingsgraad en geometrie.
 * @column geometry de union van de goal_habitat_geometrien voor het betreffende assessment_area.
  */
 CREATE TABLE assessment_areas_to_relevant_goal_habitats (
	assessment_area_id integer NOT NULL,
	goal_habitat_type_id integer NOT NULL,
	coverage fraction NOT NULL,
	geometry geometry(MultiPolygon),

	CONSTRAINT assessment_areas_to_relevant_goal_habitats_pkey PRIMARY KEY (assessment_area_id, goal_habitat_type_id)
);

CREATE INDEX idx_assessment_areas_to_relevant_goal_habitats_geometry_gist ON assessment_areas_to_relevant_goal_habitats USING GIST (geometry);
CREATE INDEX idx_assessment_areas_to_relevant_goal_habitats ON assessment_areas_to_relevant_goal_habitats (goal_habitat_type_id);


/*
 * receptors_to_assessment_areas_on_relevant_goal_habitats
 * -------------------------------------------------------
 * Koppeltabel tussen assessment_areas en het relevante deel van de hexagonen (met hun receptor_id) en hun bijbehorende surface, weight en geometrie.
 * @column geometry de intersectie tussen het hexagon en de relevante goal_habitat_types.
 */
 CREATE TABLE receptors_to_assessment_areas_on_relevant_goal_habitats (
 	assessment_area_id integer NOT NULL,
 	receptor_id integer NOT NULL,
 	weight numeric NOT NULL,
 	surface posreal NOT NULL,
	geometry geometry(MultiPolygon),

	CONSTRAINT receptors_to_assessment_areas_on_relevant_goal_habitats_pkey PRIMARY KEY (assessment_area_id, receptor_id)
);

CREATE INDEX idx_receptors_to_assessment_areas_on_relevant_goal_habitats_geometry_gist ON receptors_to_assessment_areas_on_relevant_goal_habitats USING GIST (geometry);
CREATE INDEX idx_receptors_to_assessment_areas_on_relevant_goal_habitats ON receptors_to_assessment_areas_on_relevant_goal_habitats (receptor_id);
