/*
 * exceeding_receptors
 * -------------------
 * Tabel met daarin de receptoren waarbij de KDW na realisatie van de behoefte dreigt overschreden te worden (KDW - 70 mol).
 * Het gaat in dit geval om de depositie naar provinciaal beleid.
 */
CREATE TABLE exceeding_receptors (
	receptor_id integer NOT NULL,
	year year_type NOT NULL,

	CONSTRAINT exceeding_receptors_pkey PRIMARY KEY (receptor_id, year),
	CONSTRAINT exceeding_receptors_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors
);


/*
 * foreign_countries
 * -----------------
 * Tabel met daarin de landen met bijbehorende landgroep indeling, zoals deze voor de buitenland-deposities in Monitor gebruikt worden.
 * De groepindeling is als volgt bepaald: Duitsland, Belgie, Frankrijk, Verenigd Koninkrijk en Overig. Overig bevat alle andere beschikbare landen.
 * @column foreign_country_group_index Deze kolom bestaat ter validatie van de vertaling tussen een foreign_country_group_type en bijbehorende index.
 * In {@link sectorgroup_depositions_abroad} wordt de index gebruikt i.p.v. het type. Om zeker te weten dat deze koppeling klopt, worden beide 
 * kolommen hier opgeslagen en getest met een CHECK-constraint. Dit scheelt aan opslag voor de resulterende export file van de inhoud van sectorgroup_depositions_abroad tabel.
 * Om te waarborgen dat er geldige indexen worden ingevoerd zijn er CASTs toegevoegd van integer naar het enum type 
 * en wordt dit getest middels een CHECK-constraint. Deze contraint wordt op NOT VALID gezet om te voorkomen dat PG_DUMP 
 * een verkeerde volgorde hanteerd waardoor de backup niet meer zonder fouten ge-restored kan worden.
 */
CREATE TABLE foreign_countries (
    foreign_country_id integer NOT NULL,
    name text NOT NULL,
    foreign_country_group foreign_country_group_type NOT NULL,
    foreign_country_group_index integer NOT NULL,

    CONSTRAINT foreign_countries_pkey PRIMARY KEY (foreign_country_id)    
);

ALTER TABLE foreign_countries
    ADD CONSTRAINT foreign_countries_check CHECK (foreign_country_group_index::foreign_country_group_type = foreign_country_group) NOT VALID;
