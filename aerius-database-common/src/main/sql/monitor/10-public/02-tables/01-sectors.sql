/*
 * sectors_sectorgroup
 * -------------------
 * Koppeltabel voor sector en sectorgroep.
 */
CREATE TABLE sectors_sectorgroup (
	sector_id integer NOT NULL,
	sectorgroup sectorgroup_type NOT NULL,

	CONSTRAINT sectors_sectorgroup_pkey PRIMARY KEY (sector_id),
	CONSTRAINT sectors_sectorgroup_fkey_sectors FOREIGN KEY (sector_id) REFERENCES sectors
);


/*
 * sectors_abroad
 * --------------
 * Tabel met daarin de buitenlandse sectoren (veelal SNAPsectoren) en de sectorgroup waarin zij ingedeeld zijn.
 * Deze buitenlandse sectoren zijn weergegeven in kolom sector_abroad_id; hier kunnen echter ook andere sectoren tussen staan die 
 * ook bij de buitenland depositie horen maar geen snapsector zijn, zoals bv de RONS-sector (Rest Of the North Sea) 
 * met id 3840.
 * Dit is de reden dat voor deze kolom de naam 'sector_abroad_id' is gekozen ipv. de voor de hand liggende 'snapsector_id'.
 * Toegevoegd aan deze tabel is de sectorgroep indeling zoals deze gebruikt wordt in monitor, met bijbehorende index, welke wordt 
 * bepaald via de sectorgroup_abroad_type. 
 * Om te waarborgen dat er geldige indexen worden ingevoerd zijn er CASTs toegevoegd van integer naar het enum type 
 * en wordt dit getest middels een CHECK-constraint.
 * Deze contraint wordt op NOT VALID gezet om te voorkomen dat PG_DUMP 
 * een verkeerde volgorde hanteerd waardoor de backup niet meer zonder fouten ge-restored kan worden.
 */
CREATE TABLE sectors_abroad (
	sector_abroad_id integer NOT NULL,
	name text NOT NULL,
	sectorgroup_abroad sectorgroup_abroad_type NOT NULL,
	sectorgroup_abroad_index integer NOT NULL,

	CONSTRAINT sectors_abroad_pkey PRIMARY KEY (sector_abroad_id)
);

ALTER TABLE sectors_abroad 
	ADD CONSTRAINT sectors_abroad_check CHECK (sectorgroup_abroad_index::sectorgroup_abroad_type = sectorgroup_abroad) NOT VALID;
