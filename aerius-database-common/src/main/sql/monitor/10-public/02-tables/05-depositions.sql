/*
 * sector_depositions_no_policies
 * ------------------------------
 * Depositiebijdrage per sector zonder beleid.
 */
CREATE TABLE sector_depositions_no_policies (
	year year_type NOT NULL,
	sector_id integer NOT NULL,
	receptor_id integer NOT NULL,
	deposition posreal NOT NULL,

	CONSTRAINT sector_depositions_no_policies_pkey PRIMARY KEY (receptor_id, year, sector_id),
	CONSTRAINT sector_depositions_no_policies_fkey_sectors FOREIGN KEY (sector_id) REFERENCES sectors,
	CONSTRAINT sector_depositions_no_policies_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors
);

CREATE INDEX sector_depositions_no_policies_idx_receptor ON sector_depositions_no_policies (receptor_id);


/*
 * other_depositions
 * -----------------
 * Depositiebijdrage die niet aan een sector is toe te kennen.
 */
CREATE TABLE other_depositions (
	year year_type NOT NULL,
	other_deposition_type other_deposition_type NOT NULL,
	receptor_id integer NOT NULL,
	deposition real NOT NULL,

	CONSTRAINT other_depositions_pkey PRIMARY KEY (receptor_id, year, other_deposition_type),
	CONSTRAINT other_depositions_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors
);


/*
 * depositions_no_policies
 * -----------------------
 * Total depositiebijdrage per receptor zonder beleid.
 * Bevat zowel bijdrage van sectoren als de overige depositie (de bijdrage die niet aan een sector toe te kennen is).
 */
CREATE TABLE depositions_no_policies (
	receptor_id integer NOT NULL,
	year year_type NOT NULL,
	deposition posreal NOT NULL,

	CONSTRAINT depositions_no_policies_pkey PRIMARY KEY (receptor_id, year),
	CONSTRAINT depositions_no_policies_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors
);

CREATE INDEX depositions_no_policies_idx_year ON depositions_no_policies (year);


/*
 * delta_depositions_no_policies
 * -----------------------------
 * Totale depositiebijdrage per receptor zonder beleid.
 * Bevat zowel de bijdrage van sectoren en buitenland depositie als de overige depositie (de bijdrage die niet aan een sector toe te kennen is).
 */
CREATE TABLE delta_depositions_no_policies (
	receptor_id integer NOT NULL,
	year year_type NOT NULL,
	delta_deposition real NOT NULL,

	CONSTRAINT delta_depositions_no_policies_pkey PRIMARY KEY (receptor_id, year),
	CONSTRAINT delta_depositions_no_policies_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors
);


/*
 * sectorgroup_depositions_abroad
 * ------------------------------
 * Depositiebijdrage buitenland per jaar, landgroep en buitenlandse sectorgroep.
 * Omdat de databestanden erg groot worden als we de enum types {@link foreign_country_group_type} en {@link sectorgroup_abroad_type} 
 * als tekststring benoemen, is er voor gekozen om in plaats daarvan de index binnen de enum op te slaan (resp. veld foreign_country_group_index 
 * en sectorgroup_abroad_index). Om te waarborgen dat er geldige indexen worden ingevoerd zijn er CASTs toegevoegd van integer naar het enum type 
 * en wordt dit getest middels een CHECK-constraint. Deze contraints worden op NOT VALID gezet om te voorkomen dat PG_DUMP 
 * een verkeerde volgorde hanteerd waardoor de backup niet meer zonder fouten ge-restored kan worden.
 */
CREATE TABLE sectorgroup_depositions_abroad (
	year year_type NOT NULL,
	foreign_country_group_index integer NOT NULL,
	sectorgroup_abroad_index integer NOT NULL,
	receptor_id integer NOT NULL,
	deposition posreal NOT NULL,

	CONSTRAINT sectorgroup_depositions_abroad_pkey PRIMARY KEY (receptor_id, year, foreign_country_group_index, sectorgroup_abroad_index),
	CONSTRAINT sectorgroup_depositions_abroad_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors
);

ALTER TABLE sectorgroup_depositions_abroad 
	ADD CONSTRAINT sectorgroup_depositions_abroa_foreign_country_group_index_check CHECK (foreign_country_group_index::foreign_country_group_type IS NOT NULL) NOT VALID,
    ADD CONSTRAINT sectorgroup_depositions_abroad_sectorgroup_abroad_index_check CHECK (sectorgroup_abroad_index::sectorgroup_abroad_type IS NOT NULL) NOT VALID;
