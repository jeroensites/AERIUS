/*
 * ae_round_deposition
 * -------------------
 * Rond de meegegeven depositie-waarde af op 6 decimalen en retourneert een numeric. 
 */
CREATE OR REPLACE FUNCTION ae_round_deposition(v_deposition numeric)
    RETURNS numeric AS 
$BODY$
    SELECT ROUND(v_deposition, 6)
$BODY$
LANGUAGE sql IMMUTABLE;


/*
 * ae_round_surface
 * ----------------
 * Rond de meegegeven oppervlakte (in meters) af en retourneert een numeric. 
 */
CREATE OR REPLACE FUNCTION ae_round_surface(v_surface numeric)
    RETURNS numeric AS 
$BODY$
    SELECT ROUND(v_surface)
$BODY$
LANGUAGE sql IMMUTABLE;


/*
 * ae_get_nitrogen_load_classification
 * -----------------------------------
 * Retourneert de stikstofbelastingsclassificatie, gegeven de KDW en de depositie in molen.
 */
CREATE OR REPLACE FUNCTION ae_get_nitrogen_load_classification(v_critical_deposition numeric, v_total_deposition numeric)
    RETURNS nitrogen_load_classification AS
$BODY$
DECLARE
    v_delta_deposition numeric;

BEGIN
    v_delta_deposition := v_total_deposition - v_critical_deposition;
    RETURN (CASE WHEN v_delta_deposition >= -70 AND v_delta_deposition <= 0 THEN 'near_overload'
            WHEN v_delta_deposition > 0 AND v_delta_deposition <= 70 THEN 'light_overload'
            WHEN v_delta_deposition > 70 AND v_delta_deposition < v_critical_deposition THEN 'moderate_overload'
            WHEN v_delta_deposition >= v_critical_deposition THEN 'heavy_overload'
            ELSE 'no_nitrogen_problem'
        END);
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;
