/*
 * calculation_results_view
 * ------------------------
 * Alle emission results (concentraties, deposities en overschrijdingsdagen) per calculation en receptor.
 * Gebruik 'calculation_id', 'receptor_id', 'substance_id' en 'result_type' in de WHERE-clause.
 */
CREATE OR REPLACE VIEW calculation_results_view AS
SELECT
	calculation_id,
	receptor_id,
	substance_id,
	result_type,
	result

	FROM calculation_results
		INNER JOIN calculation_result_sets USING (calculation_result_set_id)

	WHERE result_set_type = 'total'
;


/*
 * calculation_sector_results_view
 * -------------------------------
 * Alle emission results (concentraties, deposities en overschrijdingsdagen) per calculation en receptor.
 * Gebruik 'calculation_id', 'receptor_id', 'substance_id' en 'result_type' in de WHERE-clause.
 */
CREATE OR REPLACE VIEW calculation_sector_results_view AS
SELECT
	calculation_id,
	receptor_id,
	substance_id,
	result_type,
	result_set_type_key AS sector_id,
	result

	FROM calculation_results
		INNER JOIN calculation_result_sets USING (calculation_result_set_id)

	WHERE result_set_type = 'sector'
;


/*
 * calculation_point_results_view
 * ------------------------------
 * Alle emission results (concentraties, deposities en overschrijdingsdagen) per calculation en custom points.
 * Gebruik 'calculation_id', 'receptor_id', 'substance_id' en 'result_type' in de WHERE-clause.
 */
CREATE OR REPLACE VIEW calculation_point_results_view AS
SELECT
	calculation_id,
	calculation_point_id,
	label,
	substance_id,
	result_type,
	result,
	calculation_points.geometry

	FROM calculation_point_results
		INNER JOIN calculation_result_sets USING (calculation_result_set_id)
		INNER JOIN calculations USING (calculation_id)
		INNER JOIN calculation_points USING (calculation_point_set_id, calculation_point_id)
		
	WHERE result_set_type = 'total'
;


/*
 * calculation_point_sector_results_view
 * -------------------------------------
 * Alle emission results (concentraties, deposities en overschrijdingsdagen) per calculation en custom points.
 * Gebruik 'calculation_id', 'receptor_id', 'substance_id' en 'result_type' in de WHERE-clause.
 */
CREATE OR REPLACE VIEW calculation_point_sector_results_view AS
SELECT
	calculation_id,
	calculation_point_id,
	label,
	substance_id,
	result_type,
	result_set_type_key AS sector_id,
	result,
	calculation_points.geometry

	FROM calculation_point_results
		INNER JOIN calculation_result_sets USING (calculation_result_set_id)
		INNER JOIN calculations USING (calculation_id)
		INNER JOIN calculation_points USING (calculation_point_set_id, calculation_point_id)
		
	WHERE result_set_type = 'sector'
;


/*
 * calculation_summed_deposition_results_view
 * ------------------------------------------
 * Retourneert de gesommeerde NOx en NH3 (= substance_id 11 + 17) projectbijdrage (calculation_deposition) per berekening en receptor.
 * 
 * Gebruik 'calculation_id' in de WHERE-clause.
 */
CREATE OR REPLACE VIEW calculation_summed_deposition_results_view AS
SELECT 
	calculation_id, 
	receptor_id, 
	SUM(result) AS deposition

	FROM calculation_results_view

	WHERE
		substance_id IN (11, 17)
		AND result_type = 'deposition'

	GROUP BY calculation_id, receptor_id
;


/*
 * calculation_point_summed_deposition_results_view
 * ------------------------------------------------
 * Retourneert de gesommeerde NOx en NH3 (= substance_id 11 + 17) projectbijdrage (deposition) per berekening en custom point.
 * 
 * Gebruik 'calculation_id' in de WHERE-clause.
 */
CREATE OR REPLACE VIEW calculation_point_summed_deposition_results_view AS
SELECT 
	calculation_id, 
	calculation_point_id, 
	SUM(result) AS deposition

	FROM calculation_point_results_view

	WHERE
		substance_id IN (11, 17)
		AND result_type = 'deposition'

	GROUP BY calculation_id, calculation_point_id
;


/*
 * calculation_deposition_results_view
 * -----------------------------------
 * Retourneert de projectbijdrage (calculation_deposition) per berekening, receptor en rekenstof.
 * substance_id wordt omgezet naar een calculation_substance_type (rekenstof). Retourneert ook de gesommeerde rekenstof "noxnh3" (= substance_id 11 + 17).
 *
 * Gebruik 'calculation_id' en eventueel 'calculation_substance' in de WHERE-clause.
 */
CREATE OR REPLACE VIEW calculation_deposition_results_view AS
SELECT calculation_id, receptor_id, 'nox'::calculation_substance_type AS calculation_substance, result AS calculation_deposition
	FROM calculation_results_view
	WHERE
		substance_id = 11
		AND result_type = 'deposition'
UNION ALL
SELECT calculation_id, receptor_id, 'nh3'::calculation_substance_type AS calculation_substance, result AS calculation_deposition
	FROM calculation_results_view
	WHERE
		substance_id = 17
		AND result_type = 'deposition'
UNION ALL
SELECT calculation_id, receptor_id, 'noxnh3'::calculation_substance_type AS calculation_substance, deposition AS calculation_deposition
	FROM calculation_summed_deposition_results_view
;


/*
 * calculation_sector_summed_deposition_results_view
 * -------------------------------------------------
 * Retourneert de gesommeerde NOx en NH3 (= substance_id 11 + 17) projectbijdrage (calculation_deposition) per berekening, sector en receptor.
 * 
 * Gebruik 'calculation_id' in de WHERE-clause.
 */
CREATE OR REPLACE VIEW calculation_sector_summed_deposition_results_view AS
SELECT 
	calculation_id, 
	receptor_id, 
	sector_id,
	SUM(result) AS calculation_deposition

	FROM calculation_sector_results_view

	WHERE
		substance_id IN (11, 17)
		AND result_type = 'deposition'

	GROUP BY calculation_id, receptor_id, sector_id
;


/*
 * calculation_sector_deposition_results_view
 * ------------------------------------------
 * Retourneert de projectbijdrage (calculation_deposition) per berekening, receptor en rekenstof.
 * substance_id wordt omgezet naar een calculation_substance_type (rekenstof). Retourneert ook de gesommeerde rekenstof "noxnh3" (= substance_id 11 + 17).
 *
 * Gebruik 'calculation_id' en eventueel 'calculation_substance' in de WHERE-clause.
 */
CREATE OR REPLACE VIEW calculation_sector_deposition_results_view AS
SELECT calculation_id, receptor_id, 'nox'::calculation_substance_type AS calculation_substance, sector_id, result AS calculation_deposition
	FROM calculation_sector_results_view
	WHERE 
		substance_id = 11
		AND result_type = 'deposition'
UNION ALL
SELECT calculation_id, receptor_id, 'nh3'::calculation_substance_type AS calculation_substance, sector_id, result AS calculation_deposition
	FROM calculation_sector_results_view
	WHERE
		substance_id = 17
		AND result_type = 'deposition'
UNION ALL
SELECT calculation_id, receptor_id, 'noxnh3'::calculation_substance_type AS calculation_substance, sector_id, calculation_deposition
	FROM calculation_sector_summed_deposition_results_view
;


/*
 * ae_calculation_all_depositions
 * ------------------------------
 * Convenience SQL functie voor het bepalen van deposities voor verschillende stoffen en combinaties van stoffen voor een berekening (voor 1 situatie of 2 situaties).
 * Hierbij wordt wel rekening gehouden met receptoren waarvoor in de ene berekening wel resultaten zijn en bij de andere niet. 
 * Hierbij wordt geen rekening gehouden met grenswaardes en/of dalingen (deze worden gewoon teruggegeven).
 * 
 * Door gebruik te maken van een SQL functie wordt voorkomen dat de group by in calculation_summed_deposition_results_view parten gaat spelen voor de performance.
 * Er wordt direct op bepaalde berekeningen gefiltered waardoor een kleinere set geretourneerd wordt ipv dat calculation_summed_deposition_results_view voor alle berekeningen wordt uitgevoerd.
 */
CREATE OR REPLACE FUNCTION ae_calculation_all_depositions(v_proposed_calculation_id integer, v_current_calculation_id integer)
	RETURNS TABLE(receptor_id integer, calculation_substance calculation_substance_type, current_deposition real, proposed_deposition real) AS
$BODY$
	SELECT 
		receptor_id,
		COALESCE(current.calculation_substance, proposed.calculation_substance) AS calculation_substance,
		COALESCE(current.calculation_deposition, 0) AS current_deposition,
		COALESCE(proposed.calculation_deposition, 0) AS proposed_deposition

		FROM 
			(SELECT DISTINCT receptor_id 
				FROM calculation_results
					INNER JOIN calculation_result_sets USING (calculation_result_set_id) 
				WHERE calculation_id IN (v_current_calculation_id, v_proposed_calculation_id)
			) AS receptors
			LEFT JOIN (
				SELECT 
						calculation_id AS current_calculation_id, 
						receptor_id, 
						calculation_substance, 
						calculation_deposition

						FROM calculation_deposition_results_view
						WHERE calculation_id = v_current_calculation_id
			) AS current USING (receptor_id)
			LEFT JOIN (
					SELECT 
						calculation_id AS proposed_calculation_id, 
						receptor_id, 
						calculation_substance, 
						calculation_deposition
						FROM calculation_deposition_results_view
					WHERE calculation_id = v_proposed_calculation_id
			) AS proposed USING (receptor_id)
	
		WHERE current.receptor_id IS NULL 
			OR proposed.receptor_id IS NULL 
			OR (current.receptor_id = proposed.receptor_id AND current.calculation_substance = proposed.calculation_substance)
	;
$BODY$
LANGUAGE sql STABLE;


/*
 * calculation_combinations_view
 * -----------------------------
 * Retourneert de verschillende combinaties van berekeningen om zo tot alle mogelijke proposed en current calculation id's te komen die hetzelfde rekenjaar gebruiken.
 * Ook de combinatie van proposed zonder current calculation wordt hierbij meegenomen.
 */
CREATE OR REPLACE VIEW calculation_combinations_view AS
SELECT
	proposed.calculation_id AS proposed_calculation_id,
	current.calculation_id AS current_calculation_id,
	proposed.year

	FROM calculations AS proposed
		INNER JOIN calculations AS current USING (year)
	WHERE proposed.calculation_id != current.calculation_id
UNION ALL
SELECT
	calculation_id AS proposed_calculation_id,
	NULL AS current_calculation_id,
	year

	FROM calculations
;


/*
 * calculation_combination_all_depositions_view
 * --------------------------------------------
 * Retourneert de deposities voor verschillende stoffen en combinaties van stoffen 
 * van verschillende combinaties van berekeningen.
 */
CREATE OR REPLACE VIEW calculation_combination_all_depositions_view AS
SELECT 
	proposed_calculation_id,
	current_calculation_id,
	year,
	(ae_calculation_all_depositions(proposed_calculation_id, current_calculation_id)).calculation_substance,
	(ae_calculation_all_depositions(proposed_calculation_id, current_calculation_id)).receptor_id,
	(ae_calculation_all_depositions(proposed_calculation_id, current_calculation_id)).current_deposition,
	(ae_calculation_all_depositions(proposed_calculation_id, current_calculation_id)).proposed_deposition

	FROM calculation_combinations_view
;


/*
 * ae_has_results_above_threshold
 * ------------------------------
 * Controleer of er resultaten zijn die boven de drempelwaarde zijn voor een specifiek natuurgebied.
 *
 * Hierbij wordt alleen gekeken naar de 'deposition' resultaten in de 'total' resultset.
 * 
 * Deze methode kan gebruikt worden om te controleren of er uiteindelijk resultaten zullen zijn 
 * (na verwijderen van lage resultaten door ae_delete_low_calculation_results)
 * voordat een tussentijdse samenvatting van het gebied naar de gebruiker wordt gestuurd.
 */
CREATE OR REPLACE FUNCTION ae_has_results_above_threshold(v_calculation_id integer, v_assessment_area_id integer)
	RETURNS boolean AS
$BODY$
	SELECT EXISTS
			(SELECT
				1

				FROM calculation_summed_deposition_results_view
					INNER JOIN receptors_to_assessment_areas USING (receptor_id)
					
				WHERE calculation_id = v_calculation_id
					AND receptors_to_assessment_areas.assessment_area_id = v_assessment_area_id
					AND deposition > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal
			)
	;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculation_summed_deposition_results
 * ----------------------------------------
 * Retourneert de gesommeerde NOx en NH3 (= substance_id 11 + 17) depositie (calculation_deposition) per receptor voor een berekening.
 * 
 * Functie-versie van {@link calculation_summed_deposition_results_view} waarbij de calculation_id in de group by wordt gezet.
 */
CREATE OR REPLACE FUNCTION ae_calculation_summed_deposition_results(v_calculation_id integer)
	RETURNS TABLE(calculation_id integer, receptor_id integer, deposition real) AS
$BODY$
SELECT 
	calculation_id, 
	receptor_id, 
	SUM(result) AS deposition

	FROM calculation_results_view

	WHERE
		substance_id IN (11, 17)
		AND result_type = 'deposition'
		AND calculation_id = v_calculation_id

	GROUP BY calculation_id, receptor_id
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculation_point_summed_deposition_results
 * ----------------------------------------------
 * Retourneert de gesommeerde NOx en NH3 (= substance_id 11 + 17) depositie (calculation_deposition) per custom point voor een berekening.
 */
CREATE OR REPLACE FUNCTION ae_calculation_point_summed_deposition_results(v_calculation_id integer)
	RETURNS TABLE(calculation_id integer, calculation_point_id integer, deposition real) AS
$BODY$
SELECT 
	calculation_id, 
	calculation_point_id, 
	SUM(result) AS deposition

	FROM calculation_point_results_view

	WHERE
		substance_id IN (11, 17)
		AND result_type = 'deposition'
		AND calculation_id = v_calculation_id

	GROUP BY calculation_id, calculation_point_id
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculations_max_summed_deposition_results
 * ---------------------------------------------
 * Retourneert het maximum van NOx + NH3 (= substance_id 11 + 17) depositie (calculation_deposition) per receptor over meerdere berekeningen.
 */
CREATE OR REPLACE FUNCTION ae_calculations_max_summed_deposition_results(v_calculation_ids integer[])
	RETURNS TABLE(receptor_id integer, deposition real) AS
$BODY$
SELECT
	receptor_id,
	MAX(deposition) AS deposition

	FROM calculation_summed_deposition_results_view

	WHERE calculation_id = ANY(v_calculation_ids)

	GROUP BY receptor_id
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_calculation_point_max_summed_deposition_results
 * --------------------------------------------------
 * Retourneert het maximum van NOx + NH3 (= substance_id 11 + 17) depositie (calculation_deposition) per receptor over meerdere berekeningen.
 */
CREATE OR REPLACE FUNCTION ae_calculation_point_max_summed_deposition_results(v_calculation_ids integer[])
	RETURNS TABLE(calculation_point_id integer, deposition real) AS
$BODY$
SELECT
	calculation_point_id,
	MAX(deposition) AS deposition

	FROM calculation_point_summed_deposition_results_view

	WHERE calculation_id = ANY(v_calculation_ids)

	GROUP BY calculation_point_id
;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_delete_low_calculation_results
 * ---------------------------------
 * Verwijder alle resultaten van een berekening die kleiner dan of gelijk aan 0.
 *
 * Hierbij wordt alleen gekeken naar de 'deposition' resultaten in de 'total' resultset. De receptoren worden vervolgens wel uit alle
 * resultsets verwijderd (dus ook uit de 'sector' resultsets en andere resulttypes zoals 'concentration').
 *
 */
CREATE OR REPLACE FUNCTION ae_delete_low_calculation_results(v_calculation_id integer)
	RETURNS void AS
$BODY$
	DELETE FROM calculation_results

	USING calculation_result_sets

	WHERE
		calculation_results.calculation_result_set_id = calculation_result_sets.calculation_result_set_id
		AND calculation_id = v_calculation_id
		AND receptor_id IN
			(SELECT
				DISTINCT receptor_id

				FROM ae_calculation_summed_deposition_results(v_calculation_id)
				WHERE
					NOT (deposition > 0)
			) -- receptors_to_delete
	;
$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_project_calculation
 * -------------------------------
 * Berekening voor een projectberekening.
 *
 * Hierbij worden de resultaten van de beoogde situatie als basis gebruikt, indien aanwezig worden hiervan de resultaten
 * van een referentiesituatie en/of salderingssituatie van afgetrokken. De hexagonenset voor de beoogde situatie is leidend.
 *
 */
CREATE OR REPLACE FUNCTION ae_scenario_project_calculation(v_proposed_calculation_id integer, v_reference_calculation_id integer, v_netting_calculation_id integer)
	RETURNS TABLE(calculation_id integer, receptor_id integer, deposition real) AS

$BODY$
	SELECT
		proposed_calculation_results.calculation_id,
		receptor_id,
		proposed_calculation_results.deposition
			- COALESCE(reference_calculation_results.deposition, 0)
			- COALESCE(netting_calculation_results.deposition, 0)
			AS deposition

	FROM ae_calculation_summed_deposition_results(v_proposed_calculation_id) AS proposed_calculation_results
		LEFT JOIN ae_calculation_summed_deposition_results(v_reference_calculation_id) AS reference_calculation_results USING (receptor_id)
		LEFT JOIN ae_calculation_summed_deposition_results(v_netting_calculation_id) AS netting_calculation_results USING (receptor_id)

	WHERE ABS(proposed_calculation_results.deposition
			- COALESCE(reference_calculation_results.deposition, 0)
			- COALESCE(netting_calculation_results.deposition, 0)) > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal

$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_calculation_point_project_results
 * ---------------------------------------------
 * Resultaten voor een projectberekening voor custom points.
 *
 * Hierbij worden de resultaten van de beoogde situatie als basis gebruikt, indien aanwezig worden hiervan de resultaten
 * van een referentiesituatie en/of salderingssituatie van afgetrokken. De puntenset voor de beoogde situatie is leidend.
 *
 */
CREATE OR REPLACE FUNCTION ae_scenario_calculation_point_project_results(v_proposed_calculation_id integer, v_reference_calculation_id integer, v_netting_calculation_id integer)
	RETURNS TABLE(calculation_id integer, calculation_point_id integer, deposition real) AS

$BODY$
	SELECT
		proposed_calculation_results.calculation_id,
		calculation_point_id,
		proposed_calculation_results.deposition
			- COALESCE(reference_calculation_results.deposition, 0)
			- COALESCE(netting_calculation_results.deposition, 0)
			AS deposition

	FROM ae_calculation_point_summed_deposition_results(v_proposed_calculation_id) AS proposed_calculation_results
		LEFT JOIN ae_calculation_point_summed_deposition_results(v_reference_calculation_id) AS reference_calculation_results USING (calculation_point_id)
		LEFT JOIN ae_calculation_point_summed_deposition_results(v_netting_calculation_id) AS netting_calculation_results USING (calculation_point_id)

	WHERE ABS(proposed_calculation_results.deposition
			- COALESCE(reference_calculation_results.deposition, 0)
			- COALESCE(netting_calculation_results.deposition, 0)) > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal

$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_temporary_calculation
 * ---------------------------------
 * Berekening voor een berekening voor tijdelijke bijdrage of effect.
 *
 * Hierbij worden de resultaten van het maximum van alle tijdelijke situatie als basis gebruikt.
 * De resultaten van een salderingssituatie en/of een referentiesituatie (indien aanwezig,
 * en alleen bij de berekening tijdelijke effect) worden hiervan afgetrokken. Indien deze niet aanwezig is of niet gebruikt
 * wordt kan een 0 worden meegegeven.
 *
 * De hexagonenset van de vereniging van alle tijdelijke situaties is leidend.
 *
 */
CREATE OR REPLACE FUNCTION ae_scenario_temporary_calculation(v_temporary_calculation_ids integer[], v_reference_calculation_id integer, v_netting_calculation_id integer)
	RETURNS TABLE(calculation_id integer, receptor_id integer, deposition real) AS

$BODY$
	SELECT
		v_temporary_calculation_ids[1],
		receptor_id,
		temporary_calculation_results.deposition
			- COALESCE(reference_calculation_results.deposition, 0)
			- COALESCE(netting_calculation_results.deposition, 0)
			AS deposition

	FROM ae_calculations_max_summed_deposition_results(v_temporary_calculation_ids) AS temporary_calculation_results
		LEFT JOIN ae_calculation_summed_deposition_results(v_reference_calculation_id) AS reference_calculation_results USING (receptor_id)
		LEFT JOIN ae_calculation_summed_deposition_results(v_netting_calculation_id) AS netting_calculation_results USING (receptor_id)

	WHERE ABS(temporary_calculation_results.deposition
			- COALESCE(reference_calculation_results.deposition, 0)
			- COALESCE(netting_calculation_results.deposition, 0)) > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal

$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_calculation_point_temporary_results
 * ---------------------------------
 * Resultaten voor een berekening voor tijdelijke bijdrage of effect voor custom points.
 *
 * Hierbij worden de resultaten van het maximum van alle tijdelijke situatie als basis gebruikt.
 * De resultaten van een salderingssituatie en/of een referentiesituatie (indien aanwezig,
 * en alleen bij de berekening tijdelijke effect) worden hiervan afgetrokken. Indien deze niet aanwezig is of niet gebruikt
 * wordt kan een 0 worden meegegeven.
 *
 * De puntenset van de vereniging van alle tijdelijke situaties is leidend.
 *
 */
CREATE OR REPLACE FUNCTION ae_scenario_calculation_point_temporary_results(v_temporary_calculation_ids integer[], v_reference_calculation_id integer, v_netting_calculation_id integer)
	RETURNS TABLE(calculation_id integer, calculation_point_id integer, deposition real) AS

$BODY$
	SELECT
		v_temporary_calculation_ids[1],
		calculation_point_id,
		temporary_calculation_results.deposition
			- COALESCE(reference_calculation_results.deposition, 0)
			- COALESCE(netting_calculation_results.deposition, 0)
			AS deposition

	FROM ae_calculation_point_max_summed_deposition_results(v_temporary_calculation_ids) AS temporary_calculation_results
		LEFT JOIN ae_calculation_point_summed_deposition_results(v_reference_calculation_id) AS reference_calculation_results USING (calculation_point_id)
		LEFT JOIN ae_calculation_point_summed_deposition_results(v_netting_calculation_id) AS netting_calculation_results USING (calculation_point_id)

	WHERE ABS(temporary_calculation_results.deposition
			- COALESCE(reference_calculation_results.deposition, 0)
			- COALESCE(netting_calculation_results.deposition, 0)) > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal

$BODY$
LANGUAGE SQL VOLATILE;
