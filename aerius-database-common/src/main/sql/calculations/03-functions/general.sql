/*
 * ae_delete_calculation
 * ---------------------
 * Gebruik deze functie om een berekening + resultaten te verwijderen.
 * ON DELETE CASCADE is toegevoegd aan de foreign key constraints van calculation_results, calculation_result_sets, 
 * calculation_point results, calculation_batch_options, development_space_demands en calculation_points. 
 * Hierdoor worden automatisch, en zonder kans op deletion failures, alle records met v_calculation_id uit 
 * calculation_results, calculation_result_sets, calculation_point results en calculation_batch_options verwijderd. 
 * Uit development_space_demands worden de gerelaterde proposed_calculation_id's automatisch verwijderd. 
 * Uit calculation_points worden automatisch de calculation_point_set_id's verwijderd als deze worden verwijderd uit calculation_point_sets.
 * 
 */
CREATE OR REPLACE FUNCTION ae_delete_calculation(v_calculation_id integer)
	RETURNS void AS
$BODY$
DECLARE
	only_calculation_in_set boolean;
	v_calculation_point_set_id integer;
BEGIN
	v_calculation_point_set_id := calculation_point_set_id FROM calculations WHERE calculation_id = v_calculation_id;

	only_calculation_in_set := COUNT(*) = 1 FROM calculations WHERE calculation_point_set_id = v_calculation_point_set_id;

	DELETE FROM development_space_demands WHERE current_calculation_id <> 0 AND current_calculation_id = v_calculation_id;

	IF EXISTS(SELECT table_name FROM information_schema.tables WHERE table_name = 'job_calculations')
		THEN 
			DELETE FROM job_calculations WHERE calculation_id = v_calculation_id;
	END IF;

	DELETE FROM calculations WHERE calculation_id = v_calculation_id;

	IF only_calculation_in_set THEN
		DELETE FROM calculation_point_sets WHERE calculation_point_set_id = v_calculation_point_set_id;
	END IF;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


/*
 * ae_determine_permit_authority
 * -----------------------------
 * Functie om het bevoegd gezag voor een berekening te bepalen, waarbij de berekening een vergunning voorstelt.
 *
 * Dit door te kijken waar de receptor met de hoogste behoefte ligt. De volgorde is:
 * - Bevoegd gezag van het gebied waar de hoogste projectbijdrage is.
 * - Indien meerdere gebieden: grootste relevante oppervlakte binnen het hexagon, of daarna korste afstand zwaartepunt bronnen en hexagon.
 * - Indien geen projectbijdrage in een Nederlands gebied (en wel in een buitenlands-gebied): Provincie waar het zwaartepunt van de bronnen ligt of
 * daarna korste afstand zwaartepunt bronnen en provincie.
 *
 * Let op, er wordt niet meer gebruik gemaakt van rank(), alleen nog maar één ORDER BY. Maar er zijn wel twee manieren waarop er meerdere gebieden
 * kunnen zijn. Beide gevallen worden nu uniform opgepakt in de ORDER BY:
 * - De receptor met hoogste development_space_demand kan in meerdere gebieden tegelijk liggen, dan krijg je duplicatie door de INNER JOIN.
 * - De hoogste development_space_demand kan gelijk zijn voor verschillende receptors, wat weer meerdere gebieden kan betekenen.
 *
 * Deze functie gaat waarschijnlijk aangeroepen worden vanuit Calculator waarbij er nog geen vergunnings-id beschikbaar is. Vandaar de de benodigde
 * informatie los wordt meegegeven:
 *
 * @column v_proposed_calculation_id Id van de berekening met voorgestelde situatie.
 * @column v_current_calculation_id Id van de berekening met huidige situatie (mag NULL zijn).
 * @column v_centroid Middelpunt van de bronnen van de vergunning.
 * @returns Id van bevoegd gezag (authority_id).
 *
 * @todo Not used anymore. Clean up?
 */
CREATE OR REPLACE FUNCTION ae_determine_permit_authority(v_proposed_calculation_id integer, v_current_calculation_id integer, v_centroid geometry)
	RETURNS integer AS
$BODY$
DECLARE
	v_authority_id integer := NULL;
BEGIN
	v_authority_id := authority_id FROM
		(SELECT
			authority_id,
			assessment_area_id,
			receptor_id

			FROM development_space_demands
				INNER JOIN receptors_to_assessment_areas_on_relevant_habitat_view USING (receptor_id) -- A receptor can be in multiple areas
				INNER JOIN assessment_areas USING (assessment_area_id)
				INNER JOIN receptors USING (receptor_id)
				INNER JOIN authorities_view USING (authority_id)

			WHERE
				proposed_calculation_id = v_proposed_calculation_id
				AND current_calculation_id = COALESCE(v_current_calculation_id, 0)
				AND assessment_areas.type = 'natura2000_area'
				AND foreign_authority IS FALSE

			ORDER BY development_space_demand DESC, surface DESC, ST_Distance(receptors.geometry, v_centroid) ASC

			LIMIT 1
		) AS best_authority
	;

	IF v_authority_id IS NULL THEN
		-- Nothing found can really only mean there is no deposition in Dutch areas. Maybe it's abroad or there's nothing relevant, in either case we
		-- then simply take the province closest to the sources centroid point.
		SELECT authority_id INTO STRICT v_authority_id FROM ae_determine_nearest_province(v_centroid) AS province_area_id INNER JOIN province_areas USING (province_area_id);
	END IF;

	RETURN v_authority_id;
END;
$BODY$
LANGUAGE plpgsql STABLE;


/*
 * ae_calculation_results_bounding_box
 * -----------------------------------
 * Functie berekent en retouneert een tabel met de coordinaten die gebruikt kunnen worden
 * voor een bounding box voor calculation results van 1 calculation.
 * De coordinaten zijn voor het linker-beneden punt en het rechter-boven punt van de bounding box.
 * Zoomlevel wordt gebruikt om de grootte van de hexagonen aan de randen mee te nemen.
 */
CREATE OR REPLACE FUNCTION ae_calculation_results_bounding_box(v_calculation_id int, zoom_level int)
	RETURNS TABLE(min_x double precision, min_y double precision, max_x double precision, max_y double precision) AS
$BODY$
DECLARE
	scaling_factor posint;
	hexagon_side_size double precision;
	hexagon_width double precision;
	hexagon_height double precision;
BEGIN
	scaling_factor = 2^(zoom_level-1)::posint;
	hexagon_side_size = sqrt((2/(3*sqrt(3))*10000));		--10000 is a hectare
	hexagon_width = (hexagon_side_size * 2)::double precision;
	hexagon_height = (hexagon_side_size * sqrt(3))::double precision;

	RETURN QUERY
		SELECT MIN(ST_X(ae_determine_coordinates_from_receptor_id(receptor_id))) - hexagon_width,
			MIN(ST_Y(ae_determine_coordinates_from_receptor_id(receptor_id))) - hexagon_height,
			MAX(ST_X(ae_determine_coordinates_from_receptor_id(receptor_id))) + hexagon_width,
			MAX(ST_Y(ae_determine_coordinates_from_receptor_id(receptor_id))) + hexagon_height

			FROM calculation_results results
				INNER JOIN calculation_result_sets USING (calculation_result_set_id)

			WHERE calculation_result_sets.calculation_id = v_calculation_id;
END;
$BODY$
LANGUAGE plpgsql STABLE;
