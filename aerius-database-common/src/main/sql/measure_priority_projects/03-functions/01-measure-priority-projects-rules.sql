/*
 * ae_priority_project_development_rule_exceeding_space_check
 * ----------------------------------------------------------
 * Functie om voor elke receptor binnen de situatie-berekening te bepalen
 * of door de toename van deze situatie een overschrijding van de gereserveerde ruimte in de maatregel ontstaat
 * Toetst alleen op (bijna) overbelaste hexagonen.
 */
CREATE OR REPLACE FUNCTION ae_priority_project_development_rule_exceeding_space_check(v_proposed_calculation_id integer, v_current_calculation_id integer, v_priority_project_reference text)
	RETURNS TABLE(receptor_id integer, rule development_rule_type, passed boolean) AS
$BODY$
	SELECT
		development_space_demands.receptor_id,
		'exceeding_space_check'::development_rule_type,
		ae_has_space(measure_priority_project_development_spaces.available_space, development_space_demand) AS passed

		FROM development_space_demands
			INNER JOIN measure_priority_project_development_spaces USING (receptor_id)
			INNER JOIN measure_priority_projects USING (priority_project_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE
			proposed_calculation_id = v_proposed_calculation_id
			AND current_calculation_id = COALESCE(v_current_calculation_id, 0)
			AND measure_priority_projects.priority_project_reference = v_priority_project_reference
			AND non_exceeding_receptors.receptor_id IS NULL
	;
$BODY$
LANGUAGE SQL STABLE;


/*
 * ae_priority_project_development_rule_checks
 * -------------------------------------------
 * Bepaal de uitkomsten van alle beleidsregels voor de opgegeven maatregel (bij toepassen van de opgegeven berekeningen).
 */
CREATE OR REPLACE FUNCTION ae_priority_project_development_rule_checks(v_proposed_calculation_id integer, v_current_calculation_id integer, v_priority_project_reference text)
	RETURNS TABLE(receptor_id integer, rule development_rule_type, passed boolean) AS
$BODY$
	SELECT * FROM ae_priority_project_development_rule_exceeding_space_check(v_proposed_calculation_id, v_current_calculation_id, v_priority_project_reference);
$BODY$
LANGUAGE SQL STABLE;
