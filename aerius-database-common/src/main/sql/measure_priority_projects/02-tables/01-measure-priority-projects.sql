
/*
 * measure_priority_projects
 * -------------------------
 * Bevat de maatregelen met referentie voor gebruik in Calculator.
 * Deze tabel wordt gesynchroniseerd vanuit Register.
 *
 * @see measure_priority_projects_view
 */
CREATE TABLE measure_priority_projects (
	priority_project_id integer NOT NULL,
	priority_project_reference text NOT NULL UNIQUE,
	name text NOT NULL,

	CONSTRAINT measure_priority_projects_pkey PRIMARY KEY (priority_project_id)
);


/*
 * measure_priority_project_development_spaces
 * -------------------------------------------
 * Bevat de initieel beschikbare, toegewezen en beschikbare ontwikkelingsruimte per maatregel.
 * Deze tabel wordt gesynchroniseerd vanuit Register.
 *
 * @see measure_priority_project_development_spaces_view
 */
CREATE TABLE measure_priority_project_development_spaces (
	priority_project_id integer NOT NULL,
	receptor_id integer NOT NULL,
	initial_available_space posreal NOT NULL,
	assigned_space posreal NOT NULL,
	available_space real NOT NULL,

	CONSTRAINT measure_priority_project_development_spaces_pkey PRIMARY KEY (priority_project_id, receptor_id),
	CONSTRAINT measure_priority_project_development_spaces_fkey_receptors FOREIGN KEY (receptor_id) REFERENCES receptors
);
