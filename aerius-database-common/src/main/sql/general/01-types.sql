/*
 * emission_result_type
 * --------------------
 * Geeft aan wat voor type een resultaat is.
 */
CREATE TYPE emission_result_type AS ENUM
	('concentration', 'direct_concentration', 'deposition', 'exceedance_days', 'exceedance_hours', 'dry_deposition', 'wet_deposition');


/*
 * year_category_type
 * ------------------
 * Jaarcategorie, voor welke toepassing een jaar wordt gebruikt.
 * source = Het jaar waarvan de bronbestanden zijn berekend.
 * last = Achtergrond depostitie (calculator).
 * past = Een jaar in het verleden die niet 'last' is.
 * future = Prognosejaren.
 * reference = Vergelijkingsjaar voor prognoses.
 * Het referentie jaar kan hierdoor afwijken van het achtergrond-depositie jaar
 */
CREATE TYPE year_category_type AS ENUM
	('source',  'past', 'last', 'future', 'reference');


/*
 * point_weight
 * ------------
 * Typedefinitie voor een punt met een gewicht (wegingsfactor)
 */
CREATE TYPE point_weight AS
(
	point geometry,
	weight fraction
);


/*
 * ae_color_range_rs
 * -----------------
 * Type die aangeeft wat de totalen (aantal receptoren, totale depositie, totale oppervlakte) binnen een kleurbereik zijn.
 * Wordt gebruikt bij de aggregate functie ae_color_range.
 */
CREATE TYPE ae_color_range_rs AS (
	lower_value real,
	color text,
	total numeric
);


/*
 * color_range_type
 * ----------------
 * Type van kleur bereik.
 * Hoort overeen te komen met enum `ColorRangeType` in de Java-code.
 */
CREATE TYPE color_range_type AS ENUM
	('deposition_contribution', 'deposition_difference', 'deposition_background', 'road_max_speed');


/*
 * critical_deposition_classification
 * ----------------------------------
 * Type van kdw (kritische depositie waarde) classificatie.
 * Huidige klassering:
 * - high_sensitivity: zeer gevoelig
 * - normal_sensitivity: gevoelig
 * - low_sensitivity: minder/niet gevoelig
 */
CREATE TYPE critical_deposition_classification AS ENUM
	('high_sensitivity', 'normal_sensitivity', 'low_sensitivity');


/*
 * unit_type
 * ---------
 * Type van eenheid (gebruikt voor emissiefactoren bij plannen).
 */
CREATE TYPE unit_type AS ENUM
	('hectare', 'giga_joule', 'mega_watt_hours', 'count', 'no_unit', 'square_meters', 'tonnes');


/*
 * segment_type
 * ------------
 * Enum voor de onderverdeling van de ontwikkelingsruimte (is onderdeel van depositieruimte).
 */
CREATE TYPE segment_type AS ENUM
	('priority_projects', 'projects', 'permit_threshold', 'priority_subprojects');

/*
 * theme_type
 * ----------
 * Enum voor de verschillende themas.
 */
CREATE TYPE theme_type AS ENUM
  ('nca', 'wnb', 'rbl');

-- This cast is needed by the syncer
CREATE CAST (CHARACTER VARYING AS segment_type) WITH INOUT AS ASSIGNMENT;
