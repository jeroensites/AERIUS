/*
 * ae_assert_equals
 * ----------------
 * Bevestigt dat twee parameters dezelfde waarde hebben. Houdt rekening met NULL-waarden.
 * De gegeven parameters moeten van hetzelfde data type zijn.
 */
CREATE OR REPLACE FUNCTION setup.ae_assert_equals(v_expected anyelement, v_actual anyelement, v_message text = NULL)
	RETURNS void AS
$BODY$
BEGIN
	IF v_actual IS DISTINCT FROM v_expected THEN
		RAISE EXCEPTION 'assert_equals: expected=% actual=% %', v_expected, v_actual, COALESCE('[' || v_message || ']', '');
	END IF;
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;


/*
 * ae_assert_differs
 * -----------------
 * Bevestigt dat twee parameters NIET dezelfde waarde hebben. Houdt rekening met NULL-waarden.
 * De gegeven parameters moeten van hetzelfde data type zijn.
 */
CREATE OR REPLACE FUNCTION setup.ae_assert_differs(v_not_allowed anyelement, v_actual anyelement, v_message text = NULL)
	RETURNS void AS
$BODY$
BEGIN
	IF v_actual IS NOT DISTINCT FROM v_not_allowed THEN
		RAISE EXCEPTION 'assert_differs: not allowed=% actual=% %', v_not_allowed, v_actual, COALESCE('[' || v_message || ']', '');
	END IF;
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;


/*
 * ae_assert_true
 * --------------
 * Bevestigt dat de gegeven conditie TRUE is.
 *
 * Deze assertie is handig voor verschillende soorten checks, bijvoorbeeld om te bevestigen that een query rijen teruggeeft:
 *	PERFORM * FROM some_table WHERE id = 123;
 *	PERFORM setup.ae_assert_true(FOUND);
 *
 * of gebruik één van de vele subquery expressies:
 * 	PERFORM setup.ae_assert_true(EXISTS(SELECT * FROM some_table WHERE id = 123));
 * Meer op: https://www.postgresql.org/docs/current/static/functions-subquery.html
 */
CREATE OR REPLACE FUNCTION setup.ae_assert_true(v_condition boolean, v_message text = NULL)
	RETURNS void AS
$BODY$
BEGIN
	IF v_condition IS NOT TRUE THEN
		RAISE EXCEPTION 'assert_true: condition=% %', v_condition, COALESCE('[' || v_message || ']', '');
	END IF;
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;


/*
 * ae_assert_false
 * ---------------
 * Bevestigt dat de gegeven conditie FALSE is. Let op dat NULL niet als hetzelfde als FALSE wordt beschouwd.
 */
CREATE OR REPLACE FUNCTION setup.ae_assert_false(v_condition boolean, v_message text = NULL)
	RETURNS void AS
$BODY$
BEGIN
	IF v_condition IS NOT FALSE THEN
		RAISE EXCEPTION 'assert_false: condition=% %', v_condition, COALESCE('[' || v_message || ']', '');
	END IF;
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;


/*
 * ae_assert_not_null
 * ------------------
 * Bevestigt dat de gegeven waarde niet NULL is.
 */
CREATE OR REPLACE FUNCTION setup.ae_assert_not_null(v_value anyelement, v_message text = NULL)
	RETURNS void AS
$BODY$
BEGIN
	IF v_value IS NULL THEN
		RAISE EXCEPTION 'assert_not_null: value=% %', v_value, COALESCE('[' || v_message || ']', '');
	END IF;
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;


/*
 * ae_assert_null
 * --------------
 * Bevestigt dat de gegeven waarde NULL is.
 */
CREATE OR REPLACE FUNCTION setup.ae_assert_null(v_value anyelement, v_message text = NULL)
	RETURNS void AS
$BODY$
BEGIN
	IF v_value IS NOT NULL THEN
		RAISE EXCEPTION 'assert_null: value=% %', v_value, COALESCE('[' || v_message || ']', '');
	END IF;
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;
