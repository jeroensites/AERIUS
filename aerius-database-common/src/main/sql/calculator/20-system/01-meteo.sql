/*
 * meteo
 * -----
 * Systeem tabel met daarin de beschikbare meteo data.
 * 
 * Het veld code geeft een unieke code aan, die terugkomt in de gebruikte rekeninstellingen.
 * Het veld description bevat een gebruikersvriendelijke omschrijving (bijv. voor gebruik in UI).
 * Het veld start_year geeft het start jaar van de meteo aan.
 * Het veld end_year geeft het eind jaar van de meteo aan.
 * 
 * In het geval van een enkeljarig meteo bestand zijn start_year en end_year hetzelfde.
 */
CREATE TABLE system.meteo (
	code text NOT NULL,
	description text NOT NULL,
	start_year int NOT NULL,
	end_year int NOT NULL,

	CONSTRAINT meteo_pkey PRIMARY KEY (code)
);
