/*
 * scenario_assessment_area_statistics
 * -----------------------------------
 * De geaggregeerde statistieken van een berekening per resultaat type, hexagoon type en natuurgebied.
 */
CREATE TABLE scenario_assessment_area_statistics (
	job_id integer NOT NULL,
	result_type scenario_result_type NOT NULL,
	hexagon_type hexagon_type NOT NULL,
	assessment_area_id integer NOT NULL,
	result_statistic_type result_statistic_type NOT NULL,
	value real,

	CONSTRAINT scenario_assessment_area_statistics_pkey PRIMARY KEY (job_id, result_type, hexagon_type, assessment_area_id, result_statistic_type),
	CONSTRAINT scenario_assessment_area_statistics_fk_jobs FOREIGN KEY (job_id) REFERENCES jobs ON DELETE CASCADE
);


/*
 * scenario_assessment_area_statistic_markers
 * ------------------------------------------
 * De markers (of receptoren) waar de betreffende statistieken gelden per resultaat type, hexagoon type en natuurgebied.
 */
CREATE TABLE scenario_assessment_area_statistic_markers (
	job_id integer NOT NULL,
	result_type scenario_result_type NOT NULL,
	hexagon_type hexagon_type NOT NULL,
	assessment_area_id integer NOT NULL,
	result_statistic_type result_statistic_type NOT NULL,
	receptor_id integer,

	CONSTRAINT scenario_assessment_area_statistic_markers_pkey PRIMARY KEY (job_id, result_type, hexagon_type, assessment_area_id, result_statistic_type),
	CONSTRAINT scenario_assessment_area_statistic_markers_f_jobs FOREIGN KEY (job_id) REFERENCES jobs ON DELETE CASCADE
);


/*
 * scenario_critical_deposition_area_statistics
 * --------------------------------------------
 * De geaggregeerde statistieken van een berekening per resultaat type, hexagoon type, natuurgebied en habitat type.
 */
CREATE TABLE scenario_critical_deposition_area_statistics (
	job_id integer NOT NULL,
	result_type scenario_result_type NOT NULL,
	hexagon_type hexagon_type NOT NULL,
	assessment_area_id integer NOT NULL,
	critical_deposition_area_id integer NOT NULL,
	result_statistic_type result_statistic_type NOT NULL,
	value real,

	CONSTRAINT scenario_critical_deposition_area_statistics_pkey PRIMARY KEY (job_id, result_type, hexagon_type, assessment_area_id, critical_deposition_area_id, result_statistic_type),
	CONSTRAINT scenario_critical_deposition_area_statistics_f_jobs FOREIGN KEY (job_id) REFERENCES jobs ON DELETE CASCADE
);


/*
 * scenario_assessment_area_chart_statistics
 * -----------------------------------------
 * De geaggregeerde statistieken van een berekening per resultaat type, hexagoon type en natuurgebied voor grafieken.
 */
CREATE TABLE scenario_assessment_area_chart_statistics (
	job_id integer NOT NULL,
	result_type scenario_result_type NOT NULL,
	hexagon_type hexagon_type NOT NULL,
	assessment_area_id integer NOT NULL,
	color_range_type color_range_type NOT NULL,
	lower_bound real NOT NULL,
	cartographic_surface real NOT NULL,

	CONSTRAINT scenario_assessment_area_chart_statistics_pkey PRIMARY KEY (job_id, result_type, hexagon_type, assessment_area_id, color_range_type, lower_bound),
	CONSTRAINT scenario_assessment_area_chart_statistics_f_jobs FOREIGN KEY (job_id) REFERENCES jobs ON DELETE CASCADE
);
