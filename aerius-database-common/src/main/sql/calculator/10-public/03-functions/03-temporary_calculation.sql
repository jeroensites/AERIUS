/*
 * ae_scenario_temporary_calculation_statistics_base
 * -------------------------------------------------
 * Basis functie om statistieken per tijdelijke berekening op te bepalen.
 */
CREATE OR REPLACE FUNCTION ae_scenario_temporary_calculation_statistics_base(v_temporary_calculation_ids integer[], v_reference_calculation_id integer, v_netting_calculation_id integer)
	RETURNS TABLE(calculation_id integer, receptor_id integer, assessment_area_id integer, critical_deposition_area_id integer, cartographic_surface real, deposition real, total_deposition real) AS

$BODY$
	SELECT
		calculation_id,
		receptor_id,
		assessment_area_id,
		critical_deposition_area_id,
		surface * receptor_habitat_coverage AS cartographic_surface,
		calculation_results.deposition AS deposition,
		calculation_results.deposition + background.total_deposition AS total_deposition

	FROM ae_scenario_temporary_calculation(v_temporary_calculation_ids, v_reference_calculation_id, v_netting_calculation_id) AS calculation_results
		INNER JOIN receptors_to_critical_deposition_areas_view USING (receptor_id)
		INNER JOIN calculations USING (calculation_id)
		INNER JOIN depositions_jurisdiction_policies AS background USING (receptor_id, year)

	WHERE type = 'relevant_habitat'
$BODY$
LANGUAGE SQL VOLATILE;
