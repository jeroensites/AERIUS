/*
 * ae_scenario_job_project_calculation
 * -----------------------------------
 * Voer een projectberekening uit op basis van de job-key en de referentie van de beoogde situatie
 */
CREATE OR REPLACE FUNCTION ae_scenario_job_project_calculation(v_job_key text, v_proposed_situation_reference text)
	RETURNS TABLE(calculation_id integer, receptor_id integer, deposition real) AS

$BODY$
	SELECT
		calculation_id,
		receptor_id,
		deposition

	FROM ae_scenario_project_calculation(
			ae_scenario_get_calculation_id_of_situation_reference(v_job_key, v_proposed_situation_reference),
			ae_scenario_get_calculation_id_of_situation_type(v_job_key, 'reference'),
			ae_scenario_get_calculation_id_of_situation_type(v_job_key, 'netting')
		)
$BODY$
LANGUAGE SQL VOLATILE;

/*
 * ae_scenario_project_calculation_statistics_base
 * -----------------------------------------------
 * Basis functie om statistieken per berekening op te bepalen.
 */
CREATE OR REPLACE FUNCTION ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id integer, v_reference_calculation_id integer, v_netting_calculation_id integer)
	RETURNS TABLE(calculation_id integer, receptor_id integer, assessment_area_id integer, critical_deposition_area_id integer, cartographic_surface real, deposition real, total_deposition real) AS

$BODY$
	SELECT
		calculation_id,
		receptor_id,
		assessment_area_id,
		critical_deposition_area_id,
		surface * receptor_habitat_coverage AS cartographic_surface,
		calculation_results.deposition AS deposition,
		calculation_results.deposition + background.total_deposition AS total_deposition

	FROM ae_scenario_project_calculation(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id) AS calculation_results
		INNER JOIN receptors_to_critical_deposition_areas_view USING (receptor_id)
		INNER JOIN calculations USING (calculation_id)
		INNER JOIN depositions_jurisdiction_policies AS background USING (receptor_id, year)

	WHERE type = 'relevant_habitat'
$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_project_calculation_statistics
 * ------------------------------------------
 * Geeft de algemene statistieken over de projectberekening terug.
 */
CREATE OR REPLACE FUNCTION ae_scenario_project_calculation_statistics(v_proposed_calculation_id integer, v_reference_calculation_id integer, v_netting_calculation_id integer)
	RETURNS TABLE(hexagon_type hexagon_type, result_statistic_type result_statistic_type, value real) AS
$BODY$

	SELECT
		'relevant_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'sum_cartographic_surface_increase'::result_statistic_type, 'sum_cartographic_surface_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), coalesce(sum(cartographic_surface) filter ( where deposition > 0 ), 0), coalesce(sum(cartographic_surface) filter ( where deposition < 0 ), 0), max(deposition) filter ( where deposition > 0 ), -min(deposition) filter ( where deposition < 0 ), max(total_deposition)]) AS value

		FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id)
	UNION
	SELECT
		'exceeding_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'sum_cartographic_surface_increase'::result_statistic_type, 'sum_cartographic_surface_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), coalesce(sum(cartographic_surface) filter ( where deposition > 0 ), 0), coalesce(sum(cartographic_surface) filter ( where deposition < 0 ), 0), max(deposition) filter ( where deposition > 0 ), -min(deposition) filter ( where deposition < 0 ), max(total_deposition)]) AS value

		FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE non_exceeding_receptors.receptor_id IS NULL;

$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_project_calculation_receptor_statistics
 * ---------------------------------------------------
 * Geeft de receptors met de hoogste waarde per statistiek over de projectberekening terug.
 */
CREATE OR REPLACE FUNCTION ae_scenario_project_calculation_receptor_statistics(v_proposed_calculation_id integer)
	RETURNS TABLE(hexagon_type hexagon_type, result_statistic_type result_statistic_type, assessment_area_id integer, receptor_id integer) AS
$BODY$

	SELECT
		hexagon_type,
		result_statistic_type,
		(ae_max_with_key(assessment_area_id::numeric, value::numeric)).key::integer as assessment_area_id,
		(ae_max_with_key(receptor_id::numeric, value::numeric)).key::integer as receptor_id

		FROM scenario_calculation_assessment_area_statistic_markers
		JOIN scenario_calculation_assessment_area_statistics USING (job_id, result_type, calculation_id, hexagon_type, assessment_area_id, result_statistic_type)

		WHERE calculation_id = v_proposed_calculation_id

		GROUP BY calculation_id, result_type, hexagon_type, result_statistic_type

$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_project_calculation_fill_calculation_statistics
 * -----------------------------------------------------------
 * Gebruik deze functie om statistieken voor een projectberekening te bepalen en op te slaan voor een specifiek gebied.
 */
CREATE OR REPLACE FUNCTION ae_scenario_project_calculation_fill_calculation_statistics(v_job_id integer, v_proposed_calculation_id integer, v_reference_calculation_id integer, v_netting_calculation_id integer, v_assessment_area_id integer)
	RETURNS void AS
$BODY$

	-- Assessment area
	INSERT INTO scenario_calculation_assessment_area_statistics (job_id, result_type, calculation_id, assessment_area_id, hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'project_calculation'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		'relevant_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'sum_cartographic_surface_increase'::result_statistic_type, 'sum_cartographic_surface_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), coalesce(sum(cartographic_surface) filter ( where deposition > 0 ), 0), coalesce(sum(cartographic_surface) filter ( where deposition < 0 ), 0), max(deposition) filter ( where deposition > 0 ), -min(deposition) filter ( where deposition < 0 ), max(total_deposition)]) AS value

		FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id)
		WHERE assessment_area_id = v_assessment_area_id
		GROUP BY calculation_id, assessment_area_id;

	INSERT INTO scenario_calculation_assessment_area_statistics (job_id, result_type, calculation_id, assessment_area_id, hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'project_calculation'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		'exceeding_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'sum_cartographic_surface_increase'::result_statistic_type, 'sum_cartographic_surface_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), coalesce(sum(cartographic_surface) filter ( where deposition > 0 ), 0), coalesce(sum(cartographic_surface) filter ( where deposition < 0 ), 0), max(deposition) filter ( where deposition > 0 ), -min(deposition) filter ( where deposition < 0 ), max(total_deposition)]) AS value

		FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE assessment_area_id = v_assessment_area_id
			AND non_exceeding_receptors.receptor_id IS NULL

		GROUP BY calculation_id, assessment_area_id;


	-- Assessment area and critical deposition area
	INSERT INTO scenario_calculation_critical_deposition_area_statistics (job_id, result_type, calculation_id, assessment_area_id, critical_deposition_area_id, hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'project_calculation'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		critical_deposition_area_id,
		'relevant_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'sum_cartographic_surface_increase'::result_statistic_type, 'sum_cartographic_surface_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), coalesce(sum(cartographic_surface) filter ( where deposition > 0 ), 0), coalesce(sum(cartographic_surface) filter ( where deposition < 0 ), 0), max(deposition) filter ( where deposition > 0 ), -min(deposition) filter ( where deposition < 0 ), max(total_deposition)]) AS value

		FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id)
		WHERE assessment_area_id = v_assessment_area_id
		GROUP BY calculation_id, assessment_area_id, critical_deposition_area_id;

	INSERT INTO scenario_calculation_critical_deposition_area_statistics (job_id, result_type, calculation_id, assessment_area_id, critical_deposition_area_id, hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'project_calculation'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		critical_deposition_area_id,
		'exceeding_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'sum_cartographic_surface_increase'::result_statistic_type, 'sum_cartographic_surface_decrease'::result_statistic_type, 'max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), coalesce(sum(cartographic_surface) filter ( where deposition > 0 ), 0), coalesce(sum(cartographic_surface) filter ( where deposition < 0 ), 0), max(deposition) filter ( where deposition > 0 ), -min(deposition) filter ( where deposition < 0 ), max(total_deposition)]) AS value

		FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE assessment_area_id = v_assessment_area_id
			AND non_exceeding_receptors.receptor_id IS NULL

		GROUP BY calculation_id, assessment_area_id, critical_deposition_area_id;


	-- Markers
	INSERT INTO scenario_calculation_assessment_area_statistic_markers (job_id, result_type, calculation_id, assessment_area_id, hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (calculation_id, assessment_area_id, result_statistic_type)
		v_job_id,
		'project_calculation'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		'relevant_hexagons'::hexagon_type,
		UNNEST(array ['max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			(SELECT receptor_id FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id) WHERE deposition > 0 AND assessment_area_id = v_assessment_area_id ORDER BY deposition DESC LIMIT 1),
			(SELECT receptor_id FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id) WHERE deposition < 0 AND assessment_area_id = v_assessment_area_id ORDER BY deposition ASC LIMIT 1),
			first_value(receptor_id) OVER (PARTITION BY calculation_id, assessment_area_id ORDER BY total_deposition DESC, receptor_id)
		]) AS receptor_id

		FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id)
		WHERE assessment_area_id = v_assessment_area_id;

	INSERT INTO scenario_calculation_assessment_area_statistic_markers (job_id, result_type, calculation_id, assessment_area_id, hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (calculation_id, assessment_area_id, result_statistic_type)
		v_job_id,
		'project_calculation'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		'exceeding_hexagons'::hexagon_type,
		UNNEST(array ['max_increase'::result_statistic_type, 'max_decrease'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			(SELECT receptor_id FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id) WHERE deposition > 0 AND assessment_area_id = v_assessment_area_id ORDER BY deposition DESC LIMIT 1),
			(SELECT receptor_id FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id) WHERE deposition < 0 AND assessment_area_id = v_assessment_area_id ORDER BY deposition ASC LIMIT 1),
			first_value(receptor_id) OVER (PARTITION BY calculation_id, assessment_area_id ORDER BY total_deposition DESC, receptor_id)
		]) AS receptor_id

		FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE assessment_area_id = v_assessment_area_id
			AND non_exceeding_receptors.receptor_id IS NULL;


	-- Charts
	INSERT INTO scenario_calculation_assessment_area_chart_statistics (job_id, result_type, calculation_id, assessment_area_id, hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'project_calculation'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		'relevant_hexagons'::hexagon_type,
		'deposition_difference'::color_range_type,
		(UNNEST(ae_color_range('deposition_difference'::color_range_type, deposition::numeric, cartographic_surface::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range('deposition_difference'::color_range_type, deposition::numeric, cartographic_surface::numeric))).total::real AS cartographic_surface

		FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id)
		WHERE assessment_area_id = v_assessment_area_id
		GROUP BY calculation_id, assessment_area_id;

	INSERT INTO scenario_calculation_assessment_area_chart_statistics (job_id, result_type, calculation_id, assessment_area_id, hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'project_calculation'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		'exceeding_hexagons'::hexagon_type,
		'deposition_difference'::color_range_type,
		(UNNEST(ae_color_range('deposition_difference'::color_range_type, deposition::numeric, cartographic_surface::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range('deposition_difference'::color_range_type, deposition::numeric, cartographic_surface::numeric))).total::real AS cartographic_surface

		FROM ae_scenario_project_calculation_statistics_base(v_proposed_calculation_id, v_reference_calculation_id, v_netting_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE assessment_area_id = v_assessment_area_id
			AND non_exceeding_receptors.receptor_id IS NULL

		GROUP BY calculation_id, assessment_area_id;

$BODY$
LANGUAGE SQL VOLATILE;
