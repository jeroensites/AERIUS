/*
 * ae_scenario_job_temporary_effect
 * --------------------------------
 * Voer een tijdelijke bijdrage berekening uit op basis van de job-key
 */
CREATE OR REPLACE FUNCTION ae_scenario_job_temporary_effect(v_job_key text)
	RETURNS TABLE(calculation_id integer, receptor_id integer, deposition real) AS
$BODY$
	SELECT
		calculation_id,
		receptor_id,
		deposition

	FROM ae_scenario_temporary_calculation(
			ae_scenario_get_calculation_ids_of_situation_type(v_job_key, 'temporary'),
			ae_scenario_get_calculation_id_of_situation_type(v_job_key, 'reference'),
			ae_scenario_get_calculation_id_of_situation_type(v_job_key, 'netting')
		)
$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_temporary_effect_statistics
 * ---------------------------------------
 * Geeft de algemene statistieken over het maximale tijdelijke effect terug.
 */
CREATE OR REPLACE FUNCTION ae_scenario_temporary_effect_statistics(v_temporary_calculation_ids integer[], v_reference_calculation_id integer, v_netting_calculation_id integer)
	RETURNS TABLE(hexagon_type hexagon_type, result_statistic_type result_statistic_type, value real) AS
$BODY$

	SELECT
		'relevant_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), max(deposition), max(total_deposition)]) AS value

		FROM ae_scenario_temporary_calculation_statistics_base(v_temporary_calculation_ids, v_reference_calculation_id, v_netting_calculation_id)
	UNION
	SELECT
		'exceeding_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), max(deposition), max(total_deposition)]) AS value

		FROM ae_scenario_temporary_calculation_statistics_base(v_temporary_calculation_ids, v_reference_calculation_id, v_netting_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE non_exceeding_receptors.receptor_id IS NULL;

$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_temporary_effect_fill_calculation_statistics
 * --------------------------------------------------------
 * Gebruik deze functie om statistieken voor een maximale tijdelijke effect te bepalen en op te slaan voor een specifiek gebied.
 */
CREATE OR REPLACE FUNCTION ae_scenario_temporary_effect_fill_calculation_statistics(v_job_id integer, v_temporary_calculation_ids integer[], v_reference_calculation_id integer, v_netting_calculation_id integer, v_assessment_area_id integer)
	RETURNS void AS
$BODY$

	-- Assessment area
	INSERT INTO scenario_assessment_area_statistics (job_id, result_type, assessment_area_id, hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		'relevant_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), max(deposition), max(total_deposition)]) AS value

		FROM ae_scenario_temporary_calculation_statistics_base(v_temporary_calculation_ids, v_reference_calculation_id, v_netting_calculation_id)
		WHERE assessment_area_id = v_assessment_area_id
		GROUP BY assessment_area_id;

	INSERT INTO scenario_assessment_area_statistics (job_id, result_type, assessment_area_id, hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		'exceeding_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), max(deposition), max(total_deposition)]) AS value

		FROM ae_scenario_temporary_calculation_statistics_base(v_temporary_calculation_ids, v_reference_calculation_id, v_netting_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE assessment_area_id = v_assessment_area_id
			AND non_exceeding_receptors.receptor_id IS NULL

		GROUP BY assessment_area_id;


	-- Assessment area and critical deposition area
	INSERT INTO scenario_critical_deposition_area_statistics (job_id, result_type, assessment_area_id, critical_deposition_area_id, hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		critical_deposition_area_id,
		'relevant_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), max(deposition), max(total_deposition)]) AS value

		FROM ae_scenario_temporary_calculation_statistics_base(v_temporary_calculation_ids, v_reference_calculation_id, v_netting_calculation_id)
		WHERE assessment_area_id = v_assessment_area_id
		GROUP BY assessment_area_id, critical_deposition_area_id;

	INSERT INTO scenario_critical_deposition_area_statistics (job_id, result_type, assessment_area_id, critical_deposition_area_id, hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		critical_deposition_area_id,
		'exceeding_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), max(deposition), max(total_deposition)]) AS value

		FROM ae_scenario_temporary_calculation_statistics_base(v_temporary_calculation_ids, v_reference_calculation_id, v_netting_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE assessment_area_id = v_assessment_area_id
			AND non_exceeding_receptors.receptor_id IS NULL

		GROUP BY assessment_area_id, critical_deposition_area_id;


	-- Markers
	INSERT INTO scenario_assessment_area_statistic_markers (job_id, result_type, assessment_area_id, hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (calculation_id, assessment_area_id, result_statistic_type)
		v_job_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		'relevant_hexagons'::hexagon_type,
		UNNEST(array ['max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			first_value(receptor_id) OVER (PARTITION BY calculation_id, assessment_area_id ORDER BY deposition DESC, receptor_id),
			first_value(receptor_id) OVER (PARTITION BY calculation_id, assessment_area_id ORDER BY total_deposition DESC, receptor_id)
		]) AS receptor_id

		FROM ae_scenario_temporary_calculation_statistics_base(v_temporary_calculation_ids, v_reference_calculation_id, v_netting_calculation_id)
		WHERE assessment_area_id = v_assessment_area_id;

	INSERT INTO scenario_assessment_area_statistic_markers (job_id, result_type, assessment_area_id, hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (calculation_id, assessment_area_id, result_statistic_type)
		v_job_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		'exceeding_hexagons'::hexagon_type,
		UNNEST(array ['max_temp_increase'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			first_value(receptor_id) OVER (PARTITION BY calculation_id, assessment_area_id ORDER BY deposition DESC, receptor_id),
			first_value(receptor_id) OVER (PARTITION BY calculation_id, assessment_area_id ORDER BY total_deposition DESC, receptor_id)
		]) AS receptor_id

		FROM ae_scenario_temporary_calculation_statistics_base(v_temporary_calculation_ids, v_reference_calculation_id, v_netting_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE assessment_area_id = v_assessment_area_id
			AND non_exceeding_receptors.receptor_id IS NULL;


	-- Charts
	INSERT INTO scenario_assessment_area_chart_statistics (job_id, result_type, assessment_area_id, hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		'relevant_hexagons'::hexagon_type,
		'deposition_difference'::color_range_type,
		(UNNEST(ae_color_range('deposition_difference'::color_range_type, deposition::numeric, cartographic_surface::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range('deposition_difference'::color_range_type, deposition::numeric, cartographic_surface::numeric))).total::real AS cartographic_surface

		FROM ae_scenario_temporary_calculation_statistics_base(v_temporary_calculation_ids, v_reference_calculation_id, v_netting_calculation_id)
		WHERE assessment_area_id = v_assessment_area_id
		GROUP BY assessment_area_id;

	INSERT INTO scenario_assessment_area_chart_statistics (job_id, result_type, assessment_area_id, hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'max_temporary_effect'::scenario_result_type,
		assessment_area_id,
		'exceeding_hexagons'::hexagon_type,
		'deposition_difference'::color_range_type,
		(UNNEST(ae_color_range('deposition_difference'::color_range_type, deposition::numeric, cartographic_surface::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range('deposition_difference'::color_range_type, deposition::numeric, cartographic_surface::numeric))).total::real AS cartographic_surface

		FROM ae_scenario_temporary_calculation_statistics_base(v_temporary_calculation_ids, v_reference_calculation_id, v_netting_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE assessment_area_id = v_assessment_area_id
			AND non_exceeding_receptors.receptor_id IS NULL

		GROUP BY assessment_area_id;

$BODY$
LANGUAGE SQL VOLATILE;

