/*
 * ae_calculation_statistics_base
 * ------------------------------
 * Basis functie om statistieken per berekening op te bepalen. 
 */
CREATE OR REPLACE FUNCTION ae_calculation_statistics_base(v_calculation_id integer)
	RETURNS TABLE(calculation_id integer, receptor_id integer, assessment_area_id integer, critical_deposition_area_id integer, cartographic_surface real, deposition real, total_deposition real) AS
	
$BODY$
	SELECT 
		calculation_id,
		receptor_id,
		assessment_area_id,
		critical_deposition_area_id,
		surface * receptor_habitat_coverage AS cartographic_surface,
		calculation_results.deposition AS deposition,
		calculation_results.deposition + background.total_deposition AS total_deposition

		FROM ae_calculation_summed_deposition_results(v_calculation_id) AS calculation_results 
			INNER JOIN receptors_to_critical_deposition_areas_view USING (receptor_id) 
			INNER JOIN calculations USING (calculation_id)
			INNER JOIN depositions_jurisdiction_policies AS background USING (receptor_id, year)

		WHERE type = 'relevant_habitat'
			AND calculation_results.deposition > ae_constant('PRONOUNCEMENT_THRESHOLD_VALUE')::posreal;
$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_calculation_statistics
 * -------------------------
 * Geeft de algemene statistieken over de berekening terug.
 */
CREATE OR REPLACE FUNCTION ae_calculation_statistics(v_calculation_id integer)
	RETURNS TABLE(calculation_id integer, hexagon_type hexagon_type, result_statistic_type result_statistic_type, value real) AS
$BODY$

	SELECT
		calculation_id,
		'relevant_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), max(deposition), max(total_deposition)]) AS value

		FROM ae_calculation_statistics_base(v_calculation_id)

		GROUP BY calculation_id
	UNION
	SELECT
		calculation_id,
		'exceeding_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), max(deposition), max(total_deposition)]) AS value

		FROM ae_calculation_statistics_base(v_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE non_exceeding_receptors.receptor_id IS NULL

		GROUP BY calculation_id;

$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_fill_calculation_statistics
 * ------------------------------
 * Gebruik deze functie om statistieken voor een berekening te bepalen en op te slaan voor een specifiek gebied. 
 */
CREATE OR REPLACE FUNCTION ae_fill_calculation_statistics(v_job_id integer, v_calculation_id integer, v_assessment_area_id integer)
	RETURNS void AS
$BODY$
	-- Assessment area
	INSERT INTO scenario_calculation_assessment_area_statistics (job_id, result_type, calculation_id, assessment_area_id, hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		'relevant_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), max(deposition), max(total_deposition)]) AS value

		FROM ae_calculation_statistics_base(v_calculation_id)
		WHERE assessment_area_id = v_assessment_area_id
		GROUP BY calculation_id, assessment_area_id;

	INSERT INTO scenario_calculation_assessment_area_statistics (job_id, result_type, calculation_id, assessment_area_id, hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		'exceeding_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), max(deposition), max(total_deposition)]) AS value

		FROM ae_calculation_statistics_base(v_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE assessment_area_id = v_assessment_area_id
			AND non_exceeding_receptors.receptor_id IS NULL

		GROUP BY calculation_id, assessment_area_id;


	-- Statistic markers
	INSERT INTO scenario_calculation_assessment_area_statistic_markers (job_id, result_type, calculation_id, assessment_area_id, hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (calculation_id, assessment_area_id, result_statistic_type)
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		'relevant_hexagons'::hexagon_type,
		UNNEST(array ['max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			first_value(receptor_id) OVER (PARTITION BY calculation_id, assessment_area_id ORDER BY deposition DESC, receptor_id),
			first_value(receptor_id) OVER (PARTITION BY calculation_id, assessment_area_id ORDER BY total_deposition DESC, receptor_id)
		]) AS receptor_id

		FROM ae_calculation_statistics_base(v_calculation_id)
		WHERE assessment_area_id = v_assessment_area_id;

	INSERT INTO scenario_calculation_assessment_area_statistic_markers (job_id, result_type, calculation_id, assessment_area_id, hexagon_type, result_statistic_type, receptor_id)
	SELECT DISTINCT ON (calculation_id, assessment_area_id, result_statistic_type)
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		'exceeding_hexagons'::hexagon_type,
		UNNEST(array ['max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [
			first_value(receptor_id) OVER (PARTITION BY calculation_id, assessment_area_id ORDER BY deposition DESC, receptor_id),
			first_value(receptor_id) OVER (PARTITION BY calculation_id, assessment_area_id ORDER BY total_deposition DESC, receptor_id)
		]) AS receptor_id

		FROM ae_calculation_statistics_base(v_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE assessment_area_id = v_assessment_area_id
			AND non_exceeding_receptors.receptor_id IS NULL;


	-- Chart statistics
	INSERT INTO scenario_calculation_assessment_area_chart_statistics (job_id, result_type, calculation_id, assessment_area_id, hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		'relevant_hexagons'::hexagon_type,
		'deposition_contribution'::color_range_type,
		(UNNEST(ae_color_range('deposition_contribution'::color_range_type, deposition::numeric, cartographic_surface::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range('deposition_contribution'::color_range_type, deposition::numeric, cartographic_surface::numeric))).total::real AS cartographic_surface

		FROM ae_calculation_statistics_base(v_calculation_id)
		WHERE assessment_area_id = v_assessment_area_id
		GROUP BY calculation_id, assessment_area_id;

	INSERT INTO scenario_calculation_assessment_area_chart_statistics (job_id, result_type, calculation_id, assessment_area_id, hexagon_type, color_range_type, lower_bound, cartographic_surface)
	SELECT
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		'exceeding_hexagons'::hexagon_type,
		'deposition_contribution'::color_range_type,
		(UNNEST(ae_color_range('deposition_contribution'::color_range_type, deposition::numeric, cartographic_surface::numeric))).lower_value::real AS lower_bound,
		(UNNEST(ae_color_range('deposition_contribution'::color_range_type, deposition::numeric, cartographic_surface::numeric))).total::real AS cartographic_surface

		FROM ae_calculation_statistics_base(v_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE assessment_area_id = v_assessment_area_id
			AND non_exceeding_receptors.receptor_id IS NULL

		GROUP BY calculation_id, assessment_area_id;

	-- Assessment area and critical deposition area
	INSERT INTO scenario_calculation_critical_deposition_area_statistics (job_id, result_type, calculation_id, assessment_area_id, critical_deposition_area_id, hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		critical_deposition_area_id,
		'relevant_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), max(deposition), max(total_deposition)]) AS value

		FROM ae_calculation_statistics_base(v_calculation_id)
		WHERE assessment_area_id = v_assessment_area_id
		GROUP BY calculation_id, assessment_area_id, critical_deposition_area_id;

	INSERT INTO scenario_calculation_critical_deposition_area_statistics (job_id, result_type, calculation_id, assessment_area_id, critical_deposition_area_id, hexagon_type, result_statistic_type, value)
	SELECT
		v_job_id,
		'situation_result'::scenario_result_type,
		calculation_id,
		assessment_area_id,
		critical_deposition_area_id,
		'exceeding_hexagons'::hexagon_type,
		UNNEST(array ['sum_cartographic_surface'::result_statistic_type, 'max_contribution'::result_statistic_type, 'max_total'::result_statistic_type]) AS result_statistic_type,
		UNNEST(array [sum(cartographic_surface), max(deposition), max(total_deposition)]) AS value

		FROM ae_calculation_statistics_base(v_calculation_id)
			LEFT JOIN non_exceeding_receptors USING (receptor_id)

		WHERE assessment_area_id = v_assessment_area_id
			AND non_exceeding_receptors.receptor_id IS NULL

		GROUP BY calculation_id, assessment_area_id, critical_deposition_area_id;
$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_calculation_receptor_statistics
 * -------------------------------------------
 * Geeft de receptors met de hoogste waarde per statistiek over de berekening terug.
 */
CREATE OR REPLACE FUNCTION ae_scenario_calculation_receptor_statistics(v_result_type scenario_result_type, v_calculation_id integer)
	RETURNS TABLE(result_type scenario_result_type, calculation_id integer, hexagon_type hexagon_type, result_statistic_type result_statistic_type, assessment_area_id integer, receptor_id integer) AS
$BODY$

	SELECT
		result_type,
		calculation_id,
		hexagon_type,
		result_statistic_type,
		(ae_max_with_key(assessment_area_id::numeric, value::numeric)).key::integer as assessment_area_id,
		(ae_max_with_key(receptor_id::numeric, value::numeric)).key::integer as receptor_id

		FROM scenario_calculation_assessment_area_statistic_markers
			INNER JOIN scenario_calculation_assessment_area_statistics USING (job_id, result_type, calculation_id, hexagon_type, assessment_area_id, result_statistic_type)

		WHERE calculation_id = v_calculation_id
			AND result_type = v_result_type

		GROUP BY result_type, calculation_id, hexagon_type, result_statistic_type

$BODY$
LANGUAGE SQL VOLATILE;


/*
 * ae_scenario_receptor_statistics
 * -------------------------------
 * Geeft de receptors met de hoogste waarde per statistiek over de berekening terug.
 */
CREATE OR REPLACE FUNCTION ae_scenario_receptor_statistics(v_result_type scenario_result_type, v_job_id integer)
	RETURNS TABLE(result_type scenario_result_type, hexagon_type hexagon_type, result_statistic_type result_statistic_type, assessment_area_id integer, receptor_id integer) AS
$BODY$

	SELECT
		result_type,
		hexagon_type,
		result_statistic_type,
		(ae_max_with_key(assessment_area_id::numeric, value::numeric)).key::integer as assessment_area_id,
		(ae_max_with_key(receptor_id::numeric, value::numeric)).key::integer as receptor_id

		FROM scenario_assessment_area_statistic_markers
			INNER JOIN scenario_assessment_area_statistics USING (job_id, result_type, hexagon_type, assessment_area_id, result_statistic_type)

		WHERE job_id = v_job_id
			AND result_type = v_result_type

		GROUP BY result_type, hexagon_type, result_statistic_type

$BODY$
LANGUAGE SQL VOLATILE;

