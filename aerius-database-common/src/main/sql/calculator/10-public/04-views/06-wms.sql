/*
 * wms_calculation_exceeding_receptors_view
 * ----------------------------------------
 * WMS view voor overbelaste hexagonen die binnen een rekenresultaat vallen.
 *
 * Gebruik 'calculation_id', 'calculation_substance' en 'zoom_level' in de WHERE-clause.
 */
CREATE OR REPLACE VIEW wms_calculation_exceeding_receptors_view AS
SELECT
	hexagons.receptor_id,
	hexagons.zoom_level,
	calculation_id,
	calculation_substance,
	hexagons.geometry

	FROM hexagons
		INNER JOIN wms_calculation_substance_deposition_results_view USING (receptor_id)
		LEFT OUTER JOIN non_exceeding_receptors USING (receptor_id)

	WHERE non_exceeding_receptors.receptor_id IS NULL
;

/*
 * wms_calculation_substance_deposition_exceeding_results_view
 * -----------------------------------------------------------
 * WMS view geeft dezelfde resultaten als wms_calculation_substance_deposition_results_view maar alleen
 * op (bijna) overbelaste hexagonen
 *
 * Gebruik 'calculation_id', 'calculation_substance' en 'zoom_level' in de WHERE-clause.
 */
CREATE OR REPLACE VIEW wms_calculation_substance_deposition_exceeding_results_view AS
SELECT
	calculation_id,
	receptor_id,
	calculation_substance,
	deposition,
	zoom_level,
	geometry

	FROM wms_calculation_substance_deposition_results_view
		LEFT OUTER JOIN non_exceeding_receptors USING (receptor_id)

	WHERE non_exceeding_receptors.receptor_id IS NULL
;

/*
 * wms_calculations_exceeding_receptors_difference_view
 * ----------------------------------------------------
 * WMS view voor overbelaste hexagonen die binnen een verschil rekenresultaat vallen.
 *
 * Gebruik 'calculation_a_id', 'calculation_b_id', 'calculation_substance' en 'zoom_level' in de WHERE-clause.
 */
CREATE OR REPLACE VIEW wms_calculations_exceeding_receptors_difference_view AS
SELECT
	hexagons.receptor_id,
	hexagons.zoom_level,
	calculation_a_id,
	calculation_b_id,
	calculation_substance,
	hexagons.geometry

	FROM hexagons
		INNER JOIN wms_calculations_substance_deposition_results_difference_view USING (receptor_id)
		LEFT OUTER JOIN non_exceeding_receptors USING (receptor_id)

	WHERE non_exceeding_receptors.receptor_id IS NULL
;
