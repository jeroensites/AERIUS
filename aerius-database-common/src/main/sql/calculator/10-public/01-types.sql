/*
 * hexagon_type
 * ------------
 * Type van een hexagon, gebruikt bij statistieken van een berekening
 */
CREATE TYPE hexagon_type AS ENUM
	('relevant_hexagons', 'exceeding_hexagons');

/*
 * result_statistic_type
 * ---------------------
 * Type van statistiek, gebruikt bij statistieken van een berekening.
 */
CREATE TYPE result_statistic_type AS ENUM
	('sum_cartographic_surface', 'max_contribution', 'max_total', 'sum_cartographic_surface_increase', 'sum_cartographic_surface_decrease', 'max_increase', 'max_decrease', 'max_temp_contribution', 'max_temp_increase');

/*
 * scenario_result_type
 * --------------------
 * Type van een resultaat
 */
CREATE TYPE scenario_result_type AS ENUM
	('situation_result', 'project_calculation', 'max_temporary_contribution', 'max_temporary_effect');
