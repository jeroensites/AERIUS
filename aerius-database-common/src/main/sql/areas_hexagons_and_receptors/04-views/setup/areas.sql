/*
 * build_relevant_habitat_areas_view
 * ---------------------------------
 * View om de relevant_habitat_areas tabel te vullen (o.b.v. habitat_areas).
 *
 * De volgende twee situaties zijn relevant:
 * - Stikstofgevoelige aangewezen habitat (of H9999) binnen een HR-gebied.
 * - Aangewezen habitat- of vogelsoorten binnen een stikstofgevoelig habitat-gebied binnen een HR- of VR-gebied.
 *
 * Alleen het relevante deel van de geometrie wordt opgeslagen, d.w.z. de intersectie tussen N2000-deelgebied en habitatgebied die aan deze condities voldoet.
 *
 * Daarnaast moeten de N2000-deelgebieden, habitats en soorten, de juiste doelstellingsstatus hebben.
 * In het geval dat het deelgebied de definitief-status heeft, dan moet het betreffende habitatgebied ook definitief zijn, of moet één van de soorten in het
 * habitat definitief zijn (bij de tweede conditie).
 * In het geval dat het deelgebied de ontwerp-status heeft, dan hoeven de habitats en soorten niet definitief te zijn.
 */
CREATE OR REPLACE VIEW setup.build_relevant_habitat_areas_view AS
WITH natura2000_directive_area_properties AS (
	SELECT
		natura2000_directive_area_id,
		natura2000_area_id AS assessment_area_id,
		bird_directive,
		habitat_directive,
		design_status AS natura2000_directive_area_design_status,
		geometry

		FROM natura2000_directive_areas
			INNER JOIN natura2000_area_properties USING (natura2000_area_id)
)
SELECT * FROM
	(SELECT
		assessment_area_id,
		habitat_area_id,
		habitat_type_id,
		coverage,
		ST_CollectionExtract(ST_Multi(ST_Union(ST_Intersection(natura2000_directive_area_geometry, habitat_area_geometry))), 3) AS geometry

		FROM
			-- Stikstofgevoelige aangewezen habitat (of H9999) binnen een HR-gebied, met juiste doelstellingstatus
			(SELECT
				assessment_area_id,
				habitat_area_id,
				habitat_type_id,
				natura2000_directive_area_id,
				coverage,
				natura2000_directive_area_properties.geometry AS natura2000_directive_area_geometry,
				habitat_areas.geometry AS habitat_area_geometry

				FROM habitat_areas
					INNER JOIN habitat_types USING (habitat_type_id)
					INNER JOIN habitat_type_critical_depositions_view USING (habitat_type_id)
					INNER JOIN natura2000_directive_area_properties USING (assessment_area_id)
					INNER JOIN habitat_type_relations USING (habitat_type_id)
					LEFT JOIN habitat_properties USING (goal_habitat_type_id, assessment_area_id)
					LEFT JOIN designated_habitats_view USING (habitat_type_id, assessment_area_id)

				WHERE
					sensitive IS TRUE
					AND habitat_directive IS TRUE
					AND (designated_habitats_view.habitat_type_id IS NOT NULL
						OR habitat_types.name ILIKE 'H9999%')
					AND (natura2000_directive_area_design_status = 'ontwerp'
						OR (natura2000_directive_area_design_status = 'definitief' AND habitat_properties.design_status = 'definitief')
						OR habitat_types.name ILIKE 'H9999%')
			UNION

			-- Aangewezen habitat- of vogelsoorten binnen een stikstofgevoelig habitat-gebied binnen een HR- of VR-gebied, met juiste doelstellingstatus
			SELECT
				assessment_area_id,
				habitat_area_id,
				habitat_type_id,
				natura2000_directive_area_id,
				coverage,
				natura2000_directive_area_properties.geometry AS natura2000_directive_area_geometry,
				habitat_areas.geometry AS habitat_area_geometry

				FROM habitat_areas
					INNER JOIN habitat_type_critical_depositions_view USING (habitat_type_id)
					INNER JOIN natura2000_directive_area_properties USING (assessment_area_id)
					INNER JOIN designated_species_to_habitats_view USING (assessment_area_id, habitat_type_id)
					INNER JOIN species USING (species_id)
					INNER JOIN species_properties USING (species_id, assessment_area_id)

				WHERE
					sensitive IS TRUE
					AND ((species_type IN ('breeding_bird_species', 'non_breeding_bird_species') AND bird_directive IS TRUE)
						OR (species_type = 'habitat_species' AND habitat_directive IS TRUE))
					AND (natura2000_directive_area_design_status = 'ontwerp'
						OR (natura2000_directive_area_design_status = 'definitief' AND species_properties.design_status = 'definitief'))

			) AS relevant_habitats

		GROUP BY assessment_area_id, habitat_area_id, habitat_type_id, coverage
	) AS relevant_habitat_areas

	WHERE NOT ST_IsEmpty(geometry)
;


/*
 * build_habitats_view
 * -------------------
 * View om de habitats tabel te vullen (samenvoeging van alle habitat-gebieden van hetzelfde type in een natuurgebied).
 *
 * In de bepaling van de gemiddelde dekkingsgraad/coverage voor een habitat, is de oppervlakte van de losse habitat-gebieden
 * als weging meegenomen.
 */
CREATE OR REPLACE VIEW setup.build_habitats_view AS
SELECT
	assessment_area_id,
	habitat_type_id,
	ae_weighted_avg(coverage::numeric, ST_Area(habitat_areas.geometry)::numeric)::fraction AS habitat_coverage,
	ST_CollectionExtract(ST_Multi(ST_Union(habitat_areas.geometry)), 3) AS geometry

	FROM habitat_areas

	GROUP BY assessment_area_id, habitat_type_id
;


/*
 * build_relevant_habitats_view
 * ----------------------------
 * View om de relevant_habitats tabel te vullen (samenvoeging van alle relevante (delen van) habitat-gebieden van hetzelfde type
 * in een natuurgebied).
 *
 * In de bepaling van de gemiddelde dekkingsgraad/coverage voor een relevant habitat, is de oppervlakte van de losse relevante
 * habitat-gebieden als weging meegenomen. Betreft het een habitat-gebied dat deels relevant is, dan wordt wel de gehele
 * oppervlakte gebruikt om te wegen. (Op deze manier wordt niet de aanname gedaan dat de dekkingsgraad egaal verdeeld is over
 * het gehele habitat-gebied.)
 */
CREATE OR REPLACE VIEW setup.build_relevant_habitats_view AS
SELECT
	assessment_area_id,
	habitat_type_id,
	ae_weighted_avg(habitat_areas.coverage::numeric, ST_Area(habitat_areas.geometry)::numeric)::fraction AS habitat_coverage,
	ST_CollectionExtract(ST_Multi(ST_Union(relevant_habitat_areas.geometry)), 3) AS geometry

	FROM relevant_habitat_areas
		INNER JOIN habitat_areas USING (assessment_area_id, habitat_area_id, habitat_type_id)

	GROUP BY assessment_area_id, habitat_type_id
;
