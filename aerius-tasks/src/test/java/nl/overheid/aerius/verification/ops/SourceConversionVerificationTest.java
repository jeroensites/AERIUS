/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.verification.ops;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.conversion.EngineSourceExpander;
import nl.overheid.aerius.calculation.conversion.OPSSourceConverter;
import nl.overheid.aerius.calculation.conversion.SourceConversionHelper;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.importer.ImporterFacade;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.OPSVersion;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.io.BrnFileReader;
import nl.overheid.aerius.ops.io.BrnFileWriter;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.SridPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.importer.ImportType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.test.TestDomain;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Unit test to verify the conversion of AERIUS sources to OPS sources. This unit test reads a set of gml input files
 * and converts them to OPS source files. Those files are tested against reference OPS source files.
 * Each gml file verification is run as a single test.
 */
class SourceConversionVerificationTest extends BaseDBTest {

  private static final Logger LOG = LoggerFactory.getLogger(SourceConversionVerificationTest.class);
  private static SourceConverter geometryExpander;
  private static SectorCategories sectorCategories;

  @TempDir
  File tempDir;

  @BeforeAll
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    geometryExpander = new OPSSourceConverter(getCalcPMF().getConnection(), null);
    sectorCategories = SectorRepository.getSectorCategories(getCalcPMF(), LocaleUtils.getDefaultLocale());
  }

  /**
   * Constant used to check if all available verification files have been handled. This is just a check if a file
   * might be missed because somewhere in the test something happened, causing the file to be skipped.
   */
  private static final int EXPECTED_COUNT = 11;

  private static final FilenameFilter GML_FILENAME_FILTER = new FilenameFilter() {
    @Override
    public boolean accept(final File dir, final String name) {
      return (name != null) && (ImportType.determineByFilename(name) == ImportType.GML);
    }
  };

  /**
   * Comparator for sorting ops source objects so they can be compared item by item.
   */
  private static final Comparator<OPSSource> OPS_SORT_COMPARATOR = new Comparator<OPSSource>() {
    @Override
    public int compare(final OPSSource o1, final OPSSource o2) {
      final int cx = Double.compare(o1.getPoint().getX(), o2.getPoint().getX());
      final int cy = cx == 0 ? Double.compare(o1.getPoint().getY(), o2.getPoint().getY()) : cx;
      final int d = cy == 0 ? Integer.compare(o1.getDiameter(), o2.getDiameter()) : cy;
      final int dv = d == 0 ? Integer.compare(o1.getDiurnalVariation(), o2.getDiurnalVariation()) : d;
      final int hc = dv == 0 ? Double.compare(o1.getHeatContent(), o2.getHeatContent()) : dv;
      final int s = hc == 0 ? Double.compare(o1.getSpread(), o2.getSpread()) : hc;
      final int psd = s == 0 ? Integer.compare(o1.getParticleSizeDistribution(), o2.getParticleSizeDistribution()) : s;
      return psd == 0 ? Double.compare(o1.getEmissionHeight(), o2.getEmissionHeight()) : psd;
    }
  };

  private static List<File> getGMLFiles() throws FileNotFoundException {
    return FileUtil.getFilesWithExtension(new File(SourceConversionVerificationTest.class.getResource(".").getPath()), GML_FILENAME_FILTER);
  }

  /**
   * Initialize test with a specific file. testId is used to check if number of GML files found is as expected.
   */
  static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> fileNames = new ArrayList<>();
    int i = 0;
    for (final File file : getGMLFiles()) {
      final Object[] f = new Object[2];
      f[0] = i++;
      f[1] = new File(SourceConversionVerificationTest.class.getResource(file.getName()).getPath());
      fileNames.add(f);
    }
    return fileNames;
  }

  @Test
  void testNumberofGMLFiles() throws FileNotFoundException {
    assertEquals(EXPECTED_COUNT, getGMLFiles().size(), "Number of gml files tested");
  }

  @ParameterizedTest
  @MethodSource("data")
  void testConversionValidation(final Integer testId, final File file) throws SQLException, IOException, AeriusException {
    final ImporterFacade importer = new ImporterFacade(getCalcPMF(), new TestDomain().getCategories());
    final String fileWithoutExt = file.getPath().substring(0, file.getPath().lastIndexOf("."));
    final ImportParcel result;
    try (final InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
      result = importer.convertInputStream2ImportResult(file.getName(), inputStream).get(0);
    }
    if (!result.getExceptions().isEmpty()) {
      throw result.getExceptions().get(0);
    }
    final List<OPSSource> opsSources = new ArrayList<>();
    try (final Connection con = getCalcConnection()) {
      // Convert sources to ops points
      conver2OpsSources(result, opsSources, con, List.of(Substance.RESULT_SUBSTANCES));
    }
    assertNotNull(opsSources, "sources may never be null. GML file: " + file.getName());
    for (final Substance substance : Substance.RESULT_SUBSTANCES) {
      LOG.trace("Calculate emission for key: {}", substance.hatch());
      final OPSInputData input = new OPSInputData(OPSVersion.VERSION, RECEPTOR_GRID_SETTINGS.getZoomLevel1().getSurfaceLevel1());
      input.setSubstances(substance.hatch());
      input.setYear(result.getSituation().getYear());
      input.setEmissionResultKeys(
          EnumSet.copyOf(Stream.of(EmissionResultKey.values()).filter(erk -> erk.getSubstance() == substance).collect(Collectors.toSet())));
      input.setEmissionSources(1, opsSources);
      final File opsFile = new File(fileWithoutExt + "_" + substance.getName().toLowerCase() + ".brn");
      // get reference file, if exists for substance check results
      if (opsFile.exists()) {
        LOG.info("Verify {}", opsFile);
        //Write computed ops sources to file to make sure it matches exactly the expected ops sources, including roundings due to file format.
        final File computedTempPath = Files.createDirectories(new File(tempDir, UUID.randomUUID().toString()).toPath()).toFile();
        BrnFileWriter.writeFile(computedTempPath, input.getEmissionSources().get(1), substance, null);
        final File computedTempFile = new File(computedTempPath, BrnFileWriter.getFileName());
        boolean testSuccess = false;
        try (InputStream cs = new FileInputStream(computedTempFile);
            InputStream is = new FileInputStream(opsFile)) {
          // read reference file
          final BrnFileReader edfr = new BrnFileReader(substance);
          final LineReaderResult<OPSSource> expectedReader = edfr.readObjects(is);
          assertTrue(expectedReader.getExceptions().isEmpty(),
              "Check if no errors in ops reference file. GML file: " + file.getName() + ". Exceptions: "
                  + Arrays.toString(expectedReader.getExceptions().toArray()));
          final List<OPSSource> expectedObjects = expectedReader.getObjects();
          // sort sources and reference sources for easy comparing.
          Collections.sort(expectedObjects, OPS_SORT_COMPARATOR);
          // Read computed sources from file again.
          final BrnFileReader comfr = new BrnFileReader(substance);
          final LineReaderResult<OPSSource> computedReader = comfr.readObjects(cs);
          final List<OPSSource> actualObjects = computedReader.getObjects();
          Collections.sort(actualObjects, OPS_SORT_COMPARATOR);
          assertFalse(expectedObjects.isEmpty(), "Check if sources list is not empty. GML file: " + file.getName());
          assertEquals(expectedObjects.size(),
              actualObjects.size(), "Check if same number of ops sources in reference file. GML file: " + file.getName());
          for (int i = 0; i < expectedObjects.size(); i++) {
            actualObjects.get(i).setId(0); //reset id to avoid checking it.
            final OPSSource opsSource = expectedObjects.get(i);
            final SridPoint point = opsSource.getPoint();
            final SridPoint opssPoint = actualObjects.get(i).getPoint();
            assertEquals(opsSource, actualObjects.get(i), "Check if object (" + i + ") equal. GML file: " + file.getName());
            assertEquals(point.getRoundedX() + "," + point.getRoundedY(),
                opssPoint.getRoundedX() + "," + opssPoint.getRoundedY(),
                "Check if x & y equal (" + opsSource + "). GML file: " + file.getName());
            assertEquals(opsSource.getEmission(substance),
                actualObjects.get(i).getEmission(substance), 0.001, "Check emission equals (" + opsSource + "). GML file: " + file.getName());
            assertEquals(opsSource, actualObjects.get(i),
                "Check if object ops characteristics (x:" + opssPoint.getX() + ",y:" + opssPoint.getY() + ") equal. GML file: "
                    + file.getName());
          }
          testSuccess = true;
        } finally {
          //Delete files if test successful, or if trace not enabled.
          if (testSuccess || !LOG.isTraceEnabled()) {
            Files.delete(computedTempFile.toPath());
            Files.delete(computedTempPath.toPath());
          }
        }
      } else if ((input.getEmissionSources() != null) && !input.getEmissionSources().isEmpty()) {
        // if not exists, but we found results it's an error.
        LOG.warn("No ops reference file found while source file has emissions for substance {} ({}), missing file: {}.",
            substance, input.getEmissionSources().size(), opsFile.getName());
      } // else no reference file and no results for substance, so nothing to check just ignore substance
    }
  }

  private void conver2OpsSources(final ImportParcel result, final List<OPSSource> opsSources, final Connection con,
      final List<Substance> substances) throws SQLException, AeriusException {
    final SourceConversionHelper conversionHelper = new SourceConversionHelper(getCalcPMF(), sectorCategories, result.getSituation().getYear());
    for (final EngineSource es : EngineSourceExpander.toEngineSources(con, geometryExpander, conversionHelper,
        result.getSituation(), substances, true)) {
      if (es instanceof OPSSource) {
        opsSources.add((OPSSource) es);
      }
    }
  }
}
