/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.intercept.InterceptRedirect.InterceptRedirectCommand;
import nl.overheid.aerius.intercept.InterceptRemove.InterceptRemoveCommand;
import nl.overheid.aerius.intercept.InterceptRun.InterceptRunCommand;
import nl.overheid.aerius.intercept.InterceptSave.InterceptSaveCommand;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Test class for {@link InterceptCommandLineOptions}.
 */
class InterceptCommandLineOptionsTest {

  @Test
  void testInterceptRedirect() throws ParseException, FileNotFoundException {
    final InterceptCommandLineOptions cmd = getOptions(new String[]{"-interceptRedirect", "inQuename", "outQuename"});
    final InterceptRedirectCommand opts = (InterceptRedirectCommand) cmd.getInterceptOptions();
    assertEquals("inQuename", opts.getInQueueName(), "redirect inQueue");
    assertEquals("outQuename", opts.getOutQueueName(), "redirect outQuename");
    assertTrue(cmd.isIntercept(), "redirect intercept");
  }

  @Test
  void testInterceptRun() throws ParseException, FileNotFoundException {
    final InterceptCommandLineOptions cmd = getOptions(new String[]{"-interceptRun", "myQueue", "ops"});
    final InterceptRunCommand opts = (InterceptRunCommand) cmd.getInterceptOptions();
    assertEquals("myQueue", opts.getQueueName(), "run qeuename");
    assertSame(WorkerType.OPS, opts.getWorkerType(), "run worker type");
    assertTrue(cmd.isIntercept(), "run intercept");
  }

  @Test
  void testInterceptSave() throws ParseException, FileNotFoundException {
    final String path = new File(getClass().getResource(".").getPath()).getAbsolutePath();
    final InterceptCommandLineOptions cmd = getOptions(new String[]{"-interceptSave", "queue", path});
    final InterceptSaveCommand opts = (InterceptSaveCommand) cmd.getInterceptOptions();
    assertEquals("queue", opts.getQueueName(), "save qeuename");
    assertEquals(path, opts.getPath().getAbsolutePath(), "save path");
    assertTrue(cmd.isIntercept(), "save intercept");
  }

  @Test
  void testInterceptRemove() throws ParseException, FileNotFoundException {
    final String path = new File(getClass().getResource(".").getPath()).getAbsolutePath();
    final InterceptCommandLineOptions cmd = getOptions(new String[]{"-interceptRemove", "queue", path});
    final InterceptRemoveCommand opts = (InterceptRemoveCommand) cmd.getInterceptOptions();
    assertEquals("queue", opts.getQueueName(), "remove qeuename");
    assertEquals(path, opts.getPath().getAbsolutePath(), "remove path");
    assertTrue(cmd.isIntercept(), "remove intercept");
  }

  private InterceptCommandLineOptions getOptions(final String[] args) throws ParseException {
    final Options options = new Options();
    options.addOptionGroup(InterceptCommandLineOptions.getOptions());
    final CommandLine cmd = new DefaultParser().parse(options, args);
    return new InterceptCommandLineOptions(cmd);
  }
}
