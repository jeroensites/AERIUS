/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.ReplacementToken;

/**
 * Util class for testing MailWorker.
 */
class MailWorkerTestHelper {

  private static final EnumSet<ReplacementToken> DEFAULT_DB_TOKENS = EnumSet.of(ReplacementToken.MAIL_CONTENT, ReplacementToken.MAIL_SUBJECT);

  private final List<MailMessageData> sendMails = new ArrayList<>();

  public void sendMail(final MailMessageData mailMessageData) {
    sendMails.add(mailMessageData);
  }

  public MailMessageData getMail(final int index) {
    return sendMails.get(index);
  }

  public void validateMailsSend(final int nrOfMails) {
    assertEquals(nrOfMails, sendMails.size(), "Nr of mails send");
    for (final MailMessageData sendMail : sendMails) {
      assertFalse(sendMail.getMailTo().getToAddresses().isEmpty(), "Should have some addresses to send to");
    }
  }

  public void validateSubject(final int index, final MessagesEnum messagesEnum) {
    validateDBReplacementToken(index, ReplacementToken.MAIL_SUBJECT, messagesEnum);
  }

  public void validateContent(final int index, final MessagesEnum messagesEnum) {
    validateDBReplacementToken(index, ReplacementToken.MAIL_CONTENT, messagesEnum);
  }

  public void validateNrOfReplacements(final int expectedDirect, final int expectedFromDatabase) {
    for (final MailMessageData sendMail : sendMails) {
      assertEquals(expectedDirect, sendMail.getReplacements().size(), "Nr of replacement tokens " + represent(sendMail));
      assertEquals(expectedFromDatabase, sendMail.getDbReplacements().size(), "Nr of DB replacements " + represent(sendMail));
    }
  }

  public void validateReplacementTokens(final ReplacementToken... expectedTokens) {
    final Set<ReplacementToken> tokens = new HashSet<>();
    for (final ReplacementToken token : expectedTokens) {
      tokens.add(token);
    }
    validateReplacementTokens(EnumSet.copyOf(tokens));
  }

  public void validateReplacementTokens(final EnumSet<ReplacementToken> expectedTokenSet) {
    for (final MailMessageData sendMail : sendMails) {
      validateReplacementTokens(sendMail, expectedTokenSet);
    }
  }

  public void validateReplacementTokens(final int index, final ReplacementToken... expectedTokens) {
    final Set<ReplacementToken> tokens = new HashSet<>();
    for (final ReplacementToken token : expectedTokens) {
      tokens.add(token);
    }
    validateReplacementTokens(index, EnumSet.copyOf(tokens));
  }

  public void validateReplacementTokens(final int index, final EnumSet<ReplacementToken> expectedTokenSet) {
    validateReplacementTokens(getMail(index), expectedTokenSet);
  }

  private void validateReplacementTokens(final MailMessageData sendMail, final EnumSet<ReplacementToken> expectedTokenSet) {
    final EnumSet<ReplacementToken> actualTokenSet = EnumSet.copyOf(sendMail.getReplacements().keySet());
    assertEquals(expectedTokenSet, actualTokenSet, "Normal replacement tokens " + represent(sendMail));
  }

  public void validateDBReplacementTokens(final EnumSet<ReplacementToken> expectedTokenSet) {
    for (final MailMessageData sendMail : sendMails) {
      final EnumSet<ReplacementToken> actualTokenSet = EnumSet.copyOf(sendMail.getDbReplacements().keySet());
      assertEquals(expectedTokenSet, actualTokenSet, "DB replacement tokens " + represent(sendMail));
    }
  }

  public void validateDBReplacementTokens(final int index, final EnumSet<ReplacementToken> expectedTokenSet) {
    final EnumSet<ReplacementToken> actualTokenSet = EnumSet.copyOf(getMail(index).getDbReplacements().keySet());
    assertEquals(expectedTokenSet, actualTokenSet, "DB replacement tokens " + represent(getMail(index)));
  }

  public void validateDBReplacementToken(final ReplacementToken token, final MessagesEnum messagesEnum) {
    for (final MailMessageData sendMail : sendMails) {
      validateDBReplacementToken(sendMail, token, messagesEnum);
    }
  }

  public void validateDBReplacementToken(final int index, final ReplacementToken token, final MessagesEnum messagesEnum) {
    validateDBReplacementToken(getMail(index), token, messagesEnum);
  }

  private void validateDBReplacementToken(final MailMessageData sendMail, final ReplacementToken token, final MessagesEnum messagesEnum) {
    assertEquals(messagesEnum, sendMail.getDbReplacements().get(token), "DB replacement token value " + represent(sendMail));
  }

  public void validateDefaultExportMails(final ReplacementToken... expectedTokens) {
    final Set<ReplacementToken> tokens = new HashSet<>();
    tokens.add(ReplacementToken.DOWNLOAD_LINK);
    tokens.addAll(Arrays.asList(expectedTokens));
    validateDefaultCalculationMails(tokens.toArray(new ReplacementToken[tokens.size()]));
  }

  private void validateDefaultCalculationMails(final ReplacementToken... expectedTokens) {
    final Set<ReplacementToken> tokens = new HashSet<>();
    tokens.add(ReplacementToken.CALC_CREATION_DATE);
    tokens.add(ReplacementToken.CALC_CREATION_TIME);
    tokens.addAll(Arrays.asList(expectedTokens));
    validateReplacementTokens(tokens.toArray(new ReplacementToken[tokens.size()]));
    validateDBReplacementTokens(DEFAULT_DB_TOKENS);
  }

  public void validateDefaultErrorMails() {
    validateDefaultCalculationMails(ReplacementToken.ERROR_CODE, ReplacementToken.ERROR_MESSAGE, ReplacementToken.ERROR_SOLUTION);
  }

  private String represent(final MailMessageData sendMail) {
    return sendMail.getMailTo().getToAddresses().get(0);
  }
}
