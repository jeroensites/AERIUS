/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.configuration.BrokerConfiguration;
import nl.aerius.taskmanager.client.configuration.ConnectionConfiguration;
import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor;
import nl.aerius.taskmanager.client.mq.RabbitMQWorkerMonitor.RabbitMQWorkerObserver;
import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.WNBMockConnection;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.shared.domain.export.ExportData;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.util.OSUtils;
import nl.overheid.aerius.worker.base.AbstractDBWorker;

/**
 * Base class for Worker tests.
 */
public abstract class BaseDBWorkerTest<T extends AbstractDBWorker<?, ?>> extends BaseDBTest {

  protected final MailWorkerTestHelper mailHelper = new MailWorkerTestHelper();

  protected CalculatorBuildDirector calculatorBuildDirector;

  protected T worker;
  protected TestBrokerConnectionFactory factory;
  protected Properties properties;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    properties = OSUtils.isWindows() ? PropertiesUtil.getFromPropertyFile("worker.properties")
        : PropertiesUtil.getFromPropertyFile("worker_test_linux.properties");
    factory = new TestBrokerConnectionFactory(EXECUTOR, new BrokerConfiguration(properties).buildConnectionConfiguration());
    worker = createWorker();
  }

  @Override
  @AfterEach
  public void tearDown() throws Exception {
    super.tearDown();
    factory.shutdown();
  }

  protected void initCalculator(final PMF pmf, final BrokerConnectionFactory factory) throws IOException {
    try {
      if (calculatorBuildDirector == null) {
        final RabbitMQWorkerMonitor workerMonitor = mock(RabbitMQWorkerMonitor.class);
        doAnswer(invocation -> {
          ((RabbitMQWorkerObserver) invocation.getArgument(0)).updateWorkers(WorkerType.OPS.type().getWorkerQueueName(), 1, 0);
          return null;
        }).when(workerMonitor).addObserver(any());
        calculatorBuildDirector = new CalculatorBuildDirector(getCalcPMF(), factory, 1, workerMonitor, WorkerType.CONNECT,
            null);
      }
    } catch (final SQLException | AeriusException e) {
      throw new RuntimeException(e);
    }
  }

  protected abstract T createWorker() throws IOException, SQLException;

  protected void assertExportedData(final ExportedData taskResult, final ExportData inputData) {
    assertNotNull(taskResult, "Result");
    assertEquals(inputData.isEmailUser(), taskResult.getDownloadInfo() != null, "Result download info");
    assertEquals(inputData.isReturnFile(), taskResult.getFileContent() != null, "Result file content");
    assertEquals(inputData.isReturnFile(), taskResult.getFileName() != null, "Result file name");
    if (inputData.isEmailUser()) {
      final String prefix = "https://";
      assertTrue(taskResult.getDownloadInfo().getUrl().startsWith(prefix),
          "Result should start with https://, but was " + taskResult.getDownloadInfo().getUrl());
    }
  }

  protected static class TestBrokerConnectionFactory extends BrokerConnectionFactory {
    //does both nox and nh3..
    private double depositionStartValue = 1.0;
    private double resultDecrementFactor = 0.5;

    public TestBrokerConnectionFactory(final ExecutorService executorService, final ConnectionConfiguration connectionConfiguration) {
      super(executorService, connectionConfiguration);
    }

    @Override
    protected com.rabbitmq.client.Connection createNewConnection() throws IOException {
      return new WNBMockConnection(depositionStartValue, resultDecrementFactor);
    }

    public void setDepostionStartValue(final double depositionStartValue) {
      this.depositionStartValue = depositionStartValue;
    }

    public void setResultDecrementFactor(final double resultDecrementFactor) {
      this.resultDecrementFactor = resultDecrementFactor;
    }

  }
}
