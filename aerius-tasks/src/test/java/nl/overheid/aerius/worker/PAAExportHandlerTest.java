/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.lowagie.text.FontFactory;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.export.PdfExportWorkerConfiguration;
import nl.overheid.aerius.export.PdfExportWorkerFactory;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.paa.PAAExportHandler;
import nl.overheid.aerius.shared.Constants;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.v2.base.TimeUnit;
import nl.overheid.aerius.shared.domain.v2.geojson.Geometry;
import nl.overheid.aerius.shared.domain.v2.geojson.LineString;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.geojson.Polygon;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.point.CustomCalculationPoint;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioResults;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.MooringMaritimeShippingEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.v2.source.shipping.maritime.StandardMooringMaritimeShipping;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.test.TestDomain;

/**
 * Test class for {@link PAAExportHandler} in combination with {@link CalculatorExportWorker}.
 *
 * Relies heavily on filled AERIUS database.
 */
class PAAExportHandlerTest extends BaseDBWorkerTest<CalculatorExportWorker> {

  //source X:186277 Y:474477
  private static final int XCOORD = 114051;
  private static final int YCOORD = 395806;

  private static TestDomain testDomain;

  @BeforeAll
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    testDomain = new TestDomain(getCalcPMF());
  }

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    getCalcPMF().setAutoCommit(true);
    super.setUp();
    FontFactory.registerDirectory(PAAExportHandlerTest.class.getResource("/fonts/").getFile());
  }

  @Test
  void testHandleWorkLoadPAAInvalidInputData() throws Exception {
    final CalculationInputData paaInputData = new CalculationInputData();
    paaInputData.setQueueName("I_would_like_to_thank_my_parents");
    paaInputData.setEmailAddress("aerius@example.com");
    paaInputData.setExportType(ExportType.PAA);
    paaInputData.setLocale(Constants.LOCALE_NL);

    final Scenario scenario = new Scenario(Theme.WNB);
    final ScenarioSituation situation = new ScenarioSituation();
    situation.getEmissionSourcesList().addAll(getExampleEmissionSources());
    scenario.getSituations().add(situation);
    //year 1 won't play well with DB.
    situation.setYear(1);
    situation.setType(SituationType.PROPOSED);
    scenario.getOptions().getSubstances().add(Substance.NOXNH3);
    scenario.getOptions().getEmissionResultKeys().add(EmissionResultKey.NOXNH3_DEPOSITION);
    paaInputData.setScenario(scenario);

    final AeriusException thrown = assertThrows(AeriusException.class, () -> worker.run(paaInputData, new JobIdentifier("someCorrelation")));
    assertEquals(AeriusExceptionReason.SQL_ERROR, thrown.getReason(), "Reason for exception");
    mailHelper.validateMailsSend(1);
    mailHelper.validateDefaultErrorMails();
  }

  @Test
  void testHandleWorkLoadPAAInputData() throws Exception {
    final CalculationInputData paaInputData = new CalculationInputData();
    paaInputData.setQueueName("I_really_do_not_care");
    paaInputData.setEmailAddress("aerius@example.com");

    paaInputData.setLocale(Constants.LOCALE_NL);
    paaInputData.setExportType(ExportType.PAA);

    final Scenario scenario = new Scenario(Theme.WNB);

    final ScenarioSituation situationOne = new ScenarioSituation();
    situationOne.getEmissionSourcesList().addAll(getExampleEmissionSources());
    situationOne.setName("Death star two");
    situationOne.setYear(TestDomain.YEAR);
    scenario.getSituations().add(situationOne);

    final ScenarioSituation situationTwo = new ScenarioSituation();
    situationTwo.getEmissionSourcesList().addAll(getExampleEmissionSources().subList(0, 2));
    situationTwo.setName("Death star three");
    situationTwo.setYear(TestDomain.YEAR);
    scenario.getSituations().add(situationTwo);

    scenario.getMetaData().setDescription("Building the third death star");
    scenario.getMetaData().setCorporation("The Galactic Empire");
    scenario.getMetaData().setProjectName("A death star");

    scenario.getOptions().setCalculationType(CalculationType.PERMIT);

    scenario.getOptions().getSubstances().add(Substance.NOXNH3);
    scenario.getOptions().getEmissionResultKeys().add(EmissionResultKey.NOXNH3_DEPOSITION);
    paaInputData.setScenario(scenario);

    final ScenarioResults results = new ScenarioResults();
    final ScenarioSituationResults resultsOne = new ScenarioSituationResults();
    results.getResultsPerSituation().put(situationOne.getId(), resultsOne);
    resultsOne.getResults().add(createResultPoint("Rebel Camp", 10.2));
    resultsOne.getResults().add(createResultPoint("The Trap", 10.3));
    final ScenarioSituationResults resultsTwo = new ScenarioSituationResults();
    results.getResultsPerSituation().put(situationTwo.getId(), resultsTwo);
    resultsTwo.getResults().add(createResultPoint("Rebel Camp", 10.4));
    resultsTwo.getResults().add(createResultPoint("The Trap", 10.5));

    final ExportedData taskResult = worker.run(paaInputData, new JobIdentifier("1"));
    assertExportedData(taskResult, paaInputData);

    mailHelper.validateMailsSend(1);
    mailHelper.validateDefaultExportMails(ReplacementToken.PROJECT_NAME);
  }

  @Override
  protected CalculatorExportWorker createWorker() throws IOException, SQLException {
    initCalculator(getCalcPMF(), factory);
    final CalculatorWorkerFactory workerFactory = new CalculatorWorkerFactory(WorkerType.CONNECT);
    final PdfExportWorkerConfiguration configuration = new PdfExportWorkerFactory().createConfiguration(properties);
    final ExportHandler paaExportWorker = new PAAExportHandler(getCalcPMF(), configuration, new JobIdentifier("1"), "1", ReleaseType.CONCEPT);

    return new CalculatorExportWorker(calculatorBuildDirector, getCalcPMF(), factory, workerFactory.createConfiguration(properties),
        paaExportWorker) {
      @Override
      protected boolean sendMail(final MailMessageData mailMessageData) {
        mailHelper.sendMail(mailMessageData);
        return true;
      }
    };
  }

  private CalculationPointFeature createResultPoint(final String name, final double value) {
    final CalculationPointFeature feature = new CalculationPointFeature();
    feature.setGeometry(new Point(XCOORD + 1000, YCOORD - 1000));
    final CustomCalculationPoint calculationPoint = new CustomCalculationPoint();
    calculationPoint.setLabel(name);
    calculationPoint.getResults().put(EmissionResultKey.NH3_DEPOSITION, 10.3);
    feature.setProperties(calculationPoint);
    return feature;
  }

  private List<EmissionSourceFeature> getExampleEmissionSources() throws AeriusException {
    final List<EmissionSourceFeature> sources = new ArrayList<>();

    final int xCoord = XCOORD;
    final int yCoord = YCOORD;

    final Point geometrySource = new Point(xCoord, yCoord);
    final EmissionSourceFeature source1 = getSource("1", geometrySource, "SomeSource", TestDomain.getGenericEmissionSource());
    sources.add(source1);

    final int xCoord1 = xCoord + 100;
    final int yCoord1 = yCoord + 100;

    final Point geometrySource1 = new Point(xCoord1, yCoord1);
    final EmissionSourceFeature source2 = getSource("2", geometrySource1, "SomeFarmSource", testDomain.getFarmEmissionSource());
    source2.getProperties().setCharacteristics(TestDomain.getNonDefaultCharacteristics());
    source2.getProperties().setSectorId(4110);
    sources.add(source2);

    final int xCoord2 = xCoord;
    final int yCoord2 = yCoord1;

    final Polygon geometrySource2 = new Polygon();
    geometrySource2.setCoordinates(new double[][][] {{
        {xCoord, yCoord},
        {xCoord1, yCoord1},
        {xCoord2, yCoord2},
        {xCoord, yCoord},
    }});

    final EmissionSourceFeature source3 = getSource("3", geometrySource2, "SomeOffRoadSource",
        testDomain.getOffRoadMobileEmissionSource(new OffRoadMobileEmissionSource()));
    source3.getProperties().setSectorId(Sector.SECTOR_DEFAULT.getSectorId());
    sources.add(source3);

    final LineString geometrySource3 = new LineString();
    geometrySource3.setCoordinates(new double[][] {
        {xCoord, yCoord},
        {xCoord1, yCoord1},
        {xCoord2, yCoord2}
    });

    final EmissionSourceFeature source4 = getSource("3", geometrySource3, "SomeRoadSource", testDomain.getSRM2EmissionSource());
    source4.getProperties().setSectorId(Sector.SECTOR_DEFAULT.getSectorId());
    sources.add(source4);

    final int xCoord3 = xCoord2 + 100;
    final int yCoord3 = yCoord2 + 100;

    final int nodeXCoord = xCoord2 + 30;
    final int nodeYCoord = yCoord2 + 30;
    final LineString geometrySource4 = new LineString();
    geometrySource4.setCoordinates(new double[][] {
        {xCoord2, yCoord2},
        {xCoord3, yCoord3}
    });
    final LineString routeGeometry = new LineString();
    routeGeometry.setCoordinates(new double[][] {
        {nodeXCoord, nodeYCoord},
        {(nodeXCoord - 1000), (nodeYCoord + 1000)}
    });
    final LineString maritimeRouteGeometry = new LineString();
    maritimeRouteGeometry.setCoordinates(new double[][] {
        {(nodeXCoord - 1000), (nodeYCoord + 1000)},
        {(nodeXCoord - 1000), (nodeYCoord + 1500)}
    });
    final EmissionSourceFeature source5 = getSource("4", geometrySource4, "SomeShipSource",
        getShipEmissionValues(routeGeometry, maritimeRouteGeometry));
    sources.add(source5);

    final EmissionSourceFeature source6 = getSource("5", geometrySource2, "SomePlanSource", testDomain.getPlanEmissionSource());
    source6.getProperties().setSectorId(9000);
    sources.add(source6);

    final int xCoord4 = xCoord3 + 100;
    final int yCoord4 = yCoord3 - 100;

    final LineString geometrySource6 = new LineString();
    geometrySource6.setCoordinates(new double[][] {
        {xCoord3, yCoord3},
        {xCoord4, yCoord4}
    });
    final EmissionSourceFeature source7 = getSource("6", geometrySource6, "SomeInlandRouteSource", testDomain.getInlandRouteEmissionValues());
    source7.getProperties().setSectorId(Sector.SECTOR_DEFAULT.getSectorId());
    sources.add(source7);

    final int xCoord5 = xCoord4 + 100;
    final int yCoord5 = yCoord4 + 100;

    final int nodeXCoord1 = xCoord4 + 30;
    final int nodeYCoord1 = yCoord4 + 30;

    final LineString geometrySource7 = new LineString();
    geometrySource7.setCoordinates(new double[][] {
        {xCoord4, yCoord4},
        {xCoord5, yCoord5}
    });
    final LineString inlandMooringRouteGeometry = new LineString();
    inlandMooringRouteGeometry.setCoordinates(new double[][] {
        {nodeXCoord1, nodeYCoord1},
        {(nodeXCoord1 - 1000), (nodeYCoord1 + 1000)}
    });
    final EmissionSourceFeature source8 = getSource("7", geometrySource7, "SomeInlandMooringSource",
        testDomain.getInlandMooringEmissionSource());
    source8.getProperties().setSectorId(Sector.SECTOR_DEFAULT.getSectorId());
    sources.add(source8);

    return sources;
  }

  private MooringMaritimeShippingEmissionSource getShipEmissionValues(final LineString routeGeometry, final LineString maritimeRouteGeometry) {
    final MooringMaritimeShippingEmissionSource emissionValues = new MooringMaritimeShippingEmissionSource();
    final StandardMooringMaritimeShipping vesselGroupEmissionValues = new StandardMooringMaritimeShipping();
    vesselGroupEmissionValues.setShipCode("TestShip");
    vesselGroupEmissionValues.setDescription("Burgundy");
    vesselGroupEmissionValues.setAverageResidenceTime(300);
    vesselGroupEmissionValues.setShipsPerTimeUnit(30000);
    vesselGroupEmissionValues.setTimeUnit(TimeUnit.YEAR);
    emissionValues.getSubSources().add(vesselGroupEmissionValues);

    final StandardMooringMaritimeShipping vesselGroupEmissionValues1 = new StandardMooringMaritimeShipping();
    vesselGroupEmissionValues1.setShipCode("TestBarge");
    vesselGroupEmissionValues1.setDescription("Flying dutchmen");
    vesselGroupEmissionValues1.setAverageResidenceTime(100);
    vesselGroupEmissionValues1.setShipsPerTimeUnit(15000);
    vesselGroupEmissionValues1.setTimeUnit(TimeUnit.YEAR);
    emissionValues.getSubSources().add(vesselGroupEmissionValues1);
    return emissionValues;
  }

  private <E extends EmissionSource> EmissionSourceFeature getSource(final String id, final Geometry geometry, final String label, final E orgSource) {
    return TestDomain.getSource(id, geometry, label, orgSource);
  }

}
