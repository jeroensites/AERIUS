/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.NotSerializableException;
import java.io.Serializable;
import java.util.HashMap;

import org.junit.jupiter.api.Test;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Envelope;

import nl.aerius.taskmanager.client.util.QueueHelper;
import nl.aerius.taskmanager.test.MockedChannelFactory;

/**
 * Test class for WorkerConsumer. Tests should work without MQ.
 */
class WorkerConsumerTest {

  @Test
  void testHandleDeliveryTestTaskInput() throws IOException {
    final Object returned = handleDeliveryForObject(new TestTaskInput());
    assertTrue(returned instanceof TestTaskOutput, "Returned object should be one of TestTaskOutput");
  }

  @Test
  void testHandleDeliveryTestIntegerDoubleTaskInput() throws IOException {
    final TestIntegerDoubleTaskInput input = new TestIntegerDoubleTaskInput();
    input.setTobeDoubled(2);
    final Object returned = handleDeliveryForObject(input);
    assertTrue(returned instanceof TestIntegerDoubleTaskOutput,
        "Returned object should be one of TestIntegerDoubleTaskOutput");
    final TestIntegerDoubleTaskOutput output = (TestIntegerDoubleTaskOutput) returned;
    assertEquals(4, output.getDoubled(), "Output of doubled");
  }

  @Test
  void testHandleDeliveryWrongObject() throws IOException {
    final Object returned = handleDeliveryForObject(Integer.valueOf(12));
    assertTrue(returned instanceof UnsupportedOperationException, "Should have thrown exception");
  }

  private Object handleDeliveryForObject(final Serializable object) throws IOException {
    final MockWorkerHandler workerHandler = new MockWorkerHandler();
    final WorkerConsumer workerConsumer = new WorkerConsumer(MockedChannelFactory.create(), workerHandler);
    final BasicProperties props = new BasicProperties.Builder()
        .correlationId("SOME")
        .headers(new HashMap<String, Object>())
        .build();
    final Envelope envelope = new Envelope(0, false, null, null);
    workerConsumer.handleDelivery(null, envelope, props, QueueHelper.objectToBytes(object));
    return workerHandler.getLastReturnObject();
  }

  @Test
  void testWorkerConsumerWithoutHandler() throws IOException {
    assertThrows(IllegalArgumentException.class, () -> new WorkerConsumer(MockedChannelFactory.create(), null),
        "Should throw IllegalArgumentException when no handler passed.");
  }

  @Test
  void testConvertingNonSerializableObject() throws IOException {
    assertThrows(NotSerializableException.class, () -> QueueHelper.objectToBytes(new Serializable() {
      private static final long serialVersionUID = -3407379864907106639L;
    }), "Should throw NotSerializableException when unserializable object passed.");
  }

  @Test
  void testWorkerConsumerWithStringAsByteInput() throws IOException, ClassNotFoundException {
    final MockWorkerHandler workerHandler = new MockWorkerHandler();
    final Channel mockChannel = MockedChannelFactory.create();
    final WorkerConsumer workerConsumer = new WorkerConsumer(mockChannel, workerHandler);
    final BasicProperties props = new BasicProperties.Builder()
        .correlationId("SOME").headers(new HashMap<String, Object>()).build();
    final Envelope envelope = new Envelope(0, false, null, null);
    //object should be serialized by QueueHelper.
    assertThrows(IOException.class, () -> workerConsumer.handleDelivery(null, envelope, props, "Invalid".getBytes()),
        "Should throw an exception when passed String instead of byte data");
  }

}
