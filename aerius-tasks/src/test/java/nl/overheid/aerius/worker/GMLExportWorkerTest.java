/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.shared.Constants;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.Theme;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariation;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.characteristics.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.v2.geojson.Point;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.domain.v2.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.test.TestDomain;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.base.AbstractExportWorker.ImmediateExportData;

/**
 * Test class for {@link GMLExportHandler} in combination with {@link CalculatorExportWorker}.
 */
class GMLExportWorkerTest extends BaseDBWorkerTest<CalculatorExportWorker> {

  private static final String DUMMY_EMAIL_ADDRESS = "aerius@example.com";

  private ImmediateExportData handleExport;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    getCalcPMF().setAutoCommit(true);
    super.setUp();
  }

  @Override
  protected CalculatorExportWorker createWorker() throws IOException, SQLException {
    initCalculator(getCalcPMF(), factory);
    handleExport = null;
    final CalculatorWorkerFactory workerFactory = new CalculatorWorkerFactory(WorkerType.CONNECT);
    return new CalculatorExportWorker(calculatorBuildDirector, getCalcPMF(), factory, workerFactory.createConfiguration(properties),
        new GMLExportHandler(getCalcPMF())) {

      @Override
      protected nl.overheid.aerius.worker.base.AbstractExportWorker.ImmediateExportData handleExport(final CalculationInputData inputData,
          final ArrayList<StringDataSource> backupAttachments, final JobIdentifier jobIdentifier) throws Exception {
        handleExport = super.handleExport(inputData, backupAttachments, jobIdentifier);
        return handleExport;
      }

      @Override
      protected boolean sendMail(final MailMessageData mailMessageData) {
        mailHelper.sendMail(mailMessageData);
        return true;
      }

    };
  }

  @Test
  void testHandleWorkLoadGMLInputDataWithError() throws Exception {
    final CalculationInputData inputData = new CalculationInputData();
    inputData.setEmailAddress(DUMMY_EMAIL_ADDRESS);
    final Scenario scenario = new Scenario(Theme.WNB);
    final ScenarioSituation situation = new ScenarioSituation();
    situation.getEmissionSourcesList().addAll(getExampleEmissionSources());
    // Point without coordinates
    situation.getEmissionSourcesList().get(0).setGeometry(new Point());
    scenario.getSituations().add(situation);
    inputData.setScenario(scenario);
    try {
      worker.run(inputData, new JobIdentifier("SomeCorrelation")); // should throw an exception
      fail("expected Exception");
    } catch (final AeriusException e) {
      mailHelper.validateMailsSend(1);
      mailHelper.validateDefaultErrorMails();
    }
  }

  @Test
  void testHandleWorkLoadGMLInputDataWithoutCalculation() throws Exception {
    final CalculationInputData inputData = new CalculationInputData();
    inputData.setEmailAddress(DUMMY_EMAIL_ADDRESS);
    final Scenario scenario = new Scenario(Theme.WNB);
    final ScenarioSituation situation = new ScenarioSituation();
    situation.getEmissionSourcesList().addAll(getExampleEmissionSources());
    scenario.getSituations().add(situation);
    inputData.setScenario(scenario);
    inputData.setExportType(ExportType.GML_WITH_RESULTS);

    assertThrows(AeriusException.class, () -> worker.run(inputData, new JobIdentifier("SomeCorrelation")),
        "Expected AeriusException when no calculation data present.");
    mailHelper.validateMailsSend(1);
    mailHelper.validateDefaultErrorMails();
  }

  private List<EmissionSourceFeature> getExampleEmissionSources() {
    final List<EmissionSourceFeature> sources = new ArrayList<>();
    sources.add(createExampleEmissionSources(Sector.SECTOR_DEFAULT));
    return sources;
  }

  @Test
  void testHandleWorkLoadGMLInputData() throws Exception {
    //test the GML worker by letting it send a mail for an existing calculation.
    final CalculationInputData inputData = createExportData();

    final String propsCorrelationId = UUID.randomUUID().toString();
    final ExportedData taskResult = worker.run(inputData, new JobIdentifier(propsCorrelationId));
    assertExportedData(taskResult, inputData);
    mailHelper.validateMailsSend(1);
    mailHelper.validateDefaultExportMails(ReplacementToken.AERIUS_REFERENCE);
  }

  @Test
  void testHandleWorkLoadGMLInputDataWithJobName() throws Exception {
    //test the GML worker by letting it send a mail for an existing calculation and exisiting job name
    final CalculationInputData inputData = createExportData();
    inputData.setName("calculation name"); // add a name triggers different mail subject

    final String propsCorrelationId = UUID.randomUUID().toString();
    final ExportedData taskResult = worker.run(inputData, new JobIdentifier(propsCorrelationId));
    assertExportedData(taskResult, inputData);
    mailHelper.validateMailsSend(1);
    mailHelper.validateDefaultExportMails(ReplacementToken.AERIUS_REFERENCE, ReplacementToken.JOB);
  }

  @Test
  void testHandleWorkLoadGMLMultipleSectors() throws Exception {
    final CalculationInputData inputData = createExportData();
    inputData.setExportType(ExportType.GML_WITH_SECTORS_RESULTS);
    worker.run(inputData, new JobIdentifier("SomeCorrelation"));
    assertEquals(3, handleExport.getFilesInZip(), "Incorrect number of files zip ");
  }

  private CalculationInputData createExportData() throws SQLException {
    final SectorCategories sc = SectorRepository.getSectorCategories(getCalcPMF(), LocaleUtils.getDefaultLocale());
    final CalculationInputData inputData = new CalculationInputData();
    inputData.setEmailAddress(DUMMY_EMAIL_ADDRESS);
    inputData.setLocale(Constants.LOCALE_NL);
    inputData.setExportType(ExportType.GML_WITH_RESULTS);
    inputData.setQueueName("Never_gonna_find_me");

    final Scenario scenario = new Scenario(Theme.WNB);
    final ScenarioSituation situation = new ScenarioSituation();
    scenario.getSituations().add(situation);
    situation.getEmissionSourcesList().add(createExampleEmissionSources(sc.getSectorById(1800)));
    situation.getEmissionSourcesList().add(createExampleEmissionSources(sc.getSectorById(1100)));
    //situation.getEmissionSources().add(createExampleEmissionSources(sc.getSectorById(3111)));
    situation.setName("TestSituation");
    situation.setYear(TestDomain.YEAR);
    situation.setType(SituationType.PROPOSED);
    scenario.getOptions().setCalculationType(CalculationType.PERMIT);
    scenario.getOptions().getSubstances().add(Substance.NOXNH3);
    scenario.getOptions().getEmissionResultKeys().add(EmissionResultKey.NOXNH3_DEPOSITION);
    inputData.setScenario(scenario);
    return inputData;
  }

  private EmissionSourceFeature createExampleEmissionSources(final Sector sector) {
    //source x: 122442 y: 473298
    final EmissionSourceFeature feature = new EmissionSourceFeature();
    final int xCoord = 122442;
    final int yCoord = 473298;
    feature.setGeometry(new Point(xCoord, yCoord));
    feature.setId("1");
    final GenericEmissionSource source = new GenericEmissionSource();
    source.setLabel("SomeSource" + sector.getSectorId());
    source.setSectorId(sector.getSectorId());
    final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
    characteristics.setHeatContent(0.22);
    characteristics.setEmissionHeight(40.0);
    characteristics.setSpread(20.0);
    characteristics.setDiurnalVariation(DiurnalVariation.INDUSTRIAL_ACTIVITY);
    characteristics.setParticleSizeDistribution(1);
    source.setCharacteristics(characteristics);
    source.getEmissions().put(Substance.NOX, 1000.0);
    source.getEmissions().put(Substance.NH3, 1000.0);
    feature.setProperties(source);
    return feature;
  }
}
