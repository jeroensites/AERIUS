/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mail;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.i18n.MessageRepository;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Testing templates in the database: If they contain replacement tokens, are they correctly spelled and expected?
 */
class MailTemplateTest extends BaseDBTest {

  private static final Pattern REPLACEMENT_TOKEN_PATTERN = Pattern.compile("\\[(\\w*)\\]");

  @Test
  void testTemplates() throws SQLException {
    final Map<MessagesEnum, EnumSet<ReplacementToken>> templateMap = getTemplateMap();
    for (final Entry<MessagesEnum, EnumSet<ReplacementToken>> entry : templateMap.entrySet()) {
      final String template = MessageRepository.getString(getCalcConnection(), entry.getKey(), LocaleUtils.getDefaultLocale());
      assertEquals(entry.getValue(), getTokensInTemplate(template), "Replacement tokens in template for " + entry.getKey());
    }
  }

  private EnumSet<ReplacementToken> getTokensInTemplate(final String template) {
    final Set<ReplacementToken> tokensFound = new HashSet<>();
    final Matcher matcher = REPLACEMENT_TOKEN_PATTERN.matcher(template);
    while (matcher.find()) {
      tokensFound.add(ReplacementToken.valueOf(matcher.group(1)));
    }
    return tokensFound.isEmpty() ? EnumSet.noneOf(ReplacementToken.class) : EnumSet.copyOf(tokensFound);
  }

  private Map<MessagesEnum, EnumSet<ReplacementToken>> getTemplateMap() {
    final Map<MessagesEnum, EnumSet<ReplacementToken>> templateMap = new HashMap<>();
    //ensure all message enums are in the map
    for (final MessagesEnum messagesEnum : MessagesEnum.values()) {
      templateMap.put(messagesEnum, EnumSet.noneOf(ReplacementToken.class));
    }
    //set every expected replacement token
    templateMap.put(MessagesEnum.MAIL_SUBJECT_TEMPLATE, EnumSet.of(ReplacementToken.MAIL_SUBJECT));
    templateMap.put(MessagesEnum.MAIL_CONTENT_TEMPLATE, EnumSet.of(ReplacementToken.MAIL_CONTENT, ReplacementToken.MAIL_SIGNATURE));

    templateMap.put(MessagesEnum.DEFAULT_FILE_MAIL_CONTENT,
        EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME, ReplacementToken.DOWNLOAD_LINK));

    templateMap.put(MessagesEnum.ERROR_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.ERROR_CODE, ReplacementToken.ERROR_MESSAGE, ReplacementToken.ERROR_SOLUTION));

    templateMap.put(MessagesEnum.PAA_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.DOWNLOAD_LINK));

    templateMap.put(MessagesEnum.PAA_MAIL_SUBJECT, EnumSet.of(ReplacementToken.PROJECT_NAME));
    templateMap.put(MessagesEnum.PAA_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.DOWNLOAD_LINK));

    templateMap.put(MessagesEnum.GML_MAIL_SUBJECT, EnumSet.of(ReplacementToken.AERIUS_REFERENCE));
    templateMap.put(MessagesEnum.GML_MAIL_SUBJECT_JOB, EnumSet.of(ReplacementToken.AERIUS_REFERENCE, ReplacementToken.JOB));
    templateMap.put(MessagesEnum.GML_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.DOWNLOAD_LINK));

    templateMap.put(MessagesEnum.CSV_MAIL_SUBJECT, EnumSet.of(ReplacementToken.AERIUS_REFERENCE));
    templateMap.put(MessagesEnum.CSV_MAIL_SUBJECT_JOB, EnumSet.of(ReplacementToken.AERIUS_REFERENCE, ReplacementToken.JOB));
    templateMap.put(MessagesEnum.CSV_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.DOWNLOAD_LINK));

    templateMap.put(MessagesEnum.NSL_MAIL_SUBJECT, EnumSet.of(ReplacementToken.AERIUS_REFERENCE));
    templateMap.put(MessagesEnum.NSL_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.DOWNLOAD_LINK));

    templateMap.put(MessagesEnum.CONNECT_APIKEY_CONFIRM_BODY,
        EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME, ReplacementToken.CONNECT_APIKEY));

    templateMap.put(MessagesEnum.NSL_ERROR_MAIL_CONTENT,
        EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME, ReplacementToken.ERROR_CODE,
            ReplacementToken.ERROR_MESSAGE, ReplacementToken.ERROR_SOLUTION));

    templateMap.put(MessagesEnum.NSL_MAIL_CONTENT_TEMPLATE,
        EnumSet.of(ReplacementToken.MAIL_CONTENT, ReplacementToken.MAIL_SIGNATURE));

    return templateMap;
  }
}
