/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mail;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.MessageRepository;
import nl.overheid.aerius.db.test.BaseDBTest;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.JobIdentifier;

/**
 * Test class for {@link MailMessageWorker}.
 */
class MailMessageWorkerTest extends BaseDBTest {

  private static final String DUMMY_EMAIL_ADDRESS = "aerius@example.com";
  private MailMessageWorker worker;
  private MockMailWrapper mailWrapper;

  @Override
  @BeforeEach
  public void setUp() throws Exception {
    worker = new MailMessageWorker(getCalcPMF()) {
      @Override
      protected MailWrapper getMailWrapper(final Locale locale) {
        mailWrapper = new MockMailWrapper(getCalcPMF(), locale);
        return mailWrapper;
      }
    };
  }

  @Test
  void testRun() throws Exception {
    final MailTo mailTo = new MailTo(DUMMY_EMAIL_ADDRESS);
    final Locale locale = LocaleUtils.getDefaultLocale();
    final MailMessageData data = new MailMessageData(MessagesEnum.DEFAULT_FILE_MAIL_SUBJECT, MessagesEnum.DEFAULT_FILE_MAIL_CONTENT,
        locale, mailTo);
    final MailAttachment attachment = new MailAttachment("test", "unimportant", "same");
    data.addAttachment(attachment);
    final boolean send = worker.run(data, null, new JobIdentifier("test"));
    assertTrue(send, "Worker result");
    assertNotNull(mailWrapper, "MockMailWrapper should be created");
    assertTrue(mailWrapper.isMailSend(), "MailWrapper should have send");
    assertEquals(mailTo, mailWrapper.getEmailTo(), "Mail to used");
    assertEquals(MessageRepository.getString(getCalcPMF(), MessagesEnum.DEFAULT_FILE_MAIL_SUBJECT, locale), mailWrapper.getSubject(), "Mail subject");
    //enable next line for quick check on content. It's templated though, so should always fail.
    //assertEquals("Mail content", MessageRepository.getString(getCalcPMF(), MessagesEnum.DEFAULT_FILE_MAIL_CONTENT, locale), mailWrapper.getBody());
    assertTrue(mailWrapper.getBody().contains(MessageRepository.getString(getCalcPMF(), MessagesEnum.DEFAULT_FILE_MAIL_CONTENT, locale)),
        "Mail content should be in the body.");
    assertEquals(1, mailWrapper.getAttachments().size(), "Mail attachments list size.");
    assertEquals(attachment, mailWrapper.getAttachments().get(0), "Mail attachments list size.");
  }

  @Test
  void testEscapedReplaceTokenValue() {
    final ReplacementToken testToken = ReplacementToken.ERROR_MESSAGE;
    mailWrapper = new MockMailWrapper(getCalcPMF(), Locale.getDefault());
    mailWrapper.setReplaceTokenValue(testToken, "<a href='Malicious'>Link</a>");
    assertEquals("Link", mailWrapper.getReplaceTokenValue(testToken), "ReplacementToken value should be escaped");
  }

  private static class MockMailWrapper extends MailWrapper {

    private boolean mailSend;
    private MailTo emailTo;
    private List<MailAttachment> attachments;
    private String subject;
    private String body;

    public MockMailWrapper(final PMF pmf, final Locale locale) {
      super(pmf, locale);
    }

    @Override
    protected boolean sendMail(final MailTo emailTo, final List<MailAttachment> attachments, final String actualSubject, final String body)
        throws IOException {
      // No mail is actually send, just return true.
      mailSend = true;
      this.emailTo = emailTo;
      this.attachments = attachments;
      this.subject = actualSubject;
      this.body = body;
      return mailSend;
    }

    boolean isMailSend() {
      return mailSend;
    }

    MailTo getEmailTo() {
      return emailTo;
    }

    List<MailAttachment> getAttachments() {
      return attachments;
    }

    String getSubject() {
      return subject;
    }

    public String getBody() {
      return body;
    }

    String getReplaceTokenValue(final ReplacementToken token) {
      return mailReplaceTokenMap.get(token);
    }

  }

}
