/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mail;

import java.util.Locale;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.base.AbstractDBWorker;

/**
 * Worker that performs the actual sending of the e-mail messages.
 */
public class MailMessageWorker extends AbstractDBWorker<MailMessageData, Boolean> {

  private static final Logger LOG = LoggerFactory.getLogger(MailMessageWorker.class);

  public MailMessageWorker(final PMF pmf) {
    super(pmf);
  }

  @Override
  public Boolean run(final MailMessageData input, final WorkerIntermediateResultSender resultSender, final JobIdentifier jobIdentifier) throws Exception {
    LOG.info("Received mail message {}", input);
    final MailWrapper wrapper = getMailWrapper(input.getLocale());

    for (final Entry<ReplacementToken, MessagesEnum> entry : input.getDbReplacements().entrySet()) {
      wrapper.setReplaceTokenValue(entry.getKey(), getPMF(), entry.getValue(), input.getLocale());
    }

    for (final Entry<ReplacementToken, String> entry : input.getReplacements().entrySet()) {
      wrapper.setReplaceTokenValue(entry.getKey(), entry.getValue());
    }

    final boolean send = wrapper.sendMailToUser(input.getMailTo(), input.getAttachments());
    if (send) {
      LOG.info("Succesfully send mail for {}", input);
    } else {
      LOG.error("Failed to send mail for {}", input);
    }
    return send;
  }

  protected MailWrapper getMailWrapper(final Locale locale) {
    return new MailWrapper(getPMF(), locale);
  }

}
