/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mail;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.i18n.MessageRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.enums.MessagesEnum;

/**
 *
 */
class MailWrapper {

  private static final Logger LOG = LoggerFactory.getLogger(MailWrapper.class);

  private static final String DEFAULT_ERROR_CODE = "NoErrorCode";
  private static final String DEFAULT_ERROR_MESSAGE = "Internal error";
  private static final String DEFAULT_ERROR_SOLUTION = "Contact our helpdesk";

  private final String emailFrom;
  private final String emailSubject;
  private final String emailTemplate;
  protected final MailReplaceTokenMap mailReplaceTokenMap;

  private final PolicyFactory escapePolicyFactory = new HtmlPolicyBuilder().toFactory();

  /**
   * @param pmf The PMF to use when retrieving constants from DB.
   * @param locale The locale to use.
   */
  public MailWrapper(final PMF pmf, final Locale locale) {
    this.emailFrom = ConstantRepository.getString(pmf, ConstantsEnum.NOREPLY_EMAIL);
    this.emailTemplate = MessageRepository.getString(pmf, MessagesEnum.MAIL_CONTENT_TEMPLATE, locale);
    this.emailSubject = MessageRepository.getString(pmf, MessagesEnum.MAIL_SUBJECT_TEMPLATE, locale);
    this.mailReplaceTokenMap = getDefaultMailMap();
    // default signature can be overwritten
    setReplaceTokenValue(ReplacementToken.MAIL_SIGNATURE, pmf, MessagesEnum.MAIL_SIGNATURE_DEFAULT, locale);
  }

  /**
   * Send a mail to the user. Will use all replacetokens-values supplied with setReplaceTokenValue()
   * to ensure the right text is used.
   * @param emailTo The email address(es) to send the email to.
   * @param attachments The attachments to send along with the mail (can be null)
   * @return true if the mail was successfully send.
   * @throws IOException If sending the email was not possible.
   */
  public boolean sendMailToUser(final MailTo emailTo, final List<MailAttachment> attachments) throws IOException {
    final String actualSubject = mailReplaceTokenMap.getMailText(emailSubject);
    final String body = mailReplaceTokenMap.getMailText(mailReplaceTokenMap.containsKey(ReplacementToken.MAIL_CONTENT_TEMPLATE)
        ? mailReplaceTokenMap.get(ReplacementToken.MAIL_CONTENT_TEMPLATE)
        : emailTemplate);

    LOG.info("Sending mail. Attachments included: " + (attachments != null && !attachments.isEmpty()));

    return sendMail(emailTo, attachments, actualSubject, body);
  }

  /**
   * Performs the actual send. Override in tests to skip actual sending of e-mails.
   */
  protected boolean sendMail(final MailTo emailTo, final List<MailAttachment> attachments, final String actualSubject, final String body)
      throws IOException {
    return SendEmail.send(emailFrom, emailTo.getToAddresses(), emailTo.getCcAddresses(), emailTo.getBccAddresses(), actualSubject, body, attachments);
  }

  /**
   * Set the value to replace a ReplacementToken with when sending an email,
   * overwriting any previously added value for that token.
   * @param token The token to set the value for
   * @param value The value that will replace the token in the template.
   * Can be null, in which case the token will not be replaced at all
   * (leaving a [TOKEN] in the mail if it was present in the template).
   *
   * NOTE: HTML in the value will be escaped
   */
  public void setReplaceTokenValue(final ReplacementToken token, final String value) {
    if (value == null) {
      setReplaceTokenTrustedValue(token, null);
    } else {
      final String sanitizedValue = escapePolicyFactory.sanitize(value);
      setReplaceTokenTrustedValue(token, sanitizedValue);
    }
  }

  /**
   * Set the value to replace a ReplacementToken with when sending an email,
   * overwriting any previously added value for that token.
   * The value is obtained from the database using the MessagesEnum.
   *
   * NOTE: HTML in the value is not escaped, stored messages in the database are trusted.
   *
   * @param token The token to set the value for
   * @param pmf Database factory
   * @param messageEnum Message enum to retrieve
   * @param locale Locale to retrieve
   */
  public void setReplaceTokenValue(final ReplacementToken token, final PMF pmf, final MessagesEnum messageEnum, final Locale locale) {
    final String databaseValue = MessageRepository.getString(pmf, messageEnum, locale);
    setReplaceTokenTrustedValue(token, databaseValue);
  }

  /**
   * Set the value to replace a ReplacementToken with when sending an email,
   * overwriting any previously added value for that token.
   * @param token The token to set the value for
   * @param value The value that will replace the token in the template.
   * Can be null, in which case the token will not be replaced at all
   * (leaving a [TOKEN] in the mail if it was present in the template).
   *
   * WARNING: HTML in the value is not escaped but trusted
   */
  private void setReplaceTokenTrustedValue(final ReplacementToken token, final String value) {
    if (value == null) {
      mailReplaceTokenMap.remove(token);
    } else {
      mailReplaceTokenMap.put(token, value);
    }
  }

  private MailReplaceTokenMap getDefaultMailMap() {
    final MailReplaceTokenMap mailReplaceTokenMap = new MailReplaceTokenMap();
    mailReplaceTokenMap.put(ReplacementToken.ERROR_MESSAGE, DEFAULT_ERROR_MESSAGE);
    mailReplaceTokenMap.put(ReplacementToken.ERROR_SOLUTION, DEFAULT_ERROR_SOLUTION);
    mailReplaceTokenMap.put(ReplacementToken.ERROR_CODE, DEFAULT_ERROR_CODE);

    return mailReplaceTokenMap;
  }

}
