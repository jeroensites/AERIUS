/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.paa;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.gml.GMLScenarioHelper;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.importer.PermitGMLWriter;
import nl.overheid.aerius.shared.domain.scenario.IsScenario;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;

public class GMLGenerator {
  private static final Set<SituationType> WNB_SITUATION_TYPES = new HashSet<>(List.of(
      SituationType.REFERENCE,
      SituationType.NETTING,
      SituationType.PROPOSED
  ));

  private final PMF pmf;
  private final PermitGMLWriter permitGMLWriter;

  public GMLGenerator(final PMF pmf) throws SQLException {
    this.pmf = pmf;
    permitGMLWriter = new PermitGMLWriter(pmf);
  }

  /**
   * Get a list of relevant gmls for each proposed situation in the scenario.
   */
  public Map<String, List<StringDataSource>> getGMLsForEachProposedSituation(final Scenario scenario, final Map<String, StringDataSource> gmls) {
    final Map<String, List<StringDataSource>> gmlsByProposedSituation = new HashMap<>();
    getProposedSituations(scenario).forEach(proposedSituation ->
        gmlsByProposedSituation.put(proposedSituation.getId(), scenario.getSituations().stream()
            .filter(situation -> isRelevantForWnbExport(situation, proposedSituation.getId()))
            .map(situation -> gmls.get(situation.getId()))
            .collect(Collectors.toList())));
    return gmlsByProposedSituation;
  }

  /**
   * Build GML dataSources for all relevant situations in the scenario
   *
   * @param scenario a scenario
   * @return GMLs of all WNB relevant situations in the input scenario by their situation id.
   */
  public Map<String, StringDataSource> createGMLs(final Scenario scenario)
      throws SQLException, AeriusException {
    final Map<String, StringDataSource> gmls = new HashMap<>();

    for (int i = 0; i < scenario.getSituations().size(); i++) {
      final var situation = scenario.getSituations().get(i);
      if (isRelevantForWnbExport(situation)) {
        final IsScenario gmlScenario = GMLScenarioHelper.constructScenario(situation, scenario.getCustomPointsList());
        final MetaDataInput gmlMetaData = GMLScenarioHelper
            .constructMetaData(scenario, situation, () -> false, AeriusVersion.getVersionNumber(), pmf.getDatabaseVersion());
        gmls.put(situation.getId(), permitGMLWriter.writeToString(gmlScenario, gmlMetaData, i));
      }
    }

    return gmls;
  }

  private List<ScenarioSituation> getProposedSituations(final Scenario scenario) {
    return scenario.getSituations().stream()
        .filter(scenarioSituation -> scenarioSituation.getType() == SituationType.PROPOSED)
        .collect(Collectors.toList());
  }

  private boolean isRelevantForWnbExport(final ScenarioSituation situation, final String proposedSituationId) {
    if (situation.getType() == SituationType.PROPOSED && !situation.getId().equals(proposedSituationId)) {
      return false;
    }
    return isRelevantForWnbExport(situation);
  }

  private boolean isRelevantForWnbExport(final ScenarioSituation situation) {
    return WNB_SITUATION_TYPES.contains(situation.getType());
  }
}
