/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa;

import static nl.overheid.aerius.shared.InternalRequestMappings.FILE_DELETE;
import static nl.overheid.aerius.shared.RequestMappings.FILE_CODE;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.export.PdfExportWorkerConfiguration;
import nl.overheid.aerius.export.PdfGenerator;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.shared.domain.scenario.SituationType;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.reference.ReferenceUtil;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.util.ZipFileMaker;
import nl.overheid.aerius.worker.ExportHandler;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.base.AbstractExportWorker.ImmediateExportData;

/**
 * Permit Application Appendix Handler. Used to generate the PDF that can be used for a permit application.
 */
public class PAAExportHandler implements ExportHandler {

  private static final Logger LOG = LoggerFactory.getLogger(PAAExportHandler.class);

  private static final String FILENAME_PREFIX = "AERIUS_bijlage";
  private static final String ZIP_EXTENSION = ".zip";
  private static final String PDF_EXTENSION = ".pdf";
  //Timeout for cleanup call, in seconds.
  private static final int TIMEOUT = 10;
  private final GMLGenerator gmlGenerator;
  private final PdfGenerator pdfGenerator;
  private final String scenarioFileIdentifier;
  private final PdfExportWorkerConfiguration configuration;
  private int fileCount;
  private Map<String, List<StringDataSource>> gmls;

  public PAAExportHandler(final PMF pmf, final PdfExportWorkerConfiguration configuration, final JobIdentifier jobIdentifier,
      final String scenarioFileIdentifier, final ReleaseType releaseType) throws IOException, SQLException {
    this.gmlGenerator = new GMLGenerator(pmf);
    this.configuration = configuration;
    this.pdfGenerator = new PdfGenerator(configuration, jobIdentifier, scenarioFileIdentifier, releaseType);
    this.scenarioFileIdentifier = scenarioFileIdentifier;
  }

  @Override
  public void prepare(final CalculationInputData inputData, final ArrayList<StringDataSource> backupAttachments)
      throws SQLException, AeriusException {
    final Map<String, StringDataSource> gmlsStringDataSources = gmlGenerator.createGMLs(inputData.getScenario());
    backupAttachments.addAll(gmlsStringDataSources.values());

    final Map<String, List<StringDataSource>> gmLsForEachProposedSituation =
        gmlGenerator.getGMLsForEachProposedSituation(inputData.getScenario(), gmlsStringDataSources);

    if (gmLsForEachProposedSituation.size() < 1) {
      throw new AeriusException(AeriusExceptionReason.FAULTY_REQUEST, "There should be at least one proposed situation with relevant gmls");
    }
    this.gmls = gmLsForEachProposedSituation;
  }

  @Override
  public void createFileContent(final File outputFile, final CalculationInputData inputData, final ArrayList<StringDataSource> backupAttachments)
      throws AeriusException, IOException {
    LOG.info("Generating PDF for project, reference: {}", inputData.getScenario().getMetaData().getReference());

    final File tempDirectory = Files.createTempDirectory("PAA_Export").toFile();
    final List<File> pdfs = new ArrayList<>();
    for (final ScenarioSituation situation : inputData.getScenario().getSituations()) {
      if (isProposedSituation(situation)) {
        final String reference = ReferenceUtil.generatePermitReference();
        final String pdfFileName = getPDFFileName(situation, reference, inputData.getCreationDate());
        final File tempFile = new File(tempDirectory, pdfFileName);
        pdfs.add(tempFile);

        final String proposedSituationId = situation.getId();
        writeSinglePDF(tempFile, reference, proposedSituationId);
      }
    }

    ZipFileMaker.files2ZipStream(outputFile, pdfs, tempDirectory, true);
    fileCount = pdfs.size();

    try {
      cleanup();
    } catch (final IOException | URISyntaxException | HttpException e) {
      LOG.error("Error cleaning up scenario with id '{}' from fileservice.", scenarioFileIdentifier, e);
    }
  }

  @Override
  public ImmediateExportData postProcess(final ImmediateExportData immediateExportData) {
    immediateExportData.setFilesInZip(fileCount);
    return immediateExportData;
  }

  @Override
  public MessagesEnum getMailSubject(final CalculationInputData inputData) {
    return MessagesEnum.PAA_MAIL_SUBJECT;
  }

  @Override
  public void setReplacementTokens(final CalculationInputData inputData, final MailMessageData mailMessageData) {
    mailMessageData.setReplacement(ReplacementToken.PROJECT_NAME,
        inputData.getScenario().getMetaData().getProjectName() == null ? "" : inputData.getScenario().getMetaData().getProjectName());
  }

  @Override
  public MessagesEnum getMailContent(final CalculationInputData inputData) {
    return MessagesEnum.PAA_MAIL_CONTENT;
  }

  @Override
  public String getFileName(final CalculationInputData inputData) {
    return FileUtil.getFileName(FILENAME_PREFIX, ZIP_EXTENSION, inputData.getScenario().getMetaData().getReference(),
        inputData.getCreationDate());
  }

  private static String getPDFFileName(final ScenarioSituation proposedSituation, final String reference, final Date creationDate) {
    final String name = String.format("%s-%s", proposedSituation.getName(), reference);
    return FileUtil.getFileName(FILENAME_PREFIX, PDF_EXTENSION, name, creationDate);
  }

  private boolean isProposedSituation(final ScenarioSituation scenarioSituation) {
    return scenarioSituation.getType() == SituationType.PROPOSED;
  }

  private void writeSinglePDF(final File outputFile, final String reference, final String proposedSituationId) throws AeriusException {
    try (final FileOutputStream fileOutputStream = new FileOutputStream(outputFile)) {
      final List<String> gmlsAsString = gmls.get(proposedSituationId).stream().map(StringDataSource::getData).collect(Collectors.toList());
      fileOutputStream.write(pdfGenerator.generateFileContents(reference, proposedSituationId, gmlsAsString));
    } catch (final IOException e) {
      LOG.error("Error generating pdf with reference '{}'", reference, e);
      throw new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
    }
  }

  private void cleanup() throws IOException, URISyntaxException, HttpException {
    final String deleteFilePath = FILE_DELETE.replace(FILE_CODE, scenarioFileIdentifier);

    final URIBuilder uriBuilder = new URIBuilder(configuration.getFileServiceUrl());
    uriBuilder.setPath(StringUtils.join(uriBuilder.getPathSegments(), "/") + deleteFilePath);

    // When the debug property is present and true, only then keep scenario file on the fileservice.
    if (!configuration.preventCleanup()) {
      LOG.info("Cleaning up file from fileservice with id '{}'", scenarioFileIdentifier);
      try (final CloseableHttpClient client = HttpClientManager.getHttpClient(TIMEOUT)) {
        HttpClientProxy.deleteRemoteContent(client, uriBuilder.build().toString());
      }
    } else {
      LOG.warn("Cleanup of intermediate PDF files is disabled, '{}' remains on fileservice after pdf export.", scenarioFileIdentifier);
    }
  }
}
