/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.emissions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.importer.ImportParcel;
import nl.overheid.aerius.shared.domain.v2.importer.ImportType;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.io.LegacyNSLImportReader;
import nl.overheid.aerius.srm2.io.SRM2EmissionsWriter;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Writes emission values from csv files per line to a file.
 */
public final class EmissionWriter {
  private static final Logger LOG = LoggerFactory.getLogger(EmissionWriter.class);

  private static final FilenameFilter CSV_FILENAME_FILTER = new FilenameFilter() {
    @Override
    public boolean accept(final File dir, final String name) {
      return name != null && ImportType.determineByFilename(name) == ImportType.SRM2;
    }
  };

  private EmissionWriter() {
    // util class
  }

  /**
   * Write emission values for input directory/file.
   * @param pmf database manager
   * @param emissionsFile emission file or directory with files.
   * @param year year of emissions
   * @param target
   * @throws AeriusException
   * @throws IOException
   * @throws SQLException
   */
  public static void writeEmissions(final PMF pmf, final String emissionsFile, final int year, final String target)
      throws IOException, AeriusException, SQLException {
    final File targetDir = getTargetDir(target);
    final List<File> files = FileUtil.getFilesWithExtension(new File(emissionsFile), CSV_FILENAME_FILTER);
    try (final Connection con = pmf.getConnection()) {
      final SectorCategories categories =
          SectorRepository.getSectorCategories(con, new DBMessagesKey(ProductType.CALCULATOR, LocaleUtils.getDefaultLocale()));
      for (final File file : files) {
        writeEmissionOfFile(categories, file, year, targetDir);
      }
    }
  }

  /**
   * @param target
   * @return
   */
  private static File getTargetDir(final String target) {
    final File dir = new File(target);
    if (!dir.isDirectory()) {
      throw new IllegalArgumentException("-target option is not a directory:" + target);
    }
    if (!dir.canWrite()) {
      throw new IllegalArgumentException("-target option is not a directory isn't writable:" + target);
    }
    return dir;
  }

  /**
   * @param context
   * @param file
   * @param year
   * @param targetDir
   * @throws IOException
   * @throws AeriusException
   */
  private static void writeEmissionOfFile(final SectorCategories categories, final File file, final int year, final File targetDir)
      throws IOException, AeriusException {
    final LegacyNSLImportReader reader = new LegacyNSLImportReader();
    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportParcel read = new ImportParcel();
      reader.read(file.getName(), inputStream, categories, null, read);
      final List<EmissionSourceFeature> sources = read.getSituation().getEmissionSourcesList();
      final File outputFile = new File(targetDir, FileUtil.getFileWithoutExtension(file));
      LOG.info("Write emission file {} (#sources: {}) for year {}", outputFile, sources.size(), year);
      SRM2EmissionsWriter.writeEmissions(outputFile, year, sources);
    }
  }
}
