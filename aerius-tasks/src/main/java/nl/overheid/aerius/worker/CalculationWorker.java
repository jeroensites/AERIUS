/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.JobRepository;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.shared.i18n.AeriusExceptionMessages;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.base.AbstractDBWorker;

/**
 * Worker to perform a complete calculation. It chunks the task, calls the calculation engine workers and collects the results in the database and
 * send the results back.
 */
public class CalculationWorker extends AbstractDBWorker<CalculationInputData, Boolean> {

  private static final Logger LOGGER = LoggerFactory.getLogger(CalculationWorker.class);

  private final CalculatorBuildDirector calculatorBuildDirector;

  public CalculationWorker(final CalculatorBuildDirector calculatorBuildDirector, final PMF pmf) {
    super(pmf);
    this.calculatorBuildDirector = calculatorBuildDirector;
  }

  @Override
  public Boolean run(final CalculationInputData inputData, final WorkerIntermediateResultSender resultSender, final JobIdentifier jobIdentifier)
      throws Exception {
    final String correlationId = jobIdentifier.getCorrelationId();
    try {
      if (inputData.isTrackJobProcess()) {
        cleanUpCrashedCalculations(correlationId);
      }
      final CalculationJob job = calculatorBuildDirector.construct(inputData, jobIdentifier, resultSender).calculate();

      if (inputData.isTrackJobProcess()) {
        setEndtimeAndCompleteJob(jobIdentifier, job.getState());
      }
    } catch (RuntimeException | AeriusException e) {
      // When a runtime or aerius Exception is thrown the job is cancelled and an endtime should be set
      // Other exceptions mean the job will get back on the queue, and can be picked up again, so no endtime is set.
      if (inputData.isTrackJobProcess()) {
        if (isNotCancelledJob(e)) {
          setErrorStateAndMessageJob(jobIdentifier, getLocale(inputData), e);
        }
      }
    }
    return Boolean.TRUE;
  }

  /**
   * When a calculation crashed and was set back on the queue it could mean there are still calculation results left in the database.
   * This call removes these results if any are present before the calculation starts.
   * @param correlationId job key to remove results for.
   */
  private void cleanUpCrashedCalculations(final String correlationId) {
    try (final Connection con = getPMF().getConnection()) {
      JobRepository.removeJobCalculations(con, correlationId);
    } catch (final SQLException e) {
      LOGGER.error("Error occurred while trying clean up a possible crashed job {}", correlationId, e);
    }
  }

  private void setEndtimeAndCompleteJob(final JobIdentifier jobIdentifier, final CalculationState finalState) {
    final String correlationId = jobIdentifier.getCorrelationId();
    try (final Connection con = getPMF().getConnection()) {
      JobRepository.setEndTimeToNow(con, correlationId);
      JobRepository.updateJobStatus(con, correlationId, map(finalState));
    } catch (final SQLException e) {
      LOGGER.error("Error occurred while trying to complete the job {}", correlationId, e);
    }
  }

  private boolean isNotCancelledJob(final Exception e) {
    return !(e instanceof AeriusException) || ((AeriusException) e).getReason() != AeriusExceptionReason.CONNECT_JOB_CANCELLED;
  }

  private void setErrorStateAndMessageJob(final JobIdentifier jobIdentifier, final Locale locale, final Exception originException) {
    final String correlationId = jobIdentifier.getCorrelationId();
    final String message = getErrorMessage(originException, locale);
    try (final Connection con = getPMF().getConnection()) {
      JobRepository.setEndTimeToNow(con, correlationId);
      JobRepository.setErrorMessage(con, correlationId, message);
    } catch (final SQLException e) {
      LOGGER.error("Error occurred while trying to set the error message {} for the job {}", correlationId, message, e);
    }
  }

  protected static Locale getLocale(final CalculationInputData inputData) {
    return LocaleUtils.getLocale(inputData.getLocale());
  }

  protected String getErrorMessage(final Exception e, final Locale locale) {
    return new AeriusExceptionMessages(locale).getString(e);
  }

  private JobState map(final CalculationState state) {
    return state == CalculationState.CANCELLED ? JobState.CANCELLED : JobState.COMPLETED;
  }
}
