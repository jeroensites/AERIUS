/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.base;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.mail.MailAttachment;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.MailTo;
import nl.overheid.aerius.mail.MessageTaskClient;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.shared.InternalRequestMappings;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.DownloadInfo;
import nl.overheid.aerius.shared.domain.export.ExportData;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.i18n.AeriusExceptionMessages;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.JobIdentifier;
import nl.overheid.aerius.worker.util.DownloadDirectoryUtils;
import nl.overheid.aerius.worker.util.DownloadDirectoryUtils.StorageLocation;

/**
 * Worker to use when sending mails containing a link to an export of some kind.
 * @param <T> Class of the actual inputdata.
 */
public abstract class AbstractExportWorker<T extends ExportData> extends AbstractDBWorker<T, ExportedData> {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractExportWorker.class);

  private static final int TIMEOUT_SECONDS = 10;

  protected final TaskManagerClient taskManagerClient;
  protected final String downloadDirectory;
  private final JsonParser jsonParser = new JsonParser();
  private final String webappURL;

  /**
   * @param pmf The persistence manager factory to use.
   * @param downloadDirectory The directory to place files in, which should be reachable from the web (used in resulting URL).
   * @param factory queue connection factory
   */
  public AbstractExportWorker(final PMF pmf, final String downloadDirectory, final BrokerConnectionFactory factory) {
    super(pmf);
    this.downloadDirectory = downloadDirectory;
    this.taskManagerClient = new TaskManagerClient(factory);
    webappURL = ConstantRepository.getString(pmf, ConstantsEnum.DEFAULT_FILE_MAIL_DOWNLOAD_LINK);
  }

  @Override
  public final ExportedData run(final T inputData, final WorkerIntermediateResultSender resultSender, final JobIdentifier jobIdentifier)
      throws Exception {
    //ExportWorkers don't use the resultCallback (yet anyway).
    return run(inputData, jobIdentifier);
  }

  public final ExportedData run(final T inputData, final JobIdentifier jobIdentifier) throws Exception {
    // Generate metadata before calculating, because if this fails the calculations are useless.
    final ArrayList<StringDataSource> backupAttachments = new ArrayList<>();
    try {
      // handle the actual export, including sending a mail to the PDF to user.
      final ImmediateExportData immediateData = handleExport(inputData, backupAttachments, jobIdentifier);
      final ExportedData exportedData = new ExportedData();
      final String downloadUrl;

      if (inputData.isSubmitFileToService()) {
        exportedData.setFileCode(submitFileToService(immediateData));
        exportedData.setFileName(immediateData.getFilename());
        downloadUrl = StringUtils.appendIfMissing(inputData.getFileServiceUrl(), "/") + exportedData.getFileCode();
      } else {
        downloadUrl = immediateData.getUrl();
      }
      if (inputData.isEmailUser()) {
        exportedData.setDownloadInfo(sendMailToUser(inputData, downloadUrl, backupAttachments));
      }
      if (inputData.isReturnFile()) {
        exportedData.setFileName(getFileName(inputData));
        exportedData.setFileContent(immediateData.getReturnFileContent());
        exportedData.setFilesInZip(immediateData.getFilesInZip());
      }
      return exportedData;
    } catch (final AeriusException | RuntimeException e) {
      /* Catch only exceptions that will cause the task to be dropped from the Queue.
       * It will be logged further on, but send an email to the person who requested.
       * Any other exception will cause a worker to shutdown and the task to be send to another worker.
       * This could cause a cascade of workers going down, but at least the user will never be notified about it (hurray?)*/
      if (inputData.isEmailUser()) {
        LOG.info("Sending error mail for: " + inputData);
        sendMailToUserOnError(inputData, e, backupAttachments);
        LOG.info("Error mail send to user.");
      }
      throw e;
    }
  }

  /**
   * Performs a HTTP POST to the file server and returns the unique identification of this file as returned by the file server.
   *
   * @param immediateData
   * @return
   * @throws ParseException
   * @throws URISyntaxException
   * @throws IOException
   * @throws HttpException
   */
  private String submitFileToService(final ImmediateExportData immediateData) throws ParseException, URISyntaxException, IOException, HttpException {
    final String fileServiceUrl = ConstantRepository.getString(getPMF(), ConstantsEnum.FILE_SERVICE_URL_INTERNAL);

    try (final CloseableHttpClient client = HttpClientManager.getHttpClient(TIMEOUT_SECONDS)) {
      final String response = HttpClientProxy.postFile(client, fileServiceUrl, immediateData.getFilename(), immediateData.getFile(),
          Map.of(InternalRequestMappings.FILE_PUBLIC_PARAMETER, true));
      // handle exception.
      final JsonObject object = (JsonObject) jsonParser.parse(response);
      LOG.info("response:{}", response);
      return object.get("fileCode").getAsString();
    }
  }

  /**
   * @param inputData The data to generate a filename for.
   * @return The filename to use for a file based on the inputdata for this worker.
   */
  protected String getFileName(final T inputData) {
    return null;
  }

  /**
   * @param inputData The input to export.
   * @param backupAttachments A list of files to use as attachments when an exception occurs fails.
   * Can be used to add GML files so user can recover from a PDF export failed for instance.
   * @param jobIdentifier The job identifier.
   * @return The URL to use by the user to download the file.
   * @throws Exception In case of exceptions.
   */
  protected ImmediateExportData handleExport(final T inputData, final ArrayList<StringDataSource> backupAttachments,
      final JobIdentifier jobIdentifier)
      throws Exception {
    return null;
  }

  /**
   * @param inputData The inputdata to save the file for (used for filename if needed).
   * @param fileForDownload The content of the file to save.
   * @return The URL to use to download the file.
   * @throws IOException In case saving the file failed.
   */
  protected String saveFile(final T inputData, final byte[] fileForDownload) throws IOException {
    final StorageLocation storageLocation = createStorageLocation(inputData);

    try (final OutputStream os = new FileOutputStream(storageLocation.getOutputFile())) {
      os.write(fileForDownload);
    }
    return storageLocation.getUrl();
  }

  /**
   * @param inputData The inputdata to create a storage location for.
   * @return The location data can be stored.
   * @throws IOException In case creating the storage.
   */
  protected StorageLocation createStorageLocation(final T inputData) throws IOException {
    return DownloadDirectoryUtils.createStorage(webappURL, downloadDirectory, getFileName(inputData));
  }

  /**
   * @param inputData The inputdata to use when constructing a fallback error code.
   * @return A fallback error code to use in error-mails (can be null).
   */
  protected String getFallbackErrorCode(final T inputData) {
    //default return no error code
    return null;
  }

  protected MailMessageData getMailMessageData(final T inputData) {
    final MailMessageData mailMessageData = new MailMessageData(getMailSubject(inputData), getMailContent(inputData),
        getLocale(inputData), getMailTo(inputData));
    setDefaultReplacementTokens(inputData, mailMessageData);
    return mailMessageData;
  }

  /**
   * @return The MessagesEnum specifying the subject template to use in the mail to be send to the user.
   */
  protected MessagesEnum getMailSubject(final T inputData) {
    return MessagesEnum.DEFAULT_FILE_MAIL_SUBJECT;
  }

  /**
   * @return The MessagesEnum specifying the content template to use in the mail to be send to the user.
   */
  protected MessagesEnum getMailContent(final T inputData) {
    return MessagesEnum.DEFAULT_FILE_MAIL_CONTENT;
  }

  private MailMessageData getErrorMailMessageData(final T inputData) {
    final MailMessageData mailMessageData = new MailMessageData(
        getErrorMailSubject(inputData), getErrorMailContent(inputData),
        getLocale(inputData), getMailToOnError(inputData));
    setDefaultReplacementTokens(inputData, mailMessageData);
    return mailMessageData;
  }

  /**
   * @param inputData input data
   * @return The MessagesEnum specifying the subject template to use in an error mail to be send to the user.
   */
  protected MessagesEnum getErrorMailSubject(final T inputData) {
    return MessagesEnum.ERROR_MAIL_SUBJECT;
  }

  /**
   * @param inputData input data
   * @return The MessagesEnum specifying the content template to use in an error mail to be send to the user.
   */
  protected MessagesEnum getErrorMailContent(final T inputData) {
    return MessagesEnum.ERROR_MAIL_CONTENT;
  }

  protected MailTo getMailTo(final T inputData) {
    return new MailTo(inputData.getEmailAddress());
  }

  protected MailTo getMailToOnError(final T inputData) {
    return getMailTo(inputData);
  }

  protected final void setDefaultReplacementTokens(final T inputData, final MailMessageData mailMessageData) {
    final Date creationDate = inputData.getCreationDate();
    final Locale locale = getLocale(inputData);
    mailMessageData.setReplacement(ReplacementToken.CALC_CREATION_DATE, MessageTaskClient.getDefaultDateFormatted(creationDate, locale));
    mailMessageData.setReplacement(ReplacementToken.CALC_CREATION_TIME, MessageTaskClient.getDefaultTimeFormatted(creationDate, locale));
    mailMessageData.setReplacement(ReplacementToken.MANUAL_URL, ConstantRepository.getString(getPMF(), SharedConstantsEnum.MANUAL_URL));
    mailMessageData.setReplacement(ReplacementToken.QUICK_START_URL, ConstantRepository.getString(getPMF(), SharedConstantsEnum.QUICK_START_URL));
    mailMessageData.setReplacement(ReplacementToken.BIJ12_HELPDESK_URL,
        ConstantRepository.getString(getPMF(), SharedConstantsEnum.BIJ12_HELPDESK_URL));
  }

  protected abstract void setReplacementTokens(final T inputData, final MailMessageData mailMessageData);

  protected Locale getLocale(final T inputData) {
    return LocaleUtils.getLocale(inputData.getLocale());
  }

  protected DownloadInfo sendMailToUser(final T inputData, final String downloadURL, final ArrayList<StringDataSource> backupAttachments)
      throws IOException, SQLException, AeriusException {
    final DownloadInfo di;

    if (downloadURL == null) {
      LOG.error("Saving file to download directory failed after all the hard work done to generate it.. Sending error message instead");
      sendMailToUserOnError(inputData, null, backupAttachments);
      di = null;
    } else {
      final MailMessageData mailMessageData = getMailMessageData(inputData);

      LOG.trace("Adding download link: {}", downloadURL);
      mailMessageData.setReplacement(ReplacementToken.DOWNLOAD_LINK, downloadURL);

      setReplacementTokens(inputData, mailMessageData);
      di = new DownloadInfo();
      di.setEmailSuccess(sendMail(mailMessageData));
      di.setUrl(downloadURL);
      di.setEmailAddress(inputData.getEmailAddress());
    }
    return di;
  }

  protected void sendMailToUserOnError(final T inputData, final Exception e, final ArrayList<StringDataSource> attachments)
      throws IOException, AeriusException {
    final MailMessageData errorMailMessageData = getErrorMailMessageData(inputData);

    setErrorReplacementTokens(errorMailMessageData, inputData, e);

    attachments.stream()
        .map(stringDataSource -> new MailAttachment(stringDataSource.getData(), stringDataSource.getFileName(), stringDataSource.getMimeType()))
        .forEach(attachment -> errorMailMessageData.getAttachments().add(attachment));

    sendMail(errorMailMessageData);
  }

  protected String getErrorMessage(final Exception e, final Locale locale) {
    return new AeriusExceptionMessages(locale).getString(e);
  }

  protected String getErrorSolution(final Exception e, final Locale locale) {
    return new AeriusExceptionMessages(locale).getString(e);
  }

  protected void setErrorReplacementTokens(final MailMessageData errorMailMessageData, final T inputData, final Exception e) {
    setDefaultReplacementTokens(inputData, errorMailMessageData);
    final Locale locale = getLocale(inputData);

    errorMailMessageData.setReplacement(ReplacementToken.ERROR_MESSAGE, getErrorMessage(e, locale));
    errorMailMessageData.setReplacement(ReplacementToken.ERROR_SOLUTION, getErrorSolution(e, locale));

    if (getFallbackErrorCode(inputData) != null) {
      errorMailMessageData.setReplacement(ReplacementToken.ERROR_CODE, getFallbackErrorCode(inputData));
    }

    if (e instanceof AeriusException && ((AeriusException) e).getReason() != null) {
      final AeriusException aeriusException = (AeriusException) e;
      if (aeriusException.getReason() != null) {
        errorMailMessageData.setReplacement(ReplacementToken.ERROR_CODE, Integer.toString(aeriusException.getReason().getErrorCode()));
      }
    }
  }

  protected boolean sendMail(final MailMessageData mailMessageData) {
    boolean send = false;
    try {
      MessageTaskClient.startMessageTask(taskManagerClient, mailMessageData);
      send = true;
    } catch (final IOException e) {
      LOG.error("Error sending mail message data {} to client, returning to user anyway.", mailMessageData, e);
    }
    return send;
  }

  public static class ImmediateExportData {

    private final String url;
    private final String filename;
    private final File file;

    private final byte[] returnFileContent;
    private int filesInZip;

    public ImmediateExportData(final String url) {
      this(url, null, null);
    }

    public ImmediateExportData(final String url, final String filename, final File file) {
      this.url = url;
      this.filename = filename;
      this.file = file;
      this.returnFileContent = null;
    }

    public ImmediateExportData(final byte[] returnFileContent) {
      this.url = null;
      this.file = null;
      this.filename = null;
      this.returnFileContent = returnFileContent;
    }

    public File getFile() {
      return file;
    }

    public String getFilename() {
      return filename;
    }

    public String getUrl() {
      return url;
    }

    public byte[] getReturnFileContent() {
      return returnFileContent;
    }

    public int getFilesInZip() {
      return filesInZip;
    }

    public void setFilesInZip(final int filesInZip) {
      this.filesInZip = filesInZip;
    }

  }
}
