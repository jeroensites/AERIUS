/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.conversion.EngineSourceExpander;
import nl.overheid.aerius.calculation.conversion.OPSSourceConverter;
import nl.overheid.aerius.calculation.conversion.SourceConversionHelper;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationInfoRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.wnb.ReceptorRepository;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.ops.OPSVersion;
import nl.overheid.aerius.ops.conversion.CalculationPointOPSConverter;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.io.OPSFileWriter;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.export.OPSExportData;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.util.ZipFileMaker;
import nl.overheid.aerius.worker.base.AbstractExportWorker;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;
import nl.overheid.aerius.worker.util.DownloadDirectoryUtils.StorageLocation;

/**
 * Worker converts sources and custom calculation points to OPS sources and receptors.
 * Creates OPS files based on that input.
 * When calculation ID supplied is not null, results for that calculation are also converted.
 * Sends generic mails containing the generated files after it is done generating files (or when an error occurred).
 */
public class OPSExportWorker extends AbstractExportWorker<OPSExportData> {

  private static final Logger LOG = LoggerFactory.getLogger(OPSExportWorker.class);
  private static final String FILENAME_PREFIX = "AERIUS_ops";
  private static final String FILENAME_EXTENSION = ".zip";

  private static final File OPS_ROOT_PLACEHOLDER = new File("[OPS_ROOT]");

  private static final List<Substance> SUBSTANCES = List.of(Substance.NH3, Substance.NOX, Substance.PM10);

  /**
   * @param pmf The Persistence Manager Factory to use
   * @param config The Database WorkerConfiguration to get any configuration from.
   */
  public OPSExportWorker(final PMF pmf, final DBWorkerConfiguration config, final BrokerConnectionFactory factory) throws IOException {
    super(pmf, config.getGMLDownloadDirectory(), factory);
  }

  @Override
  protected ImmediateExportData handleExport(final OPSExportData inputData, final ArrayList<StringDataSource> backupAttachments,
      final JobIdentifier jobIdentifier) throws AeriusException, SQLException, IOException {
    LOG.info("OPS export worker kicked in for {}", inputData);

    final List<File> opsFiles = new ArrayList<>();
    //ensure all files are written to the same base directory.
    final File tempDir = Files.createTempDirectory("OPS_Export").toFile();

    //create a writer with emulate = false: we're not actually running it and the placeholder will cause an exception otherwise.
    final OPSConfiguration cfo = new OPSConfiguration();
    cfo.setOpsRoot(OPS_ROOT_PLACEHOLDER);
    cfo.setRunFilesDirectory(tempDir);
    final OPSFileWriter writer = new OPSFileWriter(cfo);
    final Scenario scenario = inputData.getScenario();
    for (final ScenarioSituation situation : scenario.getSituations()) {
      final Optional<ScenarioSituationResults> results = Optional.ofNullable(inputData.getScenarioResults())
          .map(s -> s.getResultsPerSituation().get(situation.getId()));
      opsFiles.addAll(getOPSFiles(writer, scenario, situation, results));
    }

    //save the files to a zip and return the URL to download the zip.
    final StorageLocation storageLocation = createStorageLocation(inputData);
    ZipFileMaker.files2ZipStream(storageLocation.getOutputFile(), opsFiles, tempDir, true);
    return new ImmediateExportData(storageLocation.getUrl());
  }

  @Override
  protected String getFileName(final OPSExportData inputData) {
    return FileUtil.getFileName(FILENAME_PREFIX, FILENAME_EXTENSION, null, inputData.getCreationDate());
  }

  private List<File> getOPSFiles(final OPSFileWriter writer, final Scenario scenario, final ScenarioSituation situation,
      final Optional<ScenarioSituationResults> results) throws SQLException, AeriusException, IOException {
    final List<File> files = new ArrayList<>();
    final OPSInputData opsInputData;
    try (Connection con = getPMF().getConnection()) {
      opsInputData = toOPSInputData(con, scenario, situation, results);
    }
    //ensure the points have the right terrain data.
    ReceptorRepository.setTerrainData(getPMF(), opsInputData.getReceptors());
    for (final Substance substance : SUBSTANCES) {
      final String folderOptionalPart = getFolderOptionalPart(scenario, situation);
      final String runId = FileUtil.getFileName(substance.name(), folderOptionalPart, null);
      final List<OPSSource> sources = opsInputData.getEmissionSources().entrySet().stream()
          .flatMap(e -> e.getValue().stream())
          .collect(Collectors.toList());

      writer.writeFiles(runId, sources, opsInputData.getReceptors(), substance, situation.getYear(), opsInputData.getMeteo(), null);
      files.addAll(Arrays.asList(writer.getRunDirectory(runId).listFiles()));
    }
    return files;
  }

  private String getFolderOptionalPart(final Scenario scenario, final ScenarioSituation situation) {
    // Name of the sources is not guaranteed to be unique, so add something to make it unique within the scenario.
    if (scenario.getSituations().size() > 1) {
      // Don't use calculation ID since it's not guaranteed that a calculation has been started.
      // Start with the unique bit, since there is a limit to the length of the optional part of the file name.
      return scenario.getSituations().indexOf(situation) + "_" + situation.getName();
    } else {
      return situation.getName();
    }
  }

  private OPSInputData toOPSInputData(final Connection con, final Scenario scenario, final ScenarioSituation situation,
      final Optional<ScenarioSituationResults> results) throws SQLException, AeriusException {

    final OPSInputData inputData = new OPSInputData(OPSVersion.VERSION, getSurfaceZoomLevel1(con));
    final EnumSet<EmissionResultKey> emissionResultKeys = EnumSet.noneOf(EmissionResultKey.class);

    for (final Substance substance : SUBSTANCES) {
      emissionResultKeys.add(EmissionResultKey.valueOf(substance));
    }
    inputData.setSubstances(SUBSTANCES);
    inputData.setEmissionResultKeys(emissionResultKeys);
    inputData.setYear(situation.getYear());
    inputData.setEmissionSources(1,
        getSources(con, situation, scenario.getOptions().isStacking()));
    inputData.setMeteo(scenario.getOptions().getMeteo());
    inputData.setReceptors(getReceptors(con, scenario, results.map(r -> r.getCalculationId())));
    return inputData;
  }

  private List<OPSSource> getSources(final Connection con, final ScenarioSituation situation, final boolean stacking)
      throws SQLException, AeriusException {
    final OPSSourceConverter geometryExpander = new OPSSourceConverter(con, situation);

    final SectorCategories sectorCategories = SectorRepository.getSectorCategories(getPMF(), LocaleUtils.getDefaultLocale());
    final SourceConversionHelper conversionHelper = new SourceConversionHelper(getPMF(), sectorCategories, situation.getYear());

    final List<EngineSource> list =
        EngineSourceExpander.toEngineSources(con, geometryExpander, conversionHelper, situation, SUBSTANCES, stacking);
    return list.stream().filter(OPSSource.class::isInstance).map(OPSSource.class::cast).collect(Collectors.toList());
  }

  private List<OPSReceptor> getReceptors(final Connection con, final Scenario scenario, final Optional<Integer> calculationId) throws SQLException {
    final List<OPSReceptor> receptors = new ArrayList<>();
    if (!calculationId.isPresent() || calculationId.get() == 0) {
      addReceptors(receptors, scenario.getCustomPointsList());
    } else {
      addReceptors(receptors, CalculationInfoRepository.getCalculationResults(con, calculationId.get()).getResults());
    }
    return receptors;
  }

  private void addReceptors(final List<OPSReceptor> receptors, final Collection<CalculationPointFeature> points) {
    for (final CalculationPointFeature feature : points) {
      receptors.add(CalculationPointOPSConverter.convert(feature.getProperties(), feature.getGeometry()));
    }
  }

  @Override
  protected void setReplacementTokens(final OPSExportData inputData, final MailMessageData mailMessageData) {
    //NO-OP
  }

  private int getSurfaceZoomLevel1(final Connection con) throws SQLException {
    return ReceptorGridSettingsRepository.getZoomLevels(con).get(0).getSurfaceLevel1();
  }

}
