/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import org.apache.commons.io.FileUtils;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.srm.io.NSLCalculationPointWriter;
import nl.overheid.aerius.srm.io.NSLCorrectionWriter;
import nl.overheid.aerius.srm.io.NSLDispersionLineWriter;
import nl.overheid.aerius.srm.io.NSLMeasureWriter;
import nl.overheid.aerius.srm.io.NSLSRM1RoadWriter;
import nl.overheid.aerius.srm.io.NSLSRM2RoadWriter;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.worker.DirectoryExportHandler.InputExportHandler;

/**
 *
 */
public class NSLCSVInputExportHandler implements InputExportHandler {

  private static final int SCALE = 10;

  private static final String FILENAME_SOURCES_SRM1 = "%s_nsl_srm1_roads";
  private static final String FILENAME_SOURCES_SRM2 = "%s_nsl_srm2_roads";
  private static final String FILENAME_POINTS = "%s_nsl_calculation_points";
  private static final String FILENAME_DISPERSION_LINES = "%s_nsl_dispersion_lines";
  private static final String FILENAME_CORRECTIONS = "%s_nsl_corrections";
  private static final String FILENAME_MEASURES = "%s_nsl_measures";
  private static final String CSV_EXT = "csv";

  private final NSLSRM1RoadWriter srm1Writer = new NSLSRM1RoadWriter(SCALE);
  private final NSLSRM2RoadWriter srm2Writer = new NSLSRM2RoadWriter(SCALE);
  private final NSLCalculationPointWriter pointWriter = new NSLCalculationPointWriter();
  private final NSLDispersionLineWriter dispersionLineWriter = new NSLDispersionLineWriter();
  private final NSLCorrectionWriter correctionWriter = new NSLCorrectionWriter(SCALE);
  private final NSLMeasureWriter measureWriter = new NSLMeasureWriter(SCALE);

  @Override
  public void exportInput(final CalculationInputData inputData, final File targetDirectory) {
    final Scenario scenario = inputData.getScenario();

    for (final ScenarioSituation situation : scenario.getSituations()) {
      exportSrm1Sources(situation, targetDirectory, inputData);
      exportSrm2Sources(situation, targetDirectory, inputData);
      exportPoints(situation, scenario, targetDirectory, inputData);
      exportDispersionLines(situation, targetDirectory, inputData);
      exportCorrections(situation, targetDirectory, inputData);
      exportMeasures(situation, targetDirectory, inputData);
    }

  }

  private void exportSrm1Sources(final ScenarioSituation situation, final File targetDirectory, final CalculationInputData inputData) {
    final File targetFile = targetFile(targetDirectory, FILENAME_SOURCES_SRM1, situation, inputData);
    try {
      srm1Writer.write(targetFile, situation.getEmissionSourcesList());
    } catch (final IOException | RuntimeException e) {
      writeErrorInFile(targetDirectory, e, "Error writing NSL SRM1 sources to file");
      FileUtils.deleteQuietly(targetFile);
    }
  }

  private void exportSrm2Sources(final ScenarioSituation situation, final File targetDirectory, final CalculationInputData inputData) {
    final File targetFile = targetFile(targetDirectory, FILENAME_SOURCES_SRM2, situation, inputData);
    try {
      srm2Writer.write(targetFile, situation.getEmissionSourcesList());
    } catch (final IOException | RuntimeException e) {
      writeErrorInFile(targetDirectory, e, "Error writing NSL SRM2 sources to file");
      FileUtils.deleteQuietly(targetFile);
    }
  }

  private void exportPoints(final ScenarioSituation situation, final Scenario scenario, final File targetDirectory,
      final CalculationInputData inputData) {
    final File targetFile = targetFile(targetDirectory, FILENAME_POINTS, situation, inputData);
    try {
      pointWriter.write(targetFile, scenario.getCustomPointsList());
    } catch (final IOException | RuntimeException e) {
      writeErrorInFile(targetDirectory, e, "Error writing NSL points to file");
      FileUtils.deleteQuietly(targetFile);
    }
  }

  private void exportDispersionLines(final ScenarioSituation situation, final File targetDirectory, final CalculationInputData inputData) {
    final File targetFile = targetFile(targetDirectory, FILENAME_DISPERSION_LINES, situation, inputData);
    try {
      if (!situation.getNslDispersionLinesList().isEmpty()) {
        dispersionLineWriter.write(targetFile, situation.getNslDispersionLinesList());
      }
    } catch (final IOException | RuntimeException e) {
      writeErrorInFile(targetDirectory, e, "Error writing NSL dispersion lines to file");
      FileUtils.deleteQuietly(targetFile);
    }
  }

  private void exportCorrections(final ScenarioSituation situation, final File targetDirectory, final CalculationInputData inputData) {
    final File targetFile = targetFile(targetDirectory, FILENAME_CORRECTIONS, situation, inputData);
    try {
      if (!situation.getNslCorrections().isEmpty()) {
        correctionWriter.write(targetFile, situation.getNslCorrections());
      }
    } catch (final IOException | RuntimeException e) {
      writeErrorInFile(targetDirectory, e, "Error writing NSL corrections to file");
      FileUtils.deleteQuietly(targetFile);
    }
  }

  private void exportMeasures(final ScenarioSituation situation, final File targetDirectory, final CalculationInputData inputData) {
    final File targetFile = targetFile(targetDirectory, FILENAME_MEASURES, situation, inputData);
    try {
      if (!situation.getNslMeasuresList().isEmpty()) {
        measureWriter.write(targetFile, situation.getNslMeasuresList());
      }
    } catch (final IOException | RuntimeException e) {
      writeErrorInFile(targetDirectory, e, "Error writing NSL measures to file");
      FileUtils.deleteQuietly(targetFile);
    }
  }

  private File targetFile(final File targetDirectory, final String fileTypeFormat, final ScenarioSituation situation,
      final CalculationInputData inputData) {
    final String scenarioName = inputData.getScenario().getMetaData().getProjectName();
    final Date exportDate = inputData.getCreationDate();
    final String prefix = String.format(fileTypeFormat, situation.getId());
    final String filename = FileUtil.getFileName(prefix, CSV_EXT, scenarioName, exportDate);
    final Path openFile = Paths.get(targetDirectory.getAbsolutePath(), filename);
    return openFile.toFile();
  }

}
