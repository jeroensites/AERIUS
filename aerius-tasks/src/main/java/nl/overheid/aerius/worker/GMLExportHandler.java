/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.calculator.CalculationInfoRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.emissions.CategoryBasedEmissionFactorSupplier;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.gml.GMLScenarioHelper;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.importer.PermitGMLWriter;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.scenario.IsScenario;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.v2.point.CalculationPointFeature;
import nl.overheid.aerius.shared.domain.v2.scenario.Scenario;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioResults;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituation;
import nl.overheid.aerius.shared.domain.v2.scenario.ScenarioSituationResults;
import nl.overheid.aerius.shared.domain.v2.source.EmissionSourceFeature;
import nl.overheid.aerius.shared.emissions.EmissionFactorSupplier;
import nl.overheid.aerius.shared.emissions.EmissionsUpdater;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.geometry.GeometryCalculator;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.GeometryCalculatorImpl;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.util.ZipFileMaker;
import nl.overheid.aerius.worker.base.AbstractExportWorker.ImmediateExportData;

/**
 * Handler converts sources and custom calculation points to GML.
 * When calculation ID supplied is not null, results for that calculation are also converted.
 * Sends generic mails containing the generated .gml file after it is done generating files (or when an error occurred).
 */
public class GMLExportHandler implements ExportHandler {

  private static final Logger LOG = LoggerFactory.getLogger(GMLExportHandler.class);

  private static final String FILENAME_GML_PREFIX = "AERIUS_gml";
  private static final String FILENAME_GML_EXTENSION = ".zip";

  private final PermitGMLWriter writer;
  private final SectorCategories categories;
  private final GeometryCalculator geometryCalculator = new GeometryCalculatorImpl();
  private PMF pmf;
  private int filesCount; //files count in zip fle

  /**
   * @param pmf The Persistence Manager Factory to use.
   * @throws SQLException
   */
  public GMLExportHandler(final PMF pmf) throws SQLException {
    this.setPMF(pmf);
    writer = new PermitGMLWriter(pmf);
    categories = SectorRepository.getSectorCategories(pmf, LocaleUtils.getDefaultLocale());
  }

  @Override
  public void prepare(final CalculationInputData inputData, final ArrayList<StringDataSource> backupAttachments)
      throws SQLException, AeriusException {
    final Scenario scenario = inputData.getScenario();
    ensureProperEmissions(scenario);

    // Generate metadata before calculating, because if this fails the calculations are useless.
    LOG.debug("Generating GML for project name: {}", scenario.getMetaData().getProjectName());
  }

  @Override
  public final void createFileContent(final File outputFile, final CalculationInputData inputData,
      final ArrayList<StringDataSource> backupAttachments) throws IOException, AeriusException, SQLException {
    LOG.info("Generating ZIP for project, reference: {}", inputData.getScenario().getMetaData().getReference());
    final File tempDir = Files.createTempDirectory("GML_Export").toFile();
    final List<File> exportFiles = createGMLs(tempDir, inputData);
    ZipFileMaker.files2ZipStream(outputFile, exportFiles, tempDir, true);
    setFilesCount(exportFiles.size());
  }

  @Override
  public ImmediateExportData postProcess(final ImmediateExportData immediateExportData) {
    immediateExportData.setFilesInZip(getFileCount());
    return immediateExportData;
  }

  private List<File> createGMLs(final File tempDir, final CalculationInputData inputData) throws AeriusException, SQLException {
    final Scenario scenario = inputData.getScenario();
    final Optional<ScenarioResults> scenarioResults = inputData.getScenarioResults();
    final List<File> gmlFiles = new ArrayList<>();
    gmlFiles.addAll(defaultGmlExport(tempDir, scenario, scenarioResults));

    // add individual sector result gml files
    if (inputData.isSectorOutput()) {
      gmlFiles.addAll(sectorsGmlExport(tempDir, scenario, scenarioResults));
    }
    return gmlFiles;
  }

  private void ensureProperEmissions(final Scenario scenario) throws SQLException, AeriusException {
    for (final ScenarioSituation situation : scenario.getSituations()) {
      try (Connection con = getPMF().getConnection()) {
        final EmissionFactorSupplier supplier = new CategoryBasedEmissionFactorSupplier(categories, pmf, situation.getYear());
        final EmissionsUpdater updater = new EmissionsUpdater(supplier, geometryCalculator);
        updater.updateEmissions(situation.getEmissionSourcesList());
      }
    }
  }

  private List<File> defaultGmlExport(final File tempDir, final Scenario scenario, final Optional<ScenarioResults> results)
      throws AeriusException, SQLException {
    return exportGML(tempDir, scenario, results, this::getResults, Optional.empty());
  }

  private List<File> sectorsGmlExport(final File tempDir, final Scenario scenario, final Optional<ScenarioResults> results)
      throws AeriusException, SQLException {
    final List<File> generated = new ArrayList<>();
    for (final int sectorId : determineSectors(scenario)) {
      generated.addAll(sectorGmlExport(tempDir, scenario, results, sectorId));
    }
    return generated;
  }

  private List<Integer> determineSectors(final Scenario scenario) {
    final List<Integer> sectorList = new ArrayList<>();
    // create unique sector list
    for (final ScenarioSituation situation : scenario.getSituations()) {
      for (final EmissionSourceFeature emissionSource : situation.getSources().getFeatures()) {
        final int sectorId = emissionSource.getProperties().getSectorId();
        if (!sectorList.contains(sectorId)) {
          sectorList.add(sectorId);
        }
      }
    }
    return sectorList;
  }

  private List<File> sectorGmlExport(final File tempDir, final Scenario scenario, final Optional<ScenarioResults> results, final int sectorId)
      throws AeriusException, SQLException {
    return exportGML(tempDir, scenario, results, calculationId -> getSectorResults(calculationId, sectorId), Optional.of(new SectorFilter(sectorId)));
  }

  private List<File> exportGML(final File tempDir, final Scenario scenario, final Optional<ScenarioResults> results,
      final IntFunction<List<CalculationPointFeature>> calculationResultRetrieval, final Optional<SectorFilter> filter)
      throws AeriusException, SQLException {
    final List<ScenarioSituation> situations = scenario.getSituations();
    final List<File> files = new ArrayList<>(situations.size());
    for (final ScenarioSituation situation : situations) {
      final MetaDataInput metaDataInput = constructMetaData(scenario, situation, results);
      final Optional<ScenarioSituationResults> scenarioResults = results.map(r -> r.getResultsPerSituation().get(situation.getId()));
      final Optional<Integer> calculationId = scenarioResults.map(ScenarioSituationResults::getCalculationId);
      final List<CalculationPointFeature> receptorPoints = getReceptors(calculationId, scenario, calculationResultRetrieval);
      final IsScenario gmlScenario = GMLScenarioHelper.constructScenario(situation, receptorPoints);
      if (filter.isPresent()) {
        final SectorFilter sourceFilter = filter.get();
        final List<EmissionSourceFeature> sources = gmlScenario.getSources().stream()
            .filter(sourceFilter::include)
            .collect(Collectors.toList());
        gmlScenario.getSources().clear();
        gmlScenario.getSources().addAll(sources);
      }

      files.add(writer.writeToFile(tempDir, gmlScenario, metaDataInput, situations.indexOf(situation),
          filter.isPresent() ? Optional.ofNullable(filter.get().getOptionalFileName()) : Optional.empty(), Optional.empty()));
    }
    return files;
  }

  private MetaDataInput constructMetaData(final Scenario scenario, final ScenarioSituation situation, final Optional<ScenarioResults> results)
      throws SQLException {
    return GMLScenarioHelper.constructMetaData(scenario, situation,
        () -> results.isPresent() && results.get().getResultsPerSituation().containsKey(situation.getId()),
        AeriusVersion.getVersionNumber(), getPMF().getDatabaseVersion());
  }

  /**
   * Get the receptors from the scenario: results if calculation id is valid, custom calculation point otherwise (and available).
   */
  private List<CalculationPointFeature> getReceptors(final Optional<Integer> calculationId, final Scenario scenario,
      final IntFunction<List<CalculationPointFeature>> calculationResultRetrieval) {
    final List<CalculationPointFeature> receptors = new ArrayList<>();
    if (!calculationId.isPresent() || calculationId.get() == 0) {
      receptors.addAll(scenario.getCustomPointsList());
    } else {
      receptors.addAll(calculationResultRetrieval.apply(calculationId.get()));
      if (receptors.isEmpty()) {
        receptors.addAll(scenario.getCustomPointsList());
      }
    }
    return receptors;
  }

  private List<CalculationPointFeature> getResults(final int calculationId) {
    try (final Connection connection = getPMF().getConnection()) {
      final PartialCalculationResult result = CalculationInfoRepository.getCalculationResults(connection, calculationId);
      return result.getResults();
    } catch (final SQLException e) {
      throw new RuntimeException("SQL exception retrieving results for GML export", e);
    }
  }

  private List<CalculationPointFeature> getSectorResults(final int calculationId, final int sectorId) {
    try (final Connection connection = getPMF().getConnection()) {
      final PartialCalculationResult result = CalculationInfoRepository.getCalculationSectorResults(connection,
          calculationId, sectorId);
      return result.getResults();
    } catch (final SQLException e) {
      throw new RuntimeException("SQL exception retrieving sector results for GML export", e);
    }
  }

  @Override
  public MessagesEnum getMailSubject(final CalculationInputData inputData) {
    return inputData.isNameEmpty() ? MessagesEnum.GML_MAIL_SUBJECT : MessagesEnum.GML_MAIL_SUBJECT_JOB;
  }

  @Override
  public void setReplacementTokens(final CalculationInputData inputData, final MailMessageData mailMessageData) {
    mailMessageData.setReplacement(ReplacementToken.AERIUS_REFERENCE, inputData.getScenario().getMetaData().getReference());
    mailMessageData.setReplacement(ReplacementToken.JOB, inputData.getName());
  }

  @Override
  public MessagesEnum getMailContent(final CalculationInputData inputData) {
    return MessagesEnum.GML_MAIL_CONTENT;
  }

  @Override
  public String getFileName(final CalculationInputData inputData) {
    return FileUtil.getFileName(FILENAME_GML_PREFIX, FILENAME_GML_EXTENSION, null, null);
  }

  private PMF getPMF() {
    return pmf;
  }

  private void setPMF(final PMF pmf) {
    this.pmf = pmf;
  }

  private void setFilesCount(final int size) {
    this.filesCount = size;
  }

  private int getFileCount() {
    return filesCount;
  }

  private static class SectorFilter {

    private final int sectorId;

    SectorFilter(final int sectorId) {
      this.sectorId = sectorId;
    }

    public String getOptionalFileName() {
      return String.valueOf(sectorId);
    }

    public boolean include(final EmissionSourceFeature emissionSource) {
      return emissionSource.getProperties().getSectorId() == sectorId;
    }

  }

}
