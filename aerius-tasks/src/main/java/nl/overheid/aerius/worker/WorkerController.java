/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.metrics.MetricFactory;
import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.configuration.BrokerConfiguration;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.emissions.EmissionWriter;
import nl.overheid.aerius.exec.ExecuteWrapper;
import nl.overheid.aerius.intercept.WorkerInterceptor;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.InvalidInputException;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.base.ScheduledWorkerFactory;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;
import nl.overheid.aerius.worker.util.WorkerClientPoolContainer;
import nl.overheid.aerius.worker.util.WorkerPMFFactory;

/**
 * Worker Main class.
 */
public final class WorkerController {

  private static final Logger LOG = LoggerFactory.getLogger(WorkerController.class);

  private static String configurationFile;

  private final WorkerClientPoolContainer container = new WorkerClientPoolContainer();
  private final WorkerFactory<?>[] workerFactories;
  private final ScheduledWorkerFactory<?>[] scheduledWorkerFactories;

  public WorkerController(final WorkerFactory<?>[] workerFactories, final ScheduledWorkerFactory<?>[] scheduledWorkerFactories) {
    this.workerFactories = workerFactories;
    this.scheduledWorkerFactories = scheduledWorkerFactories;
  }

  /**
   * When this main method is used, the worker will be started.
   *
   * @param args no arguments needed, but if supplied, they should fit the description given by -help.
   * @throws Exception Exceptions during startup
   */
  public void start(final String[] args) throws Exception {
    final CmdOptions cmdOpts = new CmdOptions(args);

    if (cmdOpts.printIfInfoOption()) {
      return;
    }
    final String emissionsFile = cmdOpts.getEmssionsFile();
    final String requestFiles = cmdOpts.getRequestFilesType();
    final String storePPPath = cmdOpts.getStorePPFile();
    configurationFile = cmdOpts.getConfigFile();

    LOG.info("Configuration file: {}", configurationFile);
    Thread.setDefaultUncaughtExceptionHandler((t, e) -> LOG.error("Uncaught exception from {}", t, e));
    // Read the configuration.
    final Properties props = readProperties();
    final BrokerConfiguration brokerConfig = new BrokerConfiguration(props);
    final List<String> validationErrors = brokerConfig.getValidationErrors();

    if (!validationErrors.isEmpty()) {
      throw new InvalidInputException(validationErrors);
    }

    // Init metric factory using a shortId
    MetricFactory.init(props,
        "worker-" + Long.toString(ByteBuffer.wrap(UUID.randomUUID().toString().getBytes(Charset.defaultCharset())).getLong(), Character.MAX_RADIX));

    final ExecutorService executor = Executors.newCachedThreadPool();
    final BrokerConnectionFactory factory = new BrokerConnectionFactory(executor, brokerConfig.buildConnectionConfiguration());
    try {
      if (emissionsFile != null) {
        startWriteEmissionFiles(props, emissionsFile, cmdOpts.getYear(), cmdOpts.getTarget());
      } else if (cmdOpts.getInterceptCmdOptions().isIntercept()) {
        new WorkerInterceptor(factory, workerFactories, props).intercept(cmdOpts.getInterceptCmdOptions());
      } else {
        startWorkers(executor, factory, props);
        startScheduledWorkers(executor, factory, props);
        waitForEver(executor);
      }
    } finally {
      try {
        container.stopWorkers();
      } catch (final RuntimeException e) {
        LOG.debug("Exception while stopping worker", e);
      }
      factory.shutdown();
      executor.shutdownNow();
      ExecuteWrapper.EXECUTOR.shutdownNow();
    }
  }

  private static Properties readProperties() throws IOException {
    return PropertiesUtil.getFromPropertyFile(configurationFile);
  }

  /**
   * Starts all workers of the worker factories.
   *
   * @param executor executor to start the worker threads on
   * @param factory RabbitMQ connection factory
   * @param properties configuration properties
   * @throws Exception Exceptions during startup
   */
  private void startWorkers(final ExecutorService executor, final BrokerConnectionFactory factory, final Properties properties) throws Exception {
    final WorkerConfiguration[] configs = new WorkerConfiguration[workerFactories.length];
    // Validations
    InvalidInputException invalidInputException = null;
    for (int i = 0; i < workerFactories.length; i++) {
      configs[i] = workerFactories[i].createConfiguration(properties);
      invalidInputException = validate(configs[i], invalidInputException);
    }
    handleInvalidInput(invalidInputException);
    // start workers
    for (final WorkerFactory<?> workerFactory : workerFactories) {
      final Runnable runner = container.createWorkerClientPool(properties, factory, workerFactory);

      if (runner != null) {
        executor.execute(runner);
      }
    }
  }

  private static InvalidInputException validate(final WorkerConfiguration workerConfig, final InvalidInputException invalidInputException) {
    InvalidInputException returnException = invalidInputException;
    try {
      workerConfig.validate();
    } catch (final InvalidInputException e) {
      if (invalidInputException == null) {
        returnException = e;
      } else {
        returnException.merge(e);
      }
    }
    return returnException;
  }

  private static void handleInvalidInput(final InvalidInputException invalidInputException) throws InvalidInputException {
    if (invalidInputException != null) {
      LOG.error("One or more validations failed:");
      for (final String violation : invalidInputException.getConstraintViolations()) {
        LOG.error("- {}", violation);
      }
      throw invalidInputException;
    }
  }

  /**
   * Start workers that run on an internal schedule.
   *
   * @param executor
   * @param factory broker factory
   * @param props configuration properties.
   * @throws IOException
   * @throws AeriusException
   * @throws SQLException
   */
  @SuppressWarnings({"unchecked", "rawtypes"})
  private void startScheduledWorkers(final ExecutorService executor, final BrokerConnectionFactory factory, final Properties props)
      throws IOException, AeriusException, SQLException {
    for (final ScheduledWorkerFactory workerFactory : scheduledWorkerFactories) {
      final WorkerConfiguration cfg = workerFactory.createConfiguration(props);
      if (workerFactory.isEnabled(cfg)) {
        executor.execute(workerFactory.createScheduledWorker(cfg, factory));
      }
    }
  }

  private void waitForEver(final ExecutorService executor) {
    final Object waitForever = new Object();
    synchronized (waitForever) {
      while (!executor.isTerminated()) {
        try {
          waitForever.wait();
        } catch (final InterruptedException e) {
          LOG.error("Thread interrupted while waiting forever.", e);
          Thread.currentThread().interrupt();
        }
      }
    }
  }

  /**
   * @param emissionsFile
   * @param year
   * @param string
   * @throws SQLException
   * @throws AeriusException
   * @throws IOException
   */
  private void startWriteEmissionFiles(final Properties properties, final String emissionsFile, final int year, final String target)
      throws IOException, AeriusException, SQLException {
    final PMF pmf = WorkerPMFFactory.createPMF(new DBWorkerConfiguration(properties, ProductType.CALCULATOR, WorkerType.CONNECT));
    EmissionWriter.writeEmissions(pmf, emissionsFile, year, target);
  }

}
