/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.configuration;

import java.util.Properties;

import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * WorkerConfiguration assures the properties for OPS are always retrieved
 * by the same property name throughout the application.
 */
public class ImportWorkerConfiguration extends DBWorkerConfiguration {

  /**
   * Configuration for DB workers.
   * @param properties Properties containing the predefined properties.
   * @param productType product database
   */
  public ImportWorkerConfiguration(final Properties properties, final ProductType productType, final WorkerType workerType) {
    super(properties, productType, workerType);
  }

  @Override
  public int getProcesses() {
    return getProcesses(getProductType().getDbRepresentation() + ".import." + PROCESSES);
  }

}
