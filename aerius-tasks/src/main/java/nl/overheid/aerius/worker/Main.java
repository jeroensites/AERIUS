/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.adms.ADMSWorkerFactory;
import nl.overheid.aerius.calculator.JobCleanupWorkerFactory;
import nl.overheid.aerius.ops.OPSWorkerFactory;
import nl.overheid.aerius.srm.worker.SRMWorkerFactory;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.base.ScheduledWorkerFactory;

/**
 * Main class for all workers.
 */
public class Main {
  private static final Logger LOG = LoggerFactory.getLogger(Main.class);

  private static final WorkerFactory<?>[] WORKER_FACTORIES = {
      new ADMSWorkerFactory(),
      new OPSWorkerFactory(),
      new SRMWorkerFactory(),
      new CalculatorWorkerFactory(WorkerType.CONNECT),
      new MessageWorkerFactory(),
  };

  private static final ScheduledWorkerFactory<?>[] SCHEDULED_WORKERS = {
      new JobCleanupWorkerFactory(),
  };


  public static void main(final String[] args) {
    try {
      final WorkerController worker = new WorkerController(WORKER_FACTORIES, SCHEDULED_WORKERS);
      worker.start(args);
    } catch (final Exception e) {
      LOG.error("Exception in main. ", e);
      System.exit(1); // NOPMD - not a JEE application, so we can exit as we like.
    }
  }
}
