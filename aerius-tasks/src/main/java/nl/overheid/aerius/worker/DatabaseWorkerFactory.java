/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Properties;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;
import nl.overheid.aerius.worker.util.WorkerPMFFactory;

/**
 * Factory for database based workers.
 */
abstract class DatabaseWorkerFactory implements WorkerFactory<DBWorkerConfiguration> {

  protected final WorkerType workerType;
  private final ProductType productType;

  protected DatabaseWorkerFactory(final WorkerType workerType, final ProductType productType) {
    this.workerType = workerType;
    this.productType = productType;
  }

  @Override
  public DBWorkerConfiguration createConfiguration(final Properties properties) {
    return new DBWorkerConfiguration(properties, productType, workerType);
  }

  @Override
  public final Worker<Serializable, Serializable> createWorkerHandler(final DBWorkerConfiguration configuration,
      final BrokerConnectionFactory factory) throws AeriusException, IOException, SQLException {
    return createWorkerHandlerWithPMF(configuration, factory);
  }

  protected Worker<Serializable, Serializable> createWorkerHandlerWithPMF(final DBWorkerConfiguration configuration,
      final BrokerConnectionFactory factory) throws AeriusException, IOException, SQLException {
    return createWorkerHandler(configuration, factory, WorkerPMFFactory.createPMF(configuration));
  }

  /**
   * Creates the worker type specific worker handler.
   * @param configuration
   * @param factory
   * @param pmf
   * @return
   * @throws AeriusException
   * @throws SQLException
   */
  protected abstract Worker<Serializable, Serializable> createWorkerHandler(DBWorkerConfiguration configuration, BrokerConnectionFactory factory,
      PMF pmf) throws AeriusException, IOException, SQLException;

  @Override
  public WorkerType getWorkerType() {
    return workerType;
  }

  public ProductType getProductType() {
    return productType;
  }

}
