/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.export.PdfExportWorkerConfiguration;
import nl.overheid.aerius.export.PdfExportWorkerFactory;
import nl.overheid.aerius.paa.PAAExportHandler;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.export.OPSExportData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusExceptionReason;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;
import nl.overheid.aerius.worker.util.WorkerPMFFactory;

/**
 * DatabaseWorkerFactory for calculator database worker.
 */
public class CalculatorWorkerFactory extends DatabaseWorkerFactory {

  private static final Logger LOG = LoggerFactory.getLogger(CalculatorWorkerFactory.class);

  private PdfExportWorkerConfiguration pdfExportWorkerConfiguration;

  public CalculatorWorkerFactory(final WorkerType workerType) {
    super(workerType, ProductType.CALCULATOR);
  }

  @Override
  public DBWorkerConfiguration createConfiguration(final Properties properties) {
    pdfExportWorkerConfiguration = new PdfExportWorkerFactory().createConfiguration(properties);
    return super.createConfiguration(properties);
  }

  @Override
  protected Worker<Serializable, Serializable> createWorkerHandlerWithPMF(final DBWorkerConfiguration configuration,
      final BrokerConnectionFactory factory) throws AeriusException, IOException, SQLException {
    return new CalculatorWorkerHandler(configuration, pdfExportWorkerConfiguration, factory, workerType);
  }

  @Override
  protected Worker createWorkerHandler(final DBWorkerConfiguration configuration, final BrokerConnectionFactory factory, final PMF pmf)
      throws AeriusException, IOException, SQLException {
    throw new AssertionError("createWorkerHandler should not be called, but createWorkerHandlerWithPMF override");
  }

  /**
   * Worker for the Database worker queue. This worker sends the data to the specific
   * database worker.
   */
  private static class CalculatorWorkerHandler implements Worker<Serializable, Serializable> {
    private static final String DB_CONNECTION_UI = "ui";
    private static final String DB_CONNECTION_EXPORT = "export";

    private final BrokerConnectionFactory factory;
    private final DBWorkerConfiguration configuration;
    private final PMF pmfUI;
    private final PMF pmfExport;
    private final CalculatorBuildDirector calculatorBuildDirectorUI;
    private final CalculatorBuildDirector calculatorBuildDirectorExport;
    private final PdfExportWorkerConfiguration pdfExportWorkerConfiguration;

    /**
     * @param configuration The database configuration to use for setting up connections with the DB and such
     * @param factory broker connection factory
     * @throws SQLException
     * @throws AeriusException
     */
    public CalculatorWorkerHandler(final DBWorkerConfiguration configuration, final PdfExportWorkerConfiguration pdfExportWorkerConfiguration,
        final BrokerConnectionFactory factory, final WorkerType workerType) throws SQLException, IOException, AeriusException {
      this.configuration = configuration;
      this.pdfExportWorkerConfiguration = pdfExportWorkerConfiguration;
      this.factory = factory;
      pmfUI = WorkerPMFFactory.createPMF(configuration, DB_CONNECTION_UI);

      calculatorBuildDirectorUI =
          new CalculatorBuildDirector(pmfUI, factory, configuration.getProcesses(), workerType, configuration.getNSLMonitorSrm2Directory());
      if (configuration.getDatabaseUrl(DB_CONNECTION_UI).equals(configuration.getDatabaseUrl(DB_CONNECTION_EXPORT))) {
        pmfExport = pmfUI;
        calculatorBuildDirectorExport = calculatorBuildDirectorUI;
      } else {
        pmfExport = WorkerPMFFactory.createPMF(configuration, DB_CONNECTION_EXPORT);
        calculatorBuildDirectorExport =
            new CalculatorBuildDirector(pmfExport, factory, configuration.getProcesses(), workerType, configuration.getNSLMonitorSrm2Directory());
      }
    }

    @Override
    public Serializable run(final Serializable input, final WorkerIntermediateResultSender resultSender, final JobIdentifier jobIdentifier)
        throws Exception {
      final Serializable output;
      try {
        if (input instanceof CalculationInputData) {
          output = handleCalculationExport((CalculationInputData) input, resultSender, jobIdentifier);
        } else if (input instanceof OPSExportData) {
          output = handleOPSExport((OPSExportData) input, jobIdentifier);
        } else {
          throw new UnsupportedOperationException("Worker does not know how to handle the input:"
              + input.getClass().getName());
        }
      } catch (final SQLException e) {
        LOG.error("SQLException for " + input.getClass(), e);
        return new AeriusException(AeriusExceptionReason.INTERNAL_ERROR);
      }
      return output;
    }

    private Serializable handleCalculationExport(final CalculationInputData input, final WorkerIntermediateResultSender resultSender,
        final JobIdentifier jobIdentifier) throws Exception {
      final Serializable output;
      if (input.getExportType() == ExportType.CALCULATION_UI) {
        output = handleCalculationUI(input, resultSender, jobIdentifier);
      } else {
        output = handleExport(input, jobIdentifier);
      }
      return output;
    }

    private Serializable handleCalculationUI(final CalculationInputData input, final WorkerIntermediateResultSender resultSender,
        final JobIdentifier jobIdentifier) throws Exception {
      return new CalculationWorker(calculatorBuildDirectorUI, pmfUI).run(input, resultSender, jobIdentifier);
    }

    private Serializable handleExport(final CalculationInputData input, final JobIdentifier jobIdentifier) throws Exception {
      final ExportHandler handler;
      final ExportType exportType = input.getExportType();
      switch (exportType) {
        case PAA:
          final ReleaseType releaseType = ConstantRepository.getEnum(pmfExport, ConstantsEnum.RELEASE, ReleaseType.class);
          handler = new PAAExportHandler(pmfExport, pdfExportWorkerConfiguration, jobIdentifier, input.getScenarioFileIdentifier(), releaseType);
          break;
        case GML_WITH_RESULTS:
        case GML_WITH_SECTORS_RESULTS:
        case GML_SOURCES_ONLY:
          handler = new GMLExportHandler(pmfExport);
          break;
        case CSV:
        case CSV_EXTENDED:
          if (input.isNSL()) {
            handler = new DirectoryExportHandler(WorkerUtil.getFolderForJob(jobIdentifier),
                new GMLInputExportHandler(pmfExport),
                new NSLCSVInputExportHandler());
          } else {
            handler = new DirectoryExportHandler(WorkerUtil.getFolderForJob(jobIdentifier));
          }
          break;
        case CALCULATION_UI:
        case OPS:
        default:
          throw new UnsupportedOperationException("Worker does not know how to handle the export type: " + exportType);
      }
      return new CalculatorExportWorker(calculatorBuildDirectorExport, pmfExport, factory, configuration, handler).run(input, jobIdentifier);
    }

    private Serializable handleOPSExport(final OPSExportData input, final JobIdentifier jobIdentifier) throws Exception {
      return new OPSExportWorker(pmfExport, configuration, factory).run(input, jobIdentifier);
    }
  }
}
