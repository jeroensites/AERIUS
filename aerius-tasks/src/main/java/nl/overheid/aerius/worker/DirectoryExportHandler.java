/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.ZipFileMaker;
import nl.overheid.aerius.worker.base.AbstractExportWorker.ImmediateExportData;

/**
 * Worker to ZIP and mail the files that were generated and written in a specific directory.
 */
public class DirectoryExportHandler implements ExportHandler {

  public interface InputExportHandler {
    void exportInput(CalculationInputData inputData, File targetDirectory);

    default void writeErrorInFile(final File targetDirectory, final Exception e, final String message) {
      LOG.error(message, e);
      try {
        FileUtils.writeStringToFile(new File(targetDirectory, "error.log"),
            message + ": " + ExceptionUtils.getStackTrace(e), true);
      } catch (final IOException e1) {
        LOG.trace("Error writing log to file", e1);
      }
    }
  }

  private static final Logger LOG = LoggerFactory.getLogger(DirectoryExportHandler.class);

  private static final String FILENAME_PREFIX = "AERIUS_CSV";

  private int fileCount;

  private final File filesToExportDirectory;
  private final List<InputExportHandler> inputExportHandlers = new ArrayList<>();

  public DirectoryExportHandler(final File filesToExportDirectory, final InputExportHandler... inputExportHandlers) {
    this.filesToExportDirectory = filesToExportDirectory;
    this.inputExportHandlers.addAll(Arrays.asList(inputExportHandlers));
  }

  @Override
  public void prepare(final CalculationInputData inputData, final ArrayList<StringDataSource> backupAttachments)
      throws SQLException, AeriusException {
    // NO-OP
  }

  @Override
  public final void createFileContent(final File outputFile, final CalculationInputData inputData,
      final ArrayList<StringDataSource> backupAttachments) throws IOException {
    generateInputExports(inputData);
    LOG.info("Generating ZIP for CSV files, reference: {}", inputData.getScenario().getMetaData().getReference());
    final File[] exportFiles = filesToExportDirectory.listFiles();
    ZipFileMaker.files2ZipStream(outputFile, Arrays.asList(exportFiles), filesToExportDirectory, true);
    fileCount = exportFiles.length;
    if (!filesToExportDirectory.delete()) {
      LOG.warn("Deleting of temporary directory '{}' failed.", filesToExportDirectory);
    }
  }

  @Override
  public ImmediateExportData postProcess(final ImmediateExportData immediateExportData) {
    immediateExportData.setFilesInZip(fileCount);
    return immediateExportData;
  }

  @Override
  public MessagesEnum getMailSubject(final CalculationInputData inputData) {
    return inputData.isNSL()
        ? MessagesEnum.NSL_MAIL_SUBJECT
        : (inputData.isNameEmpty() ? MessagesEnum.CSV_MAIL_SUBJECT : MessagesEnum.CSV_MAIL_SUBJECT_JOB);
  }

  @Override
  public void setReplacementTokens(final CalculationInputData inputData, final MailMessageData mailMessageData) {
    mailMessageData.setReplacement(ReplacementToken.AERIUS_REFERENCE, inputData.getScenario().getMetaData().getReference());
    mailMessageData.setReplacement(ReplacementToken.JOB, inputData.getName());
    if (inputData.isNSL()) {
      mailMessageData.setDbReplacement(ReplacementToken.MAIL_CONTENT_TEMPLATE, MessagesEnum.NSL_MAIL_CONTENT_TEMPLATE);
    }
  }

  @Override
  public MessagesEnum getMailContent(final CalculationInputData inputData) {
    return inputData.isNSL() ? MessagesEnum.NSL_MAIL_CONTENT  : MessagesEnum.CSV_MAIL_CONTENT;
  }

  @Override
  public String getFileName(final CalculationInputData inputData) {
    return FileUtil.getFileName(FILENAME_PREFIX, FileFormat.ZIP.getExtension(), null, inputData.getCreationDate());
  }

  private void generateInputExports(final CalculationInputData inputData) {
    for (final InputExportHandler inputExportHandler : inputExportHandlers) {
      inputExportHandler.exportInput(inputData, filesToExportDirectory);
    }
  }

}
