/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.client;

import java.io.IOException;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import nl.aerius.taskmanager.client.WorkerResultSender;
import nl.aerius.taskmanager.client.util.QueueHelper;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;

/**
 * Class for workers to consume tasks from the queue and send results back to the clients. The actual work is done by the passed
 * {@link WorkerHandler}.
 */
class WorkerConsumer extends DefaultConsumer {

  private static final Logger LOG = LoggerFactory.getLogger(WorkerConsumer.class);

  private final WorkerHandler workerHandler;

  private final boolean returnResults;

  protected WorkerConsumer(final Channel channel, final WorkerHandler workerHandler) {
    this(channel, workerHandler, true);
  }

  protected WorkerConsumer(final Channel channel, final WorkerHandler workerHandler, final boolean returnResults) {
    super(channel);
    this.returnResults = returnResults;
    if (workerHandler == null) {
      throw new IllegalArgumentException("Workerhandler not allowed to be null.");
    }
    this.workerHandler = workerHandler;
  }

  @Override
  public void handleDelivery(final String consumerTag, final Envelope envelope, final BasicProperties properties, final byte[] body)
      throws IOException {
    LOG.info("Handling delivery. Correlation ID " + properties.getCorrelationId());
    final Channel channel = getChannel();
    try {
      // convert the byte array to a workable Java object.
      handleDelivery(properties, body, channel);
      // acknowledge the message, deleting it from the worker queue.
      channel.basicAck(envelope.getDeliveryTag(), false);
    } catch (final IOException e) {
      LOG.error("IOException shutting down this worker handler.", e);
      throw e;
    } catch (final InterruptedException e) {
      LOG.debug("This worker is shutdown", e);
      Thread.currentThread().interrupt();
    } catch (final Exception e) {
      LOG.error("Exception shutting down this worker handler.", e);
      throw new IOException(e);
    }
  }

  public void handleDelivery(final BasicProperties properties, final byte[] body, final Channel channel) throws Exception {
    final Serializable workLoad = QueueHelper.bytesToObject(body);
    final WorkerResultSender resultHandler = new WorkerResultSender(channel, properties);
    // let the workerHandler do its job.
    final Serializable result = workerHandler.handleWorkLoad(workLoad, resultHandler, properties.getCorrelationId());
    if (returnResults) {
      //send the result back
      resultHandler.sendFinalResult(result);
    }
  }
}
