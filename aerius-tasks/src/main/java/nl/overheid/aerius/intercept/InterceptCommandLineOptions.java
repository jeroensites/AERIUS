/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import java.io.FileNotFoundException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.OptionGroup;

/**
 * Maps all intercept command line options.
 */
public class InterceptCommandLineOptions {

  private static final InterceptCommandOption<?>[] CMD_OPTIONS = {
    InterceptRun.INTERCEPT_RUN_OPTIONS,
    InterceptRedirect.INTERCEPT_REDIRECT_OPTIONS,
    InterceptSave.INTERCEPT_SAVE_OPTIONS,
    InterceptRemove.INTERCEPT_REMOVE_OPTIONS,
  };

  private final CommandLine cmd;

  public InterceptCommandLineOptions(final CommandLine cmd) {
    this.cmd = cmd;
  }

  public static OptionGroup getOptions() {
    final OptionGroup interceptGroup = new OptionGroup();
    for (final InterceptCommandOption<?> ico : CMD_OPTIONS) {
      interceptGroup.addOption(ico.getOption());
    }
    return interceptGroup;
  }

  public boolean isIntercept() {
    for (final InterceptCommandOption<?> ico : CMD_OPTIONS) {
      if (ico.isOption(cmd)) {
        return true;
      }
    }
    return false;
  }

  public InterceptCommand getInterceptOptions() throws MissingArgumentException, FileNotFoundException {
    for (final InterceptCommandOption<?> ico : CMD_OPTIONS) {
      if (ico.isOption(cmd)) {
        return ico.create(cmd.getOptionValues(ico.getOptionName()));
      }
    }
    throw new MissingArgumentException("Intercept command arguments : " + cmd);
  }

  @Override
  public String toString() {
    return cmd.toString();
  }
}
