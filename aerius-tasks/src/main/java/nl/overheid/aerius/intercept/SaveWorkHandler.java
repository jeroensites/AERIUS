/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import org.apache.commons.cli.MissingArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.worker.client.WorkerClient;

class SaveWorkHandler implements WorkerHandler {
  private static final Logger LOG = LoggerFactory.getLogger(SaveWorkHandler.class);

  private final Gson gson = new Gson();
  private final String queueName;
  private final File path;
  private final boolean ack;
  private final boolean sendResults;
  private final Serializable returnValue;


  public SaveWorkHandler(final SaveWorkCommand options, final boolean ack, final boolean sendResults, final Serializable returnValue) {
    queueName = options.getQueueName();
    path = options.getPath();
    this.ack = ack;
    this.sendResults = sendResults;
    this.returnValue = returnValue;
  }

  public WorkerClient run(final BrokerConnectionFactory connectionFactory) throws Exception {
    final WorkerClient client = new WorkerClient(connectionFactory, this, queueName);
    client.get(queueName, ack, sendResults);
    return client;
  }

  @Override
  public Serializable handleWorkLoad(final Serializable input, final WorkerIntermediateResultSender resultSender, final String correlationId)
      throws Exception {
    final File output = new File(path, queueName + '_' + new Date().getTime() + ".json");
    LOG.warn("Writting output of queue '{}' to file:'{}'", queueName, output);
    try (final PrintWriter out = new PrintWriter(output, StandardCharsets.UTF_8.name())) {
      out.println(gson.toJson(input));
    }
    return returnValue;
  }


  /**
   * Base class for save/remove command wrapper.
   */
  abstract static class SaveWorkCommand extends InterceptCommand {
    private static final int IDX_QUEUE_NAME = 0;
    private static final int IDX_PATH = 1;

    private final File path;

    public SaveWorkCommand(final String... optionValues) throws FileNotFoundException, MissingArgumentException {
      super(optionValues);
      if (getOptionValues().length != 2) {
        throw new MissingArgumentException("Invalid number of arguments. It should be: <queuename> <path>");
      }
      path = new File(getOptionValues()[IDX_PATH]);
      if (!path.exists()) {
        throw new FileNotFoundException("The path '" + path + "' doesn't exist");
      }
    }

    public String getQueueName() {
      return getOptionValues()[IDX_QUEUE_NAME];
    }

    public File getPath() {
      return path;
    }
  }
}
