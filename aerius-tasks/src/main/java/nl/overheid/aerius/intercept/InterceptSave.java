/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import java.io.FileNotFoundException;

import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Option;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.intercept.SaveWorkHandler.SaveWorkCommand;
import nl.overheid.aerius.worker.client.WorkerClient;

/**
 * Intercepts the queue and saves the task at the head. The task is not removed from the queue.
 */
class InterceptSave {
  private static final String INTERCEPT_SAVE = "interceptSave";
  private static final Option INTERCEPT_SAVE_OPTION =
      Option.builder(INTERCEPT_SAVE).argName("queue> <path").numberOfArgs(2)
          .desc("Intercepts and saves the task at the head of the queue. The task will remain in the queue.").build();

  public static final InterceptCommandOption<InterceptSaveCommand> INTERCEPT_SAVE_OPTIONS =
      new InterceptCommandOption<InterceptSaveCommand>(INTERCEPT_SAVE, INTERCEPT_SAVE_OPTION) {
        @Override
        protected InterceptSaveCommand create(final String... options) throws MissingArgumentException, FileNotFoundException {
          return new InterceptSaveCommand(options);
        }
  };

  private final SaveWorkHandler handler;

  public InterceptSave(final InterceptSaveCommand options) throws FileNotFoundException, MissingArgumentException {
    handler = new SaveWorkHandler(options, false, false, null);
  }

  public WorkerClient save(final BrokerConnectionFactory connectionFactory) throws Exception {
    return handler.run(connectionFactory);
  }

  /**
   * Intercept save command wrapper.
   */
  static class InterceptSaveCommand extends SaveWorkCommand {

    public InterceptSaveCommand(final String... optionValues) throws FileNotFoundException, MissingArgumentException {
      super(optionValues);
    }

    @Override
    public void run(final InterceptVisitor visitor) throws Exception {
      visitor.run(this);
    }
  }
}
