/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import java.io.IOException;

import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.GetResponse;

import nl.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Intercepts the queue and redirects the task at the head to a different queue.
 */
class InterceptRedirect {
  private static final Logger LOG = LoggerFactory.getLogger(InterceptRedirect.class);

  private static final String INTERCEPT_REDIRECT = "interceptRedirect";
  private static final Option INTERCEPT_REDIRECT_OPTION =
      Option.builder(INTERCEPT_REDIRECT).argName("queue> <redirectQueue").numberOfArgs(2)
        .desc("Intercepts the given queue and redirects the message to the given queue. The redirectQueue will be prefixed"
          + "with 'intercept' and will fail if the queue already exists.").build();
  private static final int IDX_IN_QUEUE_NAME = 0;
  private static final int IDX_OUT_QUEUE_NAME = 1;

  public static final InterceptCommandOption<InterceptRedirectCommand> INTERCEPT_REDIRECT_OPTIONS =
      new InterceptCommandOption<InterceptRedirectCommand>(INTERCEPT_REDIRECT, INTERCEPT_REDIRECT_OPTION) {
        @Override
        protected InterceptRedirectCommand create(final String... option) throws MissingArgumentException {
          return new InterceptRedirectCommand(option);
        }
  };


  private final String outQueueName;
  private final String inQueueName;

  public InterceptRedirect(final InterceptRedirectCommand options) {
    inQueueName = options.getInQueueName();
    outQueueName = InterceptUtil.createInterceptQueueName(options.getOutQueueName());
  }

  public void redirect(final BrokerConnectionFactory factory) throws IOException, InterruptedException {
    final Connection connection = factory.getConnection();
    final Channel in = connection.createChannel();
    final Channel out = connection.createChannel();
    try {
      out.queueDeclare(outQueueName, false, false, false, null);
      final GetResponse response = in.basicGet(inQueueName, true);
      if (response == null) {
        throw new IOException("Trying to read message of queue '" + inQueueName + "', but this queue is empty or has Unacked messages");
      }
      out.basicPublish("", outQueueName, response.getProps(), response.getBody());
      LOG.warn("Redirected output of queue '{}' to queue '{}'", inQueueName, outQueueName);
    } finally {
      try {
        if (in.isOpen()) {
          in.close();
        }
        if (out.isOpen()) {
          out.close();
        }
      } catch (final IOException e) {
        LOG.error("close connection caused exception.", e);
      }
    }
  }

  /**
   * Intercept redirect command wrapper.
   */
  static class InterceptRedirectCommand extends InterceptCommand {

    public InterceptRedirectCommand(final String... optionValues) throws MissingArgumentException {
      super(optionValues);
      if (getOptionValues().length != 2) {
        throw new MissingArgumentException("No valid argument. It should be : <queue> <redirectQueue>");
      }
    }

    @Override
    public void run(final InterceptVisitor visitor)
        throws UnrecognizedOptionException, InterruptedException, MissingArgumentException, IOException, AeriusException {
      visitor.run(this);
    }

    public String getInQueueName() {
      return getOptionValues()[IDX_IN_QUEUE_NAME];
    }

    public String getOutQueueName() {
      return getOptionValues()[IDX_OUT_QUEUE_NAME];
    }
  }
}
