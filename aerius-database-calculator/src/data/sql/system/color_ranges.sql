INSERT INTO system.color_ranges 
	(color_range_type, lower_value, color)
VALUES
	('deposition_contribution', 0, 'fffdb3'),
	('deposition_contribution', 1.07, 'fde76a'),
	('deposition_contribution', 2.86, 'feb66e'),
	('deposition_contribution', 5, 'a5cc46'),
	('deposition_contribution', 7.14, '23a870'),
	('deposition_contribution', 10, '5a7a32'),
	('deposition_contribution', 15, '0093bd'),
	('deposition_contribution', 20, '0d75b5'),
	('deposition_contribution', 25, '6a70b1'),
	('deposition_contribution', 35.71, '304594'),
	('deposition_contribution', 71.43, '5e2c8f'),
	('deposition_contribution', 107.14, '3f2a84'),
	('deposition_contribution', 142.86, '2a1612');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('deposition_difference', '-infinity'::real, '507122'),
	('deposition_difference', -20, '507122'),
	('deposition_difference', -10, '738d4e'),
	('deposition_difference', -5, '96aa7a'),
	('deposition_difference', -2.86, 'b9c6a7'),
	('deposition_difference', -1.07, 'dce3d3'),
	('deposition_difference', 0, 'd8d3e5'),
	('deposition_difference', 1.07, 'b1a9cb'),
	('deposition_difference', 2.86, '8b7db0'),
	('deposition_difference', 5, '645296'),
	('deposition_difference', 10, '3d277c'),
	('deposition_difference', 20, '3d277c');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('deposition_background', 0, 'FFFFD4'),
	('deposition_background', 700, 'FEE391'),
	('deposition_background', 980, 'FEC44F'),
	('deposition_background', 1260, 'FE9929'),
	('deposition_background', 1540, 'EC7014'),
	('deposition_background', 1960, 'CC4C02'),
	('deposition_background', 2240, '8C2D04');

INSERT INTO system.color_ranges
	(color_range_type, lower_value, color)
VALUES
	('road_max_speed', 80, 'ccff66'),
	('road_max_speed', 100, 'dfdf13'),
	('road_max_speed', 120, 'e77400'),
	('road_max_speed', 130, 'e70000'),
	('road_max_speed', 0, '333333');
