{import_common 'system/layers.sql'}

--wms layers from 11+
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (11, 11, 'wms', 'calculator:wms_nature_areas_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (12, 12, 'wms', 'calculator:wms_depositions_jurisdiction_policies_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (13, 13, 'wms', 'calculator:wms_habitat_areas_sensitivity_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (14, 14, 'wms', 'calculator:wms_habitat_types');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (15, 15, 'wms', 'verkeersscheidingsstelsel_nz:begrenzing,verkeersscheidingsstelsel_nz:symbolen,verkeersscheidingsstelsel_nz:separatiezones');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (16, 16, 'wms', 'calculator:wms_shipping_maritime_network_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (17, 10, 'wms', 'calculator:wms_habitats_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (18, 10, 'wms', 'calculator:wms_relevant_habitat_info_for_receptor_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (19, 25, 'wms', 'calculator:wms_calculation_substance_deposition_results_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (20, 27, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (21, 28, 'wms', 'calculator:wms_province_areas_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (22, 29, 'wms', 'calculator:wms_inland_shipping_routes');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (23, 30, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (24, 31, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_percentage');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (25, 32, 'wms', 'calculator:wms_user_source_labels');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (26, 33, 'wms', 'calculator:wms_user_road_speed_types');	
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (27, 34, 'wms', 'calculator:wms_user_road_total_vehicles_per_day');	
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (28, 35, 'wms', 'calculator:wms_user_road_total_vehicles_per_day_light_traffic');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (29, 36, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_label');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (30, 37, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_percentage_label');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (31, 21, 'wms', 'calculator:wms_calculation_exceeding_receptor_segment_space_rule_checks');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (32, 21, 'wms', 'calculator:wms_calculation_exceeding_receptor_priority_project_space_rule_checks');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (33, 22, 'wms', 'calculator:wms_calculation_exceeding_receptors_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (34, 22, 'wms', 'calculator:wms_calculations_exceeding_receptors_difference_view');

-- theme layers
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (5, 'wnb', 2, true, false); -- brtachtergrondkaart standaard
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (7, 'wnb', 1, true, false); -- brtachtergrondkaart pastel
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (8, 'wnb', 0, true, true); -- brtachtergrondkaart grijs
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (9, 'wnb', 3, true, false); -- brtachtergrondkaart water
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (21, 'wnb', 4, true, true); -- provinciegrenzen
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (6, 'wnb', 5, false, false); -- luchtfoto
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (4, 'wnb', 6, false, false); -- bag kaart
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (11, 'wnb', 7, false, true); -- natuurgebieden
--INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
--  VALUES (12, 'wnb', 8, false, false); -- achtergronddeposities
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (13, 'wnb', 9, false, false); -- stikstofgevoeligheid
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (14, 'wnb', 10, false, false); --habittattypen
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (15, 'wnb', 11, false, false); -- zeescheepvaart netwerk
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (16, 'wnb', 12, false, false); -- scheepvaart netwerk
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (22, 'wnb', 13, false, false); -- binnenvaart netwerk
