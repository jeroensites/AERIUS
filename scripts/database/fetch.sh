export SSHPASS=$(./set_credentials.sh)

rm -rf data > /dev/null
mkdir data

sshpass -e sftp -oBatchMode=no -o "StrictHostKeyChecking=no" -b - aeriusro@aerius-sftp.rivm.nl << !
  cd latest-db/calculator
  lcd data
  get AERIUSII-calculator*
  bye
!
