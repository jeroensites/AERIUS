psql -U postgres postgres <<OMG
  create database calculator;
  create user aerius with encrypted password 'aerius';
  grant all privileges on database calculator to aerius;
OMG
