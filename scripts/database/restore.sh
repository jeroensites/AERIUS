cd data

FILENAME=$(ls | grep AERIUSII-calculator*.backup)

dropdb -U aerius calculator
createdb -U aerius calculator
pg_restore -j4 -d calculator -U aerius $FILENAME

