# Manual (calculator) db building

Note the scripts in this directory are both the documentation of what to do as well as usable scripts to manage your environment.

## Docker
Requirements: Docker (duh!).
Assumes `SFTP_READONLY_PASSWORD` environment variable is set. Can also be supplied using `SFTP_READONLY_PASSWORD=... <command>`.

To build the latest database, run `build-docker.sh`.

To run the latest database, run `run-docker.sh`.

## Non-Docker
To install ruby gems, run `setup.sh`

To configure database user settings, run `configure-settings.sh` and input things like the db-data dir.

Optional: to (later) edit the user settings, run `edit-settings.sh`.

To sync remote files, run `sync.sh`.

To build the database, run `build.sh`. The database will be named `calculator`.
