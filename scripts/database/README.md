# DATABASE

This simple directory contains some convenience scripts to fetch and restore a calculator database. It is designed to work swimmingly within the git repository without leaking credentials.

# Usage

1. Copy `set_credentials.sh.template` to `set_credentials.sh`

2. Edit `set_credentials.sh`

3. Set the sftp password (instructions are in that file)

4. Run `./update.sh`

# Information

The scripts in this directory fetch the latest database dump from the sftp, and restores that to the local postgresql installation.

For manual building, see scripts and instructions in `/scripts/database/build/`.
