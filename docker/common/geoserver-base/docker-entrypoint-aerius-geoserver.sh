#!/bin/sh

set -e

# if GEOSERVER_CONSOLE_DISABLED is set to true, disable the web interface - this also speeds up startup
if [ "${GEOSERVER_CONSOLE_DISABLED}" = "true" ]; then
  export JAVA_OPTIONS="${JAVA_OPTIONS} -DGEOSERVER_CONSOLE_DISABLED=true"
fi

# execute the CMD
exec "${@}"
