#!/usr/bin/env bash

# Crash on errors
set -e

### Validations and defaults ###
: ${AERIUS_WRAPPER_RETRY_AFTER:=3} # in seconds
: ${AERIUS_WRAPPER_WAIT_BEFORE_START:=0} # in seconds

### Run prescript(s) ###
if [[ -n "${AERIUS_WRAPPER_PRESCRIPTS}" ]]; then
  echo '# Found prescripts to run..'

  # Loop through the prescripts. Every prescript is separated by comma's.
  while read -d ',' PRESCRIPT; do
    # Skip empty entries
    if [[ -z "${PRESCRIPT}" ]]; then
      continue
    fi

    # Run prescript (sourcing so it can set/override ENV variables)
    source "${PRESCRIPT}"
  done <<< "${AERIUS_WRAPPER_PRESCRIPTS},"
fi

### Services check ###
if [[ -n "${AERIUS_WRAPPER_CHECK_SERVICES}" ]]; then
  echo '# Found services to check: "'"${AERIUS_WRAPPER_CHECK_SERVICES}"'".'
  echo '# Will retry every '"${AERIUS_WRAPPER_RETRY_AFTER}"' seconds. A dot represents a retry attempt..'

  # Loop through the services. Every service is separated by comma's and the hostname and port by a colon.
  while IFS=':' read -d ',' SERVICE_HOST SERVICE_PORT; do
    # Skip empty entries
    if [[ -z "${SERVICE_HOST}" ]] && [[ -z "${SERVICE_PORT}" ]]; then
      continue
    fi
    # Allow ENV variables in host/port if envsubst is installed
    if command -v envsubst &> /dev/null; then
      SERVICE_HOST=$(echo "${SERVICE_HOST}" | envsubst)
      SERVICE_PORT=$(echo "${SERVICE_PORT}" | envsubst)
    fi

    echo '- Checking '"${SERVICE_HOST}"' on port '"${SERVICE_PORT}"

    # Check until up
    while true; do

      # Read from host and port via Bash - if we can, it should be up.
      if (< /dev/tcp/"${SERVICE_HOST}"/"${SERVICE_PORT}") &> /dev/null; then
        echo '+ Up!'
        break
      fi

      echo -n '.'
      sleep "${AERIUS_WRAPPER_RETRY_AFTER}"s
    done
  done <<< "${AERIUS_WRAPPER_CHECK_SERVICES},"
fi

### Wait before start ###
if [[ "${AERIUS_WRAPPER_WAIT_BEFORE_START}" > 0 ]]; then
  echo '# Waiting for '"${AERIUS_WRAPPER_WAIT_BEFORE_START}"' seconds before start'
  sleep "${AERIUS_WRAPPER_WAIT_BEFORE_START}"s
fi

### Wrapper finalize ###
if [[ -n "${AERIUS_WRAPPER_ENTRYPOINT}" ]]; then
  # Execute normal entrypoint
  echo '# Executing default entrypoint'
  exec "${AERIUS_WRAPPER_ENTRYPOINT}" "${@}"
else
  # Execute command
  echo '# Executing CMD'
  exec "${@}"
fi
