# Entrypoint Wrapper

This image should be used as a base image.

At this time it can do the following stuff before it will call the original entrypoint.
- Check whether specific services are up and continue only if they're up.
- Wait an amount of time.

# Environment variables
- `AERIUS_WRAPPER_ENTRYPOINT`: Path to the entrypoint of the original container. If no entrypoint is set we'll start the CMD without the entrypoint.
- `AERIUS_WRAPPER_CHECK_SERVICES`: To enable service checking.
This should be done in the following format: `<hostname>:<port>`.
Provide multiple by separating them with comma's, e.g: `hostname1:8080,hostname2:5432`.
- `AERIUS_WRAPPER_RETRY_AFTER`: Specify after how many seconds we should retry the checks.
`Defaults to 3 seconds.`
- `AERIUS_WRAPPER_WAIT_BEFORE_START`: Specify after how many seconds we should start the default entrypoint. This is useful for databases that need to do some additional work after startup, like updating constants.
`Defaults to 0 seconds.`
`
