#!/usr/bin/env bash

# Per product check if DB processes are configured and if so add them to the list
#  of services to check before starting up
for PRODUCT in CONNECT MESSAGE; do
  ENV_NAME_PROCESSES="AERIUS_DATABASE_${PRODUCT}_PROCESSES"
  ENV_NAME_URL="AERIUS_DATABASE_${PRODUCT}_URL"
  PRODUCT_PROCESSES="${!ENV_NAME_PROCESSES}"
  PRODUCT_URL="${!ENV_NAME_URL}"

  # If configuration is not set or processes isn't higher than 0, skip it.
  if [[ -z "${PRODUCT_PROCESSES}" ]] \
      || [[ -z "${PRODUCT_URL}" ]] \
      || ! (( "${PRODUCT_PROCESSES}" > 0 )); then
    continue
  fi

  # Get service entry
  SERVICE_ENTRY="${PRODUCT_URL##*://}"
  SERVICE_ENTRY="${SERVICE_ENTRY%%/*}"

  echo '[PRESCRIPT] Found '"${PRODUCT}"' database configuration. Adding: '"${SERVICE_ENTRY}"'.'
  export AERIUS_WRAPPER_CHECK_SERVICES="${AERIUS_WRAPPER_CHECK_SERVICES},${SERVICE_ENTRY}"
done
