#!/usr/bin/env bash

# Start socat in the background that will proxy port 9222 to AERIUS_CHROME_HEADLESS_HOSTNAME on 9222
echo 'Starting socat..'
/usr/bin/socat tcp-listen:9222,fork tcp:"${AERIUS_CHROME_HEADLESS_HOSTNAME}":9222 &

# execute docker CMD
echo 'Executing specified default CMD..'
exec "$@"
