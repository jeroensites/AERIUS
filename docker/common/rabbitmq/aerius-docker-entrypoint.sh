#!/usr/bin/env bash
export RABBITMQ_SERVER_ADDITIONAL_ERL_ARGS="-rabbit default_user \"${AERIUS_RABBITMQ_USER}\" default_pass \"${AERIUS_RABBITMQ_PASSWORD}\" consumer_timeout \"${AERIUS_RABBITMQ_CONSUMER_TIMEOUT}\" max_message_size \"${AERIUS_RABBITMQ_MAX_MESSAGE_SIZE}\" vm_memory_high_watermark ${AERIUS_RABBITMQ_VM_MEMORY_HIGH_WATERMARK}"

# Execute default entrypoint
/usr/local/bin/docker-entrypoint.sh "${@}"
