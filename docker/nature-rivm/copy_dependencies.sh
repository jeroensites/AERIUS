#!/usr/bin/env bash

SOURCE_DIR='../..'
DOCKER_COMMON_DIR='../common'

# Exit on error
set -e

# Change current directory to directory of script so it can be called from everywhere
SCRIPT_PATH=$(readlink -f "${0}")
SCRIPT_DIR=$(dirname "${SCRIPT_PATH}")
cd "${SCRIPT_DIR}"

# include functions
source ../include.functions.sh

# Also copy common dependencies
../copy_dependencies_common.sh "${@}"

# worker-ops
if _is_module_enabled "${1}" 'worker-ops'; then
  cp -auv "${DOCKER_COMMON_DIR}"/worker/worker.properties \
          "${DOCKER_COMMON_DIR}"/worker/app.jar \
          worker-ops/
fi
