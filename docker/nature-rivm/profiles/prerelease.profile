SERVICES=(database-calculator rabbitmq taskmanager api geoserver-opendata worker worker-ops worker-srm nginx)
VOLUMES=(downloads)
declare -A OVERRIDE_ENV_VARIABLES=(
  # Disable Calculator worker
  [AERIUS_WORKER_CALCULATOR_PROCESSES]=0
)
