#!/usr/bin/env bash

if [[ -n "${AERIUS_ADMS_LICENSE_BASE64}" ]]; then
  if [[ -f "${AERIUS_ADMS_URBAN_LICENSE_PATH}" ]]; then
    echo '[WARN] Ignoring AERIUS_ADMS_LICENSE_BASE64 as the license file already exists and we do not want to overwrite it and cause potential issues'
  else
    echo 'Decoding ADMS license to '"${AERIUS_ADMS_URBAN_LICENSE_PATH}"' based on the AERIUS_ADMS_LICENSE_BASE64 environment variable'
    echo "${AERIUS_ADMS_LICENSE_BASE64}" | base64 -d > "${AERIUS_ADMS_URBAN_LICENSE_PATH}" || {
      echo 'Error decoding license.. Make sure it is in BASE64.. Crashing..'
      exit 1
    }
  fi
fi

if [[ ! -f "${AERIUS_ADMS_URBAN_LICENSE_PATH}" ]]; then
  echo '[ERROR] ADMS license not found.. Make sure to provide it as a volume or by setting AERIUS_ADMS_LICENSE_BASE64.'
  exit 1
else

# Symlink the license to the right path so it can be used without us having to know the version numbers.
echo 'Creating license symlinks'
ln -v -s "${AERIUS_ADMS_URBAN_LICENSE_PATH}" "${AERIUS_ADMS_ROOT}/${ADMS_VERSION}/ADMS-Urban.lic"

# execute the CMD
exec "${@}"
