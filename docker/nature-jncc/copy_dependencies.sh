#!/usr/bin/env bash

SOURCE_DIR='../..'
DOCKER_COMMON_DIR='../common'

# Exit on error
set -e

# Change current directory to directory of script so it can be called from everywhere
SCRIPT_PATH=$(readlink -f "${0}")
SCRIPT_DIR=$(dirname "${SCRIPT_PATH}")
cd "${SCRIPT_DIR}"

# include functions
source ../include.functions.sh

# Also copy common dependencies
../copy_dependencies_common.sh "${@}"

# database-calculator-uk
# create source directory and put all sources in there so we can use a single COPY statement in the Dockerfile
# (make sure it's isolated in 1 layer instead of multiple)
if _is_module_enabled "${1}" 'database-calculator-uk'; then
  mkdir -p database-calculator-uk/source
  cp -Rauv "${SOURCE_DIR}"/aerius-database-calculator-uk \
           "${SOURCE_DIR}"/aerius-database-common \
           "${SOURCE_DIR}"/aerius-database-build \
           database-calculator-uk/source/
fi

# worker-adms
if _is_module_enabled "${1}" 'worker-adms'; then
  cp -auv "${DOCKER_COMMON_DIR}"/worker/worker.properties \
          "${DOCKER_COMMON_DIR}"/worker/app.jar \
          worker-adms/
fi
