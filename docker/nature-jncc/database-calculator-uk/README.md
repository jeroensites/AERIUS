### Calculator UK DB image

##### Example build
```shell
docker build \
  --build-arg SFTP_READONLY_PASSWORD="password" \
  -t aerius-database-calculator-uk:latest .
```

##### Example run
```shell
docker run --rm --network host aerius-database-calculator-uk:latest
```
