-- Use dutch messages as basis, and overwrite parts as required
{import_common 'i18n/messages/'}

UPDATE i18n.messages set message = 
E'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<table border="0" width="600">
<tr><td>[MAIL_CONTENT]
<p style="font-family:Arial;font-size:14px;">With kind regards,<br />
[MAIL_SIGNATURE]</p>
<p style="font-family:Arial;font-size:14px;">This an automatically generated email.</p><br /></td></tr>
<tr><td>
<div style="width:560px;border:1px solid #b7c5c5;border-radius:15px;padding:8px 20px 0">
	<table style="border:0" width="560px">
		<tbody>
			<tr>
				<td colspan="2" style="padding-bottom:10px">
					<img alt="AERIUS" height="34" src="data:image/gif;base64,R0lGODlhjwAfANU/AKm5y+Hy+TKItZWmvGeFpZitwrTB0dLl7zdZg+Pt8yhNetLa5HOuzeDm7FVzl8LR3nWNqdHt98LN2UNki/H2+aOyxvDy9TlrlIqiu/r7/WV9noWbtB1FdEtrkc3W4R5pmcTi8MPc6cPX5FiCpBs7bJTU7EdojrPg8WbC5PD5/HbI5ki13imp2ZG/14XO6VWcwaPa73aYtVhvk16w1JvN4zmv2ypfi1e84Ul2nLTT5COTxRtUgxqj1h18rRlCcv///yH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAzODAxMTc0MDcyMDY4MTE4MDgzRTJEQ0EyOUQ3RjY2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkQ2MTY4Qjg0QzdBMTExRTJBRjAyQTNBREMzOTczOTEwIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkQ2MTY4QjgzQzdBMTExRTJBRjAyQTNBREMzOTczOTEwIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowMjgwMTE3NDA3MjA2ODExODIyQUZENkE3ODIxNkZEMCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowMzgwMTE3NDA3MjA2ODExODA4M0UyRENBMjlEN0Y2NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAEAAD8ALAAAAACPAB8AAAb/wJ9wSCwaj8ikcslsOp/QnwFB0lii2Kx2yy1uFk2I9VeZXLvotHrb4HSYFc1PQRrY1/i83ujw+QBLCA0/JBIKDQh7iotdBn4+ChRKJEIkdYSMmZpNGQ4InwgDShqidTISMpuqq1sWEwMVPxAKYKy2t00DVDK1uL6/UCU8w8QpSCA6ycrLOjRDIT3R0j0CLy0hRyIf2x9FDzvgO0gj4UcWcSaPJg4QTBQFGh0T8/T17BJZN8TEJ8fM/86EQJtGsMeLBEW0cfMWTtwRcuCMDFDwqKKfJYYsarTo4IyTFPuIqfD3b1nAHwMLThMgaYjCbQzLPZQ5ZMPGRw6UeOBws6cP/xMZnsAgtoIYC5LJQChdCiLAs2lDDjCYxoDIy25EvtEsAtGhEAkVIVSQQLbCnSQTfPoU5UQFsQj7IhxBpmxJyh5FWkB1yQ3rEK0RZwYWAuGRgSgL1Pqc8ITFsBo/ig5zMXeZ3b1DEkzDJuRqzMFct1L8iQWA4p4cnJwQ+cMF0cp1ldw1spnvwqwNx21VV9pngQ4+nbge1g/EPqdF6Ca7LI22tAO2YeLeSqQrkdE+DkMxfXNChsQ9ndQoJmRfCSPKdTCPViSHNAFW+372Ghp04UcQtDfhvvEB4fBMwDXMDUPoMwwK6JVk2VPNEfGCNFVF55cQgNFX3VZgWaRAfkzwZ/8RAUNYgJ1FTQgzzHlCmDiMMUSkVxIRswkh1XsISTifbqCRcRMCsSThYUUbFCBkAX1s1IRkPMglhIDEJadgbAKpRNBJnck3XY5DWFeEBEVq1A4SP55WERMgDXMUEY4NM1KLyzC1FIxSRvMCdAlZ+Vduglk4RAMDpGORfkWEKeZFSwx1YBEoGOUklEjcVVAL2dhJIZ5GaJlEAxpUlNMRgg7KRKIhhTqMkkKktx5eP2gmDWfx3XYndVnCekSmj4A5qJFLiKorZUOYKhtmekXDUp2uCtFAQ4MYYQM4ODSRIaFGdCrmEqvpGuoKbDJ6RIw/UCCANJC2Kh0RDRVgBGAxNOH/iB8m2HprRakp4dZkR6jIA3I/+JoEtz+495y4EwpBQDg29PLDAheE498SiDzClhEebKSABRRTLGgiSozXpBFM8oBivm26CQKdKGEmxINyAnxuQzuMIOTA4YxAxAKkjFXWLI8g4JERCGikQBHrWoSBEh0nkSYPCJb6ZDIzMMgeEapGE+4PnhkRA8tY25DsV2rRosSzj/xMRNCPTNASEiomfcS8Kyq9dNNRNkhEsNHQWbURGGAd89Zc9+QA30gYMKLYQ5D9U41JIAlDEoZu7OI/cJcs9xDeSvNClcUW0UABIyS8Aw4xLKy5Aw5gt84AgCuRAAaeIMAYERJ8MoEGAAS1AkQQADs=" width="143" /></td>
			</tr>
			<tr valign="top">
				<td style="font-family:Arial;font-size:12px;width:260px">
					<a href="http://pas.bij12.nl/content/helpdesk" style="color:#333;text-decoration:none">pas.bij12.nl/content/helpdesk</a><br />
					<a href="http://www.aerius.nl" style="color:#333;text-decoration:none">www.aerius.nl</a><br />
					<a href="http://twitter.com/AERIUSapp" style="color:#333;text-decoration:none">twitter.com/AERIUSapp</a><br />
					&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" style="font-family:Arial;color:#6e6e6e;font-size:12px;padding-top:8px">
					Evaluation of AERIUS is a collaboration between the British and Dutch government</td>
			</tr>
		</tbody>
	</table>
</div>
<div style="width:600px;text-align:center">
	<div class="tpl-content-highlight" style="width:560px;padding:40px 20px 0"><p style="font-family:Arial;font-size:12px;color:#6e6e6e">The owner of AERIUS accepts no responsibility for the use of information by its users. AERIUS is a registered trademark in Europa. All rights reserved.</p>
	</div>
    </td></tr></table>
</body>
</html>'
	WHERE key = 'MAIL_CONTENT_TEMPLATE' AND language_code = 'en';

UPDATE i18n.messages set message = 'Your AERIUS Connect API key'
	WHERE key = 'CONNECT_APIKEY_CONFIRM_SUBJECT' AND language_code = 'en';
UPDATE i18n.messages set message =
E'<p style="font-family:Arial;font-size:14px;">Dear Sir/Madam,</p>
<p style="font-family:Arial;font-size:14px;">You have requested at [CALC_CREATION_DATE] [CALC_CREATION_TIME] an API key for AERIUS Connect.
The API key had been generated and can be used immediately to gain acces to the AERIUS Connect services.</p>
<p style="font-family:Arial;font-size:14px;">Your API key is: <b>[CONNECT_APIKEY]</b></p>'
	WHERE key = 'CONNECT_APIKEY_CONFIRM_BODY' AND language_code = 'en';
