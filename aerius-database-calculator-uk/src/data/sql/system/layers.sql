{import_common 'UK/system/layers.sql'}

--wms layers from 11+
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (11, 11, 'wms', 'calculator:wms_nature_areas_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (12, 12, 'wms', 'calculator:wms_depositions_jurisdiction_policies_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (13, 13, 'wms', 'calculator:wms_habitat_areas_sensitivity_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (14, 14, 'wms', 'calculator:wms_habitat_types');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (17, 10, 'wms', 'calculator:wms_habitats_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (18, 10, 'wms', 'calculator:wms_relevant_habitat_info_for_receptor_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (19, 25, 'wms', 'calculator:wms_calculation_substance_deposition_results_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (20, 27, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (21, 28, 'wms', 'calculator:wms_province_areas_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (23, 30, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (24, 31, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_percentage');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (25, 36, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_label');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (26, 37, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_percentage_label');

-- theme layers
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (5, 'nca', 0, true, true); -- OS-Scale-Dependent
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (11, 'nca', 4, false, true); -- natuurgebieden
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (12, 'nca', 5, false, false); -- achtergronddeposities
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (13, 'nca', 6, false, false); -- stikstofgevoeligheid
INSERT INTO system.theme_layers (layer_id, theme, sort_order, part_of_base_layer, visible)
  VALUES (14, 'nca', 7, false, false); --habittattypen
