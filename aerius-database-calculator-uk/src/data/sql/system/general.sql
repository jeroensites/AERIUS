{import_common 'UK/system/general.sql'}

{import_common 'UK/system/sectors.sql'}
{import_common 'UK/system/sectors_sectorgroup.sql'}

{import_common 'UK/system/plans.sql'}

{import_common 'UK/system/gml_conversions.sql'}

-- Calculation config for the UI
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_ENGINE_UNITS_UI_MAX', '20000');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_UI_MIN', '5');
INSERT INTO system.constants (key, value) VALUES ('CHUNKER_RECEPTORS_UI_MAX', '50');

INSERT INTO system.constants (key, value) VALUES ('MAX_ENGINE_SOURCES_UI', '1000000');
INSERT INTO system.constants (key, value) VALUES ('NUMBER_OF_TASKCLIENT_THREADS', '40');

-- Help URLs
INSERT INTO system.constants (key, value) VALUES ('MANUAL_URL', 'https://www.aerius.nl/files/media/handleiding/calculator_2021/syllabus_werken_met_aerius_calculator.pdf');
INSERT INTO system.constants (key, value) VALUES ('QUICK_START_URL', 'https://www.aerius.nl/files/media/handleiding/calculator_2021/snel_op_weg_met_aerius_calculator.pdf');
INSERT INTO system.constants (key, value) VALUES ('BIJ12_HELPDESK_URL', 'https://www.bij12.nl/onderwerpen/stikstof-en-natura2000/helpdesk');
INSERT INTO system.constants (key, value) VALUES ('OPS_FACTSHEET_URL', 'https://www.aerius.nl/factsheets/gebouwinvloed-en-pluimstijging');
INSERT INTO system.constants (key, value) VALUES ('RELEASE_DETAILS_URL', 'https://www.aerius.nl/');

-- Calculator limits
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_LIMITS_MAX_SOURCES', '20000');
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_LIMITS_MAX_LINE_LENGTH', '100000');
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_LIMITS_MAX_POLYGON_SURFACE', '5000');
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_LIMITS_RECEPTORS_RECEPTOR_TILE', '9');
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_LARGE_CALCULATIONS_SPLIT', '100');

-- Layer references
INSERT INTO system.constants (key, value) VALUES ('LAYER_SHIP_NETWORK', 'calculator:wms_shipping_maritime_network_view');
INSERT INTO system.constants (key, value) VALUES ('LAYER_MARITIME_SHIP_NETWORK', 'nzvss_beg,nzvss_sym,nzvss_sep');

-- Calculator min/max years (used for selection-range for user).
INSERT INTO system.constants (key, value) VALUES ('MIN_YEAR', '2019');
INSERT INTO system.constants (key, value) VALUES ('MAX_YEAR', '2035');

-- Source related
INSERT INTO system.constants (key, value) VALUES ('MARITIME_MOORING_INLAND_ROUTE_SHIPPING_NODE_SEARCH_DISTANCE', '100');

-- New services endpoint
INSERT INTO system.constants (key, value) VALUES ('FILE_SERVICE_URL_INTERNAL', 'https://localhost:8080/v1/files');

-- Connect constants
INSERT INTO system.constants (key, value) VALUES ('CONNECT_GENERATING_APIKEY_ENABLED', 'true');
INSERT INTO system.constants (key, value) VALUES ('CONNECT_MAX_CONCURRENT_JOBS_FOR_NEW_USER', '3');
INSERT INTO system.constants (key, value) VALUES ('CONNECT_DEFAULT_PERIOD_JOB_RATE_LIMIT', '100');
INSERT INTO system.constants (key, value) VALUES ('CONNECT_UTIL_VALIDATION_THRESHOLD_KB', '10000');

-- Collect feedback constants
INSERT INTO system.constants (key, value) VALUES ('COLLECT_FEEDBACK', 'true');
INSERT INTO system.constants (key, value) VALUES ('COLLECT_COLLECTOR_ID', '7130abc1');

-- Easter egg constants
INSERT INTO system.constants (key, value) VALUES ('TETRIS_ENDPOINT_URL', 'https://tetris.aerius.nl/api/');
INSERT INTO system.constants (key, value) VALUES ('TETRIS_ORIGIN_POINT', '433967:332696');
