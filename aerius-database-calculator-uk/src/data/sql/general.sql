{import_common 'general/'}

{import_common 'sectors/'}

{import_common 'UK/areas_hexagons_and_receptors/load.sql'}

-- Calculations part (permit_calculation_radius_types) seems to be unused, so don't bother importing.
/*import_common 'calculations/'*/

{import_common 'UK/deposition_jurisdiction_policies/'}

-- Unsure if development spaces and rules should be a thing in UK, but probably not.
/*import_common 'UK/development_spaces_and_rules/'*/

/*import_common 'modules/shipping/'*/

{import_common 'UK/emission_factors/'}

{import_common 'pdf_export/'}