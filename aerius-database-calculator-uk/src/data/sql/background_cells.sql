BEGIN; SELECT setup.ae_load_table('background_cells', '{data_folder}/UK/public/background_cells_20211110.txt', TRUE); COMMIT;
BEGIN; SELECT setup.ae_load_table('background_cell_concentrations', '{data_folder}/UK/public/background_cell_concentrations_20211110.txt', TRUE); COMMIT;
-- Not (yet) supplied
--BEGIN; SELECT setup.ae_load_table('background_cell_depositions', '{data_folder}/UK/public/background_cell_depositions_20161021.txt'); COMMIT;


BEGIN;
	INSERT INTO background_cell_results
		SELECT background_cell_id, year, substance_id, result_type, result FROM setup.build_background_cell_results_view;
COMMIT;

BEGIN;
	INSERT INTO receptors_to_background_cells
		SELECT receptor_id, background_cell_id FROM setup.build_receptors_to_background_cells_view;
COMMIT;
